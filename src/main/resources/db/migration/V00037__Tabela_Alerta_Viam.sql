CREATE TABLE public.alerta_viam
(
    id integer NOT NULL,
    data_do_registro timestamp with time zone,
    data_do_registro_mobile timestamp without time zone,
    tipo_de_evento integer,
    empresa integer,
    motorista integer,
    jornada integer,
    status integer,
    data_da_notificacao timestamp without time zone,
    quantidade_clique_ok integer,
    quantidade_clique_cancelar integer,
    CONSTRAINT pk_alerta_viam PRIMARY KEY (id),
    CONSTRAINT fk_alerta_eventos_jornada FOREIGN KEY (tipo_de_evento)
        REFERENCES public.eventos_jornada (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT fk_alertta_empresa FOREIGN KEY (empresa)
        REFERENCES public.empresas (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT fk_alerta_motorista FOREIGN KEY (motorista)
        REFERENCES public.motoristas (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.alerta_viam
    OWNER to postgres;
COMMENT ON TABLE public.alerta_viam
    IS 'Tabela para tratar dos alertas oriundos do via-m';

COMMENT ON COLUMN public.alerta_viam.data_do_registro
    IS 'Data que o registro foi recebido pelo servidor';

COMMENT ON COLUMN public.alerta_viam.data_do_registro_mobile
    IS 'data que o registro foi criado no mobile';

COMMENT ON COLUMN public.alerta_viam.status
    IS '1 = Ativo
2 = Inativo
3 = Notificado';

COMMENT ON COLUMN public.alerta_viam.data_da_notificacao
    IS 'Quando o alerta apareceu para o motorista';
	
CREATE SEQUENCE public.alerta_viam_id_seq;

ALTER SEQUENCE public.alerta_viam_id_seq
    OWNER TO postgres;