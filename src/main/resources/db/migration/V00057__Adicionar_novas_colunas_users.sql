ALTER TABLE public.users
    ADD COLUMN cadastro_hodometro boolean DEFAULT false;

ALTER TABLE public.users
    ADD COLUMN relatorio_presenca_equipamento boolean DEFAULT false;

ALTER TABLE public.users
    ADD COLUMN relatorio_trasnporte_equipamento boolean DEFAULT false;