CREATE TABLE public.permissao
(
    id integer NOT NULL,
    descricao character varying(255) COLLATE pg_catalog."default" NOT NULL DEFAULT ''::character varying,
    grupo character varying(255) COLLATE pg_catalog."default" NOT NULL DEFAULT ''::character varying,
    nome character varying(255) COLLATE pg_catalog."default" NOT NULL DEFAULT ''::character varying,
    menu character varying(45) COLLATE pg_catalog."default" NOT NULL DEFAULT ''::character varying,
    url_navegacao character varying(100) COLLATE pg_catalog."default" NOT NULL DEFAULT ''::character varying,
    sistema integer,
    CONSTRAINT "primary_permissao" PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;
ALTER TABLE public.permissao OWNER to postgres;
CREATE SEQUENCE public.permissao_id_seq;
ALTER SEQUENCE public.permissao_id_seq OWNER TO postgres;
COMMENT ON COLUMN public.permissao.sistema IS 'representa o sistema que vai ser usado. 1 = Sistema ViaM 2 = Sistema ViaSat';