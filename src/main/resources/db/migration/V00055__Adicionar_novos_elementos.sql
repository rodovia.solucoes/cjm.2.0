ALTER TABLE public.users
    ADD COLUMN cadastro_macro boolean DEFAULT false;
	
CREATE TABLE public.macros
(
    id serial NOT NULL,
    nome character varying(20),
    data_cadastro timestamp without time zone,
	status boolean,
    empresa serial,
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.macros
    OWNER to postgres;
COMMENT ON TABLE public.macros
    IS 'Tabela responsável pelas Macros do sistema ';
	
CREATE TABLE public.evento_macro
(
    evento character varying(20),
    valor integer,
    id_macro serial
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.evento_macro
    OWNER to postgres;
COMMENT ON TABLE public.evento_macro
    IS 'Tabela para armazenar os eventos e numeros das macros';
