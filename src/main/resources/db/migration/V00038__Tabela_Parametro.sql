CREATE TABLE public.parametro
(
    id integer NOT NULL,
    tempo_direcao character varying(5),
    tempo_jornada_finalizada character varying(5),
    tempo_refeicao character varying(5),
    empresa_id integer,
    CONSTRAINT fk_parametro PRIMARY KEY (id),
    CONSTRAINT fk_parametro_empresa FOREIGN KEY (empresa_id)
        REFERENCES public.empresas (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.parametro
    OWNER to postgres;
	
CREATE SEQUENCE public.parametro_id_sequencia;

ALTER SEQUENCE public.parametro_id_sequencia
    OWNER TO postgres;
	
ALTER TABLE public.empresas
    ADD COLUMN parametro integer;