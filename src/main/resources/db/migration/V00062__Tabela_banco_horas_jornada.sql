CREATE TABLE banco_horas_jornada (
	id serial4 NOT NULL,
	motoristas_id int4 NOT NULL,
	jornada_id int4 NOT NULL,
	banco_horas_id int4 NOT NULL,
	he_normal_ate_2h float8 NULL,
	he_autorizada_pela_empresa float8 NULL,
	he_nao_autorizada_pela_empresa float8 NULL,
	he_acima_2h float8 NULL,
	he_intrajornada float8 NULL,
	he_interjornada float8 NULL,
	he_dsr float8 NULL,
	he_feriado float8 NULL,
	he_descanso_obrigatorio float8 NULL,
	he_acima_2h_ate_4h float8 NULL,
	hora_faltosa float8 NULL,
	falta float8 NULL,
	hora_abonada float8 NULL,
	tipo_hora_abonada bpchar(1) NULL,
	tempo_de_espera float8 NULL,
	validade_banco_horas timestamp without time zone NULL,
	CONSTRAINT primary_banco_horas_jornada PRIMARY KEY (id)
);

ALTER TABLE banco_horas_jornada ADD CONSTRAINT fk_banco_horas_jornada_jornada FOREIGN KEY (jornada_id) REFERENCES jornada(id);
ALTER TABLE banco_horas_jornada ADD CONSTRAINT fk_banco_horas_jornada_motoristas_id FOREIGN KEY (motoristas_id) REFERENCES motoristas(id);
ALTER TABLE banco_horas_jornada ADD CONSTRAINT fk_banco_horas_jornada_banco_horas_id FOREIGN KEY (banco_horas_id) REFERENCES banco_horas(id);

