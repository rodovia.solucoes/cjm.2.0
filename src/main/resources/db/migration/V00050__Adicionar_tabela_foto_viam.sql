CREATE TABLE public.foto_viam
(
    id integer NOT NULL,
    data_registro timestamp without time zone,
    foto text,
    descricao text,
    latitude double precision,
    longitude double precision,
    data_foto timestamp without time zone,
    motorista integer,
    PRIMARY KEY (id),
    CONSTRAINT foto_viam_motorista FOREIGN KEY (motorista)
        REFERENCES public.motoristas (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.foto_viam
    OWNER to postgres;
COMMENT ON TABLE public.foto_viam
    IS 'Tabela para armazenar as fotos que foram tiradas dos aplicativos';
	
CREATE SEQUENCE public.foto_viam_id_seq;

ALTER SEQUENCE public.foto_viam_id_seq OWNER TO postgres;