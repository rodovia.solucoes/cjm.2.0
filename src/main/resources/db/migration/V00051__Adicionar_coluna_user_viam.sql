ALTER TABLE public.users
    ADD COLUMN consulta_jornada boolean DEFAULT True;
	
ALTER TABLE public.users
    ADD COLUMN consulta_grade_jornada boolean DEFAULT True;
	
ALTER TABLE public.users
    ADD COLUMN relatorios_viam_marcacao boolean DEFAULT True;
	
	
ALTER TABLE public.users
    ADD COLUMN relatorios_viam_chat boolean DEFAULT True;
	
	
ALTER TABLE public.users
    ADD COLUMN relatorios_viam_evento boolean DEFAULT True;
	
	
ALTER TABLE public.users
    ADD COLUMN relatorios_viasat_carama_fria boolean DEFAULT True;
	
		
ALTER TABLE public.users
    ADD COLUMN relatorios_viasat_disponibilidade boolean DEFAULT True;
	
	
ALTER TABLE public.users
    ADD COLUMN relatorios_viasat_dispositivo_portatil boolean DEFAULT True;
	
	
ALTER TABLE public.users
    ADD COLUMN relatorios_viasat_eventos boolean DEFAULT True;
	
	
ALTER TABLE public.users
    ADD COLUMN relatorios_viasat_horimetro boolean DEFAULT True;
	
	
ALTER TABLE public.users
    ADD COLUMN relatorios_viasat_hodometro boolean DEFAULT True;
	
	
ALTER TABLE public.users
    ADD COLUMN relatorios_viasat_percurso boolean DEFAULT True;
	
	
ALTER TABLE public.users
    ADD COLUMN relatorios_viasat_velocidade boolean DEFAULT True;
	
	
ALTER TABLE public.users
    ADD COLUMN relatorios_viasat_tomada_forca boolean DEFAULT True;

	
ALTER TABLE public.users
    ADD COLUMN cadastro_unidade boolean DEFAULT True;
	

ALTER TABLE public.users
    ADD COLUMN cadastro_motoristas boolean DEFAULT True;

	
ALTER TABLE public.users
    ADD COLUMN cadastro_veiculos boolean DEFAULT True;

	
ALTER TABLE public.users
    ADD COLUMN cadastro_locais boolean DEFAULT True;

	
ALTER TABLE public.users
    ADD COLUMN cadastro_feriados boolean DEFAULT True;

	
ALTER TABLE public.users
    ADD COLUMN cadastro_usuario boolean DEFAULT True;
	
	
ALTER TABLE public.users
    ADD COLUMN cadastro_cor boolean DEFAULT True;
	
ALTER TABLE public.users
    ADD COLUMN cadastro_tolerancia boolean DEFAULT True;
	
ALTER TABLE public.users
    ADD COLUMN cadastro_turno boolean DEFAULT True;
	
	
ALTER TABLE public.users
    ADD COLUMN cadastro_motivo boolean DEFAULT True;
	
ALTER TABLE public.users
    ADD COLUMN cadastro_equipamento_portatil boolean DEFAULT True;
	
ALTER TABLE public.users
    ADD COLUMN cadastro_tipo_veiculo boolean DEFAULT True;
	
ALTER TABLE public.users
    ADD COLUMN cadastro_modelo_veiculo boolean DEFAULT True;
	
ALTER TABLE public.users
    ADD COLUMN cadastro_tipo_equipamento boolean DEFAULT True;
	
ALTER TABLE public.users
    ADD COLUMN acessar_integracao boolean DEFAULT True;
	
	
ALTER TABLE public.users
    ADD COLUMN acessar_viam boolean DEFAULT True;
	
ALTER TABLE public.users
    ADD COLUMN acessar_viasat boolean DEFAULT True;

	