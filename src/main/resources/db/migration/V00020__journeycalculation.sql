CREATE SEQUENCE IF NOT EXISTS journey_calculation_id_seq;
CREATE SEQUENCE IF NOT EXISTS journey_calculation_line_id_seq;
CREATE SEQUENCE IF NOT EXISTS nonconformity_id_seq;

CREATE TABLE journey_calculation (
    id bigint NOT NULL DEFAULT nextval('journey_calculation_id_seq'::regclass),
    start_date timestamp without time zone,
    end_date timestamp without time zone,
    driver_id integer,
    created_by integer,
    created_at timestamp without time zone,
    altered_by integer,
    altered_at timestamp without time zone,
    status integer,
    reference_month integer,
    CONSTRAINT journey_calculation_id_pk PRIMARY KEY (id)
);

CREATE TABLE journey_calculation_line (
    id bigint NOT NULL DEFAULT nextval('journey_calculation_line_id_seq'::regclass),
    date timestamp without time zone NOT NULL,
    daily_extra_hours_total bigint,
    daily_extra_hours_intrajourney bigint,
    daily_extra_hours_dsr bigint,
    daily_extra_hours_holiday bigint,
    nightly_extra_hours_total bigint,
    nightly_extra_hours_intrajourney bigint,
    nightly_extra_hours_dsr bigint,
    nightly_extra_hours_holiday bigint,
    interjourney_extra_hours bigint,
    daily_awaiting bigint,
    nightly_awaiting bigint,
    aditional_nightly bigint,
    graced_missing_hours bigint,
    deducted_missing_hours bigint,
    total_hours bigint,
    journey_type VARCHAR(255),
    journey_calculation_id bigint,
    CONSTRAINT journey_calculation_line_id_pk PRIMARY KEY (id)
);

CREATE TABLE nonconformity (
    id bigint NOT NULL DEFAULT nextval('nonconformity_id_seq'::regclass),
    calculus_type integer,
    type integer,
    description VARCHAR(255),
    treatment_type integer,
    treatment_description VARCHAR(255),
    treatment_args VARCHAR(255),
    driver integer,
    journey_calculation_line_id bigint,
    start_date timestamp without time zone,
    end_date timestamp without time zone,
    CONSTRAINT nonconformity_id_pk PRIMARY KEY (id)
)