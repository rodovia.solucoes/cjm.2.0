ALTER TABLE public.users
    ADD COLUMN relatorios_sensor_betoneira boolean DEFAULT false;

ALTER TABLE public.users
    ADD COLUMN cadastro_controle_combustivel boolean DEFAULT false;