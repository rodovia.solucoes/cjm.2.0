INSERT INTO public.permissao
(id, descricao, grupo, nome, menu, url_navegacao, sistema)
VALUES(1, 'Modelo de Veículos'::character varying, 'ADM1;ADM2;GER1;GER2'::character varying, 'Modelo de Veículos'::character varying, 'Cadastro'::character varying, 'NSA'::character varying, 2);

INSERT INTO public.permissao
(id, descricao, grupo, nome, menu, url_navegacao, sistema)
VALUES(2, 'Modelo de Equipamentos'::character varying, 'ADM1;ADM2;GER1;GER2'::character varying, 'Modelo de Equipamentos'::character varying, 'Cadastro'::character varying, 'NSA'::character varying, 2);

INSERT INTO public.permissao
(id, descricao, grupo, nome, menu, url_navegacao, sistema)
VALUES(3, 'Equipamentos'::character varying, 'ADM1;ADM2;GER1;GER2'::character varying, 'Equipamentos'::character varying, 'Cadastro'::character varying, 'NSA'::character varying, 2);

INSERT INTO public.permissao
(id, descricao, grupo, nome, menu, url_navegacao, sistema)
VALUES(4, 'Veículos'::character varying, 'ADM1;ADM2;GER1;GER2'::character varying, 'Veículos'::character varying, 'Cadastro'::character varying, 'NSA'::character varying, 2);

INSERT INTO public.permissao
(id, descricao, grupo, nome, menu, url_navegacao, sistema)
VALUES(5, 'Usuário'::character varying, 'ADM1;ADM2;GER1;GER2'::character varying, 'Usuário'::character varying, 'Cadastro'::character varying, 'NSA'::character varying, 1);

INSERT INTO public.permissao
(id, descricao, grupo, nome, menu, url_navegacao, sistema)
VALUES(6, 'Dashboard'::character varying, 'ADM1;ADM2;GER1;GER2'::character varying, 'Dashboard'::character varying, 'Dashboard'::character varying, 'NSA'::character varying, 1);

INSERT INTO public.permissao
(id, descricao, grupo, nome, menu, url_navegacao, sistema)
VALUES(7, 'Relatórios Gerenciais'::character varying, 'ADM1;ADM2;GER1;GER2'::character varying, 'Relatórios Gerenciais'::character varying, 'Relatórios'::character varying, 'NSA'::character varying, 2);

INSERT INTO public.permissao
(id, descricao, grupo, nome, menu, url_navegacao, sistema)
VALUES(8, 'Empresas'::character varying, 'ADM1;ADM2;GER1;GER2'::character varying, 'Empresas'::character varying, 'Cadastro'::character varying, 'NSA'::character varying, 1);

INSERT INTO public.permissao
(id, descricao, grupo, nome, menu, url_navegacao, sistema)
VALUES(9, 'Aviso'::character varying, 'ADM1;ADM2;GER1;GER2'::character varying, 'Aviso'::character varying, 'Aviso'::character varying, 'NSA'::character varying, 1);

INSERT INTO public.permissao
(id, descricao, grupo, nome, menu, url_navegacao, sistema)
VALUES(10, 'Modelo Equipamento'::character varying, 'ADM1;ADM2;GER1;GER2'::character varying, 'Modelo Equipamento'::character varying, 'Cadastro'::character varying, 'NSA'::character varying, 1);

INSERT INTO public.permissao
(id, descricao, grupo, nome, menu, url_navegacao, sistema)
VALUES(11, 'Modelo Veiculo'::character varying, 'ADM1;ADM2;GER1;GER2'::character varying, 'Modelo Veiculo'::character varying, 'Cadastro'::character varying, 'NSA'::character varying, 1);

INSERT INTO public.permissao
(id, descricao, grupo, nome, menu, url_navegacao, sistema)
VALUES(12, 'Local'::character varying, 'ADM1;ADM2;GER1;GER2'::character varying, 'Local'::character varying, 'Cadastro'::character varying, 'NSA'::character varying, 1);

INSERT INTO public.permissao
(id, descricao, grupo, nome, menu, url_navegacao, sistema)
VALUES(13, 'Consultas'::character varying, 'ADM1;ADM2;GER1;GER2'::character varying, 'Consultas'::character varying, 'Pesquisa'::character varying, 'NSA'::character varying, 1);

INSERT INTO public.permissao
(id, descricao, grupo, nome, menu, url_navegacao, sistema)
VALUES(14, 'Grade de Jornada'::character varying, 'ADM1;ADM2;GER1;GER2'::character varying, 'Grade de Jornada'::character varying, 'Pesquisa'::character varying, 'NSA'::character varying, 1);

INSERT INTO public.permissao
(id, descricao, grupo, nome, menu, url_navegacao, sistema)
VALUES(15, 'Grade de Jornada'::character varying, 'ADM1;ADM2;GER1;GER2'::character varying, 'Grade de Jornada'::character varying, 'Pesquisa'::character varying, 'NSA'::character varying, 1);

INSERT INTO public.permissao
(id, descricao, grupo, nome, menu, url_navegacao, sistema)
VALUES(16, 'Unidades'::character varying, 'ADM1;ADM2;GER1;GER2'::character varying, 'Unidade'::character varying, 'Cadastro'::character varying, 'NSA'::character varying, 1);


INSERT INTO public.permissao
(id, descricao, grupo, nome, menu, url_navegacao, sistema)
VALUES(17, 'Motoristas'::character varying, 'ADM1;ADM2;GER1;GER2'::character varying, 'Motoristas'::character varying, 'Cadastro'::character varying, 'NSA'::character varying, 1);


INSERT INTO public.permissao
(id, descricao, grupo, nome, menu, url_navegacao, sistema)
VALUES(18, 'Feriados'::character varying, 'ADM1;ADM2;GER1;GER2'::character varying, 'Feriados'::character varying, 'Cadastro'::character varying, 'NSA'::character varying, 1);


INSERT INTO public.permissao
(id, descricao, grupo, nome, menu, url_navegacao, sistema)
VALUES(19, 'Cor'::character varying, 'ADM1;ADM2;GER1;GER2'::character varying, 'Cor'::character varying, 'Cadastro'::character varying, 'NSA'::character varying, 1);

INSERT INTO public.permissao
(id, descricao, grupo, nome, menu, url_navegacao, sistema)
VALUES(20, 'Tolerância'::character varying, 'ADM1;ADM2;GER1;GER2'::character varying, 'Tolerância'::character varying, 'Cadastro'::character varying, 'NSA'::character varying, 1);

INSERT INTO public.permissao
(id, descricao, grupo, nome, menu, url_navegacao, sistema)
VALUES(21, 'Turno'::character varying, 'ADM1;ADM2;GER1;GER2'::character varying, 'Turno'::character varying, 'Cadastro'::character varying, 'NSA'::character varying, 1);

INSERT INTO public.permissao
(id, descricao, grupo, nome, menu, url_navegacao, sistema)
VALUES(22, 'Motivo'::character varying, 'ADM1;ADM2;GER1;GER2'::character varying, 'Motivo'::character varying, 'Cadastro'::character varying, 'NSA'::character varying, 1);

INSERT INTO public.permissao
(id, descricao, grupo, nome, menu, url_navegacao, sistema)
VALUES(23, 'Motivo'::character varying, 'ADM1;ADM2;GER1;GER2'::character varying, 'Motivo'::character varying, 'Cadastro'::character varying, 'NSA'::character varying, 1);

INSERT INTO public.permissao
(id, descricao, grupo, nome, menu, url_navegacao, sistema)
VALUES(24, 'Integração SASCAR'::character varying, 'ADM1;ADM2;GER1;GER2'::character varying, 'Integração SASCAR'::character varying, 'Externo'::character varying, 'NSA'::character varying, 1);




