ALTER TABLE public.empresas ADD COLUMN id_cliente_trajetto integer;

COMMENT ON COLUMN public.empresas.id_cliente_trajetto IS 'Representa o valor do ID cliente dentro do sistema Trajetto';
