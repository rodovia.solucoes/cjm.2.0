ALTER TABLE empresas ADD COLUMN IF NOT EXISTS considers_interjourney BOOLEAN DEFAULT TRUE;
ALTER TABLE empresas ADD COLUMN IF NOT EXISTS half_interjourney BOOLEAN DEFAULT TRUE;
ALTER TABLE empresas DROP COLUMN IF EXISTS interjourney_normal_8_11;
ALTER TABLE empresas DROP COLUMN IF EXISTS interjourney_normal_5_8;
ALTER TABLE empresas DROP COLUMN IF EXISTS interjourney_normal_5;