CREATE SEQUENCE IF NOT EXISTS JOURNEY_MANIFEST_ID_SEQ;

CREATE TABLE IF NOT EXISTS journey_manifest (
    id bigint NOT NULL DEFAULT nextval('JOURNEY_MANIFEST_ID_SEQ'::regclass),
    start_date timestamp without time zone,
    end_date timestamp without time zone,
    journey_id integer,
    manifest_number VARCHAR(255),
    CONSTRAINT journey_manifest_id_pk PRIMARY KEY (id)
);