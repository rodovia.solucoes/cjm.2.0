UPDATE empresas SET interjourney_compensation_limit = 0 WHERE interjourney_compensation_limit IS NULL;
UPDATE empresas SET interjourney_compensate_standby = false WHERE interjourney_compensate_standby IS NULL;
UPDATE empresas SET interjourney_normal_8_11 = false WHERE interjourney_normal_8_11 IS NULL;
UPDATE empresas SET interjourney_normal_5_8 = false WHERE interjourney_normal_5_8 IS NULL;
UPDATE empresas SET interjourney_normal_5 = false WHERE interjourney_normal_5 IS NULL;