ALTER TABLE public.empresas
    ADD COLUMN habilitar_viam boolean;
	
ALTER TABLE public.empresas
    ADD COLUMN habilitar_viasat boolean;

COMMENT ON COLUMN public.empresas.habilitar_viam
    IS 'Habilitar modulo Via-M no sistema';

COMMENT ON COLUMN public.empresas.habilitar_viasat
    IS 'Habilitar modulo Via-Sat no sistema';
	