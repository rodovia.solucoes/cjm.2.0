CREATE TABLE public.observacao
(
    id integer NOT NULL,
    data_cadastro timestamp without time zone,
    veiculo integer,
    viam integer,
    texto text,
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.observacao
    OWNER to postgres;
COMMENT ON TABLE public.observacao
    IS 'Tabela responsável pelas observações do sistema.';
	
CREATE SEQUENCE public.observacao_id_seq;

ALTER SEQUENCE public.observacao_id_seq
    OWNER TO postgres;
	