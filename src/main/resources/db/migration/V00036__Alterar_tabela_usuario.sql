ALTER TABLE public.users ADD COLUMN receber_alertas_eventos BOOLEAN;
ALTER TABLE public.users ADD COLUMN mandar_email BOOLEAN;
ALTER TABLE public.users ADD COLUMN mandar_sms BOOLEAN;
ALTER TABLE public.users ADD COLUMN mandar_sms_excesso_velocidade BOOLEAN;
ALTER TABLE public.users ADD COLUMN numero_celular_envio_sms VARCHAR(20);
ALTER TABLE public.users ADD COLUMN token_mobile VARCHAR(160);
ALTER TABLE public.users ADD COLUMN tipo_sistema_mobile VARCHAR(6);
ALTER TABLE public.users ADD COLUMN data_ultima_atualizacao_token timestamp without time zone;
