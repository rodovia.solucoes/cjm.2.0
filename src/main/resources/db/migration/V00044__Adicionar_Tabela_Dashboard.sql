CREATE TABLE public.dashboard
(
    id integer NOT NULL,
    id_empresa integer,
    id_unidade integer,
    id_jornada integer,
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.dashboard
    OWNER to postgres;
COMMENT ON TABLE public.dashboard
    IS 'Tabela responsável pelos dados que podem vir do dashboard';
	
CREATE SEQUENCE public.dashboard_id_seq;

ALTER SEQUENCE public.dashboard_id_seq
    OWNER TO postgres;
	