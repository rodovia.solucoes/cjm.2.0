CREATE TABLE IF NOT EXISTS sascar (
  id serial NOT NULL,
  usuario_sascar VARCHAR(20) NOT NULL,
  senha_sascar VARCHAR(50),
  id_veiculo_sascar INTEGER,
  operador_lancamento INTEGER,
  CONSTRAINT sascar_pkey PRIMARY KEY(id)
)
WITH (oids = false);

ALTER TABLE IF EXISTS sascar
    add constraint UK_operador_lancamento unique (operador_lancamento);