CREATE TABLE public.controle_combustivel
(
    id serial NOT NULL,
    data_cadastro timestamp without time zone,
    id_veiculo_trajetto integer,
    quantidade_abastecida double precision,
    valor_dia double precision,
    tipo_combustivel character varying(2),
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.controle_combustivel
    OWNER to postgres;
COMMENT ON TABLE public.controle_combustivel
    IS 'Responsavel pelo controle de combustivel dos veiculos';