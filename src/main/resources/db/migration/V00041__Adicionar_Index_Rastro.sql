CREATE INDEX index_rastro_motorista
    ON public.rastro USING btree
    (motorista_id ASC NULLS LAST)
    TABLESPACE pg_default;
	
CREATE INDEX index_rastro_empresa
    ON public.rastro USING btree
    (empresa_id ASC NULLS LAST)
    TABLESPACE pg_default;
	
	
CREATE INDEX index_rastro_instante_envio
    ON public.rastro USING btree
    (instante_envio ASC NULLS LAST)
    TABLESPACE pg_default;
	