ALTER TABLE public.rastro
    ADD COLUMN endereco text;

COMMENT ON COLUMN public.rastro.endereco
    IS 'Endereco onde esta o celular';

ALTER TABLE public.rastro
    ADD COLUMN bairro text;

COMMENT ON COLUMN public.rastro.bairro
    IS 'Bairro onde esta o celular';
	
ALTER TABLE public.rastro
    ADD COLUMN cidade text;

COMMENT ON COLUMN public.rastro.cidade
    IS 'cidade onde esta o celular';
	
ALTER TABLE public.rastro
    ADD COLUMN estado character varying(2);

COMMENT ON COLUMN public.rastro.estado
    IS 'Estado onde esta o celular';
	