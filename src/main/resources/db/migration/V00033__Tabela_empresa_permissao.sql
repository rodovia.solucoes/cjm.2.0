CREATE TABLE public.empresas_permissao
(
    id integer NOT NULL,
    id_permissao integer NOT NULL,
    id_empresas integer NOT NULL,
    CONSTRAINT "primary_empresas_permissao" PRIMARY KEY (id),
    CONSTRAINT fk_empresas_permissao_permissao FOREIGN KEY (id_permissao)
        REFERENCES public.permissao (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_empresas_permissao_empresas FOREIGN KEY (id_empresas)
        REFERENCES public.empresas (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.empresas_permissao OWNER to postgres;
CREATE SEQUENCE public.empresas_permissao_id_seq;
ALTER SEQUENCE public.empresas_permissao_id_seq OWNER TO postgres;