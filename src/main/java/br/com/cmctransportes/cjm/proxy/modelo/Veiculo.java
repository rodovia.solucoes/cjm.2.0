package br.com.cmctransportes.cjm.proxy.modelo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

@Getter
@Setter
public class Veiculo implements Serializable {

    private Integer id;
    private String anoFabricacao;
    private String anoModelo;
    private String chassi;
    private String cor;
    private Date dataCadastro;
    private String observacao;
    private String placa;
    private Double velocidadeMaxima;
    private Integer velocidadeMaximaChuva;
    private Integer velocidadeMaximaDesacelaracao;
    private Integer velocidadeMaximaCurva;
    private String renavam;
    private Boolean status;
    private String tipo;
    private String cidade;
    private String telefoneProprietario;
    private Boolean usaEntradaDigitalUm;
    private Boolean usaEntradaDigitalDois;
    private Boolean usaEntradaDigitalTres;
    private Boolean usaEntradaDigitalQuatro;
    private Boolean excessoVelocidade;
    private Boolean antiJummer;
    private Boolean bateriaCarroBaixa;
    private Boolean faltaEnergiaPrincipal;
    private Boolean panico;
    private Boolean quantidadeDiasSemTrasmissao;
    private Boolean roubo;
    private Boolean semComunicacao;
    private Boolean vinteQuatroHorasComunicacao;
    private Boolean ativaRotaNoMapa;
    private Boolean ativaValidacaoDeCerca;
    private String trocaDeHorimetro = "00:00;";
    private Integer rpmModoEconomicoMinimo;
    private Integer rpmModoEconomicoMaximo;
    private Integer rpmMaximo;
    private Integer rpmInicioFaixaAzul;
    private Integer rpmFimFaixaAzul;
    private Integer rpmInicioFaixaEconomica;
    private Integer rpmFimFaixaEconomica;
    private Integer rpmInicioFaixaVerde;
    private Integer rpmFimFaixaVerde;
    private Integer rpmInicioFaixaAmarela;
    private Integer rpmFimFaixaAmarela;
    private Integer rpmInicioMarchaLenta;
    private Integer rpmFimMarchaLenta;
    private Boolean geraEnderecoAutomatico;
    private Boolean validarIbutton;
    private Double cargaMaxima;
    private String combustivel;
    private Boolean gerarControleViagemAutomatico;
    private Double distanciaMinimaViagemAutomatica;
    private String tipoEntradaDigital;
    private String tipoEntradaDigitalDois;
    private String tipoEntradaDigitalTres;
    private String tipoEntradaDigitalQuatro;
    private Boolean ativarModuloTemperaturaCamaraFria;
    private Boolean enviarNotificacaoTemperaturaAplicativo;
    private Double temperaturaMinima;
    private Double temperaturaMaxima;
    private Integer tipoDeVeiculo;//Representa o tipo de veiculo que vem do sistema rodovia
    private Modelo modelo;
    private Equipamento equipamento;
    private Equipamento equipamentoDois;
    private Cliente cliente;
    private Integer unidadeId;
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal faturamento;
    private Integer porcentagemFaturamento;
    private Boolean cercaMovel;
    private Integer raioCercaMovel;
    private Boolean validaCercaMovel;
    private Boolean bloquearVeiculo;
    private Boolean validarRota;
    private String ultimoHodometro;
    private Boolean mostrarViam;


}
