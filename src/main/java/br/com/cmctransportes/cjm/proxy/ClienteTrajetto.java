package br.com.cmctransportes.cjm.proxy;

import br.com.cmctransportes.cjm.constants.Constantes;
import br.com.cmctransportes.cjm.domain.entities.Empresas;
import br.com.cmctransportes.cjm.logger.LogSistema;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;


public class ClienteTrajetto {

    private static ClienteTrajetto clienteTrajetto;

    private ClienteTrajetto(){

    }

    public static ClienteTrajetto getInstance(){
        try {
            synchronized (ClienteTrajetto.class) {
                if (clienteTrajetto == null) {
                    clienteTrajetto = new ClienteTrajetto();
                }
            }
        } catch (Exception e) {
            LogSistema.logError("GeraEvento", e);
        }
        return clienteTrajetto;
    }

    public void cadastrarCliente(Empresas empresas){
        try {
            HttpGet get = new HttpGet(Constantes.URL_SERVICO_TRAJETTO.concat("/cliente/cadastrarClienteRodovia/"+empresas.getCnpj()));
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(get);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                ObjectMapper mapper = new ObjectMapper();
                JsonNode node = mapper.readTree(response.getEntity().getContent());
                JsonNode cliente = node.path("cliente");
                empresas.setIdClienteTrajetto(cliente.path("id").asInt());
            }
        }catch (Exception e){
            LogSistema.logError("cadastrarCliente", e);
        }
    }
}
