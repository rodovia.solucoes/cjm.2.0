package br.com.cmctransportes.cjm.proxy;

import br.com.cmctransportes.cjm.constants.Constantes;
import br.com.cmctransportes.cjm.domain.entities.vo.EquipamentoPortatilVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoEquipamentoPortatilVO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.util.Date;

public class EquipamentoPortatilTrajetto {

    private static EquipamentoPortatilTrajetto equipamentoPortatilTrajetto;

    private EquipamentoPortatilTrajetto(){

    }

    public static EquipamentoPortatilTrajetto getInstance(){
        try {
            synchronized (EquipamentoPortatilTrajetto.class) {
                if (equipamentoPortatilTrajetto == null) {
                    equipamentoPortatilTrajetto = new EquipamentoPortatilTrajetto();
                }
            }
        } catch (Exception e) {
            LogSistema.logError("EquipamentoPortatilTrajetto", e);
        }
        return equipamentoPortatilTrajetto;
    }

    public RetornoEquipamentoPortatilVO cadastrar(EquipamentoPortatilVO equipamentoPortatilVO ) {
        RetornoEquipamentoPortatilVO retorno = new RetornoEquipamentoPortatilVO();
        try {
            equipamentoPortatilVO.setDataCadastro(new Date());
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/equipamentoportatil/cadastrar"));
            ObjectMapper mapper = new ObjectMapper();
            String json =  mapper.writeValueAsString(equipamentoPortatilVO);
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoEquipamentoPortatilVO.class);
            }

        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("cadastrar", e);
        }
        return retorno;
    }

    public RetornoEquipamentoPortatilVO editar(EquipamentoPortatilVO equipamentoPortatilVO) {
        RetornoEquipamentoPortatilVO retorno = new RetornoEquipamentoPortatilVO();
        try {
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/equipamentoportatil/editar"));
            ObjectMapper mapper = new ObjectMapper();
            String json =  mapper.writeValueAsString(equipamentoPortatilVO);
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoEquipamentoPortatilVO.class);
            }
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("editar", e);
        }
        return retorno;
    }

    public RetornoEquipamentoPortatilVO buscarPeloCodigo(Integer idEquipamento) {
        RetornoEquipamentoPortatilVO retorno = new RetornoEquipamentoPortatilVO();
        try {
            HttpGet get = new HttpGet(Constantes.URL_SERVICO_TRAJETTO.concat("/equipamentoportatil/buscar/"+idEquipamento.toString()));
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(get);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                ObjectMapper mapper = new ObjectMapper();
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoEquipamentoPortatilVO.class);
            }
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarPeloCodigo", e);
        }
        return retorno;
    }

    public RetornoEquipamentoPortatilVO listar(Integer idCliente, Integer idUnidade) {
        RetornoEquipamentoPortatilVO retorno = new RetornoEquipamentoPortatilVO();
        try {
            HttpGet get = new HttpGet(Constantes.URL_SERVICO_TRAJETTO.concat("/equipamentoportatil/listar/"+idCliente.toString()+"/"+idUnidade));
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(get);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                ObjectMapper mapper = new ObjectMapper();
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoEquipamentoPortatilVO.class);
            }
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("listar", e);
        }
        return retorno;
    }
}
