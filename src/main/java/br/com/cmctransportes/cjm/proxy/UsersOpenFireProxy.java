package br.com.cmctransportes.cjm.proxy;

import br.com.cmctransportes.cjm.domain.entities.UsersOpenFire;
import br.com.cmctransportes.cjm.logger.LogSistema;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

public class UsersOpenFireProxy {

    private static UsersOpenFireProxy usersOpenFire;

    public static UsersOpenFireProxy getInstance(){
        try {
            synchronized (UsersOpenFireProxy.class) {
                if (usersOpenFire == null) {
                    usersOpenFire = new UsersOpenFireProxy();
                }
            }
        } catch (Exception e) {
            LogSistema.logError("UsersOpenFire", e);
        }
        return usersOpenFire;
    }

    public void alterarUsuario(String userName, String senha){
        try {
            String s = "http://app.rodoviasolucoes.com.br:9090/plugins/restapi/v1/users/"+userName;
            HttpPut put = new HttpPut(s);
            put.setHeader("Authorization", "NlT3h43vofMmXNsE");
            ObjectMapper mapper = new ObjectMapper();
            UsersOpenFire users = new UsersOpenFire();
            users.setUsername(userName);
            users.setPassword(senha);
            String json = mapper.writeValueAsString(users);
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            put.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(put);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                LogSistema.logInfo("Usuario atualizado");
            }
        }catch (Exception e){
            LogSistema.logError("alterarUsuario", e);
        }
    }


    public void cadastrarUsuario(String userName, String senha){
        try {
            String s = "http://app.rodoviasolucoes.com.br:9090/plugins/restapi/v1/users/";
            HttpPost post = new HttpPost(s);
            post.setHeader("Authorization", "NlT3h43vofMmXNsE");
            ObjectMapper mapper = new ObjectMapper();
            UsersOpenFire users = new UsersOpenFire();
            users.setUsername(userName);
            users.setPassword(senha);
            String json = mapper.writeValueAsString(users);
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                LogSistema.logInfo("Usuario Cadastrado");
            }
        }catch (Exception e){
            LogSistema.logError("cadastrarUsuario", e);
        }
    }



}
