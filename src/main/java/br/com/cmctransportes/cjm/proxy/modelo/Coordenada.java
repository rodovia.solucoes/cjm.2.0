package br.com.cmctransportes.cjm.proxy.modelo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class Coordenada implements Serializable {
    private Integer id;
    private Integer sequencia;
    private Double latitude;
    private Double longitude;
    private Boolean isRaio;
    private Double raio;
}
