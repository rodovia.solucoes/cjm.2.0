package br.com.cmctransportes.cjm.proxy;

import br.com.cmctransportes.cjm.constants.Constantes;
import br.com.cmctransportes.cjm.domain.entities.vo.ObservacaoVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoObservacaoVO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.proxy.modelo.EquipamentoPortatil;
import br.com.cmctransportes.cjm.proxy.modelo.Observacao;
import br.com.cmctransportes.cjm.proxy.modelo.Veiculo;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

public class ObservacaoTrajetto {
    private static ObservacaoTrajetto observacaoTrajetto;

    private ObservacaoTrajetto(){

    }

    public static ObservacaoTrajetto getInstance(){
        try {
            synchronized (ObservacaoTrajetto.class) {
                if (observacaoTrajetto == null) {
                    observacaoTrajetto = new ObservacaoTrajetto();
                }
            }
        } catch (Exception e) {
            LogSistema.logError("observacaoTrajetto", e);
        }
        return observacaoTrajetto;
    }

    public RetornoObservacaoVO cadastrarObservacao(ObservacaoVO observacaoVO){
        RetornoObservacaoVO retorno = new RetornoObservacaoVO();
        try {
            Observacao observacao = convertTO(observacaoVO);
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/observacao/cadastrar"));
            ObjectMapper mapper = new ObjectMapper();

            String json =  mapper.writeValueAsString(observacao);
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoObservacaoVO.class);
            }
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("cadastrarObservacao", e);
        }
        return retorno;
    }


    public RetornoObservacaoVO buscarListaDeObservacoes(Integer id) {
        RetornoObservacaoVO retorno = new RetornoObservacaoVO();
        try {
            HttpGet get = new HttpGet(Constantes.URL_SERVICO_TRAJETTO.concat("/observacao/buscarListaDeObservacoes/"+id.toString()));
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(get);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                ObjectMapper mapper = new ObjectMapper();
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoObservacaoVO.class);
            }
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarListaDeObservacoes", e);
        }
        return retorno;
    }


    private Observacao convertTO(ObservacaoVO observacaoVO){
        Observacao observacao = new Observacao();
        if(observacaoVO.getVeiculo() != null) {
            Veiculo veiculo = new Veiculo();
            veiculo.setId(observacaoVO.getVeiculo().getId());
            observacao.setVeiculo(veiculo);
        }
        if(observacaoVO.getEquipamentoPortatil() != null){
            EquipamentoPortatil equipamentoPortatil = new EquipamentoPortatil();
            equipamentoPortatil.setId(observacaoVO.getEquipamentoPortatil().getId());
            observacao.setEquipamentoPortatil(equipamentoPortatil);
        }
        observacao.setTexto(observacaoVO.getTexto());
        return observacao;
    }
}
