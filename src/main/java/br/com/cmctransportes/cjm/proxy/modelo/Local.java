package br.com.cmctransportes.cjm.proxy.modelo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Local {

    public Local() {

    }

    public Local(Integer id) {
        this.id = id;
    }

    private Integer id;
    private String nome;
    private Integer localPai;
    private Boolean temFilho;
    private Boolean ativo;
    private Boolean mostrarNoMapaPrincipal;
    private Boolean mostrarNomeNoMapa;
    private Boolean notificaEvento;
    private Integer codigoUnico;
    private Integer funcao;
    private String endereco;
    private String bairro;
    private String complemento;
    private String cidade;
    private String uf;
    private String contato;
    private String telefone;
    private String tipo;
    private Boolean permiteDescanso;
    private Cliente cliente;
    private List<Coordenada> listaDeCoordenadas;
}
