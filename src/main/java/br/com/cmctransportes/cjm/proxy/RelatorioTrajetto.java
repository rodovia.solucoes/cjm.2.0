package br.com.cmctransportes.cjm.proxy;

import br.com.cmctransportes.cjm.constants.Constantes;
import br.com.cmctransportes.cjm.domain.entities.vo.*;
import br.com.cmctransportes.cjm.domain.entities.vo.RelatorioConverterVO;
import br.com.cmctransportes.cjm.infra.dao.MotoristasVeiculosDAO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.protocol.HTTP;

import java.io.BufferedInputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class RelatorioTrajetto {
    private static RelatorioTrajetto relatorioTrajetto;

    private RelatorioTrajetto() {
    }

    public static RelatorioTrajetto getInstance() {
        try {
            synchronized (RelatorioTrajetto.class) {
                if (relatorioTrajetto == null) {
                    relatorioTrajetto = new RelatorioTrajetto();
                }
            }
        } catch (Exception e) {
            LogSistema.logError("RelatorioTrajetto", e);
        }
        return relatorioTrajetto;
    }


    public RetornoRelatorioVO buscarRelatorioDeAcionamentoDeCompreensor(FiltroVO filtroVO) {
        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            Filtro filtro = converter(filtroVO);
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/relatorio/buscarRelatorioDeAcionamentoDeCompreensor"));
            ObjectMapper mapper = new ObjectMapper();
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            String json = mapper.writeValueAsString(filtro);
            StringEntity input = new StringEntity(json);
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoRelatorioVO.class);
            }
        } catch (Exception e) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarRelatorioDeAcionamentoDeCompreensor", e);
        }
        return retorno;
    }

    public RetornoRelatorioVO buscarRelatorioAcionamentoDePrancha(FiltroVO filtroVO) {
        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            Filtro filtro = converter(filtroVO);
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/relatorio/buscarRelatorioAcionamentoDePrancha"));
            ObjectMapper mapper = new ObjectMapper();
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            String json = mapper.writeValueAsString(filtro);
            StringEntity input = new StringEntity(json);
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoRelatorioVO.class);
            }
        } catch (Exception e) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarRelatorioAcionamentoDePrancha", e);
        }
        return retorno;
    }

    public RetornoRelatorioVO buscarRelatorioDisponibilidade(FiltroVO filtroVO) {
        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            Filtro filtro = converter(filtroVO);
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/relatorio/buscarStatusIgnicao"));
            ObjectMapper mapper = new ObjectMapper();
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            String json = mapper.writeValueAsString(filtro);
            StringEntity input = new StringEntity(json);
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoRelatorioVO.class);
            }
        } catch (Exception e) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarRelatorioAcionamentoDePrancha", e);
        }
        return retorno;
    }

    public RetornoRelatorioVO buscarRelatorioDisponibilidadeGrafico(FiltroVO filtroVO) {
        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            Filtro filtro = converter(filtroVO);
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/relatorio/buscarStatusIgnicaoGrafico"));
            ObjectMapper mapper = new ObjectMapper();
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            String json = mapper.writeValueAsString(filtro);
            StringEntity input = new StringEntity(json);
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoRelatorioVO.class);
            }
        } catch (Exception e) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarRelatorioAcionamentoDePrancha", e);
        }
        return retorno;
    }

    public RetornoRelatorioVO buscarRelatorioEvento(FiltroVO filtroVO) {
        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            Filtro filtro = converter(filtroVO);
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/relatorio/buscarEvento"));
            ObjectMapper mapper = new ObjectMapper();
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            String json = mapper.writeValueAsString(filtro);
            StringEntity input = new StringEntity(json);
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoRelatorioVO.class);
            }
        } catch (Exception e) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarRelatorioEvento", e);
        }
        return retorno;
    }


    public RetornoRelatorioVO buscarRelatorioHorimetro(FiltroVO filtroVO) {
        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            Filtro filtro = converter(filtroVO);
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/relatorio/buscarRelatorioHorimetro"));
            ObjectMapper mapper = new ObjectMapper();
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            String json = mapper.writeValueAsString(filtro);
            StringEntity input = new StringEntity(json);
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoRelatorioVO.class);
            }
        } catch (Exception e) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarRelatorioHorimetro", e);
        }
        return retorno;
    }

    public RetornoRelatorioVO buscarRelatorioPercurso(FiltroVO filtroVO) {
        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            Filtro filtro = converter(filtroVO);
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/relatorio/buscarRelatorioPercurso"));
            ObjectMapper mapper = new ObjectMapper();
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            String json = mapper.writeValueAsString(filtro);
            StringEntity input = new StringEntity(json);
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoRelatorioVO.class);
            }
        } catch (Exception e) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarRelatorioPercurso", e);
        }
        return retorno;
    }

    public RetornoRelatorioVO buscarRelatorioVelocidade(FiltroVO filtroVO) {
        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            Filtro filtro = converter(filtroVO);
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/relatorio/buscarRelatorioVelocidade"));
            ObjectMapper mapper = new ObjectMapper();
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            String json = mapper.writeValueAsString(filtro);
            StringEntity input = new StringEntity(json);
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoRelatorioVO.class);
            }
        } catch (Exception e) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarRelatorioVelocidade", e);
        }
        return retorno;
    }


    public RetornoRelatorioVO buscarRelatorioPercursoPortatil(FiltroVO filtroVO) {
        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            Filtro filtro = converter(filtroVO);
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/relatorio/buscarRelatorioPercursoPortatil"));
            ObjectMapper mapper = new ObjectMapper();
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            String json = mapper.writeValueAsString(filtro);
            StringEntity input = new StringEntity(json);
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoRelatorioVO.class);
            }
        } catch (Exception e) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarRelatorioVelocidade", e);
        }
        return retorno;
    }


    public RetornoRelatorioVO buscarRelatorioPresencaDeEquipamentos(FiltroVO filtroVO) {
        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            Filtro filtro = converter(filtroVO);
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/relatorio/buscarRelatorioPresencaDeEquipamentos"));
            ObjectMapper mapper = new ObjectMapper();
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            String json = mapper.writeValueAsString(filtro);
            StringEntity input = new StringEntity(json);
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoRelatorioVO.class);
            }
        } catch (Exception e) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarRelatorioPresencaDeEquipamentos", e);
        }
        return retorno;
    }

    public RetornoRelatorioVO buscarRelatorioTransporteDeEquipamentos(FiltroVO filtroVO) {
        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            Filtro filtro = converter(filtroVO);
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/relatorio/buscarRelatorioTransporteDeEquipamentos"));
            ObjectMapper mapper = new ObjectMapper();
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            String json = mapper.writeValueAsString(filtro);
            StringEntity input = new StringEntity(json);
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoRelatorioVO.class);
            }
        } catch (Exception e) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarRelatorioTransporteDeEquipamentos", e);
        }
        return retorno;
    }

    public RetornoRelatorioVO buscarGraficoProducaoEquipamento(FiltroVO filtroVO) {
        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            Filtro filtro = converter(filtroVO);
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/relatorio/buscarGraficoProducaoEquipamento"));
            ObjectMapper mapper = new ObjectMapper();
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            String json = mapper.writeValueAsString(filtro);
            StringEntity input = new StringEntity(json);
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoRelatorioVO.class);
            }
        } catch (Exception e) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarGraficoProducaoEquipamento", e);
        }
        return retorno;
    }

    public RetornoRelatorioVO buscarSensorBetoneira(FiltroVO filtroVO) {
        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            Filtro filtro = converter(filtroVO);
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/relatorio/buscarSensorBetoneira"));
            ObjectMapper mapper = new ObjectMapper();
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            String json = mapper.writeValueAsString(filtro);
            StringEntity input = new StringEntity(json);
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoRelatorioVO.class);
            }
        } catch (Exception e) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarSensorBetoneira", e);
        }
        return retorno;
    }

    public RetornoRelatorioVO buscarNumeroDeViagem(FiltroVO filtroVO) {
        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            Filtro filtro = converter(filtroVO);
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/relatorio/buscarNumeroDeViagem"));
            ObjectMapper mapper = new ObjectMapper();
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            String json = mapper.writeValueAsString(filtro);
            StringEntity input = new StringEntity(json);
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoRelatorioVO.class);
            }
        } catch (Exception e) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarNumeroDeViagem", e);
        }
        return retorno;
    }


    public RetornoRelatorioVO buscarSensorPorta(FiltroVO filtroVO) {
        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            Filtro filtro = converter(filtroVO);
            if (!filtro.listaDeVeiculos.isEmpty()) {
                filtro.listaDeVeiculos.forEach(v -> {
                    String m = buscarNomeDoMotorista(v.id);
                    Motorista motorista = new Motorista();
                    motorista.nome = m;
                    v.motorista = motorista;
                });
            }
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/relatorio/buscarRelatorioSensor"));
            ObjectMapper mapper = new ObjectMapper();
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            String json = mapper.writeValueAsString(filtro);
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoRelatorioVO.class);
            }
        } catch (Exception e) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarNumeroDeViagem", e);
        }
        return retorno;
    }

    public RetornoRelatorioVO buscarRelatorioReferenciaLocal(FiltroVO filtroVO) {
        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            Filtro filtro = converter(filtroVO);
            if (!filtro.listaDeVeiculos.isEmpty()) {
                filtro.listaDeVeiculos.forEach(v -> {
                    String m = buscarNomeDoMotorista(v.id);
                    Motorista motorista = new Motorista();
                    motorista.nome = m;
                    v.motorista = motorista;
                });
            }
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/relatorio/buscarRelatorioReferenciaLocal"));
            ObjectMapper mapper = new ObjectMapper();
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            String json = mapper.writeValueAsString(filtro);
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoRelatorioVO.class);
            }
        } catch (Exception e) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarRelatorioReferenciaLocal", e);
        }
        return retorno;
    }


    public RetornoRelatorioVO buscarRelatorioReferenciaLocalDetalhes(FiltroVO filtroVO) {
        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            Filtro filtro = converter(filtroVO);
            if (!filtro.listaDeVeiculos.isEmpty()) {
                filtro.listaDeVeiculos.forEach(v -> {
                    String m = buscarNomeDoMotorista(v.id);
                    Motorista motorista = new Motorista();
                    motorista.nome = m;
                    v.motorista = motorista;
                });
            }
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/relatorio/buscarRelatorioReferenciaLocalDetalhes"));
            ObjectMapper mapper = new ObjectMapper();
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            String json = mapper.writeValueAsString(filtro);
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoRelatorioVO.class);
            }
        } catch (Exception e) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarRelatorioReferenciaLocalDetalhes", e);
        }
        return retorno;
    }


    public RetornoRelatorioVO buscarPercursoDetalhado(FiltroVO filtroVO) {
        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            Filtro filtro = converter(filtroVO);
            if (!filtro.listaDeVeiculos.isEmpty()) {
                filtro.listaDeVeiculos.forEach(v -> {
                    String m = buscarNomeDoMotorista(v.id);
                    Motorista motorista = new Motorista();
                    motorista.nome = m;
                    v.motorista = motorista;
                });
            }
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/relatorio/buscarPercursoDetalhado"));
            ObjectMapper mapper = new ObjectMapper();
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            String json = mapper.writeValueAsString(filtro);
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoRelatorioVO.class);
            }
        } catch (Exception e) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarPercursoDetalhado", e);
        }
        return retorno;
    }

    public RetornoRelatorioVO buscarEventoSensoresDetalhado(FiltroVO filtroVO) {
        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            Filtro filtro = converter(filtroVO);
            if (!filtro.listaDeVeiculos.isEmpty()) {
                filtro.listaDeVeiculos.forEach(v -> {
                    String m = buscarNomeDoMotorista(v.id);
                    Motorista motorista = new Motorista();
                    motorista.nome = m;
                    v.motorista = motorista;
                });
            }
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/relatorio/buscarEventoSensoresDetalhado"));
            ObjectMapper mapper = new ObjectMapper();
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            String json = mapper.writeValueAsString(filtro);
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoRelatorioVO.class);
            }
        } catch (Exception e) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarEventoSensoresDetalhado", e);
        }
        return retorno;
    }

    public RetornoRelatorioVO buscarGerarRelatorio(RelatorioConverterVO relatorioConverter) {
        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            Filtro filtro = converter(relatorioConverter.getFiltro());

            RelatorioConverter rc = new RelatorioConverter();
            rc.filtro = filtro;
            rc.listaInformacaoRelatorio = relatorioConverter.getListaInformacaoRelatorio();
            rc.nomeRelatorio = relatorioConverter.getNomeRelatorio();
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/relatorio/gerarRelatorio"));
            ObjectMapper mapper = new ObjectMapper();
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            String json = mapper.writeValueAsString(rc);
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoRelatorioVO.class);
            }
        } catch (Exception e) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarEventoSensoresDetalhado", e);
        }
        return retorno;
    }


    public RetornoRelatorioVO buscarPontosPercorridos(FiltroVO filtroVO) {
        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            Filtro filtro = converter(filtroVO);
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/relatorio/buscarPontosPercorridos"));
            ObjectMapper mapper = new ObjectMapper();
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            String json = mapper.writeValueAsString(filtro);
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoRelatorioVO.class);
            }
        } catch (Exception e) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarPontosPercorridos", e);
        }
        return retorno;
    }

    private String buscarNomeDoMotorista(Integer idVeiculo) {
        String nomeMotorista = "Motorista não encontrado";
        try {
            br.com.cmctransportes.cjm.domain.entities.Motoristas m = new MotoristasVeiculosDAO(null).buscarMotoristaVeiculo(idVeiculo);
            if (Objects.nonNull(m)) {
                nomeMotorista = m.getNome() + " " + m.getSobrenome();
            }
        } catch (Exception e) {

        }
        return nomeMotorista;
    }

    private Filtro converter(FiltroVO filtroVO) {
        Filtro filtro = new Filtro();
        filtro.dataInicial = filtroVO.getDataInicial();
        filtro.dataFinal = filtroVO.getDataFinal();
        filtro.velocidade = filtroVO.getVelocidade();
        filtro.ignicaoAtiva = filtroVO.getIgnicaoAtiva();
        filtro.buscarPelaVelocidadeMaximaDoVeiculo = filtroVO.getBuscarPelaVelocidadeMaximaDoVeiculo();
        if (filtroVO.getListaDeVeiculos() != null) {
            List<VeiculoRel> l = new ArrayList<>();
            for (VeiculoVO vc : filtroVO.getListaDeVeiculos()) {
                VeiculoRel veiculoRel = new VeiculoRel();
                veiculoRel.id = vc.getId();
                veiculoRel.placa = vc.getPlaca();
                l.add(veiculoRel);
            }
            filtro.listaDeVeiculos = l;
        }
        if (filtroVO.getVeiculo() != null) {
            VeiculoRel veiculoRel = new VeiculoRel();
            veiculoRel.id = filtroVO.getVeiculo().getId();
            veiculoRel.placa = filtroVO.getVeiculo().getPlaca();
            filtro.veiculo = veiculoRel;
        }

        if (filtroVO.getEquipamentoPortatil() != null) {
            EquipamentoPortatil equipamentoPortatil = new EquipamentoPortatil();
            equipamentoPortatil.apelido = filtroVO.getEquipamentoPortatil().getApelido();
            equipamentoPortatil.id = filtroVO.getEquipamentoPortatil().getId();
            filtro.equipamentoPortatil = equipamentoPortatil;
        }

        if (filtroVO.getTipoArquivo() != null && !filtroVO.getTipoArquivo().isEmpty()) {
            filtro.tipoArquivo = filtroVO.getTipoArquivo();
        } else {
            filtro.tipoArquivo = "PDF";
        }

        if (Objects.nonNull(filtroVO.getTipoDeCerca())) {
            filtro.tipoDeCerca = filtroVO.getTipoDeCerca();
        } else {
            filtro.tipoDeCerca = -1;
        }

        if (Objects.nonNull(filtroVO.getHorasDiariaManutencao())) {
            filtro.horasDiariaManutencao = filtroVO.getHorasDiariaManutencao();
        } else {
            filtro.horasDiariaManutencao = -1;
        }

        if (Objects.nonNull(filtroVO.getHorasDiariaProgramada())) {
            filtro.horasDiariaProgramada = filtroVO.getHorasDiariaProgramada();
        } else {
            filtro.horasDiariaProgramada = -1;
        }

        if (Objects.nonNull(filtroVO.getIdCliente())) {
            filtro.idCliente = filtroVO.getIdCliente();
        }

        if (Objects.nonNull(filtroVO.getTipoSensor())) {
            filtro.tipoSensor = filtroVO.getTipoSensor();
        }

        if (Objects.nonNull(filtroVO.getParadaMaxima())) {
            filtro.paradaMaxima = filtroVO.getParadaMaxima();
        }

        if (Objects.nonNull(filtroVO.getVelocidadeMinima())) {
            filtro.velocidadeMinima = filtroVO.getVelocidadeMinima();
        }

        if (Objects.nonNull(filtroVO.getTipoEventoSensor())) {
            filtro.tipoEventoSensor = filtroVO.getTipoEventoSensor();
        }

        if (Objects.nonNull(filtroVO.getTempoParado())) {
            filtro.tempoParado = filtroVO.getTempoParado();
        }
        return filtro;
    }


    private String encode(String sourceFile, boolean isChunked) throws Exception {
        byte[] base64EncodedData = Base64.encodeBase64(loadFileAsBytesArray(sourceFile));
        return new String(base64EncodedData);
    }

    private byte[] loadFileAsBytesArray(String urlRelatorio) throws Exception {
        URL url = new URL(urlRelatorio);
        BufferedInputStream reader = new BufferedInputStream(url.openStream());
        byte[] bytes = new byte[reader.available()];
        reader.read(bytes, 0, reader.available());
        reader.close();
        return bytes;
    }
}

class VeiculoRel {
    Integer id;
    String placa;
    Motorista motorista;
}

class EquipamentoPortatil {
    Integer id;
    String apelido;
}

class Motorista {
    String nome = "";
}

class Filtro {
    Date dataInicial;
    Date dataFinal;
    Double velocidade;
    Boolean ignicaoAtiva;
    Boolean buscarPelaVelocidadeMaximaDoVeiculo;
    VeiculoRel veiculo;
    EquipamentoPortatil equipamentoPortatil;
    List<VeiculoRel> listaDeVeiculos;
    String tipoArquivo;
    Integer tipoDeCerca;
    Integer horasDiariaManutencao;
    Integer horasDiariaProgramada;
    Integer idCliente;
    Integer tipoSensor;
    Integer paradaMaxima;
    Double velocidadeMinima;
    /**
     * 1 = entrada de cerca
     * 2 = saida de cerca
     * 3 = sensor porta motorista
     * 4 = sensor porta bau
     */
    Integer tipoEventoSensor;
    Integer tempoParado;
}


class RelatorioConverter {
    Filtro filtro;
    String nomeRelatorio;
    List<InformacaoRelatorioVO> listaInformacaoRelatorio;
}