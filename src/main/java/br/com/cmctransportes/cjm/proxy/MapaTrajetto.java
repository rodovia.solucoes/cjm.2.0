package br.com.cmctransportes.cjm.proxy;

import br.com.cmctransportes.cjm.constants.Constantes;
import br.com.cmctransportes.cjm.domain.entities.Enderecos;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoMapaVO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.proxy.modelo.Endereco;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;


public class MapaTrajetto {

    private static MapaTrajetto mapaTrajetto;

    private MapaTrajetto() {

    }

    public static MapaTrajetto getInstance() {
        try {
            synchronized (MapaTrajetto.class) {
                if (mapaTrajetto == null) {
                    mapaTrajetto = new MapaTrajetto();
                }
            }
        } catch (Exception e) {
            LogSistema.logError("MapaTrajetto", e);
        }
        return mapaTrajetto;
    }


    public RetornoMapaVO buscarLatLngPeloEndereco(Enderecos enderecos) {
        RetornoMapaVO retorno = new RetornoMapaVO();
        try {

            Endereco endereco = convertTO(enderecos);
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/mapa/buscarLatLngPeloEndereco"));

            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(endereco);
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);


            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoMapaVO.class);
            }
        } catch (Exception e) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarLatLngPeloEndereco", e);
        }
        return retorno;
    }

    private Endereco convertTO(Enderecos enderecos) {
        Endereco endereco = new Endereco();
        endereco.setNomeRua(enderecos.getLogradouro());
        endereco.setNumero(enderecos.getNumero());
        endereco.setBairro(endereco.getBairro());
        endereco.setCidade(enderecos.getCidade().getNome());
        endereco.setUf(enderecos.getCidade().getUfCodIbge().getNome());
        return endereco;
    }

}
