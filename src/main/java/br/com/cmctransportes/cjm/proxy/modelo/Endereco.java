package br.com.cmctransportes.cjm.proxy.modelo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class Endereco implements Serializable {
    private String nomeRua;
    private String numero;
    private String bairro;
    private String cidade;
    private String uf;
}
