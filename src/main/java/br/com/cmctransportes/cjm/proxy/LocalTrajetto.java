package br.com.cmctransportes.cjm.proxy;

import br.com.cmctransportes.cjm.constants.Constantes;
import br.com.cmctransportes.cjm.domain.entities.vo.CoordenadaVO;
import br.com.cmctransportes.cjm.domain.entities.vo.EmpresaVO;
import br.com.cmctransportes.cjm.domain.entities.vo.LocalVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoLocalVO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.proxy.modelo.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class LocalTrajetto {
    private static LocalTrajetto localTrajetto;

    private LocalTrajetto(){

    }

    public static LocalTrajetto getInstance(){
        try {
            synchronized (EquipamentoTrajetto.class) {
                if (localTrajetto == null) {
                    localTrajetto = new LocalTrajetto();
                }
            }
        } catch (Exception e) {
            LogSistema.logError("LocalTrajetto", e);
        }
        return localTrajetto;
    }

    public RetornoLocalVO cadastrarLocal(LocalVO localVO){
        RetornoLocalVO retorno = new RetornoLocalVO();
        try {
            Local local = convertTO(localVO);
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/local/cadastrarLocal"));
            ObjectMapper mapper = new ObjectMapper();
            String json =  mapper.writeValueAsString(local);
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoLocalVO.class);
            }
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("cadastrarLocal", e);
        }
        return retorno;
    }


    public RetornoLocalVO excluirLocal(LocalVO localVO){
        RetornoLocalVO retorno = new RetornoLocalVO();
        try {
            Local local = convertTO(localVO);
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/local/excluirLocal"));
            ObjectMapper mapper = new ObjectMapper();
            String json =  mapper.writeValueAsString(local);
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoLocalVO.class);
            }
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("excluirLocal", e);
        }
        return retorno;
    }

    public RetornoLocalVO editarLocal(LocalVO localVO){
        RetornoLocalVO retorno = new RetornoLocalVO();
        try {
            Local local = convertTO(localVO);
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/local/editarLocal"));
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(local);
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoLocalVO.class);
            }
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("editarLocal", e);
        }
        return retorno;
    }

    public RetornoLocalVO cadastrarLocalPostoAbastecimento(LocalVO localVO){
        RetornoLocalVO retorno = new RetornoLocalVO();
        try {
            Local local = convertTO(localVO);
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/local/cadastrarLocalPostoAbastecimento"));
            ObjectMapper mapper = new ObjectMapper();
            String json =  mapper.writeValueAsString(local);
            StringEntity input = new StringEntity(json,"UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoLocalVO.class);
            }
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("cadastrarLocalPostoAbastecimento", e);
        }
        return retorno;
    }


    public RetornoLocalVO cadastrarLocalOficina(LocalVO localVO){
        RetornoLocalVO retorno = new RetornoLocalVO();
        try {
            Local local = convertTO(localVO);
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/local/cadastrarLocalOficina"));
            ObjectMapper mapper = new ObjectMapper();
            String json =  mapper.writeValueAsString(local);
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoLocalVO.class);
            }
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("cadastrarLocalOficina", e);
        }
        return retorno;
    }


    public RetornoLocalVO buscarListaLocal(EmpresaVO empresaVO){
        RetornoLocalVO retorno = new RetornoLocalVO();
        try {

            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/local/buscarListaLocal"));
            ObjectMapper mapper = new ObjectMapper();
            String json =  mapper.writeValueAsString(new Cliente(empresaVO.getIdClienteTrajetto()));
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoLocalVO.class);
                ajustarCliente(retorno.getListaDeLocal());
            }
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarListaLocal", e);
        }
        return retorno;
    }

    public RetornoLocalVO listaDeLocalPostoOficina(EmpresaVO empresaVO){
        RetornoLocalVO retorno = new RetornoLocalVO();
        try {

            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/local/listaDeLocalPostoOficina"));
            ObjectMapper mapper = new ObjectMapper();
            String json =  mapper.writeValueAsString(new Cliente(empresaVO.getIdClienteTrajetto()));
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoLocalVO.class);
                ajustarCliente(retorno.getListaDeLocal());
            }
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("listaDeLocalPostoOficina", e);
        }
        return retorno;
    }

    public RetornoLocalVO listaDeLocalPostoAbastecimento(EmpresaVO empresaVO){
        RetornoLocalVO retorno = new RetornoLocalVO();
        try {
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/local/listaDeLocalPostoAbastecimento"));
            ObjectMapper mapper = new ObjectMapper();
            String json =  mapper.writeValueAsString(new Cliente(empresaVO.getIdClienteTrajetto()));
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoLocalVO.class);
                ajustarCliente(retorno.getListaDeLocal());
            }
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("listaDeLocalPostoAbastecimento", e);
        }
        return retorno;
    }

    public RetornoLocalVO buscarListaLocalExibirMapa(EmpresaVO empresaVO){
        RetornoLocalVO retorno = new RetornoLocalVO();
        try {
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/local/buscarListaLocalExibirMapa"));
            ObjectMapper mapper = new ObjectMapper();
            String json =  mapper.writeValueAsString(new Cliente(empresaVO.getIdClienteTrajetto()));
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoLocalVO.class);
                ajustarCliente(retorno.getListaDeLocal());
            }
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarListaLocalExibirMapa", e);
        }
        return retorno;
    }

    public RetornoLocalVO buscarTodosOsLocais(EmpresaVO empresaVO){
        RetornoLocalVO retorno = new RetornoLocalVO();
        try {
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/local/buscarTodosOsLocais"));
            ObjectMapper mapper = new ObjectMapper();
            String json =  mapper.writeValueAsString(new Cliente(empresaVO.getIdClienteTrajetto()));
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoLocalVO.class);
                ajustarCliente(retorno.getListaDeLocal());
            }
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarTodosOsLocais", e);
        }
        return retorno;
    }

    public RetornoLocalVO buscarLocalPeloCodigo(Integer id) {
        RetornoLocalVO retorno = new RetornoLocalVO();
        try {
            HttpGet get = new HttpGet(Constantes.URL_SERVICO_TRAJETTO.concat("/local/buscarLocal/"+id.toString()));
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(get);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                ObjectMapper mapper = new ObjectMapper();
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());

                retorno = mapper.treeToValue(jsonNode, RetornoLocalVO.class);
                if(retorno.getLocal() != null) {
                    retorno.getLocal().getCliente().setIdClienteTrajetto(retorno.getLocal().getCliente().getId());
                    retorno.getLocal().getCliente().setId(null);
                }
            }
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarEquipamentoPeloCodigo", e);
        }
        return retorno;
    }


    private Local convertTO(LocalVO localVO){
        Local local = new Local();
        local.setId(localVO.getId());
        local.setNome(localVO.getNome());
        local.setLocalPai(localVO.getLocalPai());
        local.setTemFilho(localVO.getTemFilho());
        local.setAtivo(localVO.getAtivo());
        local.setMostrarNoMapaPrincipal(localVO.getMostrarNoMapaPrincipal());
        local.setMostrarNomeNoMapa(localVO.getMostrarNomeNoMapa());
        local.setNotificaEvento(localVO.getNotificaEvento());
        local.setCodigoUnico(localVO.getCodigoUnico());
        local.setFuncao(localVO.getFuncao());
        local.setEndereco(localVO.getEndereco());
        local.setBairro(localVO.getBairro());
        local.setComplemento(localVO.getComplemento());
        local.setCidade(localVO.getCidade());
        local.setUf(localVO.getUf());
        local.setContato(localVO.getContato());
        local.setTelefone(localVO.getTelefone());
        local.setTipo(localVO.getTipo());
        local.setPermiteDescanso(localVO.isPermiteDescanso());
        if(Objects.nonNull(localVO.getCliente())) {
            local.setCliente(new Cliente(localVO.getCliente().getIdClienteTrajetto()));
        }
        if(Objects.nonNull(localVO.getListaDeCoordenadas())){
            List<Coordenada> listaCoordenada = new ArrayList<>();
            for(CoordenadaVO coordenadaVO : localVO.getListaDeCoordenadas()){
                Coordenada coordenada = new Coordenada();
                coordenada.setId(coordenadaVO.getId());
                coordenada.setIsRaio(coordenadaVO.getIsRaio());
                coordenada.setLatitude(coordenadaVO.getLatitude());
                coordenada.setLongitude(coordenadaVO.getLongitude());
                coordenada.setSequencia(coordenadaVO.getSequencia());
                listaCoordenada.add(coordenada);
            }
            local.setListaDeCoordenadas(listaCoordenada);
        }
        return local;
    }

    private void ajustarCliente(List<LocalVO> listaDeLocais){
        for(LocalVO local : listaDeLocais){
            local.getCliente().setIdClienteTrajetto(local.getCliente().getId());
            local.getCliente().setId(null);
        }
    }

}
