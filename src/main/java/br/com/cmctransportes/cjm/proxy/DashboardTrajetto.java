package br.com.cmctransportes.cjm.proxy;

import br.com.cmctransportes.cjm.constants.Constantes;
import br.com.cmctransportes.cjm.domain.entities.vo.*;
import br.com.cmctransportes.cjm.logger.LogSistema;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

public class DashboardTrajetto {

    private static DashboardTrajetto dashboardTrajetto;

    private DashboardTrajetto() {

    }

    public static DashboardTrajetto getInstance() {
        try {
            synchronized (DashboardTrajetto.class) {
                if (dashboardTrajetto == null) {
                    dashboardTrajetto = new DashboardTrajetto();
                }
            }
        } catch (Exception e) {
            LogSistema.logError("DashboardTrajetto", e);
        }
        return dashboardTrajetto;
    }


    public RetornoMapaVO buscarListaDeVeiculoPorEmpresa(EmpresaVO empresaVO, Integer idUnidade) {
        RetornoMapaVO retorno = new RetornoMapaVO();
        try {
            HttpGet get = new HttpGet(Constantes.URL_SERVICO_TRAJETTO.concat("/areadetrabalho/buscarListaDeVeiculoPorEmpresa/" + empresaVO.getIdClienteTrajetto().toString() + "/"+idUnidade));
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(get);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                ObjectMapper mapper = new ObjectMapper();
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoMapaVO.class);
            }
        } catch (Exception e) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarListaDeVeiculoPorEmpresa", e);
        }
        return retorno;
    }

    public RetornoDashboardVO buscarDadosDoVeiculo(VeiculoVO veiculoVO) {
        RetornoDashboardVO retorno = new RetornoDashboardVO();
        try {
            HttpGet get = new HttpGet(Constantes.URL_SERVICO_TRAJETTO.concat("/areadetrabalho/buscarDadosDoVeiculo/" + veiculoVO.getId().toString()));
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(get);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                ObjectMapper mapper = new ObjectMapper();
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoDashboardVO.class);
            }
        } catch (Exception e) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarDadosDoVeiculo", e);
        }
        return retorno;
    }


    public RetornoDashboardVO buscarDadosDoEquipamentoPortatil(EquipamentoPortatilVO  portatilVO) {
        RetornoDashboardVO retorno = new RetornoDashboardVO();
        try {
            HttpGet get = new HttpGet(Constantes.URL_SERVICO_TRAJETTO.concat("/areadetrabalho/buscarDetalhesDoEquipamentoPortatil/" + portatilVO.getId().toString()));
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(get);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                ObjectMapper mapper = new ObjectMapper();
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoDashboardVO.class);
            }
        } catch (Exception e) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarDadosDoEquipamentoPortatil", e);
        }
        return retorno;
    }

    public RetornoAlertaInconformeVO buscarInconformidadesVeiculos(Integer idCliente) {
        RetornoAlertaInconformeVO retorno = new RetornoAlertaInconformeVO();
        try {
            HttpGet get = new HttpGet(Constantes.URL_SERVICO_TRAJETTO.concat("/areadetrabalho/buscarInconformidadesVeiculos/" + idCliente.toString()));
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(get);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                ObjectMapper mapper = new ObjectMapper();
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoAlertaInconformeVO.class);
            }
        } catch (Exception e) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarInconformidadesVeiculos", e);
        }
        return retorno;
    }



}
