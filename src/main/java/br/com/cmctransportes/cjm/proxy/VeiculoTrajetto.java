package br.com.cmctransportes.cjm.proxy;

import br.com.cmctransportes.cjm.constants.Constantes;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoVeiculoVO;
import br.com.cmctransportes.cjm.domain.entities.vo.VeiculoVO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.proxy.modelo.Cliente;
import br.com.cmctransportes.cjm.proxy.modelo.Equipamento;
import br.com.cmctransportes.cjm.proxy.modelo.Modelo;
import br.com.cmctransportes.cjm.proxy.modelo.Veiculo;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

public class VeiculoTrajetto {
    private static VeiculoTrajetto veiculoTrajetto;

    private VeiculoTrajetto(){}

    public static VeiculoTrajetto getInstance(){
        try {
            synchronized (VeiculoTrajetto.class) {
                if (veiculoTrajetto == null) {
                    veiculoTrajetto = new VeiculoTrajetto();
                }
            }
        } catch (Exception e) {
            LogSistema.logError("veiculoTrajetto", e);
        }
        return veiculoTrajetto;
    }

    public RetornoVeiculoVO cadastrarVeiculo(VeiculoVO veiculoVO){
        RetornoVeiculoVO retorno = new RetornoVeiculoVO();
        try {
            Veiculo veiculo = convertTO(veiculoVO);
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/veiculo/cadastrarVeiculo"));
            ObjectMapper mapper = new ObjectMapper();
            String json =  mapper.writeValueAsString(veiculo);
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoVeiculoVO.class);
            }
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("cadastrarVeiculo", e);
        }
        return retorno;
    }


    public RetornoVeiculoVO editarVeiculo(VeiculoVO veiculoVO){
        RetornoVeiculoVO retorno = new RetornoVeiculoVO();
        try {
            Veiculo veiculo = convertTO(veiculoVO);
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/veiculo/editarVeiculo"));
            ObjectMapper mapper = new ObjectMapper();
            String json =  mapper.writeValueAsString(veiculo);
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoVeiculoVO.class);
            }

        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("editarVeiculo", e);
        }
        return retorno;
    }

    public RetornoVeiculoVO listarVeiculo(Integer id, Integer unidade) {
        RetornoVeiculoVO retorno = new RetornoVeiculoVO();
        try {
            HttpGet get = new HttpGet(Constantes.URL_SERVICO_TRAJETTO.concat("/veiculo/listarVeiculo/"+id.toString()+"/"+unidade.toString()));
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(get);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                ObjectMapper mapper = new ObjectMapper();
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoVeiculoVO.class);
            }
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarModelo", e);
        }
        return retorno;
    }


    public List<VeiculoVO> listarVeiculos(Integer id, Integer unidade) {
        RetornoVeiculoVO retorno = listarVeiculo(id, unidade);
        if(retorno.getCodigoRetorno().equals(61)){
            return retorno.getListaDeVeiculos();
        }
        return null;
    }


    public RetornoVeiculoVO buscarVeiculoPeloId(Integer id) {
        RetornoVeiculoVO retorno = new RetornoVeiculoVO();
        try {
            HttpGet get = new HttpGet(Constantes.URL_SERVICO_TRAJETTO.concat("/veiculo/buscarVeiculoPeloId/"+id.toString()));
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(get);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                ObjectMapper mapper = new ObjectMapper();
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoVeiculoVO.class);
            }
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarVeiculoPeloId", e);
        }
        return retorno;
    }


    public RetornoVeiculoVO buscarListaDeVeiculoPeloCliente(Integer id) {
        RetornoVeiculoVO retorno = new RetornoVeiculoVO();
        try {
            HttpGet get = new HttpGet(Constantes.URL_SERVICO_TRAJETTO.concat("/veiculo/buscarListaDeVeiculoPeloCliente/"+id.toString()));
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(get);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                ObjectMapper mapper = new ObjectMapper();
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoVeiculoVO.class);
            }
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarListaDeVeiculoPeloCliente", e);
        }
        return retorno;
    }

    public RetornoVeiculoVO buscarListaDeVeiculoMinimaPorEmpresa(Integer id) {
        RetornoVeiculoVO retorno = new RetornoVeiculoVO();
        try {
            HttpGet get = new HttpGet(Constantes.URL_SERVICO_TRAJETTO.concat("/veiculo/buscarListaDeVeiculoMinimaPorEmpresa/"+id.toString()));
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(get);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                ObjectMapper mapper = new ObjectMapper();
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoVeiculoVO.class);
            }
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarListaDeVeiculoMinimaPorEmpresa", e);
        }
        return retorno;
    }

    public RetornoVeiculoVO buscarListaDeVeiculoMinimaPorEmpresaAtivos(Integer id) {
        RetornoVeiculoVO retorno = new RetornoVeiculoVO();
        try {
            HttpGet get = new HttpGet(Constantes.URL_SERVICO_TRAJETTO.concat("/veiculo/buscarListaDeVeiculoMinimaPorEmpresaAtivos/"+id.toString()));
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(get);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                ObjectMapper mapper = new ObjectMapper();
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoVeiculoVO.class);
            }
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarListaDeVeiculoMinimaPorEmpresaAtivos", e);
        }
        return retorno;
    }



    private Veiculo convertTO(VeiculoVO veiculoVO){
        Veiculo veiculo = new Veiculo();
        veiculo.setId(veiculoVO.getId());
        veiculo.setAnoFabricacao(veiculoVO.getAnoFabricacao());
        veiculo.setAnoModelo(veiculoVO.getAnoModelo());
        veiculo.setChassi(veiculoVO.getChassi());
        veiculo.setCor(veiculoVO.getCor());
        veiculo.setDataCadastro(veiculoVO.getDataCadastro());
        veiculo.setObservacao(veiculoVO.getObservacao());
        veiculo.setPlaca(veiculoVO.getPlaca());
        veiculo.setVelocidadeMaxima(veiculoVO.getVelocidadeMaxima());
        veiculo.setVelocidadeMaximaChuva(veiculoVO.getVelocidadeMaximaChuva());
        veiculo.setVelocidadeMaximaDesacelaracao(veiculoVO.getVelocidadeMaximaDesacelaracao());
        veiculo.setVelocidadeMaximaCurva(veiculoVO.getVelocidadeMaximaCurva());
        veiculo.setRenavam(veiculoVO.getRenavam());
        veiculo.setStatus(veiculoVO.getStatus());
        veiculo.setTipo(veiculoVO.getTipo());
        veiculo.setCidade(veiculoVO.getCidade());
        veiculo.setTelefoneProprietario(veiculoVO.getTelefoneProprietario());
        veiculo.setUsaEntradaDigitalUm(veiculoVO.getUsaEntradaDigitalUm());
        veiculo.setExcessoVelocidade(veiculoVO.getExcessoVelocidade());
        veiculo.setAntiJummer(veiculoVO.getAntiJummer());
        veiculo.setBateriaCarroBaixa(veiculoVO.getBateriaCarroBaixa());
        veiculo.setFaltaEnergiaPrincipal(veiculoVO.getFaltaEnergiaPrincipal());
        veiculo.setPanico(veiculoVO.getPanico());
        veiculo.setQuantidadeDiasSemTrasmissao(veiculoVO.getQuantidadeDiasSemTrasmissao());
        veiculo.setRoubo(veiculoVO.getRoubo());
        veiculo.setSemComunicacao(veiculoVO.getSemComunicacao());
        veiculo.setVinteQuatroHorasComunicacao(veiculoVO.getVinteQuatroHorasComunicacao());
        veiculo.setAtivaRotaNoMapa(veiculoVO.getAtivaRotaNoMapa());
        veiculo.setAtivaValidacaoDeCerca(veiculoVO.getAtivaValidacaoDeCerca());
        veiculo.setTrocaDeHorimetro(veiculoVO.getTrocaDeHorimetro());
        veiculo.setRpmModoEconomicoMinimo(veiculoVO.getRpmModoEconomicoMinimo());
        veiculo.setRpmModoEconomicoMaximo(veiculoVO.getRpmModoEconomicoMaximo());
        veiculo.setRpmMaximo(veiculoVO.getRpmMaximo());
        veiculo.setGeraEnderecoAutomatico(veiculoVO.getGeraEnderecoAutomatico());
        veiculo.setValidarIbutton(veiculoVO.getValidarIbutton());
        veiculo.setCargaMaxima(veiculoVO.getCargaMaxima());
        veiculo.setCombustivel(veiculoVO.getCombustivel());
        veiculo.setGerarControleViagemAutomatico(veiculoVO.getGerarControleViagemAutomatico());
        veiculo.setDistanciaMinimaViagemAutomatica(veiculoVO.getDistanciaMinimaViagemAutomatica());
        veiculo.setTipoEntradaDigital(veiculoVO.getTipoEntradaDigital());
        veiculo.setAtivarModuloTemperaturaCamaraFria(veiculoVO.getAtivarModuloTemperaturaCamaraFria());
        veiculo.setEnviarNotificacaoTemperaturaAplicativo(veiculoVO.getEnviarNotificacaoTemperaturaAplicativo());
        veiculo.setTemperaturaMinima(veiculoVO.getTemperaturaMinima());
        veiculo.setTemperaturaMaxima(veiculoVO.getTemperaturaMaxima());

        if(Objects.nonNull(veiculoVO.getTiposDeVeiculo())) {
            veiculo.setTipoDeVeiculo(veiculoVO.getTiposDeVeiculo().getId());//Representa o tipo de veiculo que vem do sistema rodovia
        }
        Modelo modelo = ModeloTrajetto.getInstance().convertTO(veiculoVO.getModelo());
        veiculo.setModelo(modelo);
        if(Objects.nonNull(veiculoVO.getEquipamento())) {
            Equipamento equipamento = EquipamentoTrajetto.getInstance().convertTO(veiculoVO.getEquipamento());
            veiculo.setEquipamento(equipamento);
        }

        if(veiculoVO.getEquipamento()!= null) {
            Equipamento equipamento = EquipamentoTrajetto.getInstance().convertTO(veiculoVO.getEquipamento());
            veiculo.setEquipamento(equipamento);
        }

        if(veiculoVO.getEquipamentoDois()!= null) {
            Equipamento equipamento = EquipamentoTrajetto.getInstance().convertTO(veiculoVO.getEquipamentoDois());
            veiculo.setEquipamentoDois(equipamento);
        }

        veiculo.setCliente(new Cliente(veiculoVO.getEmpresaVO().getIdClienteTrajetto()));
        veiculo.setUnidadeId(veiculoVO.getUnidadeId());
        if(Objects.nonNull(veiculoVO.getFaturamentoFormatada())){
            String s = veiculoVO.getFaturamentoFormatada().replaceAll("[^\\d,]", "").replaceAll(",", ".");
            BigDecimal bigDecimal = new BigDecimal(s);
            veiculo.setFaturamento(bigDecimal);

        }
        if(Objects.nonNull(veiculoVO.getPorcentagemFaturamento())){
            veiculo.setPorcentagemFaturamento(veiculoVO.getPorcentagemFaturamento());
        }
        veiculo.setCercaMovel(veiculoVO.getCercaMovel());
        veiculo.setRaioCercaMovel(veiculoVO.getRaioCercaMovel());
        veiculo.setValidaCercaMovel(veiculoVO.getValidaCercaMovel());
        if(Objects.nonNull(veiculoVO.getUsaEntradaDigitalDois())){
            veiculo.setUsaEntradaDigitalDois(veiculoVO.getUsaEntradaDigitalDois());
        }

        if(Objects.nonNull(veiculoVO.getUsaEntradaDigitalTres())){
            veiculo.setUsaEntradaDigitalTres(veiculoVO.getUsaEntradaDigitalTres());
        }

        if(Objects.nonNull(veiculoVO.getUsaEntradaDigitalQuatro())){
            veiculo.setUsaEntradaDigitalQuatro(veiculoVO.getUsaEntradaDigitalQuatro());
        }

        if(Objects.nonNull(veiculoVO.getTipoEntradaDigitalDois())){
            veiculo.setTipoEntradaDigitalDois(veiculoVO.getTipoEntradaDigitalDois());
        }

        if(Objects.nonNull(veiculoVO.getTipoEntradaDigitalTres())){
            veiculo.setTipoEntradaDigitalTres(veiculoVO.getTipoEntradaDigitalTres());
        }

        if(Objects.nonNull(veiculoVO.getTipoEntradaDigitalQuatro())){
            veiculo.setTipoEntradaDigitalQuatro(veiculoVO.getTipoEntradaDigitalQuatro());
        }

        if(Objects.nonNull(veiculoVO.getBloquearVeiculo())){
            veiculo.setBloquearVeiculo(veiculoVO.getBloquearVeiculo());
        }else{
            veiculo.setBloquearVeiculo(false);
        }

        if(Objects.nonNull(veiculoVO.getValidarRota())){
            veiculo.setValidarRota(veiculoVO.getValidarRota());
        }else{
            veiculo.setValidarRota(false);
        }


        if(Objects.nonNull(veiculoVO.getRpmInicioFaixaAzul())){
            veiculo.setRpmInicioFaixaAzul(veiculoVO.getRpmInicioFaixaAzul());
        }

        if(Objects.nonNull(veiculoVO.getRpmFimFaixaAzul())){
            veiculo.setRpmFimFaixaAzul(veiculoVO.getRpmFimFaixaAzul());
        }

        if(Objects.nonNull(veiculoVO.getRpmInicioFaixaEconomica())){
            veiculo.setRpmInicioFaixaEconomica(veiculoVO.getRpmInicioFaixaEconomica());
        }

        if(Objects.nonNull(veiculoVO.getRpmFimFaixaEconomica())){
            veiculo.setRpmFimFaixaEconomica(veiculoVO.getRpmFimFaixaEconomica());
        }

        if(Objects.nonNull(veiculoVO.getRpmInicioFaixaVerde())){
            veiculo.setRpmInicioFaixaVerde(veiculoVO.getRpmInicioFaixaVerde());
        }

        if(Objects.nonNull(veiculoVO.getRpmFimFaixaVerde())){
            veiculo.setRpmFimFaixaVerde(veiculoVO.getRpmFimFaixaVerde());
        }

        if(Objects.nonNull(veiculoVO.getRpmInicioFaixaAmarela())){
            veiculo.setRpmInicioFaixaAmarela(veiculoVO.getRpmInicioFaixaAmarela());
        }

        if(Objects.nonNull(veiculoVO.getRpmFimFaixaAmarela())){
            veiculo.setRpmFimFaixaAmarela(veiculoVO.getRpmFimFaixaAmarela());
        }

        if(Objects.nonNull(veiculoVO.getRpmInicioMarchaLenta())){
            veiculo.setRpmInicioMarchaLenta(veiculoVO.getRpmInicioMarchaLenta());
        }

        if(Objects.nonNull(veiculoVO.getRpmFimMarchaLenta())){
            veiculo.setRpmFimMarchaLenta(veiculoVO.getRpmFimMarchaLenta());
        }

        if(Objects.nonNull(veiculoVO.getMostrarViam())){
            veiculo.setMostrarViam(veiculoVO.getMostrarViam());
        }

        if(Objects.nonNull(veiculoVO.getUltimoHodometro())){
            veiculo.setUltimoHodometro(String.valueOf(veiculoVO.getUltimoHodometro()));
        }

        return veiculo;
    }

}
