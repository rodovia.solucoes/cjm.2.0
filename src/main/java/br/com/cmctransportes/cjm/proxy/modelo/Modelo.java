package br.com.cmctransportes.cjm.proxy.modelo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class Modelo implements Serializable {
    private Integer id;
    private String fabricante;
    private String marca;
    private String tipo;
    private Boolean status;
    private Boolean portatil;
}
