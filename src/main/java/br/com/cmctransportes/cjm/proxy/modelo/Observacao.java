package br.com.cmctransportes.cjm.proxy.modelo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class Observacao implements Serializable {
    private Veiculo veiculo;
    private EquipamentoPortatil equipamentoPortatil;
    private String texto;
}
