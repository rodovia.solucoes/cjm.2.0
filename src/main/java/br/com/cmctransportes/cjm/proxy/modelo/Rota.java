package br.com.cmctransportes.cjm.proxy.modelo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Rota {
    private Integer id;
    private String nome;
    private Boolean ativo;
    private Local localInicio;
    private Local localFinal;
    private Cliente cliente;
    private Integer idUnidade;

    private List<Coordenada> listaDeCoordenadas;
}
