package br.com.cmctransportes.cjm.proxy;

import br.com.cmctransportes.cjm.constants.Constantes;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoTransmissaoVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoVeiculoVO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.proxy.modelo.Veiculo;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.util.Objects;

public class TransmissaoTrajetto {

    private static TransmissaoTrajetto transmissaoTrajetto;

    private TransmissaoTrajetto(){

    }

    public static TransmissaoTrajetto getInstance(){
        try {
            synchronized (TransmissaoTrajetto.class) {
                if (Objects.isNull(transmissaoTrajetto)) {
                    transmissaoTrajetto = new TransmissaoTrajetto();
                }
            }
        } catch (Exception e) {
            LogSistema.logError("transmissaoTrajetto", e);
        }
        return transmissaoTrajetto;
    }

    public RetornoTransmissaoVO buscarTrajettoUltimas24horas(Integer idVeiculo){
        RetornoTransmissaoVO retorno = new RetornoTransmissaoVO();
        try {
            Veiculo veiculo = new Veiculo();
            veiculo.setId(idVeiculo);
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/transmissao/buscarTrajettoUltimas24horas"));
            ObjectMapper mapper = new ObjectMapper();
            String json =  mapper.writeValueAsString(veiculo);
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoTransmissaoVO.class);
            }
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarTrajettoUltimas24horas", e);
        }
        return retorno;
    }


    public RetornoTransmissaoVO buscarTrajetto(Integer idVeiculo, long dataInicial, long dataFinal){
        RetornoTransmissaoVO retorno = new RetornoTransmissaoVO();
        try {

            HttpGet get = new HttpGet(Constantes.URL_SERVICO_TRAJETTO.concat("/transmissao/buscarTrajettoPorPeriodo/"+idVeiculo+"/"+dataInicial+"/"+dataFinal));
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(get);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                ObjectMapper mapper = new ObjectMapper();
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoTransmissaoVO.class);
            }
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarTrajetto", e);
        }
        return retorno;
    }
}
