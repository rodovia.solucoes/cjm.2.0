package br.com.cmctransportes.cjm.proxy.modelo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class EquipamentoPortatil implements Serializable {
    private Integer id;
}
