package br.com.cmctransportes.cjm.proxy;

import br.com.cmctransportes.cjm.constants.Constantes;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoVeiculoMonitoramentoVO;
import br.com.cmctransportes.cjm.domain.entities.vo.VeiculoMonitoramentoVO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.util.Objects;

public class VeiculoMonitoramentoProxy {

    private static VeiculoMonitoramentoProxy _veiculoMonitoramentoProxy;

    public static VeiculoMonitoramentoProxy getInstance(){
        if(Objects.isNull(_veiculoMonitoramentoProxy)){
            _veiculoMonitoramentoProxy = new VeiculoMonitoramentoProxy();
        }
        return _veiculoMonitoramentoProxy;
    }

    public VeiculoMonitoramentoVO buscarVeiculo(String placa) {
        try {
            HttpGet get = new HttpGet(Constantes.URL_ARCHERHAWK.concat("/veiculomonitoramento/buscar/"+placa));
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(get);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                ObjectMapper mapper = new ObjectMapper();
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                RetornoVeiculoMonitoramentoVO retorno = mapper.treeToValue(jsonNode, RetornoVeiculoMonitoramentoVO.class);
                if(Objects.nonNull(retorno)){
                    if(Objects.nonNull(retorno.getVeiculoMonitoramento())){
                        return retorno.getVeiculoMonitoramento();
                    }
                }
            }
        }catch (Exception e){
            LogSistema.logError("buscarVeiculo", e);
        }
        return null;
    }
}
