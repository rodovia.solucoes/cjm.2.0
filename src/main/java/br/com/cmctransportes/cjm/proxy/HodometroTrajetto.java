package br.com.cmctransportes.cjm.proxy;

import br.com.cmctransportes.cjm.constants.Constantes;
import br.com.cmctransportes.cjm.domain.entities.vo.HodometroVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoHodometroVO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.proxy.modelo.Veiculo;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;


public class HodometroTrajetto {

    private static HodometroTrajetto hodometroTrajetto;

    private HodometroTrajetto(){

    }

    public static HodometroTrajetto getInstance(){
        try {
            synchronized (HodometroTrajetto.class) {
                if (hodometroTrajetto == null) {
                    hodometroTrajetto = new HodometroTrajetto();
                }
            }
        } catch (Exception e) {
            LogSistema.logError("EquipamentoTrajetto", e);
        }
        return hodometroTrajetto;
    }



    public RetornoHodometroVO registrarHodometro(HodometroVO hodometroVO) {
        RetornoHodometroVO retornoVO = new RetornoHodometroVO();
        try {
            Hodometro hodometro = convertTO(hodometroVO);
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/hodometro/registrarHodometro"));
            ObjectMapper mapper = new ObjectMapper();
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            String json =  mapper.writeValueAsString(hodometro);
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retornoVO = mapper.treeToValue(jsonNode, RetornoHodometroVO.class);
            }

        }catch (Exception e){
            retornoVO.setCodigoRetorno(-1);
            retornoVO.setMensagem(e.getMessage());
            LogSistema.logError("registrarHodometro", e);
        }
        return retornoVO;
    }

    public RetornoHodometroVO registrarHodometroSemUltimaTransmissao(HodometroVO hodometroVO) {
        RetornoHodometroVO retornoVO = new RetornoHodometroVO();
        try {
            Hodometro hodometro = convertTO(hodometroVO);
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/hodometro/registrarHodometroSemUltimaTransmissao"));
            ObjectMapper mapper = new ObjectMapper();
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            String json =  mapper.writeValueAsString(hodometro);
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retornoVO = mapper.treeToValue(jsonNode, RetornoHodometroVO.class);
            }

        }catch (Exception e){
            retornoVO.setCodigoRetorno(-1);
            retornoVO.setMensagem(e.getMessage());
            LogSistema.logError("registrarHodometro", e);
        }
        return retornoVO;
    }


    public RetornoHodometroVO buscarListaDeHodometro(Veiculo veiculo) {
        RetornoHodometroVO retornoVO = new RetornoHodometroVO();
        try {
            VeiculoT veiculoT = new VeiculoT();
            veiculoT.id = veiculo.getId();
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/hodometro/buscarListaDeHodometro"));
            ObjectMapper mapper = new ObjectMapper();
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            String json =  mapper.writeValueAsString(veiculoT);
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retornoVO = mapper.treeToValue(jsonNode, RetornoHodometroVO.class);
            }

        }catch (Exception e){
            retornoVO.setCodigoRetorno(-1);
            retornoVO.setMensagem(e.getMessage());
            LogSistema.logError("buscarListaDeHodometro", e);
        }
        return retornoVO;
    }

    private Hodometro convertTO(HodometroVO hodometroVO){
        Hodometro hodometro = new Hodometro();
        VeiculoT veiculoT = new VeiculoT();
        veiculoT.id = hodometroVO.getVeiculo().getId();
        hodometro.veiculo = veiculoT.id;
        hodometro.usuario = hodometroVO.getUsuario();
        hodometro.hodometro = hodometroVO.getHodometro();
        if(Objects.nonNull(hodometroVO.getHodometroRastreador())){
            hodometro.hodometroRastreador = hodometroVO.getHodometroRastreador();
        }
        if(Objects.nonNull(hodometroVO.getDataDoCadastro())){
            hodometro.dataDoCadastro = hodometroVO.getDataDoCadastro();
        }
        if(Objects.nonNull(hodometroVO.getDataUltimaAtualizacao())){
            hodometro.dataUltimaAtualizacao = hodometroVO.getDataUltimaAtualizacao();
        }
        if(Objects.nonNull(hodometroVO.getTipo())){
            hodometro.tipo = hodometroVO.getTipo();
        }
        if(Objects.nonNull(hodometroVO.getNomeDoMotorista())){
            hodometro.nomeDoMotorista = hodometroVO.getNomeDoMotorista();
        }
        if(Objects.nonNull(hodometroVO.getIdDoMotorista())){
            hodometro.idDoMotorista = hodometroVO.getIdDoMotorista();
        }
        if(Objects.nonNull(hodometroVO.getTipoCarregamento())){
            hodometro.tipoCarregamento = hodometroVO.getTipoCarregamento();
        }
        return hodometro;
    }

}

class Hodometro {
    Integer veiculo;
    Integer usuario;
    Double hodometro;
    Double hodometroRastreador;
    Date dataDoCadastro;
    Date dataUltimaAtualizacao;
    String tipo;
    String nomeDoMotorista;
   Integer idDoMotorista;
    Integer tipoCarregamento;
}

class VeiculoT {
    Integer id;
}


