package br.com.cmctransportes.cjm.proxy;

import br.com.cmctransportes.cjm.constants.Constantes;
import br.com.cmctransportes.cjm.domain.entities.vo.*;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.proxy.modelo.Cliente;
import br.com.cmctransportes.cjm.proxy.modelo.EnvioModelo;
import br.com.cmctransportes.cjm.proxy.modelo.Modelo;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

public class ModeloTrajetto {
    private static ModeloTrajetto modeloTrajetto;

    private ModeloTrajetto(){

    }

    public static ModeloTrajetto getInstance(){
        try {
            synchronized (EquipamentoTrajetto.class) {
                if (modeloTrajetto == null) {
                    modeloTrajetto = new ModeloTrajetto();
                }
            }
        } catch (Exception e) {
            LogSistema.logError("modeloTrajetto", e);
        }
        return modeloTrajetto;
    }

    public RetornoModeloVO cadastrarModeloEquipamento(ModeloVO modeloVO){
        RetornoModeloVO retorno = new RetornoModeloVO();
        try {
            Modelo modelo = convertTO(modeloVO);
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/modelo/cadastrarModeloEquipamento"));
            ObjectMapper mapper = new ObjectMapper();

            String json =  mapper.writeValueAsString(new EnvioModelo(new Cliente(1), modelo));
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoModeloVO.class);
            }
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("cadastrarModeloEquipamento", e);
        }
        return retorno;
    }

    public RetornoModeloVO cadastrarModeloVeiculo(ModeloVO modeloVO){
        RetornoModeloVO retorno = new RetornoModeloVO();
        try {
            Modelo modelo = convertTO(modeloVO);
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/modelo/cadastrarModeloVeiculo"));
            ObjectMapper mapper = new ObjectMapper();

            String json =  mapper.writeValueAsString(new EnvioModelo(new Cliente(1), modelo));
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoModeloVO.class);
            }
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("cadastrarModeloVeiculo", e);
        }
        return retorno;
    }


    public RetornoModeloVO editarModelo(ModeloVO modeloVO){
        RetornoModeloVO retorno = new RetornoModeloVO();
        try {
            Modelo modelo = convertTO(modeloVO);
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/modelo/editarModelo"));
            ObjectMapper mapper = new ObjectMapper();

            String json =  mapper.writeValueAsString(modelo);
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoModeloVO.class);
            }
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("editarModelo", e);
        }
        return retorno;
    }


    public RetornoModeloVO buscarListaModeloEquipamento(){
        RetornoModeloVO retorno = new RetornoModeloVO();
        try {
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/modelo/buscarListaModeloEquipamento"));
            ObjectMapper mapper = new ObjectMapper();

            String json =  mapper.writeValueAsString(new Cliente(1));
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoModeloVO.class);
            }
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarListaModeloEquipamento", e);
        }
        return retorno;
    }

    public RetornoModeloVO buscarListaModeloVeiculo(){
        RetornoModeloVO retorno = new RetornoModeloVO();
        try {
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/modelo/buscarListaModeloVeiculo"));
            ObjectMapper mapper = new ObjectMapper();

            String json =  mapper.writeValueAsString(new Cliente(1));
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoModeloVO.class);
            }
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarListaModeloVeiculo", e);
        }
        return retorno;
    }



    public RetornoModeloVO buscarModelo(Integer id) {
        RetornoModeloVO retorno = new RetornoModeloVO();
        try {
            HttpGet get = new HttpGet(Constantes.URL_SERVICO_TRAJETTO.concat("/modelo/buscarModelo/"+id.toString()));
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(get);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                ObjectMapper mapper = new ObjectMapper();
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoModeloVO.class);
            }
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarModelo", e);
        }
        return retorno;
    }

    public RetornoModeloVO buscarModeloPortatil() {
        RetornoModeloVO retorno = new RetornoModeloVO();
        try {
            HttpGet get = new HttpGet(Constantes.URL_SERVICO_TRAJETTO.concat("/modelo/buscarListaModeloPortatil"));
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(get);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                ObjectMapper mapper = new ObjectMapper();
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoModeloVO.class);
            }
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarModeloPortatil", e);
        }
        return retorno;
    }

    public Modelo convertTO(ModeloVO modeloVO){
        Modelo modelo = new Modelo();
        modelo.setFabricante(modeloVO.getFabricante());
        modelo.setId(modeloVO.getId());
        modelo.setMarca(modeloVO.getMarca());
        modelo.setStatus(modeloVO.getStatus());
        modelo.setTipo(modeloVO.getTipo());
        modelo.setPortatil(modeloVO.getPortatil());
        return modelo;
    }
}
