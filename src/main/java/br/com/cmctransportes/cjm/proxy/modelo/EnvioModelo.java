package br.com.cmctransportes.cjm.proxy.modelo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class EnvioModelo implements Serializable {
    private static final long serialVersionUID = 1L;
    private Cliente cliente;
    private Modelo modelo;

    public EnvioModelo() {
    }

    public EnvioModelo(Cliente cliente, Modelo modelo) {
        this.cliente = cliente;
        this.modelo = modelo;
    }
}
