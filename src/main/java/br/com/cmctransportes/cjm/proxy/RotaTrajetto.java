package br.com.cmctransportes.cjm.proxy;

import br.com.cmctransportes.cjm.constants.Constantes;
import br.com.cmctransportes.cjm.domain.entities.vo.*;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.proxy.modelo.Cliente;
import br.com.cmctransportes.cjm.proxy.modelo.Coordenada;
import br.com.cmctransportes.cjm.proxy.modelo.Local;
import br.com.cmctransportes.cjm.proxy.modelo.Rota;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class RotaTrajetto {

    private static RotaTrajetto rotaTrajetto;

    private RotaTrajetto(){

    }

    public static RotaTrajetto getInstance(){
        try {
            synchronized (RotaTrajetto.class) {
                if (rotaTrajetto == null) {
                    rotaTrajetto = new RotaTrajetto();
                }
            }
        } catch (Exception e) {
            LogSistema.logError("rotaTrajetto", e);
        }
        return rotaTrajetto;
    }

    public RetornoRotaVO cadastrarRota(RotaVO rotaVO){
        RetornoRotaVO retorno = new RetornoRotaVO();
        try {
            Rota rota = convertTO(rotaVO);
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/rota/cadastrarRota"));
            ObjectMapper mapper = new ObjectMapper();
            String json =  mapper.writeValueAsString(rota);
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoRotaVO.class);
            }
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("cadastrarRota", e);
        }
        return retorno;
    }

    public RetornoRotaVO editarRota(RotaVO rotaVO){
        RetornoRotaVO retorno = new RetornoRotaVO();
        try {
            Rota rota = convertTO(rotaVO);
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/rota/editarRota"));
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(rota);
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoRotaVO.class);
            }
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("editarLocal", e);
        }
        return retorno;
    }


    public RetornoRotaVO excluirRota(RotaVO rotaVO){
        RetornoRotaVO retorno = new RetornoRotaVO();
        try {
            Rota rota = convertTO(rotaVO);
            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/rota/excluirRota"));
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(rota);
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoRotaVO.class);
            }
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("editarLocal", e);
        }
        return retorno;
    }



    public RetornoRotaVO buscarListaRota(Integer idEmpresaTrajetto, Integer idUnidade) {
        RetornoRotaVO retorno = new RetornoRotaVO();
        try {
            HttpGet get = new HttpGet(Constantes.URL_SERVICO_TRAJETTO.concat("/rota/buscarListaRota/"+idEmpresaTrajetto.toString()+"/"+idUnidade.toString()));
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(get);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                ObjectMapper mapper = new ObjectMapper();
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retorno = mapper.treeToValue(jsonNode, RetornoRotaVO.class);
            }
            LogSistema.logInfo("buscarLocalPeloCodigo");
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarEquipamentoPeloCodigo", e);
        }
        return retorno;
    }


    public RetornoRotaVO buscarLocalPeloCodigo(Integer id) {
        RetornoRotaVO retorno = new RetornoRotaVO();
        try {
            HttpGet get = new HttpGet(Constantes.URL_SERVICO_TRAJETTO.concat("/rota/buscarRota/"+id.toString()));
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(get);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                ObjectMapper mapper = new ObjectMapper();
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());

                retorno = mapper.treeToValue(jsonNode, RetornoRotaVO.class);
            }
            LogSistema.logInfo("buscarLocalPeloCodigo");
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(e.getMessage());
            LogSistema.logError("buscarEquipamentoPeloCodigo", e);
        }
        return retorno;
    }


    private Rota convertTO(RotaVO rotaVO){
        Rota rota = new Rota();
        rota.setId(rotaVO.getId());
        rota.setNome(rotaVO.getNome());
        rota.setLocalInicio(new Local(rotaVO.getLocalInicio().getId()));
        rota.setLocalFinal(new Local(rotaVO.getLocalFinal().getId()));
        rota.setAtivo(rotaVO.getAtivo());
        rota.setIdUnidade(rotaVO.getIdUnidade());
        if(Objects.nonNull(rotaVO.getCliente())){
            rota.setCliente(new Cliente(rotaVO.getCliente().getIdClienteTrajetto()));
        }
        if(Objects.nonNull(rotaVO.getListaDeCoordenadas())){
            List<Coordenada> listaCoordenada = new ArrayList<>();
            for(CoordenadaVO coordenadaVO : rotaVO.getListaDeCoordenadas()){
                Coordenada coordenada = new Coordenada();
                coordenada.setId(coordenadaVO.getId());
                coordenada.setIsRaio(coordenadaVO.getIsRaio());
                coordenada.setLatitude(coordenadaVO.getLatitude());
                coordenada.setLongitude(coordenadaVO.getLongitude());
                coordenada.setSequencia(coordenadaVO.getSequencia());
                listaCoordenada.add(coordenada);
            }
            rota.setListaDeCoordenadas(listaCoordenada);
        }
        return rota;
    }

    private void ajustarCliente(List<LocalVO> listaDeLocais){
        for(LocalVO local : listaDeLocais){
            local.getCliente().setIdClienteTrajetto(local.getCliente().getId());
            local.getCliente().setId(null);
        }
    }
}
