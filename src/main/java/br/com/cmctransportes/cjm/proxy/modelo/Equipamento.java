package br.com.cmctransportes.cjm.proxy.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class Equipamento implements Serializable {
    private Integer id;
    private Date dataCadastro;
    private String imei;
    private String numeroCelular;
    private String numeroSerial;
    private String observacao;
    private String operadora;
    private Boolean status;
    private String tipoEquipamento;//PR = Proprietario PA = Particular
    private String tipoChip;//PR = Proprietario PA = Particular
    private Modelo modelo;
    private Boolean editarEquipamento;
    private Boolean editarChip;
    private Boolean equipamentoAlocado;
    private Equipamento equipamentoPai;
    @JsonIgnore
    private Boolean bloquearVeiculo;
}
