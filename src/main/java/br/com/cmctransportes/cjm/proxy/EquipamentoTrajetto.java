package br.com.cmctransportes.cjm.proxy;

import br.com.cmctransportes.cjm.constants.Constantes;
import br.com.cmctransportes.cjm.domain.entities.vo.EquipamentoVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoEquipamentoVO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.proxy.modelo.Cliente;
import br.com.cmctransportes.cjm.proxy.modelo.EnvioEquipamento;
import br.com.cmctransportes.cjm.proxy.modelo.Equipamento;
import br.com.cmctransportes.cjm.proxy.modelo.Modelo;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;


public class EquipamentoTrajetto {
    private static EquipamentoTrajetto equipamentoTrajetto;

    private EquipamentoTrajetto(){

    }

    public static EquipamentoTrajetto getInstance(){
        try {
            synchronized (EquipamentoTrajetto.class) {
                if (equipamentoTrajetto == null) {
                    equipamentoTrajetto = new EquipamentoTrajetto();
                }
            }
        } catch (Exception e) {
            LogSistema.logError("EquipamentoTrajetto", e);
        }
        return equipamentoTrajetto;
    }


    public RetornoEquipamentoVO cadastrarEquipamento(EquipamentoVO equipamentoVO) {
        RetornoEquipamentoVO retornoEquipamentoVO = new RetornoEquipamentoVO();
        try {
            EnvioEquipamento envioEquipamento = new EnvioEquipamento();
            Equipamento equipamento = convertTO(equipamentoVO);
            envioEquipamento.setEquipamento(equipamento);
            envioEquipamento.setCliente(new Cliente(equipamentoVO.getEmpresa().getIdClienteTrajetto()));

            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/equipamento/cadastrarEquipamento"));
            ObjectMapper mapper = new ObjectMapper();
            String json =  mapper.writeValueAsString(envioEquipamento);
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);

            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retornoEquipamentoVO = mapper.treeToValue(jsonNode, RetornoEquipamentoVO.class);
            }

        }catch (Exception e){
            retornoEquipamentoVO.setCodigoRetorno(-1);
            retornoEquipamentoVO.setMensagem(e.getMessage());
            LogSistema.logError("cadastrarEquipamento", e);
        }
        return retornoEquipamentoVO;
    }

    public RetornoEquipamentoVO editarEquipamento(EquipamentoVO equipamentoVO) {
        RetornoEquipamentoVO retornoEquipamentoVO = new RetornoEquipamentoVO();
        try {
            EnvioEquipamento envioEquipamento = new EnvioEquipamento();
            Equipamento equipamento = convertTO(equipamentoVO);
            envioEquipamento.setEquipamento(equipamento);
            envioEquipamento.setCliente(new Cliente(equipamentoVO.getEmpresa().getIdClienteTrajetto()));

            HttpPost post = new HttpPost(Constantes.URL_SERVICO_TRAJETTO.concat("/equipamento/editarEquipamento"));
            ObjectMapper mapper = new ObjectMapper();
            String json =  mapper.writeValueAsString(envioEquipamento);
            StringEntity input = new StringEntity(json, "UTF-8");
            input.setContentType("application/json");
            post.setEntity(input);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);

            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retornoEquipamentoVO = mapper.treeToValue(jsonNode, RetornoEquipamentoVO.class);
            }

        }catch (Exception e){
            retornoEquipamentoVO.setCodigoRetorno(-1);
            retornoEquipamentoVO.setMensagem(e.getMessage());
            LogSistema.logError("cadastrarEquipamento", e);
        }
        return retornoEquipamentoVO;
    }

    public RetornoEquipamentoVO listarEquipamentoNaoAlocado() {
        RetornoEquipamentoVO retornoEquipamentoVO = new RetornoEquipamentoVO();
        try {
            HttpGet get = new HttpGet(Constantes.URL_SERVICO_TRAJETTO.concat("/equipamento/listarEquipamentoNaoAlocado"));
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(get);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                ObjectMapper mapper = new ObjectMapper();
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retornoEquipamentoVO = mapper.treeToValue(jsonNode, RetornoEquipamentoVO.class);
            }

        }catch (Exception e){
            retornoEquipamentoVO.setCodigoRetorno(-1);
            retornoEquipamentoVO.setMensagem(e.getMessage());
            LogSistema.logError("cadastrarEquipamento", e);
        }
        return retornoEquipamentoVO;
    }


    public RetornoEquipamentoVO listarEquipamentos() {
        RetornoEquipamentoVO retornoEquipamentoVO = new RetornoEquipamentoVO();
        try {
            HttpGet get = new HttpGet(Constantes.URL_SERVICO_TRAJETTO.concat("/equipamento/listarEquipamentos"));
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(get);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                ObjectMapper mapper = new ObjectMapper();
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retornoEquipamentoVO = mapper.treeToValue(jsonNode, RetornoEquipamentoVO.class);
            }

        }catch (Exception e){
            retornoEquipamentoVO.setCodigoRetorno(-1);
            retornoEquipamentoVO.setMensagem(e.getMessage());
            LogSistema.logError("listarEquipamentos", e);
        }
        return retornoEquipamentoVO;
    }

    public RetornoEquipamentoVO buscarEquipamentoPeloCodigo(Integer idEquipamento) {
        RetornoEquipamentoVO retornoEquipamentoVO = new RetornoEquipamentoVO();
        try {
            HttpGet get = new HttpGet(Constantes.URL_SERVICO_TRAJETTO.concat("/equipamento/buscarEquipamentoPeloCodigo/"+idEquipamento.toString()));
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(get);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                ObjectMapper mapper = new ObjectMapper();
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retornoEquipamentoVO = mapper.treeToValue(jsonNode, RetornoEquipamentoVO.class);
            }
        }catch (Exception e){
            retornoEquipamentoVO.setCodigoRetorno(-1);
            retornoEquipamentoVO.setMensagem(e.getMessage());
            LogSistema.logError("buscarEquipamentoPeloCodigo", e);
        }
        return retornoEquipamentoVO;
    }


    public RetornoEquipamentoVO listarEquipamentoMinimo() {
        RetornoEquipamentoVO retornoEquipamentoVO = new RetornoEquipamentoVO();
        try {
            HttpGet get = new HttpGet(Constantes.URL_SERVICO_TRAJETTO.concat("/equipamento/listarEquipamentoMinimo/"));
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(get);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                ObjectMapper mapper = new ObjectMapper();
                JsonNode jsonNode = mapper.readTree(response.getEntity().getContent());
                retornoEquipamentoVO = mapper.treeToValue(jsonNode, RetornoEquipamentoVO.class);
            }
        }catch (Exception e){
            retornoEquipamentoVO.setCodigoRetorno(-1);
            retornoEquipamentoVO.setMensagem(e.getMessage());
            LogSistema.logError("listarEquipamentoMinimo", e);
        }
        return retornoEquipamentoVO;
    }


    public Equipamento convertTO(EquipamentoVO equipamentoVO){
        Equipamento equipamento = new Equipamento();
        equipamento.setId(equipamentoVO.getId());
        equipamento.setDataCadastro(equipamentoVO.getDataCadastro());
        equipamento.setEditarChip(equipamentoVO.getEquipamentoAlocado());
        equipamento.setImei(equipamentoVO.getImei());
        equipamento.setNumeroCelular(equipamentoVO.getNumeroCelular());
        equipamento.setNumeroSerial(equipamentoVO.getNumeroSerial());
        equipamento.setOperadora(equipamentoVO.getOperadora());
        equipamento.setStatus(equipamentoVO.getStatus());
        equipamento.setTipoEquipamento(equipamentoVO.getTipoEquipamento());
        equipamento.setTipoChip(equipamentoVO.getTipoChip());
        Modelo modelo = new Modelo();
        modelo.setFabricante(equipamentoVO.getModelo().getFabricante());
        modelo.setId(equipamentoVO.getModelo().getId());
        modelo.setMarca(equipamentoVO.getModelo().getMarca());
        modelo.setStatus(equipamentoVO.getModelo().getStatus());
        modelo.setTipo(equipamentoVO.getModelo().getTipo());
        equipamento.setModelo(modelo);
        equipamento.setEditarEquipamento(false);
        equipamento.setEditarChip(false);
        equipamento.setEquipamentoAlocado(false);
        equipamento.setEquipamentoPai(null);
        return  equipamento;
    }

}
