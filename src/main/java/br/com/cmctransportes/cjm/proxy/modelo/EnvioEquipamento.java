package br.com.cmctransportes.cjm.proxy.modelo;

import java.io.Serializable;

public class EnvioEquipamento implements Serializable {

    private Equipamento equipamento;
    private Cliente cliente;

    public Equipamento getEquipamento() {
        return equipamento;
    }

    public void setEquipamento(Equipamento equipamento) {
        this.equipamento = equipamento;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}
