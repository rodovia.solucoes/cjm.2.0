package br.com.cmctransportes.cjm.proxy.modelo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Cliente {
    private Integer id;
    private Boolean clienteFinal;

    public Cliente() {

    }

    public Cliente(Integer id) {
        this.id = id;
    }
}
