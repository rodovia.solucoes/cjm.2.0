package br.com.cmctransportes.cjm.services;

import br.com.cmctransportes.cjm.domain.entities.Jornada;
import br.com.cmctransportes.cjm.domain.entities.enums.NONCONFORMITY_CALCULUS_TYPE;
import br.com.cmctransportes.cjm.domain.entities.vo.NonconformityVO;
import br.com.cmctransportes.cjm.services.nonconformity.InterjourneyNonconfomityService;
import br.com.cmctransportes.cjm.services.nonconformity.PaidWeeklyRestNonconformityService;
import br.com.cmctransportes.cjm.services.nonconformity.base.BaseNonconformityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author William Leite
 */
@Service
public class NonconformityCalculusService {

    @Autowired
    private PaidWeeklyRestNonconformityService paidWeeklyRestNonconformityService;

    @Autowired
    private InterjourneyNonconfomityService interjourneyNonconfomityService;

    private Map<NONCONFORMITY_CALCULUS_TYPE, BaseNonconformityService<?>> serviceMap() {
        Map<NONCONFORMITY_CALCULUS_TYPE, BaseNonconformityService<?>> result = new HashMap<>();

        result.put(NONCONFORMITY_CALCULUS_TYPE.DSR, paidWeeklyRestNonconformityService);
        result.put(NONCONFORMITY_CALCULUS_TYPE.INTERJOURNEY, interjourneyNonconfomityService);

        return result;
    }

    @SuppressWarnings("unchecked")
    public List<NonconformityVO> process(Date dataInicio, Date dataFinal, List<Jornada> journeys, List<Integer> tipos, Boolean preview) {
        List<NonconformityVO> result = new ArrayList<>(0);

        // If empty
        if (Objects.isNull(journeys) || journeys.isEmpty() || Objects.isNull(journeys.get(0))) {
            return result;
        }

        // Remove nulls and journeys without a start
        // TODO: [REFACTOR] why are there journeys without start?
        // TODO: [REFACTOR] why are there null journeys within the list?
        List<Jornada> filteredJourneys = journeys.stream()
                .filter(Objects::nonNull)
                .filter(j -> Objects.nonNull(j.getJornadaInicio()))
                .collect(Collectors.toList());

        Map<NONCONFORMITY_CALCULUS_TYPE, BaseNonconformityService<?>> serviceMap = this.serviceMap();

        tipos.stream().forEach(t -> result.addAll(serviceMap.get(NONCONFORMITY_CALCULUS_TYPE.fromCode(t)).process(dataInicio, dataFinal, filteredJourneys, preview)));

        return result;
    }
}