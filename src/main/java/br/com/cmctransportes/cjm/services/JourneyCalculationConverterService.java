package br.com.cmctransportes.cjm.services;

import br.com.cmctransportes.cjm.domain.entities.JourneyCalculation;
import br.com.cmctransportes.cjm.domain.entities.JourneyCalculationLine;
import br.com.cmctransportes.cjm.domain.entities.Nonconformity;
import br.com.cmctransportes.cjm.domain.entities.Users;
import br.com.cmctransportes.cjm.domain.entities.enums.JOURNEY_CALCULATION_STATUS;
import br.com.cmctransportes.cjm.domain.entities.enums.NONCONFORMITY_CALCULUS_TYPE;
import br.com.cmctransportes.cjm.domain.entities.jornada.ApuracaoDeJornada;
import br.com.cmctransportes.cjm.domain.entities.jornada.RelatorioApuracaoDeJornada;
import br.com.cmctransportes.cjm.domain.entities.vo.NonconformityVO;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author William Leite
 */
public class JourneyCalculationConverterService implements Serializable {

    private static final long serialVersionUID = 1L;

    public RelatorioApuracaoDeJornada convert(JourneyCalculation entity) {
        RelatorioApuracaoDeJornada vo = new RelatorioApuracaoDeJornada();

        vo.setDataFim(entity.getEndDate());
        vo.setDataInicio(entity.getStartDate());
        vo.setMotorista(entity.getDriver());
        vo.setMesReferencia(entity.getReferenceMonth());

        vo.getLinhas().addAll(this.convertFromLine(entity.getLines()));

        return vo;
    }

    public JourneyCalculation convert(RelatorioApuracaoDeJornada vo, Users user) {
        JourneyCalculation entity = new JourneyCalculation();

        entity.setStartDate(vo.getDataInicio());
        entity.setEndDate(vo.getDataFim());
        entity.setDriver(vo.getMotorista());
        entity.setStatus(JOURNEY_CALCULATION_STATUS.UNALTERED);
        entity.setCreatedAt(new Date());
        entity.setCreatedBy(user);
        entity.setLines(this.convert(entity, vo));
        entity.setReferenceMonth(vo.getMesReferencia());

        return entity;
    }

    private List<ApuracaoDeJornada> convertFromLine(List<JourneyCalculationLine> vo) {
        List<ApuracaoDeJornada> result = new ArrayList<>(0);
        vo.stream().forEach(l -> {
            ApuracaoDeJornada line = new ApuracaoDeJornada();
            line.setAdicionalNoturno(l.getAditionalNightly());
            line.setData(l.getDate());
            line.setHedDSR(l.getDailyExtraHoursDSR());
            line.setHedFeriado(l.getDailyExtraHoursHoliday());
            line.setHedIntraJornada(l.getDailyExtraHoursIntrajourney());
            line.setHenDSR(l.getNightlyExtraHoursDSR());
            line.setHenFeriado(l.getNightlyExtraHoursHoliday());
            line.setHenIntraJornada(l.getNightlyExtraHoursIntrajourney());
            line.setHorasApuradas(l.getTotalHours());
            line.setHorasExtrasDiurnas(l.getDailyExtraHoursTotal());
            line.setHorasExtrasInterJornadas(l.getInterjourneyExtraHours());
            line.setHorasExtrasNoturnas(l.getNightlyExtraHoursTotal());
            line.setHorasFaltosasAbonadas(l.getGracedMissingHours());
            line.setHorasFaltosasDescontadas(l.getDeductedMissingHours());
            line.setTempoDeEsperaDiurno(l.getDailyAwaiting());
            line.setTempoDeEsperaNoturno(l.getNightlyAwaiting());
            line.setTipoJornada(l.getJourneyType());

            line.setDsrNonconformities(new ArrayList<>(0));
            line.setInterjourneyNonconformities(new ArrayList<>(0));

            l.getNonconformities().stream().forEach(n -> {
                if (n.getCalculusType() == NONCONFORMITY_CALCULUS_TYPE.DSR) {
                    line.getDsrNonconformities().add(this.convert(n));
                } else {
                    line.getInterjourneyNonconformities().add(this.convert(n));
                }
            });

            result.add(line);
        });
        return result;
    }

    private List<JourneyCalculationLine> convert(JourneyCalculation parent, RelatorioApuracaoDeJornada vo) {
        List<JourneyCalculationLine> result = new ArrayList<>(0);
        vo.getLinhas().stream().forEach(l -> {
            JourneyCalculationLine line = new JourneyCalculationLine();

            line.setAditionalNightly(l.getAdicionalNoturno());

            line.setDailyAwaiting(l.getTempoDeEsperaDiurno());
            line.setDailyExtraHoursDSR(l.getHedDSR());
            line.setDailyExtraHoursHoliday(l.getHedFeriado());
            line.setDailyExtraHoursIntrajourney(l.getHedIntraJornada());
            line.setDailyExtraHoursTotal(l.getHorasExtrasDiurnas());

            line.setDate(l.getData());

            line.setDeductedMissingHours(l.getHorasFaltosasDescontadas());
            line.setGracedMissingHours(l.getHorasFaltosasAbonadas());

            line.setInterjourneyExtraHours(l.getHorasExtrasInterJornadas());
            line.setJourneyType(l.getTipoJornada());

            line.setNightlyAwaiting(l.getTempoDeEsperaNoturno());
            line.setNightlyExtraHoursDSR(l.getHenDSR());
            line.setNightlyExtraHoursHoliday(l.getHenFeriado());
            line.setNightlyExtraHoursIntrajourney(l.getHenIntraJornada());
            line.setNightlyExtraHoursTotal(l.getHorasExtrasNoturnas());

            line.setTotalHours(l.getHorasApuradas());

            List<Nonconformity> nonconformities = new ArrayList<>(0);
            nonconformities.addAll(this.convert(line, l.getInterjourneyNonconformities()));
            nonconformities.addAll(this.convert(line, l.getDsrNonconformities()));
            line.setNonconformities(nonconformities);

            line.setJourneyCalculation(parent);

            result.add(line);
        });
        return result;
    }

    private NonconformityVO convert(Nonconformity entity) {
        NonconformityVO vo = new NonconformityVO();
        vo.setCalculusType(entity.getCalculusType());
        vo.setDescription(entity.getDescription());
        vo.setDriver(entity.getDriver());
        vo.setEndDate(entity.getEndDate());
        vo.setStartDate(entity.getStartDate());
        vo.setTreatmentArgs(entity.getTreatmentArgs());
        vo.setTreatmentDescription(entity.getTreatmentDescription());
        vo.setTreatmentType(entity.getTreatmentType());
        vo.setType(entity.getType());
        return vo;
    }

    public Nonconformity convert(NonconformityVO v) {
        Nonconformity entity = new Nonconformity();

        entity.setCalculusType(v.getCalculusType());
        entity.setDescription(v.getDescription());
        entity.setDriver(v.getDriver());
        entity.setTreatmentArgs(v.getTreatmentArgs());
        entity.setTreatmentDescription(v.getTreatmentDescription());
        entity.setTreatmentType(v.getTreatmentType());
        entity.setType(v.getType());

        entity.setStartDate(v.getStartDate());
        entity.setEndDate(v.getEndDate());

        return entity;
    }

    private List<Nonconformity> convert(JourneyCalculationLine parent, List<NonconformityVO> vo) {
        List<Nonconformity> result = new ArrayList<>(0);

        vo.stream().forEach(v -> {
            Nonconformity entity = this.convert(v);
            entity.setLine(parent);
            result.add(entity);
        });

        return result;
    }
}
