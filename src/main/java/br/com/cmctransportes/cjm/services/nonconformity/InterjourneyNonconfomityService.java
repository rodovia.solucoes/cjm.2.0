package br.com.cmctransportes.cjm.services.nonconformity;

import br.com.cmctransportes.cjm.domain.entities.Empresas;
import br.com.cmctransportes.cjm.domain.entities.Evento;
import br.com.cmctransportes.cjm.domain.entities.Jornada;
import br.com.cmctransportes.cjm.domain.entities.enums.JOURNEY_TYPE;
import br.com.cmctransportes.cjm.domain.entities.jornada.Resumo;
import br.com.cmctransportes.cjm.domain.entities.vo.DiurnoNoturno;
import br.com.cmctransportes.cjm.domain.entities.vo.InterjourneyAnalysisVO;
import br.com.cmctransportes.cjm.domain.entities.vo.LocalVO;
import br.com.cmctransportes.cjm.domain.entities.vo.NonconformityVO;
import br.com.cmctransportes.cjm.domain.services.EventoService;
import br.com.cmctransportes.cjm.domain.services.PontosCargaDescargaService;
import br.com.cmctransportes.cjm.services.nonconformity.base.BaseNonconformityService;
import br.com.cmctransportes.cjm.utils.NonconformityFactory;
import br.com.cmctransportes.cjm.utils.TimeHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * @author William Leite
 */
@Service
public class InterjourneyNonconfomityService extends BaseNonconformityService<InterjourneyAnalysisVO> {

    @Autowired
    private EventoService eventoService;

    @Autowired
    private PontosCargaDescargaService pdcService;

    @Override
    public List<NonconformityVO> process(Date startDate, Date finalDate, List<Jornada> journeys, boolean save) {
        List<NonconformityVO> result = new ArrayList<>();

        final AtomicReference<Jornada> atomicPreviousJourney = new AtomicReference<>(null);
        final Map<TimeHelper.Period, Long> normalPay = new HashMap<>();
        final Map<TimeHelper.Period, Long> extraPay = new HashMap<>();
        final Map<TimeHelper.Period, Long> nightPay = new HashMap<>();

        final List<JOURNEY_TYPE> exceptionTypes = Arrays.asList(new JOURNEY_TYPE[]{JOURNEY_TYPE.DSR, JOURNEY_TYPE.FOLGA, JOURNEY_TYPE.FERIADO, JOURNEY_TYPE.FERIAS, JOURNEY_TYPE.ABONO});

        final Long interjourneyHourLimit = TimeHelper.hours(11);

        Queue<Jornada> queue = new LinkedList<>(journeys.stream().filter(j -> j.getType() != JOURNEY_TYPE.FALTA).collect(Collectors.toList()));

        this.print("Interjourney Calculation");

        while (Objects.nonNull(queue.peek())) {
            // Current journey
            Jornada journey = queue.poll();
            // Next journey, null if end of queue
            Jornada nextJourney = queue.peek();

            this.print("Journey Loop Start [%s][%s]", journey.getJornadaInicio(), journey.getJornadaFim());

            Empresas company = journey.getEmpresaId();
            Jornada previousJourney = atomicPreviousJourney.get();

            if (company.getConsidersInterjourney()) {
                // If there is a previous journey, we continue the process
                if (Objects.nonNull(previousJourney) && Objects.nonNull(previousJourney.getJornadaFim())) {
                    // Calculates the gap between journeys
                    long gap = this.calculateGap(journey, previousJourney, null, false);

                    if (exceptionTypes.contains(journey.getType()) || exceptionTypes.contains(previousJourney.getType())) {
                        // Do nothing
                    } else {
                        long aditional = this.calculateAditional(previousJourney, journey, gap);

                        // If the gap is lesser than 11 hours
                        if (gap < interjourneyHourLimit && aditional + gap < interjourneyHourLimit) {
                            // Now we check if the next Interjouney fits the remainder
                            long nextGap = 0L;
                            // Only when there is a next journey, otherwise end of queue
                            if (Objects.nonNull(nextJourney)) {
                                nextGap = this.calculateGap(nextJourney, journey, null, false) - interjourneyHourLimit;
                            }

                            // Final gap
                            gap += aditional;

                            // If there isn't a nextGap or we couldn't achieve the interjourneyHourLimit after adding it, nonconformity
                            if (nextGap < 0 || nextGap + gap < interjourneyHourLimit) {
                                TimeHelper.Period period = new TimeHelper.Period(previousJourney.getJornadaFim(), journey.getJornadaInicio());
                                long remainingGap = interjourneyHourLimit - gap;

                                // If the previous or the current journey has a night period
                                if (this.hasNightTime(previousJourney, journey)) {
                                    long totalNightTime = this.retrieveNightTime(previousJourney, journey);
                                    nightPay.put(period, totalNightTime);
                                } else if (gap >= interjourneyHourLimit / 2) {
                                    extraPay.put(period, remainingGap);
                                } else if (gap < interjourneyHourLimit / 2 && (company.getHalfInterjourney() != null && company.getHalfInterjourney())) {
                                    extraPay.put(period, interjourneyHourLimit);
                                }
                            }
                        }
                    }
                }
            } else {
                return result;
            }
            // Updates the previous variables
            atomicPreviousJourney.set(journey);

            this.print("Journey Loop End");
        }

        InterjourneyAnalysisVO analysis = new InterjourneyAnalysisVO();
        analysis.setDriver(journeys.get(0).getMotoristasId());
        analysis.setExtraPay(extraPay);
        analysis.setNormalPay(normalPay);
        analysis.setNightPay(nightPay);

        result.addAll(this.analyse(analysis, save));

        return result;
    }

    private long retrieveNightTime(Jornada journey, boolean start) {
        Calendar calendar = Calendar.getInstance();
        Calendar aux_cal = Calendar.getInstance();

        if (start) {
            calendar.setTime(journey.getJornadaInicio());
        } else {
            calendar.setTime(journey.getJornadaFim());
        }

        // 22h - 23h59
        if (calendar.get(Calendar.HOUR_OF_DAY) > 22) {
            if (!start) {
                // 22h of the same day
                aux_cal.setTime(journey.getJornadaFim());
                aux_cal.set(Calendar.HOUR_OF_DAY, 22);
                aux_cal.set(Calendar.MINUTE, 0);
            } else {
                // 05h of the next day
                aux_cal.setTime(journey.getJornadaInicio());
                aux_cal.set(Calendar.HOUR_OF_DAY, 5);
                aux_cal.set(Calendar.MINUTE, 0);
                aux_cal.add(Calendar.DATE, 1);
            }
            // 00h - 04h59
        } else if (calendar.get(Calendar.HOUR_OF_DAY) < 5) {
            if (!start) {
                // 22h of the previous day
                aux_cal.setTime(journey.getJornadaFim());
                aux_cal.set(Calendar.HOUR_OF_DAY, 22);
                aux_cal.set(Calendar.MINUTE, 0);
                aux_cal.add(Calendar.DATE, -1);
            } else {
                // 05h of the same day
                aux_cal.setTime(journey.getJornadaInicio());
                aux_cal.set(Calendar.HOUR_OF_DAY, 05);
                aux_cal.set(Calendar.MINUTE, 0);
            }
        }

        if (start) {
            return aux_cal.getTimeInMillis() - calendar.getTimeInMillis();
        } else {
            return calendar.getTimeInMillis() - aux_cal.getTimeInMillis();
        }
    }

    private long retrieveNightTime(Jornada previous, Jornada current) {
        long result = 0L;

        if (this.hasNightTime(previous, false)) {
            result += this.retrieveNightTime(previous, false);
        }

        if (this.hasNightTime(current, true)) {
            result += this.retrieveNightTime(current, true);
        }

        return result;
    }

    private boolean hasNightTime(Jornada journey, boolean start) {
        Calendar calendar = Calendar.getInstance();

        if (start) {
            calendar.setTime(journey.getJornadaInicio());
        } else {
            calendar.setTime(journey.getJornadaFim());
        }

        return calendar.get(Calendar.HOUR_OF_DAY) > 22 || calendar.get(Calendar.HOUR_OF_DAY) < 5;
    }

    private boolean hasNightTime(Jornada previous, Jornada current) {
        // Previous ends after 22h or the current start before 05h
        return this.hasNightTime(previous, false) || this.hasNightTime(current, true);
    }

    private long calculateAditional(Jornada previous, Jornada current, long gap) {
        long result = 0L;

        // Compensation in standby can only be considered if at least 8 hours was already taken (removed, for it ruins Camila's algorihtm
//        if (gap < TimeHelper.hours(8)) {
//            return result;
//        }
        Empresas company = current.getEmpresaId();

        // Check if the company allows for compensation
        if (!company.getInterjourneyCompensateStandby()) {
            return result;
        }

        // We start counting on the end of the previous journey
        long startDate = previous.getJornadaFim().getTime();

        // Retrieve the limit for the compensation lookup
        long maxAllowed = startDate + company.getInterjourneyCompensationLimit();

        // Retrieve the current journey's summary
        Resumo summary = current.getResumoTiposEstados().get("Espera");

        // Retrieve the previous journey's summary
        Resumo previousSummary = previous.getResumoTiposEstados().get("Espera");

        if (Objects.nonNull(summary)) {
            List<Evento> waitEvents = summary.getLista();

            // If there is a previous summary and the current journey occurred on the same day as the previous
            if (Objects.nonNull(previousSummary)
                    && (TimeHelper.isSameDay(previous.getJornadaInicio(), current.getJornadaInicio())
                    || TimeHelper.isSameDay(previous.getJornadaFim(), current.getJornadaInicio()))) {
                waitEvents.addAll(previousSummary.getLista());
            }

           List<LocalVO> listaLocal = pdcService.findAllAsLocal(company.getId());

            // Filters:
            // 1) Events that start at or after the start (removed, for it ruins Camila's algorihtm
            // 2) Events that end at or before the allowed period (removed, for it ruins Camila's algorithm
            result = waitEvents.stream()
                    //                .filter(e -> e.getInstanteEvento().getTime() >= startDate)
                    //                .filter(e -> Objects.nonNull(e.getFimEvento()) && e.getFimEvento().getTime() <= maxAllowed)
                    .collect(Collectors.summingLong(event -> {
                        DiurnoNoturno dnn = event.getDayNight();
                        long totalTime = dnn.getTempoDiurno() + dnn.getTempoNoturno();

                        // Total time in rest must be greater than 2 hours and the event must have taken place in an allowed location
                        if (totalTime > TimeHelper.hours(2) && this.eventoService.eventPlaceAllowsRest(event, listaLocal)) {
                            return totalTime;
                        }

                        return 0L;
                    }));
        }

        return result;
    }

    @Override
    protected List<NonconformityVO> analyse(InterjourneyAnalysisVO analysis, boolean save) {
        List<NonconformityVO> result = new ArrayList<>();
        NonconformityFactory factory = NonconformityFactory.getInstance(analysis.getDriver());

        analysis.getNormalPay().entrySet().forEach(entry -> {
            result.add(factory.interjourneyNormalPay(entry.getKey().getStart(), entry.getKey().getEnd(), entry.getValue()));
        });

        analysis.getExtraPay().entrySet().forEach(entry -> {
            result.add(factory.interjourneyExtraPay(entry.getKey().getStart(), entry.getKey().getEnd(), entry.getValue()));
        });

        analysis.getNightPay().entrySet().forEach(entry -> {
            result.add(factory.interjourneyNightPay(entry.getKey().getStart(), entry.getKey().getEnd(), entry.getValue()));
        });

        return result;
    }
}
