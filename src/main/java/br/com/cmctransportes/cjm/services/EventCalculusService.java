package br.com.cmctransportes.cjm.services;

import br.com.cmctransportes.cjm.domain.entities.Evento;
import br.com.cmctransportes.cjm.domain.entities.vo.DiurnoNoturno;
import br.com.cmctransportes.cjm.domain.services.NoturnoService;

/**
 * @author William Leite
 */
public class EventCalculusService {
    /**
     * Returns if the event passed as a parameter is a paid event. Paid events will have a "tipoEvento" that has an
     * "objGeraEstado" that has a "tipoEstadoJornada" that has a "descricao" with the values "Ocioso" or "Deslocamento".
     *
     * @param event Event to analyze
     * @return If it is a paid event.
     */
    public boolean isPaid(Evento event) {
        try {
            String tipoEstado = event.getTipoEvento().getObjGeraEstado().getTiposEstadoJornadaId().getDescricao();
            return ("Ocioso".equals(tipoEstado) || "Deslocamento".equals(tipoEstado));
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Returns if the event passed as a parameter is a between event. Paid events will have a "tipoEvento" that has an
     * "objGeraEstado" that has a "tipoEstadoJornada" that has a "descricao" with the value "Alimentação".
     *
     * @param event Event to analyze
     * @return If it is a between event.
     */
    public boolean isBetweenJourney(Evento event) {
        String tipoEstado = event.getTipoEvento().getObjGeraEstado().getTiposEstadoJornadaId().getDescricao();
        return ("Alimentação".equals(tipoEstado));
    }

    /**
     * Returns if the event passed as a parameter is an allowance event. Paid events will have a "tipoEvento" that has an
     * "objGeraEstado" that has a "tipoEstadoJornada" that has a "descricao" with the value "Abono".
     *
     * @param event Event to analyze
     * @return If it is an allowance event.
     */
    public boolean isAllowance(Evento event) {
        String tipoEstado = event.getTipoEvento().getObjGeraEstado().getTiposEstadoJornadaId().getDescricao();
        return ("Abono".equals(tipoEstado));
    }

    /**
     * Splits the event's work hours in day and night. The result will be filled in the parameter's "dayNight" property.
     *
     * @param event Event to split.
     * @see br.com.cmctransportes.cjm.services.EventCalculusService.isPaid
     * @see br.com.cmctransportes.cjm.domain.services.NoturnoService.calculaNoturno
     */
    public void splitDayNight(Evento event) {
        NoturnoService noturnoService = new NoturnoService();
        DiurnoNoturno dayNight = noturnoService.calculaNoturno(event.getInstanteEvento(), event.getFimEvento(), this.isPaid(event));

        event.setDayNight(dayNight);
    }
}
