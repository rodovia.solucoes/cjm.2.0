package br.com.cmctransportes.cjm.services;

import br.com.cmctransportes.cjm.domain.entities.*;
import br.com.cmctransportes.cjm.domain.entities.enums.JOURNEY_TYPE;
import br.com.cmctransportes.cjm.domain.entities.jornada.Resumo;
import br.com.cmctransportes.cjm.domain.entities.vo.DiurnoNoturno;
import br.com.cmctransportes.cjm.domain.services.*;
import br.com.cmctransportes.cjm.infra.dao.ToleranciasDAO;
import br.com.cmctransportes.cjm.utils.ChartReflectionUtils;
import br.com.cmctransportes.cjm.utils.TimeHelper;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author William Leite
 */
@Service
public class JourneyCalculusService {

    private final ShiftCalculusService shiftCalculusService;
    private final EventCalculusService eventCalculusService;
    private final DsrService dsrService;
    private final FeriadosService feriadosService;
    private final EventoService eventService;
    private final JornadaService jornadaService;
    private final EventosJornadaService eventosJornadaService;

    private boolean fimDefinitivo;
    private boolean he100porcento;

    /**
     * Atributo responsável por expor as horas trabalhadas por jornada.
     *
     * @author Vinicius Prado.
     */
    private Long workHours;

    public JourneyCalculusService(DsrService dsrService, FeriadosService feriadosService, EventoService eventService,
                                  JornadaService jornadaService, EventosJornadaService eventosJornadaService) {
        this.shiftCalculusService = new ShiftCalculusService();
        this.eventCalculusService = new EventCalculusService();
        this.dsrService = dsrService;
        this.feriadosService = feriadosService;
        this.eventService = eventService;
        this.jornadaService = jornadaService;
        this.eventosJornadaService = eventosJornadaService;
    }

    /**
     * Eliminates all duplicate events inside the journey passed as a parameter. The duplicates are analyzed based on the properties "instanteEvento",
     * and "tipoEvento". If there are any duplicates, the method will maintain the first event found and remove the rest.
     *
     * @param journey Journey to analyze.
     */
    public void eliminateDoubles(Jornada journey) {
        List<Evento> events = journey.getEventos().stream().filter(e -> !e.getRemovido()).collect(Collectors.toList());

        Set<Date> eventDates = events.stream().map(Evento::getInstanteEvento).distinct().collect(Collectors.toSet());

        Set<Integer> eventTypes = events.stream().filter(e -> Objects.nonNull(e.getTipoEvento())).map(e -> e.getTipoEvento().getId()).distinct().collect(Collectors.toSet());

        eventDates.stream().forEach(ed -> {
            eventTypes.stream().forEach(et -> {
                List<Evento> filtered = events.stream()
                        .filter(e -> Objects.nonNull(e.getTipoEvento()))
                        .filter(e -> Objects.equals(e.getInstanteEvento(), ed) && Objects.equals(e.getTipoEvento().getId(), et))
                        .sorted(Comparator.comparing(Evento::getInstanteEvento))
                        .collect(Collectors.toList());

                if (filtered.size() > 1) {
                    filtered.remove(0);
                    filtered.forEach(e -> {
                        e.setRemovido(true);
                        this.eventService.update(e.getId(), e);
                    });
                }
            });
        });

    }

    /**
     * Fills the journey passed as a parameter with a new dummy event, if the last event in the journey has a "tipoEvento" with a
     * positive ID. "TipoEvento"s that has a positive ID are starts. The dummy event will be created with "instanteEvento" filled
     * with the current date, "tipoEvento" with the corresponding end based on the journey's last event. The created event will be
     * added to the parameter's event collection.
     * <p>
     * The parameter must not be null, and must have a non-empty collection of events.
     *
     * @param journey Journey to analyze.
     */
    public void fillMissingLast(Jornada journey) {
        if (Objects.isNull(journey) || Objects.isNull(journey.getEventos()) || journey.getEventos().isEmpty()) {
            return;
        }

        Collection<Evento> eventos = journey.getEventos();
        Evento last = eventos.toArray(new Evento[eventos.size()])[eventos.size() - 1];
        if (last.getTipoEvento().getId() > 0) {
            Evento missing = new Evento();

            Integer startId = last.getTipoEvento().getId() == 1 ? 0 : last.getTipoEvento().getId();
            EventosJornada endingType = this.eventosJornadaService.getById(startId * -1);

            missing.setTipoEvento(endingType);
            missing.setInstanteEvento(new Date());
            missing.setJornadaId(journey);

            journey.getEventos().add(missing);
        }
    }

    /**
     * Journey calculation entry point. The journey passed as a parameter will be filled with the calculation results.
     * In order to successfully calculated the journey, several condition apply, i.e. parameter must not be null, journey must
     * have a collection of events, the first event in the collection must have a "tipoEvento".
     * <p>
     * The second parameter "fillMissingLast", will define if the process can fill an incomplete journey.
     * <p>
     * The journey's "jornadaInicio" will be filled with the first event's "instanteEvento"; "type" will be filled with the first
     * event's "tipoEvento" (@see br.com.cmctransportes.cjm.domain.entities.enums.JOURNEY_TYPE.getById).
     * <p>
     * For each event in the journey, separates it in the stateSummary (event's "tipoEvento"'s "objGeraEstado") and stateTypeSummary
     * (event's "tipoEvento"'s "objGeraEstado"'s "tiposEstadosJornadaId") maps, then calculates the event's hours.
     * <p>
     * Updates the journey's "resumoEstados" and "resumoTiposEstados" with the stateSummary and stateTypeSummary respectively.
     * <p>
     * Calculates the journey's worked hours, extra time based on the driver's continuous driving.
     * <p>
     * If the journey is marked as "dsrHE", calculates the DSR overtime.
     * <p>
     * If the journey is marked as "waitingAsWorked", calculates the waiting as worked hours.
     * <p>
     * Lastly fixes the journey's events locations.
     *
     * @param journey         Journey to calculate.
     * @param fillMissingLast Defines if the process can fill an incomplete journey.
     */
    public void processEvents(Jornada journey, Jornada ultimaJornada, boolean fillMissingLast) {
        if (Objects.isNull(journey) || Objects.isNull(journey.getEventos()) || journey.getEventos().isEmpty()) {
            return;
        }

        this.eliminateDoubles(journey);

        if (fillMissingLast) {
            this.fillMissingLast(journey);
        }


        Evento tmpEvent = journey.getEventos().stream().filter(e -> e.getId() != null).sorted(Comparator.comparing(Evento::getInstanteEvento)).findFirst().get();
        if (Objects.isNull(tmpEvent.getTipoEvento())) {
            return;
        }

        journey.setJornadaInicio(tmpEvent.getInstanteEvento());
        journey.setType(JOURNEY_TYPE.getById(tmpEvent.getTipoEvento().getId()));

        // Get the total amount of work hours for the driver based on his/her shift
        workHours = this.shiftCalculusService.workHoursPerDate(journey.getMotoristasId().getTurnosId(), journey.getJornadaInicio());

        HashMap<String, Resumo> stateSummary = new HashMap<>(0);
        HashMap<String, Resumo> stateTypeSummary = new HashMap<>(0);

        this.initCounters(journey);

        if (Objects.isNull(journey.getEventos())) {
            return;
        }

        journey.getEventos().stream().forEach(event -> {
            if (Objects.nonNull(event.getTipoEvento())) {
                // Check if the journey has reached its end, updating geographical position and journey end while doing so
                if (event.getTipoEvento().getId() < 1) {
                    fimDefinitivo(event, journey);
                }
                // Sets the day/night worked period
                this.eventCalculusService.splitDayNight(event);

                // NIVEL 0
                EstadosJornada estado = event.getTipoEvento().getObjGeraEstado();
                if (Objects.nonNull(estado) && Objects.nonNull(estado.getTiposEstadoJornadaId())) {
                    // NIVEL 1
                    this.addSummary(estado.getDescricao(), estado.getColor(), estado.getTiposEstadoJornadaId().getIcone(), stateSummary, event);
                    if (Objects.nonNull(estado.getTiposEstadoJornadaId())) {
                        // NIVEL 2
                        TiposEstadoJornada tmpTipoEstado = estado.getTiposEstadoJornadaId();
                        if (Objects.nonNull(tmpTipoEstado)) {
                            this.addSummary(tmpTipoEstado.getDescricao(), tmpTipoEstado.getColor(), tmpTipoEstado.getIcone(), stateTypeSummary, event);
                        }
                    }
                }

                this.calculateJourney(event, workHours);
            }
        });

        journey.setResumoEstados(stateSummary);
        journey.setResumoTiposEstados(stateTypeSummary);


        this.calculateWorkedHours(journey, stateTypeSummary, workHours);
        this.calculateExtraTimeContinuousDriving(stateSummary, journey, workHours);

        if (Objects.equals(journey.getDsrHE(), true)) {
            this.calculateDSROvertime(journey);
        }

        if (Objects.equals(journey.getWaitingAsWorked(), true)) {
            this.calculateWaitingAsWorked(journey);
        }


        try {
            if (journey.getMotoristasId().getTurnosId().getTipo() == 0) {
                //PARA SANAR O MANTIS 218
                //Quando o turno é 12 x 36 acrecent novamente a hora de almoço

                Evento eventoInicioRefeicao = null;
                for (Evento evento : journey.getEventos()) {
                    if (evento.getTipoEvento().getId() == 14) {
                        eventoInicioRefeicao = evento;
                    }
                    if (evento.getTipoEvento().getId() == -14) {

                        long mili = evento.getInstanteEvento().getTime() - eventoInicioRefeicao.getInstanteEvento().getTime();
                        if (mili > 3600000) {
                            //buscar tolerancia
                            long tolerancia = new ToleranciasDAO(null).buscarToleranciaAlmoco(journey.getEmpresaId().getId());
                            if ((mili - 3600000) > tolerancia) {
                                mili = mili - tolerancia;
                            }
                        }

                        journey.setTrabalhado(journey.getTrabalhado() + mili);
                        if (journey.getHorasExtras() > 0) {
                            journey.setHorasExtras(journey.getHorasExtras() + mili);
                        }
                        if (journey.getTrabalhado() > 43200000 && journey.getHorasExtras() == 0) {
                            journey.setHorasExtras(journey.getTrabalhado() - 43200000);
                        }
                        break;
                    }
                }

            }
        } catch (Exception e) {
        }

        // this.jornadaService.verificarCalculosFinais(journey);
    }


    /**
     * Calculate the journey's, passed as a parameter, waiting hours as worked. Based on the driver's shift, if the total amount of hours
     * worked is lesser than the total amount of hours in the shift, the method tries to compensate with the journey's waiting hours amount,
     * both on the daily and night hours. At the end the journey's "esperaDiurno", "esperaNoturno" and "trabalhado" will be updated with the
     * calculated values.
     *
     * @param journey Journey to calculate the waiting hours as worked.
     */
    private void calculateWaitingAsWorked(Jornada journey) {
        long worked = journey.getTrabalhado();
        long onholdday = Objects.nonNull(journey.getEsperaDiurno()) ? journey.getEsperaDiurno() : 0;
        long onholdnight = Objects.nonNull(journey.getEsperaNoturno()) ? journey.getEsperaNoturno() : 0;

        Turnos shift = journey.getMotoristasId().getTurnosId();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(journey.getJornadaInicio());

        ChartReflectionUtils reflectionUtils = new ChartReflectionUtils();
        Long totalHoursDay = 0L;

        switch (shift.getTipo()) {
            case 2:
                int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);// + 1;
                totalHoursDay = reflectionUtils.invokeGetter(shift, String.format("tempoDiario0%s", dayOfWeek));
                break;
            default:
                break;
        }

        if (totalHoursDay > worked) {

            // Get the hours needed to complete
            Long remainder = totalHoursDay - worked;
            // First we take from the dayly amount
            Long dayRemainder = onholdday - remainder;
            // If the remainder can be compensated during the daytime
            if (dayRemainder >= 0) {
                journey.setEsperaDiurno(dayRemainder);
                journey.setTrabalhado(totalHoursDay);
                journey.setHorasFaltosas(0L);
            } else {
                journey.setEsperaDiurno(0L);
                Long nightRemainder = onholdnight + dayRemainder;

                if (nightRemainder >= 0) {
                    journey.setEsperaNoturno(nightRemainder);
                    journey.setTrabalhado(totalHoursDay);
                    journey.setHorasFaltosas(0L);
                } else {
                    journey.setEsperaNoturno(0L);
                    journey.setTrabalhado(totalHoursDay + nightRemainder);
                    journey.setHorasFaltosas(0L);
                }
            }
        }
    }

    /**
     * Calculate DSR Over Time of the journey passed as a parameter. The "hedDSR" will be updated with the journey's "esperaDiurno" * 0.3, or 0 if null, +
     * "esperaNoturno" * 0.3, or 0 if null, + "trabalhado".
     *
     * @param journey Journey with "hedDSR" updated
     */
    private void calculateDSROvertime(Jornada journey) {
        long worked = journey.getTrabalhado();
        long onholdday = Objects.nonNull(journey.getEsperaDiurno()) ? (long) (journey.getEsperaDiurno() * 0.3) : 0;
        long onholdnight = Objects.nonNull(journey.getEsperaNoturno()) ? (long) (journey.getEsperaNoturno() * 0.3) : 0;

        journey.setHedDSR(worked + onholdday + onholdnight);
    }

    /**
     * Updates the "jornadaFim" of the journey passed as a parameter, with the "instanteEvento" of the event passed as a parameter
     * if the journey's definitive end has not been reached. Then if toggles the journey's definitive end check if the event passed
     * has it's "tipoEvento"'s ID equals to 0 (journey's end) or maintains the value if the end has been previously reached.
     *
     * @param event   Event to check.
     * @param journey Journey to update.
     */
    private void fimDefinitivo(Evento event, Jornada journey) {
        if (!fimDefinitivo) {
            journey.setJornadaFim(event.getInstanteEvento());
            journey.setLatitude(event.getLatitude());
            journey.setLongitude(event.getLongitude());
        }
        fimDefinitivo = fimDefinitivo || (Objects.equals(event.getTipoEvento().getId(), 0));
    }

    /**
     * Returns the total worked extra hours. If "workHours" not null and equals 0 returns "tempoTrabalhado", else returns "tempoTrabalho" - "workHours".
     *
     * @param tempoTrabalhado The total amount of worked hours
     * @param workHours       The total amount of work hours
     * @return The total amount of worked extra hours
     */
    public Long totalWorkedExtraHours(Long tempoTrabalhado, Long workHours) {
        if (Objects.nonNull(workHours) && Objects.equals(workHours, 0L)) {
            return tempoTrabalhado;
        }
        return tempoTrabalhado - workHours;
    }

    /**
     * Calculate the journey's worked hours. First retrieve the total amount of holiday hours on this journey, then updates the journey with the holiday's
     * extra time. If "he100porcento" is false, calculates the journey's extra hours. If the journey's extra hours is lesser than 0, updates the journey's
     * "horasFaltosas" added with the journey's extra hours times -1 and the journey's "horasExtras" with 0, else updates the journey's "horasExtras" with
     * the calculated amount. If the journey has a "jornadaFim" and there is a lunch hour on the driver's shift and the total amount of lunch made is negative,
     * updates the journey's "descontoAlmoco" else if "he100porcento" is false, updates the journey's "horasExtras". If the journey's allowance hours is
     * greater than 0, updates the journey's "horasAbonadas" with it, then if the allowance hours subtracted with the journey's "horasFaltosas" if greater
     * than 0, updates the journey's "horasFaltosas" with the difference or else with 0. If the journey's lunch hours allowance is greater than 0,
     * updates the journey's "horasAbonadas" summed with the journey's lunch hour allowance or else with 0. If the journey has waiting hours,
     * updates the journey's "esperaDiurno" and "esperaNoturno" with it.
     *
     * @param journey          Journey to calculate
     * @param stateTypeSummary State Type Summary
     * @param workHours        Work hours
     */
    private void calculateWorkedHours(Jornada journey, HashMap<String, Resumo> stateTypeSummary, Long workHours) {
        DiurnoNoturno vacationTime = this.feriadosService.pegaFeriadoTime(journey.getMotoristasId(), journey.getJornadaInicio(), journey.getJornadaFim());
        calculateVacationExtraTime(journey, vacationTime, stateTypeSummary);
        //TODO: olhar tolerâncias
        journey.setHorasFaltosas(0L);

        if (!he100porcento) {
            Long tempoTotalJornada = totalWorkedExtraHours(totalWorkedHours(stateTypeSummary, Arrays.asList(new String[]{"Ocioso", "Deslocamento"})), workHours);

            if (tempoTotalJornada < 0) {
                journey.setHorasFaltosas(journey.getHorasFaltosas() + (tempoTotalJornada * (-1)));
                journey.setHorasExtras(0L);
            } else {
                //Abono não entra em hora extra
                journey.setHorasExtras(tempoTotalJornada);
            }
        }
        //ATM 2041112
        if (Objects.nonNull(journey.getJornadaFim())) {
            Long lunchHours = this.shiftCalculusService.lunchHoursPerDate(journey.getMotoristasId().getTurnosId(), journey.getJornadaInicio());
            if (Objects.nonNull(lunchHours) && lunchHours > 0) {
                Long hours = totalIntraJourneyHours(totalLunchHours(stateTypeSummary, journey), journey, stateTypeSummary);
                if (hours < 0) {
                    hours *= -1;
                    journey.setDescontoAlmoco(journey.getDescontoAlmoco() + hours);
                } else if (!he100porcento) {
                    if (journey.getDescontoAlmoco() != null && journey.getDescontoAlmoco() > 0) {
                        long calculo = hours - journey.getDescontoAlmoco();
                        if (calculo > 0) {
                            journey.setHorasExtras(journey.getHorasExtras() + calculo);
                        }
                    } else {
                        journey.setHorasExtras(journey.getHorasExtras() + hours);
                    }
                }
            }
        }
        //Aplica correção do abono normal
        Long abono = totalWorkedHours(journey.getResumoEstados(), Arrays.asList(new String[]{"Abono"}));
        if (abono > 0) {
            Long tmpFaltosa = journey.getHorasFaltosas() - abono;

            journey.setHorasAbonadas(abono);

            if (tmpFaltosa > 0) {
                journey.setHorasFaltosas(tmpFaltosa);
            } else {
                journey.setHorasFaltosas(new Long(0));
            }
        }
        //Aplica correção do abono refeicao
        abono = totalWorkedHours(journey.getResumoEstados(), Arrays.asList(new String[]{"Abono Refeição"}));
        if (abono > 0) {
            Long tmpFaltosa = journey.getDescontoAlmoco() - abono;

            Long abonadas = journey.getHorasAbonadas();

            if (abonadas == null) {
                abonadas = 0L;
            }

            journey.setHorasAbonadas(abonadas + abono);

            if (tmpFaltosa > 0) {
                journey.setDescontoAlmoco(tmpFaltosa);
            } else {
                journey.setDescontoAlmoco(new Long(0));
            }
        }
        //ATM 2041111
        Resumo espera = stateTypeSummary.get("Espera");
        if (espera != null) {
            journey.setEsperaDiurno(espera.getSomatorioDiurno());
            journey.setEsperaNoturno(espera.getSomatorioNoturno());
        }
    }

    /**
     * Returns the total amount of lunch hours if the lunch event starts at or after the journey's "jornadaInicio" plus 2 hours and ends
     * before the "jornadaFim" plus 2 hours.
     *
     * @param stateTypeSummary State Type Summary
     * @param journey          Journey to analyze
     * @return The total amount of lunch hours
     */
    private boolean habilitarLimiteRefeicao = true;

    public Long totalLunchHours(HashMap<String, Resumo> stateTypeSummary, Jornada journey) {
        Resumo alimentacao = stateTypeSummary.get("Alimentação");
        if (journey.getEmpresaId() != null && journey.getEmpresaId().getParametro() != null && journey.getEmpresaId().getParametro().getHabilitarLimiteRefeicao() != null) {
            habilitarLimiteRefeicao = journey.getEmpresaId().getParametro().getHabilitarLimiteRefeicao();
        }
        if (Objects.nonNull(alimentacao)) {
            long jrndBegTime = journey.getJornadaInicio().getTime();
            long jrndEndTime = journey.getJornadaFim().getTime();

            return alimentacao.getLista().stream()
                    .filter(e -> {
                        long begin = e.getInstanteEvento().getTime();
                        long end = 0;

                        if (e.getFimEvento() != null) {
                            end = e.getFimEvento().getTime();
                        }
                        if (habilitarLimiteRefeicao) {
                            return (begin >= (jrndBegTime + TimeHelper.DUAS_HORAS)) && (jrndEndTime > (end + TimeHelper.DUAS_HORAS));
                        } else {
                            return true;
                        }

                    })
                    .map(e -> e.getDayNight().getTempoDiurno() + e.getDayNight().getTempoNoturno())
                    .reduce(new Long(0), Long::sum);
        }

        return 0L;
    }

    /**
     * Returns the total amount of lunch hours if the lunch event starts after the journey's "jornadaInicio" plus 2 hours and ends
     * after the "jornadaFim" plus 2 hours.
     *
     * @param stateTypeSummary State Type Summary
     * @param journey          Journey to analyze
     * @return The total amount of lunch hours
     */
    public Long totalUnusedLunchHours(HashMap<String, Resumo> stateTypeSummary, Jornada journey) {
        Resumo alimentacao = stateTypeSummary.get("Alimentação");

        if (Objects.nonNull(alimentacao)) {
            long jrndBegTime = journey.getJornadaInicio().getTime();
            long jrndEndTime = journey.getJornadaFim().getTime();

            return alimentacao.getLista().stream()
                    .filter(e -> {
                        long begin = e.getInstanteEvento().getTime();
                        long end = 0;

                        if (e.getFimEvento() != null) {
                            end = e.getFimEvento().getTime();
                        }

                        return (begin > (jrndBegTime + TimeHelper.DUAS_HORAS)) && (jrndEndTime < (end + TimeHelper.DUAS_HORAS));
                    })
                    .map(e -> e.getDayNight().getTempoDiurno() + e.getDayNight().getTempoNoturno())
                    .reduce(new Long(0), Long::sum);
        }

        return 0L;
    }

    /**
     * Updates the journey's "horasExtras", "horasExtrasDiurnas" and the nightly extra time hours if the driver spent 5h30min of
     * continuous driving without a ten minutes or more break.
     *
     * @param stateSummary State Summary
     * @param journey      Journey to analyze
     * @param workHours    Work hours
     */
    private void calculateExtraTimeContinuousDriving(HashMap<String, Resumo> stateSummary, Jornada journey, Long workHours) {
        Resumo driving = stateSummary.get("Em Direção");

        long drivingHours = 0;
        long lastEnding = 0;

        if (Objects.nonNull(driving)) {
            for (Evento event : driving.getLista()) {
                long daytime = event.getDayNight().getTempoDiurno();
                long night = event.getDayNight().getTempoNoturno();
                long h5m30 = (TimeHelper.CINCO_HORAS + TimeHelper.TRINTA_MINUTOS);

                if (lastEnding > 0) {
                    long rest = event.getInstanteEvento().getTime() - lastEnding;
                    if (rest >= TimeHelper.DEZ_MINUTOS) {
                        drivingHours = 0;
                    }
                }

                drivingHours += daytime + night;

                if (drivingHours > (h5m30)) {
                    if (night > 0) {
                        this.calculateNocturnalExtraTime(journey, TimeHelper.TRINTA_MINUTOS, workHours);
                    } else {
                        journey.setHorasExtrasDiurnas(journey.getHorasExtrasDiurnas() + TimeHelper.TRINTA_MINUTOS);
                    }
                    /**ATM: 2041043, TODO: depois colocar HED e HEN na tela de consulta
                     *
                     *
                     */
                    journey.setHorasExtras(journey.getHorasExtras() + TimeHelper.TRINTA_MINUTOS);

                    drivingHours = 0;
                }

                Date endEvent = event.getFimEvento();
                if (Objects.nonNull(endEvent)) {
                    lastEnding = endEvent.getTime();
                }// else{} // TODO: o que fazer se esse cara for nulo?
            }
        }
    }

    /**
     * Calculates the lunch extra time hours. If the jorney's "jornadaInicio" hour is greater than 22 hours,
     * updates "henIntraJornada" with the "alimentacao" passed as a parameter else updates "hedIntraJornada"
     * with it.
     *
     * @param alimentacao Amount of lunch hours
     * @param journey     Journey to update
     */
    private void calculateLunchExtraTime(long alimentacao, Jornada journey) {
        Date d0 = TimeHelper.getDate000000(journey.getJornadaInicio());
        long horaInicial = journey.getJornadaInicio().getTime() - d0.getTime();

        if ((horaInicial + TimeHelper.DUAS_HORAS) > TimeHelper.VINTEDUAS_HORAS) {
            journey.setHenIntraJornada(alimentacao);
        } else {
            journey.setHedIntraJornada(alimentacao);
        }
    }

    /**
     * Calculates the total Intra Journey's hours. If the "lunchHours" is null, returns 0. If the journey's no lunch hours is
     * lesser than the minimal amount of hours (15 minutes between 4 and 6 hours worked, 1 hour if greater than 6 or the amount
     * defined by the driver's shift if greater) calculates the lunch hour extra time, respecting the driver's shift's tolerance's
     * "horaExtraAlmoco", and if the total unused lunch hours is greater than the minimum amount, respecting the driver's shift's
     * tolerance's "atrasoAlmoco" updates the journey's "descontoAlmoco" with its value summed with the difference. If the journey's
     * no lunch hour is greater, and the amount of lunch hours is greater than 2 hours, updates the journey with the lunch extra time.
     *
     * @param lunchHours       Amount of lunch hours in the journey
     * @param journey          Journey to update
     * @param stateTypeSummary State Type Summary
     * @return The total amount of lunch hours
     */
    private Long totalIntraJourneyHours(Long lunchHours, Jornada journey, HashMap<String, Resumo> stateTypeSummary) {
        Long shiftLunchHours = this.shiftCalculusService.lunchHoursPerDate(journey.getMotoristasId().getTurnosId(), journey.getJornadaInicio());
        Tolerancias tolerancias = journey.getMotoristasId().getTurnosId().getToleranciasId();
        Long noLunchHours = totalNoLunchHours(stateTypeSummary);
        Long unusedLunchHours = totalUnusedLunchHours(stateTypeSummary, journey);

        if (lunchHours == null) {
            return new Long(0);
        }

        long minHours = 0;
        if ((noLunchHours > TimeHelper.QUATRO_HORAS) && (noLunchHours <= TimeHelper.SEIS_HORAS)) {
            minHours = TimeHelper.QUINZE_MINUTOS;
        } else if (noLunchHours > TimeHelper.SEIS_HORAS) {
            minHours = TimeHelper.UMA_HORA;
            if (minHours < shiftLunchHours) {
                minHours = shiftLunchHours;
            }
        }

        long lunch = minHours - lunchHours;
        if (lunch > 0) {
            if (lunch <= tolerancias.getHoraExtraAlmoco()) {
                lunch = 0;
            }

            this.calculateLunchExtraTime(lunch, journey);
            long hours = unusedLunchHours - minHours;
            if (hours > tolerancias.getAtrasoAlmoco()) {
                journey.setDescontoAlmoco(journey.getDescontoAlmoco() + hours);
            }
        } else {
            if ((lunch * -1) <= tolerancias.getAtrasoAlmoco()) { // inverte o sinal,  caso contrário será sempre menor pois é negativo;
                lunch = 0;
            }

            if (lunchHours > TimeHelper.DUAS_HORAS) {
                long heAlimentacao = lunchHours - TimeHelper.DUAS_HORAS; // o que passou de 2h é HE normal
                this.calculateLunchExtraTime(heAlimentacao, journey);
            }
        }

        return lunch;
    }

    /**
     * Returns the total hours that aren't lunch related, i.e. "Ocioso", "Deslocamento" and "Espera"
     *
     * @param stateTypeSummary State Type Summary
     * @return Total amount of hours
     */
    private Long totalNoLunchHours(HashMap<String, Resumo> stateTypeSummary) {
        Long result = this.totalWorkedHours(stateTypeSummary, Arrays.asList(new String[]{"Ocioso", "Deslocamento", "Espera"}));
        return result;
    }

    /**
     * Returns the sum of the "stateTypeSummary"'s "somatorio" filter by the "keys"
     *
     * @param stateTypeSummary State Type Summary
     * @param keys             Keys to filter
     * @return Sum of "stateTypeSummary"'s "somatorio"
     */
    public Long totalWorkedHours(Map<String, Resumo> stateTypeSummary, List<String> keys) {
        return stateTypeSummary.entrySet().stream()
                .filter(e -> keys.contains(e.getKey()))
                .map(e -> e.getValue().getSomatorio())
                .reduce(new Long(0), Long::sum);
    }

    /**
     * Adds the event's daily worked hours to the event's journey's "trabalhado" then checks for extra time. If there is extra time and
     * the journey already has "horasExtrasNoturnas", the extra time is updated as night hours and the method exits. If there isn't nightly
     * extra time and the amount of extra time is lesser than the worked hours in the event, the method overwrites the value with the extra time.
     * Then the event's journey's "horasExtrasDiurnas" is updated by summing the previous value with the event's worked hours.
     *
     * @param event     Event to analyze
     * @param workHours Total amount of work hours
     */
    private void calculateDayTimeWork(Evento event, Long workHours) {

        Long workedDay = event.getDayNight().getTempoDiurno();

        event.getJornadaId().setTrabalhado(event.getJornadaId().getTrabalhado() + workedDay);
        if (this.isExtraTime(event.getJornadaId(), workHours)) {
            //se ja tem HENoturna, é tudo noturno
            if (event.getJornadaId().getHorasExtrasNoturnas() > 0) {
                this.calculateNocturnalExtraTime(event.getJornadaId(), workedDay, workHours);
                return;
            }

            Long extraTime = event.getJornadaId().getTrabalhado() - workHours;
            //se o intervalo for maior do que o que ultrapassou, adiciona só o que ultrapassou;
            if (workedDay > extraTime) {
                workedDay = extraTime;
            }

            Long hed = event.getJornadaId().getHorasExtrasDiurnas();
            event.getJornadaId().setHorasExtrasDiurnas(hed + workedDay);
        }
    }

    /**
     * Updates the event's journey's "trabalhado" with the nightly work hours. If there's extra time, this method will update the journey
     * with it as well.
     *
     * @param event     Event to analyze
     * @param workHours Total work hours
     */
    private void calculateNocturnalWork(Evento event, Long workHours) {
        if (workHours != null) {
            Long workedNight = event.getDayNight().getTempoNoturno();
            event.getJornadaId().setTrabalhado(event.getJornadaId().getTrabalhado() + workedNight);
            if (this.isExtraTime(event.getJornadaId(), workHours)) {
                this.calculateNocturnalExtraTime(event.getJornadaId(), workedNight, workHours);
            }
        }
    }

    /**
     * Calculates the event worked hours if the event is not an allowance.
     *
     * @param event     Event to analyze
     * @param workHours Total work hours
     */
    private void calculateWorked(Evento event, Long workHours) {
        if (this.eventCalculusService.isAllowance(event) == false) {
            calculateDayTimeWork(event, workHours);
            calculateNocturnalWork(event, workHours);
        }
    }

    /**
     * Calculates holiday's extra time. First it sums all the "Alimentação" and "Espera" day and night hours, then sets "hedFeriado" with the "diurnoNoturno"'s
     * "tempoDiurno" subtracted by the previous sum day hour and sets "henFeriado" with the "diurnoNoturno"'s "tempoNoturno" subtracted by the previous sum
     * night hour. If "hedFeriado" added with "henFeriado" is greater than 0, sets the journey's "horasExtrasDiurnas" and "horasExtrasNoturnas" with 0 and sets
     * "he100porcento" with true.
     *
     * @param journey          Journey to update
     * @param diurnoNoturno    Total amount of holiday hours
     * @param stateTypeSummary State Type Summary
     * @return If the journey's "hedFeriado" added with "henFeriado" is greater than 0
     */
    private boolean calculateVacationExtraTime(Jornada journey, DiurnoNoturno diurnoNoturno, HashMap<String, Resumo> stateTypeSummary) {
        long tmpDiurno = 0;
        long tmpNoturno = 0;
        int dias = Days.daysBetween(new DateTime(journey.getJornadaInicio()), new DateTime(journey.getJornadaFim())).getDays();
        /*
         * Quebra galho para as pernadas dos meninos!
         * Nao esta tratando feriados em 2 dias
         */
        if (diurnoNoturno.getTempoDiurno() > 0 && diurnoNoturno.getTempoNoturno() > 0 && dias > 0) {
            Resumo sumResume = sumDayNightTimeByTypeList(stateTypeSummary, Arrays.asList(new String[]{"Deslocamento", "Ocioso"}));
            if (Objects.nonNull(sumResume)) {
                tmpDiurno = sumResume.getSomatorioDiurno();
                tmpNoturno = sumResume.getSomatorioNoturno();
            }
            journey.setHedFeriado(tmpDiurno);
            journey.setHenFeriado(tmpNoturno);
            he100porcento = true;
            journey.setHorasExtrasDiurnas(0L);
            journey.setHorasExtrasNoturnas(0L);
        } else {
            boolean jornadaComecouNoFeriado = false;
            boolean jornadaTerminouNoFeriado = false;
            if (Objects.nonNull(journey.getMotoristasId().getEmpresa().getParametro())
                    && Objects.nonNull(journey.getMotoristasId().getEmpresa().getParametro().getCalcularFeriado())
                    && journey.getMotoristasId().getEmpresa().getParametro().getCalcularFeriado()) {
                List<Feriados> listaDeFeriados = feriadosService.getEventoEmFeriado(journey.getJornadaInicio(), journey.getJornadaFim(), journey.getMotoristasId());
                if (Objects.nonNull(listaDeFeriados) && !listaDeFeriados.isEmpty()) {
                    Feriados feriado = listaDeFeriados.get(0);
                    jornadaComecouNoFeriado = TimeHelper.isSameDay(feriado.getInicioFeriado(), journey.getJornadaInicio());
                    jornadaTerminouNoFeriado = TimeHelper.isSameDay(journey.getJornadaFim(), feriado.getFimFeriado());

                }
            }

            if (!jornadaComecouNoFeriado && jornadaTerminouNoFeriado) {
                Resumo sumResume = sumDayNightTimeByTypeList(stateTypeSummary, Arrays.asList(new String[]{"Alimentação", "Espera"}));
                journey.setHedFeriado(0l);
                journey.setHenFeriado(0l);
                he100porcento = true;
                return true;
            } else {
                if (jornadaComecouNoFeriado && !jornadaTerminouNoFeriado) {
                    Long trabalhado = journey.getTrabalhado();
                    if (Objects.nonNull(journey.getAdicionalNoturno()) && journey.getAdicionalNoturno() > 0) {
                        journey.setHedFeriado(trabalhado - journey.getAdicionalNoturno());
                        journey.setHenFeriado(journey.getAdicionalNoturno());
                    } else {
                        journey.setHedFeriado(trabalhado);
                        journey.setHenFeriado(0l);
                    }

                    he100porcento = true;
                    journey.setHorasExtrasDiurnas(0L);
                    journey.setHorasExtrasNoturnas(0L);
                    return true;
                } else {
                    //ATM 2041111
                    Resumo sumResume = sumDayNightTimeByTypeList(stateTypeSummary, Arrays.asList(new String[]{"Alimentação", "Espera"}));
                    if (Objects.nonNull(sumResume)) {
                        tmpDiurno = sumResume.getSomatorioDiurno();
                        tmpNoturno = sumResume.getSomatorioNoturno();
                    }

                    journey.setHedFeriado(diurnoNoturno.getTempoDiurno() - tmpDiurno);
                    journey.setHenFeriado(diurnoNoturno.getTempoNoturno() - tmpNoturno);

                    Long tmpHeFeriado = journey.getHedFeriado() + journey.getHenFeriado();
                    if (tmpHeFeriado > 0) {
                        he100porcento = true;
                        journey.setHorasExtrasDiurnas(0L);
                        journey.setHorasExtrasNoturnas(0L);

                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Updates the journey with the event passed as a parameter. If the event is not an allowance, adds to the event's journey's "adicionalNoturno"
     * the event's "dayNight"'s "tempoNoturno".
     *
     * @param event     Event to analyze
     * @param workHours Total work hours
     */
    private void calculateJourney(Evento event, Long workHours) {
        if (this.eventCalculusService.isPaid(event)) {
            calculateWorked(event, workHours);
            /** ATM 2041135
             *
             */
            if (!this.eventCalculusService.isAllowance(event)) {
                event.getJornadaId().setAdicionalNoturno(event.getJornadaId().getAdicionalNoturno() + event.getDayNight().getTempoNoturno());
            }
        }
    }

    /**
     * Define incones e cores para cada evento na grade
     *
     * @param label
     * @param color
     * @param icon
     * @param destination
     * @param event
     */
    private void addSummary(String label, String color, String icon, HashMap<String, Resumo> destination, Evento event) {
        Resumo tmpResumo;
        if (destination.containsKey(label)) {
            tmpResumo = destination.get(label);
        } else {
            tmpResumo = new Resumo();
            tmpResumo.setColor(color);
            tmpResumo.setIcon(icon);
        }

        tmpResumo.addToList(event);
        destination.put(label, tmpResumo);

    }

    /**
     * Initialize the journey passed as a parameter, by assigning zero to its hour counters.
     *
     * @param journey Journey to initialize.
     */
    private void initCounters(Jornada journey) {
        journey.setTrabalhado(0L);
        journey.setHorasExtras(0L);
        journey.setHorasExtrasDiurnas(0L);
        journey.setHorasExtrasNoturnas(0L);
        journey.setAdicionalNoturno(0L);
        journey.setHedIntraJornada(0L);
        journey.setHenIntraJornada(0L);
        journey.setHedFeriado(0L);
        journey.setHenFeriado(0L);
        journey.setHedDSR(0L);
        journey.setHenDSR(0L);
        journey.setDescontoAlmoco(0L);

        fimDefinitivo = false;
        he100porcento = false;
    }

    /**
     * Updates the journey's "horasExtrasNoturnas" with the nightly extra time. If the "nocturnalExtraTime" is greater
     * than the difference between the journey's "trabalhado" and "workHour", "nocturnalExtraTime" is overwritten with it.
     * Then the journey's "horasExtrasNoturnas" is updated by summing "nocturnalExtraTime" with the previous value.
     *
     * @param journey            Journey to analyze
     * @param nocturnalExtraTime Nightly extra time hours
     * @param workHour           Work hours
     */
    public void calculateNocturnalExtraTime(Jornada journey, Long nocturnalExtraTime, Long workHour) {
        Long extraTime = journey.getTrabalhado() - workHour;

        if (nocturnalExtraTime > extraTime) {
            nocturnalExtraTime = extraTime;
        }

        journey.setHorasExtrasNoturnas(journey.getHorasExtrasNoturnas() + nocturnalExtraTime);
    }

    /**
     * Returns with the journey's "trabalhado" is greater than the "workHours" passed as parameter
     *
     * @param journey   Journey to analyze
     * @param workHours Total work hours
     * @return If journey's "trabalhado" is greater than the parameter
     */
    private boolean isExtraTime(Jornada journey, Long workHours) {
        if (workHours == null) {
            return true;
        }

        return journey.getTrabalhado() > workHours;
    }

    /**
     * Filters the "stateTypeSummary" based on the "keys" passed, then adds the "somatorioDiurno" and "somatorioNoturno" fields and
     * returns a summary with the sum.
     *
     * @param stateTypeSummary State type Summary
     * @param keys             Keys to filter
     * @return "Resumo" with the sum
     */
    private Resumo sumDayNightTimeByTypeList(Map<String, Resumo> stateTypeSummary, List<String> keys) {

        Resumo sumResumo = stateTypeSummary.entrySet().stream()
                .filter(e -> keys.contains(e.getKey()))
                .map(e -> e.getValue())
                .reduce(new Resumo(), (e1, e2) -> {
                    Resumo tmpResumo = new Resumo();
                    tmpResumo.setSomatorioDiurno(e1.getSomatorioDiurno() + e2.getSomatorioDiurno());
                    tmpResumo.setSomatorioNoturno(e1.getSomatorioNoturno() + e2.getSomatorioNoturno());
                    return tmpResumo;
                });

        return sumResumo;
    }

    /**
     * Total de horas que podem ser abonadas?
     *
     * @param stateTypeSummary
     * @param keys
     * @param motivoAbono
     * @return
     */
    public Long totalAllowanceHours(Map<String, Resumo> stateTypeSummary, List<String> keys, Integer motivoAbono) {

        Long result = 0L;

        if (motivoAbono == null) {
            result = stateTypeSummary.entrySet().stream()
                    .filter(e -> keys.contains(e.getKey()))
                    .map(e -> e.getValue().getSomatorio())
                    .reduce(new Long(0), Long::sum);
        } else {
            stateTypeSummary.entrySet().stream()
                    .filter(e -> keys.contains(e.getKey()))
                    .map(e -> e.getValue().getSomatorio())
                    .reduce(new Long(0), Long::sum);
        }

        return result;
    }

    /**
     * Método que expõe as horas trabalhadas calculas para jornada.
     *
     * @return
     * @author Vinicius Prado.
     */
    public Long getWorkHours() {
        return workHours;
    }
}
