package br.com.cmctransportes.cjm.services.nonconformity;

import br.com.cmctransportes.cjm.domain.entities.*;
import br.com.cmctransportes.cjm.domain.entities.enums.EVENT_TYPE;
import br.com.cmctransportes.cjm.domain.entities.enums.JOURNEY_TYPE;
import br.com.cmctransportes.cjm.domain.entities.vo.DSRAnalysisVO;
import br.com.cmctransportes.cjm.domain.entities.vo.NonconformityVO;
import br.com.cmctransportes.cjm.services.nonconformity.base.BaseNonconformityService;
import br.com.cmctransportes.cjm.utils.ChartReflectionUtils;
import br.com.cmctransportes.cjm.utils.NonconformityFactory;
import br.com.cmctransportes.cjm.utils.TimeHelper;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * @author William Leite
 */
@Service
public class PaidWeeklyRestNonconformityService extends BaseNonconformityService<DSRAnalysisVO> {

    private List<Date> retrieveSundays(Date startDate, Date finalDate) {
        List<Date> datesInRange = TimeHelper.getDatesBetween(startDate, finalDate);
        return datesInRange.stream().filter(TimeHelper::isSunday).collect(Collectors.toList());
    }

    private void incrementDSR(AtomicInteger dsrInPeriod, long neededGap) {
        this.print("Current DSR Count: [%s]; Gap: [%s]", dsrInPeriod.get(), neededGap);
        long remainingGap = neededGap;
        while (remainingGap >= TimeHelper.hours(24) - TimeHelper.seconds(1)) {
            dsrInPeriod.incrementAndGet();
            remainingGap -= TimeHelper.hours(24);
        }
        this.print("Updated DSR Count: [%s]; Remaining Gap: [%s]", dsrInPeriod.get(), remainingGap);
    }

    private long neededGap(Integer dsrInPeriod, Integer weeksPassed, Jornada previousJourney) {
        if (weeksPassed == 0 || weeksPassed >= dsrInPeriod) {
            int neededDSR = weeksPassed + 1 - dsrInPeriod;
            if (neededDSR > 0) {
                // (DSR * 24H) + 11 hours of Interjourney
                long interjourney = TimeHelper.hours(11);
                if (Objects.isNull(previousJourney) || previousJourney.getType() == JOURNEY_TYPE.FERIADO || previousJourney.getType() == JOURNEY_TYPE.FOLGA) {
                    interjourney = 0L;
                }
                return (neededDSR * TimeHelper.hours(24)) + interjourney;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    private Map<Integer, Integer> calculateMissingDaysPerWeek(List<Jornada> journeys, List<Date> days, Turnos shift) {
        List<Date> missingJourneys = journeys.stream().filter(j -> j.getType() == JOURNEY_TYPE.FALTA).map(Jornada::getJornadaInicio).collect(Collectors.toList());
        Map<Integer, Integer> missingDaysPerWeek = new HashMap<>(0);

        final AtomicInteger weeksPassed = new AtomicInteger(-1);
        final Calendar calendar = Calendar.getInstance();

        // Get all the missing days in the weeks in the period
        days.stream().forEach(d -> {
            calendar.setTime(d);

            // Check if a new week has begun
            boolean isNewWeek = calendar.get(Calendar.DAY_OF_WEEK) == (shift.getWeekTurnDay() + 1);
            if (isNewWeek) {
                weeksPassed.incrementAndGet();
                this.print("[calculateMissingDaysPerWeek] New Week: [%s]", weeksPassed.get());
            }

            if (!missingDaysPerWeek.containsKey(weeksPassed.get())) {
                missingDaysPerWeek.put(weeksPassed.get(), 0);
            }

            if (missingJourneys.stream().anyMatch(j -> TimeHelper.isSameDay(d, j))) {
                missingDaysPerWeek.put(weeksPassed.get(), missingDaysPerWeek.get(weeksPassed.get()) + 1);
            }
        });

        return missingDaysPerWeek;
    }

    public Date calculateHolidayEnd(Jornada journey) {
        Turnos shift = journey.getMotoristasId().getTurnosId();
        Date end = TimeHelper.getDate235959(journey.getJornadaInicio());

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(journey.getJornadaInicio());

        ChartReflectionUtils reflectionUtils = new ChartReflectionUtils();

        switch (shift.getTipo()) {
            case 2:
                int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) + 1;
                end = new Date(journey.getJornadaInicio().getTime() + (long) reflectionUtils.invokeGetter(shift, String.format("tempoDiario0%s", dayOfWeek)));
                break;
            default:
                break;
        }

        return end;
    }

    @Override
    public List<NonconformityVO> process(Date startDate, Date finalDate, List<Jornada> journeys, boolean save) {
        final List<NonconformityVO> result = new ArrayList<>(0);

        final List<Date> days = TimeHelper.getDatesBetween(startDate, finalDate);

        final Map<Date, Long> dsrLackingCompensation = new HashMap<>(0);
        final AtomicLong minimumDSRCompensation = new AtomicLong(0);
        final AtomicReference<Date> minimumDSRCompensationDate = new AtomicReference<>(null);
        final AtomicLong minimumDSRTime = new AtomicLong(0);

        final AtomicBoolean hasSundayDSR = new AtomicBoolean(false);
        final AtomicInteger dsrCount = new AtomicInteger(0);
        final AtomicInteger previousDayOfWeek = new AtomicInteger(0);
        final AtomicInteger weeksPassed = new AtomicInteger(0);
        final AtomicInteger workedJourneys = new AtomicInteger(0);
        final AtomicLong leastWorkedSundayHours = new AtomicLong(0);
        final AtomicReference<Date> leastWorkedSunday = new AtomicReference<>(null);
        final AtomicReference<Jornada> atomicPreviousJourney = new AtomicReference<>(null);
        final Calendar calendar = Calendar.getInstance();
        final List<Date> availableSundays = this.retrieveSundays(startDate, finalDate);
        final Map<Date, Long> probableDSRMap = new HashMap<>(0);
        final List<Integer> processedJourneys = new ArrayList<>(0);
        final Map<Integer, Boolean> dsrWeek = new HashMap<>(0);

        final Turnos shift = journeys.stream().findFirst().get().getMotoristasId().getTurnosId();
//        final Map<Integer, Integer> missingDaysPerWeek = this.calculateMissingDaysPerWeek(journeys, days, shift);

        // Gets the company from the first journey in the list (works as long as a driver has only one company)
        final Empresas defaultCompany = journeys.stream().findFirst().get().getEmpresaId();
        final List<Jornada> filteredJourneys = journeys.stream().filter(j -> j.getType() != JOURNEY_TYPE.FALTA).collect(Collectors.toList());

        dsrWeek.put(0, false);

        days.stream().forEach(d -> {
            this.print(String.format("Days Loop Start [%s]", d));

            Jornada previousJourney = atomicPreviousJourney.get();
            calendar.setTime(d);

            if(shift.getTipo() == 0){
                shift.setWeekTurnDay(1);
            }

            // Check if a new week has begun
            boolean isNewWeek = calendar.get(Calendar.DAY_OF_WEEK) == (shift.getWeekTurnDay() + 1) && Objects.nonNull(previousJourney);
            if (isNewWeek) {
                weeksPassed.incrementAndGet();
                dsrWeek.put(weeksPassed.get(), false);
                this.print("New Week: [%s]", weeksPassed.get());
            }

            // Retrieve a journey that starts in the current day
            Optional<Jornada> currentJourney = filteredJourneys.stream().filter(j -> {
                Date start = j.getJornadaInicio();

                if (j.getType() == JOURNEY_TYPE.DSR) {
                    // Retrieve the DSR Start Event in the journey
                    Evento evento = j.getEventos().stream().filter(e -> e.getTipoEvento().getId() == EVENT_TYPE.DSR.getStartCode()).findFirst().orElse(null);
                    start = evento.getInstanteEvento();
                }

                Date end = Objects.nonNull(j.getJornadaFim()) ? j.getJornadaFim() : start;
                List<Date> dates = TimeHelper.getDatesBetween(start, end);
                return dates.stream().anyMatch(dt -> TimeHelper.isSameDay(dt, d));
            }).sorted((j1, j2) -> j2.getJornadaInicio().compareTo(j1.getJornadaInicio())).findFirst();

            Calendar nextJourneyCalendar = Calendar.getInstance();
            nextJourneyCalendar.setTime(d);
            nextJourneyCalendar.add(Calendar.DATE, 1);

            Date dNextJourney = nextJourneyCalendar.getTime();

            Optional<Jornada> nextJourney = filteredJourneys.stream().filter(j -> {
                Date end = Objects.nonNull(j.getJornadaFim()) ? j.getJornadaFim() : j.getJornadaInicio();
                List<Date> dates = TimeHelper.getDatesBetween(j.getJornadaInicio(), end);
                return dates.stream().anyMatch(dt -> TimeHelper.isSameDay(dt, dNextJourney));
            }).sorted((j1, j2) -> j2.getJornadaInicio().compareTo(j1.getJornadaInicio())).findFirst();

            long gap = 0L;

            // If there is a journey
            if (currentJourney.isPresent()) {
                Jornada journey = currentJourney.get();

                if (!processedJourneys.contains(journey.getId())) {
                    if (journey.getType() != JOURNEY_TYPE.FERIADO) {
                        // Calculates the gap between journeys
                        if (Objects.nonNull(previousJourney) && Objects.nonNull(previousJourney.getJornadaFim())) {
                            gap = this.calculateGap(journey, previousJourney, nextJourney.orElse(null));
                        }

                        // If an incomplete DSR compensation period was set
                        if (Objects.nonNull(minimumDSRCompensationDate.get())) {
                            // Check if 6 days have yet to pass
                            if ((minimumDSRCompensationDate.get().getTime() + TimeHelper.days(6)) > journey.getJornadaInicio().getTime()) {
                                // Remove the 11 hours of normal Interjourney
                                long extraGap = gap - TimeHelper.hours(11);
                                // If there are any extra hours
                                if (extraGap > 0) {
                                    // Adds to the compensation total, and checks if further compensation is needed
                                    if (minimumDSRCompensation.addAndGet(extraGap) + minimumDSRTime.get() >= TimeHelper.hours(35)) {
                                        minimumDSRCompensationDate.set(null);
                                        minimumDSRCompensation.set(0L);
                                        minimumDSRTime.set(0L);
                                    }
                                }
                            } else {
                                // 6 days have come and gone
                                dsrLackingCompensation.put(minimumDSRCompensationDate.getAndSet(null), minimumDSRCompensation.getAndSet(0L));
                                minimumDSRTime.set(0L);
                            }
                        }

                        // If the current journey is a DSR
                        if (journey.getType() == JOURNEY_TYPE.DSR) {
                            // Increments the DSR count according to the DSR gap inside the journey
                            this.incrementDSR(dsrCount, this.calculateDSRGap(journey));

                            // Retrieve the DSR Start Event in the journey
                            Evento evento = journey.getEventos().stream().filter(e -> e.getTipoEvento().getId() == EVENT_TYPE.DSR.getStartCode()).findFirst().orElse(null);

                            // Check if the DSR starts on a Sunday
                            if (Objects.nonNull(evento) && TimeHelper.isSunday(evento.getInstanteEvento())) {
                                this.print("DSR Journey on a Sunday!");
                                hasSundayDSR.set(true);
                                availableSundays.remove(TimeHelper.getDate000000(evento.getInstanteEvento()));
                            }

                            Date end = Objects.nonNull(journey.getJornadaFim()) ? journey.getJornadaFim() : journey.getJornadaInicio();
                            long elapsed = this.calculateGap(end, journey.getJornadaInicio());
                            if (elapsed < TimeHelper.hours(35) && elapsed >= TimeHelper.hours(30)) {
                                minimumDSRCompensationDate.set(journey.getJornadaInicio());
                                minimumDSRTime.set(elapsed);
                            }

                            dsrWeek.put(weeksPassed.get(), true);
                            workedJourneys.set((int) (workedJourneys.get() - defaultCompany.getWorkedDaysDSR()));
                        } else if (Objects.equals(journey.getDsrHE(), false) || Objects.isNull(journey.getDsrHE())) {
//                            Boolean created = this.calculateProbableDSR(defaultCompany, missingDaysPerWeek, weeksPassed, workedJourneys, dsrCount, previousJourney, startDate, d, gap, probableDSRMap, availableSundays, hasSundayDSR, journey);
//                            if (created) {
//                                dsrWeek.put(weeksPassed.get(), true);
//                                workedJourneys.set((int) (workedJourneys.get() - defaultCompany.getWorkedDaysDSR()));
//
//                                Jornada tmp = new Jornada();
//                                tmp.setJornadaFim(TimeHelper.getDate235959(d));
//                                tmp.setType(JOURNEY_TYPE.DSR);
//                                atomicPreviousJourney.set(tmp);
//                            }
                        } else if (Objects.equals(journey.getDsrHE(), true)) {
                            dsrCount.incrementAndGet();
                            dsrWeek.put(weeksPassed.get(), true);
                            workedJourneys.set((int) (workedJourneys.get() - defaultCompany.getWorkedDaysDSR()));
                        }

                        if (journey.getType() == JOURNEY_TYPE.JORNADA) {
                            // Increments worked journeys if the current journey was a valid one
                            workedJourneys.incrementAndGet();

                            // If sunday
                            if (TimeHelper.isSunday(journey.getJornadaInicio())) {
                                // And if liable, update least worked sunday
                                if (journey.getTrabalhado() < leastWorkedSundayHours.get()) {
                                    leastWorkedSundayHours.set(journey.getTrabalhado());
                                    leastWorkedSunday.set(journey.getJornadaInicio());
                                }
                            }
                        }

                        atomicPreviousJourney.set(journey);
                    } else if (calendar.get(Calendar.DAY_OF_WEEK) == 0) {
                        // Holiday on a Sunday (calculate normally)
//                        if (Objects.nonNull(previousJourney)) {
//                            gap = this.calculateGap(TimeHelper.getDate235959(d), previousJourney.getJornadaFim());
//                        } else {
//                            gap = this.calculateGap(TimeHelper.getDate235959(d), TimeHelper.getDate000000(startDate));
//                        }
//                        Boolean created = this.calculateProbableDSR(defaultCompany, missingDaysPerWeek, weeksPassed, workedJourneys, dsrCount, previousJourney, startDate, d, gap, probableDSRMap, availableSundays, hasSundayDSR, journey);
//                        if (created) {
//                            dsrWeek.put(weeksPassed.get(), true);
//                            workedJourneys.set((int) (workedJourneys.get() - defaultCompany.getWorkedDaysDSR()));
//
//                            Jornada tmp = new Jornada();
//                            tmp.setJornadaFim(TimeHelper.getDate235959(d));
//                            tmp.setType(JOURNEY_TYPE.DSR);
//                            atomicPreviousJourney.set(tmp);
//                        }
                    } else {
                        // Holiday on any other day (do nothing, only set it as the last journey looked)
                        // But for the interjourney gap calculation, we must alter the journey to include the driver's shift
                        // And be extra careful not to break the previous journey's interjourney

//                    Date start = d;
//                    if (Objects.nonNull(previousJourney)) {
//                        start = new Date(previousJourney.getJornadaFim().getTime() + TimeHelper.hours(11));
//                    }
//                    
//                    journey.setJornadaInicio(start);
//                    journey.setJornadaFim(this.calculateHolidayEnd(journey));
                        atomicPreviousJourney.set(journey);
                    }

                    processedJourneys.add(journey.getId());
                }
            } else {
//                if (Objects.nonNull(previousJourney)) {
//                    Date end = Objects.nonNull(previousJourney.getJornadaFim()) ? previousJourney.getJornadaFim() : previousJourney.getJornadaInicio();
//                    gap = this.calculateGap(TimeHelper.getDate235959(d), end);
//                } else {
//                    gap = this.calculateGap(TimeHelper.getDate235959(d), TimeHelper.getDate000000(startDate));
//                }

//                Boolean created = this.calculateProbableDSR(defaultCompany, missingDaysPerWeek, weeksPassed, workedJourneys, dsrCount, previousJourney, startDate, d, gap, probableDSRMap, availableSundays, hasSundayDSR, null);
//                if (created) {
//                    dsrWeek.put(weeksPassed.get(), true);
//
//                    Jornada tmp = new Jornada();
//                    tmp.setJornadaFim(TimeHelper.getDate235959(d));
//                    tmp.setType(JOURNEY_TYPE.DSR);
//                    atomicPreviousJourney.set(tmp);
//                }
            }

            previousDayOfWeek.set(calendar.get(Calendar.DAY_OF_WEEK));

            this.print("Days Loop End");
        });

        // There can be periods where no work was done an a Sunday
        if (Objects.isNull(leastWorkedSunday.get())) {
            // Chose the last sunday as the least worked
            List<Date> sundays = this.retrieveSundays(startDate, finalDate);
            if (Objects.nonNull(sundays) && !sundays.isEmpty()) {
                leastWorkedSunday.set(sundays.get(sundays.size() - 1));
            }
        }

        DSRAnalysisVO analysis = new DSRAnalysisVO();
        analysis.setDsrCount(dsrCount.get());
        // Weeks start at 0, here we need the absolute amount
        analysis.setWeeksPassed(weeksPassed.get() + 1);
        analysis.setStart(startDate);
        analysis.setEnd(finalDate);
        analysis.setHasSundayDSR(hasSundayDSR.get());
        analysis.setDriver(journeys.get(0).getMotoristasId());
        analysis.setCompany(journeys.get(0).getEmpresaId());
        analysis.setProbableDSRMap(probableDSRMap);
        analysis.setAvailableSundays(availableSundays);
        analysis.setLeastWorkedSunday(leastWorkedSunday.get());
        analysis.setDsrWeek(dsrWeek);
        analysis.setDsrLackingCompensation(dsrLackingCompensation);

        result.addAll(this.analyse(analysis, save));

        return result;
    }

    private boolean calculateProbableDSR(Empresas company, final Map<Integer, Integer> missingDaysPerWeek, final AtomicInteger weeksPassed, final AtomicInteger workedJourneys,
                                         final AtomicInteger dsrCount, Jornada previousJourney, Date startDate, Date d, Long gap, final Map<Date, Long> probableDSRMap, final List<Date> availableSundays,
                                         final AtomicBoolean hasSundayDSR, Jornada currentJourney) {
        Long workedDaysDSR = company.getWorkedDaysDSR();
        Long missingDaysDSR = company.getMissingDaysDSR();
        Integer missingDaysWeek = missingDaysPerWeek.get(weeksPassed.get());

        if (Objects.isNull(missingDaysWeek)) {
            missingDaysWeek = 0;
        }

        this.print("Weeks Passed: [%s]", weeksPassed.get());
        this.print("Worked Journeys: [%s] Worked Days DSR: [%s] Missing Days DSR: [%s] Missing Days Week: [%s]", workedJourneys.get(), workedDaysDSR, missingDaysDSR, missingDaysWeek);

        // Only once the company's worked journey have passed and the number of missing days in the week is less than the company's missing days, are we liable to receive a DSR
        if (workedJourneys.get() >= workedDaysDSR && missingDaysWeek <= missingDaysDSR) {
            // Retrieves the needed gap for a possible DSR
            long neededGap = this.neededGap(dsrCount.get(), weeksPassed.get(), previousJourney);
            //long minGap = neededGap - TimeHelper.hours(5);
            this.print("Needed Gap: [%s][%s]\tWeeks Passed: [%s]\tDSR Count: [%s]\tMin Gap: [%s]", neededGap, neededGap / TimeHelper.UMA_HORA, weeksPassed.get(), dsrCount.get(), 0L);

            // If there is a neededGap and the gap is not enough for the total neededGap, but the gap has more than 30 hours, we allow a DSR for all the amount available
            // i.e. 30 to 35 hours (DSR in need of compensation)
            if (neededGap > 0 && gap < neededGap && gap >= TimeHelper.hours(30)) {
                neededGap = gap;
            }

            // If there is a neededGap and it is greater than or equals to the current gap period is liable to receive a DSR
            if (neededGap > 0 && gap >= neededGap) {
                // Calculates the best DSR start date
                Map<String, Object> probableDSR;
                if (Objects.nonNull(previousJourney) && Objects.nonNull(previousJourney.getJornadaFim())) {
                    probableDSR = this.calculateDSRStart(previousJourney.getJornadaFim(), Objects.nonNull(currentJourney) ? currentJourney.getJornadaInicio() : TimeHelper.getDate235959(d), neededGap);
                } else {
                    probableDSR = this.calculateDSRStart(TimeHelper.getDate000000(startDate), Objects.nonNull(currentJourney) ? currentJourney.getJornadaInicio() : TimeHelper.getDate235959(d), neededGap);
                }
                this.print("Probable DSR: [%s]\tSunday: [%s]", probableDSR.get("dsrStart"), probableDSR.get("sunday"));

                probableDSRMap.put((Date) probableDSR.get("dsrStart"), neededGap);

                // If there is a sunday in the DSR period, remove it from the available list
                if (Objects.nonNull(probableDSR.get("sunday"))) {
                    availableSundays.remove((Date) probableDSR.get("sunday"));
                    hasSundayDSR.set(true);
                }

                // Increments the DSR count according to the neededGap
                this.incrementDSR(dsrCount, neededGap);

                // Resets the workedJourneys for the next DSR
                workedJourneys.set(0);

                return true;
            }
        }

        return false;
    }

    private Map<String, Object> calculateDSRStart(Date start, Date end, long neededGap) {
        // Default is the start received plus a milisecond
        Date dsrStart = new Date(start.getTime() + 1);
        Date sunday = null;

        List<Date> datesInRange = TimeHelper.getDatesBetween(start, end);
        Optional<Date> optionalSunday = datesInRange.stream().filter(TimeHelper::isSunday).findFirst();

        // If there is a Sunday in the period, we must prioritize it
        if (optionalSunday.isPresent()) {
            // Sunday at 00:00:00
            Date sundayBeginning = TimeHelper.getDate000000(optionalSunday.get());
            sunday = sundayBeginning;

            // Sunday at 23:59:59
            Date sundayEnd = TimeHelper.getDate235959(optionalSunday.get());

            // If sunday starts before the start; so we assume result as the start
            if (start.getTime() > sundayBeginning.getTime()) {
                dsrStart = new Date(start.getTime() + 1);
                // If sunday ends after the end; so we assume result as the end minus the neededGap
            } else if (end.getTime() < sundayEnd.getTime()) {
                dsrStart = new Date(end.getTime() - neededGap);
                // Else, sunday fits confortably in the given range
            } else // If the beginning of Sunday plus the neededGap fits in period
                if (sundayBeginning.getTime() + neededGap <= end.getTime()) {
                    dsrStart = sundayBeginning;
                    // If the end of Sunday minus the neededGap fits in period
                } else if (sundayEnd.getTime() - neededGap >= start.getTime()) {
                    dsrStart = new Date(sundayEnd.getTime() - neededGap + 1);
                }
        }

        Map<String, Object> result = new HashMap<>(0);

//        long gap = this.calculateGap(end, start);
//        // if the period gap is lesser than the neededGap; we arrived here based on the minimum, therefore work with what is given
//        if (gap < neededGap) {
//            dsrStart = new Date(start.getTime() + 1);
//            result.put("gap", gap);
//        } else {
//            result.put("gap", neededGap);
//        }
        result.put("dsrStart", dsrStart);
        result.put("sunday", sunday);
        return result;
    }

    @Override
    protected List<NonconformityVO> analyse(DSRAnalysisVO analysis, boolean save) {
        List<NonconformityVO> result = new ArrayList<>(0);
        NonconformityFactory factory = NonconformityFactory.getInstance(analysis.getDriver());

        this.print("DSR Analysis Start");

        // Creating the presumable DSRs
        analysis.getProbableDSRMap().entrySet().stream().map(entry -> {
            Long gap = entry.getValue();
            boolean is24Multiple = gap % TimeHelper.hours(24) == 0;

            // Normal DSR (current gap subtracted by 11 hours must be a multiple of 24)
            // DSR without interjouney (gap must be a multiple of 24)
            if ((gap - TimeHelper.hours(11)) % TimeHelper.hours(24) == 0 || is24Multiple) {
                this.print("Creating presumable DSR [%s][%s]", entry.getKey(), gap);

                Dsr dsr = new Dsr();
                dsr.setEmpresaId(analysis.getCompany());
                dsr.setMotorista(analysis.getDriver());
                dsr.setOperadorLancamento(new Users(-1));

                Date dsrStart, dsrEnd;

                if (is24Multiple) {
                    dsrStart = new Date(entry.getKey().getTime() + TimeHelper.seconds(1));
                    dsrEnd = new Date(dsrStart.getTime() + gap);
                } else {
                    Date inicioInterjornada = new Date(entry.getKey().getTime());
                    Date fimInterjornada = new Date(inicioInterjornada.getTime() + TimeHelper.hours(11));

                    dsr.setInicioInterjornada(inicioInterjornada);
                    dsr.setFimInterjornada(fimInterjornada);

                    dsrStart = new Date(fimInterjornada.getTime() + TimeHelper.seconds(1));
                    dsrEnd = new Date(dsrStart.getTime() + (gap - TimeHelper.hours(11)));
                }

                dsr.setInicioDSR(dsrStart);
                dsr.setFimDSR(dsrEnd);

                //result.add(factory.presumableDSR(dsr));
                return null;
            } else {
                result.add(factory.dsrWithCompensation(entry.getKey(), gap));
                return null;
            }
        }).filter(Objects::nonNull).forEach(dsr -> {
//            if (save) {
//                dsrService.save(dsr);
//            }            
        });

        // In a period we must have the same number of DSR as weeks passed
        if (analysis.getDsrCount() != analysis.getWeeksPassed()) {
//            this.print("DSR Count [%s] != Weeks Passed [%s]", analysis.getDsrCount(), analysis.getWeeksPassed());
//            result.add(factory.dsrCount(analysis.getStart(), analysis.getEnd(), analysis.getDsrCount(), analysis.getWeeksPassed()));

            analysis.getDsrWeek().entrySet().stream()
                    .filter((entry) -> !entry.getValue())
                    .forEach((entry) -> {
                        result.add(factory.dsrHE(entry.getKey(), analysis.getStart()));
                    });
        }

        // In a period we must have one DSR in a Sunday
        if (!analysis.isHasSundayDSR()) {
            this.print("No Sunday DSR in period [%s]", analysis.isHasSundayDSR());
            result.add(factory.noSundayDSR(analysis.getStart(), analysis.getEnd(), analysis.getLeastWorkedSunday()));

            // All sundays are occupied by journeys
//            if (analysis.getAvailableSundays().isEmpty()) {
//                this.print("No available sunday [%s]", analysis.getAvailableSundays().size());
//                result.add(factory.noAvailableSunday(analysis.getStart(), analysis.getEnd()));
//                // Although there are available sundays, none was able to hold a DSR
//            } else {
//                this.print("No liable sunday for DSR [%s]", analysis.getAvailableSundays().stream().map(Date::toString).collect(Collectors.joining(";")));
//                result.add(factory.noLiableSunday(analysis.getStart(), analysis.getEnd(), analysis.getAvailableSundays()));
//            }
        }

        if (Objects.nonNull(analysis.getDsrLackingCompensation()) && !analysis.getDsrLackingCompensation().isEmpty()) {
            result.addAll(analysis.getDsrLackingCompensation().entrySet().stream().map(factory::dsrLackingCompensation).collect(Collectors.toList()));
        }

        this.print("DSR Analysis End");

        return result;
    }
}
