package br.com.cmctransportes.cjm.services.nonconformity.base;

import br.com.cmctransportes.cjm.domain.entities.Evento;
import br.com.cmctransportes.cjm.domain.entities.Jornada;
import br.com.cmctransportes.cjm.domain.entities.enums.EVENT_TYPE;
import br.com.cmctransportes.cjm.domain.entities.enums.JOURNEY_TYPE;
import br.com.cmctransportes.cjm.domain.entities.vo.NonconformityVO;
import br.com.cmctransportes.cjm.utils.TimeHelper;

import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @param <V>
 * @author William Leite
 */
public abstract class BaseNonconformityService<V> {

    protected void print(String text) {
        this.print("--------------- [%s] ---------------", text);
    }

    protected void print(String pattern, Object... params) {
        System.out.println(String.format(pattern, params));
    }

    protected long calculateDSRGap(Jornada journey) {
        Long gap = 0L;

        if (Objects.nonNull(journey) && Objects.nonNull(journey.getEventos()) && !journey.getEventos().isEmpty()) {
            Evento dsrStart = journey.getEventos().stream().filter(e -> e.getTipoEvento().getId() == EVENT_TYPE.DSR.getStartCode()).findFirst().orElse(null);
            Evento dsrEnd = journey.getEventos().stream().filter(e -> e.getTipoEvento().getId() == EVENT_TYPE.DSR.getEndCode()).findFirst().orElse(null);

            if (Objects.nonNull(dsrStart) && Objects.nonNull(dsrEnd)) {
                gap = dsrEnd.getInstanteEvento().getTime() - dsrStart.getInstanteEvento().getTime();
            }
        }

        return gap;
    }

    protected long calculateGap(Date current, Date previous) {
        Long gap = current.getTime() - previous.getTime();
        this.print("[calculateGap]: [%s to %s] = [%s][%s]", previous.toString(), current.toString(), gap, gap / TimeHelper.UMA_HORA);
        return Math.abs(gap);
    }

    protected long calculateGap(Jornada current, Jornada previous, Jornada next) {
        return this.calculateGap(current, previous, next, true);
    }

    protected long calculateGap(Jornada current, Jornada previous, Jornada next, boolean simple) {
        // Calculate the gap between journeys
        Long gap = 0L;
        if (Objects.nonNull(previous) && Objects.nonNull(previous.getJornadaFim()) && Objects.nonNull(current) && Objects.nonNull(current.getJornadaFim())) {
            gap = current.getJornadaInicio().getTime() - previous.getJornadaFim().getTime();
            Date trueEnd = null;
            if (Objects.nonNull(next) && current.getType() != JOURNEY_TYPE.JORNADA) {
                if (next.getType() == JOURNEY_TYPE.FALTA) {
                    trueEnd = next.getJornadaFim();
                    gap += (next.getJornadaFim().getTime() - current.getJornadaFim().getTime());
                } else {
                    trueEnd = next.getJornadaInicio();
                    gap += (next.getJornadaInicio().getTime() - current.getJornadaFim().getTime());
                }
            }

            // If complex, we must add to the gap any interjourney event inside the current journey
            if (!simple) {
                Evento interjourneyStart = current.getEventos().stream().filter(e -> Objects.equals(e.getTipoEvento().getId(), EVENT_TYPE.INTERJORNADA.getStartCode())).findFirst().orElse(null);
                Evento interjourneyEnd = current.getEventos().stream().filter(e -> Objects.equals(e.getTipoEvento().getId(), EVENT_TYPE.INTERJORNADA.getEndCode())).findFirst().orElse(null);

                // If there is an interjourney event inside the journey, add to the gap the elapsed interjourney
                if (Objects.nonNull(interjourneyStart) && Objects.nonNull(interjourneyEnd)) {
                    // In case of overlapping journeys, reset the gap
                    if (gap < 0) {
                        gap = 0L;
                    }
                    gap += (interjourneyEnd.getInstanteEvento().getTime() - interjourneyStart.getInstanteEvento().getTime());
                }
            }
            this.print("[calculateGap]: [%s to %s] = [%s][%s]", previous.getJornadaFim().toString(), Objects.nonNull(trueEnd) ? trueEnd.toString() : current.getJornadaInicio().toString(), gap, gap / TimeHelper.UMA_HORA);
        }
        // In case of a negative gap, we request the absolute value
        return Math.abs(gap);
    }

    public abstract List<NonconformityVO> process(Date startDate, Date finalDate, List<Jornada> journeys, boolean save);

    protected abstract List<NonconformityVO> analyse(V analysis, boolean save);
}
