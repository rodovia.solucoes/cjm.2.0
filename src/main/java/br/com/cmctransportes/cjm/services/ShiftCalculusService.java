package br.com.cmctransportes.cjm.services;

import br.com.cmctransportes.cjm.domain.entities.Turnos;
import br.com.cmctransportes.cjm.utils.TimeHelper;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Objects;
import java.util.Optional;

/**
 * @author William Leite
 * <p>
 * Classe para calcular o turno de trabalho
 */
public class ShiftCalculusService {

    /**
     * Returns the total number of work hours that must be done in a day, based on a shift and date.
     * If the shift passed as a parameter has "tipo" equals 1, the method will calculate the work hours
     * by the shift's weekday's "horarioSaida" - "horarioEntrada" - "tempoAlimentacao"; if "tipo" equals
     * 2, returns the shift's weekday's "tempoDiario"; and if "tipo" equals "3", returns "tempoDiario"
     *
     * @param shift Driver's shift.
     * @param data  Date to analyze.
     * @return The total number of work hours that must be done in a day.
     */
    public Long workHoursPerDate(Turnos shift, Date data) {
        if (Objects.isNull(data)) {
            return null;
        }
        /** Shift - Turno
         *
         * workHours - Horas trabalhadas
         */
        if (shift.getTipo() == 1 || shift.getTipo() == 2) {
            GregorianCalendar cal = new GregorianCalendar();
            cal.setTime(data);
            int weekDay = cal.get(GregorianCalendar.DAY_OF_WEEK) - 1;
            /**Vai consultar no banco de dados as horas trabalhadas se tem incidência de horas extras
             *
             */
            Long[] workHours;
            if (shift.getTipo() == 2) {
                workHours = new Long[]{
                        Optional.ofNullable(shift.getTempoDiario01()).orElse(0L),
                        Optional.ofNullable(shift.getTempoDiario02()).orElse(0L),
                        Optional.ofNullable(shift.getTempoDiario03()).orElse(0L),
                        Optional.ofNullable(shift.getTempoDiario04()).orElse(0L),
                        Optional.ofNullable(shift.getTempoDiario05()).orElse(0L),
                        Optional.ofNullable(shift.getTempoDiario06()).orElse(0L),
                        Optional.ofNullable(shift.getTempoDiario07()).orElse(0L)
                };
            } else {
                workHours = new Long[]{
                        TimeHelper.nullsafeSubtract(shift.getHorarioSaida01(), shift.getHorarioEntrada01(), shift.getTempoAlimentacao01()),
                        TimeHelper.nullsafeSubtract(shift.getHorarioSaida02(), shift.getHorarioEntrada02(), shift.getTempoAlimentacao02()),
                        TimeHelper.nullsafeSubtract(shift.getHorarioSaida03(), shift.getHorarioEntrada03(), shift.getTempoAlimentacao03()),
                        TimeHelper.nullsafeSubtract(shift.getHorarioSaida04(), shift.getHorarioEntrada04(), shift.getTempoAlimentacao04()),
                        TimeHelper.nullsafeSubtract(shift.getHorarioSaida05(), shift.getHorarioEntrada05(), shift.getTempoAlimentacao05()),
                        TimeHelper.nullsafeSubtract(shift.getHorarioSaida06(), shift.getHorarioEntrada06(), shift.getTempoAlimentacao06()),
                        TimeHelper.nullsafeSubtract(shift.getHorarioSaida07(), shift.getHorarioEntrada07(), shift.getTempoAlimentacao07())
                };
            }
            return workHours[weekDay];
        } else if (shift.getTipo() == 3) {
            return shift.getTempoDiario01();
        }else if (shift.getTipo() == 0){
            //caso esteja 12x36 representa 12 horas em milesundos
            return 43200000l;
        }

        return null;
    }

    /**
     * Returns the total number of lunch hours that must be done in a day, based on a shift and date.
     * If the shift passed as a parameter has "tipo" equals 1 or 2, the method will returns the shift's weekday's
     * "tempoAlimentacao"; and if "tipo" equals "3", returns "tempoAlimentacao"
     *
     * @param shift Driver's shift.
     * @param data  Date to analyze.
     * @return The total number of lunch hours that must be done in a day.
     */
    public Long lunchHoursPerDate(Turnos shift, Date data) {
        Long[] lunchHours = new Long[]{
                shift.getTempoAlimentacao01(),
                shift.getTempoAlimentacao02(),
                shift.getTempoAlimentacao03(),
                shift.getTempoAlimentacao04(),
                shift.getTempoAlimentacao05(),
                shift.getTempoAlimentacao06(),
                shift.getTempoAlimentacao07()
        };

        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(data);
        int weekDay = cal.get(GregorianCalendar.DAY_OF_WEEK) - 1;

        switch (shift.getTipo()) {
            case 0:{
                //12x36
                return 3600000L;
            }
            case 1: // FIXO
            case 2: // FLEX (com grade do horário)
                return lunchHours[weekDay];
            case 3: // TOTAL FLEX
                return shift.getTempoAlimentacao01();
            default:
                break;
        }

        return null;
    }
}
