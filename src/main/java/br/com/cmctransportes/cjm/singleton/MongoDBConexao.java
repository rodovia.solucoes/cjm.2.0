package br.com.cmctransportes.cjm.singleton;

import br.com.cmctransportes.cjm.logger.LogSistema;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoDatabase;

public class MongoDBConexao {

    private static MongoDBConexao mongoDBConexao;
    private MongoClient mongoClient;
    private final String enderecoServidor = "159.89.54.41";

    public static synchronized MongoDBConexao getMongoDBConexao()  {
        try {
            synchronized (MongoDBConexao.class) {
                if (mongoDBConexao == null) {
                    mongoDBConexao = new MongoDBConexao();
                    mongoDBConexao.criarConexaoMongo();
                }
            }
            return mongoDBConexao;
        } catch (Exception e) {
            LogSistema.logError("getMongoDBConexao", e);
        }
        return null;
    }

    private void criarConexaoMongo() {
        try {
            MongoCredential credential = MongoCredential.createCredential("rodoviamongo", "rodovia", "rodoviamongo2021".toCharArray());
            MongoClientOptions.Builder builder = MongoClientOptions.builder().connectTimeout(300000);
            mongoClient = new MongoClient(new ServerAddress(enderecoServidor, 27017));//, Arrays.asList(credential), builder.build());

        } catch (Exception e) {
            LogSistema.logError("criarConexaoMongo", e);
        }
    }

    public MongoDatabase pegarBaseRodovia() {
        try {
            return mongoClient.getDatabase("rodovia");
        } catch (Exception e) {
            LogSistema.logError("pegarBaseRodovia", e);
        }

        return null;
    }


}
