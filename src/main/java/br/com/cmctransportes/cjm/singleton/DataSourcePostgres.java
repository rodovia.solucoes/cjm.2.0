package br.com.cmctransportes.cjm.singleton;

import br.com.cmctransportes.cjm.utils.ArquivoDeConfiguracao;
import com.mchange.v2.c3p0.ComboPooledDataSource;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

public class DataSourcePostgres {

    private static DataSourcePostgres datasource;
    private final ComboPooledDataSource cpds;

    private DataSourcePostgres() throws PropertyVetoException {
        cpds = new ComboPooledDataSource();
        cpds.setDriverClass("org.postgresql.Driver");
        cpds.setJdbcUrl(new ArquivoDeConfiguracao().buscarValor("spring.datasource.url"));
        cpds.setUser(new ArquivoDeConfiguracao().buscarValor("spring.datasource.username"));
        cpds.setPassword(new ArquivoDeConfiguracao().buscarValor("spring.datasource.password"));
        cpds.setInitialPoolSize(3);
        cpds.setMinPoolSize(1);
        cpds.setMaxPoolSize(100);
        cpds.setAcquireIncrement(10);
        cpds.setIdleConnectionTestPeriod(60);
        cpds.setMaxIdleTime(25000);
    }

    public synchronized static DataSourcePostgres getInstance() throws IOException, SQLException, PropertyVetoException {
        if (datasource == null) {
            datasource = new DataSourcePostgres();
            return datasource;
        } else {
            return datasource;
        }
    }

    public synchronized Connection getConnection() throws SQLException {
        return this.cpds.getConnection();
    }
}
