package br.com.cmctransportes.cjm.singleton;

import br.com.cmctransportes.cjm.utils.ArquivoDeConfiguracao;
import com.mchange.v2.c3p0.ComboPooledDataSource;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

public class DataSourcePostgresOrion {

    private static DataSourcePostgresOrion datasource;
    private final ComboPooledDataSource cpds;

    private DataSourcePostgresOrion() throws PropertyVetoException {
        cpds = new ComboPooledDataSource();
        cpds.setDriverClass("org.postgresql.Driver");
        cpds.setJdbcUrl(new ArquivoDeConfiguracao().buscarValor("spring.datasource.orion.url"));
        cpds.setUser(new ArquivoDeConfiguracao().buscarValor("spring.datasource.orion.username"));
        cpds.setPassword(new ArquivoDeConfiguracao().buscarValor("spring.datasource.orion.password"));
        cpds.setInitialPoolSize(1);
        cpds.setMinPoolSize(1);
        cpds.setMaxPoolSize(5);
        cpds.setMaxIdleTime(5);
    }

    public synchronized static DataSourcePostgresOrion getInstance() throws IOException, SQLException, PropertyVetoException {
        if (datasource == null) {
            datasource = new DataSourcePostgresOrion();
            return datasource;
        } else {
            return datasource;
        }
    }

    public synchronized Connection getConnection() throws SQLException {
        return this.cpds.getConnection();
    }
}
