package br.com.cmctransportes.cjm.activemq;

import java.util.Random;
import javax.jms.*;

import br.com.cmctransportes.cjm.constants.Constantes;
import br.com.cmctransportes.cjm.domain.entities.Rastro;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.RedeliveryPolicy;

public class ActiveMQMessageProducer {
    private static final String ACTION_ID_HEADER = "actionId";
    private ConnectionFactory connFactory;
    private Connection connection;
    private Session session;
    private Destination destination;
    private MessageProducer msgProducer;
    private final String activeMqBrokerUri;
    private final String username;
    private final String password;

    public ActiveMQMessageProducer() {
        super();
        this.activeMqBrokerUri = Constantes.ACTIVEMQ_BROKER_URI;
        this.username = Constantes.ACTIVEMQ_USERNAME;
        this.password = Constantes.ACTIVEMQ_PASSWORD;
    }

    public void setup(final boolean transacted, final boolean isDestinationTopic, final String destinationName) throws JMSException {
        setConnectionFactory(activeMqBrokerUri, username, password);
        setConnection();
        setSession(transacted);
        setDestination(isDestinationTopic, destinationName);
        setMsgProducer();
    }

    public void close() throws JMSException {
        if (msgProducer != null) {
            msgProducer.close();
            msgProducer = null;
        }
        if (session != null) {
            session.close();
            session = null;
        }
        if (connection != null) {
            connection.close();
            connection = null;
        }
    }

    public void commit(final boolean transacted) throws JMSException {
        if (transacted) {
            session.commit();
        }
    }

    public void sendMessage(final Rastro rastro) throws JMSException {
        ObjectMessage objectMessage = buildMessageWithProperty(rastro);
        msgProducer.send(destination, objectMessage);
    }

    public void sendMessage(final String dados) throws JMSException {
        TextMessage message = session.createTextMessage(dados);
        msgProducer.send(destination, message);
    }

    private ObjectMessage buildMessageWithProperty(final Rastro dados) throws JMSException {

        ObjectMessage objectMessage = session.createObjectMessage(Rastro.class);
        Random rand = new Random();
        int value = rand.nextInt(100);
        objectMessage.setObject(dados);
        objectMessage.setStringProperty(ACTION_ID_HEADER, String.valueOf(value));
        return objectMessage;
    }

    private void setDestination(final boolean isDestinationTopic, final String destinationName) throws JMSException {
        if (isDestinationTopic) {
            destination = session.createTopic(destinationName);
        } else {
            destination = session.createQueue(destinationName);
        }
    }

    private void setMsgProducer() throws JMSException {
        msgProducer = session.createProducer(destination);
    }

    private void setSession(final boolean transacted) throws JMSException {
        session = connection.createSession(transacted, Session.AUTO_ACKNOWLEDGE);
    }

    private void setConnection() throws JMSException {
        connection = connFactory.createConnection();
        connection.start();
    }

    private void setConnectionFactory(final String activeMqBrokerUri, final String username, final String password) {
        connFactory = new ActiveMQConnectionFactory(username, password, activeMqBrokerUri);
        ((ActiveMQConnectionFactory) connFactory).setUseAsyncSend(true);
        RedeliveryPolicy policy = ((ActiveMQConnectionFactory) connFactory).getRedeliveryPolicy();
        policy.setInitialRedeliveryDelay(500);
        policy.setBackOffMultiplier(2);
        policy.setUseExponentialBackOff(true);
        policy.setMaximumRedeliveries(2);
    }
}
