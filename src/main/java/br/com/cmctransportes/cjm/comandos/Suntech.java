package br.com.cmctransportes.cjm.comandos;

import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.proxy.modelo.Equipamento;
import br.com.cmctransportes.cjm.proxy.modelo.Veiculo;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgresOrion;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.sql.*;
import java.util.Date;
import java.util.Objects;

public class Suntech extends ComandoMaster implements IComando {

    private Equipamento _equipamento;
    @Override
    public void setEquipamento(Equipamento equipamento) {
        this._equipamento = equipamento;
    }

    @Override
    public void bloquear(Veiculo veiculo) {
        if(Objects.nonNull(this._equipamento) && Objects.nonNull(this._equipamento.getBloquearVeiculo()) && this._equipamento.getBloquearVeiculo()){
            String comando = "ST300CMD;" + this._equipamento.getImei().trim() + ";02;Enable1";
            this.inserirComandoGenerico(comando, veiculo.getId(), "COMANDO_SUNTECH", "COMANDOGERAL");
        }
    }

    @Override
    public void desbloquear(Veiculo veiculo) {
        String comando = "ST300CMD;" + this._equipamento.getImei().trim() + ";02;Disable1";
        this.inserirComandoGenerico(comando, veiculo.getId(), "COMANDO_SUNTECH", "COMANDOGERAL");
    }

    @Override
    public void versaoDoFirmaware(Veiculo veiculo) {
        throw new NotImplementedException();
    }

    @Override
    public void buscarNumeroDeSerie(Veiculo veiculo) {
        throw new NotImplementedException();
    }

    @Override
    public void bloquearIbutton(Veiculo veiculo) {
        throw new NotImplementedException();
    }

    @Override
    public void desbloquearIbutton(Veiculo veiculo) {
        throw new NotImplementedException();
    }

}
