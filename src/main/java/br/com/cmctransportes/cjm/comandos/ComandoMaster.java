package br.com.cmctransportes.cjm.comandos;

import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgresOrion;

import java.sql.*;
import java.util.Date;

public class ComandoMaster {

    public void inserirComandoGenerico(String comando, Integer idVeiculo, String fila, String nomeDoComando) {
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "INSERT INTO comando( "
                    + "	id, "
                    + " comando, comando_enviado, "
                    + " data, retorno_recebido, sigla, "
                    + " valor_comando, veiculo, comando_em_processo, depende_outro_comando, mostrar_retorno, nome_da_fila) "
                    + "	VALUES (nextval('seq_comando'), ? , false, ?, false, 'CGCMD1', ?, ?, false, false, true, ?)";
            connection = DataSourcePostgresOrion.getInstance().getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, nomeDoComando);
            preparedStatement.setTimestamp(2, new Timestamp(new Date().getTime()));
            preparedStatement.setString(3, comando);
            preparedStatement.setInt(4, idVeiculo);
            preparedStatement.setString(5, fila);
            preparedStatement.executeUpdate();
            connection.commit();
        } catch (Exception e) {
            LogSistema.logError("inserirComandoGenerico", e);
        }finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("inserirComandoGenerico", ex);
            }
        }
    }
}
