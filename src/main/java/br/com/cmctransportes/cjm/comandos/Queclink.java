package br.com.cmctransportes.cjm.comandos;

import br.com.cmctransportes.cjm.proxy.modelo.Equipamento;
import br.com.cmctransportes.cjm.proxy.modelo.Veiculo;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class Queclink extends ComandoMaster implements IComando{
    private Equipamento _equipamento;
    private final String FILA_COMANDO = "COMANDO_QUECLINK";

    @Override
    public void setEquipamento(Equipamento equipamento) {
        this._equipamento = equipamento;
    }

    @Override
    public void bloquear(Veiculo veiculo) {
        String comando = "AT+GTOUT=gv300can,2,0,0,0,0,0,,,,0,,,,0,5,,FFFF$";
        this.inserirComandoGenerico(comando, veiculo.getId(), FILA_COMANDO, "GTOUT");
    }

    @Override
    public void desbloquear(Veiculo veiculo) {
        String comando = "AT+GTOUT=gv300can,0,0,0,0,0,0,,,,0,,,,0,5,,FFFF$";
        this.inserirComandoGenerico(comando, veiculo.getId(), FILA_COMANDO, "GTOUT");
    }

    @Override
    public void versaoDoFirmaware(Veiculo veiculo) {
        String comando = "AT+GTRTO=gv300can,8,,0,,,,FFFF$";
        this.inserirComandoGenerico(comando, veiculo.getId(), FILA_COMANDO, "GTVER");
    }

    @Override
    public void buscarNumeroDeSerie(Veiculo veiculo) {
        String comando = "AT+GTRTO=gv300can,12,,0,,,,FFFF$";
        this.inserirComandoGenerico(comando, veiculo.getId(), FILA_COMANDO, "GTCVN");
    }

    @Override
    public void bloquearIbutton(Veiculo veiculo) {
        String comando = "AT+GTIDA=gv300can,2,1,1,,30,7,30,,,,1,0,0,0,0,,,,FFFF$";
        this.inserirComandoGenerico(comando, veiculo.getId(), FILA_COMANDO, "GTIDA");
    }

    @Override
    public void desbloquearIbutton(Veiculo veiculo) {
        String comando = "AT+GTIDA=gv300can,0,1,1,,30,2,30,,,,0,0,0,0,0,,,,FFFF$";
        this.inserirComandoGenerico(comando, veiculo.getId(), FILA_COMANDO, "GTIDA");
    }
}
