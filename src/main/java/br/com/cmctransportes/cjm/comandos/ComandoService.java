package br.com.cmctransportes.cjm.comandos;

import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.proxy.modelo.Equipamento;
import br.com.cmctransportes.cjm.proxy.modelo.Modelo;
import br.com.cmctransportes.cjm.proxy.modelo.Veiculo;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgresOrion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ComandoService {

    private static ComandoService _comandoService = null;
    private Veiculo veiculo;

    private ComandoService() {

    }


    public static ComandoService getInstance(){
        if(Objects.isNull(_comandoService)){
            _comandoService = new ComandoService();
        }
        return _comandoService;
    }

    /**
     * Pesquisa qual o modelo do equipamento para poder enviar o comando
     * @param idVeiculo
     * @return
     */
    public List<IComando> processarComando(Integer idVeiculo){
        List<IComando> lista = new ArrayList<>();
        try {
            veiculo = new Veiculo();
            veiculo.setId(idVeiculo);
            Equipamento equipamentoUm = buscarImeiUm(idVeiculo);
            if(Objects.nonNull(equipamentoUm)){
                IComando ic = identificarEquipamento(equipamentoUm);
                if(Objects.nonNull(ic)){
                    lista.add(ic);
                }
                veiculo.setEquipamento(equipamentoUm);
            }

            Equipamento equipamentoDois = buscarImeiDois(idVeiculo);
            if(Objects.nonNull(equipamentoDois)){
                IComando ic = identificarEquipamento(equipamentoDois);
                if(Objects.nonNull(ic)){
                    lista.add(ic);
                }
                veiculo.setEquipamentoDois(equipamentoDois);
            }

        }catch (Exception e){
            LogSistema.logError("processarComando", e);
        }
        return lista;
    }


    private IComando identificarEquipamento(Equipamento equipamento){
        IComando iComando = null;
        try {
            if("Suntech".equalsIgnoreCase(equipamento.getModelo().getFabricante())){
                iComando = new Suntech();
                iComando.setEquipamento(equipamento);
            }
            if("Queclink".equalsIgnoreCase(equipamento.getModelo().getFabricante())){
                iComando = new Queclink();
                iComando.setEquipamento(equipamento);
            }
        }catch (Exception e){
            LogSistema.logError("identificarEquipamento", e);
        }
        return iComando;
    }

    private Equipamento buscarImeiUm(Integer idVeiculo) {
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select e.imei, m.marca, m.fabricante, v.bloquear_veiculo from veiculo v " +
                    " inner join equipamento e on e.id = v.equipamento  " +
                    " inner join modelo m on m.id = e.modelo  " +
                    " where v.id = ?";
            connection = DataSourcePostgresOrion.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idVeiculo);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                String imei = resultSet.getString("imei");
                if(Objects.nonNull(imei)  && !imei.contains("-")) {
                    Equipamento e = new Equipamento();
                    e.setImei(imei);
                    Modelo m = new Modelo();
                    m.setFabricante(resultSet.getString("marca"));
                    m.setMarca(resultSet.getString("fabricante"));
                    e.setModelo(m);
                    e.setBloquearVeiculo(resultSet.getBoolean("bloquear_veiculo"));
                    return e;
                }
            }
        } catch (Exception e) {
            LogSistema.logError("buscarImeiUm", e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarImeiUm", ex);
            }
        }
        return null;
    }

    private Equipamento buscarImeiDois(Integer idVeiculo) {
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select e.imei, m.marca, m.fabricante, v.bloquear_veiculo from veiculo v " +
                    " inner join equipamento e on e.id = v.equipamento2   " +
                    " inner join modelo m on m.id = e.modelo  " +
                    " where v.id = ?";
            connection = DataSourcePostgresOrion.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idVeiculo);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                String imei = resultSet.getString("imei");
                if(Objects.nonNull(imei)  && !imei.contains("-")) {
                    Equipamento e = new Equipamento();
                    e.setImei(resultSet.getString("imei"));
                    Modelo m = new Modelo();
                    m.setFabricante(resultSet.getString("marca"));
                    m.setMarca(resultSet.getString("fabricante"));
                    e.setModelo(m);
                    e.setBloquearVeiculo(resultSet.getBoolean("bloquear_veiculo"));
                    return e;
                }
            }
        } catch (Exception e) {
            LogSistema.logError("buscarImeiDois", e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarImeiDois", ex);
            }
        }
        return null;
    }
}
