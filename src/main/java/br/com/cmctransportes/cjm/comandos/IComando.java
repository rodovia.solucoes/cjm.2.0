package br.com.cmctransportes.cjm.comandos;

import br.com.cmctransportes.cjm.proxy.modelo.Equipamento;
import br.com.cmctransportes.cjm.proxy.modelo.Veiculo;

public interface IComando {
    void setEquipamento(Equipamento equipamento);
    void bloquear(Veiculo veiculo);
    void desbloquear(Veiculo veiculo);
    void versaoDoFirmaware(Veiculo veiculo);
    void buscarNumeroDeSerie(Veiculo veiculo);
    void bloquearIbutton(Veiculo veiculo);
    void desbloquearIbutton(Veiculo veiculo);
}
