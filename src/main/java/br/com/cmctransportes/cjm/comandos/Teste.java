package br.com.cmctransportes.cjm.comandos;

import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.proxy.modelo.Veiculo;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgresOrion;

import java.sql.*;
import java.util.Date;
import java.util.List;

public class Teste {
    public static void main(String[] args) {
        try {
           Veiculo v = new Veiculo();
           v.setId(502);
           List<IComando> l = ComandoService.getInstance().processarComando(502);
           for(IComando comando : l){
               comando.versaoDoFirmaware(v);
           }
            System.out.println("");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
