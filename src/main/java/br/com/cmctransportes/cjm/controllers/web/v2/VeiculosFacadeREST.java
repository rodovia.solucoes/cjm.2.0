package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.Veiculos;
import br.com.cmctransportes.cjm.domain.entities.vo.*;
import br.com.cmctransportes.cjm.domain.services.VeiculosService;
import br.com.cmctransportes.cjm.proxy.VeiculoTrajetto;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

/**
 * @author Fabio
 */
@RestController
@RequestMapping(path = "App/v2/web/veiculos")
public class VeiculosFacadeREST {

    @Autowired
    private VeiculosService vs;


    @RequestMapping(method = RequestMethod.GET, path = "/lista/{idEmpresa}/{idUnidade}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<Veiculos>> findAll(@PathVariable("idEmpresa") Integer idEmpresa, @PathVariable("idUnidade") Integer idUnidade) {
        String error = null;
        List<Veiculos> entity = Collections.emptyList();

        try {
            entity = vs.findAll(idEmpresa, idUnidade);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, entity);
    }

    @RequestMapping(value = "/cadastrarVeiculo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoVeiculoVO> cadastrarVeiculo(@RequestBody VeiculoVO veiculoVO) {

        RetornoVeiculoVO retorno = new RetornoVeiculoVO();
        try {
            retorno = vs.cadastrarVeiculo(veiculoVO);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/editarVeiculo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoVeiculoVO> editarVeiculo(@RequestBody VeiculoVO veiculoVO) {

        RetornoVeiculoVO retorno = new RetornoVeiculoVO();
        try {
            retorno = vs.editarVeiculo(veiculoVO);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/listarVeiculo/{idEmpresa}/{idUnidade}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoVeiculoVO> listarVeiculo(@PathVariable("idEmpresa") Integer idEmpresa, @PathVariable("idUnidade") Integer idUnidade) {
        String error = null;
        RetornoVeiculoVO retorno = new RetornoVeiculoVO();
        try {
            retorno = vs.listarVeiculo(idEmpresa, idUnidade);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/buscarVeiculoPeloId/{idEmpresa}/{idVeiculo}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoVeiculoVO> buscarVeiculoPeloId(@PathVariable("idEmpresa") Integer idEmpresa, @PathVariable("idVeiculo") Integer idVeiculo) {
        String error = null;
        RetornoVeiculoVO retorno = new RetornoVeiculoVO();
        try {
            retorno = vs.buscarVeiculoPeloId(idEmpresa, idVeiculo);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/buscarListaDeVeiculoPeloCliente/{idEmpresa}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoVeiculoVO> buscarListaDeVeiculoPeloCliente(@PathVariable("idEmpresa") Integer idEmpresa) {
        String error = null;
        RetornoVeiculoVO retorno = new RetornoVeiculoVO();
        try {
            retorno = vs.buscarListaDeVeiculoPeloCliente(idEmpresa);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/buscarListaDeVeiculoMinimaPorEmpresa/{idEmpresa}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoVeiculoVO> buscarListaDeVeiculoMinimaPorEmpresa(@PathVariable("idEmpresa") Integer idEmpresa) {
        String error = null;
        RetornoVeiculoVO retorno = new RetornoVeiculoVO();
        try {
            retorno = vs.buscarListaDeVeiculoMinimaPorEmpresa(idEmpresa);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/buscarListaDeVeiculoMinimaPorEmpresaAtivos/{idEmpresa}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoVeiculoVO> buscarListaDeVeiculoMinimaPorEmpresaAtivos(@PathVariable("idEmpresa") Integer idEmpresa) {
        String error = null;
        RetornoVeiculoVO retorno = new RetornoVeiculoVO();
        try {
            retorno = vs.buscarListaDeVeiculoMinimaPorEmpresaAtivos(idEmpresa);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }



}