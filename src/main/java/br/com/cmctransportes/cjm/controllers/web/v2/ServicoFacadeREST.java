package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.vo.*;
import br.com.cmctransportes.cjm.domain.services.ServicoService;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "App/v2/web/servicos")
public class ServicoFacadeREST {

    @Autowired
    private ServicoService servicoService;


    @RequestMapping(value = "/registrarManifesto", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ServicoRetornoVO> registrarHodometro(@RequestBody ManifestoVO manifestoVO) {
        ServicoRetornoVO retorno = new ServicoRetornoVO();
        try {
            retorno = servicoService.gravarManifesto(manifestoVO);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    @RequestMapping(value = "/registrarViagem", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ServicoRetornoVO> registrarViagem(@RequestBody EnvioViagemVO envioViagemVO) {

        ServicoRetornoVO retorno = new ServicoRetornoVO();
        try {
            retorno = servicoService.gravarViagem(envioViagemVO.getListaDeViagem(), envioViagemVO.getUuid());
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }

        LogSistema.logInfo("RETORNO REGISTRAR VIAGEM: "+ retorno.getCodigoRetorno() + " - " + retorno.getMensagem());

        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarListaDeVeiculos", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ServicoRetornoVO> buscarListaDeVeiculos(@RequestBody ParametrosServicoVO parametrosServicoVO) {

        ServicoRetornoVO retorno = new ServicoRetornoVO();
        try {
            retorno = servicoService.buscarListaDeVeiculos(parametrosServicoVO);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }

        LogSistema.logInfo("RETORNO REGISTRAR VIAGEM: "+ retorno.getCodigoRetorno() + " - " + retorno.getMensagem());

        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }
}
