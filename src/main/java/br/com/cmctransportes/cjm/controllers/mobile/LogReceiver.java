/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cmctransportes.cjm.controllers.mobile;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author analista.ti
 */
@RestController
@RequestMapping(path = "App/log")
public class LogReceiver {

    // TODO: colocar isso em arquivo de configuração!
    private String UPLOADED_FILE_PATH =
            /* Homologação*/  "/opt/tomcat/upload";
    /* Desenvolvimento  "c:"+File.separator+"tmp"+File.separator+"CJM_LOG"; //+File.separator;*/

    @RequestMapping(method = RequestMethod.POST, path = "/", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String uploadFile(@RequestParam("file") MultipartFile file) {

        String fileName = "";

        try {
            fileName = file.getOriginalFilename();
            fileName = fileName.replace("_", File.separator);
            fileName = fileName.replace(' ', '_');

            //convert the uploaded file to inputstream
            byte[] bytes = file.getBytes();

            //constructs upload file path
            fileName = UPLOADED_FILE_PATH + fileName;

            writeFile(bytes, fileName);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return "uploadFile is called, Uploaded file name : " + fileName;

    }

    //save to somewhere
    private void writeFile(byte[] content, String filename) throws IOException {

        File file = new File(filename);

        File parent = file.getParentFile();
        if (!parent.exists() && !parent.mkdirs()) {
            throw new IllegalStateException("Couldn't create dir: " + parent);
        }

        if (!file.exists()) {
            file.createNewFile();
        }

        FileOutputStream fop = new FileOutputStream(file);

        fop.write(content);
        fop.flush();
        fop.close();

    }

}
