package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.Permissao;
import br.com.cmctransportes.cjm.domain.entities.vo.RESTResponseVO;
import br.com.cmctransportes.cjm.domain.services.PermissaoService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping(path = "App/v2/web/permissao")
public class PermissaoFacedeREST {

    @Autowired
    private PermissaoService permissaoService;

    @RequestMapping(method = RequestMethod.GET, path = "buscarTodas", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<Permissao>> buscarTodas() {
        String error = "";
        List<Permissao> entity = Collections.emptyList();
        try {
            entity = permissaoService.buscarTodos();
        } catch (org.hibernate.exception.ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, entity);
    }
}
