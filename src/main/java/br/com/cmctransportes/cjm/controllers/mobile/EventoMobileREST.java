package br.com.cmctransportes.cjm.controllers.mobile;

import br.com.cmctransportes.cjm.controllers.runnable.JornadaRunnble;
import br.com.cmctransportes.cjm.domain.entities.Evento;
import br.com.cmctransportes.cjm.domain.entities.Jornada;
import br.com.cmctransportes.cjm.domain.entities.vo.AlertaViamVO;
import br.com.cmctransportes.cjm.domain.entities.vo.EventoMobileVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoAlertaVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoEventoVO;
import br.com.cmctransportes.cjm.domain.services.EmpresasService;
import br.com.cmctransportes.cjm.domain.services.EventoService;
import br.com.cmctransportes.cjm.domain.services.JornadaService;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import br.com.cmctransportes.cjm.utils.ThreadFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

/**
 * @author analista.ti
 */
@RestController
@RequestMapping(path = "App/eventos")
public class EventoMobileREST {

    @Autowired
    private EventoService es;

    @Autowired
    private JornadaService js;

    @Autowired
    private EmpresasService empresasService;

    private Evento fillJourney(Evento entity) {
        Jornada j = entity.getJornadaId();

        if (j == null) {
            j = js.getById(entity.getJornada());
            entity.setJornadaId(j);
        }

        if (Objects.nonNull(entity.getJornadaId()) && Objects.isNull(entity.getJornadaId().getEventos())) {
            Integer id = entity.getJornadaId().getId();
            j = js.getById(id);

            entity.setJornadaId(j);
        }

        return entity;
    }

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Evento create(@RequestBody EventoMobileVO entityVO) {
//        entityVO.setInstanteEvento(TimeHelper.fixDaylightSavings(entityVO.getInstanteEvento()));
//        entityVO.setInstanteLancamento(TimeHelper.fixDaylightSavings(entityVO.getInstanteLancamento()));

        Evento entity = entityVO.revert(this.empresasService, this.js);
        // Se a jornada não tiver vindo preenchida, preencher.
        entity = this.fillJourney(entity);

        es.saveWithOutReOrder(entity, true);
        inserirMongo(entity);
        return entity;
    }


    @RequestMapping(value = "/cadastrarEvento", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoEventoVO> cadastrarEvento(@RequestBody EventoMobileVO entityVO){
        RetornoEventoVO retorno = new RetornoEventoVO();
        try{
            Evento entity = entityVO.revert(this.empresasService, this.js);
            // Se a jornada não tiver vindo preenchida, preencher.
            entity = this.fillJourney(entity);

            es.saveWithOutReOrder(entity, true);
            inserirMongo(entity);
            retorno.setEvento(entity);
            retorno.setCodigoRetorno(1);
            retorno.setMensagem("Evento cadastrado com Sucesso.");
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
        }

        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    private void inserirMongo(Evento evento){
        try {
            evento.setEventoSeguinte(null);
            evento.setEventoAnterior(null);
            ThreadFactory.getExecutor().execute(new JornadaRunnble(evento, "CELULAR"));
        }catch (Exception e){
            LogSistema.logError("inserirMongo", e);
        }
    }
}