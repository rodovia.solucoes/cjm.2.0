package br.com.cmctransportes.cjm.controllers.mobile;

import br.com.cmctransportes.cjm.domain.entities.Rastro;
import br.com.cmctransportes.cjm.domain.services.RastroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author analista.ti
 */
@RestController
@RequestMapping(path = "App/rastros")
public class RastroMobileREST {

    @Autowired
    private RastroService rs;

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Rastro create(@RequestBody Rastro entity) {
        rs.enviarFilaJMS(entity);

        return entity;
    }

    @RequestMapping(method = RequestMethod.POST, path = "trilha", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Rastro[] create(@RequestBody Rastro[] list) {
        for (Rastro entity : list) {
            rs.save(entity);
        }

        return list;
    }

    @RequestMapping(method = RequestMethod.PUT, path = "{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void edit(@PathVariable("id") Integer id, Rastro entity) {
        rs.update(entity);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "{id}")
    public void remove(@PathVariable("id") Integer id) {
        rs.delete(id);
    }

    @RequestMapping(method = RequestMethod.GET, path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Rastro find(@PathVariable("id") Integer id) {
        return rs.getById(id);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Rastro> findAll() {
        return rs.findAll();
    }
}
