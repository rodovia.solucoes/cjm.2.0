package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.SascarVeiculo;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoSascarVeiculoVO;
import br.com.cmctransportes.cjm.domain.services.SascarVeiculoService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "App/v2/web/sascarveiculo")
public class SascarVeiculoREST {

    @Autowired
    private SascarVeiculoService sascarVeiculoService;

    @RequestMapping(value = "/cadastrar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoSascarVeiculoVO> cadastrar(@RequestBody SascarVeiculo sascarVeiculo) {
        RetornoSascarVeiculoVO retorno = new RetornoSascarVeiculoVO();
        try {
            sascarVeiculoService.save(sascarVeiculo);
            retorno.setCodigoRetorno(1);
            retorno.setMensagem("SascarVeiculo cadastrado com sucesso!");
        } catch (org.hibernate.exception.ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/editar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoSascarVeiculoVO> editar(@RequestBody SascarVeiculo sascarVeiculo) {
        RetornoSascarVeiculoVO retorno = new RetornoSascarVeiculoVO();
        try {
            sascarVeiculoService.update(sascarVeiculo);
            retorno.setCodigoRetorno(1);
            retorno.setMensagem("SascarVeiculo editado com sucesso!");
        } catch (org.hibernate.exception.ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarPeloId/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoSascarVeiculoVO> buscarPeloId(@PathVariable("id") Integer id) {

        RetornoSascarVeiculoVO retorno = new RetornoSascarVeiculoVO();
        try {
            SascarVeiculo sascarVeiculo = sascarVeiculoService.getById(id);
            retorno.setSascarVeiculo(sascarVeiculo);
            retorno.setCodigoRetorno(1);
            retorno.setMensagem("SascarVeiculo carregado com sucesso!");
        } catch (org.hibernate.exception.ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    @RequestMapping(value = "/buscarPelaEmpresa/{idEmpresa}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoSascarVeiculoVO> buscarPelaEmpresa(@PathVariable("idEmpresa") Integer idEmpresa) {

        RetornoSascarVeiculoVO retorno = new RetornoSascarVeiculoVO();
        try {
            List<SascarVeiculo> lista = sascarVeiculoService.findAll(idEmpresa);
            retorno.setListaSascarVeiculo(lista);
            retorno.setCodigoRetorno(1);
            retorno.setMensagem("Lista SascarVeiculo carregado com sucesso!");
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

}
