package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.Users;
import br.com.cmctransportes.cjm.domain.entities.vo.RESTResponseVO;
import br.com.cmctransportes.cjm.domain.entities.vo.UserVO;
import br.com.cmctransportes.cjm.domain.services.UserService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

/**
 * REST Web Service
 *
 * @author analista.ti
 */
@RestController
@RequestMapping(path = "App/v2/web/login")
@Slf4j
public class LoginResource {

    @Autowired
    private UserService us;

    @RequestMapping(method = RequestMethod.POST, path = "manual", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<UserVO> manualLogin(@RequestBody Users user) {
        String error = null;
        UserVO loggedUser = null;

        try {
            if (Objects.isNull(user) || Objects.isNull(user.getSenha()) || Objects.isNull(user.getUserName())) {
                log.info("User == NULL (user or password is null)");
                error = "Favor preencher usuário e senha!";
            } else {
                Users dbUser = us.getByUserName(user.getUserName());
                if (Objects.isNull(dbUser)) {
                    log.info("User == NULL (user not found)");
                    error = "Usuário e/ou senha errados!";
                } else if (!dbUser.getSituacao()) {
                    log.info("User == Inativo (user inative)");
                    error = "Usuário Inativo!";
                } else {
                    log.info("User.getUserName()[{}]", user.getUserName());
                    user.setSenhaCripto(RodoviaUtils.cryptWithMD5(user.getSenha()));

                    log.info(user.getSenhaCripto());
                    log.info(dbUser.getSenhaCripto());

                    if (Objects.equals(dbUser.getSenhaCripto(), user.getSenhaCripto())) {
                        loggedUser = new UserVO(dbUser);
                    } else {
                        log.info("User == NULL (wrong password)");
                        error = "Usuário e/ou senha errados!";
                    }
                }
            }
        } catch (org.hibernate.exception.ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, loggedUser);
    }
}