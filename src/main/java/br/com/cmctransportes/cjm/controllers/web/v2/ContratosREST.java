package br.com.cmctransportes.cjm.controllers.web.v2;



import br.com.cmctransportes.cjm.domain.entities.Contratos;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoContratosVO;
import br.com.cmctransportes.cjm.domain.services.ContratosService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping(path = "App/v2/web/contratos")
public class ContratosREST {

    @Autowired
    private ContratosService contratosService;


    @RequestMapping(method = RequestMethod.POST, path = "/salvarContratos", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoContratosVO> salvarContratos(@RequestBody Contratos contratos) {
        RetornoContratosVO retorno = new RetornoContratosVO();
        try {
            contratosService.salvar(contratos);
            retorno.setMensagem("Contrato Cadastrado com sucesso");
            retorno.setCodigoRetorno(1);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.POST, path = "/editarContratos", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoContratosVO> editarContratos(@RequestBody List<Contratos> listaContratos) {
        RetornoContratosVO retorno = new RetornoContratosVO();
        try {
            contratosService.editar(listaContratos);
            retorno.setMensagem("Contrato Editado com sucesso");
            retorno.setCodigoRetorno(1);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarContratoPorEmpresa/{idEmpresa}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoContratosVO>  buscarContratoPorEmpresa(@PathVariable Integer idEmpresa) {
        RetornoContratosVO retorno = new RetornoContratosVO();
        try {
            List<Contratos> lista = this.contratosService.buscarPorEmpresa(idEmpresa);
            retorno.setListaDeContratos(lista);
            retorno.setCodigoRetorno(291);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    @RequestMapping(value = "/buscarContadores", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoContratosVO>  buscarContadores() {
        RetornoContratosVO retorno = new RetornoContratosVO();
        try {
            Contratos contratos  = this.contratosService.calcularContratos();
            retorno.setContratos(contratos);
            retorno.setCodigoRetorno(291);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarPorItem/{idItem}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoContratosVO>  buscarPorStatus(@PathVariable Integer idItem) {
        RetornoContratosVO retorno = new RetornoContratosVO();
        try {
            List<Contratos> lista  = this.contratosService.buscarListaPorItem(idItem);
            retorno.setListaDeContratos(lista);
            retorno.setCodigoRetorno(291);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


}
