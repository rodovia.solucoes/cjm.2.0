package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.Enderecos;
import br.com.cmctransportes.cjm.domain.entities.vo.*;
import br.com.cmctransportes.cjm.domain.services.LocalService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "App/v2/web/local")
public class LocalFacadeREST {

    @Autowired
    private LocalService localService;

    @RequestMapping(value = "/cadastrarLocal", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoLocalVO> cadastrarLocal(@RequestBody LocalVO localVO) {

        RetornoLocalVO retorno = new RetornoLocalVO();
        try {
            retorno = localService.cadastrarLocal(localVO);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/editarLocal", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoLocalVO> editarLocal(@RequestBody LocalVO localVO) {

        RetornoLocalVO retorno = new RetornoLocalVO();
        try {
            retorno = localService.editarLocal(localVO);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.DELETE, path = "{id}")
    public RESTResponseVO<Boolean> excluirLocal(@PathVariable("id") Integer id) {
        String error = "";
        boolean result = false;
        try {
            LocalVO localVO = new LocalVO();
            localVO.setId(id);
            localService.excluirLocal(localVO);
            result = true;
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, result);
    }


    @RequestMapping(value = "/cadastrarLocalPostoAbastecimento", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoLocalVO> cadastrarLocalPostoAbastecimento(@RequestBody LocalVO localVO) {

        RetornoLocalVO retorno = new RetornoLocalVO();
        try {
            retorno = localService.cadastrarLocalPostoAbastecimento(localVO);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/cadastrarLocalOficina", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoLocalVO> cadastrarLocalOficina(@RequestBody LocalVO localVO) {

        RetornoLocalVO retorno = new RetornoLocalVO();
        try {
            retorno = localService.cadastrarLocalOficina(localVO);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarLocal/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoLocalVO> buscarEquipamentoPeloCodigo(@PathVariable Integer id) {

        RetornoLocalVO retorno = new RetornoLocalVO();
        try {
            retorno = localService.buscarLocalPeloCodigo(id);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarListaLocal/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoLocalVO> buscarListaLocal(@PathVariable Integer id) {

        RetornoLocalVO retorno = new RetornoLocalVO();
        try {
            retorno = localService.buscarListaLocal(new EmpresaVO(id));
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/listaDeLocalPostoAbastecimento", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoLocalVO> listaDeLocalPostoAbastecimento(@PathVariable Integer id) {

        RetornoLocalVO retorno = new RetornoLocalVO();
        try {
            retorno = localService.listaDeLocalPostoAbastecimento(new EmpresaVO(id));
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/listaDeLocalPostoOficina", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoLocalVO> listaDeLocalPostoOficina(@PathVariable Integer id) {
        RetornoLocalVO retorno = new RetornoLocalVO();
        try {
            retorno = localService.listaDeLocalPostoOficina(new EmpresaVO(id));
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    @RequestMapping(value = "/buscarListaLocalExibirMapa", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoLocalVO> buscarListaLocalExibirMapa(@RequestBody EmpresaVO empresaVO) {
        RetornoLocalVO retorno = new RetornoLocalVO();
        try {
            retorno = localService.buscarListaLocalExibirMapa(empresaVO);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarTodosOsLocais", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoLocalVO> buscarTodosOsLocais(@RequestBody EmpresaVO empresaVO) {
        RetornoLocalVO retorno = new RetornoLocalVO();
        try {
            retorno = localService.buscarTodosOsLocais(empresaVO);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarLatLngPeloEndereco", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoMapaVO> buscarLatLngPeloEndereco(@RequestBody Enderecos enderecos) {
        RetornoMapaVO retorno = new RetornoMapaVO();
        try {
            retorno = localService.buscarLatLngPeloEndereco(enderecos);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/listaDeLocalDashboard/{idEmpresa}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoLocalVO> listaDeLocalDashboard(@PathVariable Integer idEmpresa) {
        RetornoLocalVO retorno = new RetornoLocalVO();
        try {
            retorno = localService.buscarTodosOsLocaisSqlNativo(new EmpresaVO(idEmpresa));
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }
}
