package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.comum.MacroSascarComum;

import br.com.cmctransportes.cjm.domain.entities.MacroSascar;
import br.com.cmctransportes.cjm.domain.entities.Sascar;
import br.com.cmctransportes.cjm.domain.entities.SascarVeiculo;
import br.com.cmctransportes.cjm.domain.entities.vo.RESTResponseVO;
import br.com.cmctransportes.cjm.domain.services.SascarJobWebService;
import br.com.cmctransportes.cjm.domain.services.SascarService;
import br.com.cmctransportes.cjm.domain.services.SascarVeiculoService;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.utils.ArquivoDeConfiguracao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Objects;


@RestController
@RequestMapping(path = "App/v2/web/sascarwebserverjob")
public class SascarWebserverJobFacadeREST {

    @Autowired
    private SascarJobWebService service;

    @Autowired
    private SascarService driverService;

    @Autowired
    private SascarVeiculoService sascarVeiculoService;

    /**
     * Classe responsavel por recber o arquivo da sascar e ecaminhar para o SascarUploadService
     * @return a RESTResponseVO caso ocorra algum erro.
     */
    /**
     * Classe resposnsável por receber a requisição do job para capturar os eventos em tempo real no Sascar
     *
     * @param startDate não tem necessidade da data inicial não está sendo usado está buscando no banco de dados.
     * @param dateEnd   data final da consulta a data inicial é buscada no banco de dados para evitar "janelas" e falhar a consulta
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, path = "/sascarjob/{startDate}/{endDate}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<String> webServiceSascar(@PathVariable("startDate") String startDate, @PathVariable("endDate") String dateEnd) {
        if (dateEnd == null) {
            throw new RuntimeException("Você deve uma data para fazer a comunicação com servidor");
        }
        Date date = new Date();

        ArquivoDeConfiguracao arquivoDeConfiguracao = new ArquivoDeConfiguracao();
        String usuarioSascar = arquivoDeConfiguracao.buscarValor("spring.sascar.usuario");
        String senha = arquivoDeConfiguracao.buscarValor("spring.sascar.senha");

        String error = "";
        String result = "";
        try {
            List<MacroSascar> listaMacroSascar = MacroSascarComum.getListaMacroSascar();
            if(Objects.nonNull(listaMacroSascar)) {
                List<SascarVeiculo> listaSascarVeiculo = sascarVeiculoService.findAll();
                if(Objects.nonNull(listaSascarVeiculo)) {
                    for (SascarVeiculo sascar : listaSascarVeiculo) {
                        this.service.ServerEvent(sascar, date.getTime(), listaMacroSascar, usuarioSascar, senha);
                    }
                }
            }
            result = "ok";
        } catch (Exception e) {
            e.printStackTrace();
            error = e.getLocalizedMessage();
        }
        return new RESTResponseVO<>(error, result);
    }


    @RequestMapping(method = RequestMethod.GET, path = "/sascarmanual", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<String> webServiceSascarManual() {

        Date date = new Date();

        ArquivoDeConfiguracao arquivoDeConfiguracao = new ArquivoDeConfiguracao();
        String usuarioSascar = arquivoDeConfiguracao.buscarValor("spring.sascar.usuario");
        String senha = arquivoDeConfiguracao.buscarValor("spring.sascar.senha");

        String error = "";
        String result = "";
        try {
            List<MacroSascar> listaMacroSascar = MacroSascarComum.getListaMacroSascar();
            if(Objects.nonNull(listaMacroSascar)) {
                List<SascarVeiculo> listaSascarVeiculo = sascarVeiculoService.findAllManual(18);
                if(Objects.nonNull(listaSascarVeiculo)) {
                    for (SascarVeiculo sascar : listaSascarVeiculo) {
                        this.service.ServerEvent(sascar, date.getTime(), listaMacroSascar, usuarioSascar, senha);
                    }
                }
            }
            result = "ok";
        } catch (Exception e) {
            e.printStackTrace();
            error = e.getLocalizedMessage();
        }
        return new RESTResponseVO<>(error, result);
    }
}
