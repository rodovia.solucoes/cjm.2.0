package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.vo.HodometroVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoHodometroVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoMacrosVO;
import br.com.cmctransportes.cjm.domain.services.HodometroService;
import br.com.cmctransportes.cjm.proxy.modelo.Veiculo;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "App/v2/web/hodometro")
public class HodometroFacadeREST {


    @Autowired
    private HodometroService hodometroService;

    @RequestMapping(value = "/registrarHodometro", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoHodometroVO> registrarHodometro(@RequestBody HodometroVO hodometroVO) {
        RetornoHodometroVO retorno = new RetornoHodometroVO();
        try {
            hodometroService.registrarHodometro(hodometroVO);
            retorno.setCodigoRetorno(1);
            retorno.setMensagem("Hodometro registrado com sucesso!");
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    @RequestMapping(value = "/buscarListaDeHodometro", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoHodometroVO>  buscarListaDeHodometro(@RequestBody Veiculo veiculo) {
        RetornoHodometroVO retorno = new RetornoHodometroVO();
        try {
            retorno = hodometroService.buscarListaDeHodometro(veiculo);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }



}
