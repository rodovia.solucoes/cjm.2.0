package br.com.cmctransportes.cjm.controllers.mobile;

import br.com.cmctransportes.cjm.domain.entities.Evento;
import br.com.cmctransportes.cjm.domain.entities.MobileDriver;
import br.com.cmctransportes.cjm.domain.entities.Motoristas;
import br.com.cmctransportes.cjm.domain.entities.Unidades;
import br.com.cmctransportes.cjm.domain.services.DriverService;
import br.com.cmctransportes.cjm.domain.services.EventoService;
import br.com.cmctransportes.cjm.domain.services.UnidadesService;
import br.com.cmctransportes.cjm.domain.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

/**
 * @author analista.ti
 */
@RestController
@RequestMapping(path = "App/motoristas")
public class MotoristaMobileREST {

    @Autowired
    private DriverService ms;

    @Autowired
    private UserService userService;

    @Autowired
    private UnidadesService unidadesService;

    @Autowired
    private EventoService eventoService;

    @RequestMapping(method = RequestMethod.GET, path = "userid/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public MobileDriver findByUserId(@PathVariable("userId") Integer userId) {
        Motoristas driver = ms.getByUserId(userId);
        MobileDriver result = new MobileDriver(driver, this.userService);

        Unidades unidade = this.unidadesService.getById(driver.getUnidadesId());

        result.setChatBroadcast(unidade.getChatBroadcast());

        Evento lastEvent = this.eventoService.getLastEvent(result.getId());

        if (Objects.nonNull(lastEvent) && Objects.nonNull(lastEvent.getTipoEvento()) && lastEvent.getTipoEvento().getId() != -1) {
            result.setLastEvent(lastEvent);
        }

        return result;
    }
}