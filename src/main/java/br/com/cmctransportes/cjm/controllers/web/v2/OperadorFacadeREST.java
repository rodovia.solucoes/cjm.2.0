package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.Operador;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoOperadorVO;
import br.com.cmctransportes.cjm.domain.services.ObservacaoService;
import br.com.cmctransportes.cjm.domain.services.OperadorService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "App/v2/web/operador")
public class OperadorFacadeREST {
    @Autowired
    private OperadorService operadorService;

    @RequestMapping(value = "/cadastrar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoOperadorVO> cadastrar(@RequestBody Operador operador) {
        RetornoOperadorVO retorno = new RetornoOperadorVO();
        try {
            operadorService.save(operador);
            retorno.setCodigoRetorno(1);
            retorno.setMensagem("Operador cadastrado com sucesso!");
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/editar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoOperadorVO> editar(@RequestBody Operador operador) {
        RetornoOperadorVO retorno = new RetornoOperadorVO();
        try {
            operadorService.update(operador);
            retorno.setCodigoRetorno(1);
            retorno.setMensagem("Operador editado com sucesso!");
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarPeloId/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoOperadorVO> buscarPeloId(@PathVariable("id") Integer id) {

        RetornoOperadorVO retorno = new RetornoOperadorVO();
        try {
            Operador operador = operadorService.getById(id);
            retorno.setOperador(operador);
            retorno.setCodigoRetorno(1);
            retorno.setMensagem("Operador carregado com sucesso!");
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    @RequestMapping(value = "/buscarPelaEmpresa/{idEmpresa}/{idUnidade}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoOperadorVO> buscarPelaEmpresa(@PathVariable("idEmpresa") Integer idEmpresa, @PathVariable("idUnidade") Integer idUnidade) {

        RetornoOperadorVO retorno = new RetornoOperadorVO();
        try {
            List<Operador> listaOperador = operadorService.findAll(idEmpresa, idUnidade);
            retorno.setListaOperador(listaOperador);
            retorno.setCodigoRetorno(1);
            retorno.setMensagem("Lista Operador carregado com sucesso!");
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }



}
