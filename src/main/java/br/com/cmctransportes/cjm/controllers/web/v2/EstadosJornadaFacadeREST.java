package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.EstadosJornada;
import br.com.cmctransportes.cjm.domain.entities.vo.RESTResponseVO;
import br.com.cmctransportes.cjm.domain.services.EstadosJornadaService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author analista.ti
 */
@RestController
@RequestMapping(path = "App/v2/web/estadosJornada")
public class EstadosJornadaFacadeREST {

    @Autowired
    private EstadosJornadaService cs;

    @RequestMapping(method = RequestMethod.GET, path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<EstadosJornada> find(@PathVariable("id") Integer id) {
        String error = "";
        EstadosJornada estadosJornada = null;

        try {
            estadosJornada = cs.getById(id);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, estadosJornada);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<EstadosJornada>> findAll() {
        String error = "";
        List<EstadosJornada> estadosJornada = null;

        try {
            estadosJornada = cs.findAll();
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, estadosJornada);
    }
}