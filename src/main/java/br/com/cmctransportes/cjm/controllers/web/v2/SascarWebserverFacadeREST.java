package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.vo.RESTResponseVO;
import br.com.cmctransportes.cjm.domain.services.SascarWebService;
import br.com.cmctransportes.cjm.sascarsoap.SasIntegraNotification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "App/v2/web/sascarwebserver")
public class SascarWebserverFacadeREST {

    @Autowired
    private SascarWebService service;
    /**
     * Classe responsavel por recber o arquivo da sascar e ecaminhar para o SascarUploadService
     * @param driver Motorista selecionado no front
     * @param startDate data inicial em milisegundos para a consulta no servidor Sascar
     * @param dateEnd data final em milisegundos para a consulta no servidor Sascar
     * @return a RESTResponseVO retorna ao requisitor uma mensagem de erro ou de sucesso.
     */
    @PostMapping(path = "{driver}/{startDate}/{endDate}", consumes = MediaType.ALL_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<String> webServiceSascar(@PathVariable("driver") Integer driver, @PathVariable("startDate") String startDate, @PathVariable("endDate") String dateEnd) {
        if (driver == null) {
            System.out.println("erro no motorista");
            throw new RuntimeException("Você deve selecionar um motorista para fazer a comunicação com servidor");
        }
        String result = "";
        String error = "";
        try {
            result = this.service.ServerEvent(driver, Long.valueOf(startDate), Long.valueOf(dateEnd));
        } catch (SasIntegraNotification e) {
            e.printStackTrace();
            error = e.getLocalizedMessage();
        }
        return new RESTResponseVO<>(error, result);
    }
}
