/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cmctransportes.cjm.controllers.web.v1;

import br.com.cmctransportes.cjm.domain.entities.Paramcfg;
import br.com.cmctransportes.cjm.domain.services.ParamcfgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * #2033016
 *
 * @author William Leite
 */
@RestController
@RequestMapping(path = "App/web/paramcfg")
public class ParamFacadeREST {

    @Autowired
    private ParamcfgService service;

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Paramcfg create(Paramcfg entity) {
        service.save(entity);
        return entity;
    }

    @RequestMapping(method = RequestMethod.PUT, path = "{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Paramcfg edit(@PathVariable("id") Integer id, @RequestBody Paramcfg entity) {
        service.update(entity);
        return entity;
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "{id}")
    public void remove(@PathVariable("id") Integer id) {
        Paramcfg param = service.findById(id);
        service.delete(param);
    }

    @RequestMapping(method = RequestMethod.GET, path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Paramcfg find(@PathVariable("id") Integer id) {
        return service.findById(id);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/group/{grcodigo}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Paramcfg> find(@PathVariable("grcodigo") String grcodigo) {
        return service.findByGroup(grcodigo);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Paramcfg> findAll() {
        return service.findAll();
    }
}