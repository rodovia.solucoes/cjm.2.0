package br.com.cmctransportes.cjm.controllers.web.v2;


import br.com.cmctransportes.cjm.domain.entities.vo.RESTResponseVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoRotaVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RotaVO;
import br.com.cmctransportes.cjm.domain.services.RotaService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(path = "App/v2/web/rota")
public class RotaFacadeREST {

    @Autowired
    private RotaService rotaService;


    @RequestMapping(method = RequestMethod.GET, path = "/listarRotas/{idEmpresa}/{idUnidade}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRotaVO> listarRotas(@PathVariable("idEmpresa") Integer idEmpresa, @PathVariable("idUnidade") Integer idUnidade) {
        String error = null;
        RetornoRotaVO retorno = new RetornoRotaVO();
        try {
            retorno = rotaService.listarRotas(idEmpresa, idUnidade);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/cadastrarRota", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRotaVO> cadastrarLocal(@RequestBody RotaVO rotaVO) {

        RetornoRotaVO retorno = new RetornoRotaVO();
        try {
            retorno = rotaService.cadastrarRota(rotaVO);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/editarRota", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRotaVO> editarRota(@RequestBody RotaVO rotaVO) {

        RetornoRotaVO retorno = new RetornoRotaVO();
        try {
            retorno = rotaService.editarRota(rotaVO);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarRota/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRotaVO> buscarRota(@PathVariable Integer id) {

        RetornoRotaVO retorno = new RetornoRotaVO();
        try {
            retorno = rotaService.buscarPeloId(id);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/excluirRota", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRotaVO> excluirRota(@RequestBody RotaVO rotaVO) {

        RetornoRotaVO retorno = new RetornoRotaVO();
        try {
            retorno = rotaService.excluirRota(rotaVO);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/limparTodasAsRotas/{idCliente}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRotaVO> excluirRotas(@PathVariable Integer idCliente) {

        RetornoRotaVO retorno = new RetornoRotaVO();
        try {
            rotaService.limparTodasAsRotas(idCliente);
            retorno.setCodigoRetorno(1);
            retorno.setMensagem("Rotas excluidas com sucesso");
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }
}
