package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.vo.RetornoVO;
import br.com.cmctransportes.cjm.domain.services.IntegracaoTruckControlService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "App/v2/web/integracaotruckcontrol")
public class IntegracaoTruckControlREST {

    @Autowired
    private IntegracaoTruckControlService integracaoTruckControlService;

    @RequestMapping(method = RequestMethod.GET, path = "/gerar", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoVO> gerar() {
        String error = null;
        RetornoVO retorno = new RetornoVO();
        try {
            integracaoTruckControlService.iniciar();
            retorno.setCodigoRetorno(1);
            retorno.setMensagem("Ok");
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }
}
