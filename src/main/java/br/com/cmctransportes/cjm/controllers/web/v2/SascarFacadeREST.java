package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.Sascar;
import br.com.cmctransportes.cjm.domain.entities.vo.RESTResponseVO;
import br.com.cmctransportes.cjm.domain.services.DriverService;
import br.com.cmctransportes.cjm.domain.services.SascarService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "App/v2/web/sascarconfig")
public class SascarFacadeREST {

    @Autowired
    private SascarService service;

    @Autowired
    private DriverService driverService;

    /**
     * Classe que faz a interação do front com o back e recebe um motorista e um objeto Sascar para salvar no banco para as futuras consultas no SOAP
     * @param driver motorista que está cadastrado no SOAP Sascar
     * @param sascar Objeto SASCAR necessário para armazenar no banco o novo usuario que irá ser integrado com a Sascar
     * @return
     */
    @PostMapping(path = "{driver}",consumes =  MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Sascar> create(@PathVariable("driver") Integer driver, @RequestBody Sascar sascar) {
        if (driver == null) {
            System.out.println("erro na gravação Sascar");
            throw new RuntimeException("Não foi possivel localizar todos os dados para salvar a Configuração Saascar");
        }
        String error = "";
        Sascar result = null;
        try {
            result = this.service.save(driver, sascar);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        if(result == null){
            error = "Motorista já cadastrado na integração sascar ou algum campo está nulo";
        }
        return new RESTResponseVO<>(error, result);
    }
}
