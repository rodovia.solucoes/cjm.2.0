package br.com.cmctransportes.cjm.controllers.web.v2;


import br.com.cmctransportes.cjm.domain.entities.Viagem;
import br.com.cmctransportes.cjm.domain.entities.vo.EnvioViagemVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoViagemVO;
import br.com.cmctransportes.cjm.domain.services.ViagemService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping(path = "App/v2/web/viagem")
public class ViagemFacadeREST {
    @Autowired
    private ViagemService viagemService;


    @RequestMapping(value = "/cadastrar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoViagemVO> cadastrar(@RequestBody EnvioViagemVO envioViagemVO) {

        RetornoViagemVO retorno = new RetornoViagemVO();
        try {
            if(Objects.nonNull(envioViagemVO.getListaDeViagem())) {
                this.viagemService.save(envioViagemVO.getListaDeViagem());
            }
            if(Objects.nonNull(envioViagemVO.getListaViagemExcluir())){
                this.viagemService.excluir(envioViagemVO.getListaViagemExcluir());
            }
            retorno.setMensagem("Viagem Cadastrado com Sucesso");
            retorno.setCodigoRetorno(1);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarPorVeiculo/{placa}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoViagemVO> buscarPorVeiculo(@PathVariable("placa") String placa) {

        RetornoViagemVO retorno = new RetornoViagemVO();
        try {
            List<Viagem> lista = this.viagemService.buscarPorVeiculo(placa);
            retorno.setListaViagens(lista);
            retorno.setCodigoRetorno(1);
            retorno.setMensagem("Lista carregada com sucesso!");
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }
}
