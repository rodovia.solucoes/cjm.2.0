package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.vo.EquipamentoPortatilVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoEquipamentoPortatilVO;
import br.com.cmctransportes.cjm.domain.services.EquipamentoPortatilService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "App/v2/web/equipamentoportatil")
public class EquipamentoPortatilFacedeREST {
    @Autowired
    private EquipamentoPortatilService equipamentoPortatilService;

    @RequestMapping(value = "/cadastrar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoEquipamentoPortatilVO> cadastrarEquipamento(@RequestBody EquipamentoPortatilVO equipamentoVO) {

        RetornoEquipamentoPortatilVO retorno = new RetornoEquipamentoPortatilVO();
        try {
            retorno = equipamentoPortatilService.cadastrar(equipamentoVO);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/editar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoEquipamentoPortatilVO> editar(@RequestBody EquipamentoPortatilVO equipamentoVO) {

        RetornoEquipamentoPortatilVO retorno = new RetornoEquipamentoPortatilVO();
        try {
            retorno = equipamentoPortatilService.editar(equipamentoVO);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscar/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoEquipamentoPortatilVO> buscarEquipamentoPeloCodigo(@PathVariable Integer id) {
        RetornoEquipamentoPortatilVO retorno = new RetornoEquipamentoPortatilVO();
        try {
            retorno = equipamentoPortatilService.buscar(id);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/listar/{idCliente}/{idUnidade}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoEquipamentoPortatilVO> buscarEquipamentoPeloCodigo(@PathVariable Integer idCliente, @PathVariable Integer idUnidade) {
        RetornoEquipamentoPortatilVO retorno = new RetornoEquipamentoPortatilVO();
        try {
            retorno = equipamentoPortatilService.listar(idCliente, idUnidade);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }
}
