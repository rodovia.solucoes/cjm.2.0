package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.comandos.ComandoService;
import br.com.cmctransportes.cjm.comandos.IComando;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoEquipamentoVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoVO;
import br.com.cmctransportes.cjm.domain.entities.vo.VeiculoVO;
import br.com.cmctransportes.cjm.domain.services.VeiculosService;
import br.com.cmctransportes.cjm.proxy.modelo.Veiculo;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "App/v2/web/comandos")
public class ComandoFacadeREST {

    @Autowired
    private VeiculosService veiculosService;


    @RequestMapping(value = "/buscarVersaoDoFirmawareQueclink/{idEmpresa}/{idUnidade}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoVO> buscarVersaoDoFirmawareQueclink(@PathVariable Integer idEmpresa, @PathVariable Integer idUnidade) {

        RetornoVO retorno = new RetornoVO();
        try {
            List<VeiculoVO> list = veiculosService.buscarVeiculos(idEmpresa, idUnidade);
            for(VeiculoVO vo : list){
                Veiculo v = new Veiculo();
                v.setId(vo.getId());
                List<IComando> l = ComandoService.getInstance().processarComando(vo.getId());
                for(IComando comando : l){
                    try {
                        comando.versaoDoFirmaware(v);
                    }catch (Exception e){

                    }
                }
            }
            retorno.setMensagem("Comandos Enviados");
            retorno.setCodigoRetorno(1);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    @RequestMapping(value = "/buscarNumeroDeSerieQueclink/{idEmpresa}/{idUnidade}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoVO> buscarNumeroDeSerieQueclink(@PathVariable Integer idEmpresa, @PathVariable Integer idUnidade) {

        RetornoVO retorno = new RetornoVO();
        try {
            List<VeiculoVO> list = veiculosService.buscarVeiculos(idEmpresa, idUnidade);
            for(VeiculoVO vo : list){
                Veiculo v = new Veiculo();
                v.setId(vo.getId());
                List<IComando> l = ComandoService.getInstance().processarComando(vo.getId());
                for(IComando comando : l){
                    try {
                        comando.buscarNumeroDeSerie(v);
                    }catch (Exception e){

                    }
                }
            }
            retorno.setMensagem("Comandos Enviados");
            retorno.setCodigoRetorno(1);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

}
