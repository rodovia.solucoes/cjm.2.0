package br.com.cmctransportes.cjm.controllers.mobile;

import br.com.cmctransportes.cjm.controllers.runnable.JornadaRunnble;
import br.com.cmctransportes.cjm.domain.entities.Evento;
import br.com.cmctransportes.cjm.domain.entities.Jornada;
import br.com.cmctransportes.cjm.domain.entities.vo.EventoMobileVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoEventoVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoJornadaVO;
import br.com.cmctransportes.cjm.domain.services.JornadaService;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import br.com.cmctransportes.cjm.utils.ThreadFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author analista.ti
 */
@RestController
@RequestMapping(path = "App/jornadas")
public class JornadaMobileREST {

    @Autowired
    private JornadaService js;

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Jornada create(@RequestBody Jornada entity) {
        js.save(entity);
        return entity;
    }



    @RequestMapping(value = "/cadastrarJornada", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoJornadaVO> cadastrarJornada(@RequestBody Jornada entity){
        RetornoJornadaVO retorno = new RetornoJornadaVO();
        try{
            js.save(entity);
            retorno.setJornada(entity);
            retorno.setCodigoRetorno(1);
            retorno.setMensagem("Jornada cadastrado com Sucesso.");
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
        }

        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST,  path = "ultimaJornada/", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Jornada adicionarJornada(@RequestBody Jornada entity) {
        js.save(entity);
        return entity;
    }

    @RequestMapping(method = RequestMethod.GET, path = "ultimaJornada/{motorista}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Jornada findLast(@PathVariable("motorista") Integer motorista) {
        return js.getLast(motorista);
    }
}