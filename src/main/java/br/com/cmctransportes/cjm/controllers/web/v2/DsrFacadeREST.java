package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.Dsr;
import br.com.cmctransportes.cjm.domain.entities.Paramcfg;
import br.com.cmctransportes.cjm.domain.entities.enums.PARAMCFG_GRUPO;
import br.com.cmctransportes.cjm.domain.entities.vo.DSRGridItemVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RESTResponseVO;
import br.com.cmctransportes.cjm.domain.services.DsrService;
import br.com.cmctransportes.cjm.domain.services.ParamcfgService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

/**
 * @author analista.ti
 */
@RestController
@RequestMapping(path = "App/v2/web/dsr")
public class DsrFacadeREST {

    @Autowired
    private DsrService dsrs;

    @Autowired
    private ParamcfgService paramcfgService;

    public DsrFacadeREST() {
    }

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Dsr> create(@RequestBody Dsr entity) {
        String error = "";
        try {
            dsrs.save(entity);
        } catch (ConstraintViolationException cause) {
            cause.printStackTrace();
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            e.printStackTrace();
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, entity);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Dsr> edit(@PathVariable("id") Integer id, @RequestBody Dsr entity) {
        String error = "";
        try {
            dsrs.update(entity);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, entity);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "{id}")
    public RESTResponseVO<Boolean> remove(@PathVariable("id") Integer id) {
        String error = "";
        boolean result = false;
        try {
            dsrs.delete(id);
            result = true;
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Dsr> find(@PathVariable("id") Integer id) {
        String error = "";
        Dsr drs = null;

        try {
            drs = dsrs.getById(id);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, drs);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<Dsr>> findAll() {
        String error = "";
        List<Dsr> result = Collections.emptyList();
        try {
            result = dsrs.findAll();
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/lista/{idEmpresa}/{idUnidade}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<Dsr>> findAll(@PathVariable("idEmpresa") Integer idEmpresa, @PathVariable("idUnidade") Integer idUnidade) {
        String error = "";
        List<Dsr> result = Collections.emptyList();
        ;
        try {
            result = dsrs.findAll(idEmpresa, idUnidade);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/lista/{idEmpresa}/{idUnidade}/{inicio}/{fim}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<Dsr>> findAll(@PathVariable("idEmpresa") Integer idEmpresa,
                                             @PathVariable("idUnidade") Integer idUnidade, @PathVariable("inicio") Long dataInicio,
                                             @PathVariable("fim") Long dataFim) {

        String error = "";
        List<Dsr> result = Collections.emptyList();
        ;
        try {
            result = dsrs.findAll(idEmpresa, idUnidade, dataInicio, dataFim);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/pormotorista/{motorista}/{inicio}/{fim}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<Dsr>> findAll(@PathVariable("motorista") Integer motorista,
                                             @PathVariable("inicio") Long dataInicio, @PathVariable("fim") Long dataFim) {

        String error = "";
        List<Dsr> result = Collections.emptyList();
        ;
        try {
            result = dsrs.findDsrList(motorista, dataInicio, dataFim);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, result);

    }

    @RequestMapping(method = RequestMethod.GET, path = "/params/{idEmpresa}/", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<Paramcfg>> pegaParamDSR(@PathVariable("idEmpresa") Integer idEmpresa) {

        String error = "";
        List<Paramcfg> result = Collections.emptyList();
        ;
        try {
            result = paramcfgService.findByGroup(PARAMCFG_GRUPO.DSR.name(), idEmpresa);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, result);

    }

    @RequestMapping(method = RequestMethod.GET, path = "/periodomotorista/{inicio}/{fim}/{motorista}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<DSRGridItemVO>> newFindAll(@PathVariable("inicio") Long dataInicio,
                                                          @PathVariable("fim") Long dataFim, @PathVariable("motorista") Integer motorista) {

        String error = "";
        List<DSRGridItemVO> result = Collections.emptyList();
        try {
            result = dsrs.fiindAll(motorista, dataInicio, dataFim);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, result);
    }
}