package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.TipoRanking;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoTipoRankingVO;
import br.com.cmctransportes.cjm.domain.services.TipoRankingService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping(path = "App/v2/web/tiposderanking")
public class TipoRankingFacadeREST {

    @Autowired
    private TipoRankingService tipoRankingService;

    @RequestMapping(method = RequestMethod.POST, value = "/cadastrar", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoTipoRankingVO> create(@RequestBody TipoRanking entity) {
        String error = null;
        RetornoTipoRankingVO retorno = new RetornoTipoRankingVO();
        try {
            tipoRankingService.save(entity);
            retorno.setCodigoRetorno(1);
            retorno.setMensagem("Cadastrado com sucesso");
        } catch (ConstraintViolationException cause) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(cause));
            retorno.setCodigoRetorno(-1);
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }

        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/editar", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoTipoRankingVO> edit(@RequestBody TipoRanking entity) {

        String error = null;
        RetornoTipoRankingVO retorno = new RetornoTipoRankingVO();
        try {
            tipoRankingService.update(entity);
            retorno.setCodigoRetorno(1);
            retorno.setMensagem("Editado com sucesso");
        } catch (ConstraintViolationException cause) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(cause));
            retorno.setCodigoRetorno(-1);
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }

        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "{id}")
    public ResponseEntity<RetornoTipoRankingVO> remove(@PathVariable("id") Integer id) {
        String error = null;
        boolean result = false;
        RetornoTipoRankingVO retorno = new RetornoTipoRankingVO();
        try {
            tipoRankingService.delete(id);
            result = true;
        } catch (ConstraintViolationException cause) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(cause));
            retorno.setCodigoRetorno(-1);
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }

        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/buscarPeloId/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoTipoRankingVO> find(@PathVariable("id") Integer id) {
        String error = null;
        TipoRanking entity = null;
        RetornoTipoRankingVO retorno = new RetornoTipoRankingVO();
        try {
            entity = tipoRankingService.getById(id);
            retorno.setCodigoRetorno(1);
            retorno.setTipoRanking(entity);
        } catch (ConstraintViolationException cause) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(cause));
            retorno.setCodigoRetorno(-1);
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }

        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.GET, value = "/lista/{idEmpresa}/{idUnidade}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoTipoRankingVO> findAll(@PathVariable("idEmpresa") Integer idEmpresa, @PathVariable("idUnidade") Integer idUnidade) {
        String error = null;
        List<TipoRanking> entity = Collections.emptyList();
        RetornoTipoRankingVO retorno = new RetornoTipoRankingVO();
        try {
            entity = tipoRankingService.buscarLista(idEmpresa, idUnidade);
            retorno.setCodigoRetorno(1);
            retorno.setListaDeTipoRanking(entity);
        } catch (ConstraintViolationException cause) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(cause));
            retorno.setCodigoRetorno(-1);
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }

        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

}
