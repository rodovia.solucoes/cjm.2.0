package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.vo.*;
import br.com.cmctransportes.cjm.domain.services.RankingMotoristaService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "App/v2/web/rankingmotoristas")
public class RankingMototristaFacadeREST {

    @Autowired
    private RankingMotoristaService rankingMotoristaService;

    @RequestMapping(method = RequestMethod.GET, value = "/resumoRanking/{idEmpresa}/{idUnidade}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRankingMototristaVO> resumoRanking(@PathVariable("idEmpresa") Integer idEmpresa, @PathVariable("idUnidade") Integer idUnidade) {
        String error = null;
        RetornoRankingMototristaVO retorno = new RetornoRankingMototristaVO();
        try {
            RankingMototristaVO rankingMototristaVO = rankingMotoristaService.buscarResumo(idEmpresa, idUnidade);
            retorno.setCodigoRetorno(1);
            retorno.setRankingMototrista(rankingMototristaVO);
        } catch (ConstraintViolationException cause) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(cause));
            retorno.setCodigoRetorno(-1);
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }

        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/buscarRankingMotorista/{idMotorista}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRankingMototristaVO> buscarRankingMotorista(@PathVariable("idMotorista") Integer idMotorista) {
        String error = null;
        RetornoRankingMototristaVO retorno = new RetornoRankingMototristaVO();
        try {
            RankingMototristaVO rankingMototristaVO = rankingMotoristaService.buscarRankingMotorista(idMotorista);
            retorno.setCodigoRetorno(1);
            retorno.setRankingMototrista(rankingMototristaVO);
        } catch (ConstraintViolationException cause) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(cause));
            retorno.setCodigoRetorno(-1);
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }

        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarPontuacaoAvancada", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRankingMototristaVO> buscarPontuacaoAvancada(@RequestBody FiltroVO filtro) {

        RetornoRankingMototristaVO retorno = new RetornoRankingMototristaVO();
        try {//List<RelatorioPontuacaoAvancadaVO> processarRelatorioPontuacaoAvancada
            List<RelatorioPontuacaoAvancadaVO> lista = rankingMotoristaService.processarRelatorioPontuacaoAvancada(filtro);
            retorno.setListaRelatorioPontuacaoAvancada(lista);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarTempoFaixa", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRankingMototristaVO> buscarTempoFaixa(@RequestBody FiltroVO filtro) {

        RetornoRankingMototristaVO retorno = new RetornoRankingMototristaVO();
        try {
            List<TemposDeFaixaVO> lista = rankingMotoristaService.processarRelatorioTempoDeFaixa(filtro);
            retorno.setListaTemposDeFaixa(lista);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

}
