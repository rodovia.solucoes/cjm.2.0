package br.com.cmctransportes.cjm.controllers.mobile;

import br.com.cmctransportes.cjm.domain.entities.FotoViaM;
import br.com.cmctransportes.cjm.domain.entities.vo.FotoViaMVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoImagemVO;
import br.com.cmctransportes.cjm.domain.services.FotoViaMService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "App/imagem")
public class ImagemREST {

    @Autowired
    private FotoViaMService fotoViaMService;

    @RequestMapping(value = "/cadastrarImagem", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoImagemVO> cadastrarImagem(@RequestBody FotoViaMVO fotoViaM){
        RetornoImagemVO retorno = new RetornoImagemVO();
        try{
            fotoViaMService.save(fotoViaM);
            retorno.setCodigoRetorno(1);
            retorno.setMensagem("Imagem cadastrada com Sucesso.");
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
        }

        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

}
