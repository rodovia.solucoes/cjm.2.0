package br.com.cmctransportes.cjm.controllers.mobile;

import br.com.cmctransportes.cjm.domain.entities.EventoMotorista;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoVO;
import br.com.cmctransportes.cjm.domain.services.EventoMotoristaService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "App/eventomotorista")
public class EventoMotoristaREST {

    @Autowired
    private EventoMotoristaService eventoMotoristaService;

    @RequestMapping(value = "/cadastrarEventoMotorista", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoVO> cadastrarEventoMotorista(@RequestBody EventoMotorista eventoMotorista){
        RetornoVO retorno = new RetornoVO();
        try{
            eventoMotoristaService.cadastrar(eventoMotorista);
            retorno.setCodigoRetorno(1);
            retorno.setMensagem("Evento Motorista cadastrado com Sucesso.");
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
        }

        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

}
