package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.vo.EquipamentoVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoEquipamentoVO;
import br.com.cmctransportes.cjm.domain.services.EquipamentoService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "App/v2/web/equipamento")
public class EquipamentoFacadeREST {

    @Autowired
    private EquipamentoService equipamentoService;


    @RequestMapping(value = "/cadastrarEquipamento", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoEquipamentoVO> cadastrarEquipamento(@RequestBody EquipamentoVO equipamentoVO) {

        RetornoEquipamentoVO retornoEquipamentoVO = new RetornoEquipamentoVO();
        try {
            retornoEquipamentoVO = equipamentoService.cadastrarEquipamento(equipamentoVO);
        } catch (ConstraintViolationException cause) {
            retornoEquipamentoVO.setCodigoRetorno(-1);
            retornoEquipamentoVO.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retornoEquipamentoVO.setMensagem(RodoviaUtils.exceptionMessage(e));
            retornoEquipamentoVO.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retornoEquipamentoVO, HttpStatus.OK);
    }

    @RequestMapping(value = "/editarEquipamento", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoEquipamentoVO> editarEquipamento(@RequestBody EquipamentoVO equipamentoVO) {

        RetornoEquipamentoVO retornoEquipamentoVO = new RetornoEquipamentoVO();
        try {
            retornoEquipamentoVO = equipamentoService.editarEquipamento(equipamentoVO);
        } catch (ConstraintViolationException cause) {
            retornoEquipamentoVO.setCodigoRetorno(-1);
            retornoEquipamentoVO.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retornoEquipamentoVO.setMensagem(RodoviaUtils.exceptionMessage(e));
            retornoEquipamentoVO.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retornoEquipamentoVO, HttpStatus.OK);
    }

    @RequestMapping(value = "/listarEquipamentoNaoAlocado", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoEquipamentoVO> listarEquipamentoNaoAlocado() {

        RetornoEquipamentoVO retornoEquipamentoVO = new RetornoEquipamentoVO();
        try {
            retornoEquipamentoVO = equipamentoService.listarEquipamentoNaoAlocado();
        } catch (ConstraintViolationException cause) {
            retornoEquipamentoVO.setCodigoRetorno(-1);
            retornoEquipamentoVO.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retornoEquipamentoVO.setMensagem(RodoviaUtils.exceptionMessage(e));
            retornoEquipamentoVO.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retornoEquipamentoVO, HttpStatus.OK);
    }


    @RequestMapping(value = "/listarEquipamentos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoEquipamentoVO> listarEquipamentos() {

        RetornoEquipamentoVO retornoEquipamentoVO = new RetornoEquipamentoVO();
        try {
            retornoEquipamentoVO = equipamentoService.listarEquipamentos();
        } catch (ConstraintViolationException cause) {
            retornoEquipamentoVO.setCodigoRetorno(-1);
            retornoEquipamentoVO.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retornoEquipamentoVO.setMensagem(RodoviaUtils.exceptionMessage(e));
            retornoEquipamentoVO.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retornoEquipamentoVO, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarEquipamentoPeloCodigo/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoEquipamentoVO> buscarEquipamentoPeloCodigo(@PathVariable Integer id) {

        RetornoEquipamentoVO retornoEquipamentoVO = new RetornoEquipamentoVO();
        try {
            retornoEquipamentoVO = equipamentoService.buscarEquipamentoPeloCodigo(id);
        } catch (ConstraintViolationException cause) {
            retornoEquipamentoVO.setCodigoRetorno(-1);
            retornoEquipamentoVO.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retornoEquipamentoVO.setMensagem(RodoviaUtils.exceptionMessage(e));
            retornoEquipamentoVO.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retornoEquipamentoVO, HttpStatus.OK);
    }


    @RequestMapping(value = "/listarEquipamentoMinimo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoEquipamentoVO> listarEquipamentoMinimo() {

        RetornoEquipamentoVO retornoEquipamentoVO = new RetornoEquipamentoVO();
        try {
            retornoEquipamentoVO = equipamentoService.listarEquipamentoMinimo();
        } catch (ConstraintViolationException cause) {
            retornoEquipamentoVO.setCodigoRetorno(-1);
            retornoEquipamentoVO.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retornoEquipamentoVO.setMensagem(RodoviaUtils.exceptionMessage(e));
            retornoEquipamentoVO.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retornoEquipamentoVO, HttpStatus.OK);
    }


}
