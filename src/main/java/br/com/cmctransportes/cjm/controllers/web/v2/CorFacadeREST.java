package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.vo.CorVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RESTResponseVO;
import br.com.cmctransportes.cjm.domain.services.CorService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * @author William Leite
 */
@RestController
@RequestMapping(path = "App/v2/web/cor")
public class CorFacadeREST {

    @Autowired
    private CorService cs;

    @RequestMapping(method = RequestMethod.GET, path = "{tipo}/{nome}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<CorVO>> find(@PathVariable("tipo") String tipo, @PathVariable("nome") String nome) {
        List<CorVO> cores = Collections.emptyList();
        String error = "";

        try {
            cores = cs.find(tipo, nome);
            if (cores.isEmpty()) {
                error = "Nenhuma cor encontrada!";
            }
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, cores);
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<CorVO> update(@RequestBody CorVO cor) {
        String error = "";

        try {
            if (Objects.isNull(cor.getCor()) || Objects.equals(cor.getCor(), "")) {
                error = "Campo Cor não pode ser vazio!";
            } else {
                error = cs.update(cor);
            }
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, cor);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "{tipo}/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Boolean> remove(@PathVariable("tipo") String tipo, @PathVariable("id") Integer id) {
        boolean result = false;
        String error = "";
        try {
            error = cs.remove(tipo, id);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, result);
    }
}
