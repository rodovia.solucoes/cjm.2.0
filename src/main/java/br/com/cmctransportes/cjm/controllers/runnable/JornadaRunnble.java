package br.com.cmctransportes.cjm.controllers.runnable;

import br.com.cmctransportes.cjm.domain.entities.*;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.singleton.MongoDBConexao;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.io.Serializable;
import java.util.Date;

/**
 * Gerencia a gravacao de jornadas dentro do mongo
 */
public class JornadaRunnble implements Runnable {

    private Evento evento;
    private String origem;
    private String jsonString;

    public JornadaRunnble(Evento evento, String origem) {
        this.evento = evento;
        this.origem = origem;
    }

    private void inserir() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            if("TECLADO".equalsIgnoreCase(origem)){
                EventoTemp eventoTemp = new EventoTemp().convert(evento);
                jsonString = mapper.writeValueAsString(eventoTemp);
            }else{
                jsonString = mapper.writeValueAsString(evento);
            }


            Document document = Document.parse(jsonString);
            MongoDatabase mdb = MongoDBConexao.getMongoDBConexao().pegarBaseRodovia();
            MongoCollection<Document> coll = mdb.getCollection("evento_viam");
            coll.insertOne(document);
        } catch (Exception e) {
            LogSistema.logError("inserir: " + origem + " - ", e);
        }
    }

    @Override
    public void run() {
        inserir();
    }


}

class EventoTemp implements Serializable {

    static final long serialVersionUID = 1L;
    Integer id;
    Date instanteEvento; // Este é sempre o instante que será levado em consideração
    Date instanteLancamento; //
    Date instanteOriginal; // Caso o lançamento seja alterado, aqui fica o valor ORIGINAL, se a alteração for alterada, não modifica esse valor que estará preenchido
    EventosJornada tipoEvento;
    Users operadorLancamento; // usuário que fez o lançamento original
    Double longitude;
    Double latitude;
    String origem;
    String positionAddress;
    Integer jornada;
    Integer eventoAnteriorId;


    EventoTemp convert(Evento evento){
        EventoTemp eventoTemp = new EventoTemp();
        eventoTemp.id = evento.getId();
        eventoTemp.instanteEvento = evento.getInstanteEvento();
        eventoTemp.instanteLancamento = evento.getInstanteLancamento();
        eventoTemp.instanteOriginal = evento.getInstanteOriginal();
        eventoTemp.tipoEvento = evento.getTipoEvento();
        eventoTemp.operadorLancamento = evento.getOperadorLancamento();
        eventoTemp.longitude = evento.getLongitude();
        eventoTemp.latitude = evento.getLatitude();
        eventoTemp.origem = evento.getOrigem();
        eventoTemp.positionAddress = evento.getPositionAddress();
        eventoTemp.jornada = evento.getJornadaId().getId();
        eventoTemp.eventoAnteriorId = 12;
        return  eventoTemp;
    }

    public Integer getId() {
        return id;
    }

    public Date getInstanteEvento() {
        return instanteEvento;
    }

    public Date getInstanteLancamento() {
        return instanteLancamento;
    }

    public Date getInstanteOriginal() {
        return instanteOriginal;
    }

    public EventosJornada getTipoEvento() {
        return tipoEvento;
    }

    public Users getOperadorLancamento() {
        return operadorLancamento;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public String getOrigem() {
        return origem;
    }

    public String getPositionAddress() {
        return positionAddress;
    }

    public Integer getJornada() {
        return jornada;
    }

    public Integer getEventoAnteriorId() {
        return eventoAnteriorId;
    }
}
