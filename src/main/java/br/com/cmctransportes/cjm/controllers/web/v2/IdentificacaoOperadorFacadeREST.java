package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.IdentificacaoOperador;
import br.com.cmctransportes.cjm.domain.entities.vo.*;
import br.com.cmctransportes.cjm.domain.services.IdentificacaoOperadorService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "App/v2/web/identificacaooperador")
public class IdentificacaoOperadorFacadeREST {

    @Autowired
    private IdentificacaoOperadorService identificacaoOperadorService;

    @RequestMapping(value = "/buscarIdentificacaoOperador/{idEmpresa}/{idUnidade}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoIdentificacaoOperadorVO> buscarIdentificacaoOperador(@PathVariable Integer idEmpresa, @PathVariable Integer idUnidade) {
        RetornoIdentificacaoOperadorVO retorno = new RetornoIdentificacaoOperadorVO();
        try {
            List<IdentificacaoOperador> lista = identificacaoOperadorService.buscarIdentificacaoOperador(idEmpresa, idUnidade);
            retorno.setListaIdentificacaoOperador(lista);
            retorno.setMensagem("Lista carregada com sucesso");
            retorno.setCodigoRetorno(1);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarMotoristaOperador/{idEmpresa}/{idUnidade}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoIdentificacaoOperadorVO> buscarMotoristaOperador(@PathVariable Integer idEmpresa, @PathVariable Integer idUnidade) {
        RetornoIdentificacaoOperadorVO retorno = new RetornoIdentificacaoOperadorVO();
        try {
            List<MotoristaOperadorVO> lista = identificacaoOperadorService.buscarMotoristaOperador(idEmpresa, idUnidade);
            retorno.setListaMotoristaOperador(lista);
            retorno.setMensagem("Lista carregada com sucesso");
            retorno.setCodigoRetorno(1);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    @RequestMapping(value = "/buscarVeiculoAutorizacao/{idEmpresa}/{idUnidade}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoIdentificacaoOperadorVO> buscarVeiculoAutorizacao(@PathVariable Integer idEmpresa, @PathVariable Integer idUnidade) {
        RetornoIdentificacaoOperadorVO retorno = new RetornoIdentificacaoOperadorVO();
        try {
            List<VeiculoAutorizacaoVO> lista = identificacaoOperadorService.buscarVeiculoAutorizacao(idEmpresa, idUnidade);
            retorno.setListaVeiculoAutorizacao(lista);
            retorno.setMensagem("Lista carregada com sucesso");
            retorno.setCodigoRetorno(1);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarIdentificacaoOperador/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoIdentificacaoOperadorVO> buscarIdentificacaoOperador(@PathVariable Integer id) {
        RetornoIdentificacaoOperadorVO retorno = new RetornoIdentificacaoOperadorVO();
        try {
            IdentificacaoOperador identificacaoOperador = identificacaoOperadorService.buscarIdentificacaoOperadorPeloId(id);
            retorno.setIdentificacaoOperador(identificacaoOperador);
            retorno.setMensagem("Lista carregada com sucesso");
            retorno.setCodigoRetorno(1);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    @RequestMapping(value = "/cadastrar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoIdentificacaoOperadorVO> cadastrar(@RequestBody IdentificacaoOperador identificacaoOperador) {

        RetornoIdentificacaoOperadorVO retorno = new RetornoIdentificacaoOperadorVO();
        try {
            identificacaoOperadorService.cadastrar(identificacaoOperador);
            retorno.setCodigoRetorno(1);
            retorno.setIdentificacaoOperador(identificacaoOperador);
            retorno.setMensagem("Cadastro realizado com sucesso");
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/editar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoIdentificacaoOperadorVO> editar(@RequestBody IdentificacaoOperador identificacaoOperador) {

        RetornoIdentificacaoOperadorVO retorno = new RetornoIdentificacaoOperadorVO();
        try {
            identificacaoOperadorService.editar(identificacaoOperador);
            retorno.setCodigoRetorno(1);
            retorno.setIdentificacaoOperador(identificacaoOperador);
            retorno.setMensagem("Edição realizado com sucesso");
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

}
