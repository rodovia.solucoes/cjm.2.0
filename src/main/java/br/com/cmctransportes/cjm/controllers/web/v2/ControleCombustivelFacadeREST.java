package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.ControleCombustivel;
import br.com.cmctransportes.cjm.domain.entities.vo.EnvioControleCombustivelVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoControleCombustivelVO;
import br.com.cmctransportes.cjm.domain.services.ControleCombustivelService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping(path = "App/v2/web/controlecombustivel")
public class ControleCombustivelFacadeREST {

    @Autowired
    private ControleCombustivelService controleCombustivelService;

    @RequestMapping(value = "/cadastrar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoControleCombustivelVO> cadastrar(@RequestBody ControleCombustivel controleCombustivel) {

        RetornoControleCombustivelVO retorno = new RetornoControleCombustivelVO();
        try {
            controleCombustivelService.save(controleCombustivel);
            retorno.setMensagem("Controle Combustivel Cadastrado com Sucesso");
            retorno.setCodigoRetorno(1);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/ajustar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoControleCombustivelVO> ajustar(@RequestBody EnvioControleCombustivelVO controleCombustivel) {

        RetornoControleCombustivelVO retorno = new RetornoControleCombustivelVO();
        try {
            if(Objects.nonNull(controleCombustivel.getListaControleCombustivel())){
                for(ControleCombustivel cc : controleCombustivel.getListaControleCombustivel()){
                    controleCombustivelService.save(cc);
                }
            }

            if(Objects.nonNull(controleCombustivel.getListaIdExcluir())){
                for(Integer id: controleCombustivel.getListaIdExcluir()){
                    this.controleCombustivelService.excluir(id);
                }
            }
            retorno.setMensagem("Controle Combustivel Cadastrado com Sucesso");
            retorno.setCodigoRetorno(1);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }



    @RequestMapping(value = "/buscarPorVeiculo/{idVeiculo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoControleCombustivelVO> buscarPorVeiculo(@PathVariable("idVeiculo") Integer idVeiculo) {

        RetornoControleCombustivelVO retorno = new RetornoControleCombustivelVO();
        try {
            List<ControleCombustivel>  lista = this.controleCombustivelService.buscarPorVeiculo(idVeiculo);
            retorno.setListaControleCombustivel(lista);
            retorno.setCodigoRetorno(1);
            retorno.setMensagem("Lista carregada com sucesso!");
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/excluir/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoControleCombustivelVO> excluir(@PathVariable("id") Integer id) {

        RetornoControleCombustivelVO retorno = new RetornoControleCombustivelVO();
        try {
            this.controleCombustivelService.excluir(id);
            retorno.setCodigoRetorno(1);
            retorno.setMensagem(id+" excluido com sucesso!");
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

}
