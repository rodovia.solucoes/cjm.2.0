package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.Unidades;
import br.com.cmctransportes.cjm.domain.entities.vo.RESTResponseVO;
import br.com.cmctransportes.cjm.domain.services.UnidadesService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

/**
 * @author analista.ti
 */
@RestController
@RequestMapping(path = "App/v2/web/unidades")
public class UnidadesFacadeREST {

    @Autowired
    private UnidadesService us;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Unidades> create(@RequestBody Unidades entity) {
        String error = "";
        try {
            us.save(entity);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, entity);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Unidades> edit(@PathVariable("id") Integer id, @RequestBody Unidades entity) {
        String error = "";
        try {
            us.update(entity);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        if (error.isEmpty()) {
            return this.find(id);
        } else {
            return new RESTResponseVO<>(error, entity);
        }
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "{id}")
    public RESTResponseVO<Boolean> remove(@PathVariable("id") Integer id) {
        String error = "";
        boolean result = false;

        try {
            us.delete(id);
            result = true;
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);

    }

    @RequestMapping(method = RequestMethod.GET, path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Unidades> find(@PathVariable("id") Integer id) {
        String error = "";
        Unidades unidades = null;

        try {
            unidades = us.getById(id);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, unidades);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<Unidades>> findAll() {
        String error = "";
        List<Unidades> result = Collections.emptyList();
        try {
            result = us.findAll();
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/lista/{idEmpresa}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<Unidades>> findAll(@PathVariable("idEmpresa") Integer idEmpresa) {
        String error = "";
        List<Unidades> result = Collections.emptyList();
        try {
            result = us.findAll(idEmpresa);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }


    @RequestMapping(method = RequestMethod.GET, path = "/buscarValorDiaria/{idMotorista}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<String> buscarValorDiaria(@PathVariable("idMotorista") Integer idMotorista) {
        String error = "";
        String valor = "";
        try {
           Double v = us.buscarValorDiaria(idMotorista);
           valor = RodoviaUtils.convertDoubleToMoneyReal(v);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, valor);
    }
}