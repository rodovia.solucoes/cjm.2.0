package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.*;
import br.com.cmctransportes.cjm.domain.entities.vo.*;
import br.com.cmctransportes.cjm.domain.services.DashboardService;
import br.com.cmctransportes.cjm.domain.services.EmpresasService;
import br.com.cmctransportes.cjm.domain.services.FotoViaMService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping(path = "App/v2/web/dashboard")
public class DashboardREST {
    @Autowired
    private DashboardService dashboardService;
    @Autowired
    private EmpresasService empresasService;
    @Autowired
    private FotoViaMService fotoViaMService;

    @RequestMapping(value = "/buscarListaDeVeiculoPorEmpresa/{idEmpresa}/{idUnidade}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoMapaVO> buscarListaDeVeiculoPorEmpresa(@PathVariable Integer idEmpresa, @PathVariable Integer idUnidade) {
        RetornoMapaVO retorno = new RetornoMapaVO();
        try {
            Empresas empresas = empresasService.getById(idEmpresa);
            if(empresas.getHabilitarViaSat() != null && empresas.getHabilitarViaSat()){
                retorno = dashboardService.buscarListaDeAparelhosEVeiculos(new EmpresaVO(idEmpresa), idUnidade);
                retorno.setViasatAtivo(true);
            }else{
                retorno.setCodigoRetorno(120);
                retorno.setViasatAtivo(false);
            }
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    @RequestMapping(value = "/buscarDadosDoVeiculo/{idVeiculo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoDashboardVO> buscarDadosDoVeiculo(@PathVariable Integer idVeiculo) {

        RetornoDashboardVO retorno = new RetornoDashboardVO();
        try {
            retorno = dashboardService.buscarDadosDoVeiculo(new VeiculoVO(idVeiculo));
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarDadosDoEquipamentoPortatil/{idEquipamentoPortatil}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoDashboardVO> buscarDadosDoEquipamentoPortatil(@PathVariable Integer idEquipamentoPortatil) {

        RetornoDashboardVO retorno = new RetornoDashboardVO();
        try {
            retorno = dashboardService.buscarDadosDoEquipamentoPortatil(new EquipamentoPortatilVO(idEquipamentoPortatil));
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarDadosDoViam/{idMotorista}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoDashboardVO> buscarDadosDoViam(@PathVariable Integer idMotorista) {

        RetornoDashboardVO retorno = new RetornoDashboardVO();
        try {
            DetalheDashboardVO detalheDashboardVO = dashboardService.buscarDadosDoViam(new MotoristaVO(idMotorista));
            retorno.setDetalheDashboard(detalheDashboardVO);
            retorno.setCodigoRetorno(291);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/salvarDashboard", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Boolean> salvarDashboard(@RequestBody Dashboard dashboard) {
        String error = null;
        Boolean result = false;
        try {
            dashboardService.gravarDashboard(dashboard);
            result = true;
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(value = "/buscarIdJornada/{idEmpresa}/{idUnidade}/{idUsuario}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Integer> buscarIdJornada(@PathVariable Integer idEmpresa, @PathVariable Integer idUnidade, @PathVariable Integer idUsuario) {
        String error = null;
        Integer id = 0;
        try {
            id = dashboardService.buscarIdJornada(idEmpresa, idUnidade, idUsuario);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, id);
    }


    @RequestMapping(value = "/mostrarTodosNaTela/{idEmpresa}/{idUnidade}/{idUsuario}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Boolean> mostrarTodosNaTela(@PathVariable Integer idEmpresa, @PathVariable Integer idUnidade, @PathVariable Integer idUsuario) {
        String error = null;
        Boolean mostrar = Boolean.FALSE;
        try {
            mostrar = dashboardService.mostrarTodosNaTela(idEmpresa, idUnidade, idUsuario);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, mostrar);
    }


    @RequestMapping(value = "/buscarFotosNaoVistas/{idMotorista}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoDashboardVO>  buscarFotosNaoVistas(@PathVariable Integer idMotorista) {
        RetornoDashboardVO retorno = new RetornoDashboardVO();
        try {
            List<FotoViaM> lista = fotoViaMService.buscarFotosNaoVistas(new Motoristas(idMotorista));
            lista.forEach(foto->{
                foto.setMotoristas(null);
                foto.setDataFormatada(new SimpleDateFormat("dd/MM/yyyy HH:mm").format(foto.getDataDaFoto()));
            });
            retorno.setListaFotoViaM(lista);
            retorno.setCodigoRetorno(291);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    @RequestMapping(value = "/editarFotoParaVista/{idFoto}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoDashboardVO>  editarFotoParaVista(@PathVariable Integer idFoto) {
        RetornoDashboardVO retorno = new RetornoDashboardVO();
        try {
            fotoViaMService.passarFotoParaVista(idFoto);
            retorno.setCodigoRetorno(291);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    @RequestMapping(value = "/timelineViaM/{idMotorista}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoDashboardVO>  timelineViaM(@PathVariable Integer idMotorista) {
        RetornoDashboardVO retorno = new RetornoDashboardVO();
        try {
            DetalheDashboardVO detalheDashboardVO = dashboardService.buscarUltimosEventos(new MotoristaVO(idMotorista));
            List<TimelineVO> lista = new ArrayList<>();
            detalheDashboardVO.getListaEvento().forEach(e ->{
                TimelineVO vo = new TimelineVO();
                vo.setData(e.getDataEvento());
                vo.setDescricao(e.getDescricao());
                lista.add(vo);
            });

            retorno.setListaTimelineVO(lista);
            retorno.setCodigoRetorno(291);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    @RequestMapping(value = "/timelineViaSat/{idVeiculo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoDashboardVO>  timelineViaSat(@PathVariable Integer idVeiculo) {
        RetornoDashboardVO retorno = new RetornoDashboardVO();
        try {
            List<TimelineVO> lista = dashboardService.buscarUltimosEventos(idVeiculo);
            retorno.setListaTimelineVO(lista);
            retorno.setCodigoRetorno(291);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarTrajettoUltimas24horas/{idVeiculo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoTransmissaoVO>  buscarTrajettoUltimas24horas(@PathVariable Integer idVeiculo) {
        RetornoTransmissaoVO retorno = new RetornoTransmissaoVO();
        try {
            retorno = dashboardService.buscarTrajettoUltimas24horas(idVeiculo);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarTrajettoPorPeriodo/{idVeiculo}/{dataInicial}/{dataFinal}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoTransmissaoVO>  buscarTrajettoPorPeriodo(@PathVariable Integer idVeiculo, @PathVariable Long dataInicial, @PathVariable Long dataFinal) {
        RetornoTransmissaoVO retorno = new RetornoTransmissaoVO();
        try {
            retorno = dashboardService.buscarTrajetto(idVeiculo, dataInicial, dataFinal);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/comando/{idComando}/{idVeiculo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoTransmissaoVO>  comando(@PathVariable Integer idComando, @PathVariable Integer idVeiculo) {
        RetornoTransmissaoVO retorno = new RetornoTransmissaoVO();
        try {
            dashboardService.enviarComandos(idComando, idVeiculo);
            retorno.setCodigoRetorno(1);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.POST, path = "/salvarRotaVeiculo", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<RetornoRotaVO> salvarRotaVeiculo(@RequestBody RotaVeiculo rotaVeiculo) {
        RetornoRotaVO result = new RetornoRotaVO();
        String error = null;
        try {
            dashboardService.gravarRotaVeiculo(rotaVeiculo);
            List<RotaVO> l = dashboardService.buscarListaDeRotas(rotaVeiculo.getVeiculo());
            result.setListaDeRota(l);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(value = "/excluirRotaVeiculo/{idRotaVeiculo}/{idDoVeiculo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRotaVO>  excluirRotaVeiculo(@PathVariable Integer idRotaVeiculo, @PathVariable Integer idDoVeiculo) {
        RetornoRotaVO retorno = new RetornoRotaVO();
        try {
            dashboardService.excluirRotaVeiculo(idRotaVeiculo, idDoVeiculo);
            List<RotaVO> l = dashboardService.buscarListaDeRotas(idDoVeiculo);
            retorno.setListaDeRota(l);
            retorno.setCodigoRetorno(1);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

}
