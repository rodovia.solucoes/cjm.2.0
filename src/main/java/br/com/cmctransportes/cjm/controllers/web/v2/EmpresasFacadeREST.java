package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.Empresas;
import br.com.cmctransportes.cjm.domain.entities.vo.RESTResponseVO;
import br.com.cmctransportes.cjm.domain.services.EmpresasService;
import br.com.cmctransportes.cjm.utils.ArquivoDeConfiguracao;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * #2033016
 *
 * @author William Leite
 */
@RestController
@RequestMapping(path = "App/v2/web/empresas")
public class EmpresasFacadeREST {

    @Autowired
    private EmpresasService service;

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Empresas> create(@RequestBody Empresas entity) {
        String error = "";
        try {
            if (Objects.nonNull(entity.getCnpj())) {
                entity.setId(null);
                String cnpj = entity.getCnpj();
                cnpj = cnpj.replaceAll("\\[./-]", "");
                entity.setCnpj(cnpj);
                service.inserirEmpresaNativo(entity);
            } else {
                error = "CNPJ não preenchido";
            }
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            e.printStackTrace();
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, entity);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Empresas> edit(@PathVariable("id") Integer id, @RequestBody Empresas entity) {
        String error = "";
        try {
            service.update(entity);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, entity);

    }

    @RequestMapping(method = RequestMethod.DELETE, path = "{id}")
    public RESTResponseVO<Boolean> remove(@PathVariable("id") Integer id) {

        String error = "";
        boolean result = false;
        try {
            service.delete(id);
            result = true;
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);

    }

    @RequestMapping( method = RequestMethod.GET, path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Empresas> find(@PathVariable("id") Integer id) {
        String error = "";
        Empresas empresas = null;

        try {
            empresas = service.getById(id);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, empresas);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<Empresas>> findAll() {
        String error = "";
        List<Empresas> result = Collections.emptyList();
        try {
            result = service.findAll();
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/buscarTodosParaContrato", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<Empresas>> buscarTodosParaContrato() {
        String error = "";
        List<Empresas> result = Collections.emptyList();
        try {
            result = service.buscarEmpresasContrato();
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, result);
    }


}