package br.com.cmctransportes.cjm.controllers.mobile;

import br.com.cmctransportes.cjm.domain.entities.Motoristas;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoParametroVO;
import br.com.cmctransportes.cjm.domain.entities.vo.TurnoVO;
import br.com.cmctransportes.cjm.domain.services.DriverService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "App/parametros")
public class ParametrosREST {

    @Autowired
    private DriverService driverService;

    @RequestMapping(method = RequestMethod.GET, path = "/buscarParametro/{idUser}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoParametroVO> buscarParametro(@PathVariable("idUser") Integer idUser) {
        String error = null;
        RetornoParametroVO retorno = new RetornoParametroVO();
        try {
            Motoristas motoristas = driverService.getByUserId(idUser);
            retorno.setParametro(motoristas.getObjEmpresa().getParametro());
            retorno.setTurno(new TurnoVO(motoristas.getTurnosId()));
            retorno.setCodigoRetorno(1);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }
}
