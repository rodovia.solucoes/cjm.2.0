package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.Ferias;
import br.com.cmctransportes.cjm.domain.entities.vo.RESTResponseVO;
import br.com.cmctransportes.cjm.domain.services.FeriasService;
import br.com.cmctransportes.cjm.domain.services.JornadaService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import br.com.cmctransportes.cjm.utils.TimeHelper;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author William Leite
 */
@RestController
@RequestMapping(path = "App/v2/web/ferias")
public class FeriasFacadeREST {

    @Autowired
    private FeriasService fs;
    @Autowired
    private JornadaService jornadaService;

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Ferias> create(@RequestBody Ferias entity) {
        String error = "";
        try {
            fs.save(entity);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, entity);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Ferias> edit(@PathVariable("id") Integer id, @RequestBody Ferias entity) {
        String error = "";
        try {
            fs.update(entity);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, entity);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "{id}")
    public RESTResponseVO<Boolean> remove(@PathVariable("id") Integer id) {
        String error = "";
        boolean result = false;
        try {
            Ferias ferias = fs.getById(id);
            jornadaService.deletarJornadaComFerias(TimeHelper.getDate000000(ferias.getDataInicio()), TimeHelper.getDate235959(ferias.getDataFinal()), ferias.getMotoristaId());
            fs.delete(id);
            result = true;
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Ferias> find(@PathVariable("id") Integer id) {
        Ferias entity = null;
        String error = "";
        try {
            entity = fs.getById(id);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, entity);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<Ferias>> findAll() {
        String error = "";
        List<Ferias> result = Collections.emptyList();
        try {
            result = fs.findAll();
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "{motorista}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<Ferias>> findByDriver(@PathVariable("motorista") Integer motorista) {
        String error = "";
        List<Ferias> result = Collections.emptyList();
        try {
            result = fs.findByDriver(motorista);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }
}