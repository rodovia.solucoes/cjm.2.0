package br.com.cmctransportes.cjm.controllers.mobile;

import br.com.cmctransportes.cjm.domain.entities.vo.AlertaViamVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoAlertaVO;
import br.com.cmctransportes.cjm.domain.services.AlertaViamService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "App/alerta")
public class AlertaMobileREST {

    @Autowired
    private AlertaViamService alertaViamService;

    @RequestMapping(value = "/cadastrarAlerta", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoAlertaVO> cadastrarAlerta(@RequestBody AlertaViamVO alertaViamVO){
        RetornoAlertaVO retorno = new RetornoAlertaVO();
        try{
             alertaViamService.save(alertaViamVO);
             retorno.setCodigoRetorno(1);
             retorno.setMensagem("Alerta cadastrado com Sucesso.");
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
        }

        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }
}
