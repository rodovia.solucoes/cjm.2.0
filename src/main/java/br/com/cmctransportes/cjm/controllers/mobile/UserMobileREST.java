package br.com.cmctransportes.cjm.controllers.mobile;

import br.com.cmctransportes.cjm.domain.entities.MobileUser;
import br.com.cmctransportes.cjm.domain.entities.Users;
import br.com.cmctransportes.cjm.domain.services.UserService;
import br.com.cmctransportes.cjm.proxy.UsersOpenFireProxy;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.logging.Logger;

/**
 * @author analista.ti
 */
@RestController
@RequestMapping(path = "App/users")
public class UserMobileREST {

    @Autowired
    private UserService us;

    private static final Logger log = Logger.getLogger("UserMobileREST");

    @RequestMapping(method = RequestMethod.PUT, path = "{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public MobileUser edit(@PathVariable("id") Integer id, @RequestBody Users user) {
        MobileUser mUsr = null;

        try {
            user.setSenhaCripto(RodoviaUtils.cryptWithMD5(user.getSenha()));
            us.update(user);
            mUsr = new MobileUser(user);
            UsersOpenFireProxy.getInstance().alterarUsuario(user.getUserName().trim(), user.getSenha().trim());
        } catch (Exception e) {
            log.info(e.getLocalizedMessage());
        }

        return mUsr;
    }
}