package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.controllers.runnable.JornadaRunnble;
import br.com.cmctransportes.cjm.domain.entities.Jornada;
import br.com.cmctransportes.cjm.domain.entities.Users;
import br.com.cmctransportes.cjm.domain.entities.jornada.RelatorioApuracaoDeJornada;
import br.com.cmctransportes.cjm.domain.entities.jornada.RelatorioRegistroDeJornadas;
import br.com.cmctransportes.cjm.domain.entities.vo.*;
import br.com.cmctransportes.cjm.domain.services.JornadaService;
import br.com.cmctransportes.cjm.domain.services.UnidadesService;
import br.com.cmctransportes.cjm.domain.services.UserService;
import br.com.cmctransportes.cjm.services.JourneyCalculusService;
import br.com.cmctransportes.cjm.singleton.MongoDBConexao;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import br.com.cmctransportes.cjm.utils.ThreadFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * @author analista.ti
 */
@RestController
@RequestMapping(path = "App/v2/web/jornadas")
public class JourneyFacadeREST {

    @Autowired
    private JornadaService js;

    @Autowired
    private UserService userService;

    @Autowired
    private UnidadesService unidadesService;

    @Autowired
    private JourneyCalculusService journeyCalculusService;

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Jornada> create(@RequestBody Jornada entity) {
        String error = "";
        try {
            js.save(entity);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, entity);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Jornada> edit(@PathVariable("id") Integer id, @RequestBody Jornada entity) {
        String error = "";
        try {
            js.update(entity);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, entity);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "{id}")
    public RESTResponseVO<Boolean> remove(@PathVariable("id") Integer id) {
        String error = "";
        boolean result = false;

        try {
            js.delete(id);
            result = true;
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.POST, path = "closeCalculation/{start}/{end}/{driver}/{user}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<String> closeCalculation(@PathVariable("start") Long start, @PathVariable("end") Long end, @PathVariable("driver") Integer driver, @PathVariable("user") Integer user) {
        String result = "";
        String error = "";

        try {
            result = js.closeCalculation(start, end, driver, user);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.POST, path = "preencherJornada/{id}/{user}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Boolean> fillJourney(@PathVariable("id") Integer id, @PathVariable("user") Integer user) {
        Boolean result = false;
        String error = "";

        try {
            Jornada journey = js.getById(id);
            Users dbUser = userService.getById(user);

            if (Objects.equals(journey.getLocked(), Boolean.TRUE) && dbUser.getUsersClaims().stream().noneMatch(c -> Objects.equals(c.getClaim(), "apuracao"))) {
                throw new RuntimeException("Jornada bloqueada e usuário não possui permissão para efetuar alteração!");
            }

            error = js.fillJourney(id, user);
            if (Objects.isNull(error) || error.isEmpty()) {
                result = true;
            }
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.POST, path = "abonoAlmoco/{id}/{user}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Boolean> fillJourneyLunch(@PathVariable("id") Integer id, @PathVariable("user") Integer user) {
        Boolean result = false;
        String error = "";

        try {
            Jornada journey = this.js.getById(id);

            Users dbUser = userService.getById(user);

            if (Objects.equals(journey.getLocked(), Boolean.TRUE) && dbUser.getUsersClaims().stream().noneMatch(c -> Objects.equals(c.getClaim(), "apuracao"))) {
                throw new RuntimeException("Jornada bloqueada e usuário não possui permissão para efetuar alteração!");
            }

            error = js.fillJourney(id, user, false);
            if (Objects.isNull(error) || error.isEmpty()) {
                result = true;
            }
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Jornada> find(@PathVariable("id") Integer id) {
        String error = "";
        Jornada journey = null;
        try {
            journey = js.getById(id);
            journeyCalculusService.processEvents(journey, null,true);

            journey.getEventos().forEach(e->{
                e.getJornadaId().getMotoristasId().setImagemId(null);
                if(Objects.nonNull(e.getOperadorAlteracao())) {
                    e.setOperadorAlteracao(null);
                }
                if(Objects.nonNull(e.getEventoSeguinte())) {
                    e.getEventoSeguinte().setOperadorLancamento(null);
                    e.getEventoSeguinte().setEventoSeguinte(null);
                }
            });

            if(Objects.nonNull(journey.getResumoTiposEstados())) {
                journey.getResumoTiposEstados().forEach((k, v) -> {
                    v.getLista().forEach(e -> {
                        e.setOperadorLancamento(null);
                        if (Objects.nonNull(e.getEventoSeguinte())) {
                            e.setEventoSeguinte(null);
                        }
                    });
                });
            }

            journey.getMotoristasId().setImagemId(null);

            js.fixEventLocation(journey);

        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, journey);
    }

    @RequestMapping(method = RequestMethod.GET, path = "ultimaJornada/{idEmpresa}/{idUnidade}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<PainelDiarioVO>> findAllLast(@PathVariable("idEmpresa") Integer empresa, @PathVariable("idUnidade") Integer unidade) {

        List<PainelDiarioVO> result = Collections.emptyList();
        String error = "";

        try {
            result = js.getAllLast(empresa, unidade);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "motoristas/{jornadas}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<MotoristaVO>> findDriversByJourney(@PathVariable("jornadas") String jornadas) {
        final List<MotoristaVO> result = new ArrayList<>();
        String error = "";

        try {
            Arrays.stream(jornadas.split(",")).forEach((jid) -> {
                Jornada j = js.getById(Integer.parseInt(jid));
                this.journeyCalculusService.processEvents(j, null,true);

                MotoristaVO vo = new MotoristaVO(j.getMotoristasId(), this.userService, this.unidadesService);
                vo.setFaltantes(j.getHorasFaltosas());
                vo.setJornadaId(j.getId());
                result.add(vo);
            });
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "period/{driver}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Map<Integer, List<MonthVO>>> period(@PathVariable("driver") Integer driver) {
        Map<Integer, List<MonthVO>> result = null;
        String error = "";

        try {
            result = js.period(driver);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "registroJornada/{mes}/{motorista}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<RelatorioRegistroDeJornadas> geraRelatorio(@PathVariable("mes") Integer mesReferencia, @PathVariable("motorista") Integer motorista) {
        RelatorioRegistroDeJornadas result = null;
        String error = "";

        try {
            result = js.getRegistroDeJornada(motorista, mesReferencia);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "registroJornada/{mes}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<RelatorioRegistroDeJornadas>> geraRelatorio(@PathVariable("mes") Integer mesReferencia) {
        List<RelatorioRegistroDeJornadas> result = Collections.emptyList();
        String error = "";

        try {
            result = js.getRegistroDeJornada(mesReferencia);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    /**
     * @param dataInicio
     * @param motorista
     * @param motorista
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, path = "registroJornada/{inicio}/{fim}/{motorista}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<RelatorioRegistroDeJornadas> geraRelatorio(@PathVariable("inicio") Long dataInicio, @PathVariable("fim") Long dataFim, @PathVariable("motorista") Integer motorista) {

        RelatorioRegistroDeJornadas result = null;
        String error = "";

        try {
            result = js.getRegistroDeJornada(motorista, dataInicio, dataFim);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "apuracaoJornada/{mes}/{motorista}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<RelatorioApuracaoDeJornada> ApuracaoDeJornada(@PathVariable("mes") Integer mesReferencia, @PathVariable("motorista") Integer motorista) {
        RelatorioApuracaoDeJornada result = null;
        String error = "";

        try {
            result = js.getApuracaoDeJornada(motorista, mesReferencia);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "apuracaoJornada/{inicio}/{fim}/{motorista}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<RelatorioApuracaoDeJornada> apuracaoDeJornada(@PathVariable("inicio") Long dataInicio, @PathVariable("fim") Long dataFim, @PathVariable("motorista") Integer motorista) {

        RelatorioApuracaoDeJornada result = null;
        String error = "";

        try {
            result = js.getApuracaoDeJornada(motorista, dataInicio, dataFim);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "apuracaoJornada/{inicio}/{fim}/{motorista}/{nonconformity}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<RelatorioApuracaoDeJornada> apuracaoDeJornada(@PathVariable("inicio") Long dataInicio, @PathVariable("fim") Long dataFim, @PathVariable("motorista") Integer motorista, @PathVariable("nonconformity") Integer nonconformity) {
        RelatorioApuracaoDeJornada result = null;
        String error = "";

        try {
            result = js.assembleCalculationReport(motorista, dataInicio, dataFim, nonconformity == 1);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "inconformidades/{inicio}/{fim}/{motorista}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<RelatorioApuracaoDeJornada> inconformidades(@PathVariable("inicio") Long dataInicio, @PathVariable("fim") Long dataFim, @PathVariable("motorista") Integer motorista) {

        RelatorioApuracaoDeJornada result = null;
        String error = "";

        try {
            result = js.getApuracaoDeJornadaInconformidades(motorista, dataInicio, dataFim);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "apuracaoJornada/{mes}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<RelatorioApuracaoDeJornada>> ApuracaoDeJornada(@PathVariable("mes") Integer mesReferencia) {

        List<RelatorioApuracaoDeJornada> result = Collections.emptyList();
        String error = "";

        try {
            result = js.getApuracaoDeJornada(mesReferencia);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);

    }

    @RequestMapping(method = RequestMethod.GET, path = "/lista/{idEmpresa}/{idUnidade}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<ResumoJornada>> findAll(@PathVariable("idEmpresa") Integer idEmpresa, @PathVariable("idUnidade") Integer idUnidade) {
        List<ResumoJornada> result = null;
        String error = null;
        try {
            result = js.getResumosJornadas(idEmpresa, idUnidade);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/summary/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<ResumoJornada> summaryById(@PathVariable("id") Integer id) {
        ResumoJornada result = null;
        String error = null;
        try {
            result = js.getResumoJornada(id);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/removeDuplicates/{inicio}/{fim}/{motorista}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<String> removeDuplicates(@PathVariable("inicio") Long dataInicio, @PathVariable("fim") Long dataFim, @PathVariable("motorista") Integer motorista) {
        String result = null;
        String error = null;

        try {
            result = this.js.removeDuplicates(dataInicio, dataFim, motorista);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/lista/{inicio}/{fim}/{motorista}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<ResumoJornada>> findAll(@PathVariable("inicio") Long dataInicio, @PathVariable("fim") Long dataFim, @PathVariable("motorista") Integer motorista) {

        List<ResumoJornada> result = null;
        String error = null;
        try {
           result = js.getResumosJornadas(dataInicio, dataFim, motorista);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/markWaitingAsWorked/{id}/{userid}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Boolean> markWaitingAsWorked(@PathVariable("id") Integer journeyID, @PathVariable("userid") Integer userId) {
        String error = null;
        Boolean result = false;

        try {
            Jornada journey = this.js.getById(journeyID);
            Users dbUser = userService.getById(userId);

            if (Objects.equals(journey.getLocked(), Boolean.TRUE) && dbUser.getUsersClaims().stream().noneMatch(c -> Objects.equals(c.getClaim(), "apuracao"))) {
                throw new RuntimeException("Jornada bloqueada e usuário não possui permissão para efetuar alteração!");
            }

            error = js.markWaitingAsWorked(journey);
            if (Objects.isNull(error) || error.isEmpty()) {
                result = true;
            }
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/markDSRHE/{id}/{userid}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Boolean> markDSRHE(@PathVariable("id") Integer journeyID, @PathVariable("userid") Integer userID) {
        String error = null;
        Boolean result = false;

        try {
            Jornada journey = this.js.getById(journeyID);
            Users dbUser = userService.getById(userID);

            if (Objects.equals(journey.getLocked(), Boolean.TRUE) && dbUser.getUsersClaims().stream().noneMatch(c -> Objects.equals(c.getClaim(), "apuracao"))) {
                throw new RuntimeException("Jornada bloqueada e usuário não possui permissão para efetuar alteração!");
            }

            String response = js.markDSRHE(journeyID);
            if (Objects.isNull(response) || response.isEmpty()) {
                result = true;
            }
            error = response;
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/marcarDiariaMotorista/{id}/{userid}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Boolean> marcarDiariaMotorista(@PathVariable("id") Integer journeyID, @PathVariable("userid") Integer userID) {
        String error = null;
        Boolean result = false;

        try {
            Jornada journey = this.js.getById(journeyID);
            Users dbUser = userService.getById(userID);

            if (Objects.equals(journey.getLocked(), Boolean.TRUE) && dbUser.getUsersClaims().stream().noneMatch(c -> Objects.equals(c.getClaim(), "apuracao"))) {
                throw new RuntimeException("Jornada bloqueada e usuário não possui permissão para efetuar alteração!");
            }

            String response = js.marcarDiariaMotorista(journeyID);
            if (Objects.isNull(response) || response.isEmpty()) {
                result = true;
            }
            error = response;
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/lista/{idEmpresa}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<ResumoJornada>> findAll(@PathVariable("idEmpresa") Integer idEmpresa) {

        List<ResumoJornada> result = null;
        String error = null;
        try {
            result = js.getResumosJornadas(idEmpresa, null);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/enviarTodoPeriodoADiaria", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Boolean> enviarTodoPeriodoADiaria(@RequestBody List<ResumoJornada> listaResumoJornada) {
        String error = null;
        Boolean result = true;
        try {
            if(listaResumoJornada != null && !listaResumoJornada.isEmpty()){
                listaResumoJornada.forEach(rj->{
                    js.marcarDiariaMotorista(rj.getId());
                });
            }
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/salvarJornadaManual", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Jornada> salvarJornadaManual(@RequestBody Jornada entity) {
        String error = "";
        try {
           js.inserirJornadaManual(entity);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, entity);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/abonarExtraNoDia", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<String> abonarExtraNoDia(@RequestBody AbonarExtraVO entity) {
        String error = "";
        try {
            js.abonarHorasExtra(entity);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, "OK");
    }

    @RequestMapping(method = RequestMethod.POST, path = "/horasExtrasCinquentaPorcento", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<String> horasExtrasCinquentaPorcento(@RequestBody AbonarExtraVO entity) {
        String error = "";
        try {
            js.horasExtrasCinquentaPorcento(entity);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, "OK");
    }
}