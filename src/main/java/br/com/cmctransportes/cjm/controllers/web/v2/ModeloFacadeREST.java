package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.vo.EmpresaVO;
import br.com.cmctransportes.cjm.domain.entities.vo.ModeloVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoModeloVO;
import br.com.cmctransportes.cjm.domain.services.ModeloService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "App/v2/web/modelo")
public class ModeloFacadeREST {

    @Autowired
    private ModeloService modeloService;

    @RequestMapping(value = "/cadastrarModeloEquipamento", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoModeloVO> cadastrarModeloEquipamento(@RequestBody ModeloVO modeloVO) {

        RetornoModeloVO retorno = new RetornoModeloVO();
        try {
            retorno = modeloService.cadastrarModeloEquipamento(modeloVO);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/cadastrarModeloVeiculo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoModeloVO> cadastrarModeloVeiculo(@RequestBody ModeloVO modeloVO) {

        RetornoModeloVO retorno = new RetornoModeloVO();
        try {
            retorno = modeloService.cadastrarModeloVeiculo(modeloVO);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/editarModelo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoModeloVO> editarModelo(@RequestBody ModeloVO modeloVO) {

        RetornoModeloVO retorno = new RetornoModeloVO();
        try {
            retorno = modeloService.editarModelo(modeloVO);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarModelo/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoModeloVO> buscarModelo(@PathVariable Integer id) {
        RetornoModeloVO retorno = new RetornoModeloVO();
        try {
            retorno = modeloService.buscarModelo(id);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarListaModeloEquipamento", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoModeloVO> buscarListaModeloEquipamento() {
        RetornoModeloVO retorno = new RetornoModeloVO();
        try {
            retorno = modeloService.buscarListaModeloEquipamento();
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarListaModeloVeiculo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoModeloVO> buscarListaModeloVeiculo() {
        RetornoModeloVO retorno = new RetornoModeloVO();
        try {
            retorno = modeloService.buscarListaModeloVeiculo();
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarModeloPortatil", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoModeloVO> buscarModeloPortatil() {
        RetornoModeloVO retorno = new RetornoModeloVO();
        try {
            retorno = modeloService.buscarModeloPortatil();
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }
}
