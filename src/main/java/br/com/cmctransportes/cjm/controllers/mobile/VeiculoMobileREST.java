package br.com.cmctransportes.cjm.controllers.mobile;

import br.com.cmctransportes.cjm.domain.entities.vo.RetornoVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoVeiculoVO;
import br.com.cmctransportes.cjm.domain.entities.vo.VeiculoVO;
import br.com.cmctransportes.cjm.domain.services.VeiculosService;
import br.com.cmctransportes.cjm.proxy.modelo.Veiculo;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "App/veiculos")
public class VeiculoMobileREST {

    @Autowired
    private VeiculosService veiculosService;

    @RequestMapping(method = RequestMethod.GET, value = "/listar/{idMotorista}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoVeiculoVO> listar(@PathVariable("idMotorista") Integer idMotorista){
        RetornoVeiculoVO retorno = new RetornoVeiculoVO();
        try{
            List<VeiculoVO> lista = veiculosService.buscarListaParaMobile(idMotorista);
            retorno.setCodigoRetorno(1);
            retorno.setListaDeVeiculos(lista);
            retorno.setMensagem("Lista carregada com Sucesso.");
        }catch (Exception e){
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
        }

        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }
}
