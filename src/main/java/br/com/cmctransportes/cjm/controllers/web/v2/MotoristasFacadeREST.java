package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.MotoristaStatus;
import br.com.cmctransportes.cjm.domain.entities.Motoristas;
import br.com.cmctransportes.cjm.domain.entities.MotoristasVeiculos;
import br.com.cmctransportes.cjm.domain.entities.vo.MotoristaVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RESTResponseVO;
import br.com.cmctransportes.cjm.domain.services.*;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

/**
 * @author analista.ti
 */
@RestController
@RequestMapping(path = "App/v2/web/motoristas")
public class MotoristasFacadeREST {

    @Autowired
    private DriverService ms;

    @Autowired
    private MotoristasVeiculosService mv;

    @Autowired
    private MotoristasStatusService mss;

    @Autowired
    private UserService userService;

    @Autowired
    private UnidadesService unidadesService;

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Motoristas> create(@RequestBody Motoristas entity) {
        String error = null;

        try {
            ms.save(entity);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, entity);
    }

    @RequestMapping(method = RequestMethod.GET, path = "inconformidades/{dataini}/{datafim}/{only_nonconformities}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<Motoristas>> findNonconformities(@PathVariable("dataini") Long dataInicio, @PathVariable("datafim") Long dataFinal, @PathVariable("only_nonconformities") boolean only_nonconformities) {
        String error = "";
        List<Motoristas> result = Collections.emptyList();
        try {
            result = ms.findNonconformities(dataInicio, dataFinal, only_nonconformities);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Motoristas> edit(@PathVariable("id") Integer id, @RequestBody Motoristas entity) {
        String error = "";

        try {
            ms.update(entity);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, entity);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "{id}")
    public RESTResponseVO<Boolean> remove(@PathVariable("id") Integer id) {
        String error = "";
        boolean result = false;

        try {
            ms.delete(id);
            result = true;
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Motoristas> find(@PathVariable("id") Integer id) {
        String error = "";
        Motoristas entity = null;

        try {
            entity = ms.getById(id);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, entity);

    }

    @RequestMapping(method = RequestMethod.GET, path = "ver/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<MotoristaVO> verMotorista(@PathVariable("id") Integer id) {
        String error = "";
        MotoristaVO result = null;

        try {
            Motoristas driver = ms.getById(id);
            result = new MotoristaVO(driver, this.userService, this.unidadesService);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<Motoristas>> findAll() {
        String error = "";
        List<Motoristas> result = Collections.emptyList();

        try {
            result = ms.findAll();
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/lista/{idEmpresa}/{idUnidade}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<Motoristas>> findAll(@PathVariable("idEmpresa") Integer idEmpresa, @PathVariable("idUnidade") Integer idUnidade) {
        List<Motoristas> result = Collections.emptyList();
        String error = "";
        try {
            result = ms.findAll(idEmpresa, idUnidade);
            result.forEach(m->{
                m.setImagemId(null);
            });
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/listaMotoristaMinimo/{idEmpresa}/{idUnidade}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<Motoristas>> findAllMinimo(@PathVariable("idEmpresa") Integer idEmpresa, @PathVariable("idUnidade") Integer idUnidade) {
        List<Motoristas> result = Collections.emptyList();
        String error = "";
        try {
            result = ms.findAllMinimo(idEmpresa, idUnidade);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, result);
    }


    @RequestMapping(method = RequestMethod.GET, path = "/findAllListForView/{idEmpresa}/{idUnidade}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<Motoristas>> findAllListForView(@PathVariable("idEmpresa") Integer idEmpresa, @PathVariable("idUnidade") Integer idUnidade) {
        List<Motoristas> result = Collections.emptyList();
        String error = "";
        try {
            result = ms.findAllListForView(idEmpresa, idUnidade);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/veiculos/", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<MotoristasVeiculos> createVeiculo(@RequestBody MotoristasVeiculos entity) {
        String error = "";
        try {
            mv.save(entity);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, entity);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/veiculos/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<MotoristasVeiculos> editVeiculo(@PathVariable("id") Integer id, @RequestBody MotoristasVeiculos entity) {

        String error = "";
        try {
            mv.update(entity);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, entity);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/veiculos/{id}")
    public RESTResponseVO<Boolean> removeVeiculo(@PathVariable("id") Integer id) {
        boolean result = false;
        String error = "";
        try {
            mv.delete(id);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.POST, path = "situacao", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<MotoristaStatus> create(@RequestBody MotoristaStatus entity) {
        String error = null;

        try {
            mss.save(entity);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, entity);
    }
}