package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.vo.MonitorViaSatVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoMonitorViaSatVO;
import br.com.cmctransportes.cjm.domain.services.MonitorViaSatService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "App/v2/web/monitorviasat")
public class MonitorViaSatREST {

    @Autowired
    private MonitorViaSatService service;

    @RequestMapping(method = RequestMethod.GET, path = "/buscarDados/{idEmpresa}/{idUnidade}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoMonitorViaSatVO> buscarDados(@PathVariable("idEmpresa") Integer idEmpresa, @PathVariable("idUnidade") Integer idUnidade) {
        String error = null;
        RetornoMonitorViaSatVO retorno = new RetornoMonitorViaSatVO();
        try {
            List<MonitorViaSatVO>  lista  = service.buscarDados(idEmpresa, idUnidade);
            retorno.setListaDeMonitorViaSat(lista);
            retorno.setCodigoRetorno(1);
            retorno.setMensagem("Lista carregada com sucesso");
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }
}



