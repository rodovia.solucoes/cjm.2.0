package br.com.cmctransportes.cjm.controllers.mobile;

import br.com.cmctransportes.cjm.domain.entities.EventosJornada;
import br.com.cmctransportes.cjm.domain.services.EventosJornadaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author analista.ti
 */
@RestController
@RequestMapping(path = "App/eventosJornada")
public class EventosJornadaMobileREST {

    @Autowired
    private EventosJornadaService cs;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<EventosJornada> findAll() {
        return cs.findAll();
    }

}