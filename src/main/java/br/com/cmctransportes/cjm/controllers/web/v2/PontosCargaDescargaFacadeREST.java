package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.PontosCargaDescarga;
import br.com.cmctransportes.cjm.domain.entities.vo.RESTResponseVO;
import br.com.cmctransportes.cjm.domain.services.PontosCargaDescargaService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

/**
 * @author analista.ti
 */
@RestController
@RequestMapping(path = "App/v2/web/pontoscargadescarga")
public class PontosCargaDescargaFacadeREST {

    @Autowired
    private PontosCargaDescargaService cgs;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<PontosCargaDescarga> create(@RequestBody PontosCargaDescarga entity) {
        String error = "";
        try {
            cgs.save(entity);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
            cause.printStackTrace();
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
            e.printStackTrace();
        }
        return new RESTResponseVO<>(error, entity);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<PontosCargaDescarga> edit(@PathVariable("id") Integer id, @RequestBody PontosCargaDescarga entity) {
        String error = "";
        try {
            cgs.update(entity);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, entity);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "{id}")
    public RESTResponseVO<Boolean> remove(@PathVariable("id") Integer id) {
        String error = "";
        boolean result = false;

        try {
            cgs.delete(id);
            result = true;
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<PontosCargaDescarga> find(@PathVariable("id") Integer id) {
        String error = "";
        PontosCargaDescarga entity = null;
        try {
            entity = cgs.getById(id);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, entity);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<PontosCargaDescarga>> findAll() {
        String error = "";
        List<PontosCargaDescarga> entity = Collections.emptyList();
        try {
            entity = cgs.findAll();
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, entity);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/lista/{idEmpresa}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<PontosCargaDescarga>> findAll(@PathVariable("idEmpresa") Integer idEmpresa) {
        String error = "";
        List<PontosCargaDescarga> entity = Collections.emptyList();
        try {
            entity = cgs.findAll(idEmpresa);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, entity);
    }
}