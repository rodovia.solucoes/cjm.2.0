package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.Turnos;
import br.com.cmctransportes.cjm.domain.entities.vo.RESTResponseVO;
import br.com.cmctransportes.cjm.domain.services.TurnosService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import br.com.cmctransportes.cjm.utils.TimeHelper;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * @author analista.ti
 */
@RestController
@RequestMapping(path = "App/v2/web/turnos")
public class TurnosFacadeREST {

    @Autowired
    private TurnosService ts;

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Turnos> create(@RequestBody Turnos entity) {
        String error = "";
        try {
            ts.save(entity);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, entity);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Turnos> edit(@PathVariable("id") Integer id, @RequestBody Turnos entity) {
        String error = "";
        try {
            switch (entity.getTipo()) {
                case 1: {
                    Long d1 = TimeHelper.nullsafeSubtract(entity.getHorarioSaida01(), entity.getHorarioEntrada01(), entity.getTempoAlimentacao01());
                    Long d2 = TimeHelper.nullsafeSubtract(entity.getHorarioSaida02(), entity.getHorarioEntrada02(), entity.getTempoAlimentacao02());
                    Long d3 = TimeHelper.nullsafeSubtract(entity.getHorarioSaida03(), entity.getHorarioEntrada03(), entity.getTempoAlimentacao03());
                    Long d4 = TimeHelper.nullsafeSubtract(entity.getHorarioSaida04(), entity.getHorarioEntrada04(), entity.getTempoAlimentacao04());
                    Long d5 = TimeHelper.nullsafeSubtract(entity.getHorarioSaida05(), entity.getHorarioEntrada05(), entity.getTempoAlimentacao05());
                    Long d6 = TimeHelper.nullsafeSubtract(entity.getHorarioSaida06(), entity.getHorarioEntrada06(), entity.getTempoAlimentacao06());
                    Long d7 = TimeHelper.nullsafeSubtract(entity.getHorarioSaida07(), entity.getHorarioEntrada07(), entity.getTempoAlimentacao07());
                    Long total = d1 + d2 + d3 + d4 + d5 + d6 + d7;
                    if (entity.getHorasMensais() == 220 && total > TimeHelper.hours(44)) {
                        throw new RuntimeException("Número de horas semanais ultrapassa 44");
                    }
                }
                break;
                case 2: {
                    Long d1 = entity.getTempoDiario01();
                    Long d2 = entity.getTempoDiario02();
                    Long d3 = entity.getTempoDiario03();
                    Long d4 = entity.getTempoDiario04();
                    Long d5 = entity.getTempoDiario05();
                    Long d6 = entity.getTempoDiario06();
                    Long d7 = entity.getTempoDiario07();
                    Long total = TimeHelper.nullsafeAdd(d1, d2, d3, d4, d5, d6, d7);
                    if (entity.getHorasMensais() == 220 && total > TimeHelper.hours(44)) {
                        throw new RuntimeException("Número de horas semanais ultrapassa 44");
                    }
                }
                break;
                case 3:
                    if (Objects.isNull(entity.getWeekDays()) || entity.getWeekDays() <= 0 || entity.getWeekDays() > 7) {
                        throw new RuntimeException("Número de dias por semana é inválido!");
                    }
                    Long total = entity.getTempoDiario01() * entity.getWeekDays();
                    if (entity.getHorasMensais() == 220 && total > TimeHelper.hours(44)) {
                        throw new RuntimeException("Número de horas semanais ultrapassa 44");
                    }
                    break;
            }

            ts.update(entity);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, entity);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "{id}")
    public RESTResponseVO<Boolean> remove(@PathVariable("id") Integer id) {
        String error = "";
        boolean result = false;

        try {
            ts.delete(id);
            result = true;
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Turnos> find(@PathVariable("id") Integer id) {
        String error = "";
        Turnos entity = null;
        try {
            entity = ts.getById(id);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, entity);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<Turnos>> findAll() {
        String error = "";
        List<Turnos> entity = Collections.emptyList();
        try {
            entity = ts.findAll();
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, entity);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/lista/{idEmpresa}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<Turnos>> findAll(@PathVariable("idEmpresa") Integer idEmpresa) {
        String error = "";
        List<Turnos> entity = Collections.emptyList();
        try {
            entity = ts.findAll(idEmpresa);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, entity);
    }
}