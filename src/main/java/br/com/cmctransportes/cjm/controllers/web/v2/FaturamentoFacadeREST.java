package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.Faturamento;
import br.com.cmctransportes.cjm.domain.entities.ResumoFaturamento;
import br.com.cmctransportes.cjm.domain.entities.TimelineFaturamento;
import br.com.cmctransportes.cjm.domain.entities.vo.DetalheGraficoVO;
import br.com.cmctransportes.cjm.domain.entities.vo.FiltroVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RESTResponseVO;
import br.com.cmctransportes.cjm.domain.services.FaturamentoService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import br.com.cmctransportes.cjm.utils.TimeHelper;
import org.hibernate.exception.ConstraintViolationException;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(path = "App/v2/web/faturamento")
public class FaturamentoFacadeREST {

   @Autowired
   private FaturamentoService faturamentoService;


    @RequestMapping(value = "/faturamentoresumido", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Faturamento> faturamentoresumido(@RequestBody FiltroVO filtroVO) {
        String error = "";
        Faturamento faturamento = new Faturamento();
        try {

            Date dataInicio = TimeHelper.getDate000000(filtroVO.getDataInicial());
            Date dataFim = TimeHelper.getDateWithNowTime(filtroVO.getDataFinal());
            int quantidadeDeDias = Days.daysBetween(new DateTime(filtroVO.getDataFinal()), new DateTime(new Date())).getDays();
            if(quantidadeDeDias > 0){
                dataFim = TimeHelper.getDate235959(filtroVO.getDataFinal());
            }

            int horas = Hours.hoursBetween(new DateTime(dataInicio), new DateTime(dataFim)).getHours();
            faturamento.setQuantidadeDeDias(horas);
            List<ResumoFaturamento> listaResumoFaturamento = faturamentoService.calcularResumoFaturamento(filtroVO);

            faturamento.setListaResumoFaturamento(listaResumoFaturamento);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, faturamento);
    }


    @RequestMapping(path = "/buscarTimelinePorPlaca", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Faturamento> buscarTimelinePorPlaca(@RequestBody FiltroVO filtroVO) {
        String error = "";
        Faturamento faturamento = null;
        try {
            int dias = Days.daysBetween(new DateTime(filtroVO.getDataInicial()), new DateTime(filtroVO.getDataFinal())).getDays();
            faturamento = faturamentoService.gerarTimeLine(filtroVO);
            faturamento.setQuantidadeDeDias(dias);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, faturamento);
    }


    @RequestMapping(path = "/gerarExcelTimeline", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<String> gerarExcelTimeline(@RequestBody Faturamento faturamento) {
        String error = "";
        String urlExcel = null;
        try {
            urlExcel = this.faturamentoService.gerarExcel(faturamento);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, urlExcel);
    }


    @RequestMapping(path = "/buscarDetalheDoGrafico", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<DetalheGraficoVO> buscarDetalheDoGrafico(@RequestBody FiltroVO filtroVO) {
        String error = "";
        DetalheGraficoVO detalheGraficoVO = null;
        try {
          detalheGraficoVO = this.faturamentoService.buscarDetalheEvento(filtroVO);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, detalheGraficoVO);
    }

}
