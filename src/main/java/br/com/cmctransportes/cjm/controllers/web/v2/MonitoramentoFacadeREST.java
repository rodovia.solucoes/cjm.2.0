package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.Empresas;
import br.com.cmctransportes.cjm.domain.entities.Transmissao;
import br.com.cmctransportes.cjm.domain.entities.vo.*;
import br.com.cmctransportes.cjm.domain.services.EmpresasService;
import br.com.cmctransportes.cjm.domain.services.MonitoramentoService;
import br.com.cmctransportes.cjm.domain.services.TransmissaoService;
import br.com.cmctransportes.cjm.domain.services.VeiculosService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = "App/v2/web/monitoramento")
public class MonitoramentoFacadeREST {

    @Autowired
    private EmpresasService empresasService;
    @Autowired
    private MonitoramentoService monitoramentoService;




    @RequestMapping(value = "/buscarListaDeVeiculoPorEmpresa/{idEmpresa}/{idUnidade}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoMonitoramentoVO> buscarListaDeVeiculoPorEmpresa(@PathVariable Integer idEmpresa, @PathVariable Integer idUnidade) {
        RetornoMonitoramentoVO retorno = new RetornoMonitoramentoVO();
        try {
            List<VeiculoMonitoramentoVO> lista = monitoramentoService.buscarListaDeVeiculos(idEmpresa, idUnidade);
            retorno.setListaVeiculoMonitoramento(lista);
            retorno.setMensagem("Lista carregada com sucesso");
            retorno.setCodigoRetorno(1);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    @RequestMapping(value = "/buscarTelemetriaPorVeiculo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoMonitoramentoVO> buscarTelemetriaPorVeiculo(@RequestBody FiltroVO filtro) {
        RetornoMonitoramentoVO retorno = new RetornoMonitoramentoVO();
        try {
            DadoTelemetriaVO dadoTelemetria  = monitoramentoService.buscarTelemetriaDosVeiculos(filtro.getListaDeVeiculos(), filtro.getDataInicial(), filtro.getDataFinal());
            retorno.setDadoTelemetria(dadoTelemetria);
            retorno.setMensagem("Carregada com sucesso");
            retorno.setCodigoRetorno(1);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    @RequestMapping(value = "/buscarUltimasTransmissao/{idVeiculo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoMonitoramentoVO> buscarUltimasTransmissao(@PathVariable Integer idVeiculo) {
        RetornoMonitoramentoVO retorno = new RetornoMonitoramentoVO();
        try {
            List<Transmissao> lista = monitoramentoService.buscarUltimasTrasnmissoes(idVeiculo);
            retorno.setListaDePontosPercorridos(lista);
            retorno.setMensagem("Lista carregada com sucesso");
            retorno.setCodigoRetorno(1);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }



}
