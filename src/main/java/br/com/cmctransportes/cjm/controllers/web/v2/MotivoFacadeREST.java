package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.Motivo;
import br.com.cmctransportes.cjm.domain.entities.vo.RESTResponseVO;
import br.com.cmctransportes.cjm.domain.services.MotivoService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

/**
 * @author analista.ti
 */
@RestController
@RequestMapping(path = "App/v2/web/motivo")
public class MotivoFacadeREST {

    @Autowired
    private MotivoService ms;

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Motivo> create(@RequestBody Motivo entity) {
        String error = "";
        try {
            ms.save(entity);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, entity);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Motivo> edit(@PathVariable("id") Integer id, @RequestBody Motivo entity) {
        String error = "";
        try {
            ms.update(entity);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, entity);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "{id}")
    public RESTResponseVO<Boolean> remove(@PathVariable("id") Integer id) {
        String error = "";
        boolean result = false;

        try {
            ms.delete(id);
            result = true;
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);

    }

    @RequestMapping(method = RequestMethod.GET, path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Motivo> find(@PathVariable("id") Integer id) {
        String error = "";
        Motivo result = null;

        try {
            result = ms.getById(id);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<Motivo>> findAll() {
        String error = "";
        List<Motivo> result = Collections.emptyList();
        try {
            result = ms.findAll();
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/lista/{idEmpresa}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<Motivo>> findAll(@PathVariable("idEmpresa") Integer idEmpresa) {
        String error = "";
        List<Motivo> result = Collections.emptyList();
        try {
            result = ms.findAll(idEmpresa);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }
}