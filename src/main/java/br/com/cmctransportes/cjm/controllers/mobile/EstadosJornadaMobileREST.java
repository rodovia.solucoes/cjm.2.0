package br.com.cmctransportes.cjm.controllers.mobile;

import br.com.cmctransportes.cjm.domain.entities.EstadosJornada;
import br.com.cmctransportes.cjm.domain.services.EstadosJornadaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author analista.ti
 */
@RestController
@RequestMapping(path = "App/estadosJornada")
public class EstadosJornadaMobileREST {

    @Autowired
    private EstadosJornadaService cs;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<EstadosJornada> findAll() {
        return cs.findAll();
    }
}