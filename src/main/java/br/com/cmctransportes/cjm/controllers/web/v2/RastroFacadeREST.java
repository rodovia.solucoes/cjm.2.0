package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.Rastro;
import br.com.cmctransportes.cjm.domain.entities.vo.RESTResponseVO;
import br.com.cmctransportes.cjm.domain.services.RastroService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

/**
 * @author analista.ti
 */
@RestController
@RequestMapping(path = "App/v2/web/rastros")
public class RastroFacadeREST {

    @Autowired
    private RastroService rs;

    @RequestMapping(method = RequestMethod.GET, path = "relatorio/{motorista}/{dataini}/{datafim}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<Rastro>> geraRelatorio(@PathVariable("motorista") Integer motoristaId, @PathVariable("dataini") Long dataInicio, @PathVariable("datafim") Long dataFim) {
        String error = "";
        List<Rastro> entity = Collections.emptyList();
        try {
            entity = rs.relatorioDePosicoes(motoristaId, dataInicio, dataFim);
            System.out.println("RastroFacedRest relatorioDePosicoes");
        } catch (org.hibernate.exception.ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
            System.out.println("RastroFacedRest handleConstraintViolation");
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
            System.out.println("RastroFacedRest exceptionMessage");
        }
        return new RESTResponseVO<>(error, entity);
    }
}