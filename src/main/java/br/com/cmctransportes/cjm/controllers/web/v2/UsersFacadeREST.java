package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.MobileUser;
import br.com.cmctransportes.cjm.domain.entities.Users;
import br.com.cmctransportes.cjm.domain.entities.vo.IDNameTupleVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RESTResponseVO;
import br.com.cmctransportes.cjm.domain.entities.vo.UserVO;
import br.com.cmctransportes.cjm.domain.services.EmpresasService;
import br.com.cmctransportes.cjm.domain.services.UnidadesService;
import br.com.cmctransportes.cjm.domain.services.UserService;
import br.com.cmctransportes.cjm.proxy.UsersOpenFireProxy;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author analista.ti
 */
@RestController
@RequestMapping(path = "App/v2/web/users")
@Slf4j
public class UsersFacadeREST {

    @Autowired
    private UserService us;

    @Autowired
    private UnidadesService unidadesService;

    @Autowired
    private EmpresasService empresasService;

    @RequestMapping(path = "create/{empresa}/{unidade}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<UserVO> create(@RequestBody UserVO entity, @PathVariable("empresa") Integer empresa, @PathVariable("unidade") Integer unidade) {
        Users user = entity.revert(empresa, unidade, this.unidadesService, this.empresasService);
        String error = "";
        try {
            user.setSenhaCripto(RodoviaUtils.cryptWithMD5(user.getSenha()));
            us.save(user);
            entity = new UserVO(user);
            UsersOpenFireProxy.getInstance().cadastrarUsuario(user.getUserName().trim(), user.getSenha().trim());
        } catch (Exception e) {
            e.printStackTrace();
            error = String.format("Erro ao cadastrar no chat: %s", e.getLocalizedMessage());
        }

        return new RESTResponseVO<>(error, entity);
    }

    @RequestMapping(path = "password/{userId}/{old}/{new}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<String> changePassword(@PathVariable("userId") Integer userId, @PathVariable("old") String oldCriptoPassword, @PathVariable("new") String newPassword) {
        String error = "";
        try {
            Users dbUser = us.getById(userId);
            if (Objects.equals(dbUser.getSenhaCripto(), RodoviaUtils.cryptWithMD5(oldCriptoPassword)) || Objects.equals(dbUser.getSenhaCripto(), oldCriptoPassword)) {
                dbUser.setSenha(newPassword);

                // TODO: ver como fazer isso aqui
                //  chatRESTService.updateUsers(UsersChatAdapter.converter(dbUser));
                dbUser.setSenhaCripto(RodoviaUtils.cryptWithMD5(newPassword));
                us.update(dbUser);
                UsersOpenFireProxy.getInstance().alterarUsuario(dbUser.getUserName().trim(), newPassword.trim());
            } else {
                error = "Senha informada não confere com a senha atual.";
            }
        } catch (Exception e) {
            error = e.getLocalizedMessage();
            e.printStackTrace();
        }

        return new RESTResponseVO<>(error, "CHANGE_PASSWORD");
    }

    @RequestMapping(method = RequestMethod.PUT, path = "{empresa}/{unidade}/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<UserVO> edit(@PathVariable("id") Integer id, @PathVariable("empresa") Integer empresa, @PathVariable("unidade") Integer unidade, @RequestBody UserVO entity) {
        Users user = entity.revert(empresa, unidade, this.unidadesService, this.empresasService);
        String error = "";

        try {
            if (Objects.nonNull(user.getSenha())) {
                user.setSenhaCripto(RodoviaUtils.cryptWithMD5(user.getSenha()));
                UsersOpenFireProxy.getInstance().alterarUsuario(user.getUserName().trim(), user.getSenha().trim());
            }
            us.update(user);
            entity = new UserVO(user);
        } catch (Exception e) {
            error = e.getLocalizedMessage();
            e.printStackTrace();
        }

        return new RESTResponseVO<>(error, entity);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public MobileUser edit(@PathVariable("id") Integer id, @RequestBody Users user) {
        MobileUser mUsr = null;

        try {
            user.setSenhaCripto(RodoviaUtils.cryptWithMD5(user.getSenha()));
            us.update(user);
            mUsr = new MobileUser(user);
            UsersOpenFireProxy.getInstance().alterarUsuario(user.getUserName().trim(), user.getSenha().trim());
        } catch (Exception e) {
            log.info(e.getLocalizedMessage());
        }

        return mUsr;
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "branch/{userId}/{branchId}")
    public RESTResponseVO<Boolean> removeBranch(@PathVariable("userId") Integer userId, @PathVariable("branchId") Integer branchId) {
        String error = "";
        boolean result = false;
        try {
            us.removeBranch(userId, branchId);
            result = true;
        } catch (Exception e) {
            error = e.getLocalizedMessage();
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "company/{userId}/{companyId}")
    public RESTResponseVO<Boolean> removeCompany(@PathVariable("userId") Integer userId, @PathVariable("companyId") Integer companyId) {
        String error = "";
        boolean result = false;
        try {
            us.removeCompany(userId, companyId);
            result = true;
        } catch (Exception e) {
            error = e.getLocalizedMessage();
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "claim/{id}")
    public RESTResponseVO<Boolean> removeClaims(@PathVariable("id") Integer id) {
        String error = "";
        boolean result = false;
        try {
            us.removeClaims(id);
            result = true;
        } catch (Exception e) {
            error = e.getLocalizedMessage();
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "{id}")
    public RESTResponseVO<Boolean> remove(@PathVariable("id") Integer id) {
        String error = "";
        boolean result = false;
        try {
            Users users = us.getById(id);
            us.delete(id);
            result = true;
        } catch (Exception e) {
            error = e.getLocalizedMessage();
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<UserVO> find(@PathVariable("id") Integer id) {
        String error = "";
        UserVO vo = null;
        try {
            Users user = us.getById(id);
            if (Objects.isNull(user)) {
                error = "Nenhum usuário encontrado";
            } else {
                vo = new UserVO(user);
                vo.setClaimList(user.getUsersClaims().stream().map(u -> new IDNameTupleVO(u.getId(), u.getClaim())).collect(Collectors.toList()));
            }
        } catch (Exception e) {
            error = e.getLocalizedMessage();
        }

        return new RESTResponseVO<>(error, vo);
    }

    @RequestMapping(method = RequestMethod.GET, path = "{company}/{branch}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<UserVO>> findAll(@PathVariable("company") Integer company, @PathVariable("branch") Integer branch) {
        String error = "";
        List<UserVO> users = Collections.emptyList();

        try {
            users = us.findByCompanyAndBranch(company, branch);
            if (users.isEmpty()) {
                error = "Não foi encontrado nenhum usuário!";
            }
        } catch (Exception e) {
            error = e.getLocalizedMessage();
        }

        return new RESTResponseVO<>(error, users);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<UserVO>> findAll() {
        String error = "";
        List<UserVO> users = Collections.emptyList();

        try {
            users = us.findAll();
            if (users.isEmpty()) {
                error = "Não foi encontrado nenhum usuário!";
            }
        } catch (Exception e) {
            error = e.getLocalizedMessage();
        }

        return new RESTResponseVO<>(error, users);
    }
}