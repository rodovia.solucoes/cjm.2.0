package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.Evento;
import br.com.cmctransportes.cjm.domain.entities.Users;
import br.com.cmctransportes.cjm.domain.entities.enums.EVENT_TYPE;
import br.com.cmctransportes.cjm.domain.entities.vo.CalendarEventVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RESTResponseVO;
import br.com.cmctransportes.cjm.domain.services.EventoService;
import br.com.cmctransportes.cjm.domain.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

/**
 * @author William Leite
 */
@RestController
@RequestMapping(path = "App/v2/web/journeygrid")
public class JourneyGridFacadeREST {

    @Autowired
    private EventoService service;

    @Autowired
    private UserService userService;

    @RequestMapping(path = "updatetime/{user}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Boolean> updateTime(@RequestBody CalendarEventVO vo, @PathVariable("user") Integer user) {
        try {

            Evento evento = service.getById(vo.getId());
            Users dbUser = userService.getById(user);

            if ((Objects.equals(evento.getLocked(), Boolean.TRUE) || Objects.equals(evento.getJornadaId().getLocked(), Boolean.TRUE)) && dbUser.getUsersClaims().stream().noneMatch(c -> Objects.equals(c.getClaim(), "apuracao"))) {
                throw new RuntimeException("Evento bloqueado e usuário não possui permissão para efetuar alteração!");
            }

            evento.setInstanteEvento(vo.getInstanteEvento());
            evento.setJustificativa(vo.getJustificativa());
            service.save(evento);

            return new RESTResponseVO<>("", true);
        } catch (Exception e) {
            e.printStackTrace();
            return new RESTResponseVO<>(e.getLocalizedMessage(), false);
        }
    }

    private Evento markAsRemoved(CalendarEventVO vo) {
        Evento evento = service.getById(vo.getId());
        evento.setRemovido(Boolean.TRUE);
        evento.setJustificativa(vo.getJustificativa());
        evento.setAberturaId(vo.getAberturaId());
        evento.setFimEventoId(vo.getFimEventoId());
        service.marcaRemovido(vo.getId(), evento);
        return evento;
    }

    @RequestMapping(path = "updateremoved", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Boolean> updateRemoved(@RequestBody CalendarEventVO vo) {
        try {
            Evento evento = this.markAsRemoved(vo);

            if (evento.getTipoEvento().getId() == EVENT_TYPE.DSR.getStartCode() || evento.getTipoEvento().getId() == EVENT_TYPE.DSR.getEndCode()
                    || evento.getTipoEvento().getId() == EVENT_TYPE.INTERJORNADA.getStartCode() || evento.getTipoEvento().getId() == EVENT_TYPE.INTERJORNADA.getEndCode()) {
                evento.getJornadaId().getEventos().stream().forEach((e) -> {
                    e.setRemovido(Boolean.TRUE);
                    e.setJustificativa(vo.getJustificativa());
                    service.marcaRemovido(e.getId(), e);
                });
            }

            return new RESTResponseVO<>("", true);
        } catch (Exception e) {
            e.printStackTrace();
            return new RESTResponseVO<>(e.getLocalizedMessage(), false);
        }
    }
}