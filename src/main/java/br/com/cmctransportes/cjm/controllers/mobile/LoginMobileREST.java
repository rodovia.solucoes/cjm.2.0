package br.com.cmctransportes.cjm.controllers.mobile;

import br.com.cmctransportes.cjm.domain.entities.MobileUser;
import br.com.cmctransportes.cjm.domain.entities.Users;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoUsuarioMobileVO;
import br.com.cmctransportes.cjm.domain.entities.vo.UsuarioMobileVO;
import br.com.cmctransportes.cjm.domain.services.UserService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST Web Service
 *
 * @author analista.ti
 */
@RestController
@RequestMapping(path = "App/login")
@Slf4j
public class LoginMobileREST {

    @Autowired
    private UserService us;

    @RequestMapping(method = RequestMethod.POST, path = "android", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public MobileUser loginAndroid(@RequestBody Users u) {

        if (u == null) {
            return null;
        }

        String e = u.getUserName();
        String p = u.getSenha();

        log.info("Mobile User[" + e + "]");

        if ((e == null) || (p == null)) {
            log.info("User == NULL (user or password is null)");
            return null;
        }

        Users user = us.getByUserName(e);

        if (user != null) {
            log.info("Mobile.User.getUserName()[" + user.getUserName() + "]");
//            log.info("Mobile.User.getSenha()["+user.getSenha()+"]");
            user.setUsersClaims(null);

            if (!user.getSenhaCripto().equals(RodoviaUtils.cryptWithMD5(p))) {
                user = null;
                // TODO: setar  erro aqui!
                // "wrong password"
                log.info("User == NULL (wrong password)");
            }
        } else {
            // TODO: setar  erro aqui!
            // "user not found"

            log.info("User == NULL (user not found)");
        }

        if (user == null) return null;

        user.setSenha(p);
        MobileUser mUsr = new MobileUser(user);

        return mUsr;
    }


    @RequestMapping(value = "/logarUsuarioMobile", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoUsuarioMobileVO> logarUsuarioMobile(@RequestBody UsuarioMobileVO usuarioMobileVO){
        RetornoUsuarioMobileVO retorno = new RetornoUsuarioMobileVO();
        try {
            us.logarUsuarioMobile(usuarioMobileVO);
            retorno.setUsuario(usuarioMobileVO);
            retorno.setCodigoRetorno(0);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

}
