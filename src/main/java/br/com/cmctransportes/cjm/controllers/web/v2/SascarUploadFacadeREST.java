package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.vo.RESTResponseVO;
import br.com.cmctransportes.cjm.domain.services.SascarUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping(path = "App/v2/web/sascarupload")
public class SascarUploadFacadeREST {

    @Autowired
    private SascarUploadService service;
    /**
     * Classe responsavel por recber o arquivo da sascar e ecaminhar para o SascarUploadService
     * @param file Arquivo recebido pelo navehador para tratamento
     * @param driver Motorista selecionado no front
     * @param user usuario logado no sistema
     * @return a RESTResponseVO retorna mensgaem de erro ou sucesso conforme o resultado.
     */
    @PostMapping(path = "{driver}/{user}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<String> uploadData(@RequestParam("file") MultipartFile file, @PathVariable("driver") Integer driver, @PathVariable("user") Integer user) {
        if (file == null) {
            throw new RuntimeException("Você deve selecionar um arquivo para fazer o upload");
        }

        String result = "";
        String error = "";
        try {
            result = this.service.uploadFile(file.getInputStream(), driver, user);
        } catch (IOException e) {
            e.printStackTrace();
            error = e.getLocalizedMessage();
        }

        return new RESTResponseVO<>(error , result);
    }
}
