package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.Empresas;
import br.com.cmctransportes.cjm.domain.entities.Inconformidade;
import br.com.cmctransportes.cjm.domain.entities.Motoristas;
import br.com.cmctransportes.cjm.domain.entities.enums.NONCONFORMITY_CALCULUS_TYPE;
import br.com.cmctransportes.cjm.domain.entities.enums.OPERATIONS;
import br.com.cmctransportes.cjm.domain.entities.vo.*;
import br.com.cmctransportes.cjm.domain.services.DriverService;
import br.com.cmctransportes.cjm.domain.services.EmpresasService;
import br.com.cmctransportes.cjm.domain.services.InconformidadeService;
import br.com.cmctransportes.cjm.domain.services.JornadaService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import br.com.cmctransportes.cjm.utils.TimeHelper;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * recebe do front-end a pesquisa por meio de JSON
 *
 * @author Producao
 */
@RestController
@RequestMapping(path = "App/v2/web/inconformidade")
public class InconformidadeFacadeREST {

    @Autowired
    private InconformidadeService is;

    @Autowired
    private JornadaService js;

    @Autowired
    private DriverService driverService;

    @Autowired
    private EmpresasService empresasService;

    @RequestMapping(method = RequestMethod.POST, path = "operation/{operationType}/{start}/{end}/{driver}/{user}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<OperationDiff> requestOperation(@RequestBody FunctionParam target, @PathVariable("operationType") OPERATIONS type, @PathVariable("start") Long start,
                                                          @PathVariable("end") Long end, @PathVariable("driver") Integer driver, @PathVariable("user") Integer user) {

        OperationDiff result = null;
        String error = "";

        try {
            result = this.is.requestOperation(target, type, start, end, driver, user);
        } catch (Exception e) {
            e.printStackTrace();
            error = e.getLocalizedMessage();
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "calendar/{dtInicio}/{dtFim}/{motorista}/{preview}/{fired}/{firedDate}/{user}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<CalendarVO> calendar(@PathVariable("dtInicio") Long dtInicio, @PathVariable("dtFim") Long dtFim, @PathVariable("motorista") Integer motorista, @PathVariable("preview") Boolean preview,
                                               @PathVariable("fired") Boolean fired, @PathVariable("firedDate") Long firedDate, @PathVariable("user") Integer user) {
        CalendarVO result = null;
        String error = "";
        try {
            Motoristas driver = this.driverService.getById(motorista);

            if (Objects.nonNull(driver.getTurnosId()) && driver.getTurnosId().getTipo() == 0) {
                driver.getTurnosId().setWeekTurnDay(0);
            }

            if (Objects.isNull(driver.getTurnosId()) || Objects.isNull(driver.getTurnosId().getWeekTurnDay())) {
                error = "Turno do motorista selecionado não está configurado corretamente!";
            } else {
                js.removeDuplicates(dtInicio, dtFim, motorista);
                Date dataInicial = new Date(dtInicio);
                Date dataFinal = new Date(dtFim);
                result = is.assembleCalendar(dataInicial, dataFinal, driver, preview, fired, new Date(firedDate), user);
            }
        } catch (Exception e) {
            e.printStackTrace();
            error = e.getMessage();
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "lista/{dtInicio}/{dtFim}/{motorista}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<NonconformityVO>> find(@PathVariable("dtInicio") Long dtInicio, @PathVariable("dtFim") Long dtFim, @PathVariable("motorista") Integer motorista, @RequestParam("tipo") List<Integer> tipos) {
        String error = "";
        List<NonconformityVO> result = Collections.emptyList();

        Motoristas driver = this.driverService.getById(motorista);

        if (Objects.isNull(driver.getTurnosId()) || Objects.isNull(driver.getTurnosId().getWeekTurnDay())) {
            error = "Turno do motorista selecionado não está configurado corretamente!";
        } else {
            try {
                Date dataInicial = TimeHelper.getDate000000(new Date(dtInicio));
                Date dataFinal = TimeHelper.getDate235959(new Date(dtFim));
                result = is.calculate(motorista, dataInicial, dataFinal, tipos, false);
            } catch (ConstraintViolationException cause) {
                error = RodoviaUtils.handleConstraintViolation(cause);
            } catch (Exception e) {
                error = RodoviaUtils.exceptionMessage(e);
            }
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "tipos", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<NONCONFORMITY_CALCULUS_TYPE>> retrieveTypes() {
        String error = "";
        List<NONCONFORMITY_CALCULUS_TYPE> result = Collections.emptyList();
        try {
            result = Arrays.asList(NONCONFORMITY_CALCULUS_TYPE.values());
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.POST, path = "uploadTreatments/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<String> uploadTreatments(@RequestBody List<NonconformityTreatment> treatments, @PathVariable("id") Integer user) {
        String error = "";
        String result = "";

        try {
            result = is.uploadTreatments(treatments, user);
        } catch (ConstraintViolationException cause) {
            cause.printStackTrace();
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            e.printStackTrace();
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/buscarInconformidadesDoDia/{idEmpresa}/{idUnidade}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<RetornoInconformidadeVO> buscarInconformidadesDoDia(@PathVariable("idEmpresa") Integer idEmpresa, @PathVariable("idUnidade") Integer idUnidade) {
        String error = "";
        RetornoInconformidadeVO retornoInconformidadeVO = new RetornoInconformidadeVO();
        try {
            List<Inconformidade> listaAlertaViaSat = is.buscarAlertasDoViaSatDia(idEmpresa, idUnidade);
            is.buscarAlertasForaDaCerca(idEmpresa, idUnidade, listaAlertaViaSat);
            List<Inconformidade> listaDeConformes = is.buscarInconformidadesDoDia(idEmpresa, idUnidade);
            List<Inconformidade> listaInconformidadeViaSat = new ArrayList<>();
            List<Inconformidade> listaInconformidadeViaM = new ArrayList<>();
            for (Inconformidade i : listaDeConformes) {
                if (Objects.nonNull(i.getIdVeiculo())) {
                    listaInconformidadeViaSat.add(i);
                } else {
                    listaInconformidadeViaM.add(i);
                }
            }
            AlertaInconformidadeVO alertaInconformidadeVO = new AlertaInconformidadeVO();
            alertaInconformidadeVO.setListaInconformidadeViaM(listaInconformidadeViaM);
            alertaInconformidadeVO.setListaInconformidadeViaSat(listaInconformidadeViaSat);
            alertaInconformidadeVO.setListaAlertaViaSat(listaAlertaViaSat);
            retornoInconformidadeVO.setAlertaInconformidade(alertaInconformidadeVO);

        } catch (ConstraintViolationException cause) {
            cause.printStackTrace();
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            e.printStackTrace();
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, retornoInconformidadeVO);
    }


    @RequestMapping(method = RequestMethod.GET, path = "/buscarTodasInconformidades/{idEmpresa}/{idUnidade}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<RetornoInconformidadeVO> buscarTodasInconformidades(@PathVariable("idEmpresa") Integer idEmpresa, @PathVariable("idUnidade") Integer idUnidade) {
        String error = "";
        RetornoInconformidadeVO retornoInconformidadeVO = new RetornoInconformidadeVO();
        try {
            Empresas empresas = empresasService.getById(idEmpresa);
            if (empresas.getHabilitarViam() != null && empresas.getHabilitarViam()) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                List<Inconformidade> listaDeConformes = is.buscarTodasInconformidades(idEmpresa, idUnidade);
                if (Objects.nonNull(listaDeConformes)) {
                    listaDeConformes.forEach(i -> {
                        i.setDataInconformidadeFormatada(simpleDateFormat.format(i.getDataInconformidade()));
                    });
                }
                retornoInconformidadeVO.setListaDeInconformidade(listaDeConformes);
            }
        } catch (ConstraintViolationException cause) {
            cause.printStackTrace();
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            e.printStackTrace();
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, retornoInconformidadeVO);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/marcarComoLido/{idInconformidade}/{idUsuario}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<RetornoInconformidadeVO> marcarComoLido(@PathVariable("idInconformidade") Integer idInconformidade, @PathVariable("idUsuario") Integer idUsuario) {
        String error = "";
        RetornoInconformidadeVO retornoInconformidadeVO = new RetornoInconformidadeVO();
        try {
            is.marcarComoLido(idInconformidade, idUsuario);
            retornoInconformidadeVO.setCodigoRetorno(1);
            retornoInconformidadeVO.setMensagem("Inconformidade editada com sucesso");
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, retornoInconformidadeVO);
    }


    @RequestMapping(method = RequestMethod.GET, path = "/buscarPeloId/{idInconformidade}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<RetornoInconformidadeVO> buscarPeloId(@PathVariable("idInconformidade") Integer idInconformidade) {
        String error = "";
        RetornoInconformidadeVO retornoInconformidadeVO = new RetornoInconformidadeVO();
        try {
            Inconformidade inconformidade = is.buscarPeloId(idInconformidade);
            retornoInconformidadeVO.setInconformidade(inconformidade);
            retornoInconformidadeVO.setCodigoRetorno(1);
            retornoInconformidadeVO.setMensagem("Inconformidade carregada com sucesso");
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, retornoInconformidadeVO);
    }

    @RequestMapping(value = "/editar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoInconformidadeVO> editar(@RequestBody Inconformidade inconformidade) {

        RetornoInconformidadeVO retornoInconformidadeVO = new RetornoInconformidadeVO();
        try {
            is.editar(inconformidade);
            retornoInconformidadeVO.setCodigoRetorno(1);
            retornoInconformidadeVO.setMensagem("Inconformidade editada com sucesso");
        } catch (ConstraintViolationException cause) {
            retornoInconformidadeVO.setCodigoRetorno(-1);
            retornoInconformidadeVO.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retornoInconformidadeVO.setMensagem(RodoviaUtils.exceptionMessage(e));
            retornoInconformidadeVO.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retornoInconformidadeVO, HttpStatus.OK);
    }

}