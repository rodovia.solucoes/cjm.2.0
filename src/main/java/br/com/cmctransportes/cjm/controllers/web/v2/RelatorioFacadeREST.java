package br.com.cmctransportes.cjm.controllers.web.v2;


import br.com.cmctransportes.cjm.domain.entities.Transmissao;
import br.com.cmctransportes.cjm.domain.entities.vo.*;
import br.com.cmctransportes.cjm.domain.services.RelatorioService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(path = "App/v2/web/rel")
public class RelatorioFacadeREST {

    @Autowired
    private RelatorioService relatorioService;


    @RequestMapping(value = "/buscarrelatoriocompreensor", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRelatorioVO> buscarRelatorioDeAcionamentoDeCompreensor(@RequestBody FiltroVO filtro) {

        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            retorno = relatorioService.buscarRelatorioDeAcionamentoDeCompreensor(filtro);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    @RequestMapping(value = "/buscarrelatoriotomadaforca", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRelatorioVO> buscarRelatorioAcionamentoTomadaDeForca(@RequestBody FiltroVO filtro) {

        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            retorno = relatorioService.buscarRelatorioAcionamentoTomadaDeForca(filtro);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarrelatoriodisponibilidade", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRelatorioVO> buscarRelatorioDisponibilidade(@RequestBody FiltroVO filtro) {

        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            retorno = relatorioService.buscarRelatorioDisponibilidade(filtro);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarrelatoriodisponibilidadegrafico", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRelatorioVO> buscarRelatorioDisponibilidadeGrafico(@RequestBody FiltroVO filtro) {

        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            retorno = relatorioService.buscarRelatorioDisponibilidadeGrafico(filtro);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarrelatorioevento", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRelatorioVO> buscarRelatorioEvento(@RequestBody FiltroVO filtro) {

        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            retorno = relatorioService.buscarRelatorioEvento(filtro);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    @RequestMapping(value = "/buscarrelatoriohorimetro", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRelatorioVO> buscarRelatorioHorimetro(@RequestBody FiltroVO filtro) {

        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            retorno = relatorioService.buscarRelatorioHorimetro(filtro);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    @RequestMapping(value = "/buscarrelatoriopercurso", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRelatorioVO> buscarRelatorioPercurso(@RequestBody FiltroVO filtro) {

        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            retorno = relatorioService.buscarRelatorioPercurso(filtro);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    @RequestMapping(value = "/buscarrelatoriovelocidade", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRelatorioVO> buscarRelatorioVelocidade(@RequestBody FiltroVO filtro) {

        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            retorno = relatorioService.buscarRelatorioVelocidade(filtro);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    @RequestMapping(value = "/buscarrelatorioequipamentoportatil", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRelatorioVO> buscarrelatorioequipamentoportatil(@RequestBody FiltroVO filtro) {

        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            retorno = relatorioService.buscarRelatorioPercursoPortatil(filtro);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarRelatorioDiariaMotorista", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRelatorioVO> buscarRelatorioDiariaMotorista(@RequestBody FiltroVO filtro) {

        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            retorno = relatorioService.buscarRelatorioDiariaMotorista(filtro);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    @RequestMapping(value = "/buscarrelatoriotransportedeequipamentos", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRelatorioVO> buscarRelatorioTransporteDeEquipamentos(@RequestBody FiltroVO filtro) {

        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            retorno = relatorioService.buscarRelatorioTransporteDeEquipamentos(filtro);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarrelatoriopresencadeequipamentos", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRelatorioVO> buscarRelatorioPresencaDeEquipamentos(@RequestBody FiltroVO filtro) {

        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            retorno = relatorioService.buscarRelatorioPresencaDeEquipamentos(filtro);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscargraficoproducaoequipamento", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRelatorioVO> buscarGraficoProducaoEquipamento(@RequestBody FiltroVO filtro) {

        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            retorno = relatorioService.buscarGraficoProducaoEquipamento(filtro);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    @RequestMapping(value = "/buscarsensorbetoneira", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRelatorioVO> buscarSensorBetoneira(@RequestBody FiltroVO filtro) {

        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            retorno = relatorioService.buscarSensorBetoneira(filtro);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    @RequestMapping(value = "/buscarNumeroDeViagem", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRelatorioVO> buscarNumeroDeViagem(@RequestBody FiltroVO filtro) {

        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            retorno = relatorioService.buscarNumeroDeViagem(filtro);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarDetalhesTelemetria", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRelatorioVO> buscarDetalhesTelemetria(@RequestBody FiltroVO filtro) {

        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            List<DetalheTelemetriaVO> lista = relatorioService.buscarDetalhesTelemetria(filtro);
            retorno.setListaDetalheTelemetria(lista);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarTempoTransporteGrafico", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRelatorioVO> buscarTempoTransporteGrafico(@RequestBody FiltroVO filtro) {

        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            RelatorioGraficoVO relatorioGraficoVO = relatorioService.buscarTempoTransporteGrafico(filtro);
            retorno.setRelatorioGrafico(relatorioGraficoVO);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarTempoCarregamentoGrafico", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRelatorioVO> buscarTempoCarregamentoGrafico(@RequestBody FiltroVO filtro) {

        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            RelatorioGraficoVO relatorioGraficoVO = relatorioService.buscarTempoCarregamento(filtro);
            retorno.setRelatorioGrafico(relatorioGraficoVO);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    @RequestMapping(value = "/buscarVelocidadeParaRelatorio", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRelatorioVO> buscarVelocidadeParaRelatorio(@RequestBody FiltroVO filtro) {
        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            RelatorioGraficoVO relatorioGraficoVO = relatorioService.buscarVelocidadeParaRelatorio(filtro);
            retorno.setRelatorioGrafico(relatorioGraficoVO);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    @RequestMapping(value = "/buscarSensorPorta", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRelatorioVO> buscarSensorPorta(@RequestBody FiltroVO filtro) {
        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            retorno = relatorioService.buscarSensorPorta(filtro);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    @RequestMapping(value = "/buscarRelatorioReferenciaLocal", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRelatorioVO> buscarRelatorioReferenciaLocal(@RequestBody FiltroVO filtro) {
        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            retorno = relatorioService.buscarRelatorioReferenciaLocal(filtro);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    @RequestMapping(value = "/buscarRelatorioReferenciaLocalDetalhes", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRelatorioVO> buscarRelatorioReferenciaLocalDetalhes(@RequestBody FiltroVO filtro) {
        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            retorno = relatorioService.buscarRelatorioReferenciaLocalDetalhes(filtro);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    @RequestMapping(value = "/buscarRelatorioPercursoDetalhado", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRelatorioVO> buscarRelatorioPercursoDetalhado(@RequestBody FiltroVO filtro) {
        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            retorno = relatorioService.buscarRelatorioPercursoDetalhado(filtro);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarRelatorioEventoSensoresDetalhado", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRelatorioVO> buscarRelatorioEventoSensoresDetalhado(@RequestBody FiltroVO filtro) {
        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            retorno = relatorioService.buscarRelatorioEventoSensoresDetalhado(filtro);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarRelatorioProdutividade", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRelatorioVO> buscarRelatorioProdutividade(@RequestBody FiltroVO filtro) {
        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            retorno = relatorioService.buscarRelatorioEventosMotoristas(filtro);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarRelatorioProdutividadeVeiculo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRelatorioVO> buscarRelatorioProdutividadeVeiculo(@RequestBody FiltroVO filtro) {
        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            retorno = relatorioService.buscarRelatorioProdutividadeVeiculo(filtro);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarPontosPercorridos", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoRelatorioVO> buscarPontosPercorridos(@RequestBody FiltroVO filtro) {
        RetornoRelatorioVO retorno = new RetornoRelatorioVO();
        try {
            retorno = relatorioService.buscarPontosPercorridos(filtro);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


}
