package br.com.cmctransportes.cjm.controllers.web.v2;

/**
 * @author Catatau
 */

import br.com.cmctransportes.cjm.domain.entities.UsersClaims;
import br.com.cmctransportes.cjm.domain.entities.vo.RESTResponseVO;
import br.com.cmctransportes.cjm.domain.services.UsersClaimsService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping(path = "App/v2/web/claims")
public class UserClaimsFacadeRest {

    @Autowired
    private UsersClaimsService claimsService;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<UsersClaims> create(@RequestBody UsersClaims entity) {
        String error = "";
        try {
            claimsService.save(entity);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, entity);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<UsersClaims> edit(@PathVariable("id") Integer id, @RequestBody UsersClaims entity) {
        String error = "";
        try {
            claimsService.update(entity);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, entity);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "{id}")
    public RESTResponseVO<Boolean> remove(@PathVariable("id") Integer id) {
        String error = "";
        boolean result = false;

        try {
            claimsService.delete(id);
            result = true;
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<UsersClaims> find(@PathVariable("id") Integer id) {
        String error = "";
        UsersClaims entity = null;
        try {
            entity = claimsService.getById(id);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, entity);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<UsersClaims>> findAll() {
        String error = "";
        List<UsersClaims> entity = Collections.emptyList();
        try {
            entity = claimsService.findAll();
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, entity);
    }
}