package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.EventosJornada;
import br.com.cmctransportes.cjm.domain.entities.MotivoAbono;
import br.com.cmctransportes.cjm.domain.entities.vo.RESTResponseVO;
import br.com.cmctransportes.cjm.domain.services.EventosJornadaService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

/**
 * @author analista.ti
 */
@RestController
@RequestMapping(path = "App/v2/web/eventosJornada")
public class EventosJornadaFacadeREST {

    @Autowired
    private EventosJornadaService cs;

    @RequestMapping(method = RequestMethod.GET, path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<EventosJornada> find(@PathVariable("id") Integer id) {
        EventosJornada result = null;
        String error = "";
        try {
            result = cs.getById(id);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<EventosJornada>> findAll() {
        List<EventosJornada> result = Collections.emptyList();
        String error = "";
        try {
            result = cs.findAll();
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "{idJornada}/{date}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<EventosJornada>> findByDateJourney(@PathVariable("idJornada") Integer idJornada, @PathVariable("date") Long date) {
        List<EventosJornada> result = Collections.emptyList();
        String error = "";
        try {
            result = cs.findByDateJourney(idJornada, date);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "allowances", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<MotivoAbono>> findAllowances() {
        List<MotivoAbono> result = Collections.emptyList();
        String error = "";
        try {
            result = cs.findAllowances();
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, result);
    }
}