package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.vo.ObservacaoVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoObservacaoVO;
import br.com.cmctransportes.cjm.domain.services.ObservacaoService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "App/v2/web/observacao")
public class ObservacaoRest {

    @Autowired
    private ObservacaoService observacaoService;

    @RequestMapping(value = "/cadastrar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoObservacaoVO> cadastrar(@RequestBody ObservacaoVO observacaoVO) {

        RetornoObservacaoVO retorno = new RetornoObservacaoVO();
        try {
            retorno = observacaoService.cadastrar(observacaoVO);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    @RequestMapping(value = "/listarObservacaoTrajetto/{idVeiculo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoObservacaoVO> listarObservacaoTrajetto(@PathVariable("idVeiculo") Integer idVeiculo) {

        RetornoObservacaoVO retorno = new RetornoObservacaoVO();
        try {
            retorno = observacaoService.listarObservacaoTrajetto(idVeiculo);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    @RequestMapping(value = "/listarObservacaoViam/{idMotorista}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoObservacaoVO> listarObservacaoViam(@PathVariable("idMotorista") Integer idMotorista) {

        RetornoObservacaoVO retorno = new RetornoObservacaoVO();
        try {
            retorno = observacaoService.listarObservacaoViam(idMotorista);
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }
}
