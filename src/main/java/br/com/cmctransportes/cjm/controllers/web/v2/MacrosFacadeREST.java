package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.domain.entities.Macros;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoMacrosVO;
import br.com.cmctransportes.cjm.domain.services.MacrosService;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "App/v2/web/macros")
public class MacrosFacadeREST {


    @Autowired
    private MacrosService macrosService;

    @RequestMapping(value = "/cadastrar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoMacrosVO> cadastrar(@RequestBody Macros macros) {
        RetornoMacrosVO retorno = new RetornoMacrosVO();
        try {
            macrosService.save(macros);
            retorno.setCodigoRetorno(1);
            retorno.setMensagem("Macros cadastrado com sucesso!");
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/editar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoMacrosVO> editar(@RequestBody Macros macros) {
        RetornoMacrosVO retorno = new RetornoMacrosVO();
        try {
            macrosService.editar(macros);
            retorno.setCodigoRetorno(1);
            retorno.setMensagem("Macros editada com sucesso!");
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }


    @RequestMapping(value = "/buscarMacros/{idEmpresa}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoMacrosVO> buscarLista(@PathVariable Integer idEmpresa) {
        RetornoMacrosVO retorno = new RetornoMacrosVO();
        try {
            List<Macros> lista = this.macrosService.buscarLista(idEmpresa);
            retorno.setListaDeMacros(lista);
            retorno.setCodigoRetorno(1);
            retorno.setMensagem("Macros listada com sucesso!");
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }

    @RequestMapping(value = "/buscarPeloId/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetornoMacrosVO> buscarPeloId(@PathVariable Integer id) {
        RetornoMacrosVO retorno = new RetornoMacrosVO();
        try {
            Macros macros = this.macrosService.buscarPeloId(id);
            retorno.setMacros(macros);
            retorno.setCodigoRetorno(1);
            retorno.setMensagem("Macros recuparada com sucesso!");
        } catch (ConstraintViolationException cause) {
            retorno.setCodigoRetorno(-1);
            retorno.setMensagem(RodoviaUtils.handleConstraintViolation(cause));
        } catch (Exception e) {
            retorno.setMensagem(RodoviaUtils.exceptionMessage(e));
            retorno.setCodigoRetorno(-1);
        }
        return new ResponseEntity<>(retorno, HttpStatus.OK);
    }
}
