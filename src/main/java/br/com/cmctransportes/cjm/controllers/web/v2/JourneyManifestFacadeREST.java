package br.com.cmctransportes.cjm.controllers.web.v2;

/**
 * @author William Leite
 */

import br.com.cmctransportes.cjm.domain.entities.Jornada;
import br.com.cmctransportes.cjm.domain.entities.JourneyManifest;
import br.com.cmctransportes.cjm.domain.entities.vo.JourneyManifestVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RESTResponseVO;
import br.com.cmctransportes.cjm.domain.services.JourneyManifestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "App/v2/web/journeymanifest")
public class JourneyManifestFacadeREST {

    @Autowired
    private JourneyManifestService service;

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<JourneyManifestVO> create(@RequestBody JourneyManifestVO vo) {
        try {

            JourneyManifest entity = service.convert(vo);
            service.save(entity);

            return new RESTResponseVO<>("", service.convert(entity));
        } catch (Exception e) {
            e.printStackTrace();
            return new RESTResponseVO<>(e.getLocalizedMessage(), null);
        }
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<JourneyManifestVO> update(@RequestBody JourneyManifestVO vo) {
        try {
            JourneyManifest entity = service.getById(vo.getId());
            entity.setEndDate(vo.getEndDate());
            entity.setStartDate(vo.getStartDate());
            entity.setJourney(new Jornada(vo.getJourney()));
            entity.setManifestNumber(vo.getManifest());

            service.save(entity);

            return new RESTResponseVO<>("", service.convert(entity));
        } catch (Exception e) {
            e.printStackTrace();
            return new RESTResponseVO<>(e.getLocalizedMessage(), null);
        }
    }

    @RequestMapping(path = "{journey}", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<JourneyManifestVO>> retrieve(@PathVariable("journey") Integer journey) {
        try {
            List<JourneyManifest> result = service.findByJourney(journey);
            return new RESTResponseVO<>("", result.stream().map(service::convert).collect(Collectors.toList()));
        } catch (Exception e) {
            e.printStackTrace();
            return new RESTResponseVO<>(e.getLocalizedMessage(), new ArrayList<>(0));
        }
    }
}