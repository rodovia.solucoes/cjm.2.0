package br.com.cmctransportes.cjm.controllers.web.v2;

import br.com.cmctransportes.cjm.controllers.runnable.JornadaRunnble;
import br.com.cmctransportes.cjm.domain.entities.*;
import br.com.cmctransportes.cjm.domain.entities.evento.EventoEncadeado;
import br.com.cmctransportes.cjm.domain.entities.vo.*;
import br.com.cmctransportes.cjm.domain.services.*;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import br.com.cmctransportes.cjm.utils.TimeHelper;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author analista.ti
 */
@RestController
@RequestMapping(path = "App/v2/web/eventos")
public class EventoFacadeREST {

    @Autowired
    private EventoService es;

    @Autowired
    private JornadaService js;

    @Autowired
    private DriverService ds;

    @Autowired
    private UserService userService;

    @Autowired
    private EmpresasService empresasService;

    @Autowired
    private EventosJornadaService eventosJornadaService;

    @Autowired
    private RastroService rastroService;


    private void fillJourney(Evento entity) {
        if (Objects.isNull(entity.getJornadaId().getEventos())) {
            entity.setJornadaId(js.getById(entity.getJornadaId().getId()));
        }
    }

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Evento> create(@RequestBody Evento entity) {
        String error = "";
        try {
            this.fillJourney(entity);

            es.save(entity);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, entity);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "{id}/{userid}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Evento> edit(@PathVariable("id") Integer id, @RequestBody Evento entity, @PathVariable("userid") Integer userId) {
        String error = "";
        try {
            Evento evento = es.getById(id);

            Users dbUser = userService.getById(userId);

            if (Objects.equals(evento.getLocked(), Boolean.TRUE) && dbUser.getUsersClaims().stream().noneMatch(c -> Objects.equals(c.getClaim(), "apuracao"))) {
                throw new RuntimeException("Evento bloqueado e usuário não possui permissão para efetuar alteração!");
            }

            es.update(id, entity);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, entity);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "marcaremovido/{id}/{userid}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Boolean> marcaRemovido(@PathVariable("id") Integer id, @RequestBody Evento entity, @PathVariable("userid") Integer userId) {

        String error = "";
        boolean result = false;

        try {
            Evento evento = this.es.getById(id);
            Users dbUser = userService.getById(userId);

            if (Objects.equals(evento.getLocked(), Boolean.TRUE) && dbUser.getUsersClaims().stream().noneMatch(c -> Objects.equals(c.getClaim(), "apuracao"))) {
                throw new RuntimeException("Evento bloqueado ou usuário não possui permissão para efetuar alteração!");
            }

            es.marcaRemovido(id, entity);
            result = true;
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);

    }

    @RequestMapping(method = RequestMethod.DELETE, path = "{id}/{user}")
    public RESTResponseVO<Boolean> remove(@PathVariable("id") Integer id, @PathVariable("user") Integer user) {
        String error = "";
        boolean result = false;

        try {
            es.delete(id, user);
            result = true;
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);

    }

    @RequestMapping(method = RequestMethod.GET, path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Evento> find(@PathVariable("id") Integer id) {
        String error = "";
        Evento evento = null;

        try {
            evento = es.getById(id);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, evento);
    }

    @RequestMapping(method = RequestMethod.POST, path = "encadeado/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Boolean> createChained(@RequestBody EventoEncadeado evento, @PathVariable("id") Integer user) {
        String error = "";
        Boolean result = false;
        try {
            Jornada journey = js.getById(evento.getEventoFinal().getJornadaId());
            Users dbUser = userService.getById(user);

            boolean locked = Objects.equals(journey.getLocked(), Boolean.TRUE);

            if (locked && dbUser.getUsersClaims().stream().noneMatch(c -> Objects.equals(c.getClaim(), "apuracao"))) {
                throw new RuntimeException("Jornada bloqueada e usuário não possui permissão para efetuar alteração!");
            }

            Evento eventoIni = evento.getEventoInicio().revert(this.empresasService, this.js, this.eventosJornadaService, this.userService);
            Evento eventoFim = evento.getEventoFinal().revert(this.empresasService, this.js, this.eventosJornadaService, this.userService);

            es.saveChained(eventoIni, eventoFim, true);
            result = true;
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "encadeado/{idInicio}/{idFinal}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<Evento>> editChained(@PathVariable("idInicio") Integer idInicio, @PathVariable("idFinal") Integer idFinal, @RequestBody EventoEncadeado evento) {

        String error = "";
        List<Evento> result = Collections.emptyList();
        try {
            Evento eventoIni = evento.getEventoInicio().revert(this.empresasService, this.js, this.eventosJornadaService, this.userService);
            Evento eventoFim = evento.getEventoFinal().revert(this.empresasService, this.js, this.eventosJornadaService, this.userService);

            es.updateChained(idInicio, eventoIni, idFinal, eventoFim);
            result = Arrays.asList(eventoIni, eventoFim);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "encadeado/{idInicio}/{idFinal}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Boolean> removeChained(@PathVariable("idInicio") Integer idInicio, @PathVariable("idFinal") Integer idFinal) {

        String error = "";
        boolean result = false;

        try {
            es.deleteChained(idInicio, idFinal);
            result = true;
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "relatorio/{motorista}/{dataini}/{datafim}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<RelatorioEventoVO>> geraRelatorio(@PathVariable("motorista") Integer motoristaId, @PathVariable("dataini") Long dataInicio, @PathVariable("datafim") Long dataFim) {
        String error = "";
        List<RelatorioEventoVO> result = Collections.emptyList();

        try {
            result = es.relatorioDeEventos(motoristaId, dataInicio, dataFim);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "eventoAtual/{idEmpresa}/{idUnidade}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<RetornoEventoVO> findCurrentEventAll(@PathVariable("idEmpresa") Integer idEmpresa, @PathVariable("idUnidade") Integer idUnidade) {

        List<Evento> listaDeEventos = new ArrayList<>();
        RetornoEventoVO retornoEventoVO = new RetornoEventoVO();
        retornoEventoVO.setViamAtivo(false);
        String error = null;
        try {
            retornoEventoVO.setCodigoRetorno(0);
            Empresas empresas = empresasService.getById(idEmpresa);
            if (empresas.getHabilitarViam() != null && empresas.getHabilitarViam()) {
                List<Evento> result = es.findCurrentEventAll(idEmpresa, idUnidade == 0 ? null : idUnidade);
                result.forEach(r -> {
                    r.setResumoJornada(js.getResumoJornada(r.getJornada()));
                    MotoristaVO motoristas = r.getOperadorLancamento().getMotoristas();
                    if (motoristas == null) {
                        motoristas = js.findDriversByJourney(r.getJornada());
                        r.getOperadorLancamento().setMotoristas(motoristas);
                    }
                    // Rastro rastro = rastroService.buscarUltimoRastro(motoristas);
                    r.setRastro(null);
                    r.setInstanteEventoFormatado(new SimpleDateFormat("dd/MM HH:mm:ss").format(r.getInstanteEvento()));
                });


                Map<Integer, Evento> map = new HashMap<>();
                result.forEach(e -> {
                    Motoristas m = e.getJornadaId().getMotoristasId();
                    if (map.containsKey(m.getId())) {
                        Evento e2 = map.get(m.getId());
                        long data1 = e.getInstanteLancamento().getTime();
                        long data2 = e2.getInstanteLancamento().getTime();
                        if (data1 > data2) {
                            map.replace(m.getId(), e);
                        }
                    } else {
                        map.put(m.getId(), e);
                    }
                });

                listaDeEventos.addAll(map.values());

                retornoEventoVO.setListaDeEventos(listaDeEventos);
                List<EventosJornada> lista = eventosJornadaService.findAll();
                retornoEventoVO.setListaDeEventosJornada(lista);
                retornoEventoVO.setViamAtivo(true);
            }


        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, retornoEventoVO);
    }

    @RequestMapping(method = RequestMethod.GET, path = "ultimoEvento/{motorista}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Evento> findLastEvent(@PathVariable("motorista") Integer motorista) {
        String error = "";
        Evento evento = new Evento();

        try {
            evento = es.getLastEvent(motorista);

            if (evento == null) {
                evento = new Evento();
            }
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, evento);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<Evento>> findAll() {
        List<Evento> result = Collections.emptyList();
        String error = null;
        try {
            result = es.findAll();
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }
        return new RESTResponseVO<>(error, result);
    }

    // #2041560
    @RequestMapping(method = RequestMethod.POST, path = "abono/periodo/{motorista}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<Evento> createAllowancePeriod(@RequestBody EventoEncadeado evento, @PathVariable("motorista") Integer motorista) {

        String error = "";
        Evento eventoIni = null;
        try {
            eventoIni = evento.getEventoInicio().revert(this.empresasService, this.js, this.eventosJornadaService, this.userService);
            Evento eventoFim = evento.getEventoFinal().revert(this.empresasService, this.js, this.eventosJornadaService, this.userService);

            js.saveRangedAllowance(eventoIni, eventoFim, motorista);
        } catch (ConstraintViolationException cause) {
            error = RodoviaUtils.handleConstraintViolation(cause);
        } catch (Exception e) {
            error = RodoviaUtils.exceptionMessage(e);
        }

        return new RESTResponseVO<>(error, eventoIni);
    }

    @RequestMapping(method = RequestMethod.POST, path = "list/{dtInicio}/{dtFim}/{user}/{type}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<JornadaMotorista>> getList(@PathVariable("type") String type, @RequestBody List<Integer> motoristasId, @PathVariable("dtInicio") Long dtInicio, @PathVariable("dtFim") Long dtFim, @PathVariable("user") Integer user) {

        String error = "";
        List<JornadaMotorista> result = new ArrayList<>();

        try {
            AtomicBoolean turnoErr = new AtomicBoolean(false);
            List<Motoristas> motoristas = new ArrayList<>();

            motoristasId.stream().forEach(id -> {
                Motoristas m = ds.getById(id);
                motoristas.add(m);

                js.removeDuplicates(dtInicio, dtFim, id);

                if (Objects.isNull(m.getTurnosId()) || Objects.isNull(m.getTurnosId().getWeekTurnDay())) {
                    turnoErr.set(true);
                }
            });

            if (turnoErr.get()) {
                error = "Turno do motorista selecionado não está configurado corretamente!";
            } else {
                Date dataInicial = TimeHelper.getDate000000(new Date(dtInicio));
                Date dataFinal = TimeHelper.getDate235959(new Date(dtFim));

                result = es.mountEventList(dataInicial, dataFinal, motoristas, user, type);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new RESTResponseVO<>(error, result);
    }

}