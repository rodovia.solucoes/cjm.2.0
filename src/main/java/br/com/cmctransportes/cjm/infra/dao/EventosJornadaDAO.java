/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.EventosJornada;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 * @author analista.ti
 */
public class EventosJornadaDAO extends GenericDAO<Integer, EventosJornada> {

    public EventosJornadaDAO(EntityManager entityManager) {
        super(entityManager);
    }

    public List<EventosJornada> findByName(String nome) {
        List<EventosJornada> result = null;

        try {
            String sql = "FROM EventosJornada WHERE descricao LIKE :descricao";
            Query query = entityManager.createQuery(sql).setParameter("descricao", "%" + nome + "%");
            result = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return result;
    }


    public List<EventosJornada> buscarEventoDSR() {
        List<EventosJornada> result = null;
        try {
            result = findByName("DSR");
            for(EventosJornada e: result){
                if(e.getId() > 0){
                    e.setDescricao("Inicio DSR");
                }else{
                    e.setDescricao("Fim DSR");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return result;
    }
}
