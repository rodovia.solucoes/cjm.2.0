package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.Empresas;
import br.com.cmctransportes.cjm.domain.entities.Enderecos;
import br.com.cmctransportes.cjm.domain.entities.Parametro;
import br.com.cmctransportes.cjm.domain.entities.Permissao;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgres;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgresOrion;
import br.com.cmctransportes.cjm.utils.TimeHelper;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.sql.*;
import java.util.*;
import java.util.Date;

/**
 * #2033016
 *
 * @author William Leite
 */
public class EmpresasDAO extends GenericDAO<Integer, Empresas> {

    private EntityManager em;

    public EmpresasDAO(EntityManager entityManager) {
        super(entityManager);
        this.em = entityManager;
    }

    public List<Empresas> buscarTodos() throws Exception {
        Query q = entityManager.createQuery("FROM Empresas e ORDER BY e.nome");
        List<Empresas> result = q.getResultList();
        return result;
    }

    public boolean chaveValida(String uuid) throws Exception {
        Query q = entityManager.createQuery("FROM Empresas e where e.uuid = :uuid");
        q.setParameter("uuid", uuid);
        List<Empresas> result = q.getResultList();
        if(Objects.nonNull(result) && result.isEmpty()){
            return false;
        }

        if(Objects.nonNull(result) && !result.isEmpty()){
            return true;
        }
        return false;
    }

    public void inserirEmpresaNativo(Empresas empresas) throws Exception {

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {

            if (empresas.getEndereco() == null) {
                empresas.setEndereco(new Enderecos());
            }

            int idEndereco = new EnderecosDAO(this.em).gravarEnderecoNativo(empresas.getEndereco());
            if (empresas.getParametro() == null) {
                empresas.setParametro(new Parametro());
            }
            int idParametro = new ParametroDAO(this.em).gravarParametrosnativo(empresas.getParametro());


            String sql = "INSERT INTO empresas( " +
                    " id, nome, display_name, cnpj, inscricao_estadual, inscricao_municipal, endereco, qtd_usuarios, data_validade, empresa_id, telefone, responsavel, email, licenca, bosh_service_url, interjourney_compensation_limit, interjourney_compensate_standby, worked_days_dsr, missing_days_dsr, considers_interjourney, half_interjourney, sascar_integration, frequency_reading, habilitar_viam, habilitar_viasat, id_cliente_trajetto, parametro) " +
                    " VALUES (nextval('empresas_id_seq'), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            connection = DataSourcePostgres.getInstance().getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(sql);
            if (empresas.getNome() != null) {
                preparedStatement.setString(1, empresas.getNome());
            } else {
                preparedStatement.setNull(1, Types.VARCHAR);
            }
            if (empresas.getDisplayName() != null) {
                preparedStatement.setString(2, empresas.getDisplayName());
            } else {
                preparedStatement.setNull(2, Types.VARCHAR);
            }

            if (empresas.getCnpj() != null) {
                preparedStatement.setString(3, empresas.getCnpj());
            } else {
                preparedStatement.setNull(3, Types.VARCHAR);
            }

            if (empresas.getInscricaoEstadual() != null) {
                preparedStatement.setString(4, empresas.getInscricaoEstadual());
            } else {
                preparedStatement.setNull(4, Types.VARCHAR);
            }

            if (empresas.getInscricaoMunicipal() != null) {
                preparedStatement.setString(5, empresas.getInscricaoMunicipal());
            } else {
                preparedStatement.setNull(5, Types.VARCHAR);
            }

            preparedStatement.setInt(6, idEndereco);

            if (empresas.getQuantidadeUsuarios() != null) {
                preparedStatement.setInt(7, empresas.getQuantidadeUsuarios());
            } else {
                preparedStatement.setNull(7, Types.INTEGER);
            }

            if (empresas.getDataValidade() != null) {
                preparedStatement.setTimestamp(8, new Timestamp(empresas.getDataValidade().getTime()));
            } else {
                preparedStatement.setNull(8, Types.TIMESTAMP);
            }

            if (empresas.getEmpresaPrincipal() != null && empresas.getEmpresaPrincipal().getId() != null) {
                preparedStatement.setInt(9, empresas.getEmpresaPrincipal().getId());
            } else {
                preparedStatement.setNull(9, Types.INTEGER);
            }

            if (empresas.getTelefone() != null) {
                preparedStatement.setString(10, empresas.getTelefone());
            } else {
                preparedStatement.setNull(10, Types.VARCHAR);
            }

            if (empresas.getResponsavel() != null) {
                preparedStatement.setString(11, empresas.getResponsavel());
            } else {
                preparedStatement.setNull(11, Types.VARCHAR);
            }

            if (empresas.getEmail() != null) {
                preparedStatement.setString(12, empresas.getEmail());
            } else {
                preparedStatement.setNull(12, Types.VARCHAR);
            }

            if (empresas.getNumeroLicenca() != null) {
                preparedStatement.setString(13, empresas.getNumeroLicenca());
            } else {
                preparedStatement.setNull(13, Types.VARCHAR);
            }

            if (empresas.getBoshServiceURL() != null) {
                preparedStatement.setString(14, empresas.getBoshServiceURL());
            } else {
                preparedStatement.setNull(14, Types.VARCHAR);
            }

            if (empresas.getInterjourneyCompensationLimit() != null) {
                preparedStatement.setLong(15, empresas.getInterjourneyCompensationLimit());
            } else {
                preparedStatement.setNull(15, Types.BIGINT);
            }

            if (empresas.getInterjourneyCompensateStandby() != null) {
                preparedStatement.setBoolean(16, empresas.getInterjourneyCompensateStandby());
            } else {
                preparedStatement.setNull(16, Types.BOOLEAN);
            }

            if (empresas.getWorkedDaysDSR() != null) {
                preparedStatement.setLong(17, empresas.getWorkedDaysDSR());
            } else {
                preparedStatement.setNull(17, Types.BIGINT);
            }

            if (empresas.getMissingDaysDSR() != null) {
                preparedStatement.setLong(18, empresas.getMissingDaysDSR());
            } else {
                preparedStatement.setNull(18, Types.BIGINT);
            }

            if (empresas.getConsidersInterjourney() != null) {
                preparedStatement.setBoolean(19, empresas.getConsidersInterjourney());
            } else {
                preparedStatement.setNull(19, Types.BOOLEAN);
            }

            if (empresas.getHalfInterjourney() != null) {
                preparedStatement.setBoolean(20, empresas.getHalfInterjourney());
            } else {
                preparedStatement.setNull(20, Types.BOOLEAN);
            }

            if (empresas.getSascarintegration() != null) {
                preparedStatement.setBoolean(21, empresas.getSascarintegration());
            } else {
                preparedStatement.setNull(21, Types.BOOLEAN);
            }

            if (empresas.getFrequencyreading() != null) {
                preparedStatement.setInt(22, empresas.getFrequencyreading());
            } else {
                preparedStatement.setNull(22, Types.INTEGER);
            }

            if (empresas.getHabilitarViam() != null) {
                preparedStatement.setBoolean(23, empresas.getHabilitarViam());
            } else {
                preparedStatement.setNull(23, Types.BOOLEAN);
            }

            if (empresas.getHabilitarViaSat() != null) {
                preparedStatement.setBoolean(24, empresas.getHabilitarViaSat());
            } else {
                preparedStatement.setNull(24, Types.BOOLEAN);
            }

            if (empresas.getIdClienteTrajetto() != null) {
                preparedStatement.setInt(25, empresas.getIdClienteTrajetto());
            } else {
                preparedStatement.setNull(25, Types.INTEGER);
            }

            preparedStatement.setInt(26, idParametro);
            preparedStatement.executeUpdate();
            connection.commit();

        }catch (Exception e){
            LogSistema.logError("inserirEmpresaNativo", e);
        }finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    LogSistema.logError("inserirEmpresaNativo", e);
                }
            }

            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LogSistema.logError("inserirEmpresaNativo", e);
                }
            }
        }
    }


    public Double buscarValorDiaria(Integer idMotorista){
        try {
            String sql = "select p.valor_diaria from motoristas m " +
                    " inner join empresas e on e.id = m.empresa_id  " +
                    " inner join parametro p on p.id = e.parametro " +
                    " where m.id = :idMotorista";

            Query query = entityManager.createNativeQuery(sql)
                    .setParameter("idMotorista",idMotorista);
            List result = query.getResultList();
            if(result.size() == 1){
                return (Double)result.get(0);
            }
        }catch (Exception e){
            LogSistema.logError("buscarValorDiaria", e);
        }
        return 0d;
    }

    public List<Empresas> buscarBaseOrion(){
        List<Empresas> listaEmpresas = new ArrayList<>();
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select id, nome from cliente c where status = true and mostrar_rodovia = true;";
            connection = DataSourcePostgresOrion.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Integer id = resultSet.getInt("id");
                String nome = resultSet.getString("nome");
                Empresas empresas = new Empresas();
                empresas.setId(id);
                empresas.setNome(nome);
                empresas.setTipoSistema("ORION");
                listaEmpresas.add(empresas);
            }
        }catch (Exception e){
            LogSistema.logError("buscarBaseOrion", e);
        }finally {
            try {
                if(resultSet != null){
                    resultSet.close();
                }
                if(connection != null){
                    connection.close();
                }
            }catch (Exception e){

            }
        }
        return listaEmpresas;
    }

}