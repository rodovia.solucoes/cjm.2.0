package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.JourneyCalculation;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @author William Leite
 */
public class JourneyCalculationDAO extends GenericDAO<Integer, JourneyCalculation> {

    public JourneyCalculationDAO(EntityManager entityManager) {
        super(entityManager);
    }

    @SuppressWarnings("unchecked")
    public JourneyCalculation findByStartDateAndEndDateAndDriverId(Date start, Date end, Integer driver) {
        String hql = "SELECT j FROM JourneyCalculation j WHERE j.startDate = :startDate AND j.endDate = :endDate AND j.driver.id = :driver";
        Query query = entityManager.createQuery(hql, JourneyCalculation.class).setParameter("startDate", start).setParameter("endDate", end).setParameter("driver", driver);
        JourneyCalculation result = null;

        List<JourneyCalculation> tempList = query.getResultList();
        if (Objects.nonNull(tempList) && !tempList.isEmpty()) {
            result = tempList.get(0);
        }

        return result;
    }
}
