package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.RotaVeiculo;
import br.com.cmctransportes.cjm.domain.entities.vo.CoordenadaVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RotaVO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgresOrion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RotaVeiculoDAO {


    public List<RotaVeiculo> buscarRotasPorVeiculo(Integer idVeiculo){
        List<RotaVeiculo> lista = new ArrayList<>();
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try{
            String sql = "select rota, atual from rota_veiculo where veiculo = ? ;";
            connection = DataSourcePostgresOrion.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idVeiculo);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Integer idRota = resultSet.getInt("rota");
                Boolean atual = resultSet.getBoolean("atual");
                RotaVeiculo rotaVeiculo = new RotaVeiculo();
                rotaVeiculo.setAtual(atual);
                rotaVeiculo.setRota(idRota);
                lista.add(rotaVeiculo);
            }
        }catch (Exception e){
            LogSistema.logError("buscarRotasPorVeiculo", e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarLocais", ex);
            }
        }

        return lista;
    }

    public void salvar(RotaVeiculo rotaVeiculo){
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "INSERT INTO public.rota_veiculo (id, veiculo, rota, atual) VALUES(nextval('seq_rota_veiculo'), ?, ?, false);";
            connection = DataSourcePostgresOrion.getInstance().getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, rotaVeiculo.getVeiculo());
            preparedStatement.setInt(2, rotaVeiculo.getRota());
            preparedStatement.executeUpdate();
            connection.commit();
        }catch (Exception e){
            LogSistema.logError("salvar", e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarLocais", ex);
            }
        }
    }

    public void excluirRotaVeiculo(Integer idRota, Integer idVeiculo){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "delete from rota_veiculo where rota = ? and veiculo = ?";
            connection = DataSourcePostgresOrion.getInstance().getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idRota);
            preparedStatement.setInt(2, idVeiculo);
            preparedStatement.executeUpdate();
            connection.commit();
        }catch (Exception e){
            LogSistema.logError("excluirRotaVeiculo", e);
        } finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("excluirRotaVeiculo", ex);
            }
        }
    }

    public List<RotaVO> buscarListaDeRotas(Integer idVeiculo){
        Map<Integer, RotaVO> map = new HashMap<>();
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try{
            String sql = "select r.id as idrota, nome, latitude, longitude, sequencia from rota_veiculo rv " +
                    " inner join rota r on r.id = rv.rota " +
                    " inner join coordenada c2 on r.id = c2.rota " +
                    " where rv.veiculo = ? " ;
            connection = DataSourcePostgresOrion.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idVeiculo);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Integer idRota = resultSet.getInt("idrota");
                String nome = resultSet.getString("nome");
                Double latitude = resultSet.getDouble("latitude");
                Double longitude = resultSet.getDouble("longitude");
                Integer sequencia = resultSet.getInt("sequencia");

                if(map.containsKey(idRota)){
                    CoordenadaVO coordenadaVO = new CoordenadaVO();
                    coordenadaVO.setLatitude(latitude);
                    coordenadaVO.setLongitude(longitude);
                    coordenadaVO.setSequencia(sequencia);
                    map.get(idRota).getListaDeCoordenadas().add(coordenadaVO);
                }else{
                    RotaVO rotaVO = new RotaVO();
                    rotaVO.setId(idRota);
                    rotaVO.setNome(nome);
                    List<CoordenadaVO> voList = new ArrayList<>();
                    CoordenadaVO coordenadaVO = new CoordenadaVO();
                    coordenadaVO.setLatitude(latitude);
                    coordenadaVO.setLongitude(longitude);
                    coordenadaVO.setSequencia(sequencia);
                    voList.add(coordenadaVO);
                    rotaVO.setListaDeCoordenadas(voList);
                    map.put(idRota, rotaVO);
                }
            }
        }catch (Exception e){
            LogSistema.logError("buscarRotasPorVeiculo", e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarLocais", ex);
            }
        }
        List<RotaVO> lista = new ArrayList<>();
        lista.addAll(map.values());
        return lista;
    }

}
