/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.Enderecos;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgres;

import javax.persistence.EntityManager;
import java.sql.*;

/**
 * @author analista.ti
 */
public class EnderecosDAO extends GenericDAO<Integer, Enderecos> {




    public EnderecosDAO(EntityManager entityManager) {
        super(entityManager);
    }

    public int gravarEnderecoNativo(Enderecos enderecos){
        int retorno = 0;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try{
            String sql = "INSERT INTO public.enderecos( " +
                    " id, logradouro, numero, bairro, cep, cidade, complemento, empresa_id) " +
                    " VALUES (nextval('enderecos_id_seq'), ?, ?, ?, ?, ?, ?, ?) ";

            connection =DataSourcePostgres.getInstance().getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            if(enderecos.getLogradouro() != null){
                preparedStatement.setString(1, enderecos.getLogradouro());
            }else{
                preparedStatement.setString(1,"Não informado");
            }

            if(enderecos.getNumero() != null){
                preparedStatement.setString(2, enderecos.getNumero());
            }else{
                preparedStatement.setString(2,"0");
            }

            if(enderecos.getBairro() != null){
                preparedStatement.setString(3, enderecos.getBairro());
            }else{
                preparedStatement.setString(3,"Não informado");
            }

            if(enderecos.getCep() != null){
                preparedStatement.setString(4, enderecos.getCep());
            }else{
                preparedStatement.setString(4,"00000-000");
            }

            if(enderecos.getCidade() != null){
                preparedStatement.setString(5, enderecos.getCidade().getCodIbge());
            }else{
                preparedStatement.setString(5,"3118601");
            }

            if(enderecos.getComplemento() != null){
                preparedStatement.setString(6, enderecos.getComplemento());
            }else{
                preparedStatement.setString(6,"-");
            }

            if(enderecos.getEmpresaId() != null){
                preparedStatement.setInt(7, enderecos.getEmpresaId());
            }else{
                preparedStatement.setInt(7,1);
            }



            preparedStatement.executeUpdate();
            connection.commit();
            try (ResultSet rs = preparedStatement.getGeneratedKeys()) {
                if (rs.next()) {
                    retorno = rs.getInt(1);
                }
            }

        }catch (Exception e){
            LogSistema.logError("gravarEnderecoNativo", e);
        }finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    LogSistema.logError("gravarEnderecoNativo", e);
                }
            }

            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LogSistema.logError("gravarEnderecoNativo", e);
                }
            }
        }
        return retorno;
    }

}
