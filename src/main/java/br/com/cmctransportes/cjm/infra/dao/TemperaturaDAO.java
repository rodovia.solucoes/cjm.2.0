package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.vo.TemperaturaVO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.singleton.MongoDBConexao;
import com.mongodb.BasicDBObject;
import com.mongodb.QueryBuilder;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TemperaturaDAO {

    public List<TemperaturaVO> buscarLista(Integer idVeiculo){
        List<TemperaturaVO> lista = new ArrayList<>();
        try {
            MongoDatabase mdb = MongoDBConexao.getMongoDBConexao().pegarBaseRodovia();
            MongoCollection<Document> coll = mdb.getCollection("temperatura");

            QueryBuilder qb = new QueryBuilder();
            qb.and(new QueryBuilder().put("idVeiculo").is(idVeiculo).get());

            BasicDBObject basicDBObject = new BasicDBObject();
            basicDBObject.putAll(qb.get());
            MongoCursor<Document> cursor = coll.find(basicDBObject).sort(new Document("_id", -1)).limit(10).iterator();
            while (cursor.hasNext()) {
                Document dd = cursor.next();
                TemperaturaVO telemetriaVO = convert(dd);
                lista.add(telemetriaVO);
            }
        }catch (Exception e){
            LogSistema.logError("buscarUltimasTelemetria", e);
        }
        return lista;
    }

    private TemperaturaVO convert(Document document){
        TemperaturaVO temperaturaVO = new TemperaturaVO();
        try{
            if (document.containsKey("dataMedicao")) {
                Date d = document.getDate("dataMedicao");
                temperaturaVO.setDataMedicao(new SimpleDateFormat("dd/MM HH:mm:ss").format(d));
            }

            if (document.containsKey("temperatura")) {
                Double t = document.getDouble("temperatura");
                t = t /10;
                temperaturaVO.setTemperatura(String.format("%.2f", t));
            }

        }catch (Exception e){
            LogSistema.logError("convert", e);
        }
        return temperaturaVO;
    }
}

