/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.EstadosJornada;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 * @author analista.ti
 */
public class EstadosJornadaDAO extends GenericDAO<Integer, EstadosJornada> {
    public EstadosJornadaDAO(EntityManager entityManager) {
        super(entityManager);
    }

    public List<EstadosJornada> findByName(String nome) {
        List<EstadosJornada> result = null;

        try {
            String sql = "FROM EstadosJornada WHERE descricao LIKE :descricao";
            Query query = entityManager.createQuery(sql).setParameter("descricao", "%" + nome + "%");
            result = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return result;
    }
}
