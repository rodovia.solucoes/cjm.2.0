/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.Motoristas;
import br.com.cmctransportes.cjm.domain.entities.Rastro;
import br.com.cmctransportes.cjm.domain.entities.vo.*;
import br.com.cmctransportes.cjm.sascarsoap.Motorista;
import br.com.cmctransportes.cjm.utils.TimeHelper;
import org.joda.time.DateTime;
import org.joda.time.Hours;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author analista.ti
 */
public class RastroDAO extends GenericDAO<Integer, Rastro> {

    private EntityManager em;

    public RastroDAO(EntityManager entityManager) {
        super(entityManager);
        this.em = entityManager;
    }

    public List<Rastro> getRelatorioDePosicoes(Motoristas mot, Date dataInicio, Date dataFim) {
        List<Rastro> result = null;

        try {
//            String sql = 
//                "select " +
//                    "rastro.* " +
//                "from " +
//                    "rastro " +
//                "cross join( " +
//                    "select " +
//                        "min(instante_evento) as inicio, " +
//                        "CASE WHEN min(instante_evento) = max(instante_evento) THEN :dataInicio " +
//                        "ELSE max(instante_evento) " +
//                        "END as fim " +
//                    "from " +
//                        "evento " +
//                    "where " +
//                        "operador_lancamento = :idUsuarioMotorista " +
//                        "and instante_evento between :dataInicio and :dataFim " +
//                        "and (tipo_evento = 0 or tipo_evento = 1) " + // TODO: deixar isso aqui menos dependente
//                ") jornada " +
//                "where " +
//                    "motorista_id = :idMotorista " +
//                    "and instante_marcacao between jornada.inicio and jornada.fim;";

            StringBuilder sql = new StringBuilder();
            sql.append(" SELECT * ");
            sql.append(" FROM   rastro ");
            sql.append(" WHERE  instante_marcacao BETWEEN :dataInicio AND :dataFim ");
            sql.append("        AND motorista_id = :idMotorista ORDER BY instante_marcacao ASC");

            Query query = em.createNativeQuery(sql.toString(), Rastro.class)
                    //.setParameter("idUsuarioMotorista", mot.getUserId())
                    .setParameter("idMotorista", mot.getId())
                    .setParameter("dataInicio", dataInicio)
                    .setParameter("dataFim", dataFim);

            result = query.getResultList();
        } catch (NoResultException nre) {
            nre.printStackTrace();
        } catch (Exception except) {
            except.printStackTrace();
        }
        return result;
    }

    public void ajustarRastroParaTransmissao(EmpresaVO empresaVO, RetornoMapaVO retornoMapaVO) throws Exception {
        try {
            String sql = "select m.id, nome, " +
                    "(select concat(id,'Y',latitude,'Y',longitude,'Y',instante_envio,'Y',bairro,'Y',cidade,'Y',estado,'Y',endereco) from rastro where id = " +
                    "(select max(id) from rastro where motorista_id = m.id and instante_envio between :dataInicio and :dataFinal)), " +
                    " (select texto from observacao where id = (select max(id) from observacao where viam = m.id)) as observacao " +
                    "from motoristas m where m.empresa_id = :idEmpresa and situacao = 1 ";
            Query query = em.createNativeQuery(sql.toString())
                    .setParameter("idEmpresa", empresaVO.getId())
                    .setParameter("dataInicio", TimeHelper.getDate000000())
                    .setParameter("dataFinal", new Date());

            List result = query.getResultList();
            for (int i = 0; i < result.size(); i++) {
                MotoristaMinimoVO motoristaVO = new MotoristaMinimoVO();
                TransmissaoMinimoVO transmissaoMinimo = new TransmissaoMinimoVO();
                Object[] oo = (Object[]) result.get(i);
                motoristaVO.setId((Integer) oo[0]);
                motoristaVO.setNome((String) oo[1]);
                if (oo[2] != null) {
                    String[] ts = ((String) oo[2]).split("Y");
                    transmissaoMinimo.setLatitude(Double.valueOf(ts[1]));
                    transmissaoMinimo.setLongitude(Double.valueOf(ts[2]));
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String dataS = String.valueOf(ts[3]);
                    Date dataTransmissao = simpleDateFormat.parse(dataS);
                    transmissaoMinimo.setDataTransmissao(
                            new SimpleDateFormat("dd/MM HH:mm:ss").format(dataTransmissao));
                    DateTime dateTimeInicial = new DateTime(dataTransmissao);
                    DateTime dateTimeFinal = new DateTime(new Date());

                    // Verificar se data da trasmissao é menor que a atual 60 minutos

                    int horaTransmissao = Hours.hoursBetween(new DateTime(dataTransmissao), dateTimeFinal)
                            .getHours();
                    if (horaTransmissao > 1) {
                        transmissaoMinimo.setIgnicaoAtiva(false);
                    } else {
                        transmissaoMinimo.setIgnicaoAtiva(true);
                    }
                    int horas = Hours.hoursBetween(dateTimeInicial, dateTimeFinal).getHours();

                    transmissaoMinimo.setMaisDe24HorasInativo(horas > 24);
                    if (ts.length > 4) {
                        transmissaoMinimo.setBairro(ts[4] == null ? "-" : ts[4]);
                        transmissaoMinimo.setCidade(ts[5] == null ? "-" : ts[5]);
                        transmissaoMinimo.setUf(ts[6] == null ? "-" : ts[6]);
                        transmissaoMinimo.setEndereco(ts[7] == null ? "-" : ts[7]);
                    } else {
                        transmissaoMinimo.setBairro("-");
                        transmissaoMinimo.setCidade("-");
                        transmissaoMinimo.setUf("-");
                        transmissaoMinimo.setEndereco("-");
                    }
                } else {
                    transmissaoMinimo.setMaisDe24HorasInativo(true);
                    transmissaoMinimo.setBairro("-");
                    transmissaoMinimo.setCidade("-");
                    transmissaoMinimo.setUf("-");
                    transmissaoMinimo.setEndereco("-");
                    transmissaoMinimo.setDataTransmissao("Não encontrado");
                }

                String observacao = String.valueOf(oo[3]);
                if (observacao != null) {
                    transmissaoMinimo.setObservacao("-");
                } else {
                    transmissaoMinimo.setObservacao(observacao);
                }
                transmissaoMinimo.setVelocidade("0 Km/h");
                VeiculoTransmissaoVO veiculoTransmissaoVO = new VeiculoTransmissaoVO();
                veiculoTransmissaoVO.setTransmissao(transmissaoMinimo);
                veiculoTransmissaoVO.setMotorista(motoristaVO);
                retornoMapaVO.getListaVeiculoTransmissao().add(veiculoTransmissaoVO);
            }
        } catch (Exception e) {
            throw e;
        }
    }


    public Rastro buscarUltimoRastro(MotoristaVO motorista) throws Exception {
        try {

            if(existeDadosNaRastro(motorista)) {
                String sql = "select endereco, bairro, cidade, estado  from rastro where id = (select max(id) from rastro where motorista_id = :motoristaId);";
                Query query = entityManager.createNativeQuery(sql)
                        .setParameter("motoristaId", motorista.getId());
                List lista = query.getResultList();

                if (!lista.isEmpty()) {
                    Object[] oo = (Object[])lista.get(0);
                    Rastro rastro = new Rastro();
                    rastro.setEndereco(buscarDados(oo, 0));
                    rastro.setBairro(buscarDados(oo, 1));
                    rastro.setCidade(buscarDados(oo, 2));
                    rastro.setUf(buscarDados(oo, 3));
                    return rastro;
                }
            }
            return null;
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * Metodo para verificar se existe algum registro na Rastro, senao deixa a pesquisa muito lenta
     * @param motorista
     * @return
     */
    private boolean existeDadosNaRastro(MotoristaVO motorista){
        boolean retorno = false;
        try {
            String sql = "select count(id) from rastro where motorista_id = :motoristaId";
            Query query = entityManager.createNativeQuery(sql)
                    .setParameter("motoristaId", motorista.getId());
            List lista = query.getResultList();
            if (!lista.isEmpty()) {
                return ((BigInteger) lista.get(0)).intValue() > 0;
            }

        } catch (Exception e) {
            throw e;
        }
        return retorno;
    }

    private String buscarDados(Object[] oo, int index){
        try{
            return (String)oo[index];
        }catch (Exception e){}
        return null;
    }
}
