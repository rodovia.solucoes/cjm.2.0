package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.TiposDeVeiculo;

import javax.persistence.EntityManager;

/**
 * @author analista.ti
 */
public class TiposDeVeiculoDAO extends GenericDAO<Integer, TiposDeVeiculo> {
    public TiposDeVeiculoDAO(EntityManager entityManager) {
        super(entityManager);
    }
}
