/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.Veiculos;
import br.com.cmctransportes.cjm.domain.entities.vo.LocalVO;
import br.com.cmctransportes.cjm.domain.entities.vo.VeiculoVO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.proxy.modelo.Modelo;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgres;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgresOrion;

import javax.persistence.EntityManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author analista.ti
 */
public class VeiculosDAO extends GenericDAO<String, Veiculos> {
    public VeiculosDAO(EntityManager entityManager) {
        super(entityManager);
    }



    public Veiculos buscarVeiculoPelaPlaca(String placa){
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select id from veiculo where placa = ?";
            connection = DataSourcePostgresOrion.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, placa);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Veiculos veiculos = new Veiculos();
                veiculos.setId(resultSet.getInt("id"));
                return veiculos;
            }
        }catch (Exception e){
            LogSistema.logError("buscarVeiculoPelaPlaca", e);
        }finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarVeiculoPelaPlaca", ex);
            }

        }
        return null;
    }

    public Veiculos buscarVeiculoPeloId(Integer idVeiculo){
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select rpm_maximo, rpm_inicio_faixa_azul, " +
                    " rpm_fim_faixa_azul, " +
                    " rpm_inicio_faixa_economica, " +
                    " rpm_fim_faixa_economica, " +
                    " rpm_inicio_faixa_verde, " +
                    " rpm_fim_faixa_verde," +
                    " rpm_inicio_faixa_amarela, " +
                    " rpm_fim_faixa_amarela, " +
                    " rpm_inicio_marcha_lenta, "+
                    " rpm_fim_marcha_lenta, "+
                    " placa"+
                    " from veiculo where id = ?";

            connection = DataSourcePostgresOrion.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idVeiculo);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Veiculos veiculos = new Veiculos();
                veiculos.setRpmMaximo(resultSet.getInt("rpm_maximo"));
                veiculos.setRpmInicioFaixaAzul(resultSet.getInt("rpm_inicio_faixa_azul"));
                veiculos.setRpmFimFaixaAzul(resultSet.getInt("rpm_fim_faixa_azul"));
                veiculos.setRpmInicioFaixaEconomica(resultSet.getInt("rpm_inicio_faixa_economica"));
                veiculos.setRpmFimFaixaEconomica(resultSet.getInt("rpm_fim_faixa_economica"));
                veiculos.setRpmInicioFaixaVerde(resultSet.getInt("rpm_inicio_faixa_verde"));
                veiculos.setRpmFimFaixaVerde(resultSet.getInt("rpm_fim_faixa_verde"));
                veiculos.setRpmInicioFaixaAmarela(resultSet.getInt("rpm_inicio_faixa_amarela"));
                veiculos.setRpmFimFaixaAmarela(resultSet.getInt("rpm_fim_faixa_amarela"));
                veiculos.setRpmInicioMarchaLenta(resultSet.getInt("rpm_inicio_marcha_lenta"));
                veiculos.setRpmFimMarchaLenta(resultSet.getInt("rpm_fim_marcha_lenta"));
                veiculos.setPlaca(resultSet.getString("placa"));
                return veiculos;
            }
        }catch (Exception e){
            LogSistema.logError("buscarVeiculoPelaPlaca", e);
        }finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarVeiculoPelaPlaca", ex);
            }

        }
        return null;
    }


    public Veiculos buscarVeiculoModeloPeloId(Integer idVeiculo){
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select v.id as ida, placa, m.id as idb, v.rpm_inicio_marcha_lenta, v.rpm_fim_marcha_lenta  " +
                    " from veiculo v " +
                    " inner join equipamento e on e.id = v.equipamento " +
                    " inner join modelo m on m.id = e.modelo " +
                    " where v.id = ?";

            connection = DataSourcePostgresOrion.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idVeiculo);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Veiculos veiculos = new Veiculos();
                veiculos.setId(resultSet.getInt("ida"));
                veiculos.setPlaca(resultSet.getString("placa"));
                veiculos.setIdModeloEquipamento(resultSet.getInt("idb"));
                Integer rpmLentaMin = resultSet.getInt("rpm_inicio_marcha_lenta");
                veiculos.setRpmInicioMarchaLenta(Objects.nonNull(rpmLentaMin)?rpmLentaMin:0);
                Integer rpmLentaMax = resultSet.getInt("rpm_fim_marcha_lenta");
                veiculos.setRpmFimMarchaLenta(Objects.nonNull(rpmLentaMax)?rpmLentaMax:0);
                return veiculos;
            }
        }catch (Exception e){
            LogSistema.logError("buscarVeiculoPelaPlaca", e);
        }finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarVeiculoPelaPlaca", ex);
            }

        }
        return null;
    }


    public Veiculos buscarVeiculoPeloMotorista(Integer idMotorista){
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {

            Integer idVeiculo = buscarIdVeiculoPeloMotorista(idMotorista);
            if(Objects.nonNull(idVeiculo) && !idVeiculo.equals(0)) {

                String sql = "select v.id as ida, placa, m.id as idb, v.rpm_inicio_marcha_lenta, v.rpm_fim_marcha_lenta  " +
                        " from veiculo v " +
                        " inner join equipamento e on e.id = v.equipamento " +
                        " inner join modelo m on m.id = e.modelo " +
                        " where v.id = ?";

                connection = DataSourcePostgresOrion.getInstance().getConnection();
                preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setInt(1, idVeiculo);
                resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    Veiculos veiculos = new Veiculos();
                    veiculos.setId(resultSet.getInt("ida"));
                    veiculos.setPlaca(resultSet.getString("placa"));
                    veiculos.setIdModeloEquipamento(resultSet.getInt("idb"));
                    Integer rpmLentaMin = resultSet.getInt("rpm_inicio_marcha_lenta");
                    veiculos.setRpmInicioMarchaLenta(Objects.nonNull(rpmLentaMin) ? rpmLentaMin : 0);
                    Integer rpmLentaMax = resultSet.getInt("rpm_fim_marcha_lenta");
                    veiculos.setRpmFimMarchaLenta(Objects.nonNull(rpmLentaMax) ? rpmLentaMax : 0);
                    return veiculos;
                }
            }
        }catch (Exception e){
            LogSistema.logError("buscarVeiculoPeloMotorista", e);
        }finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarVeiculoPeloMotorista", ex);
            }

        }
        return null;
    }


    private Integer buscarIdVeiculoPeloMotorista(Integer idMotorista){
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select veiculo_orion from motoristas_veiculos " +
                    " where motoristas_id = ? " +
                    " and ativo " +
                    " order by id desc limit 1;";

            connection = DataSourcePostgres.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idMotorista);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                Integer id = resultSet.getInt("veiculo_orion");
                return id;
            }
        }catch (Exception e){
            LogSistema.logError("buscarIdVeiculoPeloMotorista", e);
        }finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarIdVeiculoPeloMotorista", ex);
            }
        }
        return 0;
    }



    public List<VeiculoVO> buscarVeiculosParaOViaM(Integer idCliente, Integer idUnidade){
        List<VeiculoVO> list = new ArrayList<>();
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select v.id as idveiculo, v.placa, " +
                    " (select hodometro from hodometro h2 where veiculo = v.id order by id desc limit 1) as hodometro " +
                    " from cliente_veiculo cv " +
                    " inner join veiculo v on cv.veiculo = v.id " +
                    " where cv.cliente = ? " +
                    " and v.id_unidade = ? " +
                    " and v.mostrar_viam = true "+
                    " and v.status = true ";

            connection = DataSourcePostgresOrion.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idCliente);
            preparedStatement.setInt(2, idUnidade);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Integer id = resultSet.getInt("idveiculo");
                String placa = resultSet.getString("placa");
                Double hodometro = resultSet.getDouble("hodometro");
                VeiculoVO vo = new VeiculoVO();
                vo.setId(id);
                vo.setPlaca(placa);
                vo.setUltimoHodometro(hodometro);
                list.add(vo);
            }
        }catch (Exception e){
            LogSistema.logError("buscarIdVeiculoPeloMotorista", e);
        }finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarIdVeiculoPeloMotorista", ex);
            }
        }
        return list;
    }

    public List<VeiculoVO> buscarVeiculos(Integer idCliente, Integer idUnidade){
        List<VeiculoVO> list = new ArrayList<>();
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select v.id as idveiculo, v.placa " +
                    " from cliente_veiculo cv " +
                    " inner join veiculo v on cv.veiculo = v.id " +
                    " where cv.cliente = ? " +
                    " and v.id_unidade = ? " +
                    " and v.status = true ";

            connection = DataSourcePostgresOrion.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idCliente);
            preparedStatement.setInt(2, idUnidade);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Integer id = resultSet.getInt("idveiculo");
                String placa = resultSet.getString("placa");
                VeiculoVO vo = new VeiculoVO();
                vo.setId(id);
                vo.setPlaca(placa);
                list.add(vo);
            }
        }catch (Exception e){
            LogSistema.logError("buscarVeiculos", e);
        }finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarIdVeiculoPeloMotorista", ex);
            }
        }
        return list;
    }
}
