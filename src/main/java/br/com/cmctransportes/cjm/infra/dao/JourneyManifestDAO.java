package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.JourneyManifest;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 * @author William Leite
 */
public class JourneyManifestDAO extends GenericDAO<Long, JourneyManifest> {

    public JourneyManifestDAO(EntityManager entityManager) {
        super(entityManager);
    }

    @SuppressWarnings("unchecked")
    public List<JourneyManifest> findByJourney(Integer id) {
        String hql = "SELECT j FROM JourneyManifest j WHERE j.journey.id = :id";
        Query query = entityManager.createQuery(hql, JourneyManifest.class).setParameter("id", id);

        return query.getResultList();
    }
}