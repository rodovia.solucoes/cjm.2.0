package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.Users;
import br.com.cmctransportes.cjm.domain.entities.vo.ClienteMobileVO;
import br.com.cmctransportes.cjm.domain.entities.vo.UsuarioMobileVO;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;

/**
 * @author analista.ti
 */
public class UsersDAO extends GenericDAO<Integer, Users> {

    private EntityManager em;

    public UsersDAO(EntityManager entityManager) {
        super(entityManager);
        this.em = entityManager;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Users> findAll(Integer idEmpresa, Integer idUnidade) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT DISTINCT t FROM Users t");
        sb.append(" LEFT JOIN t.usersCompany uc LEFT JOIN t.usersBranch ub WHERE uc.company.id = :idEmpresa");
        if (idUnidade > 0) {
            sb.append(" AND ub.branch.id = :idUnidade");
        }

        Query q = entityManager.createQuery(sb.toString());
        q.setParameter("idEmpresa", idEmpresa);
        if (idUnidade > 0) {
            q.setParameter("idUnidade", idUnidade);
        }
        List<Users> result = q.getResultList();
        return result;
    }

    public Users getByUserName(String e) {
        try {
            Query q = em.createNamedQuery("Users.findByUserName").setParameter("userName", e);
            return (Users) q.getSingleResult();
        } catch (NoResultException nre) {
            // do nothing
        } catch (Exception except) {
            except.printStackTrace();
        }
        return null;
    }

    public void removeBranch(Integer user, Integer branch) throws Exception {
        String sql = "DELETE FROM users_branch WHERE user_id = :user AND branch_id = :branch";
        Query q = em.createNativeQuery(sql).setParameter("user", user).setParameter("branch", branch);
        q.executeUpdate();
    }

    public void removeCompany(Integer user, Integer company) throws Exception {
        String sql = "DELETE FROM users_company WHERE user_id = :user AND company_id = :company";
        Query q = em.createNativeQuery(sql).setParameter("user", user).setParameter("company", company);
        q.executeUpdate();
    }

    public void removeClaims(Integer id) throws Exception {
        String sql = "DELETE FROM users_claims WHERE id = :id";
        Query q = em.createNativeQuery(sql).setParameter("id", id);
        q.executeUpdate();
    }

    public void buscarUsuarioMobile(UsuarioMobileVO usuarioMobileVO) throws Exception {

        String sql = " select u.id,uc.company_id, uc2.claim,u.token_mobile, u.tipo_sistema_mobile  from users u " +
                " inner join users_company uc on uc.user_id = u.id " +
                " inner join users_claims uc2 on uc2.user_id = u.id " +
                " where u.user_name = :usuario" +
                " and u.senha = :senha " +
                " and u.situacao = true " +
                " and uc2.claim = 'frota'";

        Query q = entityManager.createNativeQuery(sql);
        q.setParameter("usuario", usuarioMobileVO.getNomeUsuario());
        q.setParameter("senha", usuarioMobileVO.getSenha());

        List<Object> result = q.getResultList();
        for (Object oo : result) {
            Object[] o = (Object[]) oo;
            usuarioMobileVO.setId((Integer) o[0]);
            ClienteMobileVO clienteMobileVO = new ClienteMobileVO();
            clienteMobileVO.setId((Integer) o[1]);
            usuarioMobileVO.setClientePai(clienteMobileVO);
            usuarioMobileVO.setTokenMobile((String) o[3]);
            usuarioMobileVO.setTipoSistemaMobile((String) o[4]);
            usuarioMobileVO.setUsuarioLogado(Boolean.TRUE);
        }
    }
}
