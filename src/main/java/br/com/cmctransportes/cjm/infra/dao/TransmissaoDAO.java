package br.com.cmctransportes.cjm.infra.dao;


import br.com.cmctransportes.cjm.domain.entities.Transmissao;
import br.com.cmctransportes.cjm.domain.entities.vo.LocalVO;
import br.com.cmctransportes.cjm.domain.entities.vo.TimelineVO;
import br.com.cmctransportes.cjm.domain.entities.vo.TransmissaoServicoVO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgresOrion;
import br.com.cmctransportes.cjm.singleton.MongoDBConexao;
import br.com.cmctransportes.cjm.utils.TimeHelper;
import com.mongodb.BasicDBObject;
import com.mongodb.QueryBuilder;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;


public class TransmissaoDAO {

    public List<TimelineVO> buscarTrasmissaoDoDiaTimeLine(Integer idVeiculo) {
        List<TimelineVO> lista = new ArrayList<>();
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select data_transmissao, velocidade, entrada_digital_um, ignicao_ativa from transmissao t where veiculo = ? and data_transmissao between ? and ?;";
            connection = DataSourcePostgresOrion.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idVeiculo);
            preparedStatement.setTimestamp(2, new Timestamp(TimeHelper.getDate000000().getTime()));
            preparedStatement.setTimestamp(3, new Timestamp(new Date().getTime()));
            resultSet = preparedStatement.executeQuery();
            Map<Integer, LocalVO> map = new HashMap<>();
            int cont = 0;
            int velocidadeMaxima = buscarVelocidadeMaxima(idVeiculo);
            int ignicaoAtiva = 0;
            boolean entradaDigital = false;
            int contEntradaDigital = 0;
            while (resultSet.next()) {
                Date data = new Date(resultSet.getTimestamp("data_transmissao").getTime());
                int velocidade = resultSet.getInt("velocidade");
                boolean entradaDigitalIn = resultSet.getBoolean("entrada_digital_um");
                int ignicaoAtivaInt = resultSet.getInt("ignicao_ativa");

                //Calcula velocidade
                if((velocidade + 5) > velocidadeMaxima){
                    lista.add(gerar(data, "Excesso velocidade "+velocidade+" Km/h" ));
                }

                //Calcula status ignicao
                if(ignicaoAtivaInt != ignicaoAtiva){
                    ignicaoAtiva = ignicaoAtivaInt;
                    if(ignicaoAtivaInt == 0){
                        lista.add(gerar(data, "Ignição desligada"));
                    }else{
                        lista.add(gerar(data, "Ignição Ligada"));
                    }
                }

                if(entradaDigitalIn){
                    if(contEntradaDigital == 0){
                        lista.add(gerar(data, "Tomada de Força"));
                    }
                    contEntradaDigital++;
                }else{
                    contEntradaDigital = 0;
                }
            }
        } catch (Exception e) {
            LogSistema.logError("buscarLocais", e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarLocais", ex);
            }
        }
        return lista;
    }

    private TimelineVO gerar(Date data, String descricao) {
        TimelineVO vo = new TimelineVO();
        vo.setDescricao(descricao);
        vo.setData(new SimpleDateFormat("dd/MM HH:mm").format(data));
        vo.setDataDoEvento(data);
        return vo;
    }


    private int buscarVelocidadeMaxima(int idVeiculo) {
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        int velocidadeMaxima = 80;
        try {
            String sql = "select velocidade_maxima from veiculo where id = ?;";
            connection = DataSourcePostgresOrion.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idVeiculo);
            resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                velocidadeMaxima = resultSet.getInt("velocidade_maxima");
            }
        } catch (Exception e) {
            LogSistema.logError("buscarVelocidadeMaxima", e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarVelocidadeMaxima", ex);
            }
        }
        return velocidadeMaxima;
    }


    public List<Transmissao> buscarDadosParaFaturamento(Integer idVeiculo, Date dataInicial, Date dataFinal){
        List<Transmissao> lista = new ArrayList<>();
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try{
            String sql = "select id, data_transmissao, velocidade, ignicao_ativa from transmissao t " +
                    " where veiculo = ? and data_transmissao between ? and ? ";
            connection = DataSourcePostgresOrion.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idVeiculo);
            preparedStatement.setTimestamp(2, new Timestamp(dataInicial.getTime()));
            preparedStatement.setTimestamp(3, new Timestamp(dataFinal.getTime()));
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Integer id = resultSet.getInt("id");
                Date data = new Date(resultSet.getTimestamp("data_transmissao").getTime());
                Integer velocidade = resultSet.getInt("velocidade");
                Boolean ignicao =  resultSet.getInt("ignicao_ativa") == 1;
                Transmissao transmissao = new Transmissao();
                transmissao.setDataTransmissao(data);
                transmissao.setId(id);
                transmissao.setIgnicaoAtiva(ignicao);
                transmissao.setVelocidade(velocidade);
                lista.add(transmissao);
            }
        }catch (Exception e) {
            LogSistema.logError("buscarDadosParaFaturamento", e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarDadosParaFaturamento", ex);
            }
        }
        return lista;
    }


    public List<Transmissao> buscarTransmissaoMongo(Integer idVeiculo, Date dataInicial, Date dataFinal){
        List<Transmissao> l = new ArrayList<>();
        try {
            MongoDatabase mdb = MongoDBConexao.getMongoDBConexao().pegarBaseRodovia();
            MongoCollection<Document> coll = mdb.getCollection("transmissao");

            BasicDBObject basicDBObject = new BasicDBObject();
            basicDBObject.put("idVeiculo", idVeiculo);
            basicDBObject.put("dataTransmissao", new BasicDBObject("$gte", TimeHelper.getDate000000(dataInicial)).append("$lte", TimeHelper.getDate235959(dataFinal)));
            MongoCursor<Document> cursor = coll.find(basicDBObject).iterator();

            while (cursor.hasNext()) {
                Document dd = cursor.next();
                Transmissao t = convertInTransmissao(dd);
                l.add(t);
            }
        }catch (Exception e){
            LogSistema.logError("buscarTransmissaoMongo",e);
        }
        return l;
    }


    public Transmissao buscarUltimaTransmissaoMongo(Integer idVeiculo){
        List<Transmissao> l = new ArrayList<>();
        try {
            MongoDatabase mdb = MongoDBConexao.getMongoDBConexao().pegarBaseRodovia();
            MongoCollection<Document> coll = mdb.getCollection("transmissao");

            BasicDBObject basicDBObject = new BasicDBObject();
            basicDBObject.put("idVeiculo", idVeiculo);

            BasicDBObject basicDBObjectSort = new BasicDBObject();
            basicDBObjectSort.put("_id", -1);

            MongoCursor<Document> cursor = coll.find(basicDBObject).sort(basicDBObjectSort).limit(1).iterator();

            if (cursor.hasNext()) {
                Document dd = cursor.next();
                Transmissao t = convertInTransmissao(dd);
                return t;
            }
        }catch (Exception e){
            LogSistema.logError("buscarUltimaTransmissaoMongo",e);
        }
        return null;
    }

    public List<Transmissao> buscarUltimasTransmissaos(Integer idVeiculo){
        List<Transmissao> lista = new ArrayList<>();
        try {
            MongoDatabase mdb = MongoDBConexao.getMongoDBConexao().pegarBaseRodovia();
            MongoCollection<Document> coll = mdb.getCollection("transmissao");

            new QueryBuilder().put("idVeiculo").is(idVeiculo).get();
            QueryBuilder qb = new QueryBuilder();
            qb.and(new QueryBuilder().put("idVeiculo").is(idVeiculo).get());
            BasicDBObject query = new BasicDBObject("dataTransmissao", //
                    new BasicDBObject("$gte", TimeHelper.getDate000000(new Date())).append("$lt", new Date()));
            qb.and(query);

            BasicDBObject basicDBObject = new BasicDBObject();
            basicDBObject.putAll(qb.get());

            MongoCursor<Document> cursor = coll.find(basicDBObject).iterator();
            while (cursor.hasNext()) {
                Document dd = cursor.next();
                Transmissao t = convertInTransmissao(dd);
                lista.add(t);
            }
            Collections.sort(lista, new Comparator<Transmissao>() {
                public int compare(Transmissao o1, Transmissao o2) {
                    return o1.getDataTransmissao().compareTo(o2.getDataTransmissao());
                }
            });

        }catch (Exception e){
            LogSistema.logError("buscarUltimasTelemetria", e);
        }

        return lista;
    }


    public List<Transmissao> buscarUltimasTransmissaosIgnicaoAtivaVelociadeMaiorQueZero(Integer idVeiculo){
        List<Transmissao> lista = new ArrayList<>();
        try {
            MongoDatabase mdb = MongoDBConexao.getMongoDBConexao().pegarBaseRodovia();
            MongoCollection<Document> coll = mdb.getCollection("transmissao");

            new QueryBuilder().put("idVeiculo").is(idVeiculo).get();
            QueryBuilder qb = new QueryBuilder();
            qb.and(new QueryBuilder().put("idVeiculo").is(idVeiculo).get());
            qb.and(new QueryBuilder().put("ignicaoAtiva").is(Boolean.TRUE).get());
            BasicDBObject query = new BasicDBObject("dataTransmissao", //
                    new BasicDBObject("$gte", TimeHelper.diminuirHoras(new Date(), -5)).append("$lt", new Date()));
            qb.and(query);

            BasicDBObject queryVelocidade = new BasicDBObject("velocidade", //
                    new BasicDBObject("$gte", new Integer(40)));
            qb.and(queryVelocidade);

            BasicDBObject basicDBObject = new BasicDBObject();
            basicDBObject.putAll(qb.get());

            MongoCursor<Document> cursor = coll.find(basicDBObject).iterator();
            while (cursor.hasNext()) {
                Document dd = cursor.next();
                Transmissao t = convertInTransmissao(dd);
                lista.add(t);
            }
            Collections.sort(lista, new Comparator<Transmissao>() {
                public int compare(Transmissao o1, Transmissao o2) {
                    return o1.getDataTransmissao().compareTo(o2.getDataTransmissao());
                }
            });

        }catch (Exception e){
            LogSistema.logError("buscarUltimasTelemetria", e);
        }

        return lista;
    }


    private  Transmissao convertInTransmissao(Document document) throws Exception {
        try {
            Transmissao t = new Transmissao();
            if (document.containsKey("id")) {
                t.setId(document.getInteger("id"));
            }
            if (document.containsKey("dataTransmissao")) {
                t.setDataTransmissao(document.getDate("dataTransmissao"));
            }
            if (document.containsKey("latitude")) {
                t.setLatitude(document.getDouble("latitude"));
            }
            if (document.containsKey("longitude")) {
                t.setLongitude(document.getDouble("longitude"));
            }

            if (document.containsKey("velocidade")) {
                t.setVelocidade(document.getDouble("velocidade").intValue());
            }

            if (document.containsKey("ignicaoAtiva")) {
                t.setIgnicaoAtiva(document.getBoolean("ignicaoAtiva"));
            }

            return t;
        } catch (Exception e) {
            throw e;
        }
    }


    public List<Transmissao> buscarTrasmissaoParaRelatorio(Integer idVeiculo, Date dataIncio, Date dataFim) {
        List<Transmissao> lista = new ArrayList<>();
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select data_transmissao, velocidade from transmissao t where veiculo = ? " +
                    " and data_transmissao between ? and ?  " +
                    " and ignicao_ativa = 1 " +
                    " and velocidade > 0 " +
                    " order by data_transmissao";
            connection = DataSourcePostgresOrion.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idVeiculo);
            preparedStatement.setTimestamp(2, new Timestamp(TimeHelper.getDate000000(dataIncio).getTime()));
            preparedStatement.setTimestamp(3, new Timestamp(TimeHelper.getDate235959(dataFim).getTime()));
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Date data = new Date(resultSet.getTimestamp("data_transmissao").getTime());
                int velocidade = resultSet.getInt("velocidade");
                Transmissao t = new Transmissao();
                t.setVelocidade(velocidade);
                t.setDataTransmissao(data);
                lista.add(t);
            }
        } catch (Exception e) {
            LogSistema.logError("buscarTrasmissaoParaRelatorio", e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarTrasmissaoParaRelatorio", ex);
            }
        }
        return lista;
    }

    public Transmissao buscarTransmissaoBasica(Integer idVeiculo){
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try{
            String sql = "SELECT tb.id_transmissao_pai, data_transmissao, ignicao_ativa, endereco, latitude, longitude, velocidade, voltagem, cidade, estado " +
                    " FROM transmissao_basica tb  " +
                    " where tb.id = (select  max(id) from transmissao_basica where veiculo = ?) ";
            connection = DataSourcePostgresOrion.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idVeiculo);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                Integer id = resultSet.getInt("id_transmissao_pai");
                Date data = new Date(resultSet.getTimestamp("data_transmissao").getTime());
                Boolean ignicao =  resultSet.getInt("ignicao_ativa") == 1;
                String endereco = resultSet.getString("endereco");
                Double latitude = resultSet.getDouble("latitude");
                Double longitude = resultSet.getDouble("longitude");
                Double velocidade = resultSet.getDouble("velocidade");
                Double voltagem = resultSet.getDouble("voltagem");
                String cidade = resultSet.getString("cidade");
                String uf = resultSet.getString("estado");
                Transmissao transmissao = new Transmissao();
                transmissao.setDataTransmissao(data);
                transmissao.setId(id);
                transmissao.setIgnicaoAtiva(ignicao);
                transmissao.setEndereco(endereco);
                transmissao.setLatitude(latitude);
                transmissao.setLongitude(longitude);
                transmissao.setVelocidade(velocidade.intValue());
                transmissao.setVoltagem(voltagem);
                transmissao.setCidade(cidade);
                transmissao.setUf(uf);

                return transmissao;
            }
        }catch (Exception e) {
            LogSistema.logError("buscarTransmissaoBasica", e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarTransmissaoBasica", ex);
            }
        }
        return null;
    }


    /**
     * Usado para mandar para a tela latitude e longitude para gerar a rota que o veiculo passou
     * @param idVeiculo
     * @return
     */
    public List<Transmissao> buscarTrasmissaoParaMonitoramento(Integer idVeiculo) {
        List<Transmissao> lista = new ArrayList<>();
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select data_transmissao, latitude, longitude, velocidade from transmissao " +
                    "where veiculo = ? " +
                    "and data_transmissao between ? and ? " +
                    "and ignicao_ativa = 1  " +
                    "and velocidade > 0 order by data_transmissao ";
            connection = DataSourcePostgresOrion.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idVeiculo);
            preparedStatement.setTimestamp(2, new Timestamp(TimeHelper.getDate000000(new Date()).getTime()));
            preparedStatement.setTimestamp(3, new Timestamp(new Date().getTime()));
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Date data = new Date(resultSet.getTimestamp("data_transmissao").getTime());
                int velocidade = resultSet.getInt("velocidade");
                Double latitude = resultSet.getDouble("latitude");
                Double longitude = resultSet.getDouble("longitude");
                Transmissao t = new Transmissao();
                t.setVelocidade(velocidade);
                t.setDataTransmissao(data);
                t.setLatitude(latitude);
                t.setLongitude(longitude);
                lista.add(t);
            }
        } catch (Exception e) {
            LogSistema.logError("buscarTrasmissaoParaRelatorio", e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarTrasmissaoParaRelatorio", ex);
            }
        }
        return lista;
    }

    public List<TransmissaoServicoVO> buscarListaDeVeiculos(List<String> listaDePlacas){
        List<TransmissaoServicoVO> lista = new ArrayList<>();
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = new StringBuilder().append("select tb.id, v.placa, tb.data_transmissao, tb.velocidade, tb.latitude, tb.longitude, tb.cidade, tb.estado, ").append(" (select l.nome from evento_distancia ed inner join local l on l.id = ed.local where veiculo = v.id order by ed.data_atualizacao desc limit 1) as local ").append(" from transmissao_basica tb ").append(" inner join veiculo v on v.id = tb.veiculo  ").append(" where v.placa = ? order by id desc limit 1;").toString();
            connection = DataSourcePostgresOrion.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            for(String p : listaDePlacas) {
                preparedStatement.setString(1, p);
                resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {

                    Integer idTransmissao = resultSet.getInt("id");
                    String placa = resultSet.getString("placa");
                    Date data = new Date(resultSet.getTimestamp("data_transmissao").getTime());
                    Double velocidade = resultSet.getDouble("velocidade");
                    Double latitude = resultSet.getDouble("latitude");
                    Double longitude = resultSet.getDouble("longitude");
                    String cidade = resultSet.getString("cidade");
                    String estado = resultSet.getString("estado");
                    String local = resultSet.getString("local");
                    TransmissaoServicoVO t = new TransmissaoServicoVO();
                    t.setIdRodovia(idTransmissao);
                    t.setPlaca(placa);
                    t.setDataEvento(data);
                    t.setVelocidade(velocidade);
                    t.setLatitude(latitude);
                    t.setLongitude(longitude);
                    t.setCidade(cidade);
                    t.setEstado(estado);
                    t.setPontoReferenciaProximo(local);
                    lista.add(t);
                }
            }
        } catch (Exception e) {
            LogSistema.logError("buscarListaDeVeiculos", e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarListaDeVeiculos", ex);
            }
        }
        return lista;
    }


    public List<Double> buscarHodometroTelemetria(Integer idVeiculo, Date dataInicial, Date dataFinal) {
        List<Double> lista = new ArrayList<>();
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select min(hodometro), max(hodometro) from transmissao t " +
                    "where veiculo = ? " +
                    "and data_transmissao between ? and ? " +
                    "and rastreador = 'QUECLINK' and hodometro > 0 ";
            connection = DataSourcePostgresOrion.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idVeiculo);
            preparedStatement.setTimestamp(2, new Timestamp(dataInicial.getTime()));
            preparedStatement.setTimestamp(3, new Timestamp(dataFinal.getTime()));
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Double min = resultSet.getDouble("min");
                Double max = resultSet.getDouble("max");
                lista.add(min);
                lista.add(max);
            }
        } catch (Exception e) {
            LogSistema.logError("buscarHodometroTelemetria", e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarHodometroTelemetria", ex);
            }
        }
        return lista;
    }

}
