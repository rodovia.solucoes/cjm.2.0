package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.UnidadesFederativas;

import javax.persistence.EntityManager;

/**
 * @author analista.ti
 */
public class UnidadesFederativasDAO extends GenericDAO<Integer, UnidadesFederativas> {

    public UnidadesFederativasDAO(EntityManager entityManager) {
        super(entityManager);
    }
}