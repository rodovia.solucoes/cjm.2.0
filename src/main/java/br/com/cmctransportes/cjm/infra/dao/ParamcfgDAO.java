package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.Paramcfg;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;

/**
 * #2033016
 *
 * @author William Leite
 */
public class ParamcfgDAO extends GenericDAO<Integer, Paramcfg> {

    public ParamcfgDAO(EntityManager entityManager) {
        super(entityManager);
    }

    public Paramcfg findParam(Paramcfg filter) {
        Paramcfg result = null;

        try {
            String sql = "SELECT p FROM Paramcfg p WHERE"
                    + " ((:grcodigo is null and p.grcodigo is null) or p.grcodigo = :grcodigo) AND "
                    + " ((:ticodigo is null and p.ticodigo is null) or p.ticodigo = :ticodigo) AND "
                    + " ((:empresaId is null and p.empresaId is null) or p.empresaId = :empresaId) AND "
                    + " ((:unidadeId is null and p.unidadeId is null) or p.unidadeId = :unidadeId)";
            Query query = entityManager.createQuery(sql, Paramcfg.class)
                    .setParameter("grcodigo", filter.getGrcodigo())
                    .setParameter("ticodigo", filter.getTicodigo())
                    .setParameter("empresaId", filter.getEmpresaId())
                    .setParameter("unidadeId", filter.getUnidadeId());

            List tmpList = query.getResultList();

            if (tmpList != null && !tmpList.isEmpty()) {
                result = (Paramcfg) tmpList.get(0);
            }
        } catch (NoResultException nre) {
            nre.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public List<Paramcfg> findByGroup(String group) {
        List<Paramcfg> result = null;

        try {
            String sql = "SELECT p FROM Paramcfg p WHERE p.grcodigo = :grcodigo";
            Query query = entityManager.createQuery(sql, Paramcfg.class)
                    .setParameter("grcodigo", group);

            result = query.getResultList();
        } catch (NoResultException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return result;
    }

    public List<Paramcfg> findByGroup(String group, Integer idEmpresa) {
        List<Paramcfg> result = null;

        try {
            String sql = "SELECT p FROM Paramcfg p WHERE p.grcodigo = :grcodigo";
            Query query = entityManager.createQuery(sql, Paramcfg.class)
                    .setParameter("grcodigo", group);

            result = query.getResultList();
        } catch (NoResultException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return result;
    }
}
