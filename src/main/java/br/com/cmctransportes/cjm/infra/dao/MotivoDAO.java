package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.Motivo;

import javax.persistence.EntityManager;

/**
 * @author analista.ti
 */
public class MotivoDAO extends GenericDAO<Integer, Motivo> {
    public MotivoDAO(EntityManager entityManager) {
        super(entityManager);
    }
}