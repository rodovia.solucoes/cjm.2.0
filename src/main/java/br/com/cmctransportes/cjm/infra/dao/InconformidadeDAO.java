package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.Inconformidade;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.utils.TimeHelper;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class InconformidadeDAO extends GenericDAO<Integer, Inconformidade>  {

    public InconformidadeDAO(EntityManager entityManager) {
        super(entityManager);
        this.entityManager = entityManager;
    }

    public List<Inconformidade> buscarLista(Integer idEmpresa, Integer unidade){
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT i FROM Inconformidade i ");
            sb.append("WHERE ");
            sb.append("i.empresa.id = :idEmpresa ");
            if(Objects.nonNull(unidade) && unidade > 0){
                sb.append(" and i.unidade = :unidade ");
            }
            sb.append(" and (i.lido = false or i.lido is null) ");
            sb.append(" ORDER BY i.id DESC");

            Query query = entityManager.createQuery(sb.toString(), Inconformidade.class)
                    .setParameter("idEmpresa", idEmpresa);
            if(Objects.nonNull(unidade) && unidade > 0){
                query.setParameter("unidade", unidade);
            }
            List tmpList = query.getResultList();
            return tmpList;
        }catch (Exception e){
            LogSistema.logError("buscarLista", e);
        }
        return null;
    }

    public List<Inconformidade> buscarListaIncoformidadeDia(Integer idEmpresa, Integer unidade){
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT i FROM Inconformidade i ");
            sb.append("WHERE ");
            sb.append("i.empresa.id = :idEmpresa ");
            if(Objects.nonNull(unidade) && unidade > 0){
                sb.append(" and i.unidade = :unidade ");
            }
            sb.append(" and (i.lido = false or i.lido is null) ");
            sb.append(" and  data_inconformidade between :dataInicial and :dataFinal ");
            sb.append(" ORDER BY i.id DESC");

            Query query = entityManager.createQuery(sb.toString(), Inconformidade.class)
                    .setParameter("idEmpresa", idEmpresa);
            if(Objects.nonNull(unidade) && unidade > 0){
                query.setParameter("unidade", unidade);
            }

            Date d = new Date();

            query.setParameter("dataInicial", TimeHelper.getDate000000(d));
            query.setParameter("dataFinal", d );
            List tmpList = query.getResultList();
            return tmpList;
        }catch (Exception e){
            LogSistema.logError("buscarLista", e);
        }
        return null;
    }

    public List<Inconformidade> buscarTodasInconformidade(Integer idEmpresa, Integer unidade){
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT i FROM Inconformidade i ");
            sb.append("WHERE ");
            sb.append("i.empresa.id = :idEmpresa ");
            if(Objects.nonNull(unidade) && unidade > 0){
                sb.append(" and i.unidade = :unidade ");
            }
            sb.append(" ORDER BY i.id DESC");

            Query query = entityManager.createQuery(sb.toString(), Inconformidade.class)
                    .setParameter("idEmpresa", idEmpresa);
            if(Objects.nonNull(unidade) && unidade > 0){
                query.setParameter("unidade", unidade);
            }
            List tmpList = query.getResultList();
            return tmpList;
        }catch (Exception e){
            LogSistema.logError("buscarLista", e);
        }
        return null;
    }


    public Inconformidade buscarPeloId(Integer id){
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT i FROM Inconformidade i ");
            sb.append("WHERE ");
            sb.append("i.id = :id ");

            Query query = entityManager.createQuery(sb.toString(), Inconformidade.class)
                    .setParameter("id", id);
            List<Inconformidade> tmpList = query.getResultList();
            if(!tmpList.isEmpty()) {
                return tmpList.get(0);
            }
        }catch (Exception e){
            LogSistema.logError("buscarLista", e);
        }
        return null;
    }
}
