package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.Telemetria;
import br.com.cmctransportes.cjm.domain.entities.Transmissao;
import br.com.cmctransportes.cjm.domain.entities.vo.TelemetriaVO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgresOrion;
import br.com.cmctransportes.cjm.singleton.MongoDBConexao;
import br.com.cmctransportes.cjm.utils.TimeHelper;
import com.mongodb.BasicDBObject;
import com.mongodb.QueryBuilder;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.bson.Document;

import java.sql.*;
import java.util.*;
import java.util.Date;

public class TelemetriaDAO {
//https://industriahoje.com.br/como-calcular-o-consumo-de-combustivel
//https://wiki.teltonika-gps.com/view/Template:FMB_Accelerometer_Features_settings

    public List<Telemetria> buscarUltimaTelemetria(Integer idVeiculo){

        List<Telemetria> lista = new ArrayList<>();
        try {
            MongoDatabase mdb = MongoDBConexao.getMongoDBConexao().pegarBaseRodovia();
            MongoCollection<Document> coll = mdb.getCollection("telemetria");

            new QueryBuilder().put("idVeiculo").is(idVeiculo).get();
            QueryBuilder qb = new QueryBuilder();
            qb.and(new QueryBuilder().put("idVeiculo").is(idVeiculo).get());
            BasicDBObject query = new BasicDBObject("dataCadastro", //
                    new BasicDBObject("$gte", TimeHelper.getDate000000(new Date())).append("$lt", new Date()));
            qb.and(query);

            BasicDBObject basicDBObject = new BasicDBObject();
            basicDBObject.putAll(qb.get());

            MongoCursor<Document> cursor = coll.find(basicDBObject).iterator();
            while (cursor.hasNext()) {
                Document dd = cursor.next();
                Telemetria telemetria = convert(dd);
                lista.add(telemetria);
            }
            Collections.sort(lista, new Comparator<Telemetria>() {
                public int compare(Telemetria o1, Telemetria o2) {
                    return o1.getDataCadastro().compareTo(o2.getDataCadastro());
                }
            });

        }catch (Exception e){
            LogSistema.logError("buscarUltimasTelemetria", e);
        }

        return lista;
    }


    public List<Telemetria> buscarUltimaTelemetria(Integer idVeiculo, Date dataInicial, Date dateFinal){

        List<Telemetria> lista = new ArrayList<>();
        try {
            MongoDatabase mdb = MongoDBConexao.getMongoDBConexao().pegarBaseRodovia();
            MongoCollection<Document> coll = mdb.getCollection("telemetria");

            new QueryBuilder().put("idVeiculo").is(idVeiculo).get();
            QueryBuilder qb = new QueryBuilder();
            qb.and(new QueryBuilder().put("idVeiculo").is(idVeiculo).get());
            BasicDBObject query = new BasicDBObject("dataCadastro", //
                    new BasicDBObject("$gte", TimeHelper.getDate000000(dataInicial)).append("$lt", dateFinal));
            qb.and(query);

            BasicDBObject basicDBObject = new BasicDBObject();
            basicDBObject.putAll(qb.get());

            MongoCursor<Document> cursor = coll.find(basicDBObject).iterator();
            while (cursor.hasNext()) {
                Document dd = cursor.next();
                Telemetria telemetria = convert(dd);
                lista.add(telemetria);
            }
            Collections.sort(lista, new Comparator<Telemetria>() {
                public int compare(Telemetria o1, Telemetria o2) {
                    return o1.getDataCadastro().compareTo(o2.getDataCadastro());
                }
            });

        }catch (Exception e){
            LogSistema.logError("buscarUltimasTelemetria", e);
        }

        return lista;
    }

    private Telemetria convert(Document document){
        //t total_odometer,fuel_level, rpm
        Telemetria telemetria = new Telemetria();
        try {
            if (document.containsKey("dataCadastro")) {
                telemetria.setDataCadastro(document.getDate("dataCadastro"));
            }

            if (document.containsKey("idTransmissao")) {
                telemetria.setIdTransmissao(document.getInteger("idTransmissao"));
            }

            if (document.containsKey("interruptorDeFreio")) {
                String s = document.getString("interruptorDeFreio");
                telemetria.setInterruptorDeFreio(s.equalsIgnoreCase("1")?"Sim":"Não");
            }

            if (document.containsKey("controleDeCruzeiroAtivo")) {
                String s = document.getString("controleDeCruzeiroAtivo");
                telemetria.setControleDeCruzeiroAtivo(s.equalsIgnoreCase("1")?"Sim":"Não");
            }

            if (document.containsKey("chaveDeEmbreagem")) {
                telemetria.setChaveDeEmbreagem( document.getString("chaveDeEmbreagem"));
            }

            if (document.containsKey("estadoPTO")) {
                telemetria.setEstadoPTO( document.getString("estadoPTO"));
            }

            if (document.containsKey("cargaAtualDoMotor")) {
                String s = document.getString("cargaAtualDoMotor");
                telemetria.setCargaAtualDoMotor(s.concat("%"));
            }

            if (document.containsKey("temperaturaLiquidoArrefecimentoMotor")) {
                telemetria.setTemperaturaLiquidoArrefecimentoMotor( document.getString("temperaturaLiquidoArrefecimentoMotor"));
            }

            if (document.containsKey("velocidadeBaseadaNaRoda")) {
                telemetria.setVelocidadeBaseadaNaRoda( document.getString("velocidadeBaseadaNaRoda"));
            }

            if (document.containsKey("posicaoDoPedalDeAcelaracao")) {
                String s = document.getString("posicaoDoPedalDeAcelaracao");
                telemetria.setPosicaoDoPedalDeAcelaracao(s.concat("%"));
            }

            if (document.containsKey("rpm")) {
                String s = String.valueOf(document.getInteger("rpm"));
                telemetria.setRpm(Integer.parseInt(s));
            }

            if (document.containsKey("temperaturaMotor")) {
                telemetria.setTemperaturaMotor(document.getString("temperaturaMotor"));
            }

            if (document.containsKey("combustivelTotalUsadoPeloMotor")) {
                telemetria.setCombustivelTotalUsadoPeloMotor(document.getString("combustivelTotalUsadoPeloMotor"));
            }

            if (document.containsKey("fuelLevel")) {
                String s = document.getString("fuelLevel");
                telemetria.setFuelLevel(s.concat("%"));
            }

            if (document.containsKey("totalHorasOperacaoMotor")) {
                telemetria.setTotalHorasOperacaoMotor(document.getString("totalHorasOperacaoMotor"));
            }

            if (document.containsKey("taxaDeCombustivel")) {
                telemetria.setTaxaDeCombustivel(document.getString("taxaDeCombustivel"));
            }

            if (document.containsKey("economiaCombustivelInstantanea")) {
                telemetria.setEconomiaCombustivelInstantanea(document.getString("economiaCombustivelInstantanea"));
            }

            if (document.containsKey("axixX")) {
                telemetria.setAxixX(document.getString("axixX"));
            }

            if (document.containsKey("axixZ")) {
                telemetria.setAxixZ(document.getString("axixZ"));
            }

            if (document.containsKey("axixY")) {
                telemetria.setAxixY(document.getString("axixY"));
            }

            if (document.containsKey("tripOdometer")) {
                telemetria.setTripOdometer(document.getString("tripOdometer"));
            }

            if (document.containsKey("totalOdometer")) {
                telemetria.setTotalOdometer(document.getString("totalOdometer"));
            }

            if (document.containsKey("acelaracaoBrusca")) {
                telemetria.setAcelaracaoBrusca(new Boolean(document.getString("acelaracaoBrusca")));
            }

            if (document.containsKey("freadaBrusca")) {
                telemetria.setFreadaBrusca(new Boolean(document.getString("freadaBrusca")));
            }

            if (document.containsKey("curvaBrusca")) {
                telemetria.setCurvaBrusca(new Boolean(document.getString("curvaBrusca")));
            }

            if (document.containsKey("greenDrivingValue")) {
                telemetria.setGreenDrivingValue(Integer.parseInt(document.getString("greenDrivingValue")));
            }

        }catch (Exception e){
            LogSistema.logError("convert", e);
        }
        return telemetria;
    }

    public Telemetria buscarTelemetriaBasica(Integer idVeiculo){
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try{
            String sql = "select total_odometer,fuel_level, rpm from telemetria where veiculo = ? order by id desc limit 1 ";
            connection = DataSourcePostgresOrion.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idVeiculo);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                Double totalOdometro = resultSet.getDouble("total_odometer");
                Integer fuelLevel = resultSet.getInt("fuel_level");
                Integer rpm = resultSet.getInt("rpm");
                Telemetria telemetria = new Telemetria();
                if(Objects.nonNull(totalOdometro)){
                    telemetria.setTotalOdometer(totalOdometro.toString());
                }else{
                    telemetria.setTotalOdometer("0");
                }

                if(Objects.nonNull(fuelLevel)){
                    telemetria.setFuelLevel(fuelLevel.toString());
                }else{
                    telemetria.setTotalOdometer("0");
                }

                if(Objects.nonNull(rpm)){
                    telemetria.setRpm(rpm);
                }else{
                    telemetria.setRpm(0);
                }

                return telemetria;
            }
        }catch (Exception e) {
            LogSistema.logError("buscarTelemetriaBasica", e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarTelemetriaBasica", ex);
            }
        }
        return null;
    }

    public List<Telemetria> buscarTelemetriaPorVeiculo(Integer idVeiculo, Date dataInicial, Date dataFinal){
        List<Telemetria> lista = new ArrayList<>();
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try{
            String sql = "select t.total_odometer, t2.horimetro, combustivel_total_usado_pelo_motor, fuel_level, " +
                    " taxa_combustivel, rpm, " +
                    " t2.velocidade, t2.ignicao_ativa, t2.data_transmissao, t2.latitude, t2.longitude, economia_combustivel_instantanea, t.acelaracao_brusca, t.freada_brusca, t.curva_brusca    " +
                    " from telemetria t " +
                    " inner join transmissao t2 on t2.id = t.id_transmissao " +
                    " where t.veiculo = ? " +
                    " and data_cadastro between ? and ? " +
                    " order by data_cadastro ";

            connection = DataSourcePostgresOrion.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idVeiculo);
            preparedStatement.setTimestamp(2, new Timestamp(dataInicial.getTime()));
            preparedStatement.setTimestamp(3, new Timestamp(dataFinal.getTime()));
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Double totalOdometro = resultSet.getDouble("total_odometer");
                Double totalHorimetro = resultSet.getDouble("horimetro");
                Double combustivelTotal = resultSet.getDouble("combustivel_total_usado_pelo_motor");
                Integer fuelLevel = resultSet.getInt("fuel_level");
                Integer taxaCombustivel = resultSet.getInt("taxa_combustivel");
                Integer rpm = resultSet.getInt("rpm");
                Double velocidade = resultSet.getDouble("velocidade");
                Boolean ignicaoAtiva =  resultSet.getInt("ignicao_ativa") == 1;
                Date data = new Date(resultSet.getTimestamp("data_transmissao").getTime());
                Double latitude = resultSet.getDouble("latitude");
                Double longitude = resultSet.getDouble("longitude");
                Integer mediaCaminhao = resultSet.getInt("economia_combustivel_instantanea");
                Boolean acelaracaoBrusca = resultSet.getBoolean("acelaracao_brusca");
                Boolean freadaBrusca = resultSet.getBoolean("freada_brusca");
                Boolean curvaBrusca = resultSet.getBoolean("curva_brusca");
                Telemetria telemetria = new Telemetria();

                /*
                t.acelaracao_brusca, t.freada_brusca, t.curva_brusca
                 */

                if(Objects.nonNull(acelaracaoBrusca)){
                    telemetria.setAcelaracaoBrusca(acelaracaoBrusca);
                }else{
                    telemetria.setAcelaracaoBrusca(Boolean.FALSE);
                }

                if(Objects.nonNull(freadaBrusca)){
                    telemetria.setFreadaBrusca(freadaBrusca);
                }else{
                    telemetria.setFreadaBrusca(Boolean.FALSE);
                }

                if(Objects.nonNull(curvaBrusca)){
                    telemetria.setCurvaBrusca(curvaBrusca);
                }else{
                    telemetria.setCurvaBrusca(Boolean.FALSE);
                }

                if(Objects.nonNull(totalOdometro)){
                    telemetria.setTotalHodometro(totalOdometro);
                }else{
                    telemetria.setTotalHodometro(0d);
                }

                if(Objects.nonNull(totalHorimetro)){
                    telemetria.setTotalHorimetro(totalHorimetro);
                }else{
                    telemetria.setTotalHorimetro(0d);
                }

                if(Objects.nonNull(combustivelTotal)){
                    telemetria.setCombustivelTotal(combustivelTotal);
                }else{
                    telemetria.setCombustivelTotal(0d);
                }

                if(Objects.nonNull(fuelLevel)){
                    telemetria.setFuelLevelInt(fuelLevel);
                }else{
                    telemetria.setFuelLevelInt(0);
                }

                if(Objects.nonNull(taxaCombustivel)){
                    telemetria.setTaxaCombustivelInt(taxaCombustivel);
                }else{
                    telemetria.setTaxaCombustivelInt(0);
                }

                if(Objects.nonNull(rpm)){
                    telemetria.setRpm(rpm);
                }else{
                    telemetria.setRpm(0);
                }

                if(Objects.nonNull(velocidade)){
                    telemetria.setVelocidade(velocidade);
                }else{
                    telemetria.setVelocidade(0d);
                }

                if(Objects.nonNull(ignicaoAtiva)){
                    telemetria.setIgnicaoAtiva(ignicaoAtiva);
                }else{
                    telemetria.setIgnicaoAtiva(Boolean.FALSE);
                }

                if(Objects.nonNull(data)){
                    telemetria.setDataCadastro(data);
                }else{
                    telemetria.setDataCadastro(new Date());
                }
                if(Objects.nonNull(mediaCaminhao)){
                    telemetria.setMediaConsumo(mediaCaminhao);
                }else{
                    telemetria.setMediaConsumo(0);
                }

                telemetria.setLatitude(latitude);
                telemetria.setLongitude(longitude);

                lista.add(telemetria);
            }
        }catch (Exception e) {
            LogSistema.logError("buscarTelemetriaPorVeiculo", e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarTelemetriaPorVeiculo", ex);
            }
        }
        return lista;
    }


    public Telemetria buscarUltimaTelemetriaMongo(Integer idVeiculo){
        Telemetria telemetria = null;
        try {
            MongoDatabase mdb = MongoDBConexao.getMongoDBConexao().pegarBaseRodovia();
            MongoCollection<Document> coll = mdb.getCollection("telemetria");

            new QueryBuilder().put("idVeiculo").is(idVeiculo).get();
            QueryBuilder qb = new QueryBuilder();
            qb.and(new QueryBuilder().put("idVeiculo").is(idVeiculo).get());

            BasicDBObject basicDBObject = new BasicDBObject();
            basicDBObject.putAll(qb.get());

            MongoCursor<Document> cursor = coll.find(basicDBObject).sort(new BasicDBObject("_id",1)).iterator();
            while (cursor.hasNext()) {
                Document dd = cursor.next();
                telemetria = convert(dd);
            }

        }catch (Exception e){
            LogSistema.logError("buscarUltimasTelemetria", e);
        }
        return telemetria;
    }

}
