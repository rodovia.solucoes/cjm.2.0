package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.IntegracaoTerceiro;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgres;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class IntegracaoTerceiroDAO {

    public IntegracaoTerceiro buscarPelaEmpresa(String nomeEmpresa){
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from integracao_terceiros where empresa = '"+nomeEmpresa+"'";
            connection = DataSourcePostgres.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                String nome = resultSet.getString("empresa");
                Boolean ativo = resultSet.getBoolean("ativo");
                Double ultimoId = resultSet.getDouble("ultimo_id");

                IntegracaoTerceiro it = new IntegracaoTerceiro();
                it.setEmpresa(nome);
                it.setAtivo(ativo);
                it.setUltimoId(ultimoId);

                return it;
            }
        }catch (Exception e){
            LogSistema.logError("buscarPelaEmpresa: " + nomeEmpresa + " - ", e);
        }finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarLocais", ex);
            }
        }
        return null;
    }

    public void editar(IntegracaoTerceiro integracaoTerceiro){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "update integracao_terceiros  set ultimo_id = ? where empresa = ?";
            connection = DataSourcePostgres.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setDouble(1, integracaoTerceiro.getUltimoId());
            preparedStatement.setString(2, integracaoTerceiro.getEmpresa());
            preparedStatement.executeUpdate();
        }catch (Exception e){
            LogSistema.logError("editar: " + integracaoTerceiro.getEmpresa() + " - ", e);
        }finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("editar", ex);
            }
        }
    }

}
