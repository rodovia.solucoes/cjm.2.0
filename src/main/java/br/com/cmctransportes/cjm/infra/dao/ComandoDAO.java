package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.comandos.ComandoService;
import br.com.cmctransportes.cjm.comandos.IComando;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.proxy.modelo.Veiculo;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgresOrion;

import java.sql.*;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class ComandoDAO {

    public void inserirComando(Integer idComando, Integer idVeiculo) {
        try {
            Veiculo veiculo = new Veiculo();
            veiculo.setId(idVeiculo);

            if (idComando == 1) {
                List<IComando> l = ComandoService.getInstance().processarComando(idVeiculo);
                for (IComando comando : l) {
                    comando.bloquear(veiculo);
                }
            }
            if (idComando == 2) {
                List<IComando> l = ComandoService.getInstance().processarComando(idVeiculo);
                for (IComando comando : l) {
                    comando.desbloquear(veiculo);
                }
            }

            if (idComando == 3) {
                List<IComando> l = ComandoService.getInstance().processarComando(idVeiculo);
                for (IComando comando : l) {
                    comando.desbloquearIbutton(veiculo);
                }
            }

            if (idComando == 4) {
                List<IComando> l = ComandoService.getInstance().processarComando(idVeiculo);
                for (IComando comando : l) {
                    comando.bloquearIbutton(veiculo);
                }
            }
        } catch (Exception e) {
            LogSistema.logError("inserirComando", e);
        }
    }


}
