package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.EntityLog;

import javax.persistence.EntityManager;

/**
 * @author William Leite
 */
public class EntityLogDAO extends GenericDAO<Integer, EntityLog> {

    public EntityLogDAO(EntityManager entityManager) {
        super(entityManager);
    }
}