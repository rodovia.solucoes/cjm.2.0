/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.Motoristas;
import br.com.cmctransportes.cjm.domain.entities.Unidades;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgres;

import javax.persistence.EntityManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author analista.ti
 */
public class UnidadesDAO extends GenericDAO<Integer, Unidades> {

    private EntityManager em;

    public UnidadesDAO(EntityManager entityManager) {
        super(entityManager);
        this.em = entityManager;
    }

    public Double buscarValorDiaria(Integer idMotorista){
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select u.valor_diaria from unidades u  " +
                    " inner join motoristas m2 on m2.unidades_id = u.id  " +
                    " where m2.id  = ?";
            connection = DataSourcePostgres.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idMotorista);
            resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                double valor = resultSet.getDouble("valor_diaria");
            }
        } catch (Exception e) {
            LogSistema.logError("buscarValorDiaria", e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarValorDiaria", ex);
            }
        }
        return 0d;
    }

}
