package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.vo.*;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgresOrion;
import br.com.cmctransportes.cjm.singleton.MongoDBConexao;
import br.com.cmctransportes.cjm.utils.TimeHelper;
import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.*;

public class EventoVeiculoDAO {

    public InformacaoRelatorioVO buscarEventosDoDia(Integer idVeiculo) {

        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        InformacaoRelatorioVO informacaoRelatorio = new InformacaoRelatorioVO();
        try {
            String sql = "select ev.data_evento, ev.tipo, l.nome, " +
                    " (select nome from motorista m where ibutton = (select ibutton from transmissao where id = ev.transmissao) and m.status = true) as motorista " +
                    " from evento_veiculo ev  inner join local l on ev.local = l.id  where ev.veiculo = ? and ev.data_evento between ? and ? order by ev.data_evento;";
            connection = DataSourcePostgresOrion.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idVeiculo);
            preparedStatement.setTimestamp(2, new Timestamp(TimeHelper.getDate000000(new Date()).getTime()));
            preparedStatement.setTimestamp(3, new Timestamp(new Date().getTime()));
            resultSet = preparedStatement.executeQuery();
            EventoInternoVO eventoInternoDaVez = null;
            int i = 0;
            while (resultSet.next()) {
                Date dataDoEvento = new Date(resultSet.getTimestamp("data_evento").getTime());
                Integer tipo = resultSet.getInt("tipo");
                String nomeLocal = resultSet.getString("nome");
                String motorista = resultSet.getString("motorista");
                if (motorista == null) {
                    motorista = "";
                }

                if (eventoInternoDaVez == null) {
                    eventoInternoDaVez = new EventoInternoVO();
                    eventoInternoDaVez.setDataDoEvento(dataDoEvento);
                    eventoInternoDaVez.setTipo(tipo);
                    eventoInternoDaVez.setNomeLocal(nomeLocal);
                    eventoInternoDaVez.setMotorista(motorista);
                    continue;
                }

                EventoInternoVO evento = new EventoInternoVO();
                evento.setDataDoEvento(dataDoEvento);
                evento.setTipo(tipo);
                evento.setNomeLocal(nomeLocal);
                evento.setMotorista(motorista);

                if (!evento.getTipo().equals(eventoInternoDaVez.getTipo())) {
                    processarValoresEvento(eventoInternoDaVez, informacaoRelatorio);
                    eventoInternoDaVez = evento;
                }

                if (i >= resultSet.getRow()) {
                    processarValoresEvento(eventoInternoDaVez, informacaoRelatorio);
                }
                i++;

            }
        } catch (Exception e) {
            LogSistema.logError("buscarLocais", e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarLocais", ex);
            }

            informacaoRelatorio.getListaInformacaoRelatorio().sort(Comparator.comparing((InformacaoRelatorioVO s) -> s.getDataDoEvento()));
            Collections.reverse(informacaoRelatorio.getListaInformacaoRelatorio());
            return informacaoRelatorio;
        }
    }

    private void processarValoresEvento(EventoInternoVO eventoInternoPrimeiro, InformacaoRelatorioVO informacaoRelatorio) {
        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            InformacaoRelatorioVO ir = new InformacaoRelatorioVO();
            ir.setCampo01(eventoInternoPrimeiro.getNomeLocal());
            String tipo = "Não Encontrado";
            switch (eventoInternoPrimeiro.getTipo()) {
                case 1:
                    tipo = "Entrada";
                    break;
                case 2:
                    tipo = "Saída";
                    break;
                case 4:
                    tipo = "Permanece";
                    break;

                default:
                    break;
            }
            ir.setCampo02(tipo);
            ir.setCampo03(dateFormat.format(eventoInternoPrimeiro.getDataDoEvento()));
            if (eventoInternoPrimeiro.getMotorista() == null) {
                ir.setCampo04("");
            } else {
                ir.setCampo04(eventoInternoPrimeiro.getMotorista());
            }
            ir.setDataDoEvento(eventoInternoPrimeiro.getDataDoEvento());
            informacaoRelatorio.adicionarInformacaoRelatorioNaLista(ir);
        } catch (Exception e) {
            LogSistema.logError("processarValoresEvento", e);
        }
    }


    public List<TimelineVO> buscarEventoParaTimeline(Integer idVeiculo) {
        List<TimelineVO> lista = new ArrayList<>();
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM HH:mm");
        try {
            String sql = "select data_evento, tipo, evento_teclado, " +
                    " (select nome from local where id = local) as nome_local, " +
                    " (select data_transmissao from transmissao t2 where id = ev.transmissao) as data_transmissao, " +
                    " ev.transmissao  " +
                    " from evento_veiculo ev  " +
                    " where veiculo = ? " +
                    " and data_evento between ? and ?" +
                    " order by data_evento ;";
            connection = DataSourcePostgresOrion.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idVeiculo);
            preparedStatement.setTimestamp(2, new Timestamp(TimeHelper.getDate000000(new Date()).getTime()));
            preparedStatement.setTimestamp(3, new Timestamp(new Date().getTime()));
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Date dataEvento = resultSet.getTimestamp("data_evento");
                Integer tipo = resultSet.getInt("tipo");
                String eventoTeclado = resultSet.getString("evento_teclado");
                String nomeLocal = resultSet.getString("nome_local");
                Date dataTransmissao = resultSet.getTimestamp("data_transmissao");
                switch (tipo){
                    case 1:{
                        TimelineVO vo = new TimelineVO();
                        vo.setDataDoEvento(dataTransmissao);
                        vo.setData(simpleDateFormat.format(dataTransmissao));
                        vo.setDescricao("E."+nomeLocal);
                        lista.add(vo);
                    }break;
                    case 2:{
                        TimelineVO vo = new TimelineVO();
                        vo.setDataDoEvento(dataTransmissao);
                        vo.setData(simpleDateFormat.format(dataTransmissao));
                        vo.setDescricao("S."+nomeLocal);
                        lista.add(vo);
                    }break;
                    case 7:{
                        TimelineVO vo = new TimelineVO();
                        vo.setDataDoEvento(dataTransmissao);
                        vo.setData(simpleDateFormat.format(dataTransmissao));
                        vo.setDescricao(eventoTeclado);
                        lista.add(vo);
                    }break;
                }

            }

        } catch (Exception e) {
            LogSistema.logError("buscarEventoParaTimeline", e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarLocais", ex);
            }
        }
        return lista;
    }


    public List<EventoVeiculoVO> buscarUltimosEventosMongo(Integer idVeiculo) {
        List<EventoVeiculoVO> lista = new ArrayList<>();
        try {
            MongoDatabase mdb = MongoDBConexao.getMongoDBConexao().pegarBaseRodovia();
            MongoCollection<Document> coll = mdb.getCollection("evento");
            Document docFind = new Document();
            docFind.append("veiculoId", idVeiculo);

            BasicDBObject query = new BasicDBObject("dataTransmissao", //
                    new BasicDBObject("$gte", TimeHelper.diminuirHoras(new Date(), -5)).append("$lt", new Date()));

            MongoCursor<Document> cursor = coll.find(docFind).sort(new Document("_id", -1)).limit(100).iterator();
            Gson gson = new Gson();
            while (cursor.hasNext()) {
                Document dd = cursor.next();
                EventoVeiculoVO vo = convertInEventoVeiculo(dd);
                lista.add(vo);
            }
        } catch (Exception e) {
            LogSistema.logError("buscarUltimosEventos", e);
        }
        return lista;
    }


    private  EventoVeiculoVO convertInEventoVeiculo(Document document) throws Exception {
        try {
            EventoVeiculoVO t = new EventoVeiculoVO();
            if (document.containsKey("id")) {
                t.setId(document.getLong("id").intValue());
            }
            if (document.containsKey("dataEvento")) {
                t.setDataEvento(document.getDate("dataEvento"));
            }
            if (document.containsKey("tipo")) {
                t.setTipo(document.getInteger("tipo"));
            }
            if (document.containsKey("localId")) {
                t.setLocalId(document.getInteger("localId"));
            }
            if (document.containsKey("veiculoId")) {
                t.setTransmissaoId(document.getInteger("transmissaoId"));
            }
            if (document.containsKey("veiculoId")) {
                t.setVeiculoId(document.getInteger("veiculoId"));
            }
            return t;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<EventoVeiculoVO> buscarListaDeEnvento(Integer idVeiculo, Date dataInicial, Date dataFinal){
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        List<EventoVeiculoVO> lista = new ArrayList<>();
        try {
            String sql = "select ev.id, data_evento, ev.tipo, l2.nome, l2.funcao from evento_veiculo ev " +
                    " inner join local  l2 on l2.id = ev.local " +
                    " where veiculo = ? " +
                    " and data_evento between ? and ?  order by data_evento";
            connection = DataSourcePostgresOrion.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idVeiculo);
            preparedStatement.setTimestamp(2, new Timestamp(TimeHelper.getDate000000(dataInicial).getTime()));
            preparedStatement.setTimestamp(3, new Timestamp(TimeHelper.getDate235959(dataFinal).getTime()));
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Integer id = resultSet.getInt("id");
                Date dataEvento = resultSet.getTimestamp("data_evento");
                Integer tipoEvento = resultSet.getInt("tipo");
                String nomeCerca = resultSet.getString("nome");
                Integer funcao = resultSet.getInt("funcao");
                EventoVeiculoVO eventoVeiculoVO = new EventoVeiculoVO();
                eventoVeiculoVO.setId(id);
                eventoVeiculoVO.setDataEvento(dataEvento);
                eventoVeiculoVO.setTipo(tipoEvento);
                LocalVO localVO = new LocalVO();
                localVO.setNome(nomeCerca);
                localVO.setFuncao(funcao);
                eventoVeiculoVO.setLocal(localVO);
                lista.add(eventoVeiculoVO);
            }
        }catch (Exception e) {
            LogSistema.logError("buscarListaDeEnvento", e);
        }finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarLocais", ex);
            }
        }
        return lista;
    }



    public List<EventoVeiculoVO> buscarListaDeEventoCarregamento(Integer idVeiculo, Date dataInicial, Date dataFinal){
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        List<EventoVeiculoVO> lista = new ArrayList<>();
        try {
            String sql = "select ev.id, data_evento, ev.tipo, l2.nome, l2.funcao from evento_veiculo ev " +
                    " inner join local  l2 on l2.id = ev.local " +
                    " where veiculo = ? " +
                    " and l2.funcao = 1 " +
                    " and data_evento between ? and ?  order by data_evento";
            connection = DataSourcePostgresOrion.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idVeiculo);
            preparedStatement.setTimestamp(2, new Timestamp(TimeHelper.getDate000000(dataInicial).getTime()));
            preparedStatement.setTimestamp(3, new Timestamp(TimeHelper.getDate235959(dataFinal).getTime()));
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Integer id = resultSet.getInt("id");
                Date dataEvento = resultSet.getTimestamp("data_evento");
                Integer tipoEvento = resultSet.getInt("tipo");
                String nomeCerca = resultSet.getString("nome");
                Integer funcao = resultSet.getInt("funcao");
                EventoVeiculoVO eventoVeiculoVO = new EventoVeiculoVO();
                eventoVeiculoVO.setId(id);
                eventoVeiculoVO.setDataEvento(dataEvento);
                eventoVeiculoVO.setTipo(tipoEvento);
                LocalVO localVO = new LocalVO();
                localVO.setNome(nomeCerca);
                localVO.setFuncao(funcao);
                eventoVeiculoVO.setLocal(localVO);
                lista.add(eventoVeiculoVO);
            }
        }catch (Exception e) {
            LogSistema.logError("buscarListaDeEnvento", e);
        }finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarLocais", ex);
            }
        }
        return lista;
    }

    public EventoVeiculoVO buscarUltimoEvento(Integer idVeiculo, Date dataInicial, Date dataFinal){
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;

        try {
            String sql = "select ev.id, data_evento, ev.tipo, l2.nome, l2.funcao from evento_veiculo ev " +
                    " inner join local  l2 on l2.id = ev.local " +
                    " where veiculo = ?  and ev.data_processado between ? and ? " +
                    " order by data_evento desc limit 1 ";
            connection = DataSourcePostgresOrion.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idVeiculo);
            preparedStatement.setTimestamp(2, new Timestamp(TimeHelper.getDate000000(dataInicial).getTime()));
            preparedStatement.setTimestamp(3, new Timestamp(dataFinal.getTime()));
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                Integer id = resultSet.getInt("id");
                Date dataEvento = resultSet.getTimestamp("data_evento");
                Integer tipoEvento = resultSet.getInt("tipo");
                String nomeCerca = resultSet.getString("nome");
                Integer funcao = resultSet.getInt("funcao");
                EventoVeiculoVO eventoVeiculoVO = new EventoVeiculoVO();
                eventoVeiculoVO.setId(id);
                eventoVeiculoVO.setDataEvento(dataEvento);
                eventoVeiculoVO.setTipo(tipoEvento);
                LocalVO localVO = new LocalVO();
                localVO.setNome(nomeCerca);
                localVO.setFuncao(funcao);
                eventoVeiculoVO.setLocal(localVO);
                return eventoVeiculoVO;
            }
        }catch (Exception e) {
            LogSistema.logError("buscarUltimoEvento", e);
        }finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarUltimoEvento", ex);
            }
        }
        return null;
    }

}
