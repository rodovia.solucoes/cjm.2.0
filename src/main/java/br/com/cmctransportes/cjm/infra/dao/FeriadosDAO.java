package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.*;
import br.com.cmctransportes.cjm.domain.services.UnidadesService;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.sascarsoap.Cidade;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgres;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @author analista.ti
 */
public class FeriadosDAO extends GenericDAO<Integer, Feriados> {
    public FeriadosDAO(EntityManager entityManager) {
        super(entityManager);
    }

    // função para listar os feriados para serem lançados nas jornadas
    public List<Date> getDataDosFeriados(Date dataInicial, Date dataFinal, Motoristas motorista, UnidadesService unidadesService) {
        try {
            String sql
                    = " select cast(data_ocorrencia as timestamp without time zone) "
                    + " from feriados "
                    + " where "
                    + " data_ocorrencia between :dataInicio and :dataFim "
                    + " and empresa_id = " + motorista.getObjEmpresa().getId()
                    + " and ( "
                    + " ambito = 3 or "                           // se for nacional
                    + " (uf = :uf and ambito = 2) or "            // do estado da unidade
                    + " (cidade = :cidade and ambito = 1)) "      // ou da cidade da unidade
                    + " and data_ocorrencia not in ( "
                    + " select cast(e.instante_evento as date) "
                    + " from evento e "
                    + " inner join jornada j on "
                    + " e.jornada_id = j.id "
                    + " where "
                    + " e.instante_evento between :dataInicio and :dataFim "
                    + " and e.tipo_evento in (18, 20, 22) "
                    + " and j.motoristas_id = :motoristaId)"
                    + " order by data_ocorrencia;";

            Cidades cidadeLotacao = motorista.pegaCidadeLotacao(unidadesService);
            String codIbgeCidade = null;
            Integer codIbgeUF = null;
            if (cidadeLotacao != null) {
                codIbgeCidade = cidadeLotacao.getCodIbge();
                codIbgeUF = cidadeLotacao.getUfCodIbge().getCodIbge();
            }

            Query query = entityManager.createNativeQuery(sql)
                    .setParameter("dataInicio", dataInicial)
                    .setParameter("dataFim", dataFinal)
                    .setParameter("cidade", codIbgeCidade)
                    .setParameter("uf", codIbgeUF)
                    .setParameter("motoristaId", motorista.getId());
            List<Date> result = query.getResultList();
            return result;
        } catch (NoResultException nre) {
            nre.printStackTrace();
        } catch (Exception except) {
            except.printStackTrace();
        }

        return null;
    }

    /**
     * Returns a list of holidays based on the "inicio", "motorista"'s "cidadeLotacao"'s "codIbge" and "motorista"'s "cidadeLotacao"'s "ufCodIbge"'s "codIbge",
     * ordered by "data_ocorrencia"
     *
     * @param inicio    Start date
     * @param fim       End date
     * @param motorista Driver
     * @return List of holidays
     */

    public List<Feriados> getEventoEmFeriado(Date inicio, Motoristas motorista, UnidadesService unidadesService) {
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            List<Feriados> lista = new ArrayList<>();
            StringBuilder sql = new StringBuilder();
            sql.append("select * ");
            sql.append("from feriados ");
            sql.append("where ");
            sql.append(" ? between data_ocorrencia and (data_ocorrencia + interval '23:59:59') ");
            sql.append(" and empresa_id = ? ");
            sql.append(" and ( ");
            sql.append("ambito = 3 or ");                           // se for nacional
            sql.append("(uf = ? and ambito = 2) or ");            // do estado da unidade
            sql.append("(cidade = ? and ambito = 1)) ");      // ou da cidade da unidade
            sql.append("order by data_ocorrencia;");

            Cidades cidadeLotacao = motorista.pegaCidadeLotacao(unidadesService);
            String codIbgeCidade = cidadeLotacao.getCodIbge();
            Integer codIbgeUF = cidadeLotacao.getUfCodIbge().getCodIbge();

            connection = DataSourcePostgres.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql.toString());
            preparedStatement.setDate(1, new java.sql.Date(inicio.getTime()));
            preparedStatement.setInt(2, motorista.getObjEmpresa().getId());
            preparedStatement.setInt(3, codIbgeUF);
            preparedStatement.setString(4, codIbgeCidade);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Feriados feriados = new Feriados();
                Integer id = resultSet.getInt("id");
                feriados.setId(id);
                Date dataOcorrecia = resultSet.getDate("data_ocorrencia");
                feriados.setDataOcorrencia(dataOcorrecia);
                String descricao = resultSet.getString("descricao");
                feriados.setDescricao(descricao);
                String cidade = resultSet.getString("cidade");
                if (cidade != null) {
                    Cidades ci = new Cidades();
                    ci.setCodIbge(cidade);
                    feriados.setCidade(ci);
                }

                Integer uf = resultSet.getInt("uf");
                if (uf > 0) {
                    UnidadesFederativas unidadesFederativas = new UnidadesFederativas();
                    unidadesFederativas.setCodIbge(uf);
                    feriados.setUf(unidadesFederativas);
                }

                Integer ambito = resultSet.getInt("ambito");
                AmbitosFeriados ambitosFeriados = new AmbitosFeriados();
                ambitosFeriados.setId(ambito);
                feriados.setAmbito(ambitosFeriados);

                lista.add(feriados);
            }

            return lista;
        } catch (NoResultException nre) {
            nre.printStackTrace();
        } catch (Exception except) {
            except.printStackTrace();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("getEventoEmFeriado", ex);
            }

        }

        return null;
    }


    public List<Feriados> getEventoEmFeriado(Date inicio, Date fim, Motoristas motorista, UnidadesService unidadesService) {
        List<Feriados> lista = new ArrayList<>();
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            if (Objects.nonNull(inicio) && Objects.nonNull(fim)) {
                StringBuilder sql = new StringBuilder();
                sql.append("select * ");
                sql.append("from feriados ");
                sql.append("where ");
                sql.append(" data_ocorrencia between ? and ? ");
                sql.append(" and empresa_id = ? ");
                sql.append(" and ( ");
                sql.append("ambito = 3 or ");                           // se for nacional
                sql.append("(uf = ? and ambito = 2) or ");            // do estado da unidade
                sql.append("(cidade = ? and ambito = 1)) ");      // ou da cidade da unidade
                sql.append("order by data_ocorrencia;");

                Cidades cidadeLotacao = motorista.pegaCidadeLotacao(unidadesService);
                String codIbgeCidade = cidadeLotacao.getCodIbge();
                Integer codIbgeUF = cidadeLotacao.getUfCodIbge().getCodIbge();

                connection = DataSourcePostgres.getInstance().getConnection();
                preparedStatement = connection.prepareStatement(sql.toString());
                preparedStatement.setDate(1, new java.sql.Date(inicio.getTime()));
                preparedStatement.setDate(2, new java.sql.Date(fim.getTime()));
                preparedStatement.setInt(3, motorista.getObjEmpresa().getId());
                preparedStatement.setInt(4, codIbgeUF);
                preparedStatement.setString(5, codIbgeCidade);
                resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    Feriados feriados = new Feriados();
                    Integer id = resultSet.getInt("id");
                    feriados.setId(id);
                    Date dataOcorrecia = resultSet.getDate("data_ocorrencia");
                    feriados.setDataOcorrencia(dataOcorrecia);
                    String descricao = resultSet.getString("descricao");
                    feriados.setDescricao(descricao);
                    String cidade = resultSet.getString("cidade");
                    if (cidade != null) {
                        Cidades ci = new Cidades();
                        ci.setCodIbge(cidade);
                        feriados.setCidade(ci);
                    }

                    Integer uf = resultSet.getInt("uf");
                    if (uf > 0) {
                        UnidadesFederativas unidadesFederativas = new UnidadesFederativas();
                        unidadesFederativas.setCodIbge(uf);
                        feriados.setUf(unidadesFederativas);
                    }

                    Integer ambito = resultSet.getInt("ambito");
                    AmbitosFeriados ambitosFeriados = new AmbitosFeriados();
                    ambitosFeriados.setId(ambito);
                    feriados.setAmbito(ambitosFeriados);

                    lista.add(feriados);
                }
            }

        } catch (NoResultException nre) {
            nre.printStackTrace();
        } catch (Exception except) {
            except.printStackTrace();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("getEventoEmFeriado", ex);
            }
        }
        return lista;
    }


    public List<Feriados> buscarTodos(Integer idEmpresa, Integer idUnidade) throws Exception {
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT f FROM Feriados f ");
            sql.append(" WHERE f.empresaId = :empresa ");
            if (Objects.nonNull(idUnidade) && idUnidade > 0) {
                sql.append(" and f.idUnidade = :unidade ");
            }
            sql.append(" ORDER BY f.descricao ASC");

            Query query = entityManager.createQuery(sql.toString(), Feriados.class)
                    .setParameter("empresa", new Empresas(idEmpresa));
            if (Objects.nonNull(idUnidade) && idUnidade > 0) {
                query.setParameter("unidade", idUnidade);
            }
            List<Feriados> feriados = query.getResultList();
            return feriados;
        } catch (Exception e) {
            throw e;
        }
    }

}
