package br.com.cmctransportes.cjm.infra.dao;

import br.com.chart.address.searcher.exception.AddressNotFoundException;
import br.com.chart.address.searcher.exception.GoogleServiceCommunicationException;
import br.com.chart.address.searcher.exception.UnhandledException;
import br.com.chart.address.searcher.service.ChartAddressSearcher;
import br.com.cmctransportes.cjm.domain.entities.Evento;
import br.com.cmctransportes.cjm.domain.entities.EventosJornada;
import br.com.cmctransportes.cjm.domain.entities.Motoristas;
import br.com.cmctransportes.cjm.domain.entities.vo.MotoristaVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RelatorioEventoVO;
import br.com.cmctransportes.cjm.domain.services.EntityLogService;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.sascarsoap.Motorista;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgres;
import br.com.cmctransportes.cjm.utils.TimeHelper;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.util.stream.Collectors;

/**
 * @author analista.ti
 */
public class EventoDAO {

    private EntityLogService<Integer, Evento> logService;

    private final EntityManager entityManager;

    public EventoDAO(EntityManager entityManager) {
        this.entityManager = entityManager;
        this.logService = new EntityLogService<>(Evento.class, entityManager);
    }

    public void delete(Evento entity) {
        try {
            Integer id = entity.getId();
            this.logService.log(EntityLogService.DELETE, entity, id, entity.getOperadorLancamento().getId());

            // Se tiver, remove referencias
            if (Objects.nonNull(entity.getEventoAnterior())) {
                entityManager.createNativeQuery("UPDATE Evento SET evento_anterior = :eventoAnteriorID WHERE evento_anterior = :eventoID")
                        .setParameter("eventoID", id)
                        .setParameter("eventoAnteriorID", entity.getEventoAnterior().getId())
                        .executeUpdate();

                entityManager.flush();
            }

            // Remove evento
            entityManager.createNativeQuery("DELETE FROM Evento e WHERE e.id = :eventoID")
                    .setParameter("eventoID", id)
                    .executeUpdate();

            entityManager.flush();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public List<RelatorioEventoVO> getRelatorioDeEventos(Motoristas motorista, Date dataInicio, Date dataFinal) {
        List<RelatorioEventoVO> result = null;

        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT e FROM Evento e JOIN FETCH e.jornadaId j");
            sql.append(" WHERE j.motoristasId.id = :motorista AND e.instanteEvento BETWEEN :inicio AND :fim");
            sql.append(" AND e.removido = false");
            sql.append(" ORDER BY e.instanteEvento ASC");

            Query query = entityManager.createQuery(sql.toString(), Evento.class)
                    .setParameter("motorista", motorista.getId())
                    .setParameter("inicio", dataInicio)
                    .setParameter("fim", dataFinal);

            List<Evento> eventos = query.getResultList();
            result = eventos.stream().map(e -> new RelatorioEventoVO(e.getTipoEvento().getDescricao(), e.getLatitude(), e.getLongitude(), null, e.getInstanteEvento(), e.getOrigem(), e.getPositionAddress()))
                    .collect(Collectors.toList());
        } catch (NoResultException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return result;
    }

    public List<Evento> findCurrentEventAll(Integer idEmpresa, Integer idUnidade) {
        List<Evento> result = null;

        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT e.* FROM evento e");
            sql.append(" INNER JOIN jornada j ON j.id = e.jornada_id");
            sql.append(" INNER JOIN motoristas m ON m.id = j.motoristas_id");
            sql.append(" INNER JOIN (SELECT j2.motoristas_id, MAX(instante_evento) AS maxDate");
            sql.append(" FROM evento e0 LEFT JOIN jornada j2 ON j2.id = e0.jornada_id WHERE e0.removido = false GROUP BY j2.motoristas_id) groupedEv");
            sql.append(" ON e.instante_evento = groupedEv.maxDate AND j.motoristas_id = groupedEv.motoristas_id");
            sql.append(" WHERE m.empresa_id = :empresa");
            sql.append(" AND m.situacao = 1");

            if (Objects.nonNull(idUnidade)) {
                sql.append(" AND m.unidades_id = :unidade");
            }

            Query query = entityManager.createNativeQuery(sql.toString(), Evento.class).setParameter("empresa", idEmpresa);

            if (Objects.nonNull(idUnidade)) {
                query.setParameter("unidade", idUnidade);
            }

            result = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return result;
    }

    public Evento getLastEvent(Integer jornada, Date fromDate) {
        Evento result = null;
        try {
            String sql = "SELECT e FROM Evento e WHERE e.instanteEvento < :fromDate AND e.jornadaId.id = :jornada AND e.removido = false ORDER BY e.instanteEvento DESC";
            Query query = entityManager.createQuery(sql, Evento.class)
                    .setParameter("jornada", jornada)
                    .setParameter("fromDate", fromDate).setMaxResults(1);

            Optional<Evento> evento = query.getResultList().stream().findFirst();
            if (evento.isPresent()) {
                result = evento.get();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public Evento getLastevent(Integer motorista) {
        try {
            String sql = "SELECT e FROM Evento e JOIN FETCH e.jornadaId j WHERE j.motoristasId.id = :motorista  AND e.removido = false ORDER BY e.instanteEvento DESC";
            Query query = entityManager.createQuery(sql, Evento.class)
                    .setParameter("motorista", motorista).setMaxResults(1);

            Evento result = null;

            if (query != null) {
                List tmpList = query.getResultList();
                if (tmpList != null && tmpList.size() > 0) {
                    result = (Evento) tmpList.get(0);
                }
            }

            return result;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public Evento getEventByInstant(Evento evt) {
        try {

//            String sql = "SELECT e FROM Evento e JOIN e.jornadaId j WHERE j.motoristasId.id = :motorista and e.instanteEvento = :instante";
            StringBuilder sql = new StringBuilder();
            sql.append("select ");
            sql.append(" e.* ");
            sql.append("from ");
            sql.append(" evento e ");
            sql.append("inner join jornada j on ");
            sql.append(" e.jornada_id = j.id ");
            sql.append("where ");
            sql.append("j.motoristas_Id = :motorista ");
            sql.append("and e.removido = false ");
            sql.append("and e.tipo_evento = :tipoEvento ");
            sql.append("and (date_trunc('second',e.instante_Evento) = :instante ");
            sql.append("or e.instante_Evento = :instante) ");

            Query query = entityManager.createNativeQuery(sql.toString(), Evento.class)
                    .setParameter("tipoEvento", evt.getTipoEvento().getId())
                    .setParameter("instante", evt.getInstanteEvento())
                    .setParameter("motorista", evt.getJornadaId().getMotoristasId().getId());

            Evento result = null;

            if (query != null) {
                List tmpList = query.getResultList();
                if (tmpList != null && tmpList.size() > 0) {
                    result = (Evento) tmpList.get(0);
                }
            }

            return result;

        } catch (NoResultException nre) {
            nre.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public Evento getEventoPorTipoNaData(Evento evt, int tipo) {
        try {

            StringBuilder sql = new StringBuilder();
            sql.append("select ");
            sql.append(" evento.* ");
            sql.append("from ");
            sql.append(" evento ");
            sql.append("inner join jornada on ");
            sql.append(" evento.jornada_id = jornada.id ");
            sql.append("where ");
            sql.append(" evento.tipo_evento = :tipo ");
            sql.append(" AND evento.removido = false ");
            sql.append("and ");
            sql.append(" jornada.motoristas_id = :motoristaId ");
            sql.append("and ");
            sql.append(" :instante between instante_evento and (instante_evento + time '23:59:59.999')");

            Date inicio = evt.getJornadaId().getJornadaInicio();
            if (inicio == null) {
                inicio = evt.getInstanteEvento();
            }

            Query query = entityManager.createNativeQuery(sql.toString(), Evento.class)
                    .setParameter("instante", inicio)
                    .setParameter("tipo", tipo)
                    .setParameter("motoristaId", evt.getJornadaId().getMotoristasId().getId());

            Evento result = null;

            if (query != null) {
                List tmpList = query.getResultList();
                if (tmpList != null && tmpList.size() > 0) {
                    result = (Evento) tmpList.get(0);
                }
            }

            return result;

        } catch (NoResultException nre) {
            nre.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public Evento address(Evento entity) {
        if (Objects.isNull(entity.getPositionAddress()) || entity.getPositionAddress().isEmpty()) {
            if (Objects.nonNull(entity.getLatitude()) && Objects.nonNull(entity.getLongitude())) {
                String mainKey = "AIzaSyCEkM34QAqu68eDMlr3cFfx2QPnykZ-8W8";
                String[] backupKeys = new String[]{"AIzaSyCOUpCyrebJnRTJ_P-zB27-Q2uBcLyO5Ko",
                        "AIzaSyAvhu5PdKeiKIz0mFfrqH-1WAsGzgh0rQI",
                        "AIzaSyAB_2FXjOCJodC7PmL9e9qqrRcptYpQqhA",
                        "AIzaSyDJuo_0sz10xgOA5YBWmo3mtOFeLxjwEHc",
                        "AIzaSyBqU20UCulWGo7pT9itC5z3V39XPmzfLdw"};
                ChartAddressSearcher searcher = new ChartAddressSearcher(mainKey, backupKeys);

                try {
                    entity.setPositionAddress(searcher.search(entity.getLatitude(), entity.getLongitude()));
                } catch (AddressNotFoundException | GoogleServiceCommunicationException | UnhandledException e) {
                    e.printStackTrace();
                }
            }
        }

        return entity;
    }

    public Evento buscarUltimoEvento(MotoristaVO motorista) throws Exception{
        try{
            String sql = "select * " +
                    " from evento e " +
                    " where e.id = (select max(id) from evento where operador_lancamento = (select user_id from motoristas where id = :motoristaId))";
            Query query = entityManager.createNativeQuery(sql, Evento.class)
                    .setParameter("motoristaId", motorista.getId());
            List lista = query.getResultList();

            if(!lista.isEmpty()){
                return (Evento)lista.get(0);
            }
            return null;
        }catch (Exception e) {
           throw e;
        }
    }

    public List<Evento> buscarDezUltimosEvento(MotoristaVO motorista) throws Exception{
        try{
            String sql = "select * from evento e where " +
                    "        id in (select id from evento where operador_lancamento = (select user_id from motoristas where id = :motoristaId) order by id desc limit 10)\n" +
                    "        order by e.instante_lancamento desc";
            Query query = entityManager.createNativeQuery(sql, Evento.class)
                    .setParameter("motoristaId", motorista.getId());
            List lista = query.getResultList();
            return lista;
        }catch (Exception e) {
            throw e;
        }
    }


    public List<Evento> buscarUltimosEventosDoDia(MotoristaVO motorista) throws Exception{
        List<Evento> listaEvento = new ArrayList<>();
        try{
            String sql = "select instante_lancamento, ej.descricao, e.latitude, e.longitude from evento e " +
                    " inner join eventos_jornada ej on ej.id = e.tipo_evento  " +
                    " where instante_evento between :dataInicio and :dataFim and operador_lancamento = (select user_id from motoristas where id = :motoristaId) order by e.instante_evento  ";
            Query query = entityManager.createNativeQuery(sql)
                    .setParameter("dataInicio", TimeHelper.getDate000000())
                    .setParameter("dataFim", new Date())
                    .setParameter("motoristaId", motorista.getId());
            List lista = query.getResultList();
            lista.forEach(o -> {
                Object[] oo = (Object[])o;
                Evento evento = new Evento();
                evento.setInstanteEvento(new Date(((Timestamp)oo[0]).getTime()));
                EventosJornada eventosJornada = new EventosJornada();
                eventosJornada.setDescricao((String)oo[1]);
                evento.setTipoEvento(eventosJornada);
                evento.setLatitude((Double)oo[2]);
                evento.setLongitude((Double)oo[3]);
                listaEvento.add(evento);
            });
            return listaEvento;
        }catch (Exception e) {
            throw e;
        }
    }

    public Integer buscarUltimoIdEvento(Integer idMotorista){
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("select tipo_evento from evento where id = (select max(id) from evento where operador_lancamento = ?); ");
            connection = DataSourcePostgres.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql.toString());
            preparedStatement.setInt(1, idMotorista);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                Integer tipoEvento = resultSet.getInt("tipo_evento");
                return tipoEvento;
            }
        } catch (NoResultException nre) {
            nre.printStackTrace();
        } catch (Exception except) {
            except.printStackTrace();
        }finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscar", ex);
            }
        }

        return -100;
    }




}


