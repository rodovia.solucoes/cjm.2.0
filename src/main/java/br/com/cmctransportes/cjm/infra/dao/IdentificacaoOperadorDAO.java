package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.*;
import br.com.cmctransportes.cjm.domain.entities.vo.MotoristaOperadorVO;
import br.com.cmctransportes.cjm.domain.entities.vo.VeiculoAutorizacaoVO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgres;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgresOrion;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.sql.*;
import java.util.*;

public class IdentificacaoOperadorDAO extends GenericDAO<Integer, IdentificacaoOperador>{

    public IdentificacaoOperadorDAO(EntityManager entityManager) {
        super(entityManager);
    }

    public List<IdentificacaoOperador> buscarIdentificacaoOperador(Integer idEmpresa, Integer idUnidade) {

        String sql = "FROM IdentificacaoOperador e  where e.empresaId = :empresaId";
        if(Objects.nonNull(idUnidade) && idEmpresa >= 0){
            sql = sql + " and  e.idUnidade = :idUnidade ";
        }
        sql = sql + " ORDER BY e.descricao ";

        Query q = entityManager.createQuery(sql);
        q.setParameter("empresaId", new Empresas(idEmpresa));
        if(Objects.nonNull(idUnidade) && idEmpresa >= 0){
            q.setParameter("idUnidade", idUnidade);
        }
        List<IdentificacaoOperador> result = q.getResultList();

        for(IdentificacaoOperador identificacaoOperador : result){
            if(Objects.nonNull(identificacaoOperador.getListaVeiculos())){
                if( identificacaoOperador.getListaVeiculos().contains(";")) {
                    int qq = identificacaoOperador.getListaVeiculos().split(";").length;
                    identificacaoOperador.setQuantidadDeVeiculos(qq);
                }
            }

            int aa = 0;
            if(Objects.nonNull(identificacaoOperador.getListaOperadores())){
                if(identificacaoOperador.getListaOperadores().contains(";")) {
                    aa = identificacaoOperador.getListaOperadores().split(";").length;
                }
            }

            if(Objects.nonNull(identificacaoOperador.getListaMotoristas())){
                if(identificacaoOperador.getListaMotoristas().contains(";")) {
                    aa = aa + identificacaoOperador.getListaMotoristas().split(";").length;
                }
            }

            identificacaoOperador.setQuantidadeOperadorMotorista(aa);
        }

        return result;
    }


    public List<MotoristaOperadorVO> buscarMotoristaOperador(Integer idEmpresa, Integer idUnidade) {
        List<MotoristaOperadorVO> lista = new ArrayList<>();
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = " select id, nome || ' ' || sobrenome as nome, categoria_cnh, ibutton, 'Motorista' as funcao, 'M' as tipo from motoristas m " +
                    "where empresa_id = ? aa and situacao = 1 and ibutton is not null " +
                    "union " +
                    "select id, nome as nome, 'NSA', ibutton, funcao, 'O' as tipo from operador o2  " +
                    "where empresa_id = ? bb and status = true and ibutton is not null; ";
            if(Objects.nonNull(idUnidade) && idUnidade > 0){
                sql = sql.replaceAll("aa", " and unidades_id =  "+ idUnidade);
                sql = sql.replaceAll("bb", " and unidade_id =  "+ idUnidade);
            }else{
                sql = sql.replaceAll("aa", "  ");
                sql = sql.replaceAll("bb", "  ");
            }

            connection = DataSourcePostgres.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idEmpresa);
            preparedStatement.setInt(2, idEmpresa);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String nome = resultSet.getString("nome");
                String categoria = resultSet.getString("categoria_cnh");
                String ibutton = resultSet.getString("ibutton");
                String funcao = resultSet.getString("funcao");
                String tipo = resultSet.getString("tipo");

                MotoristaOperadorVO vo = new MotoristaOperadorVO();
                vo.setCategoriaCnh(categoria);
                vo.setFuncao(funcao);
                vo.setIbutton(ibutton);
                vo.setNome(nome);
                vo.setTipo(tipo);
                vo.setId(id);
                lista.add(vo);
            }
        } catch (Exception e) {
            LogSistema.logError("buscarMotoristaOperador", e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarMotoristaOperador", ex);
            }
        }
        return lista;
    }


    public List<VeiculoAutorizacaoVO> buscarVeiculoAutorizacao(Integer idEmpresa, Integer idUnidade) {
        List<VeiculoAutorizacaoVO> lista = new ArrayList<>();
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select v2.id, v2.placa, e2.imei, m2.fabricante as fv, m2.marca as mv, m3.fabricante as fe, m3.marca as me, v2.id_unidade " +
                    " from cliente_veiculo cv " +
                    " inner join veiculo v2 on cv.veiculo = v2.id " +
                    " inner join equipamento e2 on v2.equipamento = e2.id " +
                    " inner join modelo m2 on v2.modelo = m2.id " +
                    " inner join modelo m3 on e2.modelo = m3.id " +
                    " where cv.cliente = ? aa and v2.validar_ibotton; ";
            if(Objects.nonNull(idUnidade) && idUnidade > 0){
                sql = sql.replaceAll("aa", " and v2.id_unidade =  "+ idUnidade);
            }else{
                sql = sql.replaceAll("aa", "  ");
            }

            connection = DataSourcePostgresOrion.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idEmpresa);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String placa = resultSet.getString("placa");
                String imei = resultSet.getString("imei");
                String fabricanteVeiculo = resultSet.getString("fv");
                String modeloveiculo = resultSet.getString("mv");
                String fabricanteEquipamento = resultSet.getString("me");
                String modeloEquipamento = resultSet.getString("fe");

                VeiculoAutorizacaoVO vo = new VeiculoAutorizacaoVO();
                vo.setPlaca(placa);
                vo.setImei(imei);
                vo.setFabricanteVeiculo(fabricanteVeiculo);
                vo.setModeloVeiculo(modeloveiculo);
                vo.setFabricanteEquipamento(fabricanteEquipamento);
                vo.setModeloEquipamento(modeloEquipamento);
                vo.setId(id);
                lista.add(vo);
            }
        } catch (Exception e) {
            LogSistema.logError("buscarVeiculoAutorizacao", e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarVeiculoAutorizacao", ex);
            }
        }
        return lista;
    }

}
