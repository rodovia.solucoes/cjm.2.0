package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.Imagem;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;

/**
 * @author analista.ti
 */
public class ImagemDAO extends GenericDAO<Integer, Imagem> {

    public ImagemDAO(EntityManager entityManager) {
        super(entityManager);
    }

    public List<Imagem> findAll() {
        List<Imagem> result = null;
        try {
            Query q = entityManager.createQuery("SELECT i FROM imagem i");
            result = q.getResultList();
        } catch (NoResultException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public Integer saveWithId(Imagem entity) {
        Query sql = entityManager.createNativeQuery("INSERT INTO imagem (dados) VALUES (:dados) RETURNING id;");
        sql.setParameter("dados", entity.getDados());
        Integer id = (Integer) sql.getSingleResult();
        return id;
    }

}
