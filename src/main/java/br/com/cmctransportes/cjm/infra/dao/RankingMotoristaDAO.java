package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.RankingMotorista;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.singleton.MongoDBConexao;
import br.com.cmctransportes.cjm.utils.TimeHelper;
import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RankingMotoristaDAO extends GenericDAO<Integer, RankingMotorista> {

    public RankingMotoristaDAO(EntityManager entityManager) {
        super(entityManager);
    }


    public List<RankingMotorista> buscarListaDeRankingMotorista(Integer idMotorista) {
        List<RankingMotorista> rankingMotoristas = null;
        try {
            String sql = "FROM RankingMotorista WHERE idMotorista = :idMotorista";
            Query query = entityManager.createQuery(sql).setParameter("idMotorista", idMotorista);
            rankingMotoristas = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return rankingMotoristas;
    }


    public List<RankingMotorista> buscarListaDeRankingMotorista(Integer idMotorista, Date dataInicial, Date dataFinal) {
        List<RankingMotorista> rankingMotoristas = null;
        try {
            dataInicial = TimeHelper.getDate000000(dataInicial);
            dataFinal = TimeHelper.getDate235959(dataFinal);

            String sql = "FROM RankingMotorista r WHERE r.idMotorista = :idMotorista AND (r.dataCadastro BETWEEN :dataInicial and :dataFinal) ";
            Query query = entityManager.createQuery(sql).setParameter("idMotorista", idMotorista).setParameter("dataInicial", dataInicial).setParameter("dataFinal", dataFinal);
            rankingMotoristas = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return rankingMotoristas;
    }

    public List<RankingMotorista> buscarRankinkPelaTramissaoMongo(){
        List<RankingMotorista> lista = new ArrayList<>();
        try {
            MongoDatabase mdb = MongoDBConexao.getMongoDBConexao().pegarBaseRodovia();
            MongoCollection<Document> coll = mdb.getCollection("ranking_motorista");

            MongoCursor<Document> cursor = coll.find().iterator();

            while (cursor.hasNext()) {
                Document dd = cursor.next();
                RankingMotorista rankingMotorista = new RankingMotorista();
                if (dd.containsKey("id")) {
                    rankingMotorista.setId(dd.getInteger("id"));
                }

                if (dd.containsKey("idTransmissao")) {
                    rankingMotorista.setIdTransmissao(dd.getInteger("idTransmissao"));
                }
            }
        }catch (Exception e){
            LogSistema.logError("buscarTransmissaoMongo",e);
        }
        return lista;
    }

}
