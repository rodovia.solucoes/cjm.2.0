package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.OperadorVeiculo;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class OperadorVeiculoDAO extends GenericDAO<Integer, OperadorVeiculo> {

    public OperadorVeiculoDAO(EntityManager entityManager) {
        super(entityManager);
    }


    public  List<OperadorVeiculo>  buscarPeloOperador(Integer operador) {

        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT mv FROM OperadorVeiculo mv ");
            sql.append(" WHERE mv.operadorId = :operadorId ");
            sql.append(" ORDER BY mv.dataInicio DESC");

            Query query = entityManager.createQuery(sql.toString(), OperadorVeiculo.class)
                    .setParameter("operadorId", operador);

            List<OperadorVeiculo> lista = query.getResultList();
            return lista;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

}
