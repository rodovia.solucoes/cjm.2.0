package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.Contratos;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgres;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgresOrion;
import org.joda.time.DateTime;
import org.joda.time.Days;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.sql.*;
import java.util.*;
import java.util.Date;

public class ContratosDAO extends GenericDAO<Integer, Contratos> {

    public ContratosDAO(EntityManager entityManager) {
        super(entityManager);
        this.entityManager = entityManager;
    }


    public List<Contratos> buscarPorEmpresa(Integer idEmpresa) throws Exception {
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT c FROM Contratos c ");
            sql.append(" WHERE c.empresa = :empresa ");
            sql.append(" ORDER BY c.id ASC");

            Query query = entityManager.createQuery(sql.toString(), Contratos.class)
                    .setParameter("empresa", idEmpresa);

            List<Contratos> contratos = query.getResultList();
            return contratos;
        } catch (Exception e) {
            throw e;
        }
    }


    public List<Contratos> buscarPorItem(Integer item) throws Exception {

        if (item == 1) {
            return buscarPorItemFaturando(item);
        } else {
            try {
                StringBuilder sql = new StringBuilder();
                sql.append("SELECT c FROM Contratos c ");
                sql.append(" WHERE c.item = :item ");
                sql.append(" and c.dataInicio is not null ");
                sql.append(" and c.dataFinal is null ");
                sql.append(" ORDER BY c.dataInicio ASC");

                Query query = entityManager.createQuery(sql.toString(), Contratos.class)
                        .setParameter("item", item);

                List<Contratos> contratos = query.getResultList();
                return contratos;
            } catch (Exception e) {
                throw e;
            }
        }
    }

    public void prepararCancelamento(Integer idEmpresa) throws Exception {
        Connection connection = null;
        Statement stmt = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DataSourcePostgres.getInstance().getConnection();
            connection.setAutoCommit(false);
            stmt = connection.createStatement();
            String sql = "update contratos set data_final = null where id = " + idEmpresa;
            stmt.executeUpdate(sql);
            connection.commit();
        } catch (Exception e) {
            throw e;
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("prepararCancelamento", ex);
            }
        }
    }


    public List<Contratos> buscarPorItemFaturando(Integer item) throws Exception {
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT c FROM Contratos c ");
            sql.append(" WHERE c.item = :item ");
            sql.append(" and c.dataInicio is not null ");
            sql.append(" and c.dataFinal is not null ");
            sql.append(" ORDER BY c.dataInicio ASC");

            Query query = entityManager.createQuery(sql.toString(), Contratos.class)
                    .setParameter("item", item);

            List<Contratos> contratos = query.getResultList();
            return contratos;
        } catch (Exception e) {
            throw e;
        }
    }

    /*
     * 1 - Faturando
     * 2 - Implantando
     * 3 - Inadimplente
     * 4 - Desenvolvendo
     * 5 - Nova licença
     * 6 - Treinamento
     */

    public Contratos calcularContratos() throws Exception {

        int faturando = 0;
        int implantando = 0;
        int inadimplente = 0;
        int desenvolvendo = 0;
        int novalicenca = 0;
        int treinamento = 0;
        int cancelado = 0;
        int teste = 0;


        int implantandoAtrasado = 0;
        int inadimplenteAtrado = 0;
        int desenvolvendoAtrasado = 0;
        int novalicencaAtrasado = 0;
        int canceladoAtrasado = 0;
        int testeAtrasado = 0;


        try {
            List<Integer> lista = buscarEmpresas();
            for (Integer id : lista) {
                if (estaFaturando(id)) {
                    faturando++;
                }
                Contratos contratos = ultimoStatus(id);
                if(Objects.nonNull(contratos)){
                    switch (contratos.getItem()) {
                        case 6:
                        case 2: {
                            implantando++;
                            if (isAtrasado(contratos)) {
                                implantandoAtrasado++;
                            }
                        }
                        break;
                        case 3: {
                            inadimplente++;
                            if (isAtrasado(contratos)) {
                                inadimplenteAtrado++;
                            }
                        }
                        break;
                        case 4: {
                            desenvolvendo++;
                            if (isAtrasado(contratos)) {
                                desenvolvendoAtrasado++;
                            }
                        }
                        break;
                        case 5: {
                            novalicenca++;
                            if (isAtrasado(contratos)) {
                                novalicencaAtrasado++;
                            }
                        }
                        break;
                        case 7: {
                            cancelado++;
                            if (isAtrasado(contratos)) {
                                canceladoAtrasado++;
                            }
                        }break;
                        case 8: {
                            teste++;
                            if (isAtrasado(contratos)) {
                                testeAtrasado++;
                            }
                        }
                        break;
                    }
                }

            }

            Contratos contratos = new Contratos();
            contratos.setTotalFaturando(faturando);
            contratos.setTotalImplantando(implantando);
            contratos.setTotalInadimplente(inadimplente);
            contratos.setTotalDesenvolvendo(desenvolvendo);
            contratos.setTotalNovaLicenca(novalicenca);
            contratos.setTotalTreinamento(treinamento);
            contratos.setTotalCancelado(cancelado);
            contratos.setTeste(teste);


            contratos.setImplantandoAtrasado(implantandoAtrasado);
            contratos.setInadimplenteAtrado(inadimplenteAtrado);
            contratos.setDesenvolvendoAtrasado(desenvolvendoAtrasado);
            contratos.setNovalicencaAtrasado(desenvolvendoAtrasado);
            contratos.setCanceladoAtrasado(canceladoAtrasado);
            contratos.setTesteAtrasado(testeAtrasado);

            return contratos;

        } catch (Exception e) {
            throw e;
        }

    }

    private List<Integer> buscarEmpresas() throws Exception {
        List<Integer> lista = new ArrayList<>();
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select distinct empresa from contratos;";
            connection = DataSourcePostgres.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Integer id = resultSet.getInt("empresa");
                lista.add(id);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarLocais", ex);
            }
        }
        return lista;
    }


    private boolean estaFaturando(Integer idEmpresa) throws Exception {
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select count(*) from contratos where empresa = ? and item = 1 and data_inicio is not null and data_final is not null;";
            connection = DataSourcePostgres.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idEmpresa);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                Integer id = resultSet.getInt("count");
                if (id > 0) {
                    return true;
                }

            }
        } catch (Exception e) {
            throw e;
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("estaFaturando", ex);
            }
        }
        return false;
    }

    private Contratos ultimoStatus(Integer idEmpresa) throws Exception {
        Contratos contratos = null;
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select item, data_inicio, expectativa_inicio, expectativa_fim from contratos where empresa = ? and item <> 1 and data_inicio is not null and data_final is null order by id limit 1;";
            connection = DataSourcePostgres.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idEmpresa);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                Integer id = resultSet.getInt("item");
                Date dataInicio = resultSet.getTimestamp("data_inicio");
                Date expInicio = resultSet.getTimestamp("expectativa_inicio");
                Date expFinal = resultSet.getTimestamp("expectativa_fim");
                contratos = new Contratos();
                contratos.setItem(id);
                contratos.setDataInicio(dataInicio);
                contratos.setExpectativaInicio(expInicio);
                contratos.setExpectativaFim(expFinal);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("ultimoStatus", ex);
            }
        }
        return contratos;
    }


    public List<Contratos> buscarListaPorItem(Integer item) throws Exception {
        try {
            List<Contratos> ll = null;
            List<Contratos> lista = new ArrayList<>();
            List<Contratos> listaDeContratos = buscarPorItem(item);
            List<Integer> listaIdEmpresa = buscarEmpresas();
            for (Integer id : listaIdEmpresa) {
                ll = new ArrayList<>();
                for (Contratos c : listaDeContratos) {
                    if (c.getEmpresa().equals(id)) {
                        ll.add(c);
                    }
                }

                if (ll.size() > 1) {
                    Contratos ct = Collections.max(ll, Comparator.comparing(Contratos::getDataInicio));
                    lista.add(ct);
                } else {
                    if (ll.size() == 1) {
                        lista.add(ll.get(0));
                    }
                }
            }

            buscarNomeEmpresaCJM(lista);
            buscarNomeEmpresaOrion(lista);
            return lista;
        } catch (Exception ex) {
            throw ex;
        }
    }


    private void buscarNomeEmpresaCJM(List<Contratos> lista) throws Exception {
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select nome from empresas e where id = ?;";
            connection = DataSourcePostgres.getInstance().getConnection();
            for (Contratos contratos : lista) {
                if ((Objects.isNull(contratos.getTipoSistema())) || (Objects.nonNull(contratos.getTipoSistema()) && !contratos.getTipoSistema().equalsIgnoreCase("ORION"))) {
                    preparedStatement = connection.prepareStatement(sql);
                    preparedStatement.setInt(1, contratos.getEmpresa());
                    resultSet = preparedStatement.executeQuery();
                    if (resultSet.next()) {
                        String nome = resultSet.getString("nome");
                        contratos.setNomeEmpresa(nome);
                    }
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("estaFaturando", ex);
            }
        }
    }


    private void buscarNomeEmpresaOrion(List<Contratos> lista) throws Exception {
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select nome from cliente e where id = ?;";
            connection = DataSourcePostgresOrion.getInstance().getConnection();
            for (Contratos contratos : lista) {
                if ((Objects.nonNull(contratos.getTipoSistema()) && contratos.getTipoSistema().equalsIgnoreCase("ORION"))) {
                    preparedStatement = connection.prepareStatement(sql);
                    preparedStatement.setInt(1, contratos.getEmpresa());
                    resultSet = preparedStatement.executeQuery();
                    if (resultSet.next()) {
                        String nome = resultSet.getString("nome");
                        contratos.setNomeEmpresa(nome);
                    }
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("estaFaturando", ex);
            }
        }
    }


    private boolean isAtrasado(Contratos contratos) {
        try {
            if (Objects.nonNull(contratos.getDataInicio()) &&
                    Objects.nonNull(contratos.getExpectativaInicio()) &&
                    Objects.nonNull(contratos.getExpectativaFim())) {
                int dif = contratos.getExpectativaFim().compareTo(new Date());
                if (dif < 0)  {
                    return true;
                }
            }
        } catch (Exception e) {
            LogSistema.logError("isAtrasado", e);
        }
        return false;
    }

}
