package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.Turnos;
import br.com.cmctransportes.cjm.logger.LogSistema;

import javax.persistence.EntityManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * @author analista.ti
 */
public class TurnosDAO extends GenericDAO<Integer, Turnos> {

    public TurnosDAO(EntityManager entityManager) {
        super(entityManager);
    }

}
