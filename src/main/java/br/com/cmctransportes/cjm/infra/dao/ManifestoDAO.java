package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.Manifesto;

import javax.persistence.EntityManager;

public class ManifestoDAO extends GenericDAO<Integer, Manifesto> {

    public ManifestoDAO(EntityManager entityManager) {
        super(entityManager);
    }

}
