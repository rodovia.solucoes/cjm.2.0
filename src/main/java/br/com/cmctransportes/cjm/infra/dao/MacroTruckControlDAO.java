package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.MacroTruckControl;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgres;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MacroTruckControlDAO {

    public MacroTruckControl buscarMacro(Integer macroTrucksControl){
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from macro_trucks_control where trucks_controle = ?;";
            connection = DataSourcePostgres.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, macroTrucksControl);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                Integer idEmpresa = resultSet.getInt("id_empresa");
                Integer trucksControle = resultSet.getInt("trucks_controle");
                Integer rodovia = resultSet.getInt("rodovia");
                Boolean ativo = resultSet.getBoolean("ativo");
                String descricao = resultSet.getString("descricao");
                String mensagem = resultSet.getString("mensagem");

                MacroTruckControl  control = new MacroTruckControl();
                control.setAtivo(ativo);
                control.setDescricao(descricao);
                control.setIdEmpresa(idEmpresa);
                control.setTrucksControle(trucksControle);
                control.setRodovia(rodovia);
                control.setMensagem(mensagem);

                return control;
            }
        }catch (Exception e){
            LogSistema.logError("buscarMacro: " + macroTrucksControl + " - ", e);
        }finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarMacro", ex);
            }
        }
        return null;
    }
}
