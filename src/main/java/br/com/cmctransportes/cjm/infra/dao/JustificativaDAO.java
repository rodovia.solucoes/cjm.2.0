package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.Justificativa;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgres;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgresOrion;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class JustificativaDAO extends GenericDAO<Integer, Justificativa>  {

    public JustificativaDAO(EntityManager entityManager) {
        super(entityManager);
    }


    public List<Justificativa> buscarPorJornada(Integer idJornada){
        List<Justificativa> lista = new ArrayList<>();
        try {
            Connection connection = null;
            ResultSet resultSet = null;
            PreparedStatement preparedStatement = null;
            try {
                String sql = "SELECT tipo, data_cadastro, dados from justificativa where id_jornada = ?";
                connection = DataSourcePostgres.getInstance().getConnection();
                preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setInt(1, idJornada);
                resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    Justificativa justificativa = new Justificativa();
                    justificativa.setTipo(resultSet.getInt("tipo"));
                    justificativa.setDataCadastro(new Date(resultSet.getTimestamp("data_cadastro").getTime()));
                    justificativa.setDados(resultSet.getString("dados"));
                    lista.add(justificativa);
                }
            }catch (Exception e){
                LogSistema.logError("buscarVeiculoPelaPlaca", e);
            }finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    if (preparedStatement != null) {
                        preparedStatement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException ex) {
                    LogSistema.logError("buscarVeiculoPelaPlaca", ex);
                }

            }
        }catch (Exception e){
            LogSistema.logError("buscarPorJornada", e);
        }
        return lista;
    }
}
