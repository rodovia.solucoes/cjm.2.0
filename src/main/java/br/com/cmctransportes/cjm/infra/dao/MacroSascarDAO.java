package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.*;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgres;

import javax.persistence.NoResultException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MacroSascarDAO {

    public  List<MacroSascar> buscar(){
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            List<MacroSascar> lista = new ArrayList<>();
            StringBuilder sql = new StringBuilder();
            sql.append("select id_empresa, sascar, rodovia, mensagem  ");
            sql.append(" from macro_sascar ");
            sql.append(" where ativo = true and sascar != -100;");
            connection = DataSourcePostgres.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql.toString());
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                MacroSascar macroSascar = new MacroSascar();
                Integer idEmpresa = resultSet.getInt("id_empresa");
                macroSascar.setIdEmpresa(idEmpresa);
                Integer sascar = resultSet.getInt("sascar");
                macroSascar.setIdSascar(sascar);
                Integer rodovia = resultSet.getInt("rodovia");
                macroSascar.setIdRodovia(rodovia);
                String mensagem = resultSet.getString("mensagem");
                macroSascar.setMensagem(mensagem);
                lista.add(macroSascar);
            }
            return lista;
        } catch (NoResultException nre) {
            nre.printStackTrace();
        } catch (Exception except) {
            except.printStackTrace();
        }finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscar", ex);
            }

        }

        return null;
    }

}
