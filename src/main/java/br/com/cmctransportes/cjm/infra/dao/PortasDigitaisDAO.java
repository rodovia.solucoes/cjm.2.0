package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.vo.PortasDigitaisVO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.singleton.MongoDBConexao;
import com.mongodb.BasicDBObject;
import com.mongodb.QueryBuilder;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PortasDigitaisDAO {

    public List<PortasDigitaisVO> buscarLista(Integer idVeiculo){
        List<PortasDigitaisVO> lista = new ArrayList<>();
        try {
            MongoDatabase mdb = MongoDBConexao.getMongoDBConexao().pegarBaseRodovia();
            MongoCollection<Document> coll = mdb.getCollection("portas_digitais");

            QueryBuilder qb = new QueryBuilder();
            qb.and(new QueryBuilder().put("idVeiculo").is(idVeiculo).get());

            BasicDBObject basicDBObject = new BasicDBObject();
            basicDBObject.putAll(qb.get());
            MongoCursor<Document> cursor = coll.find(basicDBObject).sort(new Document("_id", -1)).limit(10).iterator();
            while (cursor.hasNext()) {
                Document dd = cursor.next();
                PortasDigitaisVO telemetriaVO = convert(dd);
                lista.add(telemetriaVO);
            }
        }catch (Exception e){
            LogSistema.logError("buscarLista", e);
        }
        return lista;
    }

    private PortasDigitaisVO convert(Document document){
        PortasDigitaisVO portasDigitaisVO = new PortasDigitaisVO();
        try{
            if (document.containsKey("dataCriacao")) {
                Date d = document.getDate("dataCriacao");
                portasDigitaisVO.setDataCriacao(new SimpleDateFormat("dd/MM HH:mm:ss").format(d));
            }

            if (document.containsKey("descricao")) {
                portasDigitaisVO.setDescricao(document.getString("descricao"));
            }

        }catch (Exception e){
            LogSistema.logError("convert", e);
        }
        return portasDigitaisVO;
    }

}
