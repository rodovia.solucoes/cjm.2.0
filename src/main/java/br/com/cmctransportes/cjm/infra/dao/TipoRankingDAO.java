package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.TipoRanking;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TipoRankingDAO extends GenericDAO<Integer, TipoRanking>  {
    public TipoRankingDAO(EntityManager entityManager) {
        super(entityManager);
    }

    public List<TipoRanking> buscarLista(Integer idCliente, Integer idUnidade) throws Exception{
        List<TipoRanking> result = new ArrayList<>();
        String sql = "FROM TipoRanking t  WHERE  t.cliente = :idCliente";
        if(Objects.nonNull(idUnidade) && idUnidade > 0){
            sql = sql + " and t.unidade = :idUnidade";
        }
        Query query = entityManager.createQuery(sql);
        query.setParameter("idCliente", idCliente);
        if(Objects.nonNull(idUnidade) && idUnidade > 0){
            query.setParameter("unidade", idUnidade);
        }
        List<TipoRanking> lista = query.getResultList();

        return lista;

    }
}
