package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.ControleCombustivel;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;


public class ControleCombustivelDAO extends GenericDAO<Integer, ControleCombustivel>  {
    private EntityManager em;

    public ControleCombustivelDAO(EntityManager entityManager) {
        super(entityManager);
        this.em = entityManager;
    }

    public List<ControleCombustivel> findAll(Integer idVeiculo) {
        Query q = entityManager.createQuery("FROM ControleCombustivel o WHERE o.idVeiculo = :idVeiculo  order by dataCadastro desc ");
        q.setParameter("idVeiculo", idVeiculo);
        List<ControleCombustivel> result = q.getResultList();
        return result;
    }

}
