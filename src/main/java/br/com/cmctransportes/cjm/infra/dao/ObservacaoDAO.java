package br.com.cmctransportes.cjm.infra.dao;


import br.com.cmctransportes.cjm.domain.entities.Observacao;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class ObservacaoDAO extends GenericDAO<Integer, Observacao> {

    public ObservacaoDAO(EntityManager entityManager) {
        super(entityManager);
    }

    public List<Observacao> buscarLista(Integer idMotorista) throws Exception{
        List<Observacao> result = new ArrayList<>();
        Query query = entityManager.createQuery("FROM Observacao p  WHERE  p.idMotorista = :motorista" );
        query.setParameter("motorista", idMotorista);
        List<Observacao> lista = query.getResultList();
        return lista;

    }

}
