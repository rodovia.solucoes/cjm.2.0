package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.Empresas;
import br.com.cmctransportes.cjm.domain.entities.EmpresasPermissao;
import br.com.cmctransportes.cjm.domain.entities.Permissao;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class PermissaoDAO extends GenericDAO<Integer, Permissao> {

    public PermissaoDAO(EntityManager entityManager) {
        super(entityManager);
    }


    public List<Permissao> buscarTodos() throws Exception{
        Query q = entityManager.createQuery("FROM Permissao p ORDER BY p.nome" );
        List<Permissao> result = q.getResultList();
        return result;
    }

    public List<Permissao> buscarListaPermissao(Empresas empresas) throws Exception{
        List<Permissao> result = new ArrayList<>();
        Query query = entityManager.createQuery("FROM EmpresasPermissao p  WHERE  p.empresas = :empresas" );
        query.setParameter("empresas", empresas);
        List<EmpresasPermissao> lista = query.getResultList();
        for(EmpresasPermissao empresasPermissao : lista){
            result.add(empresasPermissao.getPermissao());
        }
        return result;

    }

    @Transactional
    public void ajustarPermissoes(Empresas empresas){
        try{
            //primeiro limpa as permissoes ja cadastradas
            excluir(empresas);
            if(empresas.getListDePermissao() != null){
                for(Permissao permissao: empresas.getListDePermissao()){
                    EmpresasPermissao empresasPermissao = new EmpresasPermissao();
                    empresasPermissao.setEmpresas(empresas);
                    empresasPermissao.setPermissao(permissao);
                    entityManager.persist(empresasPermissao);
                }
                entityManager.flush();
            }

        }catch (Exception e){

        }

    }

    @Transactional
    public void excluir(Empresas empresas){
        try {
            String hql = "delete from EmpresasPermissao where empresas = :empresas";
            Query query = entityManager.createQuery(hql);
            query.setParameter("empresas", empresas);
            int r = query.executeUpdate();
            if(r != -1){
                entityManager.flush();}
        }catch (Exception e){

        }

    }
}
