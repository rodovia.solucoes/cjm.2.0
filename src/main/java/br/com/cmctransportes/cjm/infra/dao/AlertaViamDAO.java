package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.AlertaViam;

import javax.persistence.EntityManager;

public class AlertaViamDAO extends GenericDAO<Integer, AlertaViam>  {

    private EntityManager em;

    public AlertaViamDAO(EntityManager entityManager) {
        super(entityManager);
        this.em = entityManager;
    }

}
