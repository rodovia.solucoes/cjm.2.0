package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.MotoristaStatus;

import javax.persistence.EntityManager;

/**
 * @author analista.ti
 */
public class MotoristaStatusDAO extends GenericDAO<Integer, MotoristaStatus> {

    public MotoristaStatusDAO(EntityManager entityManager) {
        super(entityManager);
    }

}
