package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.FotoViaM;
import br.com.cmctransportes.cjm.domain.entities.Motoristas;
import br.com.cmctransportes.cjm.logger.LogSistema;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class FotoViaMDAO extends GenericDAO<Integer, FotoViaM>  {

    private EntityManager em;

    public FotoViaMDAO(EntityManager entityManager) {
        super(entityManager);
        this.em = entityManager;
    }



    public Integer contarFotosNaoVistas(Motoristas motoristas){
        try{
            Query q = entityManager.createQuery("select count(*) from FotoViaM f where f.motoristas = :motoristas and f.visualizadaNodashboard = :visualizadaNodashboard ");
            q.setParameter("motoristas", motoristas);
            q.setParameter("visualizadaNodashboard", Boolean.FALSE);
            Object o = q.getSingleResult();
            return ((Long)o).intValue();
        }catch (Exception e){
            LogSistema.logError("contarFotosNaoVistas", e);
        }
        return 0;
    }


    public List<FotoViaM> buscarFotosNaoVistas (Motoristas motoristas){
        try{
            Query query = entityManager.createQuery("FROM FotoViaM p  WHERE  p.motoristas = :motorista AND p.visualizadaNodashboard = :visualizadaNodashboard" );
            query.setParameter("motorista", motoristas);
            query.setParameter("visualizadaNodashboard", Boolean.FALSE);
            List<FotoViaM> lista = query.getResultList();
            return lista;
        }catch (Exception e){
            LogSistema.logError("buscarFotosNaoVistas", e);
        }
        return null;
    }

}
