package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.EventoMacro;
import br.com.cmctransportes.cjm.domain.entities.Macros;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class MacrosDAO extends GenericDAO<Integer, Macros> {

    private EventoMacroDAO eventoMacroDAO;

    public MacrosDAO(EntityManager entityManager) {

        super(entityManager);
        eventoMacroDAO = new EventoMacroDAO(entityManager);
    }

    public void salvar(Macros macros) throws Exception {
        this.save(macros);
        for (EventoMacro eventoMacro : macros.getListaDeEventos()) {
            eventoMacro.setIdMacro(macros.getId());
            eventoMacroDAO.save(eventoMacro);
        }
    }

    public void editar(Macros macros) throws Exception {
        this.update(macros);
        List<EventoMacro> listaEvento = this.eventoMacroDAO.buscarListaDeMacros(macros);
        for (EventoMacro eventoMacro : listaEvento) {
            this.eventoMacroDAO.delete(eventoMacro);
        }
        for (EventoMacro eventoMacro : macros.getListaDeEventos()) {
            eventoMacro.setId(null);
            eventoMacro.setIdMacro(macros.getId());
            eventoMacroDAO.save(eventoMacro);
        }
    }


    public List<Macros> buscarLista(Integer idEmpresa) throws Exception {
        Query q = entityManager.createQuery("FROM Macros o WHERE empresa = :idEmpresa");
        q.setParameter("idEmpresa", idEmpresa);
        List<Macros> result = q.getResultList();
        for (Macros macros : result) {
            buscarEventos(macros);
        }
        return result;
    }



    public Macros buscarPeloId(Integer id) {
        Macros macros = this.getById(id);
        buscarEventos(macros);
        return macros;
    }


    private void buscarEventos(Macros macros) {
        List<EventoMacro> lista = this.eventoMacroDAO.buscarListaDeMacros(macros);
        macros.setListaDeEventos(lista);
    }


}
