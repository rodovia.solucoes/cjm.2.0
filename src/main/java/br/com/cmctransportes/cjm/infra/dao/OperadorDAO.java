package br.com.cmctransportes.cjm.infra.dao;


import br.com.cmctransportes.cjm.domain.entities.Operador;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class OperadorDAO  extends GenericDAO<Integer, Operador>  {

    public OperadorDAO(EntityManager entityManager) {
        super(entityManager);
    }

    public List<Operador> findAll(Integer idEmpresa) {
        Query q = entityManager.createQuery("FROM Operador o WHERE o.empresa.id = :idEmpresa");
        q.setParameter("idEmpresa", idEmpresa);
        List<Operador> result = q.getResultList();
        return result;
    }

    public List<Operador> findAll(Integer idEmpresa, Integer idUnidade) {
        String sql = "FROM Operador o WHERE o.empresa.id = :idEmpresa ";
        if(idUnidade > 0){
            sql = sql + "AND o.unidadeId = :idUnidade";
        }
        Query q = entityManager.createQuery(sql);
        q.setParameter("idEmpresa", idEmpresa);
        if(idUnidade > 0) {
            q.setParameter("idUnidade", idUnidade);
        }
        List<Operador> result = q.getResultList();
        return result;
    }
}
