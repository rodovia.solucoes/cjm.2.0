/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.UsersClaims;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 * @author Catatau
 */
public class UsersClaimsDAO extends GenericDAO<Integer, UsersClaims> {

    public UsersClaimsDAO(EntityManager entityManager) {
        super(entityManager);
    }


    public List<UsersClaims> findByUser(Integer user_id) {
        List<UsersClaims> result = null;

        try {
            String sql = "Select * FROM users_claims WHERE user_id = :user_id";
            Query query = entityManager.createNativeQuery(sql, UsersClaims.class)
                    .setParameter("user_id", user_id);
            result = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return result;
    }


}
