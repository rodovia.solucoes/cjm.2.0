package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.SascarVeiculo;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class SascarVeiculoDAO extends GenericDAO<Integer, SascarVeiculo>  {

    public SascarVeiculoDAO(EntityManager entityManager) {
        super(entityManager);
    }

    public List<SascarVeiculo> findAll(Integer idEmpresa) {
        Query q = entityManager.createQuery("FROM SascarVeiculo o WHERE o.empresa = :idEmpresa");
        q.setParameter("idEmpresa", idEmpresa);
        List<SascarVeiculo> result = q.getResultList();
        return result;
    }

    public List<SascarVeiculo> findAll() {
        Query q = entityManager.createQuery("FROM SascarVeiculo o where (manual = false or manual is null) and status = true");
        List<SascarVeiculo> result = q.getResultList();
        return result;
    }

    public List<SascarVeiculo> findAllManual(Integer idEmpresa) {
        Query q = entityManager.createQuery("FROM SascarVeiculo o where o.manual = true and o.empresa = :idEmpresa");
        q.setParameter("idEmpresa", idEmpresa);
        List<SascarVeiculo> result = q.getResultList();
        return result;
    }
}
