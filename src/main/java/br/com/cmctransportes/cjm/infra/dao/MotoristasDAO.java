package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.Empresas;
import br.com.cmctransportes.cjm.domain.entities.Motoristas;
import br.com.cmctransportes.cjm.domain.entities.Users;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgres;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


/**
 * @author analista.ti
 */
public class MotoristasDAO extends GenericDAO<Integer, Motoristas> {

    public MotoristasDAO(EntityManager entityManager) {
        super(entityManager);
    }

    public Motoristas getByUserId(Integer id) {
        Motoristas result = null;
        try {
            Query q = entityManager.createNamedQuery("Motoristas.findByUserId").setParameter("userId", id);
            result = (Motoristas) q.getSingleResult();
        } catch (NoResultException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public List<Motoristas> findAll(Integer idEmpresa) {
        Query q = entityManager.createQuery("FROM Motoristas t WHERE t.objEmpresa.id = :idEmpresa");
        q.setParameter("idEmpresa", idEmpresa);
        List<Motoristas> result = q.getResultList();
        return result;
    }

    public List<Motoristas> findAllIdSascar(Integer sascarId) {
        Query q = entityManager.createQuery("FROM Motoristas t WHERE t.sascarId = :sascarId");
        q.setParameter("sascarId", sascarId);
        List<Motoristas> result = q.getResultList();
        return result;
    }


    public List<Motoristas> findAll(Integer idEmpresa, Integer idUnidade) {
        Query q = entityManager.createQuery("FROM Motoristas t WHERE t.objEmpresa.id = :idEmpresa AND t.unidadesId = :idUnidade");
        q.setParameter("idEmpresa", idEmpresa);
        q.setParameter("idUnidade", idUnidade);
        List<Motoristas> result = q.getResultList();
        return result;
    }

    public List<Motoristas> findAllMinimo(Integer idEmpresa, Integer idUnidade) {
        Query q = entityManager.createQuery("SELECT NEW Motoristas(t.id, t.nome, t.sobrenome, t.situacao) FROM Motoristas t WHERE t.objEmpresa.id = :idEmpresa AND t.unidadesId = :idUnidade");
        q.setParameter("idEmpresa", idEmpresa);
        q.setParameter("idUnidade", idUnidade);
        List<Motoristas> result = q.getResultList();
        return result;
    }

    public List<Motoristas> findAllMinimo(Integer idEmpresa) {
        Query q = entityManager.createQuery("SELECT NEW Motoristas(t.id, t.nome, t.sobrenome, t.situacao) FROM Motoristas t WHERE t.objEmpresa.id = :idEmpresa ");
        q.setParameter("idEmpresa", idEmpresa);
        List<Motoristas> result = q.getResultList();
        return result;
    }


    public Motoristas findPeloIdMinimo(Integer idMotorista) {
        Query q = entityManager.createQuery("SELECT NEW Motoristas(t.id, t.nome, t.sobrenome, t.situacao, t.turnosId, t.unidadesId) FROM Motoristas t WHERE t.id = :idMotorista ");
        q.setParameter("idMotorista", idMotorista);
        List<Motoristas> result = q.getResultList();
        if (result.isEmpty()) {
            return null;
        }
        return result.get(0);
    }


    public Motoristas findPeloCpf(String cpf) {
        Query q = entityManager.createQuery("SELECT NEW Motoristas(t.id, t.nome, t.sobrenome, t.situacao, t.turnosId, t.unidadesId) FROM Motoristas t WHERE t.cpf = :cpf ");
        q.setParameter("cpf", cpf);
        List<Motoristas> result = q.getResultList();
        if (result.isEmpty()) {
            return null;
        }
        return result.get(0);
    }


    public List<Motoristas> findAllAtivos(Integer idEmpresa, Integer idUnidade) {
        String hql = "SELECT NEW Motoristas(t.id, t.turnosId) FROM Motoristas t WHERE t.objEmpresa.id = :idEmpresa  and t.situacao = :situacao ";
        if (idUnidade > 0) {
            hql = hql + " AND t.unidadesId = :idUnidade";
        }
        Query q = entityManager.createQuery(hql);
        q.setParameter("idEmpresa", idEmpresa);
        if (idUnidade > 0) {
            q.setParameter("idUnidade", idUnidade);
        }
        q.setParameter("situacao", 1);
        List<Motoristas> result = q.getResultList();
        return result;
    }


    public List<Motoristas> findAllRankingMotorista(Integer idEmpresa, Integer idUnidade) {
        String hql = "SELECT NEW Motoristas(t.id, t.nome, t.sobrenome) FROM Motoristas t WHERE t.objEmpresa.id = :idEmpresa  and t.situacao = :situacao ";
        if (idUnidade > 0) {
            hql = hql + " AND t.unidadesId = :idUnidade";
        }
        Query q = entityManager.createQuery(hql);
        q.setParameter("idEmpresa", idEmpresa);
        if (idUnidade > 0) {
            q.setParameter("idUnidade", idUnidade);
        }
        q.setParameter("situacao", 1);
        List<Motoristas> result = q.getResultList();
        return result;
    }

    public Motoristas buscarPeloUserId(Users users) {
        Query q = entityManager.createQuery("FROM Motoristas t WHERE t.user = :users");
        q.setParameter("users", users);
        List<Motoristas> result = q.getResultList();
        if (!result.isEmpty()) {
            return result.get(0);
        }
        return null;
    }


    public Motoristas buscarPeloNome(String nomeDoMotorista, Integer idEmpresa) {
        String nome = nomeDoMotorista.split("\\s+")[0];
        String sobrenome = nomeDoMotorista.split("\\s+")[1];
        Query q = entityManager.createQuery("FROM Motoristas t WHERE t.objEmpresa = :objEmpresa and lower(t.nome) like lower('%" + nome + "%') and lower(t.sobrenome) like lower('%" + sobrenome + "%')");
        q.setParameter("objEmpresa", new Empresas(idEmpresa));
        List<Motoristas> result = q.getResultList();
        if (!result.isEmpty()) {
            return result.get(0);
        }
        return null;
    }


    public List<Motoristas> findAllListForView(Integer idEmpresa, Integer idUnidade) {
        Query q = entityManager.createQuery("SELECT NEW Motoristas(" +
                "   t.id, " +
                "   t.nome, " +
                "   t.sobrenome, " +
                "   t.dataNascimento," +
                "   t.sexo, " +
                "   t.cpf, " +
                "   t.numeroCnh, " +
                "   t.categoriaCnh, " +
                "   t.validadeCnh, " +
                "   t.telefone, " +
                "   t.turnosId, " +
                "   t.unidadesId, " +
                "   t.dataDemissao, " +
                "   t.situacao " +
                " ) " +
                " FROM Motoristas t WHERE t.objEmpresa.id = :idEmpresa AND t.unidadesId = :idUnidade");
        q.setParameter("idEmpresa", idEmpresa);
        q.setParameter("idUnidade", idUnidade);
        List<Motoristas> result = q.getResultList();
        return result;
    }

    public List<Motoristas> findAllListForView(Integer idEmpresa) {
        Query q = entityManager.createQuery("SELECT NEW Motoristas(" +
                "   t.id, " +
                "   t.nome, " +
                "   t.sobrenome, " +
                "   t.dataNascimento," +
                "   t.sexo, " +
                "   t.cpf, " +
                "   t.numeroCnh, " +
                "   t.categoriaCnh, " +
                "   t.validadeCnh, " +
                "   t.telefone, " +
                "   t.turnosId, " +
                "   t.unidadesId, " +
                "   t.dataDemissao, " +
                "   t.situacao " +
                " ) " +
                " FROM Motoristas t WHERE t.objEmpresa.id = :idEmpresa ");
        q.setParameter("idEmpresa", idEmpresa);
        List<Motoristas> result = q.getResultList();
        return result;
    }
}
