package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.Tolerancias;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgres;

import javax.persistence.EntityManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author analista.ti
 */
public class ToleranciasDAO extends GenericDAO<Integer, Tolerancias> {
    public ToleranciasDAO(EntityManager entityManager) {
        super(entityManager);
    }

    public Long buscarToleranciaAlmoco(Integer idEmpresa) {
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        long toleranciaAlmoco = 0;
        try {
            String sql = "select hora_extra_almoco from tolerancias where empresa_id = ?;";
            connection = DataSourcePostgres.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idEmpresa);
            resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                toleranciaAlmoco = resultSet.getInt("hora_extra_almoco");
            }
        } catch (Exception e) {
            LogSistema.logError("buscarToleranciaAlmoco", e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarToleranciaAlmoco", ex);
            }
        }
        return toleranciaAlmoco;
    }

}
