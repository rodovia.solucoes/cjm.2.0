package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.Nonconformity;

import javax.persistence.EntityManager;

/**
 * @author William Leite
 */
public class NonconformityDAO extends GenericDAO<Integer, Nonconformity> {

    public NonconformityDAO(EntityManager entityManager) {
        super(entityManager);
    }
}