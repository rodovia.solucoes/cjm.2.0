package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.Viagem;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;

public class ViagemDAO extends GenericDAO<Integer, Viagem>  {


    private EntityManager em;

    public ViagemDAO(EntityManager entityManager) {
        super(entityManager);
        this.em = entityManager;
    }

    public List<Viagem> findAll(String placa) {
        Query q = entityManager.createQuery("FROM Viagem o WHERE o.placa = :placa  order by dataCadastro desc ");
        q.setParameter("placa", placa);
        List<Viagem> result = q.getResultList();
        return result;
    }

    public List<Viagem> findAll(String placa, Date dataInicial, Date dataFinal) {
        Query q = entityManager.createQuery("FROM Viagem o WHERE o.placa = :placa  AND inicioDaViagem >= :dataInicial and fimDaViagem <= :dataFinal ");
        q.setParameter("placa", placa);
        q.setParameter("dataInicial", dataInicial);
        q.setParameter("dataFinal", dataFinal);
        List<Viagem> result = q.getResultList();
        return result;
    }

}
