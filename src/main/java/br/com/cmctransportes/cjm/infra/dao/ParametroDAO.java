package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.Empresas;
import br.com.cmctransportes.cjm.domain.entities.Parametro;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgres;


import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.sql.*;
import java.util.List;

public class ParametroDAO extends GenericDAO<Integer, Parametro> {

    public ParametroDAO(EntityManager entityManager) {
        super(entityManager);
    }

    public int gravarParametrosnativo(Parametro parametro) {
        int retorno = 0;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "INSERT INTO parametro( " +
                    " id, tempo_direcao, tempo_jornada_finalizada, tempo_refeicao, repetir_alerta_tempo_direcao, repetir_alerta_tempo_jornada_finalizada, " +
                    " repetir_alerta_tempo_refeicao, inicio_tempo_direcao, jornada_trabalho_extrapolada, " +
                    " horas_extras_acima_duas_horas, horas_extras_acima_quatro_horas, refeicao_inferior_uma_hora, " +
                    " descanso_acima_onze_horas, descanso_inferior_onze_horas, horas_disposicao_acima_de_trinta_minutos, tempo_de_espera_acima_de_uma_hora, tempo_de_manutencao_acima_de_uma_hora, tempo_de_lanche_acima_de_quinze_minuto, em_fiscalizacao, sinistro, pista_interditada, habilitar_limite_refeicao, habilitar_controle_diarias, valor_diaria) " +
                    " VALUES (nextval('parametro_id_sequencia'), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";

            connection = DataSourcePostgres.getInstance().getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            if (parametro.getTempoDirecao() != null) {
                preparedStatement.setString(1, parametro.getTempoDirecao());
            } else {
                preparedStatement.setString(1, "00:00");
            }

            if (parametro.getTempoJornadaFinalizada() != null) {
                preparedStatement.setString(2, parametro.getTempoJornadaFinalizada());
            } else {
                preparedStatement.setString(2, "00:00");
            }

            if (parametro.getTempoRefeicao() != null) {
                preparedStatement.setString(3, parametro.getTempoRefeicao());
            } else {
                preparedStatement.setString(3, "00:00");
            }

            if (parametro.getRepetirAlertaTempoDirecao() != null) {
                preparedStatement.setBoolean(4, parametro.getRepetirAlertaTempoDirecao());
            } else {
                preparedStatement.setBoolean(4, Boolean.FALSE);
            }

            if (parametro.getRepetirAlertaTempoJornadaFinalizada() != null) {
                preparedStatement.setBoolean(5, parametro.getRepetirAlertaTempoJornadaFinalizada());
            } else {
                preparedStatement.setBoolean(5, Boolean.FALSE);
            }
            if (parametro.getRepetirAlertaTempoRefeicao() != null) {
                preparedStatement.setBoolean(6, parametro.getRepetirAlertaTempoRefeicao());
            } else {
                preparedStatement.setBoolean(6, Boolean.FALSE);
            }

            if (parametro.getInicioTempoDirecao() != null) {
                preparedStatement.setString(7, parametro.getInicioTempoDirecao());
            } else {
                preparedStatement.setString(7, "00:00");
            }

            if (parametro.getJornadaTrabalhoExtrapolada() != null) {
                preparedStatement.setBoolean(8, parametro.getJornadaTrabalhoExtrapolada());
            } else {
                preparedStatement.setBoolean(8, Boolean.FALSE);
            }

            if (parametro.getHorasExtrasAcimaDuasHoras() != null) {
                preparedStatement.setBoolean(9, parametro.getHorasExtrasAcimaDuasHoras());
            } else {
                preparedStatement.setBoolean(9, Boolean.FALSE);
            }

            if (parametro.getHorasExtrasAcimaQuatroHoras() != null) {
                preparedStatement.setBoolean(10, parametro.getHorasExtrasAcimaQuatroHoras());
            } else {
                preparedStatement.setBoolean(10, Boolean.FALSE);
            }

            if (parametro.getRefeicaoInferiorUmaHora() != null) {
                preparedStatement.setBoolean(11, parametro.getRefeicaoInferiorUmaHora());
            } else {
                preparedStatement.setBoolean(11, Boolean.FALSE);
            }

            if (parametro.getDescansoAcimaOnzeHoras() != null) {
                preparedStatement.setBoolean(12, parametro.getDescansoAcimaOnzeHoras());
            } else {
                preparedStatement.setBoolean(12, Boolean.FALSE);
            }

            if (parametro.getDescansoInferiorOnzeHoras() != null) {
                preparedStatement.setBoolean(13, parametro.getDescansoInferiorOnzeHoras());
            } else {
                preparedStatement.setBoolean(13, Boolean.FALSE);
            }

            if (parametro.getHorasDisposicaoAcimadeTrintaMinutos() != null) {
                preparedStatement.setString(14, parametro.getHorasDisposicaoAcimadeTrintaMinutos());
            } else {
                preparedStatement.setString(14, "00:00");
            }

            if (parametro.getTempoDeEsperaAcimaDeUmaHora() != null) {
                preparedStatement.setBoolean(15, parametro.getTempoDeEsperaAcimaDeUmaHora());
            } else {
                preparedStatement.setBoolean(15, Boolean.FALSE);
            }

            if (parametro.getTempoDeManutencaoAcimaDeUmaHora() != null) {
                preparedStatement.setBoolean(16, parametro.getTempoDeManutencaoAcimaDeUmaHora());
            } else {
                preparedStatement.setBoolean(16, Boolean.FALSE);
            }

            if (parametro.getTempoDeLancheAcimaDeQuinzeMinuto() != null) {
                preparedStatement.setBoolean(17, parametro.getTempoDeLancheAcimaDeQuinzeMinuto());
            } else {
                preparedStatement.setBoolean(17, Boolean.FALSE);
            }

            if (parametro.getEmFiscalizacao() != null) {
                preparedStatement.setBoolean(18, parametro.getEmFiscalizacao());
            } else {
                preparedStatement.setBoolean(18, Boolean.FALSE);
            }

            if (parametro.getSinistro() != null) {
                preparedStatement.setBoolean(19, parametro.getSinistro());
            } else {
                preparedStatement.setBoolean(19, Boolean.FALSE);
            }

            if (parametro.getPistaInterditada() != null) {
                preparedStatement.setBoolean(20, parametro.getPistaInterditada());
            } else {
                preparedStatement.setBoolean(20, Boolean.FALSE);
            }

            if (parametro.getHabilitarLimiteRefeicao() != null) {
                preparedStatement.setBoolean(21, parametro.getHabilitarLimiteRefeicao());
            } else {
                preparedStatement.setBoolean(21, Boolean.FALSE);
            }

            if (parametro.getHabilitarControleDeDiarias() != null) {
                preparedStatement.setBoolean(22, parametro.getHabilitarControleDeDiarias());
            } else {
                preparedStatement.setBoolean(22, Boolean.FALSE);
            }

            if (parametro.getValorDiaria() != null) {
                try {
                    String valor = parametro.getValorDiaria();
                    valor = valor.replaceAll("[^\\d.,]", "").replaceAll(",", ".");
                    preparedStatement.setDouble(23, Double.parseDouble(valor));
                } catch (Exception e) {
                    preparedStatement.setDouble(23, 0.00D);
                }

            } else {
                preparedStatement.setDouble(23, 0.00D);
            }


            preparedStatement.executeUpdate();
            connection.commit();
            try (ResultSet rs = preparedStatement.getGeneratedKeys()) {
                if (rs.next()) {
                    retorno = rs.getInt(1);
                }
            }

        } catch (Exception e) {
            LogSistema.logError("gravarEnderecoNativo", e);
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    LogSistema.logError("gravarEnderecoNativo", e);
                }
            }

            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LogSistema.logError("gravarEnderecoNativo", e);
                }
            }
        }
        return retorno;
    }

}
