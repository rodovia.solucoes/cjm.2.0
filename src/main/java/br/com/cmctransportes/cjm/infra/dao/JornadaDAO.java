package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.*;
import br.com.cmctransportes.cjm.domain.entities.enums.EVENT_TYPE;
import br.com.cmctransportes.cjm.domain.entities.enums.JOURNEY_TYPE;
import br.com.cmctransportes.cjm.domain.entities.jornada.Resumo;
import br.com.cmctransportes.cjm.domain.entities.vo.InformacaoRelatorioVO;
import br.com.cmctransportes.cjm.domain.entities.vo.PainelDiarioItemVO;
import br.com.cmctransportes.cjm.domain.entities.vo.PainelDiarioVO;
import br.com.cmctransportes.cjm.domain.entities.vo.ResumoJornada;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.services.JourneyCalculusService;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgres;
import br.com.cmctransportes.cjm.utils.TimeHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;
import java.util.stream.Collectors;

/**
 * @author analista.ti
 */
public class JornadaDAO extends GenericDAO<Integer, Jornada> {

    public JornadaDAO(EntityManager entityManager) {
        super(entityManager);
        this.entityManager = entityManager;
    }

    public List<PainelDiarioVO> getAllLast(Integer empresa, Integer unidade, JourneyCalculusService calculus) {
        List<PainelDiarioVO> result = null;

        try {
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT MAX(j.id), m.nome FROM Jornada j");
            sb.append(" LEFT JOIN Evento e ON e.jornada_id = j.id");
            sb.append(" LEFT JOIN Motoristas m ON m.id = j.motoristas_id");
            sb.append(" WHERE e.tipo_evento = 1");
            sb.append(" AND m.situacao = 1");
            sb.append(" AND e.removido = false");

            if (empresa > 0) {
                sb.append(" AND j.empresa_id = :empresa");
            }
            if (unidade > 0) {
                sb.append(" AND m.unidades_id = :unidade");
            }

            sb.append(" GROUP BY m.nome ORDER BY m.nome");

            Query query = entityManager.createNativeQuery(sb.toString());
            if (empresa > 0) {
                query.setParameter("empresa", empresa);
            }
            if (unidade > 0) {
                query.setParameter("unidade", unidade);
            }
            List<Object[]> queryList = query.getResultList();

            // TODO: remove logic from DAO
            final Set<String> estados = new HashSet<>();
            final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy HH:mm");
            result = queryList.stream().map((Object[] o) -> this.getById((Integer) o[0]))
                    .map((Jornada j) -> {
                        calculus.processEvents(j, null,false);
                        /**Alteração para geração da tabela do painel diário do motorista.
                         *
                         *
                         * @author Vinicius Prado
                         */
                        List<PainelDiarioItemVO> itens = new ArrayList<>(0);
                        j.getResumoEstados().entrySet().stream().forEach((entry) -> {
                            final Resumo resumo = entry.getValue();
                            resumo.getLista().stream().filter((event) -> event.getTipoEvento().getId() > 0).forEach((event) -> {
                                PainelDiarioItemVO item;
                                Long elapsed = 0L;

                                if (Objects.nonNull(event.getInstanteEvento()) && Objects.nonNull(event.getFimEvento())) {
                                    elapsed = event.getFimEvento().getTime() - event.getInstanteEvento().getTime();
                                    item = new PainelDiarioItemVO(resumo.getColor(), elapsed, entry.getKey());

                                    item.setInicio(sdf.format(event.getInstanteEvento()).replace(" ", " | "));
                                    item.setInicioTime(event.getInstanteEvento().getTime());
                                    item.setTermino(sdf.format(event.getFimEvento()).replace(" ", " | "));

                                    item.setFinalizado(sdf.format(event.getFimEvento()).concat(" | ").concat(TimeHelper.beautifyTime(elapsed, false)));
                                    item.setWorkHours(calculus.getWorkHours());
                                } else {
                                    item = new PainelDiarioItemVO(resumo.getColor(), elapsed, entry.getKey());
                                }

                                itens.add(item);
                            });
                        });

                        estados.addAll(j.getResumoEstados().keySet());

                        PainelDiarioVO vo = new PainelDiarioVO();
                        vo.setJornada(j.getId());
                        vo.setItens(itens);
                        vo.setMotorista(String.format("%d - %s %s", j.getMotoristasId().getId(), j.getMotoristasId().getNome(), j.getMotoristasId().getSobrenome()));
                        return vo;
                    }).collect(Collectors.toList());

            result.forEach(p -> {
                estados.forEach(e -> {
                    if (p.getItens().stream().noneMatch(i -> Objects.equals(e, i.getLabel()))) {
                        p.getItens().add(new PainelDiarioItemVO("#123456", 0L, e));
                    }
                });
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public Jornada
    getLastJourney(Integer motorista) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT j FROM Jornada j ");
            sb.append("WHERE ");
            sb.append("j.motoristasId.id = :motorista ");
            sb.append("ORDER BY j.id DESC");

            Query query = entityManager.createQuery(sb.toString(), Jornada.class)
                    .setParameter("motorista", motorista).setMaxResults(1);

            if (query != null) {
                List tmpList = query.getResultList();
                if (tmpList != null && tmpList.size() > 0) {
                    Jornada result = (Jornada) tmpList.get(0);
                    return result;
                }
            }

        } catch (NoResultException nre) {
            nre.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    public Jornada getLast(Integer motorista) {
        try {

//            String sql = "SELECT j FROM Jornada j "
//                    + "WHERE j.motoristasId.id = :motorista "
//                    + "AND e.tipoEvento = 1 " // Só interessa a ultima jornada com inicio de jornada
//                    + "ORDER BY j.id DESC";
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT j FROM Jornada j ");
            sb.append("JOIN j.eventos e ");
            sb.append("WHERE ");
            sb.append("j.motoristasId.id = :motorista ");
            sb.append("and e.tipoEvento = 1 ");
            sb.append("and e.removido = false ");
            sb.append("ORDER BY j.id DESC");

            Query query = entityManager.createQuery(sb.toString(), Jornada.class)
                    .setParameter("motorista", motorista).setMaxResults(1);

            Jornada result = new Jornada();

            if (query != null) {
                List tmpList = query.getResultList();
                if (tmpList != null && tmpList.size() > 0) {
                    result = (Jornada) tmpList.get(0);

                    result.setEventos(result.getEventos().stream()
                            .filter(evt -> evt.getTipoEvento().getId() != 17) // remove inicio de abono
                            .filter(evt -> evt.getTipoEvento().getId() != -17) // remove fim de abono
                            .collect(Collectors.toList())
                    );
                }
            }
            return result;

        } catch (NoResultException nre) {
            nre.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * @param driver
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Object[]> findPeriods(Integer driver) {
        List<Object[]> result = new ArrayList<>(0);

        try {
            StringBuilder sb = new StringBuilder(0);
            sb.append("SELECT DISTINCT date_part('month', e.instante_evento) as month, date_part('year', e.instante_evento) as year FROM Evento e");
            sb.append(" LEFT JOIN Jornada j ON j.id = e.jornada_id WHERE j.motoristas_id = :driver");

            Query query = this.entityManager.createNativeQuery(sb.toString()).setParameter("driver", driver);
            result = query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public List<Motoristas> findDriversByJourney(String jornadas) {
        List<Motoristas> result = null;

        try {
            StringBuilder sql = new StringBuilder(0);
            sql.append("SELECT j.motoristasId FROM Jornada j WHERE j.id IN (:jornadas)");

            Query query = entityManager.createQuery(sql.toString())
                    .setParameter("jornadas", Arrays.asList(jornadas.split(",", 0)).stream().map(s -> Integer.parseInt(s)).collect(Collectors.toList()));
            result = query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public Motoristas findDriversByJourney(Integer idJornada) {
        Motoristas motorista = null;

        try {
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT j FROM Jornada j ");
            sb.append("WHERE ");
            sb.append("j.id = :idJornada ");

            Query query = entityManager.createQuery(sb.toString(), Jornada.class)
                    .setParameter("idJornada", idJornada).setMaxResults(1);

            if (query != null) {
                List tmpList = query.getResultList();
                if (tmpList != null && tmpList.size() > 0) {
                    Jornada result = (Jornada) tmpList.get(0);
                    return result.getMotoristasId();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return motorista;
    }

    public List<Jornada> getRelatorioDeJornada(Integer motorista, Date dataInicio, Date dataFim) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT distinct j FROM Jornada j JOIN j.eventos e WHERE ");
            sb.append("e.removido = false AND ");

            if (!Objects.equals(motorista, -1)) {
                sb.append("j.motoristasId.id = :motorista AND ");
            }

            sb.append("e.tipoEvento in (");
            sb.append(Arrays.asList(new Integer[]{EVENT_TYPE.INICIO_JORNADA.getStartCode(), EVENT_TYPE.ABONO.getStartCode(),
                    EVENT_TYPE.FERIAS.getStartCode(), EVENT_TYPE.FALTA.getStartCode(), EVENT_TYPE.DSR.getStartCode(),
                    EVENT_TYPE.FOLGA.getStartCode(), EVENT_TYPE.FERIADO.getStartCode(), EVENT_TYPE.INTERJORNADA.getStartCode()
            }).stream().map(Object::toString).collect(Collectors.joining(",")));

            sb.append(") AND (e.instanteEvento BETWEEN :dataInicio and :dataFim) ");
//            sb.append("ORDER BY e.instanteEvento, e.id");

            Query query = entityManager.createQuery(sb.toString(), Jornada.class)
                    .setParameter("dataInicio", dataInicio)
                    .setParameter("dataFim", dataFim);

            if (!Objects.equals(motorista, -1)) {
                query.setParameter("motorista", motorista);
            }

            List<Jornada> result = query.getResultList();
            return result;
        } catch (NoResultException nre) {
            nre.printStackTrace();
        } catch (Exception except) {
            except.printStackTrace();
        }
        return null;
    }

    public List<Jornada> getRelatorioDeJornada(Date dataInicio, Date dataFim) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT DISTINCT j ");
            sb.append("FROM Jornada j ");
            sb.append("JOIN j.eventos e ");
            sb.append("WHERE (e.instanteEvento BETWEEN :dataInicio and :dataFim ) ");
            sb.append("and e.tipoEvento = 1 ");
            sb.append("and e.removido = false ");
            sb.append("ORDER BY j.motoristasId, e.instanteEvento");

            Query query = entityManager.createQuery(sb.toString(), Jornada.class)
                    .setParameter("dataInicio", dataInicio)
                    .setParameter("dataFim", dataFim);

            List<Jornada> result = query.getResultList();
            return result;
        } catch (NoResultException nre) {
            nre.printStackTrace();
        } catch (Exception except) {
            except.printStackTrace();
        }
        return null;
    }

    /**
     * Pega todos os  tipos de evento que geram jornada
     *
     * @param motorista
     * @param dataInicio
     * @param dataFim
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Jornada> getRelatorioApuracaoDeJornada(Integer motorista, Date dataInicio, Date dataFim) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT distinct j FROM Jornada j JOIN j.eventos e WHERE ");
            sb.append("j.motoristasId.id = :motorista AND ");
            sb.append("e.removido = false and ");
            sb.append("e.tipoEvento in (");

            sb.append(Arrays.stream(new Integer[]{EVENT_TYPE.FERIADO.getStartCode(), EVENT_TYPE.FOLGA.getStartCode(),
                    EVENT_TYPE.INICIO_JORNADA.getStartCode(), EVENT_TYPE.FERIAS.getStartCode(), EVENT_TYPE.DSR.getStartCode(),
                    EVENT_TYPE.FALTA.getStartCode(), EVENT_TYPE.ABONO.getStartCode()})
                    .map(Object::toString)
                    .collect(Collectors.joining(",")));

            sb.append(") AND ");
            sb.append("(e.instanteEvento BETWEEN :dataInicio and :dataFim) ");
//            sb.append("ORDER BY e.instanteEvento");

            Query query = entityManager.createQuery(sb.toString(), Jornada.class)
                    .setParameter("dataInicio", dataInicio)
                    .setParameter("dataFim", dataFim);

            if (!Objects.equals(motorista, -1)) {
                query.setParameter("motorista", motorista);
            }

            List<Jornada> result = query.getResultList();
            return result;
        } catch (NoResultException nre) {
            nre.printStackTrace();
        } catch (Exception except) {
            except.printStackTrace();
        }
        return null;
    }

    @Transactional
    public List<Jornada> getResumosJornadas(Date dataInicio, Date dataFim, Integer motorista) {
        try {
        
            StringBuilder sb = new StringBuilder();
            sb.append("FROM Jornada j WHERE j.motoristasId.id = :motorista ");
            sb.append("AND j.id IN (SELECT e.jornadaId FROM Evento e WHERE e.instanteEvento BETWEEN :dataInicio and :dataFim)");

            Query query = entityManager.createQuery(sb.toString(), Jornada.class)
                    .setParameter("dataInicio", dataInicio)
                    .setParameter("dataFim", dataFim)
                    .setParameter("motorista", motorista);
            List<Jornada> result = query.getResultList();
           //List<Jornada> result = buscarJornadaNativo(dataInicio, dataFim, motorista);
            return result;
        } catch (EntityNotFoundException ex) {
            entityManager.createNativeQuery("UPDATE evento SET local_evento_espera = null WHERE local_evento_espera = 0").executeUpdate();
            return this.getResumosJornadas(dataInicio, dataFim, motorista);
        } catch (NoResultException nre) {
            nre.printStackTrace();
        } catch (Exception except) {
            except.printStackTrace();
        }

        return Collections.emptyList();

    }

    private List<Jornada> buscarJornadaNativo(Date dataInicio, Date dataFim, Integer motorista) {

        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        Map<Integer, Jornada> map = new HashMap<>();
        try {

            Motoristas motoristas = new MotoristasDAO(entityManager).findPeloIdMinimo(motorista);
            connection = DataSourcePostgres.getInstance().getConnection();

            String sql = "select " +
                    " j.id as id_jornada, motoristas_id, j.empresa_id as empresa_id_jornada, horasextrasdiurnas, horasextrasnoturnas, dsr_he, " +
                    " waiting_as_worked, j.locked as jornada_locked, diaria_motorista, " +
                    " e2.id as id_evento, tipo_evento, instante_evento, instante_lancamento, operador_lancamento, veiculo_motor, veiculo_carga01, " +
                    " veiculo_carga02, local_evento_espera, evento_anterior, latitude, longitude, instante_original, " +
                    " operador_alteracao, justificativa, origem_id, motivo_abono_id, removido, versao_origem, position_address, e2.locked, " +
                    " ej.descricao, gera_estado, ej.icone as ej_icone, ej.color as ej_color, ej2.descricao as ej2_descricao, tipos_estado_jornada_id, ej2.color as ej2_color," +
                    " tej.descricao as tej_descricao, tej.color as tej_color, tej.icone as tej_icone" +
                    " from jornada j  " +
                    " inner join evento e2 on e2.jornada_id = j.id " +
                    " inner join eventos_jornada ej on e2.tipo_evento = ej.id " +
                    " inner join estados_jornada ej2 on ej.gera_estado = ej2.id " +
                    " inner join  tipos_estado_jornada tej on tej.id = ej2.tipos_estado_jornada_id " +
                    " where motoristas_id = ? " +
                    " and j.id in (select jornada_id from evento e where e.instante_evento between ? and ?)";


            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, motorista);
            preparedStatement.setTimestamp(2, new Timestamp(dataInicio.getTime()));
            preparedStatement.setTimestamp(3, new Timestamp(dataFim.getTime()));
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Integer jornadaId = resultSet.getInt("id_jornada");
                if (map.containsKey(jornadaId)) {
                    //cria somente o evento
                    Jornada j = map.get(jornadaId);
                    Evento e = gerarEventoManual(resultSet);
                    e.setJornadaId(j);
                    if (Objects.nonNull(JOURNEY_TYPE.getById(e.getTipoEvento().getId()))) {
                        j.setType(JOURNEY_TYPE.getById(e.getTipoEvento().getId()));
                    }
                    j.getEventos().add(e);
                } else {
                    Jornada jornada = new Jornada();
                    jornada.setId(jornadaId);
                    jornada.setMotoristasId(motoristas);
                    jornada.setEmpresaId(new Empresas(resultSet.getInt("empresa_id_jornada")));
                    jornada.setHorasExtrasDiurnas(resultSet.getLong("horasextrasdiurnas"));
                    jornada.setHorasExtrasNoturnas(resultSet.getLong("horasextrasnoturnas"));
                    jornada.setDsrHE(resultSet.getBoolean("dsr_he"));
                    jornada.setWaitingAsWorked(resultSet.getBoolean("waiting_as_worked"));
                    jornada.setLocked(resultSet.getBoolean("jornada_locked"));
                    jornada.setDiariaMotorista(resultSet.getBoolean("diaria_motorista"));
                    Collection<Evento> eventos = new ArrayList<>();
                    Evento e = gerarEventoManual(resultSet);
                    e.setJornadaId(jornada);
                    if (Objects.nonNull(JOURNEY_TYPE.getById(e.getTipoEvento().getId()))) {
                        jornada.setType(JOURNEY_TYPE.getById(e.getTipoEvento().getId()));
                    }
                    eventos.add(e);
                    jornada.setEventos(eventos);
                    map.put(jornadaId, jornada);
                }
            }
        } catch (Exception e) {
            LogSistema.logError("buscarJornadaNativo", e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("getResumosJornadas", ex);
            }

        }
        List<Jornada> lista = new ArrayList<>(map.values());
        Collections.sort(lista,
                Comparator.comparing((Jornada j) -> j.getId()).reversed());
        return lista;
    }

    public List<Jornada> buscarJornadaNativoFaltas(Date dataInicio, Date dataFim, Integer motorista) {

        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        Map<Integer, Jornada> map = new HashMap<>();
        try {

            Motoristas motoristas = new MotoristasDAO(entityManager).findPeloIdMinimo(motorista);
            connection = DataSourcePostgres.getInstance().getConnection();

            String sql = "select " +
                    " j.id as id_jornada, motoristas_id, j.empresa_id as empresa_id_jornada, horasextrasdiurnas, horasextrasnoturnas, dsr_he, " +
                    " waiting_as_worked, j.locked as jornada_locked, diaria_motorista, " +
                    " e2.id as id_evento, tipo_evento, instante_evento, instante_lancamento, operador_lancamento, veiculo_motor, veiculo_carga01, " +
                    " veiculo_carga02, local_evento_espera, evento_anterior, latitude, longitude, instante_original, " +
                    " operador_alteracao, justificativa, origem_id, motivo_abono_id, removido, versao_origem, position_address, e2.locked, " +
                    " ej.descricao, gera_estado, ej.icone as ej_icone, ej.color as ej_color, ej2.descricao as ej2_descricao, tipos_estado_jornada_id, ej2.color as ej2_color," +
                    " tej.descricao as tej_descricao, tej.color as tej_color, tej.icone as tej_icone" +
                    " from jornada j  " +
                    " inner join evento e2 on e2.jornada_id = j.id " +
                    " inner join eventos_jornada ej on e2.tipo_evento = ej.id " +
                    " inner join estados_jornada ej2 on ej.gera_estado = ej2.id " +
                    " inner join  tipos_estado_jornada tej on tej.id = ej2.tipos_estado_jornada_id " +
                    " where motoristas_id = ? " +
                    " and e2.removido = false " +
                    " and e2.tipo_evento in (1 , 17 , 18 , 19 , 20 , 21 , 22 , 26) " +
                    " and e2.instante_evento between ? and ?";

            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, motorista);
            preparedStatement.setTimestamp(2, new Timestamp(dataInicio.getTime()));
            preparedStatement.setTimestamp(3, new Timestamp(dataFim.getTime()));
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Integer jornadaId = resultSet.getInt("id_jornada");
                if (map.containsKey(jornadaId)) {
                    //cria somente o evento
                    Jornada j = map.get(jornadaId);
                    Evento e = gerarEventoManual(resultSet);
                    e.setJornadaId(j);
                    if (Objects.nonNull(JOURNEY_TYPE.getById(e.getTipoEvento().getId()))) {
                        j.setType(JOURNEY_TYPE.getById(e.getTipoEvento().getId()));
                    }
                    j.getEventos().add(e);
                } else {
                    Jornada jornada = new Jornada();
                    jornada.setId(jornadaId);
                    jornada.setMotoristasId(motoristas);
                    jornada.setEmpresaId(new Empresas(resultSet.getInt("empresa_id_jornada")));
                    jornada.setHorasExtrasDiurnas(resultSet.getLong("horasextrasdiurnas"));
                    jornada.setHorasExtrasNoturnas(resultSet.getLong("horasextrasnoturnas"));
                    jornada.setDsrHE(resultSet.getBoolean("dsr_he"));
                    jornada.setWaitingAsWorked(resultSet.getBoolean("waiting_as_worked"));
                    jornada.setLocked(resultSet.getBoolean("jornada_locked"));
                    jornada.setDiariaMotorista(resultSet.getBoolean("diaria_motorista"));
                    Collection<Evento> eventos = new ArrayList<>();
                    Evento e = gerarEventoManual(resultSet);
                    e.setJornadaId(jornada);
                    if (Objects.nonNull(JOURNEY_TYPE.getById(e.getTipoEvento().getId()))) {
                        jornada.setType(JOURNEY_TYPE.getById(e.getTipoEvento().getId()));
                    }
                    eventos.add(e);
                    jornada.setEventos(eventos);
                    map.put(jornadaId, jornada);
                }
            }
        } catch (Exception e) {
            LogSistema.logError("buscarJornadaNativo", e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("getResumosJornadas", ex);
            }

        }
        List<Jornada> lista = new ArrayList<>(map.values());
        return lista;
    }

    private Evento gerarEventoManual(ResultSet resultSet) {
        Evento evento = new Evento();
        try {
            evento.setId(resultSet.getInt("id_evento"));
            //descricao, gera_estado, icone, color
            EventosJornada eventosJornada = new EventosJornada();
            eventosJornada.setId(resultSet.getInt("tipo_evento"));
            eventosJornada.setDescricao(resultSet.getString("descricao"));

            eventosJornada.setIcone(resultSet.getString("ej_icone"));
            eventosJornada.setColor(resultSet.getString("ej_color"));

            //ej2.descricao as ej2_descricao, , ej2.color as
            EstadosJornada estadosJornada = new EstadosJornada(resultSet.getInt("gera_estado"));
            estadosJornada.setDescricao(resultSet.getString("ej2_descricao"));
            TiposEstadoJornada tiposEstadoJornada = new TiposEstadoJornada();
            tiposEstadoJornada.setId(resultSet.getInt("tipos_estado_jornada_id"));
            tiposEstadoJornada.setDescricao(resultSet.getString("tej_descricao"));
            tiposEstadoJornada.setColor(resultSet.getString("tej_color"));
            tiposEstadoJornada.setIcone(resultSet.getString("tej_icone"));
            estadosJornada.setTiposEstadoJornadaId(tiposEstadoJornada);
            eventosJornada.setObjGeraEstado(estadosJornada);
            eventosJornada.setColor(resultSet.getString("ej2_color"));
            evento.setTipoEvento(eventosJornada);

            evento.setInstanteEvento(resultSet.getTimestamp("instante_evento"));
            evento.setInstanteLancamento(resultSet.getTimestamp("instante_lancamento"));
            evento.setOperadorLancamento(new Users(resultSet.getInt("operador_lancamento")));
            evento.setVeiculoMotor(new Veiculos(resultSet.getString("veiculo_motor")));
            evento.setVeiculoCarga01(new Veiculos(resultSet.getString("veiculo_carga01")));
            evento.setVeiculoCarga02(new Veiculos(resultSet.getString("veiculo_carga02")));
            evento.setLocalEventoEspera(new PontosDeEspera(resultSet.getInt("local_evento_espera")));
            evento.setEventoAnterior(new Evento(resultSet.getInt("evento_anterior")));
            evento.setLatitude(resultSet.getDouble("latitude"));
            evento.setLongitude(resultSet.getDouble("longitude"));
            evento.setInstanteOriginal(resultSet.getTimestamp("instante_original"));
            evento.setOperadorAlteracao(new Users(resultSet.getInt("operador_alteracao")));
            evento.setJustificativa(resultSet.getString("justificativa"));
            evento.setMotivoAbono(new MotivoAbono(resultSet.getInt("motivo_abono_id")));
            evento.setRemovido(resultSet.getBoolean("removido"));
            evento.setVersaoOrigem(resultSet.getString("versao_origem"));
            evento.setPositionAddress(resultSet.getString("position_address"));
            evento.setLocked(resultSet.getBoolean("locked"));
        } catch (Exception e) {
            LogSistema.logError("gerarEventoManual", e);
        }
        return evento;
    }

    public List<ResumoJornada> getResumosJornadas(Integer empresaId, Integer unidadeId) {
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        List<ResumoJornada> lista = new ArrayList<>();
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT r.id_jornada, m.nome, m.sobrenome, m.telefone, r.inicio, ");
            sb.append("r.ultima_lancada, r.latitude, r.longitude, r.finalizada, ej.descricao AS tipo_evento,");
            sb.append(" r.locked, u.identificacao FROM ");
            sb.append("(SELECT j2.id as id_jornada, j2.locked, j2.motoristas_id, (SELECT MIN(instante_evento) FROM evento where jornada_id = j2.id) AS inicio, ");
            sb.append("e2.instante_evento AS ultima_lancada, CASE WHEN e2.tipo_evento = 0 THEN 1 ELSE 0 END as finalizada, ");
            sb.append("e2.latitude, e2.longitude, e2.tipo_evento FROM evento e2 INNER JOIN jornada j2 ON e2.jornada_id = j2.id ");
            sb.append("INNER JOIN (SELECT MAX(e0.instante_evento) AS ultima_lancada, j.motoristas_id ");
            sb.append("FROM evento e0 INNER JOIN jornada j ON e0.jornada_id = j.id INNER JOIN motoristas m ON m.id = j.motoristas_id ");
            sb.append("WHERE e0.removido = false AND  m.situacao = 1 AND m.empresa_id = ? ");  // -- filtro empresa (Obrigatorio!)

            if ((unidadeId != null) && (unidadeId > 0)) {
                sb.append("AND m.unidades_id = ?"); // -- filtra por unidade (Opcional!)
            }

            sb.append("GROUP BY j.motoristas_id) t0 ON e2.instante_evento = t0.ultima_lancada ");
            sb.append("AND j2.motoristas_id = t0.motoristas_id) r INNER JOIN evento ev ON ev.jornada_id = r.id_jornada AND ");
            sb.append("ev.instante_evento = r.ultima_lancada INNER JOIN eventos_jornada ej ON ");
            sb.append("ev.tipo_evento = ej.id INNER JOIN motoristas m ON r.motoristas_id = m.id ");
            sb.append("INNER JOIN unidades u ON m.unidades_id = u.id ORDER BY m.nome, m.sobrenome");


            connection = DataSourcePostgres.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sb.toString());
            preparedStatement.setInt(1, empresaId);

            if ((unidadeId != null) && (unidadeId > 0)) {
                preparedStatement.setInt(2, unidadeId);
            }

            resultSet = preparedStatement.executeQuery();


            while (resultSet.next()) {
                ResumoJornada rj = new ResumoJornada();
                rj.setId(resultSet.getInt(1));
                rj.setNomeMotorista(resultSet.getString(2));
                rj.setSobrenomeMotorista(resultSet.getString(3));
                rj.setTelefone(resultSet.getString(4));
                Date dd = new Date(resultSet.getTimestamp(5).getTime());
                rj.setInicio(dd);
                Date df = new Date(resultSet.getTimestamp(6).getTime());
                rj.setUltimoLancamento(df);
                rj.setLatitude(resultSet.getDouble(7));
                rj.setLongitude(resultSet.getDouble(8));
                rj.setFinalizada(resultSet.getBoolean(9));
                rj.setTipoEvento(resultSet.getString(10));
                rj.setWorked(0L);
                rj.setDsrHE(false);
                rj.setWaiting(0L);
                rj.setMissing(0L);
                rj.setWaitingAsWorked(false);
                rj.setLocked(resultSet.getBoolean(11));
                rj.setUnidade(resultSet.getString(12));
                lista.add(rj);
            }

            return lista;
        } catch (NoResultException nre) {
            nre.printStackTrace();
        } catch (Exception except) {
            except.printStackTrace();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("getResumosJornadas", ex);
            }

        }

        return Collections.emptyList();

    }

    public List<Date> getDiasSemJornada(Date dataInicio, Date dataFim, Integer motoristaId) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select cast(inicio as timestamp without time zone) ");
            sb.append("from generate_series( ");
            sb.append("cast(:dataInicio as timestamp), ");
            sb.append("cast(:dataFim as timestamp), ");
            sb.append("cast('1 day' as interval)) inicio ");
            sb.append("where ");
//                sb.append("inicio not in (  ");
            sb.append("cast(inicio as date) not in (  ");
            sb.append("select distinct cast(e.instante_evento as date) ");
            sb.append("from evento e ");
            sb.append("inner join jornada j on ");
            sb.append("e.jornada_id = j.id ");
            sb.append("where ");
            sb.append("e.removido = false ");
            sb.append("and e.instante_evento between :dataInicio and :dataFim ");
            sb.append("and e.tipo_evento in (1, 17, -17, 18, -18, 19, -19, 20, -20, 21, -21, 22, -22, 26, -26) ");
            sb.append("and j.motoristas_id = :motoristaId)");

            Query query = entityManager.createNativeQuery(sb.toString())
                    .setParameter("dataInicio", dataInicio)
                    .setParameter("dataFim", dataFim)
                    .setParameter("motoristaId", motoristaId);

            List<Date> result = query.getResultList();
            return result;
        } catch (NoResultException nre) {
            nre.printStackTrace();
        } catch (Exception except) {
            except.printStackTrace();
        }

        return Collections.EMPTY_LIST;
    }

    public List<Jornada> jornadasDoMotoristaNoPeriodo(Date dtInicial, Date dtFinal, Integer motoristaId) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT distinct j FROM Jornada j ");
            sb.append("JOIN j.eventos e ");
            sb.append("WHERE ");
            sb.append("j.motoristasId.id = :motoristaId ");
            sb.append("and e.tipoEvento in (");

            sb.append(Arrays.stream(new Integer[]{EVENT_TYPE.FERIADO.getStartCode(), EVENT_TYPE.FOLGA.getStartCode(),
                    EVENT_TYPE.INICIO_JORNADA.getStartCode(), EVENT_TYPE.FERIAS.getStartCode(), EVENT_TYPE.DSR.getStartCode(),
                    EVENT_TYPE.FALTA.getStartCode(), EVENT_TYPE.ABONO.getStartCode()})
                    .map(Object::toString)
                    .collect(Collectors.joining(",")));

            sb.append(") ");
            sb.append("and e.removido = false ");
            sb.append("and e.instanteEvento BETWEEN :dataInicio and :dataFim ");
//            sb.append("ORDER BY e.instanteEvento");

            Query query = entityManager.createQuery(sb.toString(), Jornada.class)
                    .setParameter("dataInicio", dtInicial)
                    .setParameter("dataFim", dtFinal)
                    .setParameter("motoristaId", motoristaId);

            List<Jornada> result = query.getResultList();
            return result;
        } catch (NoResultException nre) {
            nre.printStackTrace();
        } catch (Exception except) {
            except.printStackTrace();
        }

        return Collections.EMPTY_LIST;
    }

    public List<Jornada> faltasDoMotoristaNoPeriodo(Date dtInicial, Date dtFinal, Integer motoristaId) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT distinct j FROM Jornada j ");
            sb.append("JOIN j.eventos e ");
            sb.append("WHERE ");
            sb.append("j.motoristasId.id = :motoristaId ");
            sb.append("and e.tipoEvento in (");

            sb.append(Arrays.stream(new Integer[]{EVENT_TYPE.FALTA.getStartCode()})
                    .map(Object::toString)
                    .collect(Collectors.joining(",")));

            sb.append(") ");
            sb.append("and e.removido = false ");
            sb.append("and e.instanteEvento BETWEEN :dataInicio and :dataFim ");

            Query query = entityManager.createQuery(sb.toString(), Jornada.class)
                    .setParameter("dataInicio", dtInicial)
                    .setParameter("dataFim", dtFinal)
                    .setParameter("motoristaId", motoristaId);

            List<Jornada> result = query.getResultList();
            return result;
        } catch (NoResultException nre) {
            nre.printStackTrace();
        } catch (Exception except) {
            except.printStackTrace();
        }

        return Collections.EMPTY_LIST;
    }

    public List<Jornada> buscarJornadasDoDia(List<Motoristas> motoristasList) {
        try {

            StringBuilder sb = new StringBuilder();
            sb.append("SELECT j FROM Jornada j JOIN j.eventos e   ");
            sb.append(" WHERE ");
            sb.append(" j.motoristasId in (:motoristas) AND ");
            sb.append(" e.instanteEvento BETWEEN :dataInicio and :dataFim ");

            Query query = entityManager.createQuery(sb.toString(), Jornada.class)
                    .setParameter("motoristas", motoristasList)
                    .setParameter("dataInicio", TimeHelper.getDate000000())
                    .setParameter("dataFim", new Date());
            /*
                    USADO PARA TESTAR NA BASE
                    .setParameter("dataInicio", new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse("04/05/2020 00:00:00"))
                    .setParameter("dataFim", new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse("04/05/2020 22:00:00"));
             */

            List<Jornada> result = query.getResultList();
            return result;
        } catch (Exception e) {
            // LogSistema.logError("buscarInconformidadesDoDia", e);
            e.printStackTrace();
        }
        return new ArrayList<>();
    }


    public List<Jornada> buscarJornadasDoDiaViaSQLNativo(List<Motoristas> motoristasList) {
        List<Jornada> lista = new ArrayList<>();

        try {
            final StringBuffer stringBuffer = new StringBuffer();
            motoristasList.forEach(m -> {
                stringBuffer.append(m.getId() + ",");
            });

            if (stringBuffer.length() <= 0) {
                return lista;
            }

            String motoristasId = StringUtils.chop(stringBuffer.toString());

            String sql = "select j.id as jornada_id, e.id  as evento_id, j.motoristas_id as motorista, e.instante_evento, ej.id as id_ej, ej.descricao as desc_ej, " +
                    " esj.id as esj_id, tsj.id as tsj_id, tsj.descricao as tsj_desc, " +
                    " (select situacao from motoristas where id = j.motoristas_id) as situacao, " +
                    "  e.latitude as lat, e.longitude lon" +
                    " from jornada j " +
                    " inner join evento e on e.jornada_id = j.id " +
                    " inner join eventos_jornada ej on e.tipo_evento = ej.id " +
                    " inner join estados_jornada esj on ej.gera_estado = esj.id " +
                    " inner join tipos_estado_jornada tsj on esj.tipos_estado_jornada_id = tsj.id " +
                    " where j.motoristas_id in (" + motoristasId + ") " +
                    " and e.instante_evento between :dataInicio and :dataFim" +
                    " and e.removido = false ";

            Query query = entityManager.createNativeQuery(sql)
                    .setParameter("dataInicio", TimeHelper.getDate000000())
                    .setParameter("dataFim", new Date());
            /*
                    USADO PARA TESTAR NA BASE
                    .setParameter("dataInicio", new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse("09/07/2020 00:00:00"))
                    .setParameter("dataFim", new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse("09/07/2020 22:00:00"));
            */

            List result = query.getResultList();
            Map<Integer, Jornada> map = new HashMap<>();
            List<Evento> listaDeEvento = null;
            for (int i = 0; i < result.size(); i++) {
                Object[] ooArray = (Object[]) result.get(i);
                Integer idJornada = (Integer) ooArray[0];
                Integer idMotorista = (Integer) ooArray[2];
                Date dataInstante = new Date(((Timestamp) ooArray[3]).getTime());
                Integer idEj = (Integer) ooArray[4];
                String descricaoEj = (String) ooArray[5];
                Integer esj_id = (Integer) ooArray[6];
                Integer tsj_id = (Integer) ooArray[7];
                String tsj_desc = (String) ooArray[8];
                Integer situacao = (Integer) ooArray[9];
                Double latitude = (Double) ooArray[10];
                Double longitude = (Double) ooArray[11];

                if (map.containsKey(idJornada)) {
                    Evento evento = new Evento();
                    evento.setId((Integer) ooArray[1]);
                    evento.setInstanteEvento(dataInstante);
                    evento.setLatitude(latitude);
                    evento.setLongitude(longitude);
                    EventosJornada tipoEvento = new EventosJornada();
                    tipoEvento.setId(idEj);
                    tipoEvento.setDescricao(descricaoEj);
                    EstadosJornada objGeraEstado = new EstadosJornada();
                    objGeraEstado.setId(esj_id);
                    TiposEstadoJornada tiposEstadoJornadaId = new TiposEstadoJornada();
                    tiposEstadoJornadaId.setId(tsj_id);
                    tiposEstadoJornadaId.setDescricao(tsj_desc);
                    objGeraEstado.setTiposEstadoJornadaId(tiposEstadoJornadaId);
                    tipoEvento.setObjGeraEstado(objGeraEstado);
                    evento.setTipoEvento(tipoEvento);
                    map.get(idJornada).getEventos().add(evento);
                } else {
                    Jornada jornada = new Jornada();
                    jornada.setId(idJornada);
                    Motoristas motoristas = new Motoristas();
                    motoristas.setId(idMotorista);
                    motoristas.setSituacao(situacao);
                    jornada.setMotoristasId(motoristas);
                    List<Evento> eventos = new ArrayList<>();
                    Evento evento = new Evento();
                    evento.setId((Integer) ooArray[1]);
                    evento.setInstanteEvento(dataInstante);
                    EventosJornada tipoEvento = new EventosJornada();
                    tipoEvento.setId(idEj);
                    tipoEvento.setDescricao(descricaoEj);
                    EstadosJornada objGeraEstado = new EstadosJornada();
                    objGeraEstado.setId(esj_id);
                    TiposEstadoJornada tiposEstadoJornadaId = new TiposEstadoJornada();
                    tiposEstadoJornadaId.setId(tsj_id);
                    tiposEstadoJornadaId.setDescricao(tsj_desc);
                    objGeraEstado.setTiposEstadoJornadaId(tiposEstadoJornadaId);
                    tipoEvento.setObjGeraEstado(objGeraEstado);
                    evento.setTipoEvento(tipoEvento);
                    eventos.add(evento);
                    jornada.setEventos(eventos);
                    map.put(idJornada, jornada);
                }
            }

            lista = new ArrayList<>(map.values());
        } catch (Exception e) {
            LogSistema.logError("buscarJornadasDoDiaViaSQLNativo", e);
            e.printStackTrace();
        }
        return lista;
    }


    public InformacaoRelatorioVO buscarDadosRelatorioDiariaMotorista(Date dataInicial, Date dataFinal, Integer idMotorista) {
        InformacaoRelatorioVO informacaoRelatorioVO = new InformacaoRelatorioVO();
        informacaoRelatorioVO.setListaInformacaoRelatorio(new ArrayList<>());
        try {
            String sql = "select j.id, e.instante_evento, j.diaria_motorista, m.nome, m.sobrenome  " +
                    " from jornada j  " +
                    " inner join evento e on e.jornada_id = j.id " +
                    " inner join motoristas m on m.id = j.motoristas_id  " +
                    " where j.motoristas_id = :idMotorista  " +
                    " and e.instante_evento between :dataInicio and :dataFim" +
                    " and e.removido = false ";

            Query query = entityManager.createNativeQuery(sql)
                    .setParameter("idMotorista", idMotorista)
                    .setParameter("dataInicio", dataInicial)
                    .setParameter("dataFim", dataFinal);

            List result = query.getResultList();
            Map<Integer, Integer> mapTemp = new HashMap<>();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
            for (int i = 0; i < result.size(); i++) {
                Object[] ooArray = (Object[]) result.get(i);
                Integer idJornada = (Integer) ooArray[0];
                Date instanteEvento = new Date(((Timestamp) ooArray[1]).getTime());
                Boolean diariaMotorista = (Boolean) ooArray[2];
                String nome = (String) ooArray[3];
                String sobreNome = (String) ooArray[4];
                if (i == 0) {
                    informacaoRelatorioVO.setCampo01(nome + " " + sobreNome);
                }

                if (!mapTemp.containsKey(idJornada)) {
                    mapTemp.put(idJornada, idJornada);
                    InformacaoRelatorioVO vo = new InformacaoRelatorioVO();
                    vo.setCampo01(simpleDateFormat.format(instanteEvento));
                    vo.setCampo02(TimeHelper.pegarDiaDaSemana(instanteEvento));
                    if (Objects.equals(diariaMotorista, Boolean.TRUE)) {
                        vo.setCampo03("Sim");
                    } else {
                        vo.setCampo03("Não");
                    }
                    vo.setDataDoEvento(instanteEvento);
                    informacaoRelatorioVO.getListaInformacaoRelatorio().add(vo);

                }
                mapTemp.put(idJornada, idJornada);
            }
        } catch (Exception e) {
            LogSistema.logError("buscarDadosRelatorioDiariaMotorista", e);
            e.printStackTrace();
        }

        return informacaoRelatorioVO;
    }


    public List<Evento> buscarEventoTimeLineNativo(Date dataInicio, Date dataFim, Integer motorista) {
        List<Evento> lista = new ArrayList<>();
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {

            Motoristas motoristas = new MotoristasDAO(entityManager).findPeloIdMinimo(motorista);
            connection = DataSourcePostgres.getInstance().getConnection();

            String sql = "select  " +
                    " e2.id as id_evento, tipo_evento, instante_evento, instante_lancamento, operador_lancamento, veiculo_motor, veiculo_carga01,  " +
                    " veiculo_carga02, local_evento_espera, evento_anterior, latitude, longitude, instante_original,  " +
                    " operador_alteracao, justificativa, origem_id, motivo_abono_id, removido, versao_origem, position_address, e2.locked,  " +
                    " ej.descricao, gera_estado, ej.icone as ej_icone, ej.color as ej_color, ej2.descricao as ej2_descricao, tipos_estado_jornada_id, ej2.color as ej2_color, " +
                    " tej.descricao as tej_descricao, tej.color as tej_color, tej.icone as tej_icone " +
                    " from jornada j "+
                    " inner join evento e2 on e2.jornada_id = j.id "+
                    " inner join eventos_jornada ej on e2.tipo_evento = ej.id "+
                    " inner join estados_jornada ej2 on ej.gera_estado = ej2.id "+
                    " inner join  tipos_estado_jornada tej on tej.id = ej2.tipos_estado_jornada_id "+
                    " where motoristas_id = ? " +
                    " and e2.removido = false "+
                    " and instante_evento between ? and ? "+
                    " order by e2.instante_evento;";

            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, motorista);
            preparedStatement.setTimestamp(2, new Timestamp(dataInicio.getTime()));
            preparedStatement.setTimestamp(3, new Timestamp(dataFim.getTime()));
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Evento evento = gerarEventoManual(resultSet);
                lista.add(evento);
            }

        } catch (Exception e) {
            LogSistema.logError("buscarJornadaNativo", e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("getResumosJornadas", ex);
            }

        }
        return lista;
    }

    public void deletarJornadaComFerias(Date dataInicio, Date dataFim, Integer motorista) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {

            List<Integer> idsEventos = new ArrayList<>();
            List<Integer> idsJornada = new ArrayList<>();
            connection = DataSourcePostgres.getInstance().getConnection();

            String sql = "select e.id as idevento, jornada_id  from evento e " +
                    " inner join jornada j on e.jornada_id = j.id " +
                    " where j.motoristas_id = ? " +
                    " and e.instante_evento between ? and ? " +
                    " and (e.tipo_evento = 18 or e.tipo_evento = -18) " +
                    " and e.operador_lancamento = -1 ";

            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, motorista);
            preparedStatement.setTimestamp(2, new Timestamp(dataInicio.getTime()));
            preparedStatement.setTimestamp(3, new Timestamp(dataFim.getTime()));
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
               Integer idEvento = resultSet.getInt("idevento");
               Integer idJornada = resultSet.getInt("jornada_id");
               idsEventos.add(idEvento);
               idsJornada.add(idJornada);
            }
            if (resultSet != null) {
                resultSet.close();
            }

            Statement stmt = connection.createStatement();
            for(Integer id : idsEventos){
                String sqlT = "delete from evento where id = "+ id.toString();
                stmt.executeUpdate(sqlT);
            }

            for(Integer id : idsJornada){
                String sqlT = "delete from jornada where id = "+ id.toString();
                stmt.executeUpdate(sqlT);
            }

            if(stmt != null){
                stmt.close();
            }

        } catch (Exception e) {
            LogSistema.logError("deletarJornadaComFerias", e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("deletarJornadaComFerias", ex);
            }

        }
    }
}
