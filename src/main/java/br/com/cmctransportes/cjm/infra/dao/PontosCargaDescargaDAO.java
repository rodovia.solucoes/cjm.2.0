package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.PontosCargaDescarga;

import javax.persistence.EntityManager;

/**
 * @author analista.ti
 */
public class PontosCargaDescargaDAO extends GenericDAO<Integer, PontosCargaDescarga> {
    public PontosCargaDescargaDAO(EntityManager entityManager) {
        super(entityManager);
    }
}
