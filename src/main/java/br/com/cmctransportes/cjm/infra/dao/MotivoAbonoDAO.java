package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.MotivoAbono;

import javax.persistence.EntityManager;

/**
 * @author William Leite
 */
public class MotivoAbonoDAO extends GenericDAO<Integer, MotivoAbono> {
    public MotivoAbonoDAO(EntityManager entityManager) {
        super(entityManager);
    }
}