package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.Evento;
import br.com.cmctransportes.cjm.domain.entities.EventoMotorista;
import br.com.cmctransportes.cjm.domain.entities.Motoristas;
import br.com.cmctransportes.cjm.domain.entities.vo.MotoristaVO;
import br.com.cmctransportes.cjm.domain.entities.vo.VeiculoVO;
import br.com.cmctransportes.cjm.sascarsoap.Motorista;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgres;

import javax.persistence.EntityManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class EventoMotoristaDAO extends GenericDAO<Integer, EventoMotorista>  {

    public EventoMotoristaDAO(EntityManager entityManager) {
        super(entityManager);
    }


    public List<EventoMotorista> buscarPorPeriodos(Integer motoristaId, Date dataInicial, Date dataFinal) {
        List<EventoMotorista> lista = new ArrayList<>();
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from evento_motorista where motorista = ? and data_evento >= ? and data_fim_evento <= ? ";
            connection = DataSourcePostgres.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, motoristaId);
            preparedStatement.setTimestamp(2, new Timestamp(dataInicial.getTime()));
            preparedStatement.setTimestamp(3, new Timestamp(dataFinal.getTime()));
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Integer id = resultSet.getInt("id");
                Integer tipo = resultSet.getInt("tipo");
                Date data = new Date(resultSet.getTimestamp("data_evento").getTime());
                Date dataFim = new Date(resultSet.getTimestamp("data_fim_evento").getTime());
                Integer hodometroInicial = resultSet.getInt("hodometro_inicial");
                Integer hodometroFinal = resultSet.getInt("hodometro_final");

                EventoMotorista eventoMotorista = new EventoMotorista();
                eventoMotorista.setId(id);
                eventoMotorista.setDataEvento(data);
                eventoMotorista.setDataFimEvento(dataFim);
                eventoMotorista.setTipo(tipo);
                eventoMotorista.setHodometroFinal(hodometroFinal);
                eventoMotorista.setHodometroInicial(hodometroInicial);
                lista.add(eventoMotorista);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return lista;
    }

    public List<EventoMotorista> buscarPorPeriodos(List<MotoristaVO> motoristas, Date dataInicial, Date dataFinal, Integer tipoEvento) {
        List<EventoMotorista> lista = new ArrayList<>();
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {

            String m = "";
            for(MotoristaVO v: motoristas){
                m = m.concat(",").concat(v.getId().toString());
            }
            m = m.substring(1);
            String sql = " SELECT tipo, data_evento, motorista, data_fim_evento, hodometro_inicial, hodometro_final, m2.nome, m2.sobrenome " +
                    " FROM public.evento_motorista em " +
                    " inner join motoristas m2 on m2.id = em.motorista  " +
                    " where motorista in ("+m+") and data_evento >= ? and data_fim_evento <= ? ";
            if(Objects.nonNull(tipoEvento) && tipoEvento > 0){
                sql = sql + " and tipo = " + tipoEvento.toString();
            }
            connection = DataSourcePostgres.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setTimestamp(1, new Timestamp(dataInicial.getTime()));
            preparedStatement.setTimestamp(2, new Timestamp(dataFinal.getTime()));
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {

                Integer tipo = resultSet.getInt("tipo");
                Date data = new Date(resultSet.getTimestamp("data_evento").getTime());
                Date dataFim = new Date(resultSet.getTimestamp("data_fim_evento").getTime());
                Integer hodometroInicial = resultSet.getInt("hodometro_inicial");
                Integer hodometroFinal = resultSet.getInt("hodometro_final");
                Integer mIdTemp = resultSet.getInt("motorista");
                String nome = resultSet.getString("nome");
                String sobrenome = resultSet.getString("sobrenome");

                EventoMotorista eventoMotorista = new EventoMotorista();
                eventoMotorista.setDataEvento(data);
                eventoMotorista.setDataFimEvento(dataFim);
                eventoMotorista.setTipo(tipo);
                eventoMotorista.setHodometroFinal(hodometroFinal);
                eventoMotorista.setHodometroInicial(hodometroInicial);

                Motoristas mId = new Motoristas();
                mId.setId(mIdTemp);
                mId.setNome(nome);
                mId.setSobrenome(sobrenome);
                eventoMotorista.setMotoristas(mId);

                lista.add(eventoMotorista);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return lista;
    }



    public List<EventoMotorista> buscarPorVeiculos(List<VeiculoVO> veiculos, Date dataInicial, Date dataFinal, Integer tipoEvento) {
        List<EventoMotorista> lista = new ArrayList<>();
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {

            String m = "";
            for(VeiculoVO v: veiculos){
                m = m.concat(",").concat(v.getId().toString());
            }
            m = m.substring(1);
            String sql = " SELECT tipo, data_evento, motorista, data_fim_evento, hodometro_inicial, hodometro_final, m2.nome, m2.sobrenome, id_veiculo_trajetto " +
                    " FROM public.evento_motorista em " +
                    " inner join motoristas m2 on m2.id = em.motorista  " +
                    " where id_veiculo_trajetto in ("+m+") and data_evento >= ? and data_fim_evento <= ? ";
            if(Objects.nonNull(tipoEvento) && tipoEvento > 0){
                sql = sql + " and tipo = " + tipoEvento.toString();
            }
            connection = DataSourcePostgres.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setTimestamp(1, new Timestamp(dataInicial.getTime()));
            preparedStatement.setTimestamp(2, new Timestamp(dataFinal.getTime()));
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {

                Integer tipo = resultSet.getInt("tipo");
                Date data = new Date(resultSet.getTimestamp("data_evento").getTime());
                Date dataFim = new Date(resultSet.getTimestamp("data_fim_evento").getTime());
                Integer hodometroInicial = resultSet.getInt("hodometro_inicial");
                Integer hodometroFinal = resultSet.getInt("hodometro_final");
                Integer mIdTemp = resultSet.getInt("motorista");
                String nome = resultSet.getString("nome");
                String sobrenome = resultSet.getString("sobrenome");
                Integer idVeiculo = resultSet.getInt("id_veiculo_trajetto");

                EventoMotorista eventoMotorista = new EventoMotorista();
                eventoMotorista.setDataEvento(data);
                eventoMotorista.setDataFimEvento(dataFim);
                eventoMotorista.setTipo(tipo);
                eventoMotorista.setHodometroFinal(hodometroFinal);
                eventoMotorista.setHodometroInicial(hodometroInicial);
                eventoMotorista.setIdVeiculo(idVeiculo);

                Motoristas mId = new Motoristas();
                mId.setId(mIdTemp);
                mId.setNome(nome);
                mId.setSobrenome(sobrenome);
                eventoMotorista.setMotoristas(mId);

                lista.add(eventoMotorista);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return lista;
    }

    public List<EventoMotorista> buscarPorPeriodos(Integer motoristaId, Integer tipo ,Date dataInicial, Date dataFinal) {
        List<EventoMotorista> lista = new ArrayList<>();
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from evento_motorista where motorista = ? and tipo = ? and data_evento >= ? and data_fim_evento <= ? ";
            connection = DataSourcePostgres.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, motoristaId);
            preparedStatement.setInt(2, tipo);
            preparedStatement.setTimestamp(3, new Timestamp(dataInicial.getTime()));
            preparedStatement.setTimestamp(4, new Timestamp(dataFinal.getTime()));
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Integer id = resultSet.getInt("id");
                Integer tipoT = resultSet.getInt("tipo");
                Date data = new Date(resultSet.getTimestamp("data_evento").getTime());
                Date dataFim = new Date(resultSet.getTimestamp("data_fim_evento").getTime());
                Integer hodometroInicial = resultSet.getInt("hodometro_inicial");
                Integer hodometroFinal = resultSet.getInt("hodometro_final");

                EventoMotorista eventoMotorista = new EventoMotorista();
                eventoMotorista.setId(id);
                eventoMotorista.setDataEvento(data);
                eventoMotorista.setDataFimEvento(dataFim);
                eventoMotorista.setTipo(tipoT);
                eventoMotorista.setHodometroFinal(hodometroFinal);
                eventoMotorista.setHodometroInicial(hodometroInicial);
                lista.add(eventoMotorista);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return lista;
    }
}
