/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cmctransportes.cjm.infra.dao;

import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author analista.ti
 */
public class GenericDAO<PK, T> {

    // #2033016
    protected EntityManager entityManager;

    public GenericDAO(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @SuppressWarnings("unchecked")
    public T getById(PK pk) {
        try {
            return (T) entityManager.find(getTypeClass(), pk);
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    @Transactional
    public void save(T entity) {
        try {
            entityManager.persist(entity);
            entityManager.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * Chamar esse metodo quando ja estiver anotado Transactional no metodo que esta anotado
     * @param entity
     */
    public void saveSemTransactional(T entity) {
        try {
            entityManager.persist(entity);
           // entityManager.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void update(T entity) {
        entityManager.merge(entity);
    }

    @Transactional
    public void delete(T entity) {
        entityManager.remove(entity);
    }

    @SuppressWarnings("unchecked")
    public List<T> findAll() {
        return entityManager.createQuery(("FROM " + getTypeClass().getName()))
                .getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<T> nqFindAll() {
        Query q = entityManager.createNamedQuery(getTypeClass().getSimpleName() + ".findAll");
        List<T> result = q.getResultList();
        return result;
    }

    private Class<?> getTypeClass() {
        Class<?> clazz = (Class<?>) ((ParameterizedType) this.getClass()
                .getGenericSuperclass()).getActualTypeArguments()[1];
        return clazz;
    }

    @SuppressWarnings("unchecked")
    public List<T> findAll(Integer idEmpresa) {
        Query q = entityManager.createQuery("FROM " + getTypeClass().getName() + " t WHERE t.empresaId.id = :idEmpresa");
        q.setParameter("idEmpresa", idEmpresa);
        List<T> result = q.getResultList();
        return result;
    }

    @SuppressWarnings("unchecked")
    public List<T> findAll(Integer idEmpresa, Integer idUnidade) {
        StringBuilder sb = new StringBuilder();
        sb.append("FROM ").append(getTypeClass().getName()).append(" t WHERE t.empresaId.id = :idEmpresa");
        if (idUnidade > 0) {
            sb.append(" AND t.unidadesId = :idUnidade");
        }
        Query q = entityManager.createQuery(sb.toString());
        q.setParameter("idEmpresa", idEmpresa);
        if (idUnidade > 0) {
            q.setParameter("idUnidade", idUnidade);
        }
        List<T> result = q.getResultList();
        return result;
    }

    @SuppressWarnings("unchecked")
    public T getById(Integer idEmpresa, PK pk) {
        Map<String, Object> filtroEmpresa = new HashMap<>();
        filtroEmpresa.put("empresaId.id", idEmpresa);

        return (T) entityManager.find(getTypeClass(), pk, filtroEmpresa);
    }
}
