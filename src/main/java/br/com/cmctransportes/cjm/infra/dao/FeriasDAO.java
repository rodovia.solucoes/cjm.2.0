package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.Ferias;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;

/**
 * @author William Leite
 */
public class FeriasDAO extends GenericDAO<Integer, Ferias> {

    public FeriasDAO(EntityManager entityManager) {
        super(entityManager);
    }

    /**
     * Retorna cadastro de Ferias por Motorista
     *
     * @param motorista ID do Motorista
     * @return Ferias do Motorista
     */
    public List<Ferias> findByDriver(Integer motorista) {
        List<Ferias> ferias = null;

        try {
            String sql = "FROM Ferias WHERE motoristaId = :motorista";
            Query query = entityManager.createQuery(sql).setParameter("motorista", sql);

            ferias = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return ferias;
    }

    public Ferias pegaFeriasDoMotoristaPorPeriodo(Date dtIni, Date dtFim, Integer motorista) {
        Ferias result = null;

        try {

            StringBuilder sb = new StringBuilder();
            sb.append("SELECT f.* FROM Ferias f ");
            sb.append("WHERE ");
            sb.append("f.motorista_Id = :motorista ");
            sb.append("and (f.data_Inicio between :dtIni and :dtFim ");
            sb.append("or f.data_Final between :dtIni and :dtFim) ");
            sb.append("ORDER BY f.id ");

            Query query = entityManager.createNativeQuery(sb.toString(), Ferias.class)
                    .setParameter("motorista", motorista)
                    .setParameter("dtIni", dtIni)
                    .setParameter("dtFim", dtFim);

            if (query != null) {
                List tmpList = query.getResultList();
                if (tmpList != null && tmpList.size() > 0) {
                    result = (Ferias) tmpList.get(0);
                }
            }
            return result;

        } catch (NoResultException nre) {
            nre.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public List<Date> pegaDiasDeFeriasNoPeriodo(Date dataInicio, Date dataFinal, Integer motorista) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select ");
            sb.append("cast(inicio as timestamp without time zone) ");
            sb.append("from ");
            sb.append("generate_series( ");
            sb.append("cast(:dataInicio as timestamp), ");
            sb.append("cast(:dataFim as timestamp), ");
            sb.append("cast('1 day' as interval) ");
            sb.append(") inicio ");
            sb.append("where ");
            sb.append("cast(inicio as date) not in ( ");
            sb.append("select ");
            sb.append("cast(e.instante_evento as date) ");
            sb.append("from ");
            sb.append("evento e ");
            sb.append("inner join jornada j on ");
            sb.append("e.jornada_id = j.id ");
            sb.append("where ");
            sb.append("e.removido = false ");
            sb.append("and e.instante_evento between :dataInicio and :dataFim ");
            sb.append("and e.tipo_evento = 18 ");
            sb.append("and j.motoristas_id = :motoristaId ");
            sb.append(") ");

            Query query = entityManager.createNativeQuery(sb.toString())
                    .setParameter("dataInicio", dataInicio)
                    .setParameter("dataFim", dataFinal)
                    .setParameter("motoristaId", motorista);
            List<Date> result = query.getResultList();
            return result;
        } catch (NoResultException nre) {
            nre.printStackTrace();
        } catch (Exception except) {
            except.printStackTrace();
        }

        return null;
    }
}
