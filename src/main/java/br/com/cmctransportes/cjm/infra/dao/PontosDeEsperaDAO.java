/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.PontosDeEspera;

import javax.persistence.EntityManager;

/**
 * @author analista.ti
 */
public class PontosDeEsperaDAO extends GenericDAO<Integer, PontosDeEspera> {
    public PontosDeEsperaDAO(EntityManager entityManager) {
        super(entityManager);
    }

}
