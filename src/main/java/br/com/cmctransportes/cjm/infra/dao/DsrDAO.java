package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.Dsr;
import br.com.cmctransportes.cjm.domain.entities.Motoristas;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @author analista.ti
 */
public class DsrDAO extends GenericDAO<Integer, Dsr> {

    public DsrDAO(EntityManager entityManager) {
        super(entityManager);
    }

    public List<Dsr> getAgendados(Integer motorista, Date dataInicio, Date dataFim) {
        List<Dsr> result = Collections.emptyList();
        try {
            String sql =
                    "SELECT d "
                            + "FROM Dsr d "
                            + "WHERE "
                            + "d.motorista.id = :motorista "
                            + "and d.processada = 'n' "
                            + "and (d.inicioDSR BETWEEN :dataInicio and :dataFim ) "
                            + "ORDER BY d.inicioDSR";
            Query query = entityManager.createQuery(sql, Dsr.class)
                    .setParameter("motorista", motorista)
                    .setParameter("dataInicio", dataInicio)
                    .setParameter("dataFim", dataFim);

            result = query.getResultList();
        } catch (NoResultException nre) {
            nre.printStackTrace();
        } catch (Exception except) {
            except.printStackTrace();
        }
        return result;
    }

    public List<Motoristas> getMotoristasNaoAgendadosDoDia(Date dataInicio, Date dataFim) {
        List<Motoristas> result = Collections.emptyList();
        try {
            String sql =
                    "select *" +
                            "from " +
                            " motoristas " +
                            "where id not in ( " +
                            "select motorista_id " +
                            "from dsr " +
                            "where " +
                            " inicio_dsr between :dataInicio and :dataFim " +
                            " )";
            Query query = entityManager.createNativeQuery(sql, Motoristas.class)
                    .setParameter("dataInicio", dataInicio)
                    .setParameter("dataFim", dataFim);

            result = query.getResultList();
        } catch (NoResultException nre) {
            nre.printStackTrace();
        } catch (Exception except) {
            except.printStackTrace();
        }
        return result;
    }

    // ATM 2040271: considerar apenas o início de jornada
    public List<Dsr> getEventoEmDsr(Date dataInicio, Date dataFim, Integer motorista) {
        List<Dsr> result = Collections.emptyList();
        try {
            // TODO: trocar isso aqui por uma HQL
//                    "SELECT d " +
//                    "from Dsr d " +
//                    "where (( " +
//                        ":dtInicio between d.inicioDSR and d.fimDSR or " +
//                        ":dtFim between d.inicioDSR and d.fimDSR " +
//                        ") or ( " +
//                        "d.inicioDSR between :dtInicio and :dtFim and " +
//                        "d.fimDSR between :dtInicio and :dtFim) " +
//                    ") and d.motorista.id = :motoristaId " +
//                    "ORDER BY d.inicioDSR";

            // ATM 2040271: considerar apenas o início de jornada
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT d ");
            sql.append("from Dsr d ");
            sql.append("where ");
            sql.append(":dtInicio between d.inicioDSR and d.fimDSR ");
            sql.append("and d.motorista.id = :motoristaId ");
            // ATM 2041034
            sql.append("and d.processada <> 'r' "); // diferente de removido
            sql.append("ORDER BY d.inicioDSR");

            Query query = entityManager.createQuery(sql.toString(), Dsr.class)
                    .setParameter("motoristaId", motorista)
                    .setParameter("dtInicio", dataInicio);

            result = query.getResultList();
        } catch (NoResultException nre) {
            nre.printStackTrace();
        } catch (Exception except) {
            except.printStackTrace();
        }
        return result;
    }

    public List<Dsr> findAll(Integer idEmpresa, Integer idUnidade) {
        Query q = entityManager.createQuery("FROM Dsr t WHERE t.empresaId.id = :idEmpresa AND t.motorista.unidadesId = :idUnidade order by t.inicioDSR, t.motorista.nome");
        q.setParameter("idEmpresa", idEmpresa);
        q.setParameter("idUnidade", idUnidade);
        List<Dsr> result = q.getResultList();
        return result;
    }

    public List<Dsr> findAll(Integer idEmpresa, Integer idUnidade, Date dataInicio, Date dataFim) {
        Query q = entityManager.createQuery("FROM Dsr t WHERE t.empresaId.id = :idEmpresa AND t.motorista.unidadesId = :idUnidade AND t.inicioDSR between :dtInicio and :dtFim order by t.inicioDSR, t.motorista.nome");
        q.setParameter("idEmpresa", idEmpresa);
        q.setParameter("idUnidade", idUnidade);
        q.setParameter("dtInicio", dataInicio);
        q.setParameter("dtFim", dataFim);
        List<Dsr> result = q.getResultList();
        return result;
    }

    public List<Dsr> findAll(Integer idEmpresa, Date dataInicio, Date dataFim) {
        Query q = entityManager.createQuery("FROM Dsr t WHERE t.empresaId.id = :idEmpresa AND t.inicioDSR between :dtInicio and :dtFim order by t.inicioDSR, t.motorista.nome");
        q.setParameter("idEmpresa", idEmpresa);
        q.setParameter("dtInicio", dataInicio);
        q.setParameter("dtFim", dataFim);
        List<Dsr> result = q.getResultList();
        return result;
    }

    public List<Dsr> findByDriver(Integer motorista, Date dataInicial, Date dataFinal) {
        Query q = entityManager.createQuery("FROM Dsr t WHERE t.motorista.id = :motorista AND t.processada <> 'r' AND t.inicioDSR between :dtInicio and :dtFim order by t.inicioDSR");
        q.setParameter("motorista", motorista);
        q.setParameter("dtInicio", dataInicial);
        q.setParameter("dtFim", dataFinal);
        List<Dsr> result = q.getResultList();
        return result;
    }

    public Dsr findLastByDriver(Integer motorista, Date data) {
        Query q = entityManager.createQuery("FROM Dsr t WHERE t.motorista.id = :motorista AND t.processada <> 'r' AND t.inicioDSR <= :dtInicio ORDER BY t.fimDSR DESC");
        q.setParameter("motorista", motorista);
        q.setParameter("dtInicio", data);

        if (Objects.nonNull(q.getResultList()) && !q.getResultList().isEmpty()) {
            return (Dsr) q.getResultList().get(0);
        }

        return null;
    }
}
