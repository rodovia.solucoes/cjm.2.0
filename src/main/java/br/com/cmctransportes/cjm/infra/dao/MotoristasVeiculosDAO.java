package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.Motoristas;
import br.com.cmctransportes.cjm.domain.entities.MotoristasVeiculos;
import br.com.cmctransportes.cjm.domain.entities.Veiculos;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgres;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgresOrion;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author William Leite
 */
public class MotoristasVeiculosDAO extends GenericDAO<Integer, MotoristasVeiculos> {

    public MotoristasVeiculosDAO(EntityManager entityManager) {
        super(entityManager);
    }

    public Veiculos getByDriver(Integer motorista, Date data) {
        Veiculos result = null;

        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT mv FROM MotoristasVeiculos mv ");
            sql.append(" WHERE mv.motoristasId = :motorista AND mv.dataInicio <= :datainicio");
            sql.append(" ORDER BY mv.dataInicio DESC");

            Query query = entityManager.createQuery(sql.toString(), MotoristasVeiculos.class)
                    .setParameter("motorista", motorista)
                    .setParameter("datainicio", data);

            List<MotoristasVeiculos> veiculos = query.getResultList();

            Optional<MotoristasVeiculos> veiculo = veiculos.stream().findFirst();
            if (veiculo.isPresent())
                result = veiculo.get().getVeiculosId();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return result;
    }




    public List<MotoristasVeiculos> getByVeiculo(Integer veiculo) {

        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT mv FROM MotoristasVeiculos mv ");
            sql.append(" WHERE mv.veiculoOrion = :veiculo ");

            Query query = entityManager.createQuery(sql.toString(), MotoristasVeiculos.class)
                    .setParameter("veiculo", veiculo);
            List<MotoristasVeiculos> veiculos = query.getResultList();
            return veiculos;
        } catch (Exception ex) {
            LogSistema.logError("getByVeiculo:", ex);
        }

        return null;
    }


    public List<MotoristasVeiculos> getByDriver(Integer motoristasId) {

        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT mv FROM MotoristasVeiculos mv ");
            sql.append(" WHERE mv.motoristasId = :motoristasId and mv.ativo = true ");

            Query query = entityManager.createQuery(sql.toString(), MotoristasVeiculos.class)
                    .setParameter("motoristasId", motoristasId);
            List<MotoristasVeiculos> veiculos = query.getResultList();
            return veiculos;
        } catch (Exception ex) {
            LogSistema.logError("getByVeiculo:", ex);
        }

        return null;
    }

    public String buscarNomeMotoristaUltimoVeiculo(Integer idDoVeiculo){
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select CONCAT( m2.nome, ' ', m2.sobrenome) as nome from motoristas_veiculos mv  " +
                    " inner join motoristas m2 on m2.id  = mv.motoristas_id  " +
                    " where mv.veiculo_orion = ? and ativo = true" +
                    " union " +
                    " select nome as nome from operador_veiculo ov " +
                    " inner join operador o on o.id = ov.operador_id  " +
                    " where ov.veiculo_orion = ? ";
            connection = DataSourcePostgres.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idDoVeiculo);
            preparedStatement.setInt(2, idDoVeiculo);
            resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                String nome = resultSet.getString("nome");
                return nome;
            }
        }catch (Exception e){
            LogSistema.logError("buscarNomeMotoristaUltimoVeiculo",e);
        }finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarLocais", ex);
            }
        }
        return "";
    }


    public Motoristas buscarMotoristaVeiculo(Integer idDoVeiculo){
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select m2.id, m2.nome, m2.sobrenome from motoristas_veiculos mv  " +
                    " inner join motoristas m2 on m2.id = mv.motoristas_id  " +
                    " where veiculo_orion = ? order by data_inicio desc limit 1";
            connection = DataSourcePostgres.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idDoVeiculo);
            resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                Motoristas motoristas = new Motoristas();
                Integer id = resultSet.getInt("id");
                String nome = resultSet.getString("nome");
                String sobrenome = resultSet.getString("sobrenome");
                motoristas.setId(id);
                motoristas.setNome(nome);
                motoristas.setSobrenome(sobrenome);
                return motoristas;
            }
        }catch (Exception e){
            LogSistema.logError("buscarNomeMotoristaUltimoVeiculo",e);
        }finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarLocais", ex);
            }
        }
        return null;
    }


    public List<Motoristas> buscarMotoristaVeiculoPorPeriodo(Integer idDoVeiculo, Date dataInicial, Date dataFinal){
        List<Motoristas> lista = new ArrayList<>();
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select m2.id, m2.nome, m2.sobrenome from motoristas_veiculos mv  " +
                    " inner join motoristas m2 on m2.id = mv.motoristas_id  " +
                    " where veiculo_orion = ? and mv.data_inicio between ? and ? ";
            connection = DataSourcePostgres.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idDoVeiculo);
            preparedStatement.setTimestamp(2, new Timestamp(dataInicial.getTime()));
            preparedStatement.setTimestamp(3, new Timestamp(dataFinal.getTime()));
            resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                Motoristas motoristas = new Motoristas();
                Integer id = resultSet.getInt("id");
                String nome = resultSet.getString("nome");
                String sobrenome = resultSet.getString("sobrenome");
                motoristas.setId(id);
                motoristas.setNome(nome);
                motoristas.setSobrenome(sobrenome);
                if(!lista.contains(motoristas)){
                    lista.add(motoristas);
                }
            }
        }catch (Exception e){
            LogSistema.logError("buscarMotoristaVeiculoPorPeriodo",e);
        }finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarLocais", ex);
            }
        }
        return null;
    }

    public Integer buscarIdDoVeiculoPeloMotorista(Integer idDoMotorista){
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = " select veiculo_orion  " +
                    "  from motoristas_veiculos " +
                    " where motoristas_id = ? and ativo = true ";
            connection = DataSourcePostgres.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idDoMotorista);
            resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                Integer id = resultSet.getInt("veiculo_orion");
                return id;
            }
        }catch (Exception e){
            LogSistema.logError("buscarIdDoVeiculoPeloMotorista",e);
        }finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarIdDoVeiculoPeloMotorista", ex);
            }
        }
        return null;
    }
}
