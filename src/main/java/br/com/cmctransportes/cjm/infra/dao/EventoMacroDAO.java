package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.EventoMacro;
import br.com.cmctransportes.cjm.domain.entities.Macros;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class EventoMacroDAO extends GenericDAO<Integer, EventoMacro> {
    public EventoMacroDAO(EntityManager entityManager) {
        super(entityManager);
    }


    public List<EventoMacro> buscarListaDeMacros(Macros macros){
        try {
            Query q = entityManager.createQuery("FROM EventoMacro o WHERE o.idMacro = :idMacro");
            q.setParameter("idMacro", macros.getId());
            List<EventoMacro> result = q.getResultList();
            return result;
        }catch (Exception e){

        }
        return null;
    }

}
