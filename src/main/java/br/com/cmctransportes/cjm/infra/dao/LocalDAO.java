package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.comum.LocalComum;
import br.com.cmctransportes.cjm.domain.entities.vo.CoordenadaVO;
import br.com.cmctransportes.cjm.domain.entities.vo.EmpresaVO;
import br.com.cmctransportes.cjm.domain.entities.vo.LocalVO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgres;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgresOrion;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

public class LocalDAO {

    public List<LocalVO> buscarLocais(Integer idClienteTrajetto){
        List<LocalVO> lista = LocalComum.getListaLocal();
        if(Objects.isNull(lista) || lista.isEmpty()){
            lista = buscarLocaisViaBanco(idClienteTrajetto);
        }else{
            //filtrar lista pelo ID
           lista = lista.stream().filter(l->Objects.equals(l.getCliente().getId(), idClienteTrajetto)).collect(Collectors.toList());
        }
        //remover os locais sem coordenadas
        lista.removeIf(localVO -> localVO.getListaDeCoordenadas() == null || localVO.getListaDeCoordenadas().isEmpty());
        return lista;
    }


    private List<LocalVO> buscarLocaisViaBanco(Integer idClienteTrajetto){
        List<LocalVO> lista = new ArrayList<>();
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select l.id, l.nome, l.permite_descanso, latitude, longitude, sequencia, is_raio, raio from local l  " +
                    " inner join coordenada c on c.local = l.id " +
                    " where cliente = ? and l.ativo = true and l.excluido = false;";
            connection = DataSourcePostgresOrion.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idClienteTrajetto);
            resultSet = preparedStatement.executeQuery();
            Map<Integer, LocalVO> map = new HashMap<>();
            while (resultSet.next()) {

                Integer idLocal = resultSet.getInt("id");
                String nomeLocal = resultSet.getString("nome");
                Boolean permiteDescanco = resultSet.getBoolean("permite_descanso");
                Double latitude = resultSet.getDouble("latitude");
                Double longitude = resultSet.getDouble("longitude");
                Integer sequencia = resultSet.getInt("sequencia");
                Boolean isRaio = resultSet.getBoolean("is_raio");
                Double raio = resultSet.getDouble("raio");

                CoordenadaVO coordenadaVO = new CoordenadaVO();
                coordenadaVO.setRaio(raio);
                coordenadaVO.setIsRaio(isRaio);
                coordenadaVO.setSequencia(sequencia);
                coordenadaVO.setLongitude(longitude);
                coordenadaVO.setLatitude(latitude);

                if(map.containsKey(idLocal)){
                    map.get(idLocal).getListaDeCoordenadas().add(coordenadaVO);
                }else{
                    LocalVO localVO = new LocalVO();
                    localVO.setId(idLocal);
                    localVO.setNome(nomeLocal);
                    localVO.setPermiteDescanso(permiteDescanco);
                    List<CoordenadaVO> ll = new ArrayList<>();
                    ll.add(coordenadaVO);
                    localVO.setListaDeCoordenadas(ll);
                    map.put(idLocal, localVO);
                }
            }

            lista = map.values().stream().collect(Collectors.toList());

        }catch (Exception e){
            LogSistema.logError("buscarLocais", e);
        }finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarLocais", ex);
            }
        }
        //remover os locais sem coordenadas
        lista.removeIf(localVO -> localVO.getListaDeCoordenadas() == null || localVO.getListaDeCoordenadas().isEmpty());
        return lista;
    }

    private void buscarCoordenadas(List<LocalVO> listaDeLocais){
       for(LocalVO localVO : listaDeLocais){
          List<CoordenadaVO> lista = buscarListaCoordenada(localVO);
          localVO.setListaDeCoordenadas(lista);
       }
    }


    public List<LocalVO> buscarLocaisExibicao(Integer idClienteTrajetto){
        List<LocalVO> lista = new ArrayList<>();
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select id, nome, mostrar_nome_mapa  from local where cliente = ? and ativo = true and excluido = false and mostrar_mapa_principal = true;";
            connection = DataSourcePostgresOrion.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idClienteTrajetto);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                LocalVO localVO = new LocalVO();
                localVO.setId(resultSet.getInt("id"));
                localVO.setNome(resultSet.getString("nome"));
                localVO.setMostrarNomeNoMapa(resultSet.getBoolean("mostrar_nome_mapa"));
                lista.add(localVO);
            }
        }catch (Exception e){
            LogSistema.logError("buscarLocais", e);
        }finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarLocais", ex);
            }
            return lista;
        }
    }


    public List<CoordenadaVO> buscarListaCoordenada(LocalVO localVO){
        List<CoordenadaVO> lista = new ArrayList<>();
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select latitude, longitude, sequencia, is_raio, raio from coordenada  where local = ? order by sequencia";
            connection = DataSourcePostgresOrion.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, localVO.getId());
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                CoordenadaVO coordenadaVO = new CoordenadaVO();
                coordenadaVO.setLatitude(resultSet.getDouble("latitude"));
                coordenadaVO.setLongitude(resultSet.getDouble("longitude"));
                coordenadaVO.setSequencia(resultSet.getInt("sequencia"));
                coordenadaVO.setIsRaio(resultSet.getBoolean("is_raio"));
                coordenadaVO.setRaio(resultSet.getDouble("raio"));
                lista.add(coordenadaVO);
            }
        }catch (Exception e){
            LogSistema.logError("buscarListaCoordenada", e);
        }finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarListaCoordenada", ex);
            }
            return lista;
        }
    }

    public List<LocalVO> buscarTodosLocais(){
        List<LocalVO> lista = new ArrayList<>();
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select l.id, l.nome, l.permite_descanso, latitude, longitude, sequencia, is_raio, raio, cliente from local l  " +
                    " inner join coordenada c on c.local = l.id " +
                    " where l.ativo = true and excluido = false;";
            connection = DataSourcePostgresOrion.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            Map<Integer, LocalVO> map = new HashMap<>();
            while (resultSet.next()) {

                Integer idLocal = resultSet.getInt("id");
                String nomeLocal = resultSet.getString("nome");
                Boolean permiteDescanco = resultSet.getBoolean("permite_descanso");
                Double latitude = resultSet.getDouble("latitude");
                Double longitude = resultSet.getDouble("longitude");
                Integer sequencia = resultSet.getInt("sequencia");
                Boolean isRaio = resultSet.getBoolean("is_raio");
                Double raio = resultSet.getDouble("raio");
                Integer cliente = resultSet.getInt("cliente");

                CoordenadaVO coordenadaVO = new CoordenadaVO();
                coordenadaVO.setRaio(raio);
                coordenadaVO.setIsRaio(isRaio);
                coordenadaVO.setSequencia(sequencia);
                coordenadaVO.setLongitude(longitude);
                coordenadaVO.setLatitude(latitude);

                if(map.containsKey(idLocal)){
                    map.get(idLocal).getListaDeCoordenadas().add(coordenadaVO);
                }else{
                    LocalVO localVO = new LocalVO();
                    localVO.setId(idLocal);
                    localVO.setNome(nomeLocal);
                    localVO.setPermiteDescanso(permiteDescanco);
                    List<CoordenadaVO> ll = new ArrayList<>();
                    ll.add(coordenadaVO);
                    localVO.setListaDeCoordenadas(ll);
                    localVO.setCliente(new EmpresaVO(cliente));
                    map.put(idLocal, localVO);
                }
            }

            lista = map.values().stream().collect(Collectors.toList());

        }catch (Exception e){
            LogSistema.logError("buscarLocais", e);
        }finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarLocais", ex);
            }
        }
        //remover os locais sem coordenadas
        lista.removeIf(localVO -> localVO.getListaDeCoordenadas() == null || localVO.getListaDeCoordenadas().isEmpty());
        return lista;
    }

}
