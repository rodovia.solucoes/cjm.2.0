package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.Dashboard;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class DashboardDAO extends GenericDAO<Integer, Dashboard>  {

    public DashboardDAO(EntityManager entityManager) {
        super(entityManager);
        this.entityManager = entityManager;
    }

    public Dashboard buscarDashboard(Integer idEmpresa, Integer idUnidade, Integer idUsuario) {
        try {
            StringBuilder sql = new StringBuilder(0);
            sql.append("SELECT j FROM Dashboard j WHERE j.idEmpresa = :idEmpresa and j.idUnidade = :idUnidade and j.idUsuario = :idUsuario");

            Query query = entityManager.createQuery(sql.toString(), Dashboard.class)
                    .setParameter("idEmpresa", idEmpresa)
                    .setParameter("idUnidade", idUnidade)
                    .setParameter("idUsuario", idUsuario);
            List<Dashboard> lista = query.getResultList();
            if(!lista.isEmpty()){
                return lista.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

}
