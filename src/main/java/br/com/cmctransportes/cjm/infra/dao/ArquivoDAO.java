package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.Arquivo;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class ArquivoDAO extends GenericDAO<Integer, Arquivo>  {

    private EntityManager em;

    public ArquivoDAO(EntityManager entityManager) {
        super(entityManager);
        this.em = entityManager;
    }

    public List<Arquivo> findByContrato(Integer contrato) {
        List<Arquivo> arquivos = null;
        try {
            String sql = "FROM Arquivo WHERE contrato = :contrato";
            Query query = entityManager.createQuery(sql).setParameter("contrato", contrato);
            arquivos = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return arquivos;
    }

}
