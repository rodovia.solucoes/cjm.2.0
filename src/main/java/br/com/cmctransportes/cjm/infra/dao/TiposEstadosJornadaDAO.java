package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.TiposEstadoJornada;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 * @author William Leite
 */
public class TiposEstadosJornadaDAO extends GenericDAO<Integer, TiposEstadoJornada> {

    public TiposEstadosJornadaDAO(EntityManager entityManager) {
        super(entityManager);
    }

    public List<TiposEstadoJornada> findByName(String nome) {
        List<TiposEstadoJornada> result = null;

        try {
            String sql = "FROM TiposEstadoJornada WHERE descricao LIKE :descricao";
            Query query = entityManager.createQuery(sql).setParameter("descricao", "%" + nome + "%");
            result = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return result;
    }
}
