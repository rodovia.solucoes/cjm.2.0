package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.IbuttonVeiculoMotorista;

import javax.persistence.EntityManager;

public class IbuttonVeiculoMotoristaDAO extends GenericDAO<Integer, IbuttonVeiculoMotorista> {


    public IbuttonVeiculoMotoristaDAO(EntityManager entityManager) {
        super(entityManager);
    }
}
