package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.Motoristas;
import br.com.cmctransportes.cjm.domain.entities.Sascar;
import br.com.cmctransportes.cjm.domain.repository.SascarRepository;
import br.com.cmctransportes.cjm.logger.LogSistema;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.sql.SQLException;
import java.util.List;


public class SascarDAO extends GenericDAO<Integer, Sascar> {

    @Autowired
    private SascarRepository sascarRepository;

    /**
     * Classe responsável pelas consultas na tabela SASCAR do banco de dados
     * @param entityManager instancia necessária para as consultas
     */
    public SascarDAO(EntityManager entityManager) {
        super(entityManager);
    }

    /**
     * Consulta no banco tabela Sascar qual motorista viaM está releacionado ao Id na Sascar para as consultas SOAP
     * @param id operador lançamento
     * @return o valor correspondente a consulta pelo id do usuario sasrcar do motorista requisitado
     */
    public Sascar getByIdSascar(Integer id) {
        try{
            Query query = entityManager.createQuery("FROM Sascar s WHERE s.operadorLancamento = :id");
            query.setParameter("id", id);

            List<Sascar> result = query.getResultList();
            if(result.isEmpty()){
                return null;
            }else{
                return result.get(0);
            }
        }catch (Exception e){
            LogSistema.logError("getByIdSascar", e);
        }
       return null;
    }

    /**
     * Retorna todos dados da tabela Sascar
     * @return uma lista de objetos Sascar
     */
    public List<Sascar> findAllSascar() {
        Query query = entityManager.createQuery("FROM Sascar where (cargaManual = false or cargaManual is null)");
        List<Sascar> result =  (List<Sascar>) query.getResultList();
        return result;
    }


    public List<Sascar> findAllSascar(Boolean isManual) {
        Query query = entityManager.createQuery("FROM Sascar where cargaManual = :cargaManual");
        query.setParameter("cargaManual", isManual);
        List<Sascar> result =  (List<Sascar>) query.getResultList();
        return result;
    }


    /**
     * Consulta no banco e retorna um motorista específico para ser comparado se existe ou não no banco o motorista cadastrado
     * @param driver parametro necessário para consulta se existe
     * @return objeto Sacar consultado ou retorna null se não existir
     * @throws SQLException caso não exista
     */
    public Integer verificaId(Integer driver) throws SQLException {
        Integer result = null;
        try {
            Query query = entityManager.createQuery("FROM Sascar s WHERE s.operadorLancamento = :idDriver");
            query.setParameter("idDriver", driver);
            Sascar ms = (Sascar) query.getResultList();
            result = ms.getOperadorLancamento();
            return result;
        } catch (Exception e) {
            result = 0;

            return result;
        }
    }

    /**
     * Alterar o usuário sascar se no cadastro de motorista for alterado os dados do ID Sascar
     * @param entity Objeto Sascar que será alterado do motorista p ID Sascar na tabela Sascar
     */
    public void updateSascar(Motoristas entity) {
        Integer driver = entity.getId();
        Integer idsascar = entity.getSascarId();
        try {
            Query query = entityManager.createQuery("FROM Sascar s WHERE s.operadorLancamento = :idDriver");
            query.setParameter("idDriver", driver);
            Sascar ms = (Sascar) query.getResultList();
            ms.setIdVeiculoSascar(idsascar);
            this.sascarRepository.save(ms);
        } catch (Exception e) {
            System.out.println("Motorista não cadastrado para eventos via Sascar");
        }
    }

    /**
     * Consulta a ultima data registrada no banco tabela Sascar do motorista quando ele foi buscado anteriormente
     * @param driver motorista cadastrado no SOAP Sascar
     * @return a data da ultima consulta
     */
    public long findDateSascar(Integer driver) {
        Query query = entityManager.createQuery("FROM Sascar s WHERE s.operadorLancamento = :idDriver");
        query.setParameter("idDriver", driver);
        Sascar ms = (Sascar) query.getSingleResult();
        long result = ms.getStartDate().getTime();
        return result;
    }


    public Sascar getByIdVeiculo(Integer id) {
        try {
            Query query = entityManager.createQuery("FROM Sascar s WHERE s.idVeiculoSascar = :id");
            query.setParameter("id", id);
            List<Sascar> result =  query.getResultList();
            if(result.isEmpty()){
                return null;
            }else{
                return result.get(0);
            }
        }catch (Exception e){
            LogSistema.logError("getByIdVeiculo", e);
        }
        return null;

    }
}
