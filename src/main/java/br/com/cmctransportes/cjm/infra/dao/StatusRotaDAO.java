package br.com.cmctransportes.cjm.infra.dao;

import br.com.cmctransportes.cjm.domain.entities.vo.RotaVO;
import br.com.cmctransportes.cjm.domain.entities.vo.StatusRotaVO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgresOrion;

import java.sql.*;
import java.util.Date;

public class StatusRotaDAO {

    public StatusRotaVO buscarUltimoStatus(Integer idVeiculo){
        StatusRotaVO statusRotaVO = null;
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try{
            String sql = "select * from status_rota where veiculo = ? order by id desc limit 1";
            connection = DataSourcePostgresOrion.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idVeiculo);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                Integer id = resultSet.getInt("id");
                Integer veiculo = resultSet.getInt("veiculo");
                Integer rota = resultSet.getInt("rota");
                Date data = new Date(resultSet.getTimestamp("data_cadastro").getTime());
                Integer transmissao = resultSet.getInt("transmissao");
                Boolean dentroRota = resultSet.getBoolean("dentro_rota");
                statusRotaVO = new StatusRotaVO();
                statusRotaVO.setDataCadastro(data);
                statusRotaVO.setId(id);
                statusRotaVO.setVeiculo(veiculo);
                statusRotaVO.setRota(rota);
                statusRotaVO.setTransmissao(transmissao);
                statusRotaVO.setDentroRota(dentroRota);
            }
        }catch (Exception e) {
            LogSistema.logError("buscarUltimoStatus", e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarUltimoStatus", ex);
            }
        }
        return statusRotaVO;
    }


    public String buscarNomeRota(Integer idRota){
        RotaVO rotaVO = null;
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try{
            String sql = "select CONCAT('De ', " +
                    " (select nome from local where id = local_inicio), " +
                    " ' para ', " +
                    " (select nome from local where id = local_final)) as nome " +
                    " from rota where id = ?";
            connection = DataSourcePostgresOrion.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idRota);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
               String nome = resultSet.getString("nome");
               return nome;
            }
        }catch (Exception e) {
            LogSistema.logError("buscarNomeRota", e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarNomeRota", ex);
            }
        }
        return "";
    }


    public void excluirStatusRota(Integer idVeiculo){
        RotaVO rotaVO = null;
        Connection connection = null;
        Statement stmt = null;
        try{
            String sql = "delete from status_rota where veiculo = "+idVeiculo.toString();
            connection = DataSourcePostgresOrion.getInstance().getConnection();
            stmt = connection.createStatement();
            stmt.executeUpdate(sql);
        }catch (Exception e) {
            LogSistema.logError("excluirStatusRota", e);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarNomeRota", ex);
            }
        }
    }

}
