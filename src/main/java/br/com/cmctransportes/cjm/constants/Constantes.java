package br.com.cmctransportes.cjm.constants;

import java.io.File;

public class Constantes {
    private static final String DIRETORIO_PROPERTIES_WINDOWS = "C:\\projetos\\raiz\\cjm\\";
    private static final String DIRETORIO_PROPERTIES_LINUX = "/home/sistemas/rodovia/";
    private static final String DIRETORIO_TOMCAT_WINDOWS = "C:\\xampp\\htdocs\\";
    private static final String DIRETORIO_TOMCAT_LINUX = "/home/servidor/tomcat/webapps/FrotaWeb/";

    private static boolean isWindows = false;

    private static String buscarDiretorioProperties() {
        String so = System.getProperty("os.name");
        if (so.contains("Windows")) {
            isWindows = true;
            return DIRETORIO_PROPERTIES_WINDOWS;
        }
        return DIRETORIO_PROPERTIES_LINUX;
    }
    private static String buscarDiretorioTomCat(){
        String so = System.getProperty("os.name");
        if (so.contains("Windows")) {
            isWindows = true;
            return DIRETORIO_TOMCAT_WINDOWS;

        }
        return DIRETORIO_TOMCAT_LINUX;
    }

    public static String DIRETORIO_DO_LOG = buscarDiretorioProperties().concat("log".concat(File.separator));
    public static String DIRETORIO_DO_LOG_SASCAR = buscarDiretorioProperties().concat("logsascar".concat(File.separator));
    public static String DIRETORIO_IMAGENS_LOGOMARCA = buscarDiretorioProperties().concat("logomarca".concat(File.separator));
    public static String DIRETORIO_JASPER = buscarDiretorioProperties().concat("jasper".concat(File.separator));
    public static String DIRETORIO_XML_INTEGRACAO = buscarDiretorioProperties().concat("xml".concat(File.separator));
    public static String DIRETORIO_TOMCAT_RELATORIO = buscarDiretorioTomCat().concat("relatorios".concat(File.separator));


    public static String URL_SERVIDOR_RELATORIO = isWindows?"http://localhost/relatorios/":"http://45.55.80.2:8081/FrotaWeb/relatorios/";
    //SERVIDOR GERAL
    //public static String URL_SERVICO_TRAJETTO = "http://localhost:8080/orionprod/webservice";

    //PULICADO NO SERVIDOR SBH
    //public static String URL_SERVICO_TRAJETTO = "http://45.55.80.2:8080/orionprodsbh/webservice";
    //public static String URL_SERVICO_TRAJETTO = "http://localhost:8080/orion/webservice";//maquina dev
    //Servidor geral
    public static String URL_SERVICO_TRAJETTO = "http://45.55.80.2:8080/orionprod/webservice";
    //Servidor BH
    //public static String URL_SERVICO_TRAJETTO = "http://209.97.147.75:8080/orionprod/webservice";
    //URL PRODUCAO GERAL
    public static String URL_ARCHERHAWK = "http://localhost:8081/archerhawk/api";
    //ACESSAR SERVIDOR SBH
    //public static String URL_ARCHERHAWK = "http://209.97.147.75:8081/archerhawk/api";
    //public static String URL_ARCHERHAWK = "http://45.55.80.2:8081/archerhawk/api";
    //ACTIVMQ GERAL
    public static String ACTIVEMQ_BROKER_URI = "tcp://157.245.242.159:61616";
    //ACTIVMQ SBH
    //public static String ACTIVEMQ_BROKER_URI = "tcp://localhost:61616";
    public static String ACTIVEMQ_USERNAME = "trajetto";
    public static String ACTIVEMQ_PASSWORD = "vcscastro08032003";
    public static String FILA_ENTRADA_DADOS = "RODOVIA_RASTRO";
    public static String EMBARCAR_IBUTTON = "EMBARCAR_IBUTTON";

    public static String ADMIN = "admin";
    public static String APURACAO = "apuracao";

    public static Integer NUMERO_DE_THREADS = 3;

    public static Boolean INICIAR_JOBS = false;

    public static String ID_TRUCKS_CONTROL = "30257384000193";
    public static String SENHA_TRUCKS_CONTROL = "187760";

    public static Integer TEMPO_SECONDOS_INTERJORNADA = 39600; //Equivale a 11 horas
    public static Integer TEMPO_MILESEGUNDOS_INTERJORNADA = 39600000; //Equivale a 11 horas
    public static Long MINUTO_ADICIONAL_JORNADA = 60000L;
}
