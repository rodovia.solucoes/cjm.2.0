package br.com.cmctransportes.cjm.jobs;

import br.com.cmctransportes.cjm.comum.SascarComum;
import br.com.cmctransportes.cjm.constants.Constantes;
import br.com.cmctransportes.cjm.domain.entities.Sascar;
import br.com.cmctransportes.cjm.domain.services.SascarService;
import br.com.cmctransportes.cjm.domain.services.SascarVeiculoService;
import br.com.cmctransportes.cjm.logger.LogSistema;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class SascarJob implements Job {
    @Autowired
    private SascarService sascarService;
    @Autowired
    private SascarVeiculoService sascarVeiculoService;

    @Override
    public void execute(JobExecutionContext jec) throws JobExecutionException {
        try {

                if (SascarComum.cont == 0) {
                    List<Sascar> lista = sascarService.findAllSascar();
                    SascarComum.setListaSascar(lista);
                    SascarComum.cont++;
                } else {
                    if (SascarComum.cont >= 50) {
                        SascarComum.cont = 0;
                    } else {
                        SascarComum.cont++;
                    }
                }

        } catch (Exception e) {
            LogSistema.logError("SascarJob", e);
        }
    }
}
