package br.com.cmctransportes.cjm.jobs;

import br.com.cmctransportes.cjm.comum.LocalComum;
import br.com.cmctransportes.cjm.domain.entities.vo.LocalVO;
import br.com.cmctransportes.cjm.infra.dao.LocalDAO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.List;

public class LocalJob implements Job {
    @Override
    public void execute(JobExecutionContext jec) throws JobExecutionException {
        try {
            if(LocalComum.cont == 0) {
                List<LocalVO> lista = new LocalDAO().buscarTodosLocais();
                LocalComum.setListaLocal(lista);
                LocalComum.cont++;
            }else{
                if(LocalComum.cont >= 50){
                    LocalComum.cont = 0;
                }else{
                    LocalComum.cont++;
                }
            }
        } catch (Exception e) {
            LogSistema.logError("buscarListaTotalDeLocais", e);
        }
    }
}
