package br.com.cmctransportes.cjm.jobs;

import org.quartz.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class ConfigureQuartzJob {

    @Bean(name="jobOne")
    public JobDetail jobADetails() {
        return JobBuilder.newJob(LocalJob.class).withIdentity("localJob")
                .storeDurably().build();
    }

    @Bean(name="jobTwo")
    public JobDetail jobMacroSascar() {
        return JobBuilder.newJob(MacroSascarJob.class).withIdentity("macroSascarJob")
                .storeDurably().build();
    }

    @Bean(name="jobThree")
    public JobDetail jobSascar() {
        return JobBuilder.newJob(SascarJob.class).withIdentity("sascarJob")
                .storeDurably().build();
    }

    @Bean
    public Trigger jobATrigger(@Qualifier("jobOne")JobDetail jobADetails) {
        return TriggerBuilder.newTrigger().forJob(jobADetails)
                .withIdentity("TriggerA")
                .withSchedule(CronScheduleBuilder.cronSchedule("0/30 * * ? * * *"))
                .build();
    }


    @Bean
    public Trigger jobMacroSascarTrigger(@Qualifier("jobTwo")JobDetail jobADetails) {
        return TriggerBuilder.newTrigger().forJob(jobADetails)
                .withIdentity("jobMacroSascarTrigger")
                .withSchedule(CronScheduleBuilder.cronSchedule("0/30 * * ? * * *"))
                .build();
    }

   @Bean
    public Trigger jobSascarTrigger(@Qualifier("jobThree")JobDetail jobADetails) {
        return TriggerBuilder.newTrigger().forJob(jobADetails)
                .withIdentity("jobSascarTrigger")
                .withSchedule(CronScheduleBuilder.cronSchedule("0/30 * * ? * * *"))
                .build();
    }

}
