package br.com.cmctransportes.cjm.jobs;

import br.com.cmctransportes.cjm.comum.MacroSascarComum;
import br.com.cmctransportes.cjm.domain.entities.MacroSascar;
import br.com.cmctransportes.cjm.domain.services.MacroSarcarService;
import br.com.cmctransportes.cjm.logger.LogSistema;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class MacroSascarJob implements Job {
    @Autowired
    private MacroSarcarService macroSarcarService;

    @Override
    public void execute(JobExecutionContext jec) throws JobExecutionException {
        try {
            if (MacroSascarComum.cont == 0) {
                List<MacroSascar> lista = macroSarcarService.buscarTodas();
                MacroSascarComum.setListaMacroSascar(lista);
                MacroSascarComum.cont++;
            } else {
                if (MacroSascarComum.cont >= 50) {
                    MacroSascarComum.cont = 0;
                } else {
                    MacroSascarComum.cont++;
                }
            }
        } catch (Exception e) {
            LogSistema.logError("MacroSascarJob", e);
        }
    }
}
