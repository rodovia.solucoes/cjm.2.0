package br.com.cmctransportes.cjm.logger;

/**
 *
 * @author vinicius
 */
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import br.com.cmctransportes.cjm.constants.Constantes;
import org.apache.log4j.Appender;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

public class LogSistema {

    private static final String path = Constantes.DIRETORIO_DO_LOG;
    public static Logger logger = null;
    private static String currentLogPrefix;

    public static synchronized void configuraLog4j() {
        String logPrefix = new SimpleDateFormat("dd_MM_yyyy").format(new Date());
        if (logger != null && currentLogPrefix != null && logPrefix.equals(currentLogPrefix)) {
            return;
        }
        currentLogPrefix = logPrefix;
        logger = Logger.getRootLogger();

        try {

            PatternLayout layout = new PatternLayout(
                    "%d{dd/MM/yyyy HH:mm:ss,SSS} " + PatternLayout.TTCC_CONVERSION_PATTERN);
            Appender fileAppender = new FileAppender(layout,
                    path + "cjm_" + logPrefix + ".log");
            logger.removeAllAppenders();
            logger.addAppender(fileAppender);
            logger.setLevel(Level.INFO);
            BasicConfigurator.configure();
        } catch (Exception e) {
        }
    }

    public static synchronized void logDebug(String message, Throwable throwable) {
        configuraLog4j();
        if (throwable == null) {
            logger.debug(message);
        } else {
            logger.debug(message.concat(getStackTrace(throwable)));
        }
    }

    public static synchronized void logDebug(String message) {
        logDebug(message, null);
    }

    public static synchronized void logError(String message, Throwable throwable) {
        configuraLog4j();
        if (throwable == null) {
            logger.error(message);
        } else {
            logger.error(message.concat(getStackTrace(throwable)));
        }

    }

    public static synchronized void logInfo(String message, Throwable throwable) {
        configuraLog4j();
        if (throwable == null) {
            logger.info(message);
        } else {
            logger.info(message.concat(getStackTrace(throwable)));
        }
    }

    public static synchronized void logInfo(String message) {
        logInfo(message, null);
    }

    public static synchronized void logWarning(String message, Throwable throwable) {
        configuraLog4j();
        if (throwable == null) {
            logger.warn(message);
        } else {
            logger.warn(message.concat(getStackTrace(throwable)));
        }
    }

    public static synchronized void logWarning(String message) {
        logWarning(message, null);

    }

    private static synchronized String getStackTrace(Throwable t) {
        StringWriter stringWritter = new StringWriter();
        PrintWriter printWritter = new PrintWriter(stringWritter, true);
        t.printStackTrace(printWritter);
        printWritter.flush();
        stringWritter.flush();

        return stringWritter.toString();
    }
}
