
package br.com.cmctransportes.cjm.sascarsoap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoEventoTelemetriaDescricao complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoEventoTelemetriaDescricao">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="eventoDescricao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="eventoTipo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idEvento" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoEventoTelemetriaDescricao", propOrder = {
    "eventoDescricao",
    "eventoTipo",
    "idEvento"
})
public class TipoEventoTelemetriaDescricao {

    protected String eventoDescricao;
    protected String eventoTipo;
    protected Integer idEvento;

    /**
     * Gets the value of the eventoDescricao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventoDescricao() {
        return eventoDescricao;
    }

    /**
     * Sets the value of the eventoDescricao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventoDescricao(String value) {
        this.eventoDescricao = value;
    }

    /**
     * Gets the value of the eventoTipo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventoTipo() {
        return eventoTipo;
    }

    /**
     * Sets the value of the eventoTipo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventoTipo(String value) {
        this.eventoTipo = value;
    }

    /**
     * Gets the value of the idEvento property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdEvento() {
        return idEvento;
    }

    /**
     * Sets the value of the idEvento property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdEvento(Integer value) {
        this.idEvento = value;
    }

}
