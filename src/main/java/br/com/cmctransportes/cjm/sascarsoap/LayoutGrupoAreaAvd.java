
package br.com.cmctransportes.cjm.sascarsoap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for layoutGrupoAreaAvd complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="layoutGrupoAreaAvd">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="clienteId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="dataAlteracao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="dataCriacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="dataExclusao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="gerenciadoraId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="logEfetivoDelelete" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="logEfetivoInsert" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="logEfetivoUpdate" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="logIdDelelete" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="logIdInsert" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="logIdUpdate" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="nome" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "layoutGrupoAreaAvd", propOrder = {
    "clienteId",
    "dataAlteracao",
    "dataCriacao",
    "dataExclusao",
    "gerenciadoraId",
    "id",
    "logEfetivoDelelete",
    "logEfetivoInsert",
    "logEfetivoUpdate",
    "logIdDelelete",
    "logIdInsert",
    "logIdUpdate",
    "nome"
})
public class LayoutGrupoAreaAvd {

    protected Integer clienteId;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataAlteracao;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataCriacao;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataExclusao;
    protected Integer gerenciadoraId;
    protected Integer id;
    protected Integer logEfetivoDelelete;
    protected Integer logEfetivoInsert;
    protected Integer logEfetivoUpdate;
    protected Integer logIdDelelete;
    protected Integer logIdInsert;
    protected Integer logIdUpdate;
    protected String nome;

    /**
     * Gets the value of the clienteId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getClienteId() {
        return clienteId;
    }

    /**
     * Sets the value of the clienteId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setClienteId(Integer value) {
        this.clienteId = value;
    }

    /**
     * Gets the value of the dataAlteracao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataAlteracao() {
        return dataAlteracao;
    }

    /**
     * Sets the value of the dataAlteracao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataAlteracao(XMLGregorianCalendar value) {
        this.dataAlteracao = value;
    }

    /**
     * Gets the value of the dataCriacao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataCriacao() {
        return dataCriacao;
    }

    /**
     * Sets the value of the dataCriacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataCriacao(XMLGregorianCalendar value) {
        this.dataCriacao = value;
    }

    /**
     * Gets the value of the dataExclusao property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataExclusao() {
        return dataExclusao;
    }

    /**
     * Sets the value of the dataExclusao property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataExclusao(XMLGregorianCalendar value) {
        this.dataExclusao = value;
    }

    /**
     * Gets the value of the gerenciadoraId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getGerenciadoraId() {
        return gerenciadoraId;
    }

    /**
     * Sets the value of the gerenciadoraId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setGerenciadoraId(Integer value) {
        this.gerenciadoraId = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setId(Integer value) {
        this.id = value;
    }

    /**
     * Gets the value of the logEfetivoDelelete property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLogEfetivoDelelete() {
        return logEfetivoDelelete;
    }

    /**
     * Sets the value of the logEfetivoDelelete property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLogEfetivoDelelete(Integer value) {
        this.logEfetivoDelelete = value;
    }

    /**
     * Gets the value of the logEfetivoInsert property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLogEfetivoInsert() {
        return logEfetivoInsert;
    }

    /**
     * Sets the value of the logEfetivoInsert property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLogEfetivoInsert(Integer value) {
        this.logEfetivoInsert = value;
    }

    /**
     * Gets the value of the logEfetivoUpdate property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLogEfetivoUpdate() {
        return logEfetivoUpdate;
    }

    /**
     * Sets the value of the logEfetivoUpdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLogEfetivoUpdate(Integer value) {
        this.logEfetivoUpdate = value;
    }

    /**
     * Gets the value of the logIdDelelete property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLogIdDelelete() {
        return logIdDelelete;
    }

    /**
     * Sets the value of the logIdDelelete property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLogIdDelelete(Integer value) {
        this.logIdDelelete = value;
    }

    /**
     * Gets the value of the logIdInsert property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLogIdInsert() {
        return logIdInsert;
    }

    /**
     * Sets the value of the logIdInsert property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLogIdInsert(Integer value) {
        this.logIdInsert = value;
    }

    /**
     * Gets the value of the logIdUpdate property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLogIdUpdate() {
        return logIdUpdate;
    }

    /**
     * Sets the value of the logIdUpdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLogIdUpdate(Integer value) {
        this.logIdUpdate = value;
    }

    /**
     * Gets the value of the nome property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNome() {
        return nome;
    }

    /**
     * Sets the value of the nome property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNome(String value) {
        this.nome = value;
    }

}
