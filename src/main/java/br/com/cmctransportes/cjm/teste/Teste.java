package br.com.cmctransportes.cjm.teste;

import br.com.cmctransportes.cjm.domain.entities.xml.ResponseMensagemCB;
import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.methods.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.net.URL;
import java.net.URLEncoder;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class Teste {
    private static String url = "http://webservice.newrastreamentoonline.com.br";
    private static String xml =  "<RequestMensagemCB> " +
            "<login>30257384000193</login>" +
            "<senha>187760</senha>" +
            "<mId>#</mId>" +
            "</RequestMensagemCB>";;
    public static void main(String[] args) {
        try {
            HttpClient httpClient = new HttpClient();
            PostMethod postMethod = new PostMethod(url);
            postMethod.setRequestHeader("Accept", "application/soap+xml,multipart/related,text/*");
            postMethod.setRequestHeader("Cache-Control", "no-cache");
            postMethod.setRequestHeader("Pragma", "no-cache");
            postMethod.setRequestHeader("Content-Type", "text/xml; charset=utf-8");
           // String x = URLEncoder.encode(xml, "UTF-8");
            final int contentLength = xml.length();
            postMethod.setRequestHeader("Content-Length", String.valueOf(contentLength));
            postMethod.setRequestEntity(new StringRequestEntity(xml, "text/xml", "utf-8"));
            final int statusCode = httpClient.executeMethod(postMethod);
            final byte[] responseBody = postMethod.getResponseBody();
            File f = zipBytes(responseBody);
            //zipFile(responseBody, "seila.txt");

            JAXBContext jaxbContext = JAXBContext.newInstance(ResponseMensagemCB.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            ResponseMensagemCB cb = (ResponseMensagemCB) jaxbUnmarshaller.unmarshal(f);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static File zipBytes(byte[] responseBody) throws IOException {
        ZipInputStream zipStream = new ZipInputStream(new ByteArrayInputStream(responseBody));
        ZipEntry entry = null;
        File f = null;
        while ((entry = zipStream.getNextEntry()) != null) {
            String entryName = entry.getName();
            f = new File("C:\\temp\\main\\"+entryName+".xml");
            FileOutputStream out = new FileOutputStream(f);
            byte[] byteBuff = new byte[4096];
            int bytesRead = 0;
            while ((bytesRead = zipStream.read(byteBuff)) != -1)
            {
                out.write(byteBuff, 0, bytesRead);
            }
            out.close();
            zipStream.closeEntry();
        }
        zipStream.close();
        return f;
    }

    static void zipFile(byte[] classBytesArray, String entryName) {
        try {
            FileOutputStream fileOutputStream =  new FileOutputStream("C:\\temp\\main\\teste.zip");
            ZipOutputStream zos = new  ZipOutputStream(new BufferedOutputStream(fileOutputStream));
            ZipEntry entry = new ZipEntry(entryName);
            zos.putNextEntry(entry);
            zos.write(classBytesArray, 0, classBytesArray.length);
            zos.closeEntry();
            zos.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
