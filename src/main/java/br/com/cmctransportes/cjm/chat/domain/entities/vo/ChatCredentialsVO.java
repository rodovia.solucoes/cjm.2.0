package br.com.cmctransportes.cjm.chat.domain.entities.vo;

import java.io.Serializable;

/**
 * @author William Leite
 */
public class ChatCredentialsVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String jid;
    private String password;

    public ChatCredentialsVO(String jid, String password) {
        this.jid = jid;
        this.password = password;
    }

    public String getJid() {
        return jid;
    }

    public void setJid(String jid) {
        this.jid = jid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
