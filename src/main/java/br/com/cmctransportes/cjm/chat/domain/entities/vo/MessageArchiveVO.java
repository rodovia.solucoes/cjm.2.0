package br.com.cmctransportes.cjm.chat.domain.entities.vo;

import br.com.cmctransportes.cjm.chat.domain.entities.MessageArchive;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Objects;

/**
 * @author William Leite
 */
public class MessageArchiveVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private BigInteger id;
    private String fromJID;
    private String fromJIDResource;
    private String toJID;
    private String toJIDResource;
    private BigInteger sentDate;
    private String body;

    public MessageArchiveVO(MessageArchive messageArchive) {
        if (Objects.nonNull(messageArchive)) {
            this.id = messageArchive.getId();
            this.fromJID = messageArchive.getFromJID();
            this.fromJIDResource = messageArchive.getFromJIDResource();
            this.toJID = messageArchive.getToJID();
            this.toJIDResource = messageArchive.getToJIDResource();
            this.sentDate = messageArchive.getSentDate();
            this.body = messageArchive.getBody();
        }
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getFromJID() {
        return fromJID;
    }

    public void setFromJID(String fromJID) {
        this.fromJID = fromJID;
    }

    public String getFromJIDResource() {
        return fromJIDResource;
    }

    public void setFromJIDResource(String fromJIDResource) {
        this.fromJIDResource = fromJIDResource;
    }

    public String getToJID() {
        return toJID;
    }

    public void setToJID(String toJID) {
        this.toJID = toJID;
    }

    public String getToJIDResource() {
        return toJIDResource;
    }

    public void setToJIDResource(String toJIDResource) {
        this.toJIDResource = toJIDResource;
    }

    public BigInteger getSentDate() {
        return sentDate;
    }

    public void setSentDate(BigInteger sentDate) {
        this.sentDate = sentDate;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MessageArchiveVO other = (MessageArchiveVO) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "MessageArchiveVO{" + "id=" + id + ", fromJID=" + fromJID + ", fromJIDResource=" + fromJIDResource + ", toJID=" + toJID + ", toJIDResource=" + toJIDResource + ", sentDate=" + sentDate + ", body=" + body + '}';
    }
}
