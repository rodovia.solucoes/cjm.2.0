package br.com.cmctransportes.cjm.chat.infra.dao;

import br.com.cmctransportes.cjm.chat.domain.entities.Conversation;
import br.com.cmctransportes.cjm.infra.dao.GenericDAO;

import javax.persistence.EntityManager;

/**
 * @author William Leite
 */
public class ConversationDAO extends GenericDAO<Integer, Conversation> {

    public ConversationDAO(EntityManager entityManager) {
        super(entityManager);
    }

}