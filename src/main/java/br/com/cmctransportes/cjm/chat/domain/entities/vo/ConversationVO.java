package br.com.cmctransportes.cjm.chat.domain.entities.vo;

import br.com.cmctransportes.cjm.chat.domain.entities.Conversation;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author William Leite
 */
public class ConversationVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private String room;
    private Boolean isExternal;
    private BigInteger startDate;
    private BigInteger lastActivity;
    private Integer messageCount;
    private List<MessageArchiveVO> messages;

    public ConversationVO(Conversation conversation) {
        if (Objects.nonNull(conversation)) {
            this.id = conversation.getId();
            this.room = conversation.getRoom();
            this.isExternal = conversation.getIsExternal();
            this.startDate = conversation.getStartDate();
            this.lastActivity = conversation.getLastActivity();
            this.messageCount = conversation.getMessageCount();
            this.messages = conversation.getMessageArchive().stream().map(m -> new MessageArchiveVO(m)).collect(Collectors.toList());
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public Boolean getIsExternal() {
        return isExternal;
    }

    public void setIsExternal(Boolean isExternal) {
        this.isExternal = isExternal;
    }

    public BigInteger getStartDate() {
        return startDate;
    }

    public void setStartDate(BigInteger startDate) {
        this.startDate = startDate;
    }

    public BigInteger getLastActivity() {
        return lastActivity;
    }

    public void setLastActivity(BigInteger lastActivity) {
        this.lastActivity = lastActivity;
    }

    public Integer getMessageCount() {
        return messageCount;
    }

    public void setMessageCount(Integer messageCount) {
        this.messageCount = messageCount;
    }

    public List<MessageArchiveVO> getMessages() {
        return messages;
    }

    public void setMessages(List<MessageArchiveVO> messages) {
        this.messages = messages;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ConversationVO other = (ConversationVO) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ConversationVO{" + "id=" + id + ", room=" + room + ", isExternal=" + isExternal + ", startDate=" + startDate + ", lastActivity=" + lastActivity + ", messageCount=" + messageCount + ", messages=" + messages + '}';
    }
}
