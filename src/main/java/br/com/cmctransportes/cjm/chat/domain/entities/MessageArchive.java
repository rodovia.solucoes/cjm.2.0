package br.com.cmctransportes.cjm.chat.domain.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Objects;

/**
 * @author William Leite
 */
@Entity
@Table(name = "ofmessagearchive")
@XmlRootElement
public class MessageArchive implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "messageid")
    @Getter
    @Setter
    private BigInteger id;

    @ManyToOne
    @JoinColumn(name = "conversationid")
    @JsonBackReference
    @Getter
    @Setter
    private Conversation conversation;

    @Column(name = "fromjid")
    @Size(max = 1024)
    @Getter
    @Setter
    private String fromJID;

    @Column(name = "fromjidresource")
    @Size(max = 1024)
    @Getter
    @Setter
    private String fromJIDResource;

    @Column(name = "tojid")
    @Size(max = 1024)
    @Getter
    @Setter
    private String toJID;

    @Column(name = "tojidresource")
    @Size(max = 1024)
    @Getter
    @Setter
    private String toJIDResource;

    @Column(name = "sentdate")
    @Getter
    @Setter
    private BigInteger sentDate;

    @Column(name = "stanza", columnDefinition = "text")
    @Getter
    @Setter
    private String xml;

    @Column(name = "body", columnDefinition = "text")
    @Getter
    @Setter
    private String body;

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MessageArchive other = (MessageArchive) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "MessageArchive{" + "id=" + id + ", conversation=" + conversation + ", fromJID=" + fromJID + ", fromJIDResource=" + fromJIDResource + ", toJID=" + toJID + ", toJIDResource=" + toJIDResource + ", sentDate=" + sentDate + ", xml=" + xml + ", body=" + body + '}';
    }
}
