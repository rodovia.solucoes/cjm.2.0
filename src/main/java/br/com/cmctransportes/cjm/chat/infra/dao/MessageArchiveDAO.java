package br.com.cmctransportes.cjm.chat.infra.dao;

import br.com.cmctransportes.cjm.chat.domain.entities.MessageArchive;
import br.com.cmctransportes.cjm.chat.domain.entities.vo.MessageArchiveVO;
import br.com.cmctransportes.cjm.infra.dao.GenericDAO;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 * @author William Leite
 */
public class MessageArchiveDAO extends GenericDAO<Integer, MessageArchive> {

    private final String userDomain = "@rodoviasolucoes.com.br";

    public MessageArchiveDAO(EntityManager entityManager) {
        super(entityManager);
    }

    @SuppressWarnings("unchecked")
    public List<MessageArchiveVO> find(String username, Long inicio, Long fim) {
        List<MessageArchiveVO> result = null;

        try {

            username = String.format("%s%s", username, this.userDomain);

            StringBuilder sql = new StringBuilder(0);
            sql.append("SELECT DISTINCT m.* FROM ofmessagearchive m");
            // sql.append(" LEFT JOIN ofmessagearchive m ON m.conversationid = c.conversationid");
            sql.append(" WHERE (m.fromjid = :username OR m.tojid = :username) AND");
            sql.append(" m.sentdate between :inicio AND :fim");
            sql.append(" ORDER BY m.sentdate");

            Query query = this.entityManager.createNativeQuery(sql.toString(), MessageArchive.class)
                    .setParameter("username", username)
                    .setParameter("inicio", inicio)
                    .setParameter("fim", fim);

            result = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return result;
    }

}
