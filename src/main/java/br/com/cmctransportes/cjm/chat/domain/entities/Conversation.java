package br.com.cmctransportes.cjm.chat.domain.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Objects;

/**
 * @author William Leite
 */
@Entity
@Table(name = "ofconversation")
@XmlRootElement
public class Conversation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "conversationid")
    @Getter
    @Setter
    private Integer id;

    @Basic
    @Size(max = 1024)
    @Column(name = "room")
    @Getter
    @Setter
    private String room;

    @Column(columnDefinition = "smallint", name = "isexternal")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Getter
    @Setter
    private Boolean isExternal;

    @Column(name = "startdate")
    @Getter
    @Setter
    private BigInteger startDate;

    @Column(name = "lastactivity")
    @Getter
    @Setter
    private BigInteger lastActivity;

    @Column(name = "messagecount")
    @Getter
    @Setter
    private Integer messageCount;

    @JsonManagedReference
    @OneToMany(mappedBy = "conversation", fetch = FetchType.EAGER)
    @Getter
    @Setter
    private Collection<MessageArchive> messageArchive;

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Conversation other = (Conversation) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Conversation{" + "id=" + id + ", room=" + room + ", isExternal=" + isExternal + ", startDate=" + startDate + ", lastActivity=" + lastActivity + ", messageCount=" + messageCount + '}';
    }
}
