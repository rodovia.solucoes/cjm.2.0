package br.com.cmctransportes.cjm.chat.domain.services;

import br.com.cmctransportes.cjm.chat.domain.entities.vo.MessageArchiveVO;
import br.com.cmctransportes.cjm.chat.infra.dao.MessageArchiveDAO;
import br.com.cmctransportes.cjm.domain.entities.Motoristas;
import br.com.cmctransportes.cjm.domain.entities.Users;
import br.com.cmctransportes.cjm.infra.dao.MotoristasDAO;
import br.com.cmctransportes.cjm.infra.dao.UsersDAO;
import br.com.cmctransportes.cjm.utils.TimeHelper;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @author William Leite
 */
@Service
public class MessageArchiveService {

    private final MessageArchiveDAO dao;

    private final EntityManager em;

    public MessageArchiveService(EntityManager em) {
        this.em = em;
        this.dao = new MessageArchiveDAO(em);
    }

    public List<MessageArchiveVO> find(Integer motoristaID, Long dataInicio, Long dataFinal) {
        MotoristasDAO motoristaDAO = new MotoristasDAO(em);
        Motoristas motorista = motoristaDAO.getById(motoristaID);

        if (Objects.isNull(motorista.getUserId()))
            return new ArrayList<>();

        UsersDAO userDAO = new UsersDAO(em);
        Users user = userDAO.getById(motorista.getUserId());

        Date dtInicio = TimeHelper.getDate000000(new Date(dataInicio));
        Date dtFinal = TimeHelper.getDate235959(new Date(dataFinal));

        dataInicio = dtInicio.getTime();
        dataFinal = dtFinal.getTime();

        List<MessageArchiveVO> conversations = this.dao.find(user.getUserName(), dataInicio, dataFinal);
        return conversations; //.stream().map(c -> new MessageArchiveVO(c)).collect(Collectors.toList());
    }
}