package br.com.cmctransportes.cjm.chat.controllers.v2;

import br.com.cmctransportes.cjm.chat.domain.entities.vo.ChatCredentialsVO;
import br.com.cmctransportes.cjm.chat.domain.entities.vo.MessageArchiveVO;
import br.com.cmctransportes.cjm.chat.domain.services.MessageArchiveService;
import br.com.cmctransportes.cjm.domain.entities.Empresas;
import br.com.cmctransportes.cjm.domain.entities.Users;
import br.com.cmctransportes.cjm.domain.entities.vo.RESTResponseVO;
import br.com.cmctransportes.cjm.domain.services.EmpresasService;
import br.com.cmctransportes.cjm.domain.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;

/**
 * @author William Leite
 */
@RestController
@RequestMapping(path = "App/v2/web/chat")
public class ChatFacadeREST {

    @Autowired
    private MessageArchiveService cs;

    @Autowired
    private UserService us;

    @Autowired
    private EmpresasService companyService;

    @RequestMapping(method = RequestMethod.GET, path = "relatorio/{motorista}/{datainicio}/{datafinal}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<List<MessageArchiveVO>> find(@PathVariable("motorista") Integer motoristaID, @PathVariable("datainicio") Long dataInicio, @PathVariable("datafinal") Long dataFinal) {
        List<MessageArchiveVO> result = null;
        String error = null;
        try {
            result = cs.find(motoristaID, dataInicio, dataFinal);
        } catch (Exception e) {
            error = e.getLocalizedMessage();
            e.printStackTrace();
        }
        return new RESTResponseVO<>(error, result);
    }

    @RequestMapping(method = RequestMethod.GET, path = "credentials/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ChatCredentialsVO retrieveCredentials(@PathVariable("id") Integer id) {
        ChatCredentialsVO result = null;
        String error = null;
        try {
            Users user = us.getById(id);
            if (Objects.nonNull(user)) {
                String userName = String.format("%s@rodoviasolucoes.com.br", user.getUserName());

                result = new ChatCredentialsVO(userName, user.getSenha());
            } else {
                System.out.println("Não foi possível achar o usuário: " + id);
            }
        } catch (Exception e) {
            e.printStackTrace();
            error = e.getLocalizedMessage();
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.GET, path = "boshserviceurl/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RESTResponseVO<String> retrieveBoshServiceUrl(@PathVariable("id") Integer id) {
        String result = null;
        String error = null;

        try {
            Empresas company = this.companyService.getById(id);
            result = company.getBoshServiceURL();
        } catch (Exception e) {
            e.printStackTrace();
            error = e.getLocalizedMessage();
        }

        return new RESTResponseVO<>(error, result);
    }

}
