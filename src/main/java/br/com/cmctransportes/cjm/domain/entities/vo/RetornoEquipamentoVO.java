package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class RetornoEquipamentoVO extends RetornoVO {

    public RetornoEquipamentoVO() {

    }

    public RetornoEquipamentoVO(Integer codigoRetorno, String mensagem) {
        super(codigoRetorno, mensagem);
    }

    @Getter
    @Setter
    private List<EquipamentoVO> listaEquipamento;

    @Getter
    @Setter
    private EquipamentoVO equipamento;

}
