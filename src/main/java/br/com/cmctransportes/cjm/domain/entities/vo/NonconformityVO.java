package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.enums.NONCONFORMITY_CALCULUS_TYPE;
import br.com.cmctransportes.cjm.domain.entities.enums.NONCONFORMITY_TYPE;
import br.com.cmctransportes.cjm.domain.entities.enums.TREATMENT_TYPE;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

/**
 * @author William Leite
 */
public class NonconformityVO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private UUID id;
    @Getter
    @Setter
    private Date startDate;
    @Getter
    @Setter
    private Date endDate;
    @Getter
    @Setter
    private NONCONFORMITY_CALCULUS_TYPE calculusType;
    @Getter
    @Setter
    private NONCONFORMITY_TYPE type;
    @Getter
    @Setter
    private String description;

    @Getter
    @Setter
    private TREATMENT_TYPE treatmentType;
    @Getter
    @Setter
    private String treatmentDescription;
    @Getter
    @Setter
    private String treatmentArgs;

    @Getter
    @Setter
    private Integer driver;

    public NonconformityVO() {
        this.id = UUID.randomUUID();
        this.treatmentType = TREATMENT_TYPE.NO_TREATMENT_AVAILABLE;
        this.treatmentDescription = "Nenhum tratamento está disponível no momento.";
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NonconformityVO other = (NonconformityVO) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
}