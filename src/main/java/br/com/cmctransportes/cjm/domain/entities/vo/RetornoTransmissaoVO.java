package br.com.cmctransportes.cjm.domain.entities.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RetornoTransmissaoVO  extends RetornoVO {
    @Getter
    @Setter
    private List<TrajetoVO> listaDeTrajeto;
}
