package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.Inconformidade;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class RetornoInconformidadeVO extends RetornoVO implements Serializable {
   private Inconformidade inconformidade;
   private AlertaInconformidadeVO alertaInconformidade;
   private List<Inconformidade> listaDeInconformidade;
}
