package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public class ModeloVO implements Serializable {
    private static final long serialVersionUID = 1L;
    @Getter
    @Setter
    private Integer id;
    @Getter
    @Setter
    private String fabricante;
    @Getter
    @Setter
    private String marca;
    @Getter
    @Setter
    private String tipo;
    @Getter
    @Setter
    private Boolean status;
    @Getter
    @Setter
    private Boolean portatil;
}
