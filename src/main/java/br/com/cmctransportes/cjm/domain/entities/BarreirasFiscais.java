/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author analista.ti
 */
@Entity
@Table(name = "barreiras_fiscais")
@Getter
@Setter
@NamedQueries({
        @NamedQuery(name = "BarreirasFiscais.findAll", query = "SELECT b FROM BarreirasFiscais b"),
        @NamedQuery(name = "BarreirasFiscais.findById", query = "SELECT b FROM BarreirasFiscais b WHERE b.id = :id"),
        @NamedQuery(name = "BarreirasFiscais.findByTipoBarreira", query = "SELECT b FROM BarreirasFiscais b WHERE b.tipoBarreira = :tipoBarreira")})
public class BarreirasFiscais implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "BARREIRAS_FISCAIS_ID_SEQ", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "BARREIRAS_FISCAIS_ID_SEQ", sequenceName = "BARREIRAS_FISCAIS_ID_SEQ", allocationSize = 1)
    @Column(name = "id")
    private Integer id;

    @Column(name = "tipo_barreira", nullable = false)
    private int tipoBarreira;

    @JoinColumn(name = "empresa_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Empresas empresaId;

    @JoinColumn(name = "pontos_de_espera_id", referencedColumnName = "id")
    @OneToOne(optional = false)
    private PontosDeEspera pontosDeEsperaId;

    @Column(name = "longitude", nullable = true)
    private Double longitude;

    @Column(name = "latitude", nullable = true)
    private Double latitude;
}