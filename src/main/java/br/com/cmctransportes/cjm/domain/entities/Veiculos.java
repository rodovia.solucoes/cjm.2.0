/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cmctransportes.cjm.domain.entities;

import br.com.cmctransportes.cjm.proxy.modelo.Modelo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author analista.ti
 */
@Entity
@Table(name = "veiculos")
@Getter
@Setter
@NamedQueries({
        @NamedQuery(name = "Veiculos.findAll", query = "SELECT v FROM Veiculos v"),
        @NamedQuery(name = "Veiculos.findByPlaca", query = "SELECT v FROM Veiculos v WHERE v.placa = :placa"),
        @NamedQuery(name = "Veiculos.findByMarca", query = "SELECT v FROM Veiculos v WHERE v.marca = :marca"),
        @NamedQuery(name = "Veiculos.findByModelo", query = "SELECT v FROM Veiculos v WHERE v.modelo = :modelo"),
        @NamedQuery(name = "Veiculos.findByCor", query = "SELECT v FROM Veiculos v WHERE v.cor = :cor"),
        @NamedQuery(name = "Veiculos.findByRenavan", query = "SELECT v FROM Veiculos v WHERE v.renavan = :renavan")})
public class Veiculos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "placa", length = 7)
    private String placa;

    @Column(name = "marca", length = 50, nullable = false)
    private String marca;

    @Column(name = "modelo", length = 50, nullable = false)
    private String modelo;

    @Column(name = "cor", length = 15, nullable = false)
    private String cor;

    @Column(name = "renavan", length = 15)
    private String renavan;

    @JoinColumn(name = "cidade_registro", referencedColumnName = "cod_ibge")
    @ManyToOne(optional = false)
    private Cidades cidadeRegistro;

    @JoinColumn(name = "empresa_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Empresas empresaId;

    @JoinColumn(name = "tipos_de_veiculo", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TiposDeVeiculo tiposDeVeiculo;

    @Transient
    private Integer id;

    @Transient
    private Integer rpmMaximo;
    @Transient
    private Integer rpmInicioFaixaAzul;
    @Transient
    private Integer rpmFimFaixaAzul;
    @Transient
    private Integer rpmInicioFaixaEconomica;
    @Transient
    private Integer rpmFimFaixaEconomica;
    @Transient
    private Integer rpmInicioFaixaVerde;
    @Transient
    private Integer rpmFimFaixaVerde;
    @Transient
    private Integer rpmInicioFaixaAmarela;
    @Transient
    private Integer rpmFimFaixaAmarela;
    @Transient
    private Integer rpmInicioMarchaLenta;
    @Transient
    private Integer rpmFimMarchaLenta;
    @Transient
    @JsonIgnore
    private Integer  idModeloEquipamento;


    public Veiculos() {
    }

    public Veiculos(final String placa) {
        this.placa = placa;
    }
}