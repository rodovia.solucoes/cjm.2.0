package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "sascar")
@Getter
@Setter
@NamedQueries({
        @NamedQuery(name = "Sascar.findAll", query = "SELECT s FROM Sascar s"),
        @NamedQuery(name = "Sascar.findById", query = "SELECT s FROM Sascar s WHERE s.id = :id")})
public class Sascar implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "usuario_sascar")
    private String usuarioSascar;

    @Column(name = "senha_sascar")
    private String senhaSascar;

    @Column(name = "id_veiculo_sascar")
    private Integer idVeiculoSascar;

    @Column(name = "operador_lancamento")
    private Integer operadorLancamento;

    @Column(name="data_inicio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    @Column(name ="carga_manual")
    private Boolean cargaManual;
}
