package br.com.cmctransportes.cjm.domain.repository;

import br.com.cmctransportes.cjm.domain.entities.Motoristas;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MotoristasRepository extends JpaRepository<Motoristas, Integer> {
}
