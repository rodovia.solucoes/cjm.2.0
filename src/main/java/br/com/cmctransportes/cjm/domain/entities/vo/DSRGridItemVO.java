package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author William Leite
 */
public class DSRGridItemVO {
    @Getter
    @Setter
    private Long tempoTrabalhado;
    @Getter
    @Setter
    private Integer eventId;
    @Getter
    private String eventDescription;
    @Getter
    @Setter
    private boolean ehDsr;
    @Getter
    @Setter
    private Date dataInicio;
    @Getter
    @Setter
    private Date dataFim;
    @Getter
    @Setter
    private Integer tipoEvento;

    public void setEventDescription(String descricao) {
        this.eventDescription = descricao.replace("Inicio ", "");
    }

}