package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author analista.ti
 */
@Entity
@Table(name = "cidades")
@Getter
@Setter
@NamedQueries({
        @NamedQuery(name = "Cidades.findAll", query = "SELECT c FROM Cidades c"),
        @NamedQuery(name = "Cidades.findByCodIbge", query = "SELECT c FROM Cidades c WHERE c.codIbge = :codIbge"),
        @NamedQuery(name = "Cidades.findByNome", query = "SELECT c FROM Cidades c WHERE c.nome = :nome")})
public class Cidades implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "cod_ibge", length = 7)
    private String codIbge;

    @Column(name = "nome", length = 45, nullable = false)
    private String nome;

    @JoinColumn(name = "uf_cod_ibge", referencedColumnName = "cod_ibge")
    @ManyToOne(optional = false)
    private UnidadesFederativas ufCodIbge;
}