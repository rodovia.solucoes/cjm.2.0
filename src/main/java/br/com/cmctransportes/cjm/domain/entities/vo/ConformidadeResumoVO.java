package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConformidadeResumoVO {
    private boolean mostrarNoFront = false;
    private String descricao;
    private String horas;
}
