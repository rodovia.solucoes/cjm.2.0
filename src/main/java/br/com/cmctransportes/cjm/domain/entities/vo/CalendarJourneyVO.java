package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.enums.JOURNEY_TYPE;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author William Leite
 */
public class CalendarJourneyVO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private Integer id;
    @Getter
    @Setter
    private Date jornadaInicio;
    @Getter
    @Setter
    private Date jornadaFim;
    @Getter
    @Setter
    private JOURNEY_TYPE type;
    @Getter
    @Setter
    private JOURNEY_TYPE previousType;
    @Getter
    @Setter
    private Long trabalhado;
    @Getter
    @Setter
    private Long restante;
    @Getter
    @Setter
    private Long restanteAlmoco;
    @Getter
    @Setter
    private List<CalendarEventVO> events;
    @Getter
    @Setter
    private Boolean dsrHE;
    @Getter
    @Setter
    private Boolean abonarHE;
    @Getter
    @Setter
    private Boolean horasExtrasCinquentaPorcento;
    @Getter
    @Setter
    private Boolean waitingAsWorked;
    @Getter
    @Setter
    private Map<String, CalendarJourneySummaryVO> typeSummary;
    @Getter
    @Setter
    private Boolean locked;
    @Getter
    @Setter
    private Boolean mostrarCompensacaoGradeJornada;
    @Getter
    @Setter
    private Boolean mostrarAbonarHorasGradeJornada;
    @Getter
    @Setter
    private Boolean mostrarHorasExtrasCinquentaPorcento;


}
