/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cmctransportes.cjm.domain.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author analista.ti
 */
@Entity
@Table(name = "rastro")
@Getter
@Setter
@NamedQueries({
        @NamedQuery(name = "Rastro.findAll", query = "SELECT r FROM Rastro r"),
        @NamedQuery(name = "Rastro.findById", query = "SELECT r FROM Rastro r WHERE r.id = :id"),
        @NamedQuery(name = "Rastro.findByInstante", query = "SELECT r FROM Rastro r WHERE r.instante = :instante"),
        @NamedQuery(name = "Rastro.findByLatitude", query = "SELECT r FROM Rastro r WHERE r.latitude = :latitude"),
        @NamedQuery(name = "Rastro.findByLongitude", query = "SELECT r FROM Rastro r WHERE r.longitude = :longitude")})
public class Rastro implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name = "RASTRO_ID_SEQ", sequenceName = "RASTRO_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RASTRO_ID_SEQ")
    @Column(name = "id")
    private Integer id;

    @Column(name = "instante_marcacao", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date instante;

    @Column(name = "instante_envio", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date instanteEnvio;

    @Column(name = "latitude")
    private Double latitude;

    @Column(name = "longitude")
    private Double longitude;

    @Column(name = "motorista_id")
    private Integer motoristaId;

    @JsonIgnore
    @JoinColumn(name = "empresa_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Empresas empresaId;

    @Column(name = "origem_id", nullable = true)
    private String origem;

    @Column(name = "endereco")
    private String endereco;

    @Column(name = "bairro")
    private String bairro;

    @Column(name = "cidade")
    private String cidade;

    @Column(name = "estado")
    private String uf;

}