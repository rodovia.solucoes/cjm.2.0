package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.*;
import br.com.cmctransportes.cjm.domain.entities.enums.EVENT_TYPE;
import br.com.cmctransportes.cjm.domain.entities.vo.DSRGridItemVO;
import br.com.cmctransportes.cjm.domain.entities.vo.DiurnoNoturno;
import br.com.cmctransportes.cjm.domain.repository.DsrRepository;
import br.com.cmctransportes.cjm.infra.dao.DsrDAO;
import br.com.cmctransportes.cjm.services.JourneyCalculusService;
import br.com.cmctransportes.cjm.utils.TimeHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.*;

/**
 * @author analista.ti
 */
@Service
public class DsrService {
    private DsrDAO dao;
    private final EntityManager em;

    @Autowired
    private JornadaService jornadaService;

    @Autowired
    private JourneyCalculusService journeyCalculusService;

    @Autowired
    private DsrRepository repository;

    public DsrService(EntityManager em) {
        this.em = em;
        dao = new DsrDAO(em);
    }

    @Transactional
    public void save(Dsr entity) {
        try {
            this.repository.saveAndFlush(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Dsr> findAll() {
        return this.repository.findAll();
    }

    public Dsr getById(Integer id) {
        return this.repository.findById(id).get();
    }

    @Transactional
    public void update(Dsr entity) {
        try {
            this.repository.saveAndFlush(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void delete(Integer id) {
        try {
            this.repository.deleteById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void delete(Dsr entity) {
        try {
            this.repository.delete(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Dsr> findAll(Integer idEmpresa) {
        return dao.findAll(idEmpresa);
    }

    public DiurnoNoturno pegaDsrTime(Motoristas motoristasId, Date inicio, Date fim) {
        DiurnoNoturno result = new DiurnoNoturno();

        if (inicio == null || fim == null) {
            return result;
        }

        List<Dsr> lista = pegaEventoEmDsr(inicio, fim, motoristasId);
        NoturnoService calcN = new NoturnoService();

        long diurno = 0;
        long noturno = 0;

        Date tmpIni = new Date(inicio.getTime());
        Date tmpFim = new Date(fim.getTime());

        DiurnoNoturno tmpDN = new DiurnoNoturno();

        // TODO: separar diurno e noturno
        for (Dsr tmpDsr : lista) {
            Date dsrIni = tmpDsr.getInicioDSR();
            Date dsrFim = tmpDsr.getFimDSR();

            if (inicio.before(dsrIni) && fim.after(dsrIni) && (fim.compareTo(dsrFim) <= 0)) {
                // Se inicio < inicioDsr  & fim > inicioDsr & fim <= fimDsr
                tmpIni = dsrIni;
                tmpFim = fim;
            } else if (inicio.before(dsrIni) && (fim.compareTo(dsrFim) >= 0)) {
                // Se inicio < inicioDsr  & fim > fimDsr
                tmpIni = dsrIni;
                tmpFim = dsrFim;
            } else if ((inicio.compareTo(dsrIni) >= 0) && (fim.compareTo(dsrFim) <= 0)) {
                // Se inicio > inicioDsr  & fim < fimDsr 
                tmpIni = inicio;
                tmpFim = fim;
            } else if ((inicio.compareTo(dsrIni) >= 0) && fim.after(dsrFim)) {
                // Se inicio > inicioDsr e fim > fimDsr
                tmpIni = inicio;
                tmpFim = dsrFim;
            }

            tmpDN = calcN.calculaNoturno(tmpIni, tmpFim, true);

            diurno += tmpDN.getTempoDiurno();
            noturno += tmpDN.getTempoNoturno();
        }

        result.setTempoDiurno(diurno);
        result.setTempoNoturno(noturno);

        return result;
    }

    public List<Dsr> pegaEventoEmDsr(Date inicio, Date fim, Motoristas motoristasId) {
        List<Dsr> lista = dao.getEventoEmDsr(inicio, fim, motoristasId.getId());
        return lista;
    }

    List<Dsr> getAgendados(Integer motoristaId, Date dataInicial, Date dataFinal) {
        return dao.getAgendados(motoristaId, dataInicial, dataFinal);
    }

    public void agendaDSR() {
        // pega qual o  próximo DSR do motorista
        Calendar proximoDomingo = Calendar.getInstance();

        // Por enquanto será TODO domingo...
        // TODO: criar uma regra de agendamento por unidade/grade de horário
        while (proximoDomingo.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
            proximoDomingo.add(Calendar.DAY_OF_WEEK, 1);
        }

        Calendar inicioSemana = (Calendar) proximoDomingo.clone();
        inicioSemana.add(Calendar.DAY_OF_WEEK, -6);

        Date inicio = TimeHelper.getDate000000(inicioSemana.getTime());
        Date fim = TimeHelper.getDate235959(proximoDomingo.getTime());

        // e verifica se não está agendado...
        List<Motoristas> naoAgendados = dao.getMotoristasNaoAgendadosDoDia(inicio, fim);

        inicio = TimeHelper.getDate000000(proximoDomingo.getTime());
        for (Motoristas tmpDriver : naoAgendados) {
            Dsr tmpDsr = new Dsr();

            tmpDsr.setInicioDSR(inicio);
            tmpDsr.setFimDSR(fim);
            tmpDsr.setEmpresaId(tmpDriver.getEmpresa());
            tmpDsr.setMotorista(tmpDriver);
            tmpDsr.setOperadorLancamento(new Users(-1));

            save(tmpDsr);
        }
    }

    public List<Dsr> findAll(Integer idEmpresa, Integer idUnidade) {
        if (null != idUnidade && idUnidade > 0) {
            return dao.findAll(idEmpresa, idUnidade);
        } else {
            return dao.findAll(idEmpresa);
        }
    }

    public List<Dsr> findAll(Integer idEmpresa, Integer idUnidade, Long dataInicio, Long dataFim) {
        Date dataInicial = TimeHelper.getDate000000(new Date(dataInicio));
        Date dataFinal = TimeHelper.getDate235959(new Date(dataFim));

        if (null != idUnidade && idUnidade > 0) {
            return dao.findAll(idEmpresa, idUnidade, dataInicial, dataFinal);
        } else {
            return dao.findAll(idEmpresa, dataInicial, dataFinal);
        }
    }

    public List<Dsr> findDsrList(Integer motorista, Long dataInicio, Long dataFim) {
        Date dataInicial = TimeHelper.getDate000000(new Date(dataInicio));
        Date dataFinal = TimeHelper.getDate235959(new Date(dataFim));

        return dao.findByDriver(motorista, dataInicial, dataFinal);
    }

    public List<DSRGridItemVO> fiindAll(Integer motorista, Long dataInicio, Long dataFim) {
        Date dataInicial = TimeHelper.getDate000000(new Date(dataInicio));
        Date dataFinal = TimeHelper.getDate235959(new Date(dataFim));

        List<Jornada> jornadas = this.jornadaService.jornadasDoMotoristaPorPeriodo(dataInicial, dataFinal, motorista);
        ArrayList<DSRGridItemVO> result = new ArrayList<>();

        for (Jornada journey : jornadas) {
            this.journeyCalculusService.processEvents(journey, null, false);

            DSRGridItemVO tmpItem = new DSRGridItemVO();

//            tmpItem.setMotorista( tmpJrn.getMotoristasId() );
            tmpItem.setTempoTrabalhado(this.journeyCalculusService.totalWorkedHours(journey.getResumoTiposEstados(), Arrays.asList(new String[]{"Deslocamento", "Ocioso"})));

            Optional<Evento> optEvt = journey.getEventos().stream().findFirst();
            if (optEvt.isPresent()) {
                Evento tmpEvt = optEvt.get();
                tmpItem.setEventId(tmpEvt.getId());

                if (tmpEvt.getTipoEvento().getId() == EVENT_TYPE.INTERJORNADA.getStartCode()) {
                    tmpItem.setTipoEvento(EVENT_TYPE.DSR.getStartCode());
                    tmpItem.setEventDescription(EVENT_TYPE.DSR.getDescription());
                    tmpItem.setEhDsr(true);
                } else {
                    tmpItem.setTipoEvento(tmpEvt.getTipoEvento().getId());
                    tmpItem.setEventDescription(tmpEvt.getTipoEvento().getDescricao());
                    tmpItem.setEhDsr(tmpEvt.getTipoEvento().getId() == EVENT_TYPE.DSR.getStartCode());
                }

                tmpItem.setDataInicio(tmpEvt.getInstanteEvento());
                tmpItem.setDataFim(journey.getJornadaFim());
            }

            result.add(tmpItem);
        }

        return result;
    }

    public void marcaRemovidoDsr(Evento original) {
        List<Dsr> lDsr = pegaEventoEmDsr(original.getInstanteEvento(), original.getFimEvento(), original.getJornadaId().getMotoristasId());

        if (lDsr != null && !lDsr.isEmpty()) {
            Dsr tmpDsr = lDsr.get(0);

            tmpDsr.setProcessada('r');

            update(tmpDsr);

        }
    }

    public Dsr lastDsr(Integer motorista, Date data) {
        Date dataFinal = TimeHelper.getDate235959(data);

        Dsr result = dao.findLastByDriver(motorista, dataFinal);

        return result;
    }

    public List<Dsr> findDsrList(Motoristas driver, Date begDate, Date endDate) {
        return dao.findByDriver(driver.getId(), begDate, endDate);
    }

    public int countConsecutives(List<Dsr> resultList) {
        int result = 0;

        Dsr previous = null;

        for (Dsr tmpDsr : resultList) {
            if (Objects.isNull(previous)) {
                previous = tmpDsr;
            } else {
                long elepsedTime = tmpDsr.getInicioDSR().getTime() - previous.getFimDSR().getTime();

                if (elepsedTime > TimeHelper.VINTEQUATRO_HORAS) {
                    return result;
                }

                previous = tmpDsr;
                result += 1;
            }
        }

        return result;
    }
}