package br.com.cmctransportes.cjm.domain.entities.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HodometroVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    @Getter
    @Setter
    private VeiculoVO veiculo;
    @Getter
    @Setter
    private Integer usuario;
    @Getter
    @Setter
    private Date dataDoCadastro;
    @Getter
    @Setter
    private Date dataUltimaAtualizacao;
    @Getter
    @Setter
    private Double hodometro;
    @Getter
    @Setter
    private Double hodometroRastreador;
    //Se é automatico A ou M para Manual
    @Getter
    @Setter
    private String tipo;
    @Getter
    @Setter
    private String nomeUsuario;
    @Getter
    @Setter
    private String nomeDoMotorista;
    @Getter
    @Setter
    private Integer idDoMotorista;
    //1 para Carregamento 2 - Vazio
    @Getter
    @Setter
    private Integer tipoCarregamento;

}
