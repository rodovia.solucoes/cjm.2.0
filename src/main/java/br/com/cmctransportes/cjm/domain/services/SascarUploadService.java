package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.controllers.web.v2.SascarUploadFacadeREST;
import br.com.cmctransportes.cjm.domain.entities.*;
import br.com.cmctransportes.cjm.domain.repository.EventoRepository;
import br.com.cmctransportes.cjm.domain.repository.JourneyRepository;
import br.com.cmctransportes.cjm.infra.dao.EventoDAO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class SascarUploadService {

    @Autowired
    private DriverService driverService;

    @Autowired
    private EventoRepository eventoRepository;

    @Autowired
    private JourneyRepository journeyRepository;

    private final EventoDAO eventoDAO;

    public SascarUploadService() {
        eventoDAO = new EventoDAO(null);
    }
    /**
     * Metodo responsável pela leitura das linhas do arquivo CSV e devidos tratamentos
     * @param stream arquivo recebido
     * @param motoristaId Motorista selecionado
     * @param empresaId ID da empresa do motorista
     * @return um Array de eventos já no formato para criar os eventos recebidos pela sascar
     * @throws IOException lança exceção caso tenha erro na entrada do arquivo CSV
     */
    private List<Evento> readFile(final InputStream stream, Integer motoristaId, Integer empresaId) throws IOException {
        List<Evento> eventos = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(stream, Charset.forName("windows-1252")))) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            reader.lines().skip(1).forEach(line -> {
                String[] split = line.split(";");
                String tipoEventoArquivo = split[7];
                Integer tipoEvento = convertEventType(tipoEventoArquivo);
                Date instanteLancamento = null;
                Date instanteEvento = null;
                try {
                    instanteLancamento = this.formatDate(split[0], dateFormat);
                    instanteEvento = this.formatDate(split[1], dateFormat);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String[] latitudeLongitude = split[3].split("/");
                Double latitude = this.convertLatitude(latitudeLongitude[0].trim());
                Double longitude = this.convertLongitude(latitudeLongitude[1].trim());
                String pontoParada = split[4];
                Evento eventoArquivo = this.createEvento(instanteEvento, instanteLancamento, latitude, longitude, tipoEvento, pontoParada, motoristaId, empresaId);
                eventos.add(eventoArquivo);
            });
        }
        return eventos;
    }
    /**
     * Método inicial que faz as requisições dos métodos necessários ao processamento do arquivo CSV da Sascar
     * @param stream recebe o arquivo por meio do "InputStream"
     * @param motoristaId recebe o ID do motorista selecionado no front
     * @param userId recebe o usuário logado no sistema
     * @see SascarUploadFacadeREST
     * @return Um objeto string para classe Controller
     * @throws IOException
     */
    public String uploadFile(final InputStream stream, Integer motoristaId, Integer userId) throws IOException {
        Motoristas motoristas = this.driverService.getById(motoristaId);
        Empresas empresas = motoristas.getEmpresa();
        List<Evento> eventos = this.readFile(stream, motoristaId, empresas.getId());
        this.saveRecords(eventos, motoristas, empresas);

        return "Upload concluido com sucesso";
    }

    /**
     * Método que organiza os eventos pela data e inclui o fechamento do evento que não é encerrado no sascar
     * @param eventos lista de eventos recebida do método SascarUploadService
     * @param motoristas ID do motorista selecionado
     * @param empresas ID da empresa selecionada
     */
    private void saveRecords(List<Evento> eventos, Motoristas motoristas, Empresas empresas) {
        final AtomicReference<Jornada> atomicJornada = new AtomicReference<>();
        final List<Evento> eventosJornada = new ArrayList<>();

        int[] eventosEterno = new int[]{2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 15, 16, 23, 25};
        final AtomicReference<Integer> isEterno = new AtomicReference<>(null);
        eventos.stream().sorted(Comparator.comparing(Evento::getInstanteEvento)).forEach(evento -> {
            // Inicio Jornada
            if (evento.getTipoEvento().getId() == 1) {
                Jornada jornada = new Jornada();
                jornada.setMotoristasId(motoristas);
                jornada.setEmpresaId(empresas);
                jornada.setDsrHE(Boolean.FALSE);
                jornada.setWaitingAsWorked(Boolean.FALSE);
                jornada.setLocked(Boolean.FALSE);
                jornada = this.journeyRepository.saveAndFlush(jornada);
                atomicJornada.set(jornada);
            }

            if (Objects.nonNull(isEterno.get())) {
                Date instanteEvento = new Date(evento.getInstanteEvento().getTime() - 1000);
                Date instanteLancamento = new Date(evento.getInstanteLancamento().getTime() - 1000);
                Integer tipoEvento = isEterno.get() * -1;
                //Integer tipoEvento = evento.getTipoEvento().getId() * -1;
                String pontoParada = "Final Automático evento eterno";
                Evento eventoFim = this.createEvento(instanteEvento, instanteLancamento, evento.getLatitude(), evento.getLongitude(), tipoEvento, pontoParada, motoristas.getId(), empresas.getId());
                eventoFim.setJornadaId(atomicJornada.get());eventosJornada.add(this.eventoDAO.address(eventoFim));
                isEterno.set(null);
            }

            if (Arrays.binarySearch(eventosEterno, evento.getTipoEvento().getId()) >= 0) {
                isEterno.set(evento.getTipoEvento().getId());
            }
            evento.setJornadaId(atomicJornada.get());
            eventosJornada.add(this.eventoDAO.address(evento));
            // Fim jornada
            if (evento.getTipoEvento().getId() == 0) {
                this.eventoRepository.saveAll(eventosJornada);
                eventosJornada.clear();
                atomicJornada.set(null);
            }
        });
    }

    /**
     * Conversor de localização latitude longitude de graus horarios para graus decimais
     * @param position
     * @return
     */
    private Double calculateGeographicalPosition(String position) {
        String[] split = position.replaceAll("\"", StringUtils.EMPTY).replaceAll("\'", StringUtils.EMPTY).split(" ");
        BigDecimal grau = new BigDecimal(split[0].substring(0, 2));
        BigDecimal minutos = new BigDecimal(split[1]);
        BigDecimal segundos = new BigDecimal(split[2]);
        String hemisphere = split[3];
        minutos = minutos.divide(new BigDecimal(60), 6, RoundingMode.CEILING);
        segundos = segundos.divide(new BigDecimal(3600), 6, RoundingMode.CEILING);
        BigDecimal decimal = grau.add(minutos.add(segundos));

        if (Objects.equals(hemisphere, "S") || Objects.equals(hemisphere, "W")) {
            decimal = decimal.negate();
        }
        return decimal.doubleValue();
    }

    public Double convertLatitude(String position) {
        return this.calculateGeographicalPosition(position);
    }
    public Double convertLongitude(String position) {
        return this.calculateGeographicalPosition(position);
    }
    /**
     * Metodo para criar eventos
     *
     */
    public Evento createEvento(Date instanteEvento, Date instanteLancamento, Double latitude, Double longitude, Integer tipoEvento, String pontoParada,Integer motoristaId, Integer empresaId) {
        if (Objects.isNull(tipoEvento)) {
            return null;
        } else if (Objects.isNull(instanteEvento)) {
            return null;
        } else if (Objects.isNull(instanteLancamento)) {
            return null;
        }
        Evento result = new Evento();
        result.setInstanteEvento(instanteEvento);
        result.setInstanteLancamento(instanteLancamento);
        result.setLatitude(latitude);
        result.setLongitude(longitude);
        result.setTipoEvento(new EventosJornada(tipoEvento));
        result.setPositionAddress(pontoParada);
        result.setOperadorLancamento(new Users(motoristaId));
        result.setEmpresaId(new Empresas(empresaId));
        return result;
    }

    private Date formatDate(String dataArquivo, SimpleDateFormat dateFormat) throws ParseException {
        String dateAsStr = dataArquivo.replaceAll(" (UTC-3)", "");
        return dateFormat.parse(dataArquivo);
    }

    /**
     * Método para localizart o tipo de evento inserido pelo motorista no Sascar
     * @param tipoEventoArquivo será verificado a string para identifiquar de qual evento se trata
     * @return o numero do evento do sistema ViaM
     */
    public Integer convertEventType(String tipoEventoArquivo) {
        Integer result = null;
        String[] evento = tipoEventoArquivo.replaceAll("\\|", "\\|z").replaceAll("\"", StringUtils.EMPTY).split("\\|");
        String eventoPrimario = evento[0];
        String params = evento[1];

        if (eventoPrimario.equalsIgnoreCase("INICIO DE JORNADA")) {
            result = 1;
        } else if (eventoPrimario.equalsIgnoreCase("FIM DE JORNADA")) {
            result = 0;
        } else if (eventoPrimario.equalsIgnoreCase("INICIO DE DIRECAO")) {
            result = 8;
        } else if (eventoPrimario.equalsIgnoreCase("FIM DE DIRECAO")) {
            result = -8;
        } else if (eventoPrimario.equalsIgnoreCase("INICIO DE REFEICAO")) {
            result = 14;
        } else if (eventoPrimario.equalsIgnoreCase("FIM DE REFEICAO")) {
            result = -14;
        } else if (eventoPrimario.equalsIgnoreCase("CARGA/DESCARGA")) {
            if (params.contains("(X)AGUARDANDO CARGA")) {
                result = 9;
            } else if (params.contains("(X)AGUARDANDO DESCARGA")) {
                result = 11;
            } else if (params.contains("(X)CARGA")) {
                result = 10;
            } else if (params.contains("(X)DESCARGA")) {
                result = 12;
            } else if (params.contains("(X)MANIFESTO")) {
                result = 2;
            }
        } else if (eventoPrimario.equalsIgnoreCase("PARADAS")) {
            if (params.contains("(X)ABASTECIMENTO")) {
                result = 7;
            } else if (params.contains("(X)LANCHE")) {
                result = 5;
            } else if (params.contains("(X)BANHEIRO")) {
                result = 6;
            } else if (params.contains("(X)REPOUSO")) {
                result = 15;
            } else if (params.contains("(X)INTERVALO FEMININO")) {
                result = 16;
            } else if (params.contains("(X)INTERVALO PESSOAL")) {
                result = 25;
            } else if (params.contains("(X)MANUTENCAO")) {
                result = 3;
            } else if (params.contains("(X)SINISTRO")) {
                result = 4;
            } else if (params.contains("(X)PISTA INTERDITADA")) {
                result = 23;
            }
        }
        return result;
    }
}
