package br.com.cmctransportes.cjm.domain.entities.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class TrajetoVO implements Serializable {
    private TransmissaoVO transmissaoInicial;
    private TransmissaoVO transmissaoFinal;
    private List<TransmissaoVO> listaDeTrasmissao;
}
