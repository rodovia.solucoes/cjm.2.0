package br.com.cmctransportes.cjm.domain.entities;

import br.com.cmctransportes.cjm.utils.TimeHelper;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author analista.ti
 */
@Entity
@Table(name = "feriados")
@Getter
@Setter
@NamedQueries({
        @NamedQuery(name = "Feriados.findAll", query = "SELECT f FROM Feriados f"),
        @NamedQuery(name = "Feriados.findById", query = "SELECT f FROM Feriados f WHERE f.id = :id"),
        @NamedQuery(name = "Feriados.findByDataOcorrencia", query = "SELECT f FROM Feriados f WHERE f.dataOcorrencia = :dataOcorrencia"),
        @NamedQuery(name = "Feriados.findByDescricao", query = "SELECT f FROM Feriados f WHERE f.descricao = :descricao")})
public class Feriados implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "FERIADOS_ID_SEQ", sequenceName = "FERIADOS_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FERIADOS_ID_SEQ")
    @Column(name = "id")
    private Integer id;

    @Column(name = "data_ocorrencia", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date dataOcorrencia;

    @Column(name = "descricao", length = 25, nullable = false)
    private String descricao;

    @JoinColumn(name = "ambito", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private AmbitosFeriados ambito;

    @JoinColumn(name = "cidade", referencedColumnName = "cod_ibge")
    @ManyToOne
    private Cidades cidade;

    @JoinColumn(name = "empresa_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Empresas empresaId;

    @JoinColumn(name = "uf", referencedColumnName = "cod_ibge")
    @ManyToOne
    private UnidadesFederativas uf;
    @Column(name = "unidade_id")
    private Integer idUnidade;

    public Date getInicioFeriado() {
        return dataOcorrencia;
    }

    public Date getFimFeriado() {
        return TimeHelper.getDate235959(dataOcorrencia);
    }
}