package br.com.cmctransportes.cjm.domain.entities.jornada;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResumoJornada {
    private String mes;
    private String entrada;
    private String saida;
    private String paradas;
    private String refeicao;
    private String horasDirigidas;
    private String aguardando;
    private String descanso;
    private String cargaDescarga;
    private String interjornada;
    private String horaExtra;
    private String ocioso;
    private String horasTrabalhada;
    private String tempoDeEspera;
    private String totalJornada;
    private Integer tempoEsperaI;
    private String previsaoTermino;
    private String extraDirigida;
    private String extraEspera;
    private String totalExtraEspera;
    private String horaExcedentes;
    private String totalHorasExcedentes;

}
