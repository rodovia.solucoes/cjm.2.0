package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class UsuarioMobileVO implements Serializable {
    private Integer id;
    private String nomeUsuario;
    private String senha;
    private ClienteMobileVO clientePai;
    private Boolean usuarioLogado;
    private String tokenMobile;
    private String tipoSistemaMobile;
}
