package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.MotoristaStatus;
import br.com.cmctransportes.cjm.domain.entities.Motoristas;
import br.com.cmctransportes.cjm.infra.dao.MotoristaStatusDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

/**
 * @author analista.ti
 */
@Service
public class MotoristasStatusService {
    private MotoristaStatusDAO dao;

    @Autowired
    private DriverService ms;

    private final EntityManager em;

    public MotoristasStatusService(EntityManager em) {
        this.em = em;
        dao = new MotoristaStatusDAO(em);
    }

    @Transactional
    public void save(MotoristaStatus entity) throws Exception {
        try {
// pegar o motorista
            Motoristas driver = ms.getById(entity.getMotorista());
// atualizar a sitauação dele
            driver.setSituacao(entity.getSituacaoInserida());
// gravar o motorista
            ms.update(driver); // NÃO USAR O DAO DIRETAMENTE!
            dao.save(entity);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

}
