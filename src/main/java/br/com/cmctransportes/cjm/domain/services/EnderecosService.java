package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.Enderecos;
import br.com.cmctransportes.cjm.domain.repository.EnderecosRepository;
import br.com.cmctransportes.cjm.infra.dao.EnderecosDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * @author analista.ti
 */
@Service
public class EnderecosService {

    @Autowired
    private EnderecosRepository enderecosRepository;

    @Transactional
    public void save(Enderecos entity) throws Exception {
        try {
            this.enderecosRepository.saveAndFlush(entity);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

    public List<Enderecos> findAll() {
        return this.enderecosRepository.findAll();
    }

    public Enderecos getById(Integer id) {
        return this.enderecosRepository.findById(id).get();
    }

    @Transactional
    public void update(Enderecos entity) {
        try {
            this.enderecosRepository.saveAndFlush(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void delete(Integer id) {
        try {
            this.enderecosRepository.deleteById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void delete(Enderecos entity) {
        try {
            this.enderecosRepository.delete(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}