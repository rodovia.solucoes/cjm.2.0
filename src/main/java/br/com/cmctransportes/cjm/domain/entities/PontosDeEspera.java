/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cmctransportes.cjm.domain.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author analista.ti
 */
@Entity
@Table(name = "pontos_de_espera")
@Getter
@Setter
@NamedQueries({
        @NamedQuery(name = "PontosDeEspera.findAll", query = "SELECT p FROM PontosDeEspera p"),
        @NamedQuery(name = "PontosDeEspera.findById", query = "SELECT p FROM PontosDeEspera p WHERE p.id = :id"),
        @NamedQuery(name = "PontosDeEspera.findByPermiteDescanso", query = "SELECT p FROM PontosDeEspera p WHERE p.permiteDescanso = :permiteDescanso"),
        @NamedQuery(name = "PontosDeEspera.findByIdentificacao", query = "SELECT p FROM PontosDeEspera p WHERE p.identificacao = :identificacao")})
public class PontosDeEspera implements Serializable {


    public PontosDeEspera() {

    }

    public PontosDeEspera(Integer id) {
        this.id = id;
    }

    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name = "PONTOS_DE_ESPERA_ID_SEQ", sequenceName = "PONTOS_DE_ESPERA_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PONTOS_DE_ESPERA_ID_SEQ")
    @Column(name = "id")
    private Integer id;

    @Column(name = "permite_descanso")
    private Boolean permiteDescanso;

    @Column(name = "identificacao", length = 50, nullable = false)
    private String identificacao;

    @JoinColumn(name = "empresa_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @JsonIgnore
    private Empresas empresaId;

    @JoinColumn(name = "endereco", referencedColumnName = "id")
    @OneToOne(optional = false)
    private Enderecos endereco;
}