package br.com.cmctransportes.cjm.domain.repository;

import br.com.cmctransportes.cjm.domain.entities.Unidades;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UnidadesRepository extends JpaRepository<Unidades, Integer> {
    List<Unidades> findByEmpresaIdId(Integer idEmpresa);
}