package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.UnidadesFederativas;
import br.com.cmctransportes.cjm.infra.dao.UnidadesFederativasDAO;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * @author analista.ti
 */
@Service
public class UnidadesFederativasService {

    private final UnidadesFederativasDAO dao;

    private final EntityManager em;

    public UnidadesFederativasService(EntityManager em) {
        this.em = em;
        dao = new UnidadesFederativasDAO(em);
    }

    public UnidadesFederativas getbyId(Integer id) {
        return dao.getById(id);
    }

    public List<UnidadesFederativas> findAll() {
        return dao.nqFindAll();
    }

}