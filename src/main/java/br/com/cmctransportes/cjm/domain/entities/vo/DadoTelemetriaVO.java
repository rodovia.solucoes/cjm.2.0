package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;
@Getter
@Setter
public class DadoTelemetriaVO implements Serializable {
    private static final long serialVersionUID = 1L;
    private List<TelemetriaVeiculoVO> data;
    private List<GraficoTelemetriaVO> listaGraficoTelemetria;
}
