package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "dashboard")
@Getter
@Setter
public class Dashboard implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name = "dashboard_id_seq", sequenceName = "dashboard_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dashboard_id_seq")
    @Column(name = "id")
    private Integer id;
    @Column(name = "id_empresa")
    private Integer idEmpresa;
    @Column(name = "id_unidade")
    private Integer idUnidade;
    @Column(name = "id_usuario")
    private Integer idUsuario;
    @Column(name = "id_jornada")
    private Integer idJornada;
    @Column(name = "mostrar_todos_no_mapa")
    private boolean mostrarTodosNoMapa;
}
