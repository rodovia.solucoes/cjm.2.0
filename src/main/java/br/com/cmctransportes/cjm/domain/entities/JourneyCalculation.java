package br.com.cmctransportes.cjm.domain.entities;

import br.com.cmctransportes.cjm.domain.entities.enums.JOURNEY_CALCULATION_STATUS;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author William Leite
 */
@Entity
@Table(name = "journey_calculation")
@Getter
@Setter
public class JourneyCalculation implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "JOURNEY_CALCULATION_ID_SEQ", sequenceName = "JOURNEY_CALCULATION_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "JOURNEY_CALCULATION_ID_SEQ")
    @Column(name = "id")
    private Long id;

    @Column(name = "start_date")
    @Temporal(TemporalType.DATE)
    private Date startDate;

    @Column(name = "end_date")
    @Temporal(TemporalType.DATE)
    private Date endDate;

    @Column(name = "reference_month")
    private Integer referenceMonth;

    @JoinColumn(name = "driver_id", referencedColumnName = "id")
    @ManyToOne
    private Motoristas driver;

    @JoinColumn(name = "created_by", referencedColumnName = "id")
    @ManyToOne
    private Users createdBy;

    @Column(name = "created_at")
    @Temporal(TemporalType.DATE)
    private Date createdAt;

    @JoinColumn(name = "altered_by", referencedColumnName = "id")
    @ManyToOne
    private Users alteredBy;

    @Column(name = "altered_at")
    @Temporal(TemporalType.DATE)
    private Date alteredAt;

    @Enumerated
    @Column(name = "status")
    private JOURNEY_CALCULATION_STATUS status;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "journeyCalculation")
    private List<JourneyCalculationLine> lines;
}