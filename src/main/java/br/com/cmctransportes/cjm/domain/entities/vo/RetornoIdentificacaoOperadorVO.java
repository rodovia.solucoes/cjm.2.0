package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.IdentificacaoOperador;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RetornoIdentificacaoOperadorVO extends RetornoVO {
    private List<IdentificacaoOperador> listaIdentificacaoOperador;
    private List<MotoristaOperadorVO> listaMotoristaOperador;
    private List<VeiculoAutorizacaoVO> listaVeiculoAutorizacao;
    private IdentificacaoOperador identificacaoOperador;
}
