package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class StatusRotaVO implements Serializable {

    private Integer id;
    private Integer veiculo;
    private Integer rota;
    private Date dataCadastro;
    private Integer transmissao;
    private Boolean dentroRota;

}
