package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.Empresas;
import br.com.cmctransportes.cjm.domain.entities.RotaVeiculo;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoRotaVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoVeiculoVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RotaVO;
import br.com.cmctransportes.cjm.domain.entities.vo.VeiculoVO;
import br.com.cmctransportes.cjm.infra.dao.RotaVeiculoDAO;
import br.com.cmctransportes.cjm.infra.dao.StatusRotaDAO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.proxy.RotaTrajetto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class RotaService {
    @Autowired
    private EmpresasService empresasService;
    @Autowired
    private VeiculosService veiculosService;

    public RetornoRotaVO cadastrarRota(RotaVO rotaVO) throws Exception {
        try {
            if (rotaVO.getCliente().getIdClienteTrajetto() == null || rotaVO.getCliente().getIdClienteTrajetto() == 0) {
                Empresas empresas = empresasService.getById(rotaVO.getCliente().getId());
                rotaVO.getCliente().setIdClienteTrajetto(empresas.getIdClienteTrajetto());
            }
            return RotaTrajetto.getInstance().cadastrarRota(rotaVO);
        } catch (Exception e) {
            LogSistema.logError("cadastrarRota", e);
            throw new Exception(e);
        }
    }

    public RetornoRotaVO editarRota(RotaVO rotaVO) throws Exception {
        try {
            if (rotaVO.getCliente().getIdClienteTrajetto() == null || rotaVO.getCliente().getIdClienteTrajetto() == 0) {
                Empresas empresas = empresasService.getById(rotaVO.getCliente().getId());
                rotaVO.getCliente().setIdClienteTrajetto(empresas.getIdClienteTrajetto());
            }
            return RotaTrajetto.getInstance().editarRota(rotaVO);
        } catch (Exception e) {
            LogSistema.logError("editarRota", e);
            throw new Exception(e);
        }
    }


    public RetornoRotaVO excluirRota(RotaVO rotaVO) throws Exception{
        try {
            return RotaTrajetto.getInstance().excluirRota(rotaVO);
        } catch (Exception e) {
            LogSistema.logError("excluirRota", e);
            throw new Exception(e);
        }
    }
    public RetornoRotaVO listarRotas(Integer idEmpresa, Integer idUnidade) throws Exception {
        try {
            Empresas empresas = empresasService.getById(idEmpresa);
            return RotaTrajetto.getInstance().buscarListaRota(empresas.getIdClienteTrajetto(), idUnidade);
        } catch (Exception e) {
            LogSistema.logError("listarRotas", e);
            throw new Exception(e);
        }
    }

    public RetornoRotaVO buscarPeloId(Integer id) throws Exception {
        try {
            return RotaTrajetto.getInstance().buscarLocalPeloCodigo(id);
        } catch (Exception e) {
            LogSistema.logError("listarRotas", e);
            throw new Exception(e);
        }
    }

    public void salvarRotaVeiculo(RotaVeiculo rotaVeiculo) {
        try {
            new RotaVeiculoDAO().salvar(rotaVeiculo);
        } catch (Exception e) {
            LogSistema.logError("salvarRotaVeiculo", e);
        }
    }

    public List<RotaVO> buscarListaDeRotas(Integer idVeiculo)throws Exception{
        try {
            return new RotaVeiculoDAO().buscarListaDeRotas(idVeiculo);
        } catch (Exception e) {
            LogSistema.logError("buscarListaDeRotas", e);
            throw new Exception(e);
        }
    }

    public void limparTodasAsRotas(Integer idCliente){
        try {
            RetornoVeiculoVO retornoVeiculoVO = veiculosService.buscarListaDeVeiculoMinimaPorEmpresaAtivos(idCliente);
            if(Objects.nonNull(retornoVeiculoVO) && Objects.nonNull(retornoVeiculoVO.getListaDeVeiculos()) ){
                StatusRotaDAO dao = new StatusRotaDAO();
                for(VeiculoVO veiculoVO : retornoVeiculoVO.getListaDeVeiculos()){
                    dao.excluirStatusRota(veiculoVO.getId());
                }
            }
        }catch (Exception e){
            LogSistema.logError("limparTodasAsRotas", e);
        }
    }
}
