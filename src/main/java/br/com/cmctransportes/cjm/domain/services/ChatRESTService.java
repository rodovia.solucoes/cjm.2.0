//package br.com.cmctransportes.cjm.domain.services;
//
//import br.com.cmctransportes.cjm.domain.entities.UsersChat;
//import br.com.cmctransportes.cjm.domain.entities.enums.PARAMCFG_GRUPO;
//import br.com.cmctransportes.cjm.domain.entities.enums.PARAMCFG_PARAMETRO;
//import javax.ws.rs.client.Client;
//import javax.ws.rs.client.Entity;
//import javax.ws.rs.core.MediaType;
//import javax.ws.rs.core.Response;
//import org.jboss.resteasy.client.jaxrs.BasicAuthentication;
//import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
//import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
//
///**
// * Consome a api REST do Openfire
// * @author Cristhiano Roberto
// */
//public class ChatRESTService {
//
//    private String login;
//    private String senha;
//    private String endpoint;
//    private String resource;
//
//    /**
//     * Construtor com dados do banco
//     */
//    public ChatRESTService() {
//        init();
//    }
//
//    public ChatRESTService(String login, String senha, String endpoint, String resource) {
//        this.login = login;
//        this.senha = senha;
//        this.endpoint = endpoint;
//        this.resource = resource;
//    }
//
//    /**
//     * Carrega configuracoes do banco de dados.
//     */
//    private void init(){
//        // #2033016
//        ParamcfgService paramcfgService = new ParamcfgService();
//        login = paramcfgService.findDado(PARAMCFG_GRUPO.SISTEMA, PARAMCFG_PARAMETRO.XMPP_LOGIN);
//        senha = paramcfgService.findDado(PARAMCFG_GRUPO.SISTEMA, PARAMCFG_PARAMETRO.XMPP_SENHA);
//        endpoint = paramcfgService.findDado(PARAMCFG_GRUPO.SISTEMA, PARAMCFG_PARAMETRO.XMPP_ENDPOINT);
//        resource = paramcfgService.findDado(PARAMCFG_GRUPO.SISTEMA, PARAMCFG_PARAMETRO.XMPP_RESOURCE_USERS);
//    }
//
//    /**
//     * Envia o novo usuario para o Openfire
//     * @param usersChat
//     * @return
//     */
//    public int createUsers(UsersChat usersChat){
//        if(!validarConfiguracao()){
//            System.err.println("ChatRESTServico configurado incorretamente.");
//            //Bad Request
//            return 400;
//        }
//        Client client = ResteasyClientBuilder.newBuilder().build();
//        ResteasyWebTarget webTarget = (ResteasyWebTarget) client.target(endpoint+resource);
//        BasicAuthentication authentication = new BasicAuthentication(login, senha);
//        Entity<UsersChat> entity = Entity.entity(usersChat, MediaType.APPLICATION_JSON_TYPE);
//        Response response = webTarget.register(authentication).request().post(entity);
//        response.close();
//        int resposta = response.getStatus();
//        return resposta;
//    }
//
//    /**
//     * Exclui o usuario do Openfire
//     * @param usersChat
//     * @return
//     */
//    public int deleteUsers(UsersChat usersChat){
//        if(!validarConfiguracao()){
//            System.err.println("ChatRESTServico configurado incorretamente.");
//            //Bad Request
//            return 400;
//        }
//        Client client = ResteasyClientBuilder.newBuilder().build();
//        ResteasyWebTarget webTarget = (ResteasyWebTarget) client.target(endpoint+resource+"/"+usersChat.getUsername());
//        BasicAuthentication authentication = new BasicAuthentication(login, senha);
//        Response response = webTarget.register(authentication).request().delete();
//        response.close();
//        int resposta = response.getStatus();
//        return resposta;
//    }
//
//    /**
//     * Atualiza um usuario no Openfire
//     * @param usersChat
//     * @return
//     */
//    public int updateUsers(UsersChat usersChat){
//        if(!validarConfiguracao()){
//            System.err.println("ChatRESTServico configurado incorretamente.");
//            //Bad Request
//            return 400;
//        }
//        Client client = ResteasyClientBuilder.newBuilder().build();
//        ResteasyWebTarget webTarget = (ResteasyWebTarget) client.target(endpoint+resource+"/"+usersChat.getUsername());
//        BasicAuthentication authentication = new BasicAuthentication(login, senha);
//        Entity<UsersChat> entity = Entity.entity(usersChat, MediaType.APPLICATION_JSON_TYPE);
//        Response response = webTarget.register(authentication).request().put(entity);
//        response.close();
//        int resposta = response.getStatus();
//        return resposta;
//    }
//
//    /**
//     * Verifica se o Login, Senha, Endpoint e Resource estao preenchidos.
//     * @return
//     */
//    private boolean validarConfiguracao(){
//        if(null!=login && null!=senha && null!=endpoint && null!=resource){
//            return true;
//        }
//        return false;
//    }
//
//}
