package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.*;
import br.com.cmctransportes.cjm.domain.entities.vo.ManifestoVO;
import br.com.cmctransportes.cjm.domain.entities.vo.ParametrosServicoVO;
import br.com.cmctransportes.cjm.domain.entities.vo.ServicoRetornoVO;
import br.com.cmctransportes.cjm.domain.entities.vo.TransmissaoServicoVO;
import br.com.cmctransportes.cjm.infra.dao.TransmissaoDAO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class ServicoService {

    @Autowired
    private EmpresasService empresasService;

    @Autowired
    private VeiculosService veiculosService;

    @Autowired
    private DriverService driverService;

    @Autowired
    private MotoristasVeiculosService motoristasVeiculosService;

    @Autowired
    private ManifestoService manifestoService;

    @Autowired
    private ViagemService viagemService;

    @Transactional
    public ServicoRetornoVO gravarManifesto(ManifestoVO manifestoVO) throws Exception{
        ServicoRetornoVO servicoRetornoVO = new ServicoRetornoVO();
        try{
            if(validarCampos(manifestoVO, servicoRetornoVO)){
                if(validarChaveEmpresa(manifestoVO.getUuid())){
                    //Buscar id do caminhao
                    Veiculos veiculos = veiculosService.buscarVeiculoPelaPlaca(manifestoVO.getPlaca());
                    if(Objects.isNull(veiculos)){
                        servicoRetornoVO.setCodigoRetorno(146);
                        servicoRetornoVO.setMensagem("Veiculo nao cadastrado no Via-Sat");
                    }else{
                        //Tenta buscar motorista
                        String cpf = RodoviaUtils.limpaCPF(manifestoVO.getCpfMotorista());
                        Motoristas motoristas = driverService.findPeloCpf(cpf);
                        if(Objects.isNull(motoristas)){
                            servicoRetornoVO.setCodigoRetorno(147);
                            servicoRetornoVO.setMensagem("Motorista nao cadastrado no Via-M");
                        }else{
                            //Gravar motoristaVeiculo
                            MotoristasVeiculos motoristasVeiculos = new MotoristasVeiculos();
                            motoristasVeiculos.setDataInicio(new Date());
                            motoristasVeiculos.setMotoristasId(motoristas.getId());
                            motoristasVeiculos.setVeiculoOrion(veiculos.getId());
                            motoristasVeiculosService.save(motoristasVeiculos);
                            //Gravar manifesto

                            Manifesto manifesto = new Manifesto();
                            manifesto.setData(manifestoVO.getData());
                            manifesto.setDataRegistro(new Date());
                            manifesto.setMotorista(motoristas.getId());
                            manifesto.setValor(manifestoVO.getValor());
                            manifesto.setVeiculo(veiculos.getId());
                            manifesto.setNumeroManifesto(manifestoVO.getNumeroDoManifesto());
                            manifestoService.save(manifesto);
                            servicoRetornoVO.setCodigoRetorno(1);
                            servicoRetornoVO.setMensagem("Manifesto cadastrado com sucesso");
                        }
                    }
                }else{
                    servicoRetornoVO.setCodigoRetorno(145);
                    servicoRetornoVO.setMensagem("Uuid nao cadastrado no sistema");
                }
            }

        }catch (Exception e) {
            LogSistema.logError("gravarManifesto", e);
            throw new Exception(e);
        }

        return servicoRetornoVO;
    }


    @Transactional
    public ServicoRetornoVO gravarViagem(List<Viagem> listaDeViagem, String uuid) throws Exception{
        ServicoRetornoVO servicoRetornoVO = new ServicoRetornoVO();
        try{
            if(Objects.isNull(uuid) ||Objects.nonNull(uuid) && uuid.isEmpty() ){
                servicoRetornoVO.setCodigoRetorno(140);
                servicoRetornoVO.setMensagem("UUID nao informado");
            }else{
                if(validarChaveEmpresa(uuid)) {
                    int cont = 0;
                    for(Viagem viagem : listaDeViagem){
                        if(validarCampos(viagem, servicoRetornoVO)){
                            viagemService.save(viagem);
                        }else{
                            cont++;
                        }
                    }
                    if(cont == 0) {
                        servicoRetornoVO.setCodigoRetorno(1);
                        servicoRetornoVO.setMensagem("Viagem cadastrado com sucesso");
                    }
                }else {
                    servicoRetornoVO.setCodigoRetorno(148);
                    servicoRetornoVO.setMensagem("Uuid nao cadastrado no sistema");
                }
            }
        }catch (Exception e) {
            LogSistema.logError("gravarViagem", e);
            throw new Exception(e);
        }
        return servicoRetornoVO;
    }


    public ServicoRetornoVO buscarListaDeVeiculos(ParametrosServicoVO parametrosServicoVO){
        ServicoRetornoVO servicoRetornoVO = new ServicoRetornoVO();
        if(validarCampos(parametrosServicoVO, servicoRetornoVO)){
            if(validarChaveEmpresa(parametrosServicoVO.getUuid())){
                List<TransmissaoServicoVO>  lista = new TransmissaoDAO().buscarListaDeVeiculos(parametrosServicoVO.getListaDePlacas());
                servicoRetornoVO.setListaDeTransmissao(lista);
                servicoRetornoVO.setCodigoRetorno(1);
                servicoRetornoVO.setMensagem("Lista carregada com sucesso");
            }else{
                servicoRetornoVO.setCodigoRetorno(145);
                servicoRetornoVO.setMensagem("Uuid nao cadastrado no sistema");
            }
        }
        return servicoRetornoVO;
    }

    private boolean validarChaveEmpresa(String uuid){
        try{
            return empresasService.chaveValida(uuid);
        }catch (Exception e) {
            LogSistema.logError("validarChaveEmrpesa", e);
        }
        return false;
    }


    private boolean validarCampos(Object obj, ServicoRetornoVO servicoRetornoVO){
        String erro = "";
        Integer codigoErro = 0;

        if(obj instanceof ParametrosServicoVO){
            ParametrosServicoVO vo = (ParametrosServicoVO) obj;
            if(Objects.isNull(vo.getUuid()) ||Objects.nonNull(vo.getUuid()) && vo.getUuid().isEmpty() ){
                erro = "UUID nao informado";
                codigoErro = 140;
            }
            if(Objects.isNull(vo.getListaDePlacas()) ||Objects.nonNull(vo.getListaDePlacas()) && vo.getListaDePlacas().isEmpty() ){
                erro = "Lista de placas nao informado";
                codigoErro = 141;
            }
            servicoRetornoVO.setMensagem(erro);
            servicoRetornoVO.setCodigoRetorno(codigoErro);

            return codigoErro == 0;
        }

        if(obj instanceof ManifestoVO){
            ManifestoVO vo = (ManifestoVO) obj;
            if(Objects.isNull(vo.getUuid()) ||Objects.nonNull(vo.getUuid()) && vo.getUuid().isEmpty() ){
                erro = "UUID nao informado";
                codigoErro = 140;
            }

            if(Objects.isNull(vo.getCpfMotorista()) ||Objects.nonNull(vo.getCpfMotorista()) && vo.getCpfMotorista().isEmpty() ){
                erro = "cpfMotorista nao informado";
                codigoErro = 141;
            }

            if(Objects.isNull(vo.getPlaca()) ||Objects.nonNull(vo.getPlaca()) && vo.getPlaca().isEmpty() ){
                erro = "placa nao informado";
                codigoErro = 142;
            }

            if(Objects.isNull(vo.getData())){
                erro = "data nao informada";
                codigoErro = 143;
            }

            if(Objects.isNull(vo.getValor())){
                erro = "valor nao informado";
                codigoErro = 144;
            }
            servicoRetornoVO.setMensagem(erro);
            servicoRetornoVO.setCodigoRetorno(codigoErro);

            return codigoErro == 0;
        }


        if(obj instanceof Viagem){
            Viagem vo = (Viagem) obj;

            if(Objects.isNull(vo.getPlaca()) ||Objects.nonNull(vo.getPlaca()) && vo.getPlaca().isEmpty() ){
                erro = "placa nao informada";
                codigoErro = 141;
            }

            if(Objects.isNull(vo.getMeta())){
                erro = "meta nao informada";
                codigoErro = 142;
            }

            if(Objects.isNull(vo.getFaturamento())){
                erro = "faturamento nao informado";
                codigoErro = 143;
            }

            if(Objects.isNull(vo.getProjecao())){
                erro = "projecao nao informada";
                codigoErro = 144;
            }

            if(Objects.isNull(vo.getMediaFaturamento())){
                erro = "mediaFaturamento nao informado";
                codigoErro = 145;
            }

            if(Objects.isNull(vo.getInicioDaViagem())){
                erro = "inicioDaViagem nao informado";
                codigoErro = 146;
            }

            if(Objects.isNull(vo.getFimDaViagem())){
                erro = "fimDaViagem nao informado";
                codigoErro = 147;
            }
            servicoRetornoVO.setMensagem(erro);
            servicoRetornoVO.setCodigoRetorno(codigoErro);

            return codigoErro == 0;
        }



        return true;
    }

}
