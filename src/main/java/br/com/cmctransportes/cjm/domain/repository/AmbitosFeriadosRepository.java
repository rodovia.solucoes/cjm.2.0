package br.com.cmctransportes.cjm.domain.repository;

import br.com.cmctransportes.cjm.domain.entities.AmbitosFeriados;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AmbitosFeriadosRepository extends JpaRepository<AmbitosFeriados, Integer> {
}
