/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author analista.ti
 */
@Entity
@Table(name = "enderecos")
@Getter
@Setter
@NamedQueries({
        @NamedQuery(name = "Enderecos.findAll", query = "SELECT e FROM Enderecos e"),
        @NamedQuery(name = "Enderecos.findById", query = "SELECT e FROM Enderecos e WHERE e.id = :id"),
        @NamedQuery(name = "Enderecos.findByLogradouro", query = "SELECT e FROM Enderecos e WHERE e.logradouro = :logradouro"),
        @NamedQuery(name = "Enderecos.findByNumero", query = "SELECT e FROM Enderecos e WHERE e.numero = :numero"),
        @NamedQuery(name = "Enderecos.findByBairro", query = "SELECT e FROM Enderecos e WHERE e.bairro = :bairro"),
        @NamedQuery(name = "Enderecos.findByCep", query = "SELECT e FROM Enderecos e WHERE e.cep = :cep"),
        @NamedQuery(name = "Enderecos.findByComplemento", query = "SELECT e FROM Enderecos e WHERE e.complemento = :complemento")})
public class Enderecos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name = "ENDERECOS_ID_SEQ", sequenceName = "ENDERECOS_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENDERECOS_ID_SEQ")
    @Column(name = "id")
    private Integer id;

    @Column(name = "logradouro", length = 50, nullable = false)
    private String logradouro;

    @Column(name = "numero", length = 15, nullable = false)
    private String numero;

    @Column(name = "bairro", length = 45)
    private String bairro;

    @Column(name = "cep", length = 9)
    private String cep;

    @Column(name = "complemento", length = 50)
    private String complemento;

    @JoinColumn(name = "cidade", referencedColumnName = "cod_ibge")
    @ManyToOne(optional = false)
    private Cidades cidade;

    @Column(name = "empresa_id")
    private Integer empresaId;
}