package br.com.cmctransportes.cjm.domain.entities;

import br.com.cmctransportes.cjm.domain.services.UserService;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author analista.ti
 */
@Getter
@Setter
public class MobileDriver {

    private Integer id;
    private String nome;
    private String sobrenome;
    private Date dataNascimento; // TODO: rever isso aqui
    private Character sexo;
    private String cpf;
    private String numeroCnh;
    private Date validadeCnh; // TODO: rever isso aqui
    private String categoriaCnh;
    private String telefone;
    //    private Empresas empresaId;
    private Users user;
    private Integer empresaId;

    // Chat Broadcast
    private String chatBroadcast;
    private Evento lastEvent;

    public MobileDriver() {

    }

    public MobileDriver(Motoristas driver, UserService us) {
        setId(driver.getId());
        setNome(driver.getNome());
        setSobrenome(driver.getSobrenome());
        setDataNascimento(driver.getDataNascimento());
        setSexo(driver.getSexo());
        setCpf(driver.getCpf());
        setNumeroCnh(driver.getNumeroCnh());
        setValidadeCnh(driver.getValidadeCnh());
        setCategoriaCnh(driver.getCategoriaCnh());
        setTelefone(driver.getTelefone());

        Users tmpUser = us.getById(driver.getUserId());

        setUser(tmpUser);
        setEmpresaId(driver.getEmpresaId());
    }
}
