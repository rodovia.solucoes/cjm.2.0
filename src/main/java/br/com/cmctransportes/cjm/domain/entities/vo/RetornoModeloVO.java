package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;


public class RetornoModeloVO extends RetornoVO {
    @Getter
    @Setter
    private ModeloVO modelo;
    @Getter
    @Setter
    private List<ModeloVO> listaDeModelo;

    public RetornoModeloVO() {

    }

    public RetornoModeloVO(Integer codigoRetorno, String mensagem) {
        super(codigoRetorno, mensagem);
    }
}
