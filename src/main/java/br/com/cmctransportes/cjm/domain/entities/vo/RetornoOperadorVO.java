package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.Operador;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class RetornoOperadorVO extends RetornoVO {
    @Getter
    @Setter
    private Operador operador;
    @Getter
    @Setter
    private List<Operador> listaOperador;
}
