package br.com.cmctransportes.cjm.domain.entities.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RotaVO implements Serializable {
    private static final long serialVersionUID = 1L;
    @Getter
    @Setter
    private Integer id;
    @Getter
    @Setter
    private String nome;
    @Getter
    @Setter
    private Boolean ativo;
    @Getter
    @Setter
    private LocalVO localInicio;
    @Getter
    @Setter
    private LocalVO localFinal;
    @Getter
    @Setter
    private Integer idUnidade;
    @Getter
    @Setter
    private EmpresaVO cliente;
    @Getter
    @Setter
    private List<CoordenadaVO> listaDeCoordenadas;
    @Getter
    @Setter
    private Boolean atual;
}
