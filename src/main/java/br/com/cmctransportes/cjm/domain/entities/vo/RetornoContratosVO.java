package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.Contratos;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RetornoContratosVO extends RetornoVO {
    private List<Contratos> listaDeContratos;
    private Contratos contratos;
}
