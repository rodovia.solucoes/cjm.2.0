package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.*;
import br.com.cmctransportes.cjm.domain.services.EmpresasService;
import br.com.cmctransportes.cjm.domain.services.JornadaService;

import java.util.Date;
import java.util.Objects;

/**
 * @author Producao
 */
public class EventoMobileVO {

    private Integer empresaId;
    private Integer eventoAnterior;
    private Date instanteEvento;
    private Date instanteLancamento;
    private Integer jornadaId;
    private Double latitude;
    private Double longitude;
    private Users operadorLancamento;
    private String origem;
    private EventosJornada tipoEvento;
    private VeiculoVO veiculoMotor;
    private String versaoOrigem;

    public Evento revert(EmpresasService empresasService, JornadaService jornadaService) {
        Evento result = new Evento();

        Empresas empresa = pegaEmpresa(empresasService);
        result.setEmpresaId(empresa);

        Jornada jornada = pegaJornada(jornadaService);
        result.setJornadaId(jornada);

        result.setInstanteEvento(getInstanteEvento());
        result.setInstanteLancamento(getInstanteLancamento());
        result.setLatitude(getLatitude());
        result.setLongitude(getLongitude());
        result.setOperadorLancamento(getOperadorLancamento());
        result.setOrigem(getOrigem());
        result.setTipoEvento(getTipoEvento());
        result.setVersaoOrigem(getVersaoOrigem());

        if (Objects.nonNull(this.veiculoMotor)) {
            Veiculos veiculo = new Veiculos();
            veiculo.setPlaca(this.veiculoMotor.getPlaca());
            result.setVeiculoMotor(veiculo);
        }

        return result;
    }

    private Jornada pegaJornada(JornadaService jornadaService) {
        Jornada jornada = jornadaService.getById(getJornadaId());
        return jornada;
    }

    private Empresas pegaEmpresa(EmpresasService empresasService) {
        Empresas empresa = empresasService.getById(getEmpresaId());
        return empresa;
    }

    /**
     * @return the empresaId
     */
    public Integer getEmpresaId() {
        return empresaId;
    }

    /**
     * @param empresaId the empresaId to set
     */
    public void setEmpresaId(Integer empresaId) {
        this.empresaId = empresaId;
    }

    public VeiculoVO getVeiculoMotor() {
        return veiculoMotor;
    }

    public void setVeiculoMotor(VeiculoVO veiculoMotor) {
        this.veiculoMotor = veiculoMotor;
    }

    /**
     * @return the eventoAnterior
     */
    public Integer getEventoAnterior() {
        return eventoAnterior;
    }

    /**
     * @param eventoAnterior the eventoAnterior to set
     */
    public void setEventoAnterior(Integer eventoAnterior) {
        this.eventoAnterior = eventoAnterior;
    }

    /**
     * @return the instanteEvento
     */
    public Date getInstanteEvento() {
        return instanteEvento;
    }

    /**
     * @param instanteEvento the instanteEvento to set
     */
    public void setInstanteEvento(Date instanteEvento) {
        this.instanteEvento = instanteEvento;
    }

    /**
     * @return the instanteLancamento
     */
    public Date getInstanteLancamento() {
        return instanteLancamento;
    }

    /**
     * @param instanteLancamento the instanteLancamento to set
     */
    public void setInstanteLancamento(Date instanteLancamento) {
        this.instanteLancamento = instanteLancamento;
    }

    /**
     * @return the jornadaId
     */
    public Integer getJornadaId() {
        return jornadaId;
    }

    /**
     * @param jornadaId the jornadaId to set
     */
    public void setJornadaId(Integer jornadaId) {
        this.jornadaId = jornadaId;
    }

    /**
     * @return the latitude
     */
    public Double getLatitude() {
        return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the longitude
     */
    public Double getLongitude() {
        return longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    /**
     * @return the operadorLancamento
     */
    public Users getOperadorLancamento() {
        return operadorLancamento;
    }

    /**
     * @param operadorLancamento the operadorLancamento to set
     */
    public void setOperadorLancamento(Users operadorLancamento) {
        this.operadorLancamento = operadorLancamento;
    }

    /**
     * @return the origem
     */
    public String getOrigem() {
        return origem;
    }

    /**
     * @param origem the origem to set
     */
    public void setOrigem(String origem) {
        this.origem = origem;
    }

    /**
     * @return the tipoEvento
     */
    public EventosJornada getTipoEvento() {
        return tipoEvento;
    }

    /**
     * @param tipoEvento the tipoEvento to set
     */
    public void setTipoEvento(EventosJornada tipoEvento) {
        this.tipoEvento = tipoEvento;
    }

    /**
     * @return the versaoOrigem
     */
    public String getVersaoOrigem() {
        return versaoOrigem;
    }

    /**
     * @param versaoOrigem the versaoOrigem to set
     */
    public void setVersaoOrigem(String versaoOrigem) {
        this.versaoOrigem = versaoOrigem;
    }
}
