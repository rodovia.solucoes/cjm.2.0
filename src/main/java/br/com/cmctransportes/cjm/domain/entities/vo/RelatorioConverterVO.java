package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class RelatorioConverterVO implements Serializable {
    private FiltroVO filtro;
    private String nomeRelatorio;
    private List<InformacaoRelatorioVO> listaInformacaoRelatorio;
}
