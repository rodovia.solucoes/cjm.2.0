/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author analista.ti
 */
@Entity
@Table(name = "ambitos_feriados")
@Getter
@Setter
@NamedQueries({
        @NamedQuery(name = "AmbitosFeriados.findAll", query = "SELECT a FROM AmbitosFeriados a"),
        @NamedQuery(name = "AmbitosFeriados.findById", query = "SELECT a FROM AmbitosFeriados a WHERE a.id = :id"),
        @NamedQuery(name = "AmbitosFeriados.findByDescricao", query = "SELECT a FROM AmbitosFeriados a WHERE a.descricao = :descricao")})
public class AmbitosFeriados implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "AMBITOS_FERIADOS_ID_SEQ", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "AMBITOS_FERIADOS_ID_SEQ", sequenceName = "AMBITOS_FERIADOS_ID_SEQ", allocationSize = 1)
    @Column(name = "id")
    private Integer id;

    @Column(name = "descricao", length = 10, nullable = false)
    private String descricao;
}