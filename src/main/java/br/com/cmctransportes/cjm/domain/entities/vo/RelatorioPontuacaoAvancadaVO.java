package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class RelatorioPontuacaoAvancadaVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String nomeMotorista;
    private Integer nota;
    private Integer qtdeAcelaracaoBrusca;
    private Integer qtdeCurvaBruscaDireita;
    private Integer qtdeCurvaBruscaEsquerda;
    private Integer qtdeFreadaBrusca;
    private Integer qtdeExcessoVel80kmh;
    private Integer qtdeExcessoVel60kmhChuva;
    private Integer qtdeMarchaLenta;
    private Integer qtdeDirecaoAcimaFaixaVerde;
    private String distanciaPercorrida;
}
