package br.com.cmctransportes.cjm.domain.repository;

import br.com.cmctransportes.cjm.domain.entities.Feriados;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FeriadosRepository extends JpaRepository<Feriados, Integer> {
    List<Feriados> findByEmpresaIdId(Integer idEmpresa);
}
