package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

public class RetornoVeiculoMonitoramentoVO  extends RetornoVO{

    @Getter
    @Setter
    private VeiculoMonitoramentoVO veiculoMonitoramento;
}
