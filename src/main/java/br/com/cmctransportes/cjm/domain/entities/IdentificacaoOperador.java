package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "identificacao_operador")
@Getter
@Setter
public class IdentificacaoOperador implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "identificacao_operador_id_seq", sequenceName = "identificacao_operador_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "identificacao_operador_id_seq")
    @Column(name = "id")
    private Integer id;

    @Column(name = "descricao")
    private String descricao;

    @JoinColumn(name = "id_empresa", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Empresas empresaId;

    @Column(name = "id_unidade")
    private Integer idUnidade;

    @Column(name = "lista_motoristas")
    private String listaMotoristas;

    @Column(name = "lista_veiculos")
    private String listaVeiculos;

    @Column(name = "lista_operadores")
    private String listaOperadores;

    @Column(name = "atualizar")
    private Boolean atualizar;

    @Column(name = "quantidade_ibutton")
    private Integer quantidadeIbutton;

    @Transient
    private Integer quantidadeOperadorMotorista;

    @Transient
    private Integer quantidadDeVeiculos;
}
