package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class TemposDeFaixaVO implements Serializable {

    private static final long serialVersionUID = 1L;
    private String motorista;
    private String tempoFaixaAzul;
    private String tempoFaixaExtraEconomica;
    private String tempoFaixaVerde;
    private String tempoFaixaAmarela;
    private String tempoFaixaVermelha;
    private String tempoMarchaLenta;
}
