package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.Viagem;
import br.com.cmctransportes.cjm.infra.dao.ViagemDAO;
import br.com.cmctransportes.cjm.utils.TimeHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.sql.Time;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class ViagemService {

    private final EntityManager em;

    private ViagemDAO viagemDAO;

    public ViagemService(EntityManager em) {
        this.em = em;
        this.viagemDAO = new ViagemDAO(em);
    }


    @Transactional
    public void save(List<Viagem> listaViagens) throws Exception {
        try {
            for (Viagem v : listaViagens) {
                v.setId(null);
                v.setDataCadastro(new Date());
                if (Objects.isNull(v.getInicioDaViagem())) {
                    v.setInicioDaViagem(TimeHelper.getDate000000(new Date()));
                } else {
                    v.setInicioDaViagem(TimeHelper.getDate000000(v.getInicioDaViagem()));
                }
                if (Objects.isNull(v.getFimDaViagem())) {
                    v.setFimDaViagem(TimeHelper.getDate235959(new Date()));
                } else {
                    v.setFimDaViagem(TimeHelper.getDate235959(v.getFimDaViagem()));
                }
                this.viagemDAO.save(v);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional
    public void save(Viagem v) throws Exception {
        try {
            v.setId(null);
            v.setDataCadastro(new Date());
            if (Objects.isNull(v.getInicioDaViagem())) {
                v.setInicioDaViagem(TimeHelper.getDate000000(new Date()));
            } else {
                v.setInicioDaViagem(TimeHelper.getDate000000(v.getInicioDaViagem()));
            }
            if (Objects.isNull(v.getFimDaViagem())) {
                v.setFimDaViagem(TimeHelper.getDate235959(new Date()));
            } else {
                v.setFimDaViagem(TimeHelper.getDate235959(v.getFimDaViagem()));
            }
            this.viagemDAO.save(v);

        } catch (Exception e) {
            throw e;
        }
    }

    public List<Viagem> buscarPorVeiculo(String placa) throws Exception {
        try {
            return this.viagemDAO.findAll(placa);
        } catch (Exception e) {
            throw e;
        }
    }


    public List<Viagem> buscarPorVeiculo(String placa, Date dataInicial, Date dataFinal) throws Exception {
        try {
            return this.viagemDAO.findAll(placa, dataInicial, dataFinal);
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional
    public void excluir(List<Integer> listVagem) {
        try {
            for (Integer id : listVagem) {
                Viagem v = this.viagemDAO.getById(id);
                this.viagemDAO.delete(v);
            }
        } catch (Exception e) {
            throw e;
        }
    }
}
