package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class DetalheDashboardVO implements Serializable {

    public DetalheDashboardVO() {
        this.veiculoLigado = false;
        this.dataUltimaAtualizacao = "-";
        this.velocidade = "-";
        this.direcao = "-";
        this.bloqueado = false;
        this.voltagemBateria = "-";
        this.modulo = "-";
        this.imei = "-";
        this.kmRodado = "-";
        this.velocidadeMedia = "-";
        this.velocidadeMaxima = "-";
        this.tempoLigado = "-";
        this.uf = "-";
        this.cidade = "-";
        this.endereco = "-";
        this.latLong = "-";
        this.hodometro = "-";
        this.horimetro = "-";
        this.tempoTotalLigado = "-";
        this.tempoTotalDesligado = "-";
        this.qtdeFreadaBrusca = 0;
        this.qtdeArrancadaBrusca = 0;
        this.qtdeCurvaBrusca = 0;
        this.tipoDeEmergencia = "-";
        this.tempoTotalTomadaDeForcaAcionada = "-";
        this.tempoCompressorLigado = "-";
        this.temperaturaCamaraFria = "-";
        this.temperaturaForaDoParametro = "-";
        this.tipoEntradaDigitalUm = "Entrada Um";
        this.valorEntradaDigitalUm = "-";
        this.tipoEntradaDigitalDois = "Entrada Dois";
        this.valorEntradaDigitalDois = "-";
    }

    private boolean veiculoLigado;
    private String dataUltimaAtualizacao;
    private String velocidade;
    private String direcao;
    private boolean bloqueado;
    private String voltagemBateria;
    private String modulo;
    private String imei;
    private String kmRodado;
    private String velocidadeMedia;
    private String velocidadeMaxima;
    private String tempoLigado;
    private String uf;
    private String cidade;
    private String endereco;
    private String latLong;
    private String hodometro;
    private String horimetro;
    private String tempoTotalLigado;
    private String tempoTotalDesligado;
    private int qtdeFreadaBrusca;
    private int qtdeArrancadaBrusca;
    private int qtdeCurvaBrusca;
    private String tipoDeEmergencia;
    private String tempoTotalTomadaDeForcaAcionada;
    private String tempoCompressorLigado;
    private String temperaturaCamaraFria;
    private String temperaturaForaDoParametro;
    private String dataEventoViam = "-";
    private String veiculoAtrelado = "-";
    private boolean emDirecao = false;
    private boolean comInterjornada = false;
    private boolean emCarga = false;
    private boolean aguardandoDescarregamento = false;
    private boolean emAbastecimento = false;
    private boolean emDescarga = false;
    private boolean aDisposicao = false;
    private boolean aguardandoCarregamento = false;
    private boolean outroEvento = false;
    private String nomeDoEvento;
    private List<ObservacaoVO> listaObservacao = new ArrayList<>();
    private List<EventoVO> listaEvento = new ArrayList<>();
    private List<InformacaoRelatorioVO> listaInformacaoRelatorioVO = new ArrayList<>();
    private int quantidadeDeFotoViaM;
    private String tipoEntradaDigitalUm;
    private String valorEntradaDigitalUm;
    private String tipoEntradaDigitalDois;
    private String valorEntradaDigitalDois;


}
