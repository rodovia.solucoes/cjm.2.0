package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * @author William Leite
 */
@Getter
@Setter
public class JourneyManifestVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Integer journey;
    private Date startDate;
    private Date endDate;
    private String manifest;
}
