package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MotoristaOperadorVO {
    private Integer id;
    private String nome;
    private String categoriaCnh;
    private String ibutton;
    private String funcao;
    private String tipo;
}
