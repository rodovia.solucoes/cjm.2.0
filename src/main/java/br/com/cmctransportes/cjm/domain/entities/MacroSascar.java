package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MacroSascar {
    private int idEmpresa;
    private int idSascar;
    private int idRodovia;
    private String descricao;
    private boolean ativos;
    private String mensagem;
}
