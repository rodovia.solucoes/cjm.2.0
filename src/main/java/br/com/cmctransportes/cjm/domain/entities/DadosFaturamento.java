package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class DadosFaturamento implements Serializable {
    private Integer diasApurados;
    private Double meta;
    private Double metaPorDia;
    private Double faturamento;
    private Double faturamentoPorDia;
    private Double dispersao;
    private Double porcetagemFaturamento;
}
