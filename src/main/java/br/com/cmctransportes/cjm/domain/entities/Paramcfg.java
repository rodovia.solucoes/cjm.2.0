package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Representa os parametros do sistema
 * <p>
 * #2033016
 *
 * @author Cristhiano Roberto
 */
@Entity
@Table(name = "paramcfg")
@Getter
@Setter
@NamedQueries({
        @NamedQuery(name = "Paramcfg.findAll", query = "SELECT p FROM Paramcfg p"),
        @NamedQuery(name = "Paramcfg.findById", query = "SELECT p FROM Paramcfg p WHERE p.id = :id")
})
public class Paramcfg implements Serializable {

    @Id
    @SequenceGenerator(name = "PARAMCFG_ID_SEQ", sequenceName = "PARAMCFG_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PARAMCFG_ID_SEQ")
    @Column(name = "id")
    private Integer id;

    @Column(name = "grcodigo", length = 80, nullable = false)
    private String grcodigo;

    @Column(name = "ticodigo", length = 80, nullable = false)
    private String ticodigo;

    @JoinColumn(name = "empresaid", referencedColumnName = "id")
    @ManyToOne(optional = true)
    private Empresas empresaId;

    @JoinColumn(name = "unidadeid", referencedColumnName = "id")
    @ManyToOne(optional = true)
    private Unidades unidadeId;

    @Column(name = "dado", length = 200, nullable = false)
    private String dado;

    @Column(name = "nometela", length = 255)
    private String nometela;

    @Column(name = "desctela", length = 1000)
    private String desctela;
}