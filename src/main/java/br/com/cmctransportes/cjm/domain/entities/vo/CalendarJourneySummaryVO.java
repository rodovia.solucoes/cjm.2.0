package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author William Leite
 */
public class CalendarJourneySummaryVO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private String icon;
    @Getter
    @Setter
    private Long total;
    @Getter
    @Setter
    private Long dayTime;
    @Getter
    @Setter
    private Long nightTime;
}
