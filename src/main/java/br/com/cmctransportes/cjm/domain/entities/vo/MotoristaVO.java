package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.Motoristas;
import br.com.cmctransportes.cjm.domain.entities.Unidades;
import br.com.cmctransportes.cjm.domain.entities.Users;
import br.com.cmctransportes.cjm.domain.services.UnidadesService;
import br.com.cmctransportes.cjm.domain.services.UserService;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.Objects;

/**
 * @author analista.ti
 */
public class MotoristaVO {

    private Integer id;
    private String nome;
    private String sobrenome;
    private Date dataNascimento;
    private Character sexo;
    private String cpf;
    private String numeroCnh;
    private Date validadeCnh;
    private String categoriaCnh;
    private String telefone;
    private Integer empresaId;
    private String unidade;
    private String userName;
    private String turno;
    private Date dataAdmissao;

    private String enderecoLogradouro; //<dd>{{motorista.enderecosId.logradouro}}</dd>
    private String enderecoNumero; // <dd>{{motorista.enderecosId.numero}}</dd>
    private String enderecoComplemento; //<dd>{{motorista.enderecosId.complemento}}</dd>
    private String enderecoBairro; //<dd>{{motorista.enderecosId.bairro}}</dd>
    private String enderecoCidade; //<dd>{{motorista.enderecosId.cidade.nome}}</dd>
    private String enderecoUF; //<dd>{{motorista.enderecosId.cidade.ufCodIbge.sigla}}</dd>
    private String enderecoCEP; //<dd>{{motorista.enderecosId.cep}}</dd>

    private Date dataDemissao;

    @Getter
    @Setter
    private Long faltantes;
    @Getter
    @Setter
    private Integer jornadaId;

    public MotoristaVO() {

    }

    public MotoristaVO(Integer id) {
        this.id = id;
    }

    public MotoristaVO(Integer id, String nome, String sobrenome) {
        this.id = id;
        setNome(nome);
        setSobrenome(sobrenome);
    }

    public MotoristaVO(Motoristas driver, UserService userService, UnidadesService us) {
        setId(driver.getId());
        setNome(driver.getNome());
        setSobrenome(driver.getSobrenome());
        setDataNascimento(driver.getDataNascimento());
        setSexo(driver.getSexo());
        setCpf(driver.getCpf());
        setNumeroCnh(driver.getNumeroCnh());
        setValidadeCnh(driver.getValidadeCnh());
        setCategoriaCnh(driver.getCategoriaCnh());
        setTelefone(driver.getTelefone());

        Unidades u = us.getById(driver.getUnidadesId());
        setUnidade(u.getIdentificacao());

        Users user = userService.getById(driver.getUserId());
        if (Objects.nonNull(user)) {
            setUserName(user.getUserName());
        }

        setEmpresaId(driver.getEmpresaId());

        setEnderecoLogradouro(driver.getEnderecosId().getLogradouro());
        setEnderecoNumero(driver.getEnderecosId().getNumero());
        setEnderecoComplemento(driver.getEnderecosId().getComplemento());
        setEnderecoBairro(driver.getEnderecosId().getBairro());
        setEnderecoCidade(driver.getEnderecosId().getCidade().getNome());
        setEnderecoUF(driver.getEnderecosId().getCidade().getUfCodIbge().getSigla());
        setEnderecoCEP(driver.getEnderecosId().getCep());

        setTurno(driver.getTurnosId().getIdentificacao());
        setDataAdmissao(driver.getDataAdmissao());
        setDataDemissao(driver.getDataDemissao());
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the sobrenome
     */
    public String getSobrenome() {
        return sobrenome;
    }

    /**
     * @param sobrenome the sobrenome to set
     */
    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    /**
     * @return the dataNascimento
     */
    public Date getDataNascimento() {
        return dataNascimento;
    }

    /**
     * @param dataNascimento the dataNascimento to set
     */
    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    /**
     * @return the sexo
     */
    public Character getSexo() {
        return sexo;
    }

    /**
     * @param sexo the sexo to set
     */
    public void setSexo(Character sexo) {
        this.sexo = sexo;
    }

    /**
     * @return the cpf
     */
    public String getCpf() {
        return cpf;
    }

    /**
     * @param cpf the cpf to set
     */
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    /**
     * @return the numeroCnh
     */
    public String getNumeroCnh() {
        return numeroCnh;
    }

    /**
     * @param numeroCnh the numeroCnh to set
     */
    public void setNumeroCnh(String numeroCnh) {
        this.numeroCnh = numeroCnh;
    }

    /**
     * @return the validadeCnh
     */
    public Date getValidadeCnh() {
        return validadeCnh;
    }

    /**
     * @param validadeCnh the validadeCnh to set
     */
    public void setValidadeCnh(Date validadeCnh) {
        this.validadeCnh = validadeCnh;
    }

    /**
     * @return the categoriaCnh
     */
    public String getCategoriaCnh() {
        return categoriaCnh;
    }

    /**
     * @param categoriaCnh the categoriaCnh to set
     */
    public void setCategoriaCnh(String categoriaCnh) {
        this.categoriaCnh = categoriaCnh;
    }

    /**
     * @return the telefone
     */
    public String getTelefone() {
        return telefone;
    }

    /**
     * @param telefone the telefone to set
     */
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    /**
     * @return the empresaId
     */
    public Integer getEmpresaId() {
        return empresaId;
    }

    /**
     * @param empresaId the empresaId to set
     */
    public void setEmpresaId(Integer empresaId) {
        this.empresaId = empresaId;
    }

    /**
     * @return the unidade
     */
    public String getUnidade() {
        return unidade;
    }

    /**
     * @param unidade the unidade to set
     */
    public void setUnidade(String unidade) {
        this.unidade = unidade;
    }

    /**
     * @return the enderecoLogradouro
     */
    public String getEnderecoLogradouro() {
        return enderecoLogradouro;
    }

    /**
     * @param enderecoLogradouro the enderecoLogradouro to set
     */
    public void setEnderecoLogradouro(String enderecoLogradouro) {
        this.enderecoLogradouro = enderecoLogradouro;
    }

    /**
     * @return the enderecoNumero
     */
    public String getEnderecoNumero() {
        return enderecoNumero;
    }

    /**
     * @param enderecoNumero the enderecoNumero to set
     */
    public void setEnderecoNumero(String enderecoNumero) {
        this.enderecoNumero = enderecoNumero;
    }

    /**
     * @return the enderecoComplemento
     */
    public String getEnderecoComplemento() {
        return enderecoComplemento;
    }

    /**
     * @param enderecoComplemento the enderecoComplemento to set
     */
    public void setEnderecoComplemento(String enderecoComplemento) {
        this.enderecoComplemento = enderecoComplemento;
    }

    /**
     * @return the enderecoBairro
     */
    public String getEnderecoBairro() {
        return enderecoBairro;
    }

    /**
     * @param enderecoBairro the enderecoBairro to set
     */
    public void setEnderecoBairro(String enderecoBairro) {
        this.enderecoBairro = enderecoBairro;
    }

    /**
     * @return the enderecoCidade
     */
    public String getEnderecoCidade() {
        return enderecoCidade;
    }

    /**
     * @param enderecoCidade the enderecoCidade to set
     */
    public void setEnderecoCidade(String enderecoCidade) {
        this.enderecoCidade = enderecoCidade;
    }

    /**
     * @return the enderecoUF
     */
    public String getEnderecoUF() {
        return enderecoUF;
    }

    /**
     * @param enderecoUF the enderecoUF to set
     */
    public void setEnderecoUF(String enderecoUF) {
        this.enderecoUF = enderecoUF;
    }

    /**
     * @return the enderecoCEP
     */
    public String getEnderecoCEP() {
        return enderecoCEP;
    }

    /**
     * @param enderecoCEP the enderecoCEP to set
     */
    public void setEnderecoCEP(String enderecoCEP) {
        this.enderecoCEP = enderecoCEP;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the turno
     */
    public String getTurno() {
        return turno;
    }

    /**
     * @param turno the turno to set
     */
    public void setTurno(String turno) {
        this.turno = turno;
    }

    /**
     * @return the dataAdmissao
     */
    public Date getDataAdmissao() {
        return dataAdmissao;
    }

    /**
     * @param dataAdmissao the dataAdmissao to set
     */
    public void setDataAdmissao(Date dataAdmissao) {
        this.dataAdmissao = dataAdmissao;
    }

    public Date getDataDemissao() {
        return dataDemissao;
    }

    public void setDataDemissao(Date dataDemissao) {
        this.dataDemissao = dataDemissao;
    }
}
