package br.com.cmctransportes.cjm.domain.entities.enums;

/**
 * @author William Leite
 */
public enum OPERATIONS {
    SAVE_EVENT,
    DELETE_EVENT,
    ALLOWANCE_LUNCH_REMAINING,
    CREATE_ALLOWANCE,
    CREATE_EVENT,
    MANUAL_DSR,
    ALLOWANCE_REMAINING,
    MARK_DSR_HE,
    MARK_AWAITING_AS_WORKED
}
