package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public class CoordenadaVO implements Serializable {

    @Getter
    @Setter
    private Integer id;
    @Getter
    @Setter
    private Integer sequencia;
    @Getter
    @Setter
    private Double latitude;
    @Getter
    @Setter
    private Double longitude;
    @Getter
    @Setter
    private Boolean isRaio;
    @Getter
    @Setter
    private Double raio;
}
