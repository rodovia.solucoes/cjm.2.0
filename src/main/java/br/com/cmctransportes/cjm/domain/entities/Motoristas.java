package br.com.cmctransportes.cjm.domain.entities;

import br.com.cmctransportes.cjm.domain.services.UnidadesService;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;

/**
 * @author analista.ti
 */
@Entity
@Table(name = "motoristas", uniqueConstraints={@UniqueConstraint(columnNames="id_sascar")}
)
@SecondaryTable(name="sascar")
@Getter
@Setter
@NamedQueries({
        @NamedQuery(name = "Motoristas.findAll", query = "SELECT m FROM Motoristas m"),
        @NamedQuery(name = "Motoristas.findByCompany", query = "SELECT m FROM Motoristas m WHERE m.objEmpresa.id = :idEmpresa"),
        @NamedQuery(name = "Motoristas.findById", query = "SELECT m FROM Motoristas m WHERE m.id = :id"),
        @NamedQuery(name = "Motoristas.findByNome", query = "SELECT m FROM Motoristas m WHERE m.nome = :nome"),
        @NamedQuery(name = "Motoristas.findBySobrenome", query = "SELECT m FROM Motoristas m WHERE m.sobrenome = :sobrenome"),
        @NamedQuery(name = "Motoristas.findByDataNascimento", query = "SELECT m FROM Motoristas m WHERE m.dataNascimento = :dataNascimento"),
        @NamedQuery(name = "Motoristas.findBySexo", query = "SELECT m FROM Motoristas m WHERE m.sexo = :sexo"),
        @NamedQuery(name = "Motoristas.findByCpf", query = "SELECT m FROM Motoristas m WHERE m.cpf = :cpf"),
        @NamedQuery(name = "Motoristas.findByNumeroCnh", query = "SELECT m FROM Motoristas m WHERE m.numeroCnh = :numeroCnh"),
        @NamedQuery(name = "Motoristas.findByCategoriaCnh", query = "SELECT m FROM Motoristas m WHERE m.categoriaCnh = :categoriaCnh"),
        @NamedQuery(name = "Motoristas.findByTelefone", query = "SELECT m FROM Motoristas m WHERE m.telefone = :telefone"),
        @NamedQuery(name = "Motoristas.findBySascarId", query = "SELECT m FROM Motoristas m WHERE m.sascarId = :sascarId"),
        @NamedQuery(name = "Motoristas.findByUserId", query = "SELECT m FROM Motoristas m WHERE m.userId = :userId")})
public class Motoristas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name = "motoristas_id_gen", sequenceName = "motoristas_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "motoristas_id_gen")
    @Column(name = "id")
    private Integer id;

    @Column(name = "nome", length = 50, nullable = false)
    private String nome;

    @Column(name = "sobrenome", length = 50, nullable = false)
    private String sobrenome;

    @Column(name = "data_nascimento", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date dataNascimento;

    @Column(name = "sexo", nullable = false)
    private Character sexo;

    @Column(name = "cpf", length = 11, nullable = false)
    private String cpf;

    @Column(name = "numero_cnh", length = 15, nullable = false)
    private String numeroCnh;

    @Column(name = "validade_cnh", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date validadeCnh;

    @Column(name = "data_admissao")
    @Temporal(TemporalType.DATE)
    private Date dataAdmissao;

    @Column(name = "categoria_cnh", length = 2, nullable = false)
    private String categoriaCnh;

    @Column(name = "telefone", length = 15)
    private String telefone;

    @JoinColumn(name = "empresa_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @JsonIgnore
    private Empresas objEmpresa;

    @JoinColumn(name = "enderecos_id", referencedColumnName = "id")
    @OneToOne(optional = false)
    private Enderecos enderecosId;

    @JoinColumn(name = "turnos_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Turnos turnosId;

    @Column(name = "unidades_id")
    private Integer unidadesId;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "motoristaId")
    private Collection<Ferias> ferias;

    @OneToMany(mappedBy = "motoristasId")
    private Collection<MotoristasVeiculos> veiculos;

    @JoinColumn(name = "user_id", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(optional = true)
    private Users user;

    @Column(name = "user_id")
    private Integer userId;

    @Column(name = "id_sascar")
    private Integer sascarId;


    @JoinColumn(name = "imagem_id", referencedColumnName = "id")
    @ManyToOne(optional = true)
    private Imagem imagemId;

    @Column(name = "situacao", nullable = false)
    private Integer situacao;

    @Column(name = "data_demissao")
    @Temporal(TemporalType.DATE)
    private Date dataDemissao;

    @Column(name = "numero_matricula")
    private String numeroMatricula;

    @Column(name = "macros_id")
    private Integer macroId;

    @Column(name = "ibutton")
    private String ibutton;

    @Transient
    private Boolean isAtivo;

    @Transient
    public Integer getEmpresaId() {

        if (objEmpresa == null) {
            return null;
        }

        return objEmpresa.getId();
    }

    @Transient
    public void setEmpresaId(Integer empresaId) {
        objEmpresa = new Empresas(empresaId);
    }

    @JsonIgnore
    public Empresas getEmpresa() {
        return objEmpresa;
    }

    public void setUser(Integer userId) {
        this.userId = userId;
    }

    public void setEmpresa(Empresas empresa) {
        this.objEmpresa = empresa;
    }

    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true) // otherwise first ref as POJO, others as id
    public void setTurnosId(Turnos turnosId) {
        this.turnosId = turnosId;
    }

    public Cidades pegaCidadeLotacao(UnidadesService unidadesService) {
        Cidades result = null;


        Unidades unidade = unidadesService.getById(this.getUnidadesId());

        if ((unidade != null) &&
                (unidade.getEndereco() != null)) {
            result = unidade.getEndereco().getCidade();
        }

        return result;
    }

    public Integer getSascarId(Motoristas id) {
        return sascarId;
    }

    public Motoristas(){

    }

    public Motoristas(Integer id) {
        this.id = id;
    }

    public Motoristas(Integer id, Turnos turnosId) {
        this.id = id;
        this.turnosId = turnosId;
    }

    public Motoristas(Integer id, String nome, String sobrenome, Integer situacao) {
        this.id = id;
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.situacao = situacao;
    }

    public Motoristas(Integer id, String nome, String sobrenome) {
        this.id = id;
        this.nome = nome;
        this.sobrenome = sobrenome;
    }


    public Motoristas(Integer id, String nome, String sobrenome, Integer situacao, Turnos turnos, Integer unidadesId) {
        this.id = id;
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.situacao = situacao;
        this.turnosId = turnos;
        this.unidadesId = unidadesId;
    }

    public Motoristas(Integer id, String nome, String sobrenome, Date dataNascimento,
                      Character sexo, String cpf, String numeroCnh, String categoriaCnh,
   Date validadeCnh, String telefone, Turnos turnos, Integer unidadesId, Date dataDemissao, Integer situacao ) {
        this.id = id;
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.dataNascimento = dataNascimento;
        this.sexo = sexo;
        this.cpf = cpf;
        this.numeroCnh = numeroCnh;
        this.categoriaCnh = categoriaCnh;
        this.validadeCnh = validadeCnh;
        this.telefone = telefone;
        this.turnosId = turnos;
        this.unidadesId = unidadesId;
        this.dataDemissao = dataDemissao;
        this.situacao = situacao;
    }
    public void limparCampos(){
        this.user = null;
        this.veiculos  = null;
        this.imagemId = null;
        this.enderecosId = null;

    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 13 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Motoristas other = (Motoristas) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

}