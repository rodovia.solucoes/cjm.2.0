package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class VeiculoAutorizacaoVO implements Serializable {

    private Integer id;
    private String placa;
    private String imei;
    private String fabricanteVeiculo;
    private String modeloVeiculo;
    private String fabricanteEquipamento;
    private String modeloEquipamento;

}
