package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * @author William Leite
 */
public class MonthVO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private Date start;
    @Getter
    @Setter
    private Date end;
}