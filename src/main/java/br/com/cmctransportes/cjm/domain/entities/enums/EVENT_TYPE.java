package br.com.cmctransportes.cjm.domain.entities.enums;

import lombok.Getter;

/**
 * @author William Leite
 */
public enum EVENT_TYPE {
    /**
     * StartCode: 0
     * EndCode: 0
     */
    FIM_JORNADA(0, "Fim de Jornada"),
    /**
     * StartCode: 1
     * EndCode: -1
     */
    INICIO_JORNADA(1, "Jornada"),
    /**
     * StartCode: 2
     * EndCode: -2
     */
    MANIFESTO(2, "Manifesto"),
    /**
     * StartCode: 3
     * EndCode: -3
     */
    MANUTENCAO(3, "Manutenção"),
    /**
     * StartCode: 4
     * EndCode: -4
     */
    ACIDENTE(4, "Incidente"),
    /**
     * StartCode: 5
     * EndCode: -5
     */
    LANCHE(5, "Lanche"),
    /**
     * StartCode: 6
     * EndCode: -6
     */
    BANHEIRO(6, "Banheiro"),
    /**
     * StartCode: 7
     * EndCode: -7
     */
    ABASTECIMENTO(7, "Abastecimento"),
    /**
     * StartCode: 8
     * EndCode: -8
     */
    DIRECAO(8, "Direção"),
    /**
     * StartCode: 9
     * EndCode: -9
     */
    AGUARDANDO_CARGA(9, "Aguardando Carga"),
    /**
     * StartCode: 10
     * EndCode: -10
     */
    CARGA(10, "Carga"),
    /**
     * StartCode: 11
     * EndCode: -11
     */
    AGUARDANDO_DESCARGA(11, "Aguardando Descarga"),
    /**
     * StartCode: 12
     * EndCode: -12
     */
    DESCARGA(12, "Descarga"),
    /**
     * StartCode: 13
     * EndCode: -13
     */
    FISCALIZACAO(13, "Fiscalização"),
    /**
     * StartCode: 14
     * EndCode: -14
     */
    REFEICAO(14, "Refeição"),
    /**
     * StartCode: 15
     * EndCode: -15
     */
    DESCANSO(15, "Descanso"),
    /**
     * StartCode: 16
     * EndCode: -16
     */
    DESCANSO_FEMININO(16, "Descanso Feminino"),
    /**
     * StartCode: 17
     * EndCode: -17
     */
    ABONO(17, "Início Abono"),
    /**
     * StartCode: 18
     * EndCode: -18
     */
    FERIAS(18, "Ferias"),
    /**
     * StartCode: 19
     * EndCode: -19
     */
    FALTA(19, "Falta"),
    /**
     * StartCode: 20
     * EndCode: -20
     */
    DSR(20, "DSR"),
    /**
     * StartCode: 21
     * EndCode: -21
     */
    FOLGA(21, "Folga"),
    /**
     * StartCode: 22
     * EndCode: -22
     */
    FERIADO(22, "Feriado"),
    /**
     * StartCode: 23
     * EndCode: -23
     */
    PISTA_INTERROMPIDA(23, "Pista Interrompida"),
    /**
     * StartCode: 24
     * EndCode: -24
     */
    ABONO_REFEICAO(24, "Abono Refeição"),
    /**
     * StartCode: 25
     * EndCode: -25
     */
    INTERVALO_PESSOAL(25, "Intervalo Pessoal"),
    /**
     * StartCode: 26
     * EndCode: -26
     */
    INTERJORNADA(26, "Interjornada");

    @Getter
    private final int startCode;
    @Getter
    private final int endCode;
    @Getter
    private final String description;

    EVENT_TYPE(final int code, final String description) {
        this.startCode = code;
        this.endCode = Math.negateExact(code);
        this.description = description;
    }
}
