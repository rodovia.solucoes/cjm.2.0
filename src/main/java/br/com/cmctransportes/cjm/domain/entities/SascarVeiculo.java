package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "sascar_veiculo")
@Getter
@Setter
public class SascarVeiculo implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "SASCAR_VEICULO_ID_SEQ", sequenceName = "SASCAR_VEICULO_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SASCAR_VEICULO_ID_SEQ")
    @Column(name = "id")
    private Integer id;
    @Column(name = "placa")
    private String placa;
    @Column(name = "id_sascar")
    private String idSascar;
    @Column(name = "empresa")
    private Integer empresa;
    @Column(name = "status")
    private Boolean status;
    @Column(name="data_atualizacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataAtualizacao;
    @Column(name="manual")
    private Boolean manual;

}
