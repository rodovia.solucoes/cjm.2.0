package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author William Leite
 */
@Entity
@Table(name = "journey_calculation_line")
@Getter
@Setter
public class JourneyCalculationLine implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "JOURNEY_CALCULATION_LINE_ID_SEQ", sequenceName = "JOURNEY_CALCULATION_LINE_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "JOURNEY_CALCULATION_LINE_ID_SEQ")
    @Column(name = "id")
    private Long id;

    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;

    @Column(name = "daily_extra_hours_total")
    private Long dailyExtraHoursTotal;

    @Column(name = "daily_extra_hours_intrajourney")
    private Long dailyExtraHoursIntrajourney;

    @Column(name = "daily_extra_hours_dsr")
    private Long dailyExtraHoursDSR;

    @Column(name = "daily_extra_hours_holiday")
    private Long dailyExtraHoursHoliday;

    @Column(name = "nightly_extra_hours_total")
    private Long nightlyExtraHoursTotal;

    @Column(name = "nightly_extra_hours_intrajourney")
    private Long nightlyExtraHoursIntrajourney;

    @Column(name = "nightly_extra_hours_dsr")
    private Long nightlyExtraHoursDSR;

    @Column(name = "nightly_extra_hours_holiday")
    private Long nightlyExtraHoursHoliday;

    @Column(name = "interjourney_extra_hours")
    private Long interjourneyExtraHours;

    @Column(name = "daily_awaiting")
    private Long dailyAwaiting;

    @Column(name = "nightly_awaiting")
    private Long nightlyAwaiting;

    @Column(name = "aditional_nightly")
    private Long aditionalNightly;

    @Column(name = "graced_missing_hours")
    private Long gracedMissingHours;

    @Column(name = "deducted_missing_hours")
    private Long deductedMissingHours;

    @Column(name = "total_hours")
    private Long totalHours;

    @Column(name = "journey_type")
    private String journeyType;

    @ManyToOne
    @JoinColumn(name = "journey_calculation_id", referencedColumnName = "id")
    private JourneyCalculation journeyCalculation;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "line")
    private List<Nonconformity> nonconformities;
}