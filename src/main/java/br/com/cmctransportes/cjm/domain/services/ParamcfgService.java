package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.Empresas;
import br.com.cmctransportes.cjm.domain.entities.Paramcfg;
import br.com.cmctransportes.cjm.domain.entities.Unidades;
import br.com.cmctransportes.cjm.domain.entities.enums.PARAMCFG_GRUPO;
import br.com.cmctransportes.cjm.domain.entities.enums.PARAMCFG_PARAMETRO;
import br.com.cmctransportes.cjm.infra.dao.ParamcfgDAO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.function.Function;

/**
 * Serviços para os parametros do sistema
 * <p>
 * #2033016
 *
 * @author Cristhiano Roberto
 */
@Service
public class ParamcfgService {

    private ParamcfgDAO dao;

    private final EntityManager em;

    public ParamcfgService(EntityManager em) {
        this.em = em;
        this.dao = new ParamcfgDAO(em);
    }

    @Transactional
    public void save(Paramcfg paramcfg) {
        try {
            dao.save(paramcfg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void update(Paramcfg paramcfg) {
        try {
            dao.update(paramcfg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void delete(Paramcfg paramcfg) {
        try {
            dao.delete(paramcfg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Paramcfg> findAll() {
        return dao.findAll();
    }

    public List<Paramcfg> findAll(Integer idEmpresa) {
        return dao.findAll(idEmpresa);
    }

    public Paramcfg findById(Integer id) {
        return dao.getById(id);
    }

    public List<Paramcfg> findByGroup(PARAMCFG_GRUPO paramcfg_grupo) {
        return this.findByGroup(paramcfg_grupo.name());
    }

    public List<Paramcfg> findByGroup(String paramcfg_grupo) {
        return dao.findByGroup(paramcfg_grupo);
    }

    /**
     * Procura pelo parametro
     * <p>
     * #2033016
     *
     * @param paramcfg_grupo     Grupo do parametro
     * @param paramcfg_parametro Nome do parametro
     * @param empresa            Empresa a qual o parametro pertence
     * @param unidade            Unidade a qual o parametro pertence
     * @return Parametro que corresponde ao filtro.
     */
    public Paramcfg find(PARAMCFG_GRUPO paramcfg_grupo, PARAMCFG_PARAMETRO paramcfg_parametro, Empresas empresa, Unidades unidade) {
        Paramcfg filter = new Paramcfg();
        filter.setEmpresaId(empresa);
        filter.setUnidadeId(unidade);
        filter.setTicodigo(paramcfg_parametro.name());
        filter.setGrcodigo(paramcfg_grupo.name());

        Paramcfg result = dao.findParam(filter);
        return result;
    }

    /**
     * Procura pelo dado do parametro
     * <p>
     * #2033016
     *
     * @param paramcfg_grupo     Grupo do parametro
     * @param paramcfg_parametro Nome do parametro
     * @param empresa            Empresa a qual o parametro pertence
     * @param unidade            Unidade a qual o parametro pertence
     * @return Dado do parametro que corresponde ao filtro, null se não existente.
     */
    public String findDado(PARAMCFG_GRUPO paramcfg_grupo, PARAMCFG_PARAMETRO paramcfg_parametro, Empresas empresa, Unidades unidade) {

        Paramcfg filter = new Paramcfg();
        filter.setEmpresaId(empresa);
        filter.setUnidadeId(unidade);
        filter.setTicodigo(paramcfg_parametro.name());
        filter.setGrcodigo(paramcfg_grupo.name());

        Paramcfg result = dao.findParam(filter);

        return result != null ? result.getDado() : null;
    }

    public String findDado(PARAMCFG_GRUPO paramcfg_grupo, PARAMCFG_PARAMETRO paramcfg_parametro, Empresas empresa) {
        return this.findDado(paramcfg_grupo, paramcfg_parametro, empresa, null);
    }

    public String findDado(PARAMCFG_GRUPO paramcfg_grupo, PARAMCFG_PARAMETRO paramcfg_parametro) {
        return this.findDado(paramcfg_grupo, paramcfg_parametro, null, null);
    }

    public List<Paramcfg> findByGroup(String paramcfg_grupo, Integer empresaId) {
        return dao.findByGroup(paramcfg_grupo, empresaId);
    }

    public <T> T get(PARAMCFG_GRUPO group, PARAMCFG_PARAMETRO parameter, Function<String, T> converter) {
        String data = this.findDado(group, parameter);
        return converter.apply(data);
    }
}
