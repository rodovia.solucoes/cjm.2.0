package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.TipoRanking;
import br.com.cmctransportes.cjm.infra.dao.TipoRankingDAO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

@Service
public class TipoRankingService {
    private TipoRankingDAO dao;

    private final EntityManager em;

    public TipoRankingService(EntityManager em) {
        this.em = em;
        dao = new TipoRankingDAO(em);
    }

    @Transactional
    public void save(TipoRanking tipo) {
        try {
            tipo.setDataCadastro(new Date());
            dao.save(tipo);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public TipoRanking getById(Integer id) {
        return dao.getById(id);
    }

    public List<TipoRanking> buscarLista(Integer idCliente, Integer idUnidade) throws Exception{
        return dao.buscarLista(idCliente, idUnidade);
    }

    @Transactional
    public void update(TipoRanking tipo) {
        try {
            dao.update(tipo);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Transactional
    public void delete(Integer id) {
        try {
            TipoRanking tipo = getById(id);
            dao.delete(tipo);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
}
