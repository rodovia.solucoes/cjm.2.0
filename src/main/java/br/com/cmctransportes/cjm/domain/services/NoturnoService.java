package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.vo.DiurnoNoturno;
import br.com.cmctransportes.cjm.utils.TimeHelper;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Objects;

/**
 * @author analista.ti
 */
public class NoturnoService {

    //    private final long TEMPO_24H = (1000 * 60 * 60 * 24);
    private static final int NIGHTLY_INIT_HOUR = 22;
    private static final int NIGHTLY_FINAL_HOUR = 5;

    /**
     * Set's to the date passed as a parameter the hour passed as a parameter while zeroing the
     * minutes, seconds and milliseconds.
     *
     * @param date Date to set
     * @param hour Hour to set
     * @return Date with the specified hour set
     */
    private static Date setTime(Date date, int hour) {
        if (Objects.isNull(date)) {
            return null;
        }

        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.set(GregorianCalendar.HOUR_OF_DAY, hour);
        cal.set(GregorianCalendar.MINUTE, 0);
        cal.set(GregorianCalendar.SECOND, 0);
        cal.set(GregorianCalendar.MILLISECOND, 0);
        return cal.getTime();
    }

    /**
     * |------------Dia 1-----------|------------Dia 2-----------|
     * 0         5        22        0         5        22        0
     * |-noturno-|-normal-|------noturno------|-normal-|-noturno-|
     * |        |                   |                            |
     * Fim Noturno        Inicio    -  Fim Noturno     Inicio Noturno
     * <p>
     * Toda a lógica  abaixo é baseada no quadro acima, os instantes do evento são
     * tratados no dia em que ocorrem, por isso a inversão do inicio com o fim do horário
     * noturno.
     * <p>
     * TODO: não está preparado para trabalhar com eventos cujo o período passe
     * por mais de 2 dias (diferença entre I(F)Nd1 e I(F)Nd2 maior que 24h)
     * TODO: analisar o impacto de procedimentos paralelos
     *
     * @param iniEvt Event's initial date
     * @param fimEvt Event's final date
     * @param isPaid If the event is paid
     * @return The event's day and night hours.
     * @see br.com.cmctransportes.cjm.domain.services.NoturnoService.setTime
     */
    public DiurnoNoturno calculaNoturno(Date iniEvt, Date fimEvt, boolean isPaid) {
        DiurnoNoturno result = new DiurnoNoturno();

        if ((iniEvt == null) || (fimEvt == null)) {
            return result;
        }

        Date inicioNoturnoDia01 = setTime(iniEvt, NIGHTLY_INIT_HOUR);
        Date inicioNoturnoDia02 = setTime(fimEvt, NIGHTLY_INIT_HOUR);
        if (inicioNoturnoDia02.compareTo(inicioNoturnoDia01) <= 0) { // se o dia 2 for menor ou igual ao dia 1, ele passa dia 1 + 24h
            inicioNoturnoDia02 = new Date(inicioNoturnoDia01.getTime() + TimeHelper.VINTEQUATRO_HORAS);
        }

        Date fimNoturnoDia01 = setTime(iniEvt, NIGHTLY_FINAL_HOUR);
        Date fimNoturnoDia02 = setTime(fimEvt, NIGHTLY_FINAL_HOUR);
        if (fimNoturnoDia02.compareTo(fimNoturnoDia01) <= 0) { // se o dia 2 for menor ou igual ao dia 1, ele passa dia 1 + 24h
            fimNoturnoDia02 = new Date(fimNoturnoDia01.getTime() + TimeHelper.VINTEQUATRO_HORAS);
        }

        Long noturno = new Long(0);
        Long normal = new Long(0);

        if (iniEvt.compareTo(fimNoturnoDia01) < 0) { // começou antes do fim do noturno do dia 1
            if (fimEvt.compareTo(fimNoturnoDia01) < 0) {  // terminou antes do fim do noturno do dia 1
                noturno = fimEvt.getTime() - iniEvt.getTime(); // só tem noturno
                normal = new Long(0);
            } else { // terminou depois do fim noturno do dia 1
                noturno = fimNoturnoDia01.getTime() - iniEvt.getTime(); // do inicio do evento até o fim do noturno

                Long tmpHE = new Long(0); //calculaHorasExtras(trabalhado, noturno);
                // se tem HE é noturno até o fim...
                if (isPaid && (tmpHE > 0)) {
                    noturno = fimEvt.getTime() - iniEvt.getTime(); // só tem noturno
                    normal = new Long(0);
                } else if (fimEvt.compareTo(inicioNoturnoDia01) <= 0) { // terminou antes do inicio do noturno do dia 1
                    normal = fimEvt.getTime() - fimNoturnoDia01.getTime();  // do fim do noturno até o fim do evento
                } else { // se terminou depois do inicio do noturno do dia 1
                    normal = inicioNoturnoDia01.getTime() - fimNoturnoDia01.getTime();   // do inicio do noturno do dia 1 até o fim do noturno do dia 1
                    if (fimEvt.compareTo(fimNoturnoDia02) < 0) {  // terminou antes do fim do noturno do dia 2
                        noturno += fimEvt.getTime() - inicioNoturnoDia01.getTime();  // do inicio noturno do dia 1 até o fim do evento
                    } else { // terminou depois do fim do dia 02
                        noturno += fimNoturnoDia02.getTime() - inicioNoturnoDia01.getTime();  // do inicio noturno do dia 1 até o fim do noturno do dia 2

                        tmpHE = new Long(0); //calculaHorasExtras(trabalhado, noturno);
                        // se tem HE é noturno até o fim...
                        if (isPaid && (tmpHE > 0)) {
                            noturno = fimEvt.getTime() - iniEvt.getTime(); // só tem noturno
                            normal = new Long(0);
                        } else if (fimEvt.compareTo(inicioNoturnoDia02) < 0) {  // terminou antes do inicio do noturno do dia 2
                            normal += fimEvt.getTime() - fimNoturnoDia02.getTime();   // do inicio do noturno do dia 2 até o fim do evento
                        } else { // terminou depois do inicio do noturno do dia 2
                            normal += inicioNoturnoDia02.getTime() - fimNoturnoDia02.getTime();   // do fim do noturno do dia 2 até o inicio do noturno do dia 2
                            noturno += fimEvt.getTime() - inicioNoturnoDia02.getTime();   // do inicio do noturno do dia 2 até o fim do evento
                        }
                    }
                }
            }
        } else if (iniEvt.compareTo(inicioNoturnoDia01) < 0) {// começou depois do fim noturno do dia 1 e antes do inicio do noturno do dia 1
            if (fimEvt.compareTo(inicioNoturnoDia01) <= 0) { // terminou antes do inicio do noturno do dia 1
                normal = fimEvt.getTime() - iniEvt.getTime(); // não tem noturno
                noturno = new Long(0);
            } else { // terminou depois do inicio do noturno dia 1
                normal = inicioNoturnoDia01.getTime() - iniEvt.getTime(); // do inicio do evento até o inicio do noturno do dia 1
                if (fimEvt.compareTo(fimNoturnoDia02) < 0) {  // terminou antes do fim do noturno do dia 2
                    noturno = fimEvt.getTime() - inicioNoturnoDia01.getTime(); // do inicio do noturno do dia 1 até o fim do evento
                } else { // terminou depois do  fim noturno dia 2
                    noturno = fimNoturnoDia02.getTime() - inicioNoturnoDia01.getTime(); // do inicio do noturno do dia 1 até o fim do noturno dia 2
                    Long tmpHE = new Long(0); // calculaHorasExtras(trabalhado, noturno);
                    // se tem HE é noturno até o fim...
                    if (isPaid && (tmpHE > 0)) {
                        noturno = fimEvt.getTime() - iniEvt.getTime(); // só tem noturno
                        normal = new Long(0);
                    } else if (fimEvt.compareTo(inicioNoturnoDia02) < 0) { // se terminou antes do inicio do noturno do dia 2

                        normal += fimEvt.getTime() - fimNoturnoDia02.getTime(); // do fim do noturno do dia 2 até o fim do evento
                    } else { // terminou depois do inicio do noturno do dia 2
                        normal += inicioNoturnoDia02.getTime() - fimNoturnoDia02.getTime();   // do fim do noturno do dia 2 até o inicio do noturno do dia 2
                        noturno += fimEvt.getTime() - inicioNoturnoDia02.getTime();   // do inicio do noturno do dia 2 até o fim do evento
                    }
                }
            }
            // começou depois do inicio do noturno do dia 1
        } else if (fimEvt.compareTo(fimNoturnoDia02) < 0) {  // terminou antes do fim do noturno do dia 2
            normal = new Long(0); // só tem noturno
            noturno = fimEvt.getTime() - iniEvt.getTime(); // do inicio do evento até o fim do evento
        } else { // terminou depois do fim noturno do dia 2
            noturno = fimNoturnoDia02.getTime() - iniEvt.getTime(); // do inicio do evento até o fim do noturno dia 2
            if (isPaid) {
                noturno = fimEvt.getTime() - iniEvt.getTime(); // só tem noturno
                normal = new Long(0);
            } else if (fimEvt.compareTo(inicioNoturnoDia02) < 0) { // se terminou antes do inicio do noturno do dia 2
                normal = fimEvt.getTime() - fimNoturnoDia02.getTime(); // do fim do noturno do dia 2 até o fim do evento
            } else { // terminou depois do inicio do noturno do dia 2
                normal = inicioNoturnoDia02.getTime() - fimNoturnoDia02.getTime();   // do fim do noturno do dia 2 até o inicio do noturno do dia 2
                noturno += fimEvt.getTime() - inicioNoturnoDia02.getTime();   // do inicio do noturno do dia 2 até o fim do evento
            }
        }

//        System.out.println(String.format("[calculaNoturno] %s to %s - [%s][%s]", iniEvt, fimEvt, normal, noturno));
        result.setTempoDiurno(normal);
        result.setTempoNoturno(noturno);

        return result;
    }
}
