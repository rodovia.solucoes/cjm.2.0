package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.Jornada;
import br.com.cmctransportes.cjm.domain.entities.JourneyManifest;
import br.com.cmctransportes.cjm.domain.entities.vo.JourneyManifestVO;
import br.com.cmctransportes.cjm.infra.dao.JourneyManifestDAO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

/**
 * @author William Leite
 */
@Service
public class JourneyManifestService {

    private final JourneyManifestDAO dao;

    private final EntityManager em;

    public JourneyManifestService(EntityManager em) {
        this.em = em;
        dao = new JourneyManifestDAO(em);
    }

    public JourneyManifest convert(final JourneyManifestVO vo) {
        JourneyManifest entity = new JourneyManifest();
        entity.setEndDate(vo.getEndDate());
        entity.setId(vo.getId());
        entity.setJourney(new Jornada(vo.getJourney()));
        entity.setManifestNumber(vo.getManifest());
        entity.setStartDate(vo.getStartDate());
        return entity;
    }

    public JourneyManifestVO convert(final JourneyManifest entity) {
        JourneyManifestVO vo = new JourneyManifestVO();
        vo.setEndDate(entity.getEndDate());
        vo.setId(entity.getId());
        vo.setJourney(entity.getJourney().getId());
        vo.setManifest(entity.getManifestNumber());
        vo.setStartDate(entity.getStartDate());
        return vo;
    }

    public List<JourneyManifest> findByJourney(Integer journey) {
        List<JourneyManifest> result;
        try {
            result = dao.findByJourney(journey);
        } catch (Exception e) {
            e.printStackTrace();
            result = new ArrayList<>(0);
        }

        return result;
    }

    @Transactional
    public void save(JourneyManifest entity) {
        try {
            dao.save(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<JourneyManifest> findAll() {
        return dao.findAll();
    }

    public JourneyManifest getById(Long id) {
        return dao.getById(id);
    }

    @Transactional
    public void update(JourneyManifest entity) {
        try {
            dao.update(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void delete(Long id) {
        try {
            JourneyManifest tipo = this.getById(id);
            dao.delete(tipo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void delete(JourneyManifest entity) {
        try {
            dao.delete(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
