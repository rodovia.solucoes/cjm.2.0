package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class Transmissao implements Serializable {
    private Integer id;
    private Date dataTransmissao;
    private Integer velocidade;
    private Boolean ignicaoAtiva;
    private Double latitude;
    private Double longitude;
    private String endereco;
    private Double totalOdometro;
    private Integer fuelLevel;
    private Integer rpm;
    private Double voltagem;
    private String cidade;
    private String uf;
}
