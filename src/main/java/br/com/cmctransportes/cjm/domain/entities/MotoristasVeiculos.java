package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author William Leite
 */
@Entity
@Table(name = "motoristas_veiculos")
@Getter
@Setter
public class MotoristasVeiculos implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "MOTORISTAS_VEICULOS_ID_SEQ", sequenceName = "MOTORISTAS_VEICULOS_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MOTORISTAS_VEICULOS_ID_SEQ")
    @Column(name = "id")
    private Integer id;

    @Column(name = "motoristas_id", nullable = false)
    private Integer motoristasId;

    @JoinColumn(name = "veiculo_id", referencedColumnName = "placa")
    @OneToOne(optional = true)
    private Veiculos veiculosId;

    @Column(name = "data_inicio", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataInicio;

    @Column(name = "veiculo_orion")
    private Integer veiculoOrion;

    @Column(name = "data_fim")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataFim;

    @Column(name = "ativo")
    private Boolean ativo;
}