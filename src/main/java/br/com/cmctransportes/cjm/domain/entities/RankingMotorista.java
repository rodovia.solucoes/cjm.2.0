package br.com.cmctransportes.cjm.domain.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "ranking_motorista")
@Getter
@Setter
public class RankingMotorista implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name = "RANKING_MOTORISTA_ID_SEQ", sequenceName = "RANKING_MOTORISTA_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RANKING_MOTORISTA_ID_SEQ")
    @Column(name = "id")
    private Integer id;

    @JoinColumn(name = "tipo_ranking", referencedColumnName = "id")
    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    private TipoRanking tipoRanking;

    @Column(name = "data_cadastro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataCadastro;

    @Column(name = "id_transmissao")
    private Integer idTransmissao;

    @Column(name = "motorista")
    private Integer idMotorista;

    @Transient
    private Motoristas motoristas;

}
