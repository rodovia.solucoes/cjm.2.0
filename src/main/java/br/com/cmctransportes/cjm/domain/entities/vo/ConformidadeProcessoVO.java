package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.Evento;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;
import org.joda.time.Minutes;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Stream;


public class ConformidadeProcessoVO implements Serializable {
    @Getter
    @Setter
    private Evento evento;
    @Getter
    @Setter
    private ConformidadeResumoVO conformidadeResumo;
}
