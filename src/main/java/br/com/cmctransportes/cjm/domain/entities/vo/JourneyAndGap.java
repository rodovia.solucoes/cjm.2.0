package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.Jornada;
import br.com.cmctransportes.cjm.domain.entities.enums.JOURNEY_TYPE;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

/**
 * @author Producao
 */
public class JourneyAndGap {
    @Getter
    @Setter
    private Jornada journey;
    @Getter
    @Setter
    private long gap;

    public JourneyAndGap() {
    }

    public JourneyAndGap(Jornada journey, long gap) {
        this.journey = journey;
        this.gap = gap;
    }

    public void calcAndSetGap(JourneyAndGap anterior) {
        if (Objects.isNull(anterior)) return;

        if (Objects.isNull(journey.getJornadaInicio())) return;
        if (Objects.isNull(anterior.getJourney())) return;
        if (Objects.isNull(anterior.getJourney().getJornadaFim())) return;

        gap = journey.getJornadaInicio().getTime() - anterior.getJourney().getJornadaFim().getTime();

        if (anterior.getJourney().getType() == JOURNEY_TYPE.DSR) {
            gap += anterior.getGap();
        }
    }
}
