package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
@Getter
@Setter
public class EquipPortatilMinimaVO implements Serializable {
    private Integer id;
    private String apelido;
    private String imei;
    private String modelo;
    private String fabricante;
    private Boolean status;
}
