package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.enums.CALENDAR_WEEK_STATUS;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author William Leite
 */
public class CalendarWeekVO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private Integer sequence;
    @Getter
    @Setter
    private CALENDAR_WEEK_STATUS status;
    @Getter
    @Setter
    private List<CalendarDayVO> days;
    @Getter
    @Setter
    private List<NonconformityVO> nonconformities;
    @Getter
    @Setter
    private Date start;
    @Getter
    @Setter
    private Date end;
    @Getter
    @Setter
    private Integer maxEvents;
    @Getter
    @Setter
    private Integer maxTypes;
}
