package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.Transmissao;
import br.com.cmctransportes.cjm.infra.dao.TransmissaoDAO;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class TransmissaoService {

    public List<Transmissao> buscarDadosParaFaturamento(Integer idVeiculo, Date dataInicial, Date dataFinal){
        return new TransmissaoDAO().buscarDadosParaFaturamento(idVeiculo, dataInicial, dataFinal);
    }

    public List<Transmissao> buscarTrasmissaoParaRelatorio(Integer idVeiculo, Date dataIncio, Date dataFim){
        return new TransmissaoDAO().buscarTrasmissaoParaRelatorio(idVeiculo, dataIncio, dataFim);
    }

    public Transmissao buscarTransmissaoBasica(Integer idVeiculo){
        return new TransmissaoDAO().buscarTransmissaoBasica(idVeiculo);
    }

    public List<Transmissao> buscarTrasmissaoParaMonitoramento(Integer idVeiculo){
        return new TransmissaoDAO().buscarTrasmissaoParaMonitoramento(idVeiculo);
    }

    public List<Transmissao> buscarUltimasTransmissaos(Integer idVeiculo){
        return new TransmissaoDAO().buscarUltimasTransmissaos(idVeiculo);
    }

    public List<Transmissao> buscarUltimasTransmissaosIgnicaoAtivaVelociadeMaiorQueZero(Integer idVeiculo){
        return new TransmissaoDAO().buscarUltimasTransmissaosIgnicaoAtivaVelociadeMaiorQueZero(idVeiculo);
    }


}
