package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.*;
import br.com.cmctransportes.cjm.domain.entities.vo.MotoristaVO;
import br.com.cmctransportes.cjm.domain.repository.*;
import br.com.cmctransportes.cjm.infra.dao.EmpresasDAO;
import br.com.cmctransportes.cjm.infra.dao.MotoristasDAO;
import br.com.cmctransportes.cjm.infra.dao.SascarDAO;
import br.com.cmctransportes.cjm.infra.dao.UnidadesDAO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.proxy.UsersOpenFireProxy;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.sql.SQLException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

/**
 * @author analista.ti
 */
@Service
public class DriverService {
// TODO: criar GenericService

    private final MotoristasDAO dao;

    private final SascarDAO sascarDAO;

    private final EntityManager em;

    private final UnidadesDAO unidadesDAO;
    private final EmpresasDAO empresasDAO;

    @Autowired
    private UserService userService;

    @Autowired
    private MotoristasRepository repository;

    @Autowired
    private DriverService driverService;

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private EnderecosRepository enderecosRepository;

    @Autowired
    private ImagemRepository imagemRepository;
    @Autowired
    private SascarService sascarService;
    @Autowired
    private MotoristasVeiculosService motoristasVeiculosService;



    public DriverService(EntityManager em) {
        this.em = em;

        dao = new MotoristasDAO(em);
        unidadesDAO = new UnidadesDAO(em);
        empresasDAO = new EmpresasDAO(em);
        sascarDAO = new SascarDAO(em);
    }

    public void save(Motoristas entity) throws Exception {
        Enderecos tmpEndereco = entity.getEnderecosId();
        try {
            entity.setId(null);

            String userName = getUserNameFromDriver(entity);
            String tmpUserName = userName;

            Users tmpuser = null;

            boolean acabou = false;
            int idx = 0;

            while (!acabou) {
                tmpuser = this.userService.getByUserName(tmpUserName);

                if (tmpuser != null) {
                    idx += 1;
                    tmpUserName = userName + idx;
                } else {
                    acabou = true;
                }
            }

            userName = tmpUserName;

            // cria o usuario
            Users usuario = new Users();
            usuario.setSenha("123456");
            usuario.setUserName(userName);

            usuario.setUsersBranch(new ArrayList<>());

            Unidades unidade = unidadesDAO.getById(entity.getUnidadesId());
            usuario.getUsersBranch().add(new UsersBranch(null, unidade, usuario));

            usuario.setUsersCompany(new ArrayList<>());

            Empresas empresa = empresasDAO.getById(entity.getEmpresaId());
            if (Objects.isNull(empresa)) {
                empresa = unidade.getEmpresaId();
            }
            usuario.getUsersCompany().add(new UsersCompany(null, empresa, usuario));

            // salva usuario
            usuario.setSenhaCripto(RodoviaUtils.cryptWithMD5(usuario.getSenha()));

            usuario = this.usersRepository.saveAndFlush(usuario);

            entity.setUser(usuario.getId());

            preparaImagem(entity);

            if (Objects.nonNull(entity.getEmpresa())) {
                tmpEndereco.setEmpresaId(entity.getEmpresa().getId());
            } else {
                tmpEndereco.setEmpresaId(empresa.getId());
                entity.setEmpresa(empresa);
            }

            if (tmpEndereco.getId() == null) {
                try {
                    tmpEndereco = this.enderecosRepository.saveAndFlush(tmpEndereco);
                } catch (Exception e) {
                    tmpEndereco.setId(null);
                    throw e;
                }
            }

            this.repository.saveAndFlush(entity);
            if(entity.getSascarId() != null && entity.getSascarId() > 0){
                sascarService.gerarDadosSascar(entity);
            }

            UsersOpenFireProxy.getInstance().cadastrarUsuario(usuario.getUserName(), usuario.getSenha());

        } catch (Exception e) {
            e.printStackTrace();
            tmpEndereco.setId(null);
            entity.setId(null);
            throw e;
        }
    }

    public List<Motoristas> findAll() {
        return dao.nqFindAll();
    }

    /**
     * Retorna todos os motoristas que possuem jornadas com inconformidades no
     * periodo.
     *
     * @param dataInicio Data Inicio
     * @param dataFinal  Data Final
     * @return Lista de motoristas
     */
    public List<Motoristas> findNonconformities(Long dataInicio, Long dataFinal, boolean only_nonconformities) {
        List<Motoristas> motoristas = null;
        /*      
        Date inicio = new Date(dataInicio);
        Date fim = new Date(dataFinal);
        
        // Retrieve all journeys containing nonconformities
        RelatorioApuracaoDeJornada rdj = new RelatorioApuracaoDeJornada(inicio, fim);
        List<Jornada> jornadas = jornadaDAO.getRelatorioDeJornada(-1, inicio, fim);
        if (only_nonconformities)
            rdj.setNonconformities(jornadas);
        else
            rdj.setJornadas(jornadas);
        // Retrieve drivers from journeys
        List<Motoristas> motoristas = rdj.getLinhas().stream().map(ApuracaoDeJornada::getJornada)
                                                           .map(Jornada::getMotoristasId)
                                                           .distinct().collect(Collectors.toList());
         */
        return motoristas;
    }

    public Motoristas getById(Integer id) {
        return dao.getById(id);
    }

    @Transactional
    public void update(Motoristas entity) {
        try {
            preparaImagem(entity);
            Enderecos tmpEndereco = entity.getEnderecosId();
            tmpEndereco.setEmpresaId(entity.getEmpresa().getId());
            this.enderecosRepository.saveAndFlush(tmpEndereco);
            sascarDAO.updateSascar(entity);
            dao.update(entity);
            if(entity.getSascarId() != null && entity.getSascarId() > 0){
                sascarService.gerarDadosSascar(entity);
            }
            if(Objects.nonNull(entity.getDataDemissao()) && (Objects.nonNull(entity.getSituacao()) && entity.getSituacao().equals(0))){
                //Desabilita todos os veiculos do motorista.
                motoristasVeiculosService.editarMotorista(entity.getId());
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public void delete(Integer id) {
        try {
            Motoristas entity = getById(id);
            this.delete(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void delete(Motoristas entity) {
        try {
            Imagem tmpId = entity.getImagemId();
            Enderecos tmpAddress = entity.getEnderecosId();

            dao.delete(entity);
            if (Objects.nonNull(tmpId)) {
                this.imagemRepository.delete(tmpId);
            }
            this.enderecosRepository.delete(tmpAddress);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void preparaImagem(Motoristas entity) {

        if (Objects.equals(entity.getId(), null)) {
            //Inclusao
            if (Objects.nonNull(entity.getImagemId())) {
                Imagem imagem = this.imagemRepository.saveAndFlush(entity.getImagemId());
                entity.setImagemId(imagem);
            }
        } else {
            if (Objects.nonNull(entity.getImagemId())) {
                this.imagemRepository.saveAndFlush(entity.getImagemId());
            }
        }

    }

    public Motoristas getByUserId(Integer id) {
        return dao.getByUserId(id);
    }


    public List<Motoristas> findAllIdSascar(Integer idSascar) {
        return dao.findAllIdSascar(idSascar);
    }

    public List<Motoristas> findAll(Integer idEmpresa, Integer idUnidade) {
        if (null != idUnidade && idUnidade > 0) {
            return dao.findAll(idEmpresa, idUnidade);
        } else {
            return dao.findAll(idEmpresa);
        }
    }

    public List<Motoristas> findAllMinimo(Integer idEmpresa, Integer idUnidade) {
        if (null != idUnidade && idUnidade > 0) {
            return dao.findAllMinimo(idEmpresa, idUnidade);
        } else {
            return dao.findAllMinimo(idEmpresa);
        }
    }

    public List<Motoristas> findAllListForView(Integer idEmpresa, Integer idUnidade){
        List<Motoristas> lista = new ArrayList<>();
        if (null != idUnidade && idUnidade > 0) {
            lista.addAll(dao.findAllListForView(idEmpresa, idUnidade));
        } else {
            lista.addAll(dao.findAllListForView(idEmpresa));
        }


        lista.forEach(m->{
            if(Objects.isNull(m.getDataDemissao()) && m.getSituacao().equals(1)){
                m.setIsAtivo(Boolean.TRUE);
            }else{
                m.setIsAtivo(Boolean.FALSE);
            }
        });

        lista.sort((o1, o2) ->o1.getIsAtivo().compareTo(o2.getIsAtivo()));
        //lista.sort(Comparator.comparing(Motoristas::getDataDemissao, Comparator.nullsFirst(Comparator.naturalOrder())));
        return lista;
    }

    public MotoristaVO buscarMotoristaVO(Users users){
        Motoristas motoristas = dao.buscarPeloUserId(users);
        if(motoristas != null){
            return new MotoristaVO(motoristas.getId(), motoristas.getNome(), motoristas.getSobrenome());
        }
        return null;
    }

    private String getUserNameFromDriver(Motoristas entity) {
        String lastName = getLastName(entity.getSobrenome());

        String result = entity.getNome().toLowerCase();

        if (lastName != null) {
            result += "." + lastName.toLowerCase();
        }

        result = Normalizer.normalize(result, Normalizer.Form.NFD);
        result = result.replaceAll("[^\\p{ASCII}]", "");

        return result;
    }

    private String getLastName(String sobrenome) {
        if (sobrenome == null) {
            return null;
        }

        String[] tmp = sobrenome.split(" ");

        return tmp[tmp.length - 1];
    }

    public List<Motoristas> findAllAtivos(Integer idEmpresa, Integer idUnidade){
       return dao.findAllAtivos(idEmpresa, idUnidade);
    }

    public Motoristas findPeloCpf(String cpf){
        return dao.findPeloCpf(cpf);
    }

    public Motoristas buscarPeloNome(String nomeDoMotorista, Integer idEmpresa){
        return dao.buscarPeloNome(nomeDoMotorista, idEmpresa);
    }

    public Motoristas buscarPeloIdIntegracao(Integer idIntegracao){
        List<Motoristas> lista = dao.findAllIdSascar(idIntegracao);
        if(Objects.nonNull(lista) && !lista.isEmpty()){
            return lista.get(0);
        }
        return null;
    }

}

