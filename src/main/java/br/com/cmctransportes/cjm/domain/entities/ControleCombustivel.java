package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "controle_combustivel")
@Getter
@Setter
public class ControleCombustivel implements Serializable {

    @Id
    @SequenceGenerator(name = "controle_combustivel_id_seq", sequenceName = "controle_combustivel_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "controle_combustivel_id_seq")
    @Column(name = "id")
    private Integer id;

    @Column(name = "data_cadastro", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date dataCadastro;

    @Column(name = "id_veiculo_trajetto")
    private Integer idVeiculo;

    @Column(name = "quantidade_abastecida")
    private Double quantidadeAbastecida;

    @Column(name = "valor_dia")
    private Double valorDia;

    /**
     * AL = ALCOOL
     * GA = GASOLINA
     * DI = DIESEL
     */
    @Column(name = "tipo_combustivel")
    private String tipoCombustivel;

    @Column(name = "id_motorista")
    private Integer idMotorista;

}
