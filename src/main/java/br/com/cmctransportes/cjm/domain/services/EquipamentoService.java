package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.Empresas;
import br.com.cmctransportes.cjm.domain.entities.vo.EquipamentoVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoEquipamentoVO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.proxy.EquipamentoTrajetto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EquipamentoService {

    @Autowired
    private EmpresasService empresasService;

    public RetornoEquipamentoVO cadastrarEquipamento(EquipamentoVO equipamentoVO) throws Exception{
        try {
            if(equipamentoVO.getEmpresa() == null){
                return new RetornoEquipamentoVO(-1, "Empresa é obrigatorio.");
            }
            if(equipamentoVO.getEmpresa().getIdClienteTrajetto() == null || equipamentoVO.getEmpresa().getIdClienteTrajetto() == 0){
              Empresas empresas = empresasService.getById(equipamentoVO.getEmpresa().getId());
              equipamentoVO.getEmpresa().setIdClienteTrajetto(empresas.getIdClienteTrajetto());
            }
            return EquipamentoTrajetto.getInstance().cadastrarEquipamento(equipamentoVO);
        }catch (Exception e){
            LogSistema.logError("cadastrarEquipamento", e);
            throw new Exception(e);
        }
    }


    public RetornoEquipamentoVO editarEquipamento(EquipamentoVO equipamentoVO) throws Exception{
        try {
            if(equipamentoVO.getEmpresa() == null){
                return new RetornoEquipamentoVO(-1, "Empresa é obrigatorio.");
            }
            if(equipamentoVO.getEmpresa().getIdClienteTrajetto() == null || equipamentoVO.getEmpresa().getIdClienteTrajetto() == 0){
                Empresas empresas = empresasService.getById(equipamentoVO.getEmpresa().getId());
                equipamentoVO.getEmpresa().setIdClienteTrajetto(empresas.getIdClienteTrajetto());
            }
            return EquipamentoTrajetto.getInstance().editarEquipamento(equipamentoVO);
        }catch (Exception e){
            LogSistema.logError("editarEquipamento", e);
            throw new Exception(e);
        }
    }

    public RetornoEquipamentoVO listarEquipamentoNaoAlocado() throws Exception{
        try {
            return EquipamentoTrajetto.getInstance().listarEquipamentoNaoAlocado();
        }catch (Exception e){
            LogSistema.logError("editarEquipamento", e);
            throw new Exception(e);
        }
    }

    public RetornoEquipamentoVO listarEquipamentos() throws Exception{
        try {
            return EquipamentoTrajetto.getInstance().listarEquipamentos();
        }catch (Exception e){
            LogSistema.logError("listarEquipamentos", e);
            throw new Exception(e);
        }
    }

    public RetornoEquipamentoVO buscarEquipamentoPeloCodigo(Integer idEquipamento) throws Exception{
        try {
            return EquipamentoTrajetto.getInstance().buscarEquipamentoPeloCodigo(idEquipamento);
        }catch (Exception e){
            LogSistema.logError("buscarEquipamentoPeloCodigo", e);
            throw new Exception(e);
        }
    }

    public RetornoEquipamentoVO listarEquipamentoMinimo() throws Exception{
        try {
            return EquipamentoTrajetto.getInstance().listarEquipamentoMinimo();
        }catch (Exception e){
            LogSistema.logError("listarEquipamentoMinimo", e);
            throw new Exception(e);
        }
    }
}
