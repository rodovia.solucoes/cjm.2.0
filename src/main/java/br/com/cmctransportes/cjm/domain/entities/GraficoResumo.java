package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class GraficoResumo implements Serializable {
    private String evento;
    private Double horas;
    private String cor;
}
