package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class AlertaInconformeVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private VeiculoVO veiculo;
    private List<InconformidadeVO> listaDeInconformidades = new ArrayList<>();
}
