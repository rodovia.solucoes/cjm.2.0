package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class RetornoObservacaoVO extends RetornoVO{
    @Getter
    @Setter
    private List<ObservacaoVO> listaDeObservacao;
}
