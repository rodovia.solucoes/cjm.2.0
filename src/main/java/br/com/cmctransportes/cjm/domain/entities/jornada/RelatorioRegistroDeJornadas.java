package br.com.cmctransportes.cjm.domain.entities.jornada;

import br.com.cmctransportes.cjm.domain.entities.Jornada;
import br.com.cmctransportes.cjm.domain.entities.RelatorioDeJornada;
import br.com.cmctransportes.cjm.domain.entities.enums.EVENT_TYPE;
import br.com.cmctransportes.cjm.domain.services.EventosJornadaService;
import br.com.cmctransportes.cjm.services.JourneyCalculusService;
import br.com.cmctransportes.cjm.services.nonconformity.InterjourneyNonconfomityService;
import br.com.cmctransportes.cjm.services.nonconformity.PaidWeeklyRestNonconformityService;
import br.com.cmctransportes.cjm.utils.TimeHelper;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author analista.ti
 */
public class RelatorioRegistroDeJornadas extends RelatorioDeJornada {

    private ArrayList<RegistrosDeJornada> registroJornada;

    public RelatorioRegistroDeJornadas(Integer mesReferencia) {
        super(mesReferencia);
    }

    public RelatorioRegistroDeJornadas(Date inicio, Date fim) {
        super(inicio, fim);
    }

    public RelatorioRegistroDeJornadas(Integer mesReferencia, List<Jornada> listaJornadas, JourneyCalculusService journeyCalculusService,
                                       PaidWeeklyRestNonconformityService paidWeeklyRestNonconformityService,
                                       InterjourneyNonconfomityService interjourneyNonconfomityService,
                                       EventosJornadaService eventosJornadaService) {
        super(mesReferencia, listaJornadas, journeyCalculusService, paidWeeklyRestNonconformityService, interjourneyNonconfomityService, eventosJornadaService);
    }

    public void setJornadas(List<Jornada> relatorioDeJornada, JourneyCalculusService journeyCalculusService,
                            PaidWeeklyRestNonconformityService paidWeeklyRestNonconformityService,
                            InterjourneyNonconfomityService interjourneyNonconfomityService,
                            EventosJornadaService eventosJornadaService) {
        super.setJornadas(relatorioDeJornada, journeyCalculusService, paidWeeklyRestNonconformityService, interjourneyNonconfomityService, eventosJornadaService);

        registroJornada = new ArrayList<RegistrosDeJornada>();

        relatorioDeJornada.stream()
                .forEach(j -> journeyCalculusService.processEvents(j, null,false));

        relatorioDeJornada = relatorioDeJornada.stream().sorted(Comparator.nullsLast(
                Comparator.comparing(Jornada::getJornadaInicio)
        )).collect(Collectors.toList());

        for (Jornada jnd : relatorioDeJornada) {
            RegistrosDeJornada adj = new RegistrosDeJornada();
            adj.processa(jnd, eventosJornadaService);
            registroJornada.add(adj);

            if (Objects.equals(adj.getTipoJornada(), EVENT_TYPE.DSR.getDescription())) {
                List<Date> dates = TimeHelper.getDatesBetween(jnd.getJornadaInicio(), jnd.getJornadaFim());
                dates.remove(adj.getData());

                dates.stream().forEach(d -> {
                    RegistrosDeJornada clone = adj.clone();
                    clone.setData(d);
                    registroJornada.add(clone);
                });
            }
        }
    }

    public ArrayList<RegistrosDeJornada> getLinhas() {
        return registroJornada;
    }
}
