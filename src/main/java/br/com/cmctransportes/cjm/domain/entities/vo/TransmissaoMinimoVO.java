package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class TransmissaoMinimoVO implements Serializable {
    private double latitude;
    private double longitude;
    private boolean ignicaoAtiva;
    private boolean maisDe24HorasInativo;
    private String dataTransmissao;
    private String ibutton;
    private String velocidade;
    private String endereco;
    private String bairro;
    private String cidade;
    private String uf;
    private String observacao;
    private String pontoProximo;
}
