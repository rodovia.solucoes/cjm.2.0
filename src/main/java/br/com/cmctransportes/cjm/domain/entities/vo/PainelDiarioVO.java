package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * @author William Leite
 */
public class PainelDiarioVO implements Serializable {

    @Getter
    @Setter
    private String motorista;
    @Getter
    @Setter
    private Integer jornada;
    @Getter
    @Setter
    private List<PainelDiarioItemVO> itens;
}
