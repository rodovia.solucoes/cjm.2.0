/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cmctransportes.cjm.domain.entities;

import br.com.cmctransportes.cjm.domain.entities.enums.LOCATION_STATUS;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author analista.ti
 */
@Entity
@Table(name = "pontos_carga_descarga")
@Getter
@Setter
@NamedQueries({
        @NamedQuery(name = "PontosCargaDescarga.findAll", query = "SELECT p FROM PontosCargaDescarga p"),
        @NamedQuery(name = "PontosCargaDescarga.findById", query = "SELECT p FROM PontosCargaDescarga p WHERE p.id = :id"),
        @NamedQuery(name = "PontosCargaDescarga.findByTelefone", query = "SELECT p FROM PontosCargaDescarga p WHERE p.telefone = :telefone"),
        @NamedQuery(name = "PontosCargaDescarga.findByPessoaResponsavel", query = "SELECT p FROM PontosCargaDescarga p WHERE p.pessoaResponsavel = :pessoaResponsavel")})
public class PontosCargaDescarga implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "PONTOS_CARGA_DESCARGA_ID_SEQ", sequenceName = "PONTOS_CARGA_DESCARGA_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PONTOS_CARGA_DESCARGA_ID_SEQ")
    @Column(name = "id")
    private Integer id;

    @Column(name = "telefone", length = 15)
    private String telefone;

    @Column(name = "pessoa_responsavel", length = 45)
    private String pessoaResponsavel;

    @JoinColumn(name = "empresa_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Empresas empresaId;

    @JoinColumn(name = "pontos_de_espera_id", referencedColumnName = "id")
    @OneToOne(optional = false)
    private PontosDeEspera pontosDeEsperaId;

    @Column(name = "longitude", nullable = true)
    private Double longitude;

    @Column(name = "latitude", nullable = true)
    private Double latitude;

    @Column(name = "raio_localidade")
    private Integer raioLocalidade;

    @Enumerated
    @Column(name = "status")
    private LOCATION_STATUS status;

    public PontosCargaDescarga() {
    }
}