package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.Motivo;
import br.com.cmctransportes.cjm.infra.dao.MotivoDAO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * @author analista.ti
 */
@Service
public class MotivoService {
    // TODO: criar GenericService
    private MotivoDAO dao;

    private final EntityManager em;

    public MotivoService(EntityManager em) {
        this.em = em;
        dao = new MotivoDAO(em);
    }

    @Transactional
    public void save(Motivo entity) {
        try {
            dao.save(entity);
        } catch (Exception e) {
            e.printStackTrace();
            entity.setId(null);
            throw e;
        }
    }

    public List<Motivo> findAll() {
        return dao.findAll();
//        return dao.nqFindAll();
    }

    public List<Motivo> findAll(Integer idEmpresa) {
        return dao.findAll(idEmpresa);
    }

    public Motivo getById(Integer id) {
        return dao.getById(id);
    }

    @Transactional
    public void update(Motivo entity) {
        try {
            dao.update(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void delete(Integer id) {
        try {
            Motivo tipo = getById(id);
            dao.delete(tipo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void delete(Motivo entity) {
        try {
            dao.delete(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}