package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class TempoCarregamentoGraficoVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String local;
    private List<TempoCarregamentoVO> listaTempoCarregamento;
}
