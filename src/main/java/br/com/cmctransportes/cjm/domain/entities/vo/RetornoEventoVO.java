package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.Evento;
import br.com.cmctransportes.cjm.domain.entities.EventosJornada;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RetornoEventoVO extends RetornoVO {
    private Evento evento;
    private List<Evento> listaDeEventos;
    private List<EventosJornada> listaDeEventosJornada;
    private boolean viamAtivo;
}
