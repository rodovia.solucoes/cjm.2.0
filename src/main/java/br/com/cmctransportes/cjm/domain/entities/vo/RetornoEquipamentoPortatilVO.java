package br.com.cmctransportes.cjm.domain.entities.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class RetornoEquipamentoPortatilVO extends RetornoVO{
    private EquipamentoPortatilVO equipamentoPortatil;
    private List<EquipamentoPortatilVO> listaEquipamentoPortatil;
}
