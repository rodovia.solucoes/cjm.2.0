package br.com.cmctransportes.cjm.domain.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "manifesto")
public class Manifesto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "manifesto_id_seq", sequenceName = "manifesto_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "manifesto_id_seq")
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Column(name = "motorista")
    private Integer motorista;

    @Column(name = "veiculo")
    private Integer veiculo;

    @Column(name = "valor")
    private Double valor;

    @Column(name = "data")
    @Temporal(TemporalType.TIMESTAMP)
    private Date data;

    @Column(name = "data_registro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataRegistro;

    @Column(name = "numero_manifesto")
    private Integer numeroManifesto;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMotorista() {
        return motorista;
    }

    public void setMotorista(Integer motorista) {
        this.motorista = motorista;
    }

    public Integer getVeiculo() {
        return veiculo;
    }

    public void setVeiculo(Integer veiculo) {
        this.veiculo = veiculo;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Date getDataRegistro() {
        return dataRegistro;
    }

    public void setDataRegistro(Date dataRegistro) {
        this.dataRegistro = dataRegistro;
    }

    public Integer getNumeroManifesto() {
        return numeroManifesto;
    }

    public void setNumeroManifesto(Integer numeroManifesto) {
        this.numeroManifesto = numeroManifesto;
    }
}
