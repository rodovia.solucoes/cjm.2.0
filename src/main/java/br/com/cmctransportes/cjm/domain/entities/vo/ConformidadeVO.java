package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.Evento;
import br.com.cmctransportes.cjm.domain.entities.Motoristas;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class ConformidadeVO implements Serializable {
    private boolean isComforme;
    private Motoristas motoristas;
    private List<Evento> listaDeEventos;
    private List<ConformidadeProcessoVO> conformidadeProcesso = new ArrayList<>();

}
