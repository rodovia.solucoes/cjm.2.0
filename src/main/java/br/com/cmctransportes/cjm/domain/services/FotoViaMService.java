package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.FotoViaM;
import br.com.cmctransportes.cjm.domain.entities.Motoristas;
import br.com.cmctransportes.cjm.domain.entities.vo.FotoViaMVO;
import br.com.cmctransportes.cjm.infra.dao.FotoViaMDAO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.sascarsoap.Motorista;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

@Service
public class FotoViaMService {

    private FotoViaMDAO fotoViaMDAO;

    private final EntityManager em;

    public FotoViaMService(EntityManager em) {
        this.em = em;
        fotoViaMDAO = new FotoViaMDAO(em);
    }


    @Transactional
    public void save(FotoViaMVO entity) {
        try {
            FotoViaM fotoViaM = new FotoViaM();
            fotoViaM.setDataRegistro(new Date());
            fotoViaM.setDataDaFoto(new Date(entity.getDataDaFoto()));
            fotoViaM.setDescricao(entity.getDescricao());
            fotoViaM.setFoto(entity.getFoto());
            fotoViaM.setLatitude(entity.getLatitude());
            fotoViaM.setLongitude(entity.getLongitude());
            fotoViaM.setMotoristas(new Motoristas(entity.getIdMotorista()));
            fotoViaM.setVisualizadaNodashboard(Boolean.FALSE);
            fotoViaMDAO.save(fotoViaM);
        } catch (Exception e) {
            LogSistema.logError("save", e);
        }
    }


    public List<FotoViaM> buscarFotosNaoVistas(Motoristas motoristas){
        return fotoViaMDAO.buscarFotosNaoVistas(motoristas);
    }

    @Transactional
    public void passarFotoParaVista(Integer idFoto){
        try {
            FotoViaM fotoViaM = fotoViaMDAO.getById(idFoto);
            fotoViaM.setVisualizadaNodashboard(Boolean.TRUE);
            fotoViaMDAO.update(fotoViaM);
        }catch (Exception e){
            LogSistema.logError("passarFotoParaVista", e);
        }
    }

}
