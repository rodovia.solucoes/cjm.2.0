package br.com.cmctransportes.cjm.domain.repository;

import br.com.cmctransportes.cjm.domain.entities.Dsr;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DsrRepository extends JpaRepository<Dsr, Integer> {
}
