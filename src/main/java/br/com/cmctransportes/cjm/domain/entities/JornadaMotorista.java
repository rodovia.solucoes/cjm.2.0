/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cmctransportes.cjm.domain.entities;

import br.com.cmctransportes.cjm.domain.entities.vo.EventoPeriodo;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Israel Souza
 */
@Getter
@Setter
public class JornadaMotorista implements Serializable {

    private String nomeMotorista;
    private String nomeEmpresa;
    private String nomeUnidade;
    private Map<Date, List<EventoPeriodo>> resumo;
}
