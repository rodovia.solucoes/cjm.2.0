package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.Tolerancias;
import br.com.cmctransportes.cjm.infra.dao.ToleranciasDAO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * @author analista.ti
 */
@Service
public class ToleranciasService {
    // TODO: criar GenericService
    private ToleranciasDAO dao;

    private final EntityManager em;

    public ToleranciasService(EntityManager em) {
        this.em = em;
        dao = new ToleranciasDAO(em);
    }

    @Transactional
    public void save(Tolerancias entity) {
        try {
            dao.save(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // public T getById(PK pk) {
    public List<Tolerancias> findAll() {
        // return dao.findAll();
        return dao.nqFindAll();
    }

    public Tolerancias getById(Integer id) {
        return dao.getById(id);
    }

    @Transactional
    public void update(Tolerancias entity) {
        try {
            dao.update(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void delete(Integer id) {
        try {
            Tolerancias tipo = getById(id);
            dao.delete(tipo);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Transactional
    public void delete(Tolerancias entity) {
        try {
            dao.delete(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Tolerancias> findAll(Integer idEmpresa) {
        return dao.findAll(idEmpresa);
    }
}