package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class FotoViaMVO implements Serializable {
    private static final long serialVersionUID = 1L;
    private String foto;
    private Boolean enviada;
    private String data;
    private String descricao;
    private Double latitude;
    private Double longitude;
    private Long dataDaFoto;
    private Integer idMotorista;
}
