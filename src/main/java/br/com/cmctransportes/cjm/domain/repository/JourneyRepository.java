package br.com.cmctransportes.cjm.domain.repository;

import br.com.cmctransportes.cjm.domain.entities.Jornada;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JourneyRepository extends JpaRepository<Jornada, Integer> {
}
