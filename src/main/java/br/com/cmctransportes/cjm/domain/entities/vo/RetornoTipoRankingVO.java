package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.TipoRanking;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RetornoTipoRankingVO extends RetornoVO {

    @Getter
    @Setter
    private TipoRanking tipoRanking;
    @Getter
    @Setter
    private List<TipoRanking> listaDeTipoRanking;

}
