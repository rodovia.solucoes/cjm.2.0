package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "contratos")
@Getter
@Setter
public class Contratos implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "contratos_id_seq", sequenceName = "contratos_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "contratos_id_seq")
    @Column(name = "id")
    private Integer id;


    @Column(name = "data_cadastro", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataCadastro;

    /*
    * 1 - Faturando
    * 2 - Implantando
    * 3 - Inadimplente
    * 4 - Desenvolvendo
    * 5 - Nova licença
    * 6 - Treinamento
    * 8 - Em Teste
    */
    @Column(name = "item")
    private Integer item;

    @Column(name = "tarefa")
    private String tarefa;

    @Column(name = "responsavel")
    private String responsavel;

    @Column(name = "expectativa_inicio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expectativaInicio;

    @Column(name = "data_inicio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataInicio;

    @Column(name = "dias")
    private Integer dias;


    @Column(name = "data_final")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataFinal;

    @Column(name = " expectativa_fim")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expectativaFim;

    @Column(name = "produto")
    private String produto;

    @Column(name = "quantidade")
    private Integer quantidade;

    @Column(name = "observacao")
    private String observacao;

    /**
     * Em Andamento
     * Atrasado
     * Concluido
     */
    @Column(name = "status")
    private String status;

    @Column(name = "empresa")
    private Integer empresa;

    @Column(name = "tipo_sistema")
    private String tipoSistema;

    @Column(name = "revenda")
    private String revenda;

    @Column(name = "valor_implatacao")
    private Float valorImplatacao;

    @Column(name = "valor_unidade")
    private Float valorUnidade;

    @Transient
    private Float valorContrato;

    @Transient
    private Integer totalFaturando;

    @Transient
    private Integer totalImplantando;

    @Transient
    private Integer totalInadimplente;

    @Transient
    private Integer totalDesenvolvendo;

    @Transient
    private Integer totalNovaLicenca;

    @Transient
    private Integer totalTreinamento;

    @Transient
    private Integer totalCancelado;

    @Transient
    private String nomeEmpresa;

    @Transient
    private String cor;

    @Transient
    private List<Arquivo> listaDeArquivos;

    @Transient
    private Integer implantandoAtrasado = 0;
    @Transient
    private Integer inadimplenteAtrado = 0;
    @Transient
    private Integer desenvolvendoAtrasado = 0;
    @Transient
    private Integer novalicencaAtrasado = 0;
    @Transient
    private Integer treinamentoAtrasado = 0;
    @Transient
    private Integer canceladoAtrasado = 0;
    @Transient
    private Integer teste;
    @Transient
    private Integer testeAtrasado;

}
