package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.*;
import br.com.cmctransportes.cjm.domain.entities.vo.*;
import br.com.cmctransportes.cjm.infra.dao.*;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.proxy.DashboardTrajetto;
import br.com.cmctransportes.cjm.proxy.TransmissaoTrajetto;
import br.com.cmctransportes.cjm.proxy.modelo.Rota;
import br.com.cmctransportes.cjm.utils.LocationUtils;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class DashboardService {
    @Autowired
    private EmpresasService empresasService;
    @Autowired
    private RastroService rastroService;
    @Autowired
    private EventoService eventoService;
    @Autowired
    private ObservacaoService observacaoService;
    @Autowired
    private PontosCargaDescargaService pdcService;
    @Autowired
    private DriverService ms;
    @Autowired
    private MotoristasVeiculosService motoristasVeiculosService;
    @Autowired
    private RotaService rotaService;
    @Getter
    private DashboardDAO dashboardDAO;
    private FotoViaMDAO fotoViaMDAO;
    private TransmissaoDAO transmissaoDAO;
    private EventoVeiculoDAO eventoVeiculoDAO;
    private final EntityManager em;

    public DashboardService(EntityManager em) {
        this.em = em;
        dashboardDAO = new DashboardDAO(em);
        fotoViaMDAO = new FotoViaMDAO(em);
        transmissaoDAO = new TransmissaoDAO();
        eventoVeiculoDAO = new EventoVeiculoDAO();
    }


    public RetornoMapaVO buscarListaDeAparelhosEVeiculos(EmpresaVO empresaVO, Integer idUnidade) throws Exception {
        try {
            if (empresaVO == null) {
                return new RetornoMapaVO(-1, "Empresa é obrigatorio.");
            }
            if (empresaVO.getIdClienteTrajetto() == null || empresaVO.getIdClienteTrajetto() == 0) {
                Empresas empresas = empresasService.getById(empresaVO.getId());
                empresaVO.setIdClienteTrajetto(empresas.getIdClienteTrajetto());
            }
            RetornoMapaVO retornoMapaVO = DashboardTrajetto.getInstance().buscarListaDeVeiculoPorEmpresa(empresaVO, idUnidade);
            for (VeiculoTransmissaoVO vo : retornoMapaVO.getListaVeiculoTransmissao()) {
                Integer idVeiculo = vo.getVeiculo().getId();
                String motorista = motoristasVeiculosService.buscarNomeMotoristaUltimoVeiculo(idVeiculo);
                if (!motorista.isEmpty()) {
                    MotoristaMinimoVO motoristaVO = new MotoristaMinimoVO();
                    motoristaVO.setNome(motorista);
                    vo.setMotorista(motoristaVO);
                }
            }

            return retornoMapaVO;
        } catch (Exception e) {
            LogSistema.logError("buscarListaDeVeiculoPorEmpresa", e);
            throw e;
        }
    }


    public RetornoDashboardVO buscarDadosDoVeiculo(VeiculoVO veiculoVO) throws Exception {
        try {
            RetornoDashboardVO retornoDashboardVO = DashboardTrajetto.getInstance().buscarDadosDoVeiculo(veiculoVO);
            InformacaoRelatorioVO informacaoRelatorioVO = new EventoVeiculoDAO().buscarEventosDoDia(veiculoVO.getId());
            if (informacaoRelatorioVO != null && !informacaoRelatorioVO.getListaInformacaoRelatorio().isEmpty()) {
                retornoDashboardVO.getDetalheDashboard().setListaInformacaoRelatorioVO(informacaoRelatorioVO.getListaInformacaoRelatorio());
            }
            List<RotaVO> listaRotas = buscarListaDeRotas(veiculoVO.getId());
            retornoDashboardVO.setListaDeRota(listaRotas);
            TelemetriaVO telemetria = processarTelemetria(veiculoVO.getId());
            retornoDashboardVO.setTelemetria(telemetria);
            List<TemperaturaVO> listaTemperatura = buscarListaDeTemperatura(veiculoVO.getId());
            retornoDashboardVO.setListaDeTemperatura(listaTemperatura);
            List<PortasDigitaisVO> listaPortasDigitais = buscarListaDePortas(veiculoVO.getId());
            retornoDashboardVO.setListaPortasDigitais(listaPortasDigitais);
            return retornoDashboardVO;
        } catch (Exception e) {
            throw e;
        }
    }


    private TelemetriaVO processarTelemetria(Integer idVeiculo) {
        TelemetriaVO telemetriaVO = new TelemetriaVO();
        try {


            List<Telemetria> listaTelemetria = new TelemetriaDAO().buscarUltimaTelemetria(idVeiculo);
            List<Transmissao> listaTransmissao = new TransmissaoDAO().buscarUltimasTransmissaos(idVeiculo);
            Veiculos veiculos = new VeiculosDAO(null).buscarVeiculoPeloId(idVeiculo);


            Optional<Telemetria> s = IntStream.range(0, listaTelemetria.size()).mapToObj(i -> listaTelemetria.get(listaTelemetria.size() - i - 1))
                    .filter(f-> Objects.nonNull(f.getEconomiaCombustivelInstantanea()))
                    .findFirst();
            if(s.isPresent()){
                telemetriaVO.setConsumoAtual(s.get().getEconomiaCombustivelInstantanea().concat(" km/l"));
            }else {
                telemetriaVO.setConsumoAtual("Não coletado");
            }


            Optional<Telemetria> s2 = IntStream.range(0, listaTelemetria.size()).mapToObj(i -> listaTelemetria.get(listaTelemetria.size() - i - 1))
                    .filter(f-> Objects.nonNull(f.getEconomiaCombustivelInstantanea()))
                    .findFirst();
            if(s2.isPresent()){
                telemetriaVO.setConsumoInstantaneo(s2.get().getTaxaDeCombustivel().concat(" l/h"));
            }else {
                telemetriaVO.setConsumoInstantaneo("Não coletado");
            }


            String combustivelUsadoInicio = listaTelemetria.get(0).getCombustivelTotalUsadoPeloMotor();
            String combustivelUsadoFim = listaTelemetria.get(listaTelemetria.size() - 1).getCombustivelTotalUsadoPeloMotor();

            if ((Objects.nonNull(combustivelUsadoInicio) && !combustivelUsadoInicio.isEmpty()) && (Objects.nonNull(combustivelUsadoFim) && !combustivelUsadoFim.isEmpty())) {
                Integer vInicio = Integer.parseInt(combustivelUsadoInicio);
                Integer vFim = Integer.parseInt(combustivelUsadoFim);
                Integer total = vFim - vInicio;
                telemetriaVO.setConsumoTotal(String.valueOf(total).concat(" l"));
            } else {
                telemetriaVO.setConsumoTotal("Não coletado");
            }

            //Buscar todas com ignicao ativa
            try {
                List<Transmissao> listaTIgnicaoAtiva = listaTransmissao.stream().filter(t -> t.getIgnicaoAtiva()).collect(Collectors.toList());
                IntSummaryStatistics status = listaTransmissao.stream().mapToInt((x) -> x.getVelocidade()).summaryStatistics();
                telemetriaVO.setVelocidadeMaxima(String.valueOf(status.getMax()).concat(" km/h"));
                telemetriaVO.setVelocidadeAtual(String.valueOf(listaTIgnicaoAtiva.get(listaTIgnicaoAtiva.size() - 1).getVelocidade()).concat(" km/h"));
                telemetriaVO.setVelocidadeMedia(String.valueOf((int) status.getAverage()).concat(" km/h"));
            }catch (Exception e){

            }

            Integer rpm = null;
            Optional<Telemetria> lRpm = listaTelemetria.stream().filter(i -> Objects.nonNull(i.getRpm())).reduce((a, b) -> b);
            if (lRpm.isPresent()) {
                Telemetria t = lRpm.get();
                rpm = t.getRpm();
            }

            if (Objects.isNull(rpm)) {
                telemetriaVO.setRpmExcesso("");
                telemetriaVO.setRotacaoMotorExcesso("-");
                telemetriaVO.setRpmFaixaVerde("");
                telemetriaVO.setRotacaoMotorFaixaVerde("-");
                telemetriaVO.setRpmBaixaRotacao("");
                telemetriaVO.setRotacaoMotorBaixaRotacao("-");
            } else {
                if (Objects.nonNull(veiculos.getRpmMaximo())) {
                    telemetriaVO.setRpmExcesso(String.valueOf(veiculos.getRpmMaximo()));
                    if (rpm > veiculos.getRpmMaximo()) {
                        telemetriaVO.setRotacaoMotorExcesso(String.valueOf(veiculos.getRpmMaximo()));
                    }
                } else {
                    telemetriaVO.setRpmExcesso("");
                    telemetriaVO.setRotacaoMotorExcesso("-");
                }

                if (Objects.nonNull(veiculos.getRpmInicioFaixaVerde()) && Objects.nonNull(veiculos.getRpmFimFaixaVerde())) {
                    telemetriaVO.setRpmFaixaVerde(String.valueOf(veiculos.getRpmFimFaixaVerde()));
                    if (rpm >= veiculos.getRpmInicioFaixaVerde() && rpm <= veiculos.getRpmFimFaixaVerde()) {
                        telemetriaVO.setRotacaoMotorFaixaVerde(String.valueOf(rpm));
                    }
                } else {
                    telemetriaVO.setRpmFaixaVerde("");
                    telemetriaVO.setRotacaoMotorFaixaVerde("-");
                }


                if (Objects.nonNull(veiculos.getRpmInicioFaixaEconomica()) && Objects.nonNull(veiculos.getRpmFimFaixaEconomica())) {
                    telemetriaVO.setRpmBaixaRotacao(String.valueOf(veiculos.getRpmFimFaixaEconomica()));
                    if (rpm >= veiculos.getRpmInicioFaixaEconomica() && rpm <= veiculos.getRpmFimFaixaEconomica()) {
                        telemetriaVO.setRotacaoMotorBaixaRotacao(String.valueOf(rpm));
                    }
                } else {
                    telemetriaVO.setRpmBaixaRotacao("");
                    telemetriaVO.setRotacaoMotorBaixaRotacao("-");
                }

            }

            if (Objects.nonNull(listaTelemetria.get(0).getAcelaracaoBrusca())) {
                telemetriaVO.setArrancadaBrusca(listaTelemetria.get(0).getAcelaracaoBrusca() ? "Sim" : "Não");
            } else {
                telemetriaVO.setArrancadaBrusca("Não");
            }


            if (Objects.nonNull(listaTelemetria.get(0).getFreadaBrusca())) {
                telemetriaVO.setFrenagemBrusca(listaTelemetria.get(0).getFreadaBrusca() ? "Sim" : "Não");
            } else {
                telemetriaVO.setFrenagemBrusca("Não");
            }


            if (Objects.nonNull(listaTelemetria.get(0).getCurvaBrusca())) {
                telemetriaVO.setCurvaBrusca(listaTelemetria.get(0).getCurvaBrusca() ? "Sim" : "Não");
            } else {
                telemetriaVO.setCurvaBrusca("Não");
            }


            Optional<Telemetria> ss = IntStream.range(0, listaTelemetria.size()).mapToObj(i -> listaTelemetria.get(listaTelemetria.size() - i - 1))
                    .filter(f-> Objects.nonNull(f.getTemperaturaMotor()))
                    .findFirst();
            if(s2.isPresent()){
                telemetriaVO.setTemperaturaDoMotor(s2.get().getTemperaturaMotor().concat(" C°"));
            }else {
                telemetriaVO.setTemperaturaDoMotor("Não coletado");
            }

        } catch (Exception e) {

        }
        return telemetriaVO;
    }


    public RetornoDashboardVO buscarDadosDoEquipamentoPortatil(EquipamentoPortatilVO equipamentoPortatilVO) throws Exception {
        try {
            return DashboardTrajetto.getInstance().buscarDadosDoEquipamentoPortatil(equipamentoPortatilVO);
        } catch (Exception e) {
            throw e;
        }
    }

    public DetalheDashboardVO buscarDadosDoViam(MotoristaVO motoristaVO) throws Exception {
        try {

            DetalheDashboardVO detalheDashboardVO = eventoService.buscarUltimoEvento(motoristaVO);
            List<Observacao> listaObservacao = observacaoService.listarObservacaoViam(motoristaVO);
            if (listaObservacao != null && !listaObservacao.isEmpty()) {
                List<ObservacaoVO> l = new ArrayList<>();
                listaObservacao.forEach(obs -> {
                    ObservacaoVO observacaoVO = new ObservacaoVO();
                    observacaoVO.setTexto(obs.getTexto());
                    observacaoVO.setDataFormatada(new SimpleDateFormat("dd/MM HH:mm").format(obs.getDataDoCadastro()));
                    l.add(observacaoVO);
                });
                detalheDashboardVO.setListaObservacao(l);
            }
            //Buscar dados das fotos caso haja
            int quantidadeDeFotos = fotoViaMDAO.contarFotosNaoVistas(new Motoristas(motoristaVO.getId()));
            detalheDashboardVO.setQuantidadeDeFotoViaM(quantidadeDeFotos);
            return detalheDashboardVO;
        } catch (Exception e) {
            throw e;
        }
    }


    public List<TimelineVO> buscarUltimosEventos(Integer idVeiculo) {
        List<TimelineVO> lista = transmissaoDAO.buscarTrasmissaoDoDiaTimeLine(idVeiculo);
        List<TimelineVO> listaEventos = eventoVeiculoDAO.buscarEventoParaTimeline(idVeiculo);
        if (!listaEventos.isEmpty()) {
            lista.addAll(listaEventos);
            Collections.sort(lista, new Comparator<TimelineVO>() {
                public int compare(TimelineVO o1, TimelineVO o2) {
                    return o1.getDataDoEvento().compareTo(o2.getDataDoEvento());
                }
            });
        }
        return lista;

    }

    public DetalheDashboardVO buscarUltimosEventos(MotoristaVO motoristaVO) throws Exception {
        try {

            DetalheDashboardVO detalheDashboardVO = eventoService.buscarUltimosEventosDoDia(motoristaVO);
            Motoristas motoristas = ms.getById(motoristaVO.getId());
            List<LocalVO> lista = pdcService.findAllAsLocal(motoristas.getEmpresaId());
            Set<LocalVO> locations = new HashSet<>();
            List<LocalVO> listaLocalSemRaio = new ArrayList<>();

            for (LocalVO localVO : lista) {
                List<CoordenadaVO> ll = localVO.getListaDeCoordenadas().stream().filter(c -> c.getRaio() != null && c.getIsRaio()).collect(Collectors.toList());
                if (ll != null && !ll.isEmpty()) {
                    locations.add(localVO);
                } else {
                    listaLocalSemRaio.add(localVO);
                }
            }

            final LocationUtils util = new LocationUtils();
            detalheDashboardVO.getListaEvento().forEach(eventoVO -> {
                if (!locations.isEmpty()) {
                    LocalVO local = new LocalVO(0, "", eventoVO.getLatitude(), eventoVO.getLongitude(), false);
                    Optional<LocalVO> closest = util.closest(local, locations);
                    if (closest.isPresent()) {
                        LocalVO localVO = closest.get();
                        CoordenadaVO coordenadaVO = localVO.getListaDeCoordenadas().get(0);
                        if (util.closeEnough(eventoVO.getLatitude(), coordenadaVO.getLatitude(), eventoVO.getLongitude(), coordenadaVO.getLongitude(), coordenadaVO.getRaio())) {
                            eventoVO.setDescricao(eventoVO.getDescricao() + "-" + localVO.getNome());
                        }
                    }
                }

                if (!listaLocalSemRaio.isEmpty()) {
                    for (LocalVO localVO : listaLocalSemRaio) {
                        LatLng[] latLngs = localVO.convert();
                        if (eventoVO.getLatitude() != null && eventoVO.getLongitude() != null) {
                            if (util.verificarSeOPontoEstaNaRegiao(eventoVO.getLatitude(), eventoVO.getLongitude(), latLngs)) {
                                eventoVO.setDescricao(eventoVO.getDescricao() + "-" + localVO.getNome());
                                break;
                            }
                        }
                    }
                }
            });

            return detalheDashboardVO;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional
    public void gravarDashboard(Dashboard dashboard) throws Exception {
        dashboardDAO.save(dashboard);
    }

    public void gravarRotaVeiculo(RotaVeiculo rotaVeiculo) {
        rotaService.salvarRotaVeiculo(rotaVeiculo);
    }

    @Transactional
    public Integer buscarIdJornada(Integer idEmpresa, Integer idUnidade, Integer idUsuario) throws Exception {
        Dashboard dashboard = dashboardDAO.buscarDashboard(idEmpresa, idUnidade, idUsuario);
        if (dashboard != null) {
            Integer idJornada = dashboard.getIdJornada();
            if (idJornada != null && idJornada > 0) {
                dashboardDAO.delete(dashboard);
                return idJornada;
            }
        }
        return 0;
    }

    @Transactional
    public Boolean mostrarTodosNaTela(Integer idEmpresa, Integer idUnidade, Integer idUsuario) throws Exception {
        Dashboard dashboard = dashboardDAO.buscarDashboard(idEmpresa, idUnidade, idUsuario);
        if (dashboard != null) {
            Boolean mostrar = dashboard.isMostrarTodosNoMapa();
            if (mostrar != null && mostrar) {
                dashboardDAO.delete(dashboard);
                return mostrar;
            }
        }
        return Boolean.FALSE;
    }


    public RetornoTransmissaoVO buscarTrajettoUltimas24horas(Integer idVeiculo) throws Exception {
        return TransmissaoTrajetto.getInstance().buscarTrajettoUltimas24horas(idVeiculo);
    }


    public RetornoTransmissaoVO buscarTrajetto(Integer idVeiculo, long dataInicial, long dataFinal) {
        return TransmissaoTrajetto.getInstance().buscarTrajetto(idVeiculo, dataInicial, dataFinal);
    }

    public void enviarComandos(Integer idComando, Integer idVeiculo) {
        new ComandoDAO().inserirComando(idComando, idVeiculo);
    }


    public List<RotaVO> buscarListaDeRotas(Integer idVeiculo) {
        List<RotaVO> listaDeRota = new ArrayList<>();
        try {
            //Buscar Dados das Rotas
            List<RotaVeiculo> listaRotaVeiculo = new RotaVeiculoDAO().buscarRotasPorVeiculo(idVeiculo);
            listaRotaVeiculo.forEach(rotaVeiculo -> {
                try {
                    RetornoRotaVO retornoRotaVO = rotaService.buscarPeloId(rotaVeiculo.getRota());
                    RotaVO rotaVO = retornoRotaVO.getRota();
                    rotaVO.setAtual(rotaVeiculo.getAtual());
                    listaDeRota.add(rotaVO);
                } catch (Exception e) {

                }
            });
        } catch (Exception e) {
            LogSistema.logError("buscarListaDeRotas", e);
        }
        return listaDeRota;
    }


    public void excluirRotaVeiculo(Integer id, Integer idVeiculo) {
        try {
            new RotaVeiculoDAO().excluirRotaVeiculo(id, idVeiculo);
        } catch (Exception e) {
            LogSistema.logError("excluirRotaVeiculo", e);
        }
    }

    private List<TemperaturaVO> buscarListaDeTemperatura(Integer idVeiculo) {
        try {
            return new TemperaturaDAO().buscarLista(idVeiculo);
        } catch (Exception e) {
            LogSistema.logError("buscarListaDeTemperatura", e);
        }
        return null;
    }

    private List<PortasDigitaisVO> buscarListaDePortas(Integer idVeiculo) {
        try {
            return new PortasDigitaisDAO().buscarLista(idVeiculo);
        } catch (Exception e) {
            LogSistema.logError("buscarListaDePortas", e);
        }
        return null;
    }

}
