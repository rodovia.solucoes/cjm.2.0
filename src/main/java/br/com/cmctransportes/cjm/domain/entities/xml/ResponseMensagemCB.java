package br.com.cmctransportes.cjm.domain.entities.xml;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name="ResponseMensagemCB")
public class ResponseMensagemCB {

    private List<MensagemCB> listaMensagemCB;

    @XmlElement(name="MensagemCB")
    public List<MensagemCB> getListaMensagemCB() {
        return listaMensagemCB;
    }

    public void setListaMensagemCB(List<MensagemCB> listaMensagemCB) {
        this.listaMensagemCB = listaMensagemCB;
    }
}
