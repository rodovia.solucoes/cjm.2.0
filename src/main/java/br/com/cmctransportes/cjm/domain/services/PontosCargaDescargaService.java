package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.Empresas;
import br.com.cmctransportes.cjm.domain.entities.Enderecos;
import br.com.cmctransportes.cjm.domain.entities.PontosCargaDescarga;
import br.com.cmctransportes.cjm.domain.entities.PontosDeEspera;
import br.com.cmctransportes.cjm.domain.entities.enums.LOCATION_STATUS;
import br.com.cmctransportes.cjm.domain.entities.vo.CoordenadaVO;
import br.com.cmctransportes.cjm.domain.entities.vo.EmpresaVO;
import br.com.cmctransportes.cjm.domain.entities.vo.LocalVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoLocalVO;
import br.com.cmctransportes.cjm.infra.dao.EnderecosDAO;
import br.com.cmctransportes.cjm.infra.dao.LocalDAO;
import br.com.cmctransportes.cjm.infra.dao.PontosCargaDescargaDAO;
import br.com.cmctransportes.cjm.infra.dao.PontosDeEsperaDAO;
import br.com.cmctransportes.cjm.proxy.LocalTrajetto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author analista.ti
 */
@Service
public class PontosCargaDescargaService {
// TODO: criar GenericService

    private PontosCargaDescargaDAO dao;
    private PontosDeEsperaDAO pdeDao;
    private EnderecosDAO eDao;

    @Autowired
    private PontosDeEsperaService pdeS;
    @Autowired
    private EmpresasService empresasService;

    private final EntityManager em;

    public PontosCargaDescargaService(EntityManager em) {
        this.em = em;
        dao = new PontosCargaDescargaDAO(em);
        pdeDao = new PontosDeEsperaDAO(em);
        eDao = new EnderecosDAO(em);
    }

    @Transactional
    public void save(PontosCargaDescarga entity) throws Exception {
        try {
            try {
                Enderecos tmpEnd = entity.getPontosDeEsperaId().getEndereco();
                tmpEnd.setEmpresaId(entity.getEmpresaId().getId());
                eDao.save(tmpEnd);
            } catch (Exception e) {
                entity.getPontosDeEsperaId().getEndereco().setId(null);
                throw e;
            }

            try {
                PontosDeEspera tmpPDE = entity.getPontosDeEsperaId();
                tmpPDE.setEmpresaId(entity.getEmpresaId());
                pdeDao.save(tmpPDE);
            } catch (Exception e) {
                entity.getPontosDeEsperaId().setId(null);
                throw e;
            }

            dao.save(entity);
        } catch (Exception e) {
            entity.getPontosDeEsperaId().getEndereco().setId(null);
            entity.getPontosDeEsperaId().setId(null);
            entity.setId(null);
            throw e;
        }
    }

    // public T getById(PK pk) {
    public List<PontosCargaDescarga> findAll() {
        // return dao.findAll();
        return dao.nqFindAll();
    }

    public PontosCargaDescarga getById(Integer id) {
        return dao.getById(id);
    }

    @Transactional
    public void update(PontosCargaDescarga entity) throws Exception {
        try {
            PontosDeEspera tmpPDE = entity.getPontosDeEsperaId();
            if (Objects.isNull(tmpPDE.getEmpresaId())) {
                tmpPDE.setEmpresaId(entity.getEmpresaId());
            }
            pdeS.update(tmpPDE);
            dao.update(entity);
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional
    public void delete(Integer id) throws Exception {
        try {
            PontosCargaDescarga entity = getById(id);
            PontosDeEspera tmpPDE = entity.getPontosDeEsperaId();
            Enderecos tmpEnd = tmpPDE.getEndereco();
            dao.delete(entity);
            pdeDao.delete(tmpPDE);
            eDao.delete(tmpEnd);
        } catch (Exception e) {
            throw e;
        }
    }

    public void delete(PontosCargaDescarga entity) throws Exception {
        try {
            this.delete(entity);
        } catch (Exception e) {
            throw e;
        }
    }

    public List<PontosCargaDescarga> findAll(Integer idEmpresa) {
        return dao.findAll(idEmpresa);
    }

    public List<LocalVO> findAllAsLocal(Integer idEmpresa) {

        List<LocalVO> lista = new ArrayList<>();
        Empresas empresas = empresasService.getById(idEmpresa);
        return new LocalDAO().buscarLocais(empresas.getIdClienteTrajetto());
    }
}
