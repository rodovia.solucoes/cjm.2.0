package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class VeiculoMinimoVO implements Serializable {
    private Integer id;
    private String placa;
    private String tipo;
    private String modelo;
    private String fabricante;
    private String nomeMotorista;
    private Boolean status;
}
