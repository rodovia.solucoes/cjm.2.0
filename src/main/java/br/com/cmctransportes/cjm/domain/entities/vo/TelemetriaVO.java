package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class TelemetriaVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String consumoAtual = "";
    private String consumoTotal = "";

    private String velocidadeMedia = "";
    private String velocidadeAtual = "";
    private String velocidadeMaxima = "";

    private String rotacaoMotorExcesso =  "";
    private String rotacaoMotorFaixaVerde = "";
    private String rotacaoMotorBaixaRotacao = "";


    private String consumoInstantaneo = "";
    private String frenagemBrusca;
    private String arrancadaBrusca;
    private String curvaBrusca;

    private String curvaAcentuadaAEsquerda;
    private String curvaAcentuadaADireita;

    private String rpmExcesso;
    private String rpmFaixaVerde;
    private String rpmBaixaRotacao;

    private String temperaturaDoMotor;

}
