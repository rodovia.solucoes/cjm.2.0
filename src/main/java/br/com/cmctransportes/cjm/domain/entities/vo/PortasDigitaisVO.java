package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class PortasDigitaisVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String dataCriacao;
    private String descricao;
}
