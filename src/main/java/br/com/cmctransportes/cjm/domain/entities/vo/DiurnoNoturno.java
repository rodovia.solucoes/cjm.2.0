package br.com.cmctransportes.cjm.domain.entities.vo;

import java.util.Date;
import java.util.List;

/**
 * @author analista.ti
 */
public class DiurnoNoturno {
    private long tempoDiurno;
    private long tempoNoturno;
    private List<Date> listaDeDadosDeFeriados;

    /**
     * @return the tempoDiurno
     */
    public long getTempoDiurno() {
        return tempoDiurno;
    }

    /**
     * @param tempoDiurno the tempoDiurno to set
     */
    public void setTempoDiurno(long tempoDiurno) {
        this.tempoDiurno = tempoDiurno;
    }

    /**
     * @return the tempoNoturno
     */
    public long getTempoNoturno() {
        return tempoNoturno;
    }

    /**
     * @param tempoNoturno the tempoNoturno to set
     */
    public void setTempoNoturno(long tempoNoturno) {
        this.tempoNoturno = tempoNoturno;
    }

    public List<Date> getListaDeDadosDeFeriados() {
        return listaDeDadosDeFeriados;
    }

    public void setListaDeDadosDeFeriados(List<Date> listaDeDadosDeFeriados) {
        this.listaDeDadosDeFeriados = listaDeDadosDeFeriados;
    }
}
