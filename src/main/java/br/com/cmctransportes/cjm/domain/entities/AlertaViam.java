package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "alerta_viam")
@Getter
@Setter
public class AlertaViam implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "ALERTA_VIAM_ID_SEQ", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "ALERTA_VIAM_ID_SEQ", sequenceName = "ALERTA_VIAM_ID_SEQ", allocationSize = 1)
    @Column(name = "id")
    private Integer id;

    @Column(name = "data_do_registro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataRegistro;

    @Column(name = "data_do_registro_mobile")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataRegistroNoMobile;

    @JoinColumn(name = "tipo_de_evento", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private EventosJornada eventosJornada;

    @JoinColumn(name = "empresa", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Empresas empresas;

    @JoinColumn(name = "motorista", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Motoristas motoristas;

    @Column(name = "jornada")
    private Integer jornada;

    @Column(name = "status")
    private Integer status;

    @Column(name = "data_da_notificacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataDaNotificacao;

    @Column(name = "quantidade_clique_ok")
    private Integer quantidadeCliqueOk;

    @Column(name = "quantidade_clique_cancelar")
    private Integer quantidadeCliqueCancelar;

}
