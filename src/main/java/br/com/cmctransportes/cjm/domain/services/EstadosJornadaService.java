/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.EstadosJornada;
import br.com.cmctransportes.cjm.infra.dao.EstadosJornadaDAO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * @author analista.ti
 */
@Service
public class EstadosJornadaService {
    // TODO: criar GenericService
    private EstadosJornadaDAO dao;

    private final EntityManager em;

    public EstadosJornadaService(EntityManager em) {
        this.em = em;
        dao = new EstadosJornadaDAO(em);
    }

    @Transactional
    public void save(EstadosJornada entity) {
        try {
            dao.save(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // public T getById(PK pk) {
    public List<EstadosJornada> findAll() {
        // return dao.findAll();
        return dao.nqFindAll();
    }

    public EstadosJornada getById(Integer id) {
        return dao.getById(id);
    }

    @Transactional
    public void update(EstadosJornada entity) {
        try {
            dao.update(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void delete(Integer id) {
        try {
            EstadosJornada tipo = getById(id);
            dao.delete(tipo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void delete(EstadosJornada entity) {
        try {
            dao.delete(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}