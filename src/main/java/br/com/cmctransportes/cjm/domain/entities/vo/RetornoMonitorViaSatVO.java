package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class RetornoMonitorViaSatVO extends RetornoVO {
    @Getter
    @Setter
    private List<MonitorViaSatVO> listaDeMonitorViaSat;
}