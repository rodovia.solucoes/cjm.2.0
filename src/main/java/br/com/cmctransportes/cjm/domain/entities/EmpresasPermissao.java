package br.com.cmctransportes.cjm.domain.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "empresas_permissao")
public class EmpresasPermissao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name = "empresas_permissao_id_seq", sequenceName = "empresas_permissao_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "empresas_permissao_id_seq")
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "id_permissao", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Permissao permissao;

    @JoinColumn(name = "id_empresas", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Empresas empresas;

    public EmpresasPermissao() {

    }

    public EmpresasPermissao(Integer id, Permissao permissao, Empresas empresas) {
        this.id = id;
        this.permissao = permissao;
        this.empresas = empresas;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Permissao getPermissao() {
        return permissao;
    }

    public void setPermissao(Permissao permissao) {
        this.permissao = permissao;
    }

    public Empresas getEmpresas() {
        return empresas;
    }

    public void setEmpresas(Empresas empresas) {
        this.empresas = empresas;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EmpresasPermissao)) {
            return false;
        }
        EmpresasPermissao other = (EmpresasPermissao) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.cmctransportes.cjm.domain.entities[ id=" + id + " ]";
    }
}
