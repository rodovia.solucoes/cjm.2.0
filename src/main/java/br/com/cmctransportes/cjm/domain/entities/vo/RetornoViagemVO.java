package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.Viagem;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RetornoViagemVO extends RetornoVO {
    private List<Viagem> listaViagens;
}
