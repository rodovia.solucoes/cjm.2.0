package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * @author analista.ti
 */
@Getter
@Setter
public class RelatorioDeEvento {
    private Integer mesReferencia;
    private Motoristas motorista;
    private Date dataInicio;
    private Date dataFim;

    // Empresa

    private Date calculaDataFim(Integer mesReferencia) {
        GregorianCalendar agora = (GregorianCalendar) GregorianCalendar.getInstance();

        int ano = agora.get(GregorianCalendar.YEAR);
        int mes = mesReferencia;
        int dia = 20;   // TODO: usar configuração para isso aqui

        // se o mês de referência for maior que o atual, pega o ano anterior
        if (mesReferencia > agora.get(GregorianCalendar.MONTH)) {
            ano -= 1;
        }

        // se o  mês for igual
        if (mesReferencia == agora.get(GregorianCalendar.MONTH)) {

            // se não passou do dia 20 ainda não fechou o mês, pega o anterior
            if (agora.get(GregorianCalendar.DAY_OF_MONTH) <= dia) {
                mes -= 1;
            }
        }

        return (new GregorianCalendar(ano, mes, dia)).getTime();
//        return (new GregorianCalendar(ano, 11, 30)).getTime(); // TODO: valor para teste
    }

    private Date calculaDataInicio(Integer mesReferencia) {
        GregorianCalendar tmpCalendar = new GregorianCalendar();
        tmpCalendar.setTime(calculaDataFim(mesReferencia));
        tmpCalendar.add(GregorianCalendar.MONTH, -1);
        return tmpCalendar.getTime();
//        return (new GregorianCalendar(2016, 0, 1)).getTime(); // TODO: valor para teste
    }

    public RelatorioDeEvento() {
    }

    public RelatorioDeEvento(Integer mesReferencia) {
        this.mesReferencia = mesReferencia;
        setDataInicio(calculaDataInicio(mesReferencia));
        setDataFim(calculaDataFim(mesReferencia));
    }

    public RelatorioDeEvento(Date inicio, Date fim) {
        GregorianCalendar tmpCalendar = new GregorianCalendar();
        tmpCalendar.setTime(fim);

        this.mesReferencia = tmpCalendar.get(GregorianCalendar.MONTH);
        setDataInicio(inicio);
        setDataFim(fim);
    }

    public RelatorioDeEvento(Integer mesReferencia, List<Evento> relatorioDeEvento) {
        this.mesReferencia = mesReferencia;
        setDataInicio(calculaDataInicio(mesReferencia));
        setDataFim(calculaDataFim(mesReferencia));
    }

    /**
     * @return the identificacaoTurno
     */
    public String getIdentificacaoTurno() {
        if ((this.motorista != null) && (this.motorista.getTurnosId() != null)) {
            return this.motorista.getTurnosId().getIdentificacao();
        }

        return "";
    }
}
