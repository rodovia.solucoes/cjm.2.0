package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.Turnos;
import br.com.cmctransportes.cjm.infra.dao.TurnosDAO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * @author analista.ti
 */
@Service
public class TurnosService {
// TODO: criar GenericService

    private TurnosDAO dao;

    private final EntityManager em;

    public TurnosService(EntityManager em) {
        this.em = em;
        dao = new TurnosDAO(em);
    }

    @Transactional
    public void save(Turnos entity) {
        try {
            entity.setId(null);
            dao.save(entity);
        } catch (Exception e) {
            e.printStackTrace();
            entity.setId(null);
            throw e;
        }
    }

    // public T getById(PK pk) {
    public List<Turnos> findAll() {
        // return dao.findAll();
        return dao.nqFindAll();
    }

    public Turnos getById(Integer id) {
        return dao.getById(id);
    }

    @Transactional
    public void update(Turnos entity) {
        try {
            dao.update(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void delete(Integer id) {
        try {
            Turnos tipo = getById(id);
            dao.delete(tipo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void delete(Turnos entity) {
        try {
            dao.delete(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Turnos> findAll(Integer idEmpresa) {
        return dao.findAll(idEmpresa);
    }
}
