package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.constants.Constantes;
import br.com.cmctransportes.cjm.domain.entities.*;
import br.com.cmctransportes.cjm.domain.entities.xml.MensagemCB;
import br.com.cmctransportes.cjm.domain.entities.xml.ResponseMensagemCB;
import br.com.cmctransportes.cjm.domain.repository.EventoRepository;
import br.com.cmctransportes.cjm.domain.repository.JourneyRepository;
import br.com.cmctransportes.cjm.infra.dao.*;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.utils.TimeHelper;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

@Service
public class IntegracaoTruckControlService {

    @Autowired
    private DriverService driverService;
    private final EventoDAO eventoDAO;
    private final JornadaDAO jornadaDAO;
    private final EntityManager em;

    @Autowired
    private JourneyRepository journeyRepository;
    @Autowired
    private EventoRepository eventoRepository;
    @Autowired
    private RastroService rastroService;


    final AtomicReference<Jornada> atomicJornada = new AtomicReference<>();

    public IntegracaoTruckControlService(EntityManager em) {
        this.em = em;
        eventoDAO = new EventoDAO(em);
        jornadaDAO = new JornadaDAO(em);
    }

    private String url = "http://webservice.newrastreamentoonline.com.br";
    private String xml =  "<RequestMensagemCB> " +
            "<login>"+ Constantes.ID_TRUCKS_CONTROL +"</login>" +
            "<senha>"+Constantes.SENHA_TRUCKS_CONTROL+"</senha>" +
            "<mId>#</mId>" +
            "</RequestMensagemCB>";

    public void iniciar(){
        try{
            IntegracaoTerceiro integracaoTerceiro = buscarIntegracaoTerceiro();
            if(Objects.nonNull(integracaoTerceiro)){
                ResponseMensagemCB responseMensagemCB = buscarDadosNoCliente(integracaoTerceiro.getUltimoId());
                if(Objects.nonNull(responseMensagemCB) && Objects.nonNull(responseMensagemCB.getListaMensagemCB())){
                    double ultimoId = 1d;
                    List<MensagemCB> cb = responseMensagemCB.getListaMensagemCB();
                    if(!cb.isEmpty()){
                        for(MensagemCB mensagemCB : cb){
                            processarMensagemCB(mensagemCB);
                            ultimoId = mensagemCB.getmId();
                        }
                        integracaoTerceiro.setUltimoId(ultimoId);
                        editar(integracaoTerceiro);
                    }
                }
            }
        }catch (Exception e){
            LogSistema.logError("iniciar", e);
        }
    }

    private void processarMensagemCB(MensagemCB mensagemCB){
        try {
            if(Objects.nonNull(mensagemCB.getTfrID()) && mensagemCB.getTfrID() > 0){
                MacroTruckControl macroTruckControl = new MacroTruckControlDAO().buscarMacro(mensagemCB.getTfrID());
                Motoristas motoristas = driverService.buscarPeloIdIntegracao(mensagemCB.getMotID());
                if(Objects.nonNull(macroTruckControl) && Objects.nonNull(motoristas)){
                    processarJornada(macroTruckControl, motoristas, mensagemCB);
                }
            }
        }catch (Exception e){
            LogSistema.logError("processarMensagemCB", e);
        }
    }


    private void processarJornada(MacroTruckControl macroTruckContro, Motoristas motoristas, MensagemCB mensagemCB){
        List<Evento> listaDeEvento = new ArrayList<>();
        try {
            Integer ultimoEvento = this.eventoDAO.buscarUltimoIdEvento(motoristas.getUserId());
            if (macroTruckContro.getRodovia().equals(ultimoEvento)) {
                return;
            }
            Date instanteLancamento = ajustarData(mensagemCB.getDt());
            Date instanteEvento = ajustarData(mensagemCB.getDt());
            Double latitude = ajustarDouble(mensagemCB.getLat());
            Double longitude = ajustarDouble(mensagemCB.getLon());
            String end = "";
            if(Objects.nonNull(mensagemCB.getRua()) && !mensagemCB.getRua().isEmpty()){
                end = " Rua: " + mensagemCB.getRua();
            }

            if(Objects.nonNull(mensagemCB.getRod()) && !mensagemCB.getRod().isEmpty()){
                end = " Rod: " + mensagemCB.getRod();
            }
            String pontoParada = end + " Cidade: " + mensagemCB.getMun();

            //Tempos usados para fechar eventos, nesse caso tiramos um segundo
            Date tempoUm = TimeHelper.diminuirSegundosNaData(instanteEvento, 1);
            Date tempoDois = TimeHelper.diminuirSegundosNaData(instanteLancamento, 1);

            Empresas empresa = motoristas.getEmpresa();

            Integer idRodovia = macroTruckContro.getRodovia();
            if ((idRodovia != 1 && idRodovia != -100) && ultimoEvento == 0) {
                //siginifica que tem um fim de jornada com um novo evento e sem inicio de jornada
                //insere uma nova jornada

                this.createEvento(tempoUm, tempoDois, latitude, longitude, 1, pontoParada, motoristas, empresa, listaDeEvento);
                this.createEvento(instanteEvento, instanteLancamento, latitude, longitude, idRodovia, pontoParada, motoristas, empresa, listaDeEvento);
            } else if (idRodovia == 1 && (ultimoEvento == 0 || ultimoEvento == -100)) { //Abre uma nova jornada
                this.createEvento(instanteEvento, instanteLancamento, latitude, longitude, idRodovia, pontoParada, motoristas, empresa, listaDeEvento);
            } else if (idRodovia == 0) {//fecha a jornada
                if (ultimoEvento > 1) {
                    //temos que fechar os eventos
                    //tenho que fechar o evento atual
                    int valor = ultimoEvento * -1;
                    this.createEvento(tempoUm, tempoDois, latitude, longitude, valor, pontoParada, motoristas, empresa, listaDeEvento);//fechei o evento
                }
                this.createEvento(instanteEvento, instanteLancamento, latitude, longitude, idRodovia, pontoParada, motoristas, empresa, listaDeEvento);
            } else if (idRodovia != 0 && idRodovia != 1 && idRodovia != -100) {
                if (idRodovia > 1) {
                    if (ultimoEvento > 1 //nao esta fechado
                            && idRodovia > 1 /* nao é inicio de jornda*/) {
                        //tenho que fechar o evento atual
                        int valor = ultimoEvento * -1;
                        this.createEvento(tempoUm, tempoDois, latitude, longitude, valor, pontoParada, motoristas, empresa, listaDeEvento);//fechei o evento
                    }
                    //inserir novo evento
                    this.createEvento(instanteEvento, instanteLancamento, latitude, longitude, idRodovia, pontoParada, motoristas, empresa, listaDeEvento);
                } else if (idRodovia < 0) {
                    //vai fechar o evento caso tenha um evento fechado
                    this.createEvento(instanteEvento, instanteLancamento, latitude, longitude, idRodovia, pontoParada, motoristas, empresa, listaDeEvento);
                }
            }else if(ultimoEvento < 0){
                //Signidfica que tem um evento fechado, vamos abrir um outro evento normal
                this.createEvento(instanteEvento, instanteLancamento, latitude, longitude, idRodovia, pontoParada, motoristas, empresa, listaDeEvento);
            }
            createRastro(instanteEvento, instanteLancamento, latitude, longitude, motoristas, empresa, mensagemCB.getMun(), "NE", end);
        }catch (Exception e){
            LogSistema.logError("processarJornada", e);
        }
    }

    private IntegracaoTerceiro buscarIntegracaoTerceiro(){
        return new IntegracaoTerceiroDAO().buscarPelaEmpresa("Trucks Controle");
    }

    private void editar(IntegracaoTerceiro integracaoTerceiro){
        new IntegracaoTerceiroDAO().editar(integracaoTerceiro);
    }

    private ResponseMensagemCB buscarDadosNoCliente(Double ultimoId){
        File file = null;
        try {
            xml = xml.replaceAll("#", new BigDecimal(ultimoId).toPlainString());
            HttpClient httpClient = new HttpClient();
            PostMethod postMethod = new PostMethod(url);
            postMethod.setRequestHeader("Accept", "application/soap+xml,multipart/related,text/*");
            postMethod.setRequestHeader("Cache-Control", "no-cache");
            postMethod.setRequestHeader("Pragma", "no-cache");
            postMethod.setRequestHeader("Content-Type", "text/xml; charset=utf-8");
            final int contentLength = xml.length();
            postMethod.setRequestHeader("Content-Length", String.valueOf(contentLength));
            postMethod.setRequestEntity(new StringRequestEntity(xml, "text/xml", "utf-8"));
            final int statusCode = httpClient.executeMethod(postMethod);
            final byte[] responseBody = postMethod.getResponseBody();
            file = zipBytes(responseBody);
            //file = new File("C:\\temp\\xml\\92cf164f-f8bd-4adf-a98f-761579ed5203.txt.xml");
            JAXBContext jaxbContext = JAXBContext.newInstance(ResponseMensagemCB.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            ResponseMensagemCB cb = (ResponseMensagemCB) jaxbUnmarshaller.unmarshal(file);
            return cb;
        }catch (Exception e){
            LogSistema.logError("buscarDadosNoCliente", e);
        }finally {
            if(Objects.nonNull(file)){
                //file.delete();
            }
        }
        return null;
    }


    public File zipBytes(byte[] responseBody) throws IOException {
        ZipInputStream zipStream = new ZipInputStream(new ByteArrayInputStream(responseBody));
        ZipEntry entry = null;
        File f = null;
        while ((entry = zipStream.getNextEntry()) != null) {
            String entryName = entry.getName();
            String diretorio = Constantes.DIRETORIO_XML_INTEGRACAO + entryName + ".xml";
            f = new File(diretorio);
            FileOutputStream out = new FileOutputStream(f);
            byte[] byteBuff = new byte[4096];
            int bytesRead = 0;
            while ((bytesRead = zipStream.read(byteBuff)) != -1)
            {
                out.write(byteBuff, 0, bytesRead);
            }
            out.close();
            zipStream.closeEntry();
        }
        zipStream.close();
        return f;
    }

    private Date ajustarData(String data){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String s[] = data.split("T");
        data = s[0] + " " +s[1].split("-")[0];
        try {
            return simpleDateFormat.parse(data);
        } catch (ParseException e) {
           LogSistema.logError("ajustarData", e);
        }
        return new Date();
    }

    private Double ajustarDouble(String valor){
        return Double.parseDouble(valor.replace(",", "."));
    }


    /**
     * Salva o evento no Banco
     */
    public void createEvento(Date instanteEvento, Date instanteLancamento, Double latitude, Double longitude, Integer tipoEvento,
                             String pontoParada, Motoristas motoristaId, Empresas empresaId, List<Evento> listaDeEventos) {

        try {

            Evento result = new Evento();

            final List<Evento> eventosJornada = new ArrayList<>();
            int[] eventosEterno = new int[]{2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 15, 16, 23, 25};
            final AtomicReference<Integer> isEterno = new AtomicReference<>(null);

            if (tipoEvento == 1) {
                Jornada jornada = new Jornada();
                jornada.setMotoristasId(motoristaId);
                jornada.setEmpresaId(empresaId);
                jornada.setDsrHE(Boolean.FALSE);
                jornada.setWaitingAsWorked(Boolean.FALSE);
                jornada.setLocked(Boolean.FALSE);
                jornada = this.journeyRepository.saveAndFlush(jornada);
                atomicJornada.set(jornada);
            } else {
                Jornada jornada = this.jornadaDAO.getLastJourney(motoristaId.getId());
                if (Objects.isNull(jornada)) {
                    jornada = new Jornada();
                    jornada.setMotoristasId(motoristaId);
                    jornada.setEmpresaId(empresaId);
                    jornada.setDsrHE(Boolean.FALSE);
                    jornada.setWaitingAsWorked(Boolean.FALSE);
                    jornada.setLocked(Boolean.FALSE);
                    jornada = this.journeyRepository.saveAndFlush(jornada);
                }
                atomicJornada.set(jornada);
            }

            result.setJornadaId(atomicJornada.get());
            result.setInstanteEvento(instanteEvento);
            result.setInstanteLancamento(instanteLancamento);
            result.setLatitude(latitude);
            result.setLongitude(longitude);
            result.setTipoEvento(new EventosJornada(tipoEvento));
            result.setPositionAddress(pontoParada);
            result.setOperadorLancamento(new Users(motoristaId.getUserId()));
            result.setEmpresaId(empresaId);

            if (Arrays.binarySearch(eventosEterno, result.getTipoEvento().getId()) >= 0) {
                isEterno.set(result.getTipoEvento().getId());
            } else {
                this.eventoDAO.address(result);
                this.eventoRepository.save(result);
            }
            if (Objects.nonNull(isEterno.get())) {
                this.eventoDAO.address(result);
                this.eventoRepository.save(result);
            }

            if (tipoEvento == 0) {
                atomicJornada.set(null);
            }

            listaDeEventos.add(result);

        } catch (Exception e) {
            LogSistema.logError("createEvento", e);
        }
    }

    private void createRastro(Date instanteEvento, Date instanteLancamento, Double latitude, Double longitude,
                              Motoristas motoristaId, Empresas empresaId, String cidade, String uf, String endereco) {
        Rastro result = new Rastro();
        result.setEmpresaId(empresaId);
        result.setMotoristaId(motoristaId.getId());
        result.setInstante(instanteEvento);
        result.setInstanteEnvio(instanteLancamento);
        result.setLatitude(latitude);
        result.setLongitude(longitude);
        result.setCidade(cidade);
        result.setUf(uf);
        result.setEndereco(endereco);
        rastroService.save(result);
    }

}
