package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * entidade para consulta no banco de dados sobre jornadas
 *
 * @author analista.ti
 */
@Entity
@Table(name = "turnos")
@Getter
@Setter
@NamedQueries({
        @NamedQuery(name = "Turnos.findAll", query = "SELECT t FROM Turnos t"),
        @NamedQuery(name = "Turnos.findById", query = "SELECT t FROM Turnos t WHERE t.id = :id"),
        @NamedQuery(name = "Turnos.findByIdentificacao", query = "SELECT t FROM Turnos t WHERE t.identificacao = :identificacao"),
        @NamedQuery(name = "Turnos.findByHorasMensais", query = "SELECT t FROM Turnos t WHERE t.horasMensais = :horasMensais"),
        @NamedQuery(name = "Turnos.findByHorasSemanais", query = "SELECT t FROM Turnos t WHERE t.horasSemanais = :horasSemanais"),
        @NamedQuery(name = "Turnos.findByAcordoCompensacao", query = "SELECT t FROM Turnos t WHERE t.acordoCompensacao = :acordoCompensacao"),
        @NamedQuery(name = "Turnos.findByHorarioEntrada01", query = "SELECT t FROM Turnos t WHERE t.horarioEntrada01 = :horarioEntrada01"),
        @NamedQuery(name = "Turnos.findByHorarioEntrada02", query = "SELECT t FROM Turnos t WHERE t.horarioEntrada02 = :horarioEntrada02"),
        @NamedQuery(name = "Turnos.findByHorarioEntrada03", query = "SELECT t FROM Turnos t WHERE t.horarioEntrada03 = :horarioEntrada03"),
        @NamedQuery(name = "Turnos.findByHorarioEntrada04", query = "SELECT t FROM Turnos t WHERE t.horarioEntrada04 = :horarioEntrada04"),
        @NamedQuery(name = "Turnos.findByHorarioEntrada05", query = "SELECT t FROM Turnos t WHERE t.horarioEntrada05 = :horarioEntrada05"),
        @NamedQuery(name = "Turnos.findByHorarioEntrada06", query = "SELECT t FROM Turnos t WHERE t.horarioEntrada06 = :horarioEntrada06"),
        @NamedQuery(name = "Turnos.findByHorarioEntrada07", query = "SELECT t FROM Turnos t WHERE t.horarioEntrada07 = :horarioEntrada07"),
        @NamedQuery(name = "Turnos.findByHorarioSaida01", query = "SELECT t FROM Turnos t WHERE t.horarioSaida01 = :horarioSaida01"),
        @NamedQuery(name = "Turnos.findByHorarioSaida02", query = "SELECT t FROM Turnos t WHERE t.horarioSaida02 = :horarioSaida02"),
        @NamedQuery(name = "Turnos.findByHorarioSaida03", query = "SELECT t FROM Turnos t WHERE t.horarioSaida03 = :horarioSaida03"),
        @NamedQuery(name = "Turnos.findByHorarioSaida04", query = "SELECT t FROM Turnos t WHERE t.horarioSaida04 = :horarioSaida04"),
        @NamedQuery(name = "Turnos.findByHorarioSaida05", query = "SELECT t FROM Turnos t WHERE t.horarioSaida05 = :horarioSaida05"),
        @NamedQuery(name = "Turnos.findByHorarioSaida06", query = "SELECT t FROM Turnos t WHERE t.horarioSaida06 = :horarioSaida06"),
        @NamedQuery(name = "Turnos.findByHorarioSaida07", query = "SELECT t FROM Turnos t WHERE t.horarioSaida07 = :horarioSaida07"),
        @NamedQuery(name = "Turnos.findByTempoDiario01", query = "SELECT t FROM Turnos t WHERE t.tempoDiario01 = :tempoDiario01"),
        @NamedQuery(name = "Turnos.findByTempoDiario02", query = "SELECT t FROM Turnos t WHERE t.tempoDiario02 = :tempoDiario02"),
        @NamedQuery(name = "Turnos.findByTempoDiario03", query = "SELECT t FROM Turnos t WHERE t.tempoDiario03 = :tempoDiario03"),
        @NamedQuery(name = "Turnos.findByTempoDiario04", query = "SELECT t FROM Turnos t WHERE t.tempoDiario04 = :tempoDiario04"),
        @NamedQuery(name = "Turnos.findByTempoDiario05", query = "SELECT t FROM Turnos t WHERE t.tempoDiario05 = :tempoDiario05"),
        @NamedQuery(name = "Turnos.findByTempoDiario06", query = "SELECT t FROM Turnos t WHERE t.tempoDiario06 = :tempoDiario06"),
        @NamedQuery(name = "Turnos.findByTempoDiario07", query = "SELECT t FROM Turnos t WHERE t.tempoDiario07 = :tempoDiario07"),
        @NamedQuery(name = "Turnos.findByTempoAlimentacao01", query = "SELECT t FROM Turnos t WHERE t.tempoAlimentacao01 = :tempoAlimentacao01"),
        @NamedQuery(name = "Turnos.findByTempoAlimentacao02", query = "SELECT t FROM Turnos t WHERE t.tempoAlimentacao02 = :tempoAlimentacao02"),
        @NamedQuery(name = "Turnos.findByTempoAlimentacao03", query = "SELECT t FROM Turnos t WHERE t.tempoAlimentacao03 = :tempoAlimentacao03"),
        @NamedQuery(name = "Turnos.findByTempoAlimentacao04", query = "SELECT t FROM Turnos t WHERE t.tempoAlimentacao04 = :tempoAlimentacao04"),
        @NamedQuery(name = "Turnos.findByTempoAlimentacao05", query = "SELECT t FROM Turnos t WHERE t.tempoAlimentacao05 = :tempoAlimentacao05"),
        @NamedQuery(name = "Turnos.findByTempoAlimentacao06", query = "SELECT t FROM Turnos t WHERE t.tempoAlimentacao06 = :tempoAlimentacao06"),
        @NamedQuery(name = "Turnos.findByTempoAlimentacao07", query = "SELECT t FROM Turnos t WHERE t.tempoAlimentacao07 = :tempoAlimentacao07")})
public class Turnos implements Serializable {

    @Column(name = "tipo", nullable = false)
    private int tipo;
    @Column(name = "horario_entrada01")
    private Long horarioEntrada01;
    @Column(name = "horario_entrada02")
    private Long horarioEntrada02;
    @Column(name = "horario_entrada03")
    private Long horarioEntrada03;
    @Column(name = "horario_entrada04")
    private Long horarioEntrada04;
    @Column(name = "horario_entrada05")
    private Long horarioEntrada05;
    @Column(name = "horario_entrada06")
    private Long horarioEntrada06;
    @Column(name = "horario_entrada07")
    private Long horarioEntrada07;
    @Column(name = "horario_saida01")
    private Long horarioSaida01;
    @Column(name = "horario_saida02")
    private Long horarioSaida02;
    @Column(name = "horario_saida03")
    private Long horarioSaida03;
    @Column(name = "horario_saida04")
    private Long horarioSaida04;
    @Column(name = "horario_saida05")
    private Long horarioSaida05;
    @Column(name = "horario_saida06")
    private Long horarioSaida06;
    @Column(name = "horario_saida07")
    private Long horarioSaida07;
    @Column(name = "tempo_diario01")
    private Long tempoDiario01;
    @Column(name = "tempo_diario02")
    private Long tempoDiario02;
    @Column(name = "tempo_diario03")
    private Long tempoDiario03;
    @Column(name = "tempo_diario04")
    private Long tempoDiario04;
    @Column(name = "tempo_diario05")
    private Long tempoDiario05;
    @Column(name = "tempo_diario06")
    private Long tempoDiario06;
    @Column(name = "tempo_diario07")
    private Long tempoDiario07;
    @Column(name = "tempo_alimentacao01")
    private Long tempoAlimentacao01;
    @Column(name = "tempo_alimentacao02")
    private Long tempoAlimentacao02;
    @Column(name = "tempo_alimentacao03")
    private Long tempoAlimentacao03;
    @Column(name = "tempo_alimentacao04")
    private Long tempoAlimentacao04;
    @Column(name = "tempo_alimentacao05")
    private Long tempoAlimentacao05;
    @Column(name = "tempo_alimentacao06")
    private Long tempoAlimentacao06;
    @Column(name = "tempo_alimentacao07")
    private Long tempoAlimentacao07;

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TURNOS_ID_SEQ", sequenceName = "TURNOS_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TURNOS_ID_SEQ")
    @Column(name = "id")
    private Integer id;
    @Column(name = "identificacao", length = 25, nullable = false)
    private String identificacao;
    @Column(name = "horas_mensais")
    private Integer horasMensais;
    @Column(name = "horas_semanais")
    private Integer horasSemanais;
    @Column(name = "acordo_compensacao")
    private Boolean acordoCompensacao;
    @JoinColumn(name = "empresa_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Empresas empresaId;
    @JoinColumn(name = "tolerancias_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Tolerancias toleranciasId;

    @Column(name = "dia_folga")
    private Integer diaDeFolga;

    @Column(name = "week_turn_day")
    private Integer weekTurnDay;

    @Column(name = "week_days")
    private Integer weekDays;
}