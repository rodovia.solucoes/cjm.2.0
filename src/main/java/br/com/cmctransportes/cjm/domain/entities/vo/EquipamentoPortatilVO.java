package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

public class EquipamentoPortatilVO implements Serializable {
    private static final long serialVersionUID = 1L;

    public EquipamentoPortatilVO() {

    }

    public EquipamentoPortatilVO(Integer id) {
        this.id = id;
    }

    @Getter
    @Setter
    private Integer id;
    @Getter
    @Setter
    private Date dataCadastro;
    @Getter
    @Setter
    private String imei;
    @Getter
    @Setter
    private String donoDoChip;
    @Getter
    @Setter
    private String donoDoAparelho;
    @Getter
    @Setter
    private String numeroCelular;
    @Getter
    @Setter
    private String numeroSerialChip;
    @Getter
    @Setter
    private Boolean status;
    @Getter
    @Setter
    private String apelido;
    @Getter
    @Setter
    private String descricao;
    @Getter
    @Setter
    private String observacao;
    @Getter
    @Setter
    private Integer cliente;
    @Getter
    @Setter
    private Integer unidade;
    @Getter
    @Setter
    private ModeloVO modelo;
}
