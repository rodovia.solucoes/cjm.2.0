package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.Empresas;
import br.com.cmctransportes.cjm.domain.entities.vo.EquipamentoPortatilVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoEquipamentoPortatilVO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.proxy.EquipamentoPortatilTrajetto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EquipamentoPortatilService {

    @Autowired
    private EmpresasService empresasService;

    public RetornoEquipamentoPortatilVO cadastrar(EquipamentoPortatilVO  equipamentoPortatilVO) throws Exception{
        try {
            Empresas empresas = empresasService.getById(equipamentoPortatilVO.getCliente());
            equipamentoPortatilVO.setCliente(empresas.getIdClienteTrajetto());
            return EquipamentoPortatilTrajetto.getInstance().cadastrar(equipamentoPortatilVO);
        }catch (Exception e){
            LogSistema.logError("cadastrar", e);
            throw new Exception(e);
        }
    }

    public RetornoEquipamentoPortatilVO editar(EquipamentoPortatilVO  equipamentoPortatilVO) throws Exception{
        try {
            Empresas empresas = empresasService.getById(equipamentoPortatilVO.getCliente());
            equipamentoPortatilVO.setCliente(empresas.getIdClienteTrajetto());
            return EquipamentoPortatilTrajetto.getInstance().editar(equipamentoPortatilVO);
        }catch (Exception e){
            LogSistema.logError("editar", e);
            throw new Exception(e);
        }
    }


    public RetornoEquipamentoPortatilVO buscar(Integer id) throws Exception{
        try {
            return EquipamentoPortatilTrajetto.getInstance().buscarPeloCodigo(id);
        }catch (Exception e){
            LogSistema.logError("editar", e);
            throw new Exception(e);
        }
    }


    public RetornoEquipamentoPortatilVO listar(Integer idCliente, Integer idUnidade) throws Exception{
        try {
            Empresas empresas = empresasService.getById(idCliente);
            return EquipamentoPortatilTrajetto.getInstance().listar(empresas.getIdClienteTrajetto(), idUnidade);
        }catch (Exception e){
            LogSistema.logError("editar", e);
            throw new Exception(e);
        }
    }

}
