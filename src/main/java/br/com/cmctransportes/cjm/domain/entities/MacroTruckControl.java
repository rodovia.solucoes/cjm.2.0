package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MacroTruckControl {

    private Integer idEmpresa;
    private Integer trucksControle;
    private Integer rodovia;
    private Boolean ativo;
    private String descricao;
    private String mensagem;

}
