package br.com.cmctransportes.cjm.domain.entities.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class RetornoRelatorioVO extends RetornoVO {
    private String url;
    private String base64;
    private List<InformacaoRelatorioVO> listaInformacaoRelatorio;
    private InformacaoRelatorioVO informacaoRelatorio;
    private NumeroDeViagemRelatorioVO numeroDeViagemRelatorio;
    private DisponibilidadeFisicaVO disponibilidadeFisica;
    private List<DetalheTelemetriaVO> listaDetalheTelemetria;
    private RelatorioGraficoVO relatorioGrafico;

}
