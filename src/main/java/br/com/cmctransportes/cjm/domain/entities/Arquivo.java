package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "arquivo")
@Getter
@Setter
public class Arquivo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "arquivo_id_seq", sequenceName = "arquivo_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "arquivo_id_seq")
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "size")
    private Float size;
    @Column(name = "type")
    private String type;
    @Column(name = "dados")
    private String dados;
    @Column(name = "contrato")
    private Integer contrato;

}
