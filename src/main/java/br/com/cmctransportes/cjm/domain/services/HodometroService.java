package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.Users;
import br.com.cmctransportes.cjm.domain.entities.vo.HodometroVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoHodometroVO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.proxy.HodometroTrajetto;
import br.com.cmctransportes.cjm.proxy.modelo.Veiculo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HodometroService {

    @Autowired
    private UserService userService;

    public RetornoHodometroVO registrarHodometro(HodometroVO hodometroVO) throws Exception {
        try {
            return HodometroTrajetto.getInstance().registrarHodometro(hodometroVO);
        } catch (Exception e) {
            LogSistema.logError("registrarHodometro", e);
            throw new Exception(e);
        }
    }

    public RetornoHodometroVO registrarHodometroSemUltimaTransmissao(HodometroVO hodometroVO) throws Exception {
        try {
            return HodometroTrajetto.getInstance().registrarHodometroSemUltimaTransmissao(hodometroVO);
        } catch (Exception e) {
            LogSistema.logError("registrarHodometroSemUltimaTransmissao", e);
            throw new Exception(e);
        }
    }


    public RetornoHodometroVO buscarListaDeHodometro(Veiculo veiculo) throws Exception {
        try {
            RetornoHodometroVO vo = HodometroTrajetto.getInstance().buscarListaDeHodometro(veiculo);
            vo.getListaDeHodometro().forEach(v -> {
                v.setVeiculo(null);
                if ("M".equals(v.getTipo())) {
                    Users users = userService.getById(v.getUsuario());
                    v.setNomeUsuario(users.getUserName());
                }
            });
            return vo;
        } catch (Exception e) {
            LogSistema.logError("buscarListaDeHodometro", e);
            throw new Exception(e);
        }
    }
}
