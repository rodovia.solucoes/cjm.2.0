package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.ControleCombustivel;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class RetornoControleCombustivelVO extends RetornoVO {

    @Getter
    @Setter
    private ControleCombustivel controleCombustivel;
    @Getter
    @Setter
    private List<ControleCombustivel> listaControleCombustivel;

}
