package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.activemq.ActiveMQMessageProducer;
import br.com.cmctransportes.cjm.constants.Constantes;
import br.com.cmctransportes.cjm.domain.entities.Motoristas;
import br.com.cmctransportes.cjm.domain.entities.Rastro;
import br.com.cmctransportes.cjm.domain.entities.vo.EmpresaVO;
import br.com.cmctransportes.cjm.domain.entities.vo.MotoristaVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoMapaVO;
import br.com.cmctransportes.cjm.infra.dao.RastroDAO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.utils.TimeHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.jms.JMSException;
import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

/**
 * @author analista.ti
 */
@Service
public class RastroService {
    // TODO: criar GenericService
    private RastroDAO dao;

    @Autowired
    private DriverService driverService;

    private final EntityManager em;

    public RastroService(EntityManager em) {
        this.em = em;
        dao = new RastroDAO(em);
    }

    @Transactional
    public void save(Rastro entity) {
        try {
            dao.save(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // public T getById(PK pk) {
    public List<Rastro> findAll() {
        // return dao.findAll();
        return dao.nqFindAll();
    }

    public List<Rastro> relatorioDePosicoes(Integer motoristaId, Long dtInicio, Long dtFim) {
        List<Rastro> result = null;

        Motoristas mot = this.driverService.getById(motoristaId);

        Date dataInicio = TimeHelper.getDate000000(new Date(dtInicio));
        Date dataFim = TimeHelper.getDate235959(new Date(dtFim));

        result = dao.getRelatorioDePosicoes(mot, dataInicio, dataFim);

        System.out.println("RastroService RelatorioPosições");

        return result;
    }

    public Rastro getById(Integer id) {
        return dao.getById(id);
    }

    @Transactional
    public void update(Rastro entity) {
        try {
            dao.update(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void delete(Integer id) {
        try {
            Rastro tipo = getById(id);
            dao.delete(tipo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void delete(Rastro entity) {
        try {
            dao.delete(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void ajustarRastroParaTransmissao(EmpresaVO empresaVO, RetornoMapaVO retornoMapaVO) throws Exception{
        dao.ajustarRastroParaTransmissao(empresaVO, retornoMapaVO);
    }

    public void enviarFilaJMS(Rastro rastro) {
        try {
            ActiveMQMessageProducer msgQueueSender = new ActiveMQMessageProducer();
            msgQueueSender.setup(true, false, Constantes.FILA_ENTRADA_DADOS);
            msgQueueSender.sendMessage(rastro);
            msgQueueSender.commit(true);
            msgQueueSender.close();
        } catch (JMSException e) {
            LogSistema.logError("enviarFilaJMS", e);
        }
    }


    public Rastro buscarUltimoRastro(MotoristaVO motorista)  {
        try {
            return dao.buscarUltimoRastro(motorista);
        }catch (Exception e){
            LogSistema.logError("buscarUltimoRastro", e);
        }
        return null;
    }
}
