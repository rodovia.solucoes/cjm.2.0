package br.com.cmctransportes.cjm.domain.entities;

public class LatLng {

    private final int kg;
    public final double latitude;
    public final double longitude;

    private Double latitude2;
    private Double longitude2;

    public LatLng(double paramDouble1, double paramDouble2) {
        this(1, paramDouble1, paramDouble2);
        this.latitude2 = paramDouble1;
        this.longitude2 = paramDouble2;
    }

    private LatLng(int paramInt, double paramDouble1, double paramDouble2) {
        this.kg = paramInt;
        if ((-180.0D <= paramDouble2) && (paramDouble2 < 180.0D)) {
            this.longitude = paramDouble2;
        } else {
            this.longitude = ((360.0D + (paramDouble2 - 180.0D) % 360.0D) % 360.0D - 180.0D);
        }
        this.latitude = Math.max(-90.0D, Math.min(90.0D, paramDouble1));
    }

    public final int describeContents() {
        return 0;
    }

    @Override
    public final boolean equals(Object paramObject) {
        if (this == paramObject) {
            return true;
        }
        if (!(paramObject instanceof LatLng)) {
            return false;
        }
        LatLng localLatLng = (LatLng) paramObject;
        return (Double.doubleToLongBits(this.latitude) == Double
                .doubleToLongBits(localLatLng.latitude))
                && (Double.doubleToLongBits(this.longitude) == Double
                .doubleToLongBits(localLatLng.longitude));
    }

    final int getVersionCode() {
        return this.kg;
    }

    @Override
    public final int hashCode() {
        long l1 = Double.doubleToLongBits(this.latitude);
        int i = 31 + (int) (l1 ^ l1 >>> 32);
        long l2 = Double.doubleToLongBits(this.longitude);
        return i * 31 + (int) (l2 ^ l2 >>> 32);
    }

    @Override
    public final String toString() {
        return "lat/lng: (" + this.latitude + "," + this.longitude + ")";
    }

    public Double getLatitude2() {
        return latitude2;
    }

    public void setLatitude2(Double latitude2) {
        this.latitude2 = latitude2;
    }

    public Double getLongitude2() {
        return longitude2;
    }

    public void setLongitude2(Double longitude2) {
        this.longitude2 = longitude2;
    }

}
