package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.Jornada;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RetornoJornadaVO extends RetornoVO {
    private Jornada jornada;
}
