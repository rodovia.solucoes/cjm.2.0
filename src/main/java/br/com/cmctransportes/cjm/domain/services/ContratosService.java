package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.Arquivo;
import br.com.cmctransportes.cjm.domain.entities.Contratos;
import br.com.cmctransportes.cjm.infra.dao.ArquivoDAO;
import br.com.cmctransportes.cjm.infra.dao.ContratosDAO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.*;

@Service
public class ContratosService {
    private EntityManager em;
    private ContratosDAO contratosDAO;
    private ArquivoDAO arquivoDAO;

    public ContratosService(EntityManager em) {
        this.em = em;
        contratosDAO = new ContratosDAO(em);
        arquivoDAO = new ArquivoDAO(em);
    }

    @Transactional()
    public void salvar(Contratos contratos) throws Exception {
        contratos.setDataCadastro(new Date());
        if (contratos.getItem() == 7) {
            List<Item> l = gerarCancelamento(contratos.getProduto());
            for (Item i : l) {
                Contratos c = new Contratos();
                c.setEmpresa(contratos.getEmpresa());
                c.setDataCadastro(contratos.getDataCadastro());
                c.setProduto(contratos.getProduto());
                c.setQuantidade(contratos.getQuantidade());
                c.setTipoSistema(contratos.getTipoSistema());
                c.setResponsavel(i.responsavel);
                c.setItem(i.iten);
                c.setTarefa(i.tarefa);
                c.setRevenda(contratos.getRevenda());
                c.setValorContrato(contratos.getValorContrato());
                c.setValorImplatacao(contratos.getValorImplatacao());
                c.setValorUnidade(contratos.getValorUnidade());
                contratosDAO.save(c);
                gravarArquivos(c);
            }
        }

        if (contratos.getItem() == 8) {
            List<Item> l =  gerarEmTeste();
            for (Item i : l) {
                Contratos c = new Contratos();
                c.setEmpresa(contratos.getEmpresa());
                c.setDataCadastro(contratos.getDataCadastro());
                c.setProduto(contratos.getProduto());
                c.setQuantidade(contratos.getQuantidade());
                c.setTipoSistema(contratos.getTipoSistema());
                c.setResponsavel(i.responsavel);
                c.setItem(i.iten);
                c.setTarefa(i.tarefa);
                c.setRevenda(contratos.getRevenda());
                c.setValorContrato(contratos.getValorContrato());
                c.setValorImplatacao(contratos.getValorImplatacao());
                c.setValorUnidade(contratos.getValorUnidade());
                contratosDAO.save(c);
                gravarArquivos(c);
            }
        }


        if (contratos.getItem() == 3) {
            //inadimplente contrato
            contratosDAO.prepararCancelamento(contratos.getEmpresa());
            Contratos c = new Contratos();
            c.setEmpresa(contratos.getEmpresa());
            c.setDataCadastro(contratos.getDataCadastro());
            c.setProduto(contratos.getProduto());
            c.setQuantidade(contratos.getQuantidade());
            c.setTipoSistema(contratos.getTipoSistema());
            c.setResponsavel("RODOVIA");
            c.setItem(3);
            c.setTarefa("Contrato inadimplente.");
            c.setRevenda(contratos.getRevenda());
            c.setValorContrato(contratos.getValorContrato());
            c.setValorImplatacao(contratos.getValorImplatacao());
            c.setValorUnidade(contratos.getValorUnidade());
            contratosDAO.save(c);
            gravarArquivos(c);
        }

        if (contratos.getItem() == 4) {
            List<Item> l = gerarDev();
            for (Item i : l) {
                Contratos c = new Contratos();
                c.setEmpresa(contratos.getEmpresa());
                c.setDataCadastro(contratos.getDataCadastro());
                c.setProduto(contratos.getProduto());
                c.setQuantidade(contratos.getQuantidade());
                c.setTipoSistema(contratos.getTipoSistema());
                c.setResponsavel(i.responsavel);
                c.setItem(i.iten);
                c.setTarefa(i.tarefa);
                c.setRevenda(contratos.getRevenda());
                c.setValorContrato(contratos.getValorContrato());
                c.setValorImplatacao(contratos.getValorImplatacao());
                c.setValorUnidade(contratos.getValorUnidade());
                contratosDAO.save(c);
                gravarArquivos(c);
            }
        }

        if (contratos.getProduto().equalsIgnoreCase("Via-Sat") && contratos.getItem() == 2) {
            List<Item> l = gerarViaSatItem();
            for (Item i : l) {
                Contratos c = new Contratos();
                c.setEmpresa(contratos.getEmpresa());
                c.setDataCadastro(contratos.getDataCadastro());
                c.setProduto(contratos.getProduto());
                c.setQuantidade(contratos.getQuantidade());
                c.setTipoSistema(contratos.getTipoSistema());
                c.setResponsavel(i.responsavel);
                c.setItem(i.iten);
                c.setTarefa(i.tarefa);
                c.setRevenda(contratos.getRevenda());
                c.setValorContrato(contratos.getValorContrato());
                c.setValorImplatacao(contratos.getValorImplatacao());
                c.setValorUnidade(contratos.getValorUnidade());
                contratosDAO.save(c);
                gravarArquivos(c);
            }
        }

        if (contratos.getProduto().equalsIgnoreCase("Via-M") && contratos.getItem() == 2) {
            List<Item> l = gerarViaMItem();
            for (Item i : l) {
                Contratos c = new Contratos();
                c.setEmpresa(contratos.getEmpresa());
                c.setDataCadastro(contratos.getDataCadastro());
                c.setProduto(contratos.getProduto());
                c.setQuantidade(contratos.getQuantidade());
                c.setTipoSistema(contratos.getTipoSistema());
                c.setResponsavel(i.responsavel);
                c.setItem(i.iten);
                c.setTarefa(i.tarefa);
                c.setRevenda(contratos.getRevenda());
                c.setValorContrato(contratos.getValorContrato());
                c.setValorImplatacao(contratos.getValorImplatacao());
                c.setValorUnidade(contratos.getValorUnidade());
                contratosDAO.save(c);
                gravarArquivos(c);
            }
        }
    }


    public List<Contratos> buscarPorEmpresa(Integer idEmpresa) throws Exception {
        List<Contratos> lista = contratosDAO.buscarPorEmpresa(idEmpresa);
        lista.forEach(c -> {
            int diasExp = 0;
            int diasCorridos = 0;
            int dias = 0;
            int dif = 0;
            boolean isConcluido = false;

            if (Objects.nonNull(c.getExpectativaInicio()) && Objects.nonNull(c.getExpectativaFim())) {
                diasExp = Days.daysBetween(new DateTime(c.getExpectativaInicio()), new DateTime(c.getExpectativaFim())).getDays();
                diasCorridos = Days.daysBetween(new DateTime(c.getExpectativaFim()), new DateTime(new Date())).getDays();
                dif = c.getExpectativaFim().compareTo(new Date());
            }

            if (Objects.isNull(c.getDataInicio()) && Objects.isNull(c.getDataFinal())) {
                c.setStatus("Não defenido");
            } else {

                if (Objects.nonNull(c.getDataFinal())) {
                    dias = Days.daysBetween(new DateTime(c.getDataInicio()), new DateTime(c.getDataFinal())).getDays();
                    if (dias > 0) {
                        c.setDias(dias);
                    }
                    c.setStatus("Concluido");
                    c.setCor("#98FB98");
                    isConcluido = true;
                }



                if (!isConcluido) {
                    if (dif < 0) {
                        c.setStatus("Atrasado");
                        c.setCor("#FF6347");
                        c.setDias(diasExp + diasCorridos);
                    } else {
                        c.setStatus("Em Andamento");
                        c.setCor("#F0E68C");
                        diasCorridos = Days.daysBetween(new DateTime(c.getExpectativaInicio()), new DateTime(new Date())).getDays();
                        c.setDias(diasCorridos);
                    }
                }
            }

            try {
                List<Arquivo> l = arquivoDAO.findByContrato(c.getId());
                c.setListaDeArquivos(l);
            } catch (Exception e) {
            }

            try {
                if (Objects.nonNull(c.getValorUnidade())) {
                    c.setValorContrato(c.getQuantidade() * c.getValorUnidade());
                }
            } catch (Exception e) {
            }

            try {
                if (Objects.nonNull(c.getValorImplatacao())) {
                    c.setValorContrato(c.getQuantidade() * c.getValorImplatacao());
                }
            } catch (Exception e) {
            }
        });
        return lista;
    }

    @Transactional()
    public void editar(List<Contratos> lista) throws Exception {
        lista.forEach(c -> {
            contratosDAO.update(c);
            gravarArquivos(c);
        });
    }

    public Contratos calcularContratos() throws Exception {
        return contratosDAO.calcularContratos();
    }

    public List<Contratos> buscarListaPorItem(Integer item) throws Exception {
        List<Contratos> lista = contratosDAO.buscarListaPorItem(item);
        if (item == 2) {
            //buscar os itens que sao de id 6 tambem
            List<Contratos> listaDois = contratosDAO.buscarListaPorItem(6);
            lista.addAll(listaDois);
        }
        lista.forEach(c -> {
            int diasExp = 0;
            int diasCorridos = 0;
            int dias = 0;
            int dif = 0;
            boolean isConcluido = false;

            if (Objects.nonNull(c.getExpectativaInicio()) && Objects.nonNull(c.getExpectativaFim())) {
                dif = c.getExpectativaFim().compareTo(new Date());
            }

            if (Objects.nonNull(c.getDataFinal())) {
                dias = Days.daysBetween(new DateTime(c.getDataInicio()), new DateTime(new Date())).getDays();
                if (dias > 0) {
                    c.setDias(dias);
                }
                c.setStatus("Concluido");
                c.setCor("#98FB98");
                isConcluido = true;
            } else {
                dias = Days.daysBetween(new DateTime(c.getDataInicio()), new DateTime(new Date())).getDays();
                if (dias > 0) {
                    c.setDias(dias);
                }
            }

            if (!isConcluido) {

                if (dif < 0) {
                    c.setStatus("Atrasado");
                    c.setCor("#FF6347");
                } else {
                    c.setStatus("Em Andamento");
                    c.setCor("#F0E68C");
                }
            }

        });


        Collections.sort(lista, new Comparator() {
            @Override
            public int compare(Object c1, Object c2) {
                //use instanceof to verify the references are indeed of the type in question
                return ((Contratos) c1).getDataInicio()
                        .compareTo(((Contratos) c2).getDataInicio());
            }
        });
        return lista;
    }

    private void gravarArquivos(Contratos contratos) {
        try {
            if (Objects.nonNull(contratos.getListaDeArquivos()) && !contratos.getListaDeArquivos().isEmpty()) {
                List<Arquivo> lista = arquivoDAO.findByContrato(contratos.getId());
                for (Arquivo arquivo : contratos.getListaDeArquivos()) {
                    Arquivo a = lista.stream()
                            .filter(aa -> aa.getName().equalsIgnoreCase(arquivo.getName()))
                            .findAny()
                            .orElse(null);
                    if (Objects.isNull(a)) {
                        arquivo.setContrato(contratos.getId());
                        arquivoDAO.saveSemTransactional(arquivo);
                    }
                }
            }
        } catch (Exception e) {
            LogSistema.logError("gravarArquivos", e);
        }
    }

    private List<Item> gerarViaSatItem() {
        List<Item> items = new ArrayList<>();
        Item i = new Item();
        i.iten = 2;
        i.tarefa = "Assinatura do contrato";
        i.responsavel = "RODOVIA";
        items.add(i);

        i = new Item();
        i.iten = 2;
        i.tarefa = "Pedido dos rastreadores ao fornecedor";
        i.responsavel = "FORNECEDOR";
        items.add(i);

        i = new Item();
        i.iten = 2;
        i.tarefa = "Preparação dos rastreadores";
        i.responsavel = "FORNECEDOR";
        items.add(i);

        i = new Item();
        i.iten = 2;
        i.tarefa = "Envio pelos correios";
        i.responsavel = "FORNECEDOR";
        items.add(i);

        i = new Item();
        i.iten = 2;
        i.tarefa = "Preparação dos rastreadores";
        i.responsavel = "RODOVIA";
        items.add(i);

        i = new Item();
        i.iten = 2;
        i.tarefa = "Envio ao Cliente";
        i.responsavel = "RODOVIA";
        items.add(i);

        i = new Item();
        i.iten = 2;
        i.tarefa = "Recebimento dos equipamentos pelo Cliente";
        i.responsavel = "CLIENTE";
        items.add(i);

        i = new Item();
        i.iten = 2;
        i.tarefa = "Instalação dos equipamentos";
        i.responsavel = "RODOVIA";
        items.add(i);


        i = new Item();
        i.iten = 1;
        i.tarefa = "Faturamento";
        i.responsavel = "RODOVIA";
        items.add(i);

        i = new Item();
        i.iten = 6;
        i.tarefa = "Treinamento";
        i.responsavel = "RODOVIA";
        items.add(i);

        return items;
    }


    private List<Item> gerarCancelamento(String tipo) {
        List<Item> items = new ArrayList<>();
        Item i = new Item();
        i.iten = 7;
        i.tarefa = "Pedido de Cancelamento";
        i.responsavel = "RODOVIA";
        items.add(i);
        if (tipo.equalsIgnoreCase("Via-Sat")) {
            i = new Item();
            i.iten = 7;
            i.tarefa = "Desistalação dos Rastreadores";
            i.responsavel = "RODOVIA";
            items.add(i);
        }
        if (tipo.equalsIgnoreCase("Via-M")) {
            i = new Item();
            i.iten = 7;
            i.tarefa = "Suspensão da Assinatura";
            i.responsavel = "RODOVIA";
            items.add(i);
        }
        i = new Item();
        i.iten = 7;
        i.tarefa = "Cobrança da Multa";
        i.responsavel = "RODOVIA";
        items.add(i);
        return items;
    }

    private List<Item> gerarEmTeste() {
        List<Item> items = new ArrayList<>();
        Item i = new Item();
        i.iten = 8;
        i.tarefa = "Assinatura do Contrato";
        i.responsavel = "RODOVIA";
        items.add(i);

        i = new Item();
        i.iten = 8;
        i.tarefa = "Inicio dos testes";
        i.responsavel = "CLIENTE";
        items.add(i);

        i = new Item();
        i.iten = 8;
        i.tarefa = "Fim dos Testes";
        i.responsavel = "RODOVIA";
        items.add(i);
        return items;
    }


    private List<Item> gerarDev() {
        List<Item> items = new ArrayList<>();
        Item i = new Item();
        i.iten = 4;
        i.tarefa = "Assinatura do contrato";
        i.responsavel = "RODOVIA";
        items.add(i);

        i = new Item();
        i.iten = 4;
        i.tarefa = "Levantamento de Requisito";
        i.responsavel = "RODOVIA";
        items.add(i);

        i = new Item();
        i.iten = 4;
        i.tarefa = "Aprovacao do requisito";
        i.responsavel = "CLIENTE";
        items.add(i);

        i = new Item();
        i.iten = 4;
        i.tarefa = "Desenvolvimento";
        i.responsavel = "RODOVIA";
        items.add(i);

        i = new Item();
        i.iten = 4;
        i.tarefa = "Testes";
        i.responsavel = "RODOVIA";
        items.add(i);

        i = new Item();
        i.iten = 4;
        i.tarefa = "Publicar em produção";
        i.responsavel = "RODOVIA";
        items.add(i);

        return items;
    }

    private List<Item> gerarViaMItem() {
        List<Item> items = new ArrayList<>();
        Item i = new Item();
        i.iten = 2;
        i.tarefa = "Assinatura do contrato";
        i.responsavel = "RODOVIA";
        items.add(i);

        i = new Item();
        i.iten = 2;
        i.tarefa = "Cadastro da empresa e unidades";
        i.responsavel = "RODOVIA";
        items.add(i);

        i = new Item();
        i.iten = 2;
        i.tarefa = "Cadastro e configurações da empresa cliente.";
        i.responsavel = "RODOVIA";
        items.add(i);

        i = new Item();
        i.iten = 6;
        i.tarefa = "Controle de Jornada";
        i.responsavel = "RODOVIA";
        items.add(i);

        i = new Item();
        i.iten = 6;
        i.tarefa = "Departamento De Pessoal";
        i.responsavel = "RODOVIA";
        items.add(i);

        i = new Item();
        i.iten = 6;
        i.tarefa = "Motoristas";
        i.responsavel = "RODOVIA";
        items.add(i);

        i = new Item();
        i.iten = 2;
        i.tarefa = "1-Semana de acompanhamento";
        i.responsavel = "FORNECEDOR";
        items.add(i);

        i = new Item();
        i.iten = 2;
        i.tarefa = "2-Semana de acompanhamento";
        i.responsavel = "FORNECEDOR";
        items.add(i);

        i = new Item();
        i.iten = 2;
        i.tarefa = "3-Semana de acompanhamento";
        i.responsavel = "FORNECEDOR";
        items.add(i);

        i = new Item();
        i.iten = 2;
        i.tarefa = "4-Semana de acompanhamento";
        i.responsavel = "FORNECEDOR";
        items.add(i);


        i = new Item();
        i.iten = 1;
        i.tarefa = "Faturamento";
        i.responsavel = "RODOVIA";
        items.add(i);

        i = new Item();
        i.iten = 2;
        i.tarefa = "Conclusão do Projeto";
        i.responsavel = "RODOVIA";
        items.add(i);

        return items;
    }

    class Item {
        int iten;
        String tarefa;
        String responsavel;
    }
}
