package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class DadosDisponibilidadeFisicaVO implements Serializable {
    private String placaData;
    private Integer totalLigado;
    private Integer totalDesligado;
    private Integer totalManutencao;
}
