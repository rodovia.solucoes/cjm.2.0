package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class AlertaViamVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private Long dataAlerta;
    private Long horaDoEvento;
    private Integer tipoDoEvento;
    private Integer empresa;
    private Integer operador;
    private Integer jornada;
    private Integer status;
    private Integer enviado;
    private Long horaDanotificacao;
    private Integer quantidadeCliqueOk;
    private Integer quantidadeCliqueCancelar;

}
