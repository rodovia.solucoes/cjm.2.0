package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.Empresas;
import br.com.cmctransportes.cjm.domain.entities.Enderecos;
import br.com.cmctransportes.cjm.domain.entities.vo.*;
import br.com.cmctransportes.cjm.infra.dao.LocalDAO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.proxy.LocalTrajetto;
import br.com.cmctransportes.cjm.proxy.MapaTrajetto;
import br.com.cmctransportes.cjm.proxy.modelo.Coordenada;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LocalService {

    @Autowired
    private EmpresasService empresasService;

    public RetornoLocalVO cadastrarLocal(LocalVO localVO) throws Exception{
        try {

            if(localVO.getCliente() == null){
                return new RetornoLocalVO(-1, "Empresa é obrigatorio.");
            }
            if(localVO.getCliente().getIdClienteTrajetto() == null || localVO.getCliente().getIdClienteTrajetto() == 0){
                Empresas empresas = empresasService.getById(localVO.getCliente().getId());
                localVO.getCliente().setIdClienteTrajetto(empresas.getIdClienteTrajetto());
            }

            return LocalTrajetto.getInstance().cadastrarLocal(localVO);
        }catch (Exception e){
            LogSistema.logError("cadastrarLocal", e);
            throw new Exception(e);
        }
    }


    public RetornoLocalVO cadastrarLocalPostoAbastecimento(LocalVO localVO) throws Exception{
        try {

            if(localVO.getCliente() == null){
                return new RetornoLocalVO(-1, "Empresa é obrigatorio.");
            }
            if(localVO.getCliente().getIdClienteTrajetto() == null || localVO.getCliente().getIdClienteTrajetto() == 0){
                Empresas empresas = empresasService.getById(localVO.getCliente().getId());
                localVO.getCliente().setIdClienteTrajetto(empresas.getIdClienteTrajetto());
            }

            return LocalTrajetto.getInstance().cadastrarLocalPostoAbastecimento(localVO);
        }catch (Exception e){
            LogSistema.logError("cadastrarLocalPostoAbastecimento", e);
            throw new Exception(e);
        }
    }

    public RetornoLocalVO cadastrarLocalOficina(LocalVO localVO) throws Exception{
        try {

            if(localVO.getCliente() == null){
                return new RetornoLocalVO(-1, "Empresa é obrigatorio.");
            }
            if(localVO.getCliente().getIdClienteTrajetto() == null || localVO.getCliente().getIdClienteTrajetto() == 0){
                Empresas empresas = empresasService.getById(localVO.getCliente().getId());
                localVO.getCliente().setIdClienteTrajetto(empresas.getIdClienteTrajetto());
            }

            return LocalTrajetto.getInstance().cadastrarLocalOficina(localVO);
        }catch (Exception e){
            LogSistema.logError("cadastrarLocalOficina", e);
            throw new Exception(e);
        }
    }

    public RetornoLocalVO editarLocal(LocalVO localVO) throws Exception{
        try {

            if(localVO.getCliente() == null){
                return new RetornoLocalVO(-1, "Empresa é obrigatorio.");
            }
            if(localVO.getCliente().getIdClienteTrajetto() == null || localVO.getCliente().getIdClienteTrajetto() == 0){
                Empresas empresas = empresasService.getById(localVO.getCliente().getId());
                localVO.getCliente().setIdClienteTrajetto(empresas.getIdClienteTrajetto());
            }

            return LocalTrajetto.getInstance().editarLocal(localVO);
        }catch (Exception e){
            LogSistema.logError("editarLocal", e);
            throw new Exception(e);
        }
    }


    public RetornoLocalVO excluirLocal(LocalVO localVO) throws Exception{
        try {
            return LocalTrajetto.getInstance().excluirLocal(localVO);
        }catch (Exception e){
            LogSistema.logError("editarLocal", e);
            throw new Exception(e);
        }
    }


    public RetornoLocalVO buscarLocalPeloCodigo(Integer id) throws Exception{
        try {
            return LocalTrajetto.getInstance().buscarLocalPeloCodigo(id);
        }catch (Exception e){
            LogSistema.logError("buscarLocalPeloCodigo", e);
            throw new Exception(e);
        }
    }

    public RetornoLocalVO buscarListaLocal(EmpresaVO empresaVO) throws Exception{
        try {
            if(empresaVO == null){
                return new RetornoLocalVO(-1, "Empresa é obrigatorio.");
            }
            if(empresaVO.getIdClienteTrajetto() == null || empresaVO.getIdClienteTrajetto() == 0){
                Empresas empresas = empresasService.getById(empresaVO.getId());
                empresaVO.setIdClienteTrajetto(empresas.getIdClienteTrajetto());
            }
            return LocalTrajetto.getInstance().buscarListaLocal(empresaVO);
        }catch (Exception e){
            LogSistema.logError("buscarListaLocal", e);
            throw new Exception(e);
        }
    }

    public RetornoLocalVO listaDeLocalPostoAbastecimento(EmpresaVO empresaVO) throws Exception{
        try {
            if(empresaVO == null){
                return new RetornoLocalVO(-1, "Empresa é obrigatorio.");
            }
            if(empresaVO.getIdClienteTrajetto() == null || empresaVO.getIdClienteTrajetto() == 0){
                Empresas empresas = empresasService.getById(empresaVO.getId());
                empresaVO.setIdClienteTrajetto(empresas.getIdClienteTrajetto());
            }
            return LocalTrajetto.getInstance().listaDeLocalPostoAbastecimento(empresaVO);
        }catch (Exception e){
            LogSistema.logError("listaDeLocalPostoAbastecimento", e);
            throw new Exception(e);
        }
    }

    public RetornoLocalVO listaDeLocalPostoOficina(EmpresaVO empresaVO) throws Exception{
        try {
            if(empresaVO == null){
                return new RetornoLocalVO(-1, "Empresa é obrigatorio.");
            }
            if(empresaVO.getIdClienteTrajetto() == null || empresaVO.getIdClienteTrajetto() == 0){
                Empresas empresas = empresasService.getById(empresaVO.getId());
                empresaVO.setIdClienteTrajetto(empresas.getIdClienteTrajetto());
            }
            return LocalTrajetto.getInstance().listaDeLocalPostoOficina(empresaVO);
        }catch (Exception e){
            LogSistema.logError("listaDeLocalPostoOficina", e);
            throw new Exception(e);
        }
    }

    public RetornoLocalVO buscarListaLocalExibirMapa(EmpresaVO empresaVO) throws Exception{
        try {
            if(empresaVO == null){
                return new RetornoLocalVO(-1, "Empresa é obrigatorio.");
            }
            if(empresaVO.getIdClienteTrajetto() == null || empresaVO.getIdClienteTrajetto() == 0){
                Empresas empresas = empresasService.getById(empresaVO.getId());
                empresaVO.setIdClienteTrajetto(empresas.getIdClienteTrajetto());
            }
            return LocalTrajetto.getInstance().buscarListaLocalExibirMapa(empresaVO);
        }catch (Exception e){
            LogSistema.logError("buscarListaLocalExibirMapa", e);
            throw new Exception(e);
        }
    }

    public RetornoLocalVO buscarTodosOsLocais(EmpresaVO empresaVO) throws Exception{
        try {
            if(empresaVO == null){
                return new RetornoLocalVO(-1, "Empresa é obrigatorio.");
            }
            if(empresaVO.getIdClienteTrajetto() == null || empresaVO.getIdClienteTrajetto() == 0){
                Empresas empresas = empresasService.getById(empresaVO.getId());
                empresaVO.setIdClienteTrajetto(empresas.getIdClienteTrajetto());
            }
            return LocalTrajetto.getInstance().buscarTodosOsLocais(empresaVO);
        }catch (Exception e){
            LogSistema.logError("buscarTodosOsLocais", e);
            throw new Exception(e);
        }
    }


    public RetornoMapaVO buscarLatLngPeloEndereco(Enderecos enderecos) throws Exception {
        try {
            return MapaTrajetto.getInstance().buscarLatLngPeloEndereco(enderecos);
        } catch (Exception e) {
            LogSistema.logError("buscarLatLngPeloEndereco", e);
            throw new Exception(e);
        }
    }


    public RetornoLocalVO buscarTodosOsLocaisSqlNativo(EmpresaVO empresaVO) throws Exception{
        RetornoLocalVO retornoLocalVO = new RetornoLocalVO();
        try {
            if(empresaVO == null){
                return new RetornoLocalVO(-1, "Empresa é obrigatorio.");
            }
            if(empresaVO.getIdClienteTrajetto() == null || empresaVO.getIdClienteTrajetto() == 0){
                Empresas empresas = empresasService.getById(empresaVO.getId());
                empresaVO.setIdClienteTrajetto(empresas.getIdClienteTrajetto());
            }

            LocalDAO localDAO = new LocalDAO();
            List<LocalVO> listaLocal = localDAO.buscarLocaisExibicao(empresaVO.getIdClienteTrajetto());
            for(LocalVO localVO : listaLocal){
                List<CoordenadaVO> lista = localDAO.buscarListaCoordenada(localVO);
                localVO.setListaDeCoordenadas(lista);
            }

            retornoLocalVO.setListaDeLocal(listaLocal);
            retornoLocalVO.setCodigoRetorno(1);
            retornoLocalVO.setMensagem("Lista carregada com suscesso.");
        }catch (Exception e){
            LogSistema.logError("buscarTodosOsLocaisSqlNativo", e);
            retornoLocalVO.setCodigoRetorno(-1);
            retornoLocalVO.setMensagem(e.getMessage());
        }
        return retornoLocalVO;
    }

}
