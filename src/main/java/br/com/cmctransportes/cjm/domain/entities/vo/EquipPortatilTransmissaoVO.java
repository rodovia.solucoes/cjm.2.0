package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
@Getter
@Setter
public class EquipPortatilTransmissaoVO implements Serializable {

    private EquipPortatilMinimaVO equipPortatil;
    private TransmissaoMinimoVO transmissao;
    private LocalMinimoVO local;
}
