package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class RetornoVeiculoVO extends RetornoVO {

    @Getter
    @Setter
    private VeiculoVO veiculo;
    @Getter
    @Setter
    private List<VeiculoVO> listaDeVeiculos;

    public RetornoVeiculoVO() {

    }

    public RetornoVeiculoVO(Integer codigoRetorno, String mensagem) {
        super(codigoRetorno, mensagem);
    }

}
