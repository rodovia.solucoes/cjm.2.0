package br.com.cmctransportes.cjm.domain.entities.enums;

/**
 * @author William Leite
 */
public enum LOCATION_STATUS {
    ACTIVE,
    INACTIVE
}