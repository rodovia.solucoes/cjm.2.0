package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.Turnos;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
public class TurnoVO {
    private long tempoDiario01;
    private long tempoDiario02;
    private long tempoDiario03;
    private long tempoDiario04;
    private long tempoDiario05;
    private long tempoDiario06;
    private long tempoDiario07;
    private long tempoAlimentacao01;
    private long tempoAlimentacao02;
    private long tempoAlimentacao03;
    private long tempoAlimentacao04;
    private long tempoAlimentacao05;
    private long tempoAlimentacao06;
    private long tempoAlimentacao07;


    public TurnoVO(){

    }

    public TurnoVO(Turnos turnos){
        if(Objects.nonNull(turnos.getTempoAlimentacao01())){
            this.tempoAlimentacao01 = turnos.getTempoAlimentacao01();
        }else{
            this.tempoAlimentacao01 = 3600000;
        }
        if(Objects.nonNull(turnos.getTempoAlimentacao02())){
            this.tempoAlimentacao02 = turnos.getTempoAlimentacao02();
        }else{
            this.tempoAlimentacao02 = 3600000;
        }
        if(Objects.nonNull(turnos.getTempoAlimentacao03())){
            this.tempoAlimentacao03 = turnos.getTempoAlimentacao03();
        }else{
            this.tempoAlimentacao03 = 3600000;
        }
        if(Objects.nonNull(turnos.getTempoAlimentacao04())){
            this.tempoAlimentacao04 = turnos.getTempoAlimentacao04();
        }else{
            this.tempoAlimentacao04 = 3600000;
        }
        if(Objects.nonNull(turnos.getTempoAlimentacao05())){
            this.tempoAlimentacao05 = turnos.getTempoAlimentacao05();
        }else{
            this.tempoAlimentacao05 = 3600000;
        }
        if(Objects.nonNull(turnos.getTempoAlimentacao06())){
            this.tempoAlimentacao06 = turnos.getTempoAlimentacao06();
        }else{
            this.tempoAlimentacao06 = 3600000;
        }
        if(Objects.nonNull(turnos.getTempoAlimentacao07())){
            this.tempoAlimentacao07 = turnos.getTempoAlimentacao07();
        }else{
            this.tempoAlimentacao07 = 3600000;
        }

        if(Objects.nonNull(turnos.getTempoDiario01())){
            this.tempoDiario01 = turnos.getTempoDiario01();
        }else{
            this.tempoDiario01 = 28800000;
        }

        if(Objects.nonNull(turnos.getTempoDiario02())){
            this.tempoDiario02 = turnos.getTempoDiario02();
        }else{
            this.tempoDiario02 = 28800000;
        }

        if(Objects.nonNull(turnos.getTempoDiario03())){
            this.tempoDiario03 = turnos.getTempoDiario03();
        }else{
            this.tempoDiario03 = 28800000;
        }

        if(Objects.nonNull(turnos.getTempoDiario04())){
            this.tempoDiario04 = turnos.getTempoDiario04();
        }else{
            this.tempoDiario04 = 28800000;
        }

        if(Objects.nonNull(turnos.getTempoDiario05())){
            this.tempoDiario05 = turnos.getTempoDiario05();
        }else{
            this.tempoDiario05 = 28800000;
        }

        if(Objects.nonNull(turnos.getTempoDiario06())){
            this.tempoDiario06 = turnos.getTempoDiario06();
        }else{
            this.tempoDiario06 = 28800000;
        }

        if(Objects.nonNull(turnos.getTempoDiario07())){
            this.tempoDiario07 = turnos.getTempoDiario07();
        }else{
            this.tempoDiario07 = 28800000;
        }

    }


}
