/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cmctransportes.cjm.domain.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

/**
 * @author analista.ti
 */
@Entity
@Table(name = "estados_jornada")
@Getter
@Setter
@NamedQueries({
        @NamedQuery(name = "EstadosJornada.findAll", query = "SELECT e FROM EstadosJornada e"),
        @NamedQuery(name = "EstadosJornada.findById", query = "SELECT e FROM EstadosJornada e WHERE e.id = :id"),
        @NamedQuery(name = "EstadosJornada.findByDescricao", query = "SELECT e FROM EstadosJornada e WHERE e.descricao = :descricao")})
public class EstadosJornada implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "ESTADOS_JORNADA_ID_SEQ", sequenceName = "ESTADOS_JORNADA_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ESTADOS_JORNADA_ID_SEQ")
    @Column(name = "id")
    private Integer id;

    @Column(name = "descricao", length = 25, nullable = false)
    private String descricao;

    @Column(name = "color", length = 7)
    private String color;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "estado_permite_evento", joinColumns = {
            @JoinColumn(name = "estados_jornada_id", referencedColumnName = "id")}, inverseJoinColumns = {
            @JoinColumn(name = "eventos_jornada_id", referencedColumnName = "id")})
    @JsonManagedReference("objGeraEstado")
    private Collection<EventosJornada> eventosPermitidos;

    @JoinColumn(name = "empresa_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @JsonIgnore
    private Empresas empresaId;

    @JoinColumn(name = "tipos_estado_jornada_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TiposEstadoJornada tiposEstadoJornadaId;

    public EstadosJornada() {
    }

    public EstadosJornada(Integer id) {
        this.id = id;
    }
}