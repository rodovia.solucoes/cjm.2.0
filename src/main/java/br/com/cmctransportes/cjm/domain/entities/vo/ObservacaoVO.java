package br.com.cmctransportes.cjm.domain.entities.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ObservacaoVO  implements Serializable {

    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private Integer id;
    @Getter
    @Setter
    private Date dataDoCadastro;
    @Getter
    @Setter
    private VeiculoVO veiculo;
    @Getter
    @Setter
    private String texto;
    @Getter
    @Setter
    private MotoristaVO motorista;
    @Getter
    @Setter
    private String tipo;
    @Getter
    @Setter
    private String dataFormatada;
    @Getter
    @Setter
    private EquipamentoPortatilVO equipamentoPortatil;

}
