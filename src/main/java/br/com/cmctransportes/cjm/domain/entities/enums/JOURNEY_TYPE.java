package br.com.cmctransportes.cjm.domain.entities.enums;

import lombok.Getter;

import java.util.Arrays;
import java.util.Objects;

/**
 * Tipos de Jornada: definido que positivo é inicio negativo é fim
 *
 * @author Producao
 */
public enum JOURNEY_TYPE {
    JORNADA(new int[]{1}),
    ABONO(new int[]{17}),
    FERIAS(new int[]{18}),
    FALTA(new int[]{19}),
    DSR(new int[]{20, 26}),
    FOLGA(new int[]{21}),
    FERIADO(new int[]{22});

    @Getter
    public final int[] ids;

    private JOURNEY_TYPE(final int[] ids) {
        this.ids = ids;
    }

    /**
     * Based on the ID passed as parameter, returns the corresponding JOURNEY_TYPE or null if not found.
     *
     * @param parId "TipoEvento"'s ID
     * @return The corresponding JOURNEY_TYPE or null if not found.
     */
    public static JOURNEY_TYPE getById(int parId) {
        return Arrays.stream(JOURNEY_TYPE.values()).filter(t -> Arrays.stream(t.getIds()).anyMatch(id -> Objects.equals(id, parId))).findFirst().orElse(null);
    }
}
