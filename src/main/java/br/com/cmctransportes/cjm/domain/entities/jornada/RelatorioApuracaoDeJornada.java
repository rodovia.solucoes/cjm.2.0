package br.com.cmctransportes.cjm.domain.entities.jornada;

import br.com.cmctransportes.cjm.constants.Constantes;
import br.com.cmctransportes.cjm.domain.entities.*;
import br.com.cmctransportes.cjm.domain.entities.enums.EVENT_TYPE;
import br.com.cmctransportes.cjm.domain.entities.enums.JOURNEY_TYPE;
import br.com.cmctransportes.cjm.domain.entities.vo.NonconformityVO;
import br.com.cmctransportes.cjm.domain.services.EventosJornadaService;
import br.com.cmctransportes.cjm.infra.dao.ToleranciasDAO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.services.JourneyCalculusService;
import br.com.cmctransportes.cjm.services.nonconformity.InterjourneyNonconfomityService;
import br.com.cmctransportes.cjm.services.nonconformity.PaidWeeklyRestNonconformityService;
import br.com.cmctransportes.cjm.utils.TimeHelper;
import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.joda.time.Seconds;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author analista.ti
 */
public class RelatorioApuracaoDeJornada extends RelatorioDeJornada {

    private ArrayList<ApuracaoDeJornada> registroJornada;// = new ArrayList<ApuracaoDeJornada>();
    private List<ResumoJornada> listaDeResumoDeJornada;
    private List<DetalheDaJornada> listaDetalheDaJornada;
    private boolean nonconformity = false;

    public RelatorioApuracaoDeJornada() {
        this.registroJornada = new ArrayList<>(0);
    }

    public RelatorioApuracaoDeJornada(Integer mesReferencia) {
        super(mesReferencia);
    }

    public RelatorioApuracaoDeJornada(Integer mesReferencia, List<Jornada> listaJornadas, JourneyCalculusService calculusService,
                                      PaidWeeklyRestNonconformityService paidWeeklyRestNonconformityService,
                                      InterjourneyNonconfomityService interjourneyNonconfomityService,
                                      EventosJornadaService eventosJornadaService) {
        super(mesReferencia, listaJornadas, calculusService, paidWeeklyRestNonconformityService, interjourneyNonconfomityService, eventosJornadaService);
    }

    public RelatorioApuracaoDeJornada(Date inicio, Date fim) {
        super(inicio, fim);
    }

    public RelatorioApuracaoDeJornada(Date inicio, Date fim, boolean nonconformity) {
        super(inicio, fim);
        this.nonconformity = nonconformity;
    }


    private Jornada ultimaJornada;

    public void setJornadas(List<Jornada> relatorioDeJornada, JourneyCalculusService calculus,
                            PaidWeeklyRestNonconformityService paidWeeklyRestNonconformityService,
                            InterjourneyNonconfomityService interjourneyNonconfomityService,
                            EventosJornadaService eventosJornadaService) {
        super.setJornadas(relatorioDeJornada, calculus, paidWeeklyRestNonconformityService, interjourneyNonconfomityService, eventosJornadaService);

        Jornada previousJourney = null;

        relatorioDeJornada.stream()
                .forEach(j -> {
                    calculus.processEvents(j, ultimaJornada, false);
                    ultimaJornada = j;
                });

        relatorioDeJornada = relatorioDeJornada.stream().sorted(Comparator.nullsLast(
                (j1, j2) -> j1.getJornadaInicio().compareTo(j2.getJornadaInicio())
        )).collect(Collectors.toList());

        // First journey
        Date start = relatorioDeJornada.get(0).getJornadaInicio();
        // Lasr journey
        Date end = relatorioDeJornada.get(relatorioDeJornada.size() - 1).getJornadaFim();

        List<NonconformityVO> dsrNonconformities = paidWeeklyRestNonconformityService.process(start, end, relatorioDeJornada, false);
        List<NonconformityVO> interjourneyNonconformities = interjourneyNonconfomityService.process(start, end, relatorioDeJornada, false);

        // ao chegar aqui, já  tem uma jornada por dia
        registroJornada = new ArrayList<>(0);
        Map<UUID, UUID> map = new HashMap<>();
        Jornada ultimaJornada = null;
        for (Jornada jnd : relatorioDeJornada) {
            ApuracaoDeJornada adj = new ApuracaoDeJornada();
            if (this.nonconformity) {
                List<NonconformityVO> journeyDsrNonconformity = new ArrayList<>(0);
                List<NonconformityVO> journeyInterjourneyNonconformity = new ArrayList<>(0);

                if (!dsrNonconformities.isEmpty()) {
                    // We compare the end of the Nonconformity so that the INVALID_DSR_COUNT is treated at the end of the month
                    journeyDsrNonconformity = dsrNonconformities.stream().filter(n -> TimeHelper.isSameDay(jnd.getJornadaInicio(), n.getEndDate())).collect(Collectors.toList());
                }
                if (!interjourneyNonconformities.isEmpty()) {
                    for (NonconformityVO no : interjourneyNonconformities) {
                        if (TimeHelper.isSameDay(jnd.getJornadaInicio(), no.getEndDate())) {
                            if (!map.containsKey(no.getId())) {//Referente ao Mantis 158
                                journeyInterjourneyNonconformity.add(no);
                                map.put(no.getId(), no.getId());
                            }
                        }
                    }
                }

                adj.setDsrNonconformities(journeyDsrNonconformity);
                adj.setInterjourneyNonconformities(journeyInterjourneyNonconformity);
            }

            adj.processa(jnd, eventosJornadaService);

            try {
                if (jnd.getMotoristasId().getTurnosId().getTipo() == 0) {
                    //PARA SANAR O MANTIS 218
                    //Quando o turno é 12 x 36 acrecent novamente a hora de almoço
                    Evento eventoInicioRefeicao = null;
                    for (Evento evento : jnd.getEventos()) {
                        if (evento.getTipoEvento().getId() == 14) {
                            eventoInicioRefeicao = evento;
                        }
                        if (evento.getTipoEvento().getId() == -14) {
                            long mili = evento.getInstanteEvento().getTime() - eventoInicioRefeicao.getInstanteEvento().getTime();
                            if (mili > 3600000) {
                                //buscar tolerancia
                                long tolerancia = new ToleranciasDAO(null).buscarToleranciaAlmoco(jnd.getEmpresaId().getId());
                                if ((mili - 3600000) > tolerancia) {
                                    mili = mili - tolerancia;
                                }
                            }

                            if (Objects.nonNull(jnd.getAdicionalNoturno()) && jnd.getAdicionalNoturno() > 0) {
                                if (jnd.getHorasExtrasNoturnas() > 0) {
                                    adj.setHorasExtrasNoturnas(jnd.getHorasExtrasNoturnas() + mili);
                                }
                                if (jnd.getTrabalhado() > 43200000 && jnd.getHorasExtrasNoturnas() == 0) {
                                    long i = jnd.getTrabalhado() - 43200000;
                                    adj.setHorasExtrasNoturnas(jnd.getTrabalhado() - 43200000);
                                }
                            } else {
                                if (jnd.getHorasExtrasDiurnas() > 0) {
                                    adj.setHorasExtrasDiurnas(jnd.getHorasExtrasDiurnas() + mili);
                                }
                                if (jnd.getTrabalhado() > 43200000 && jnd.getHorasExtrasDiurnas() == 0) {
                                    adj.setHorasExtrasDiurnas(jnd.getTrabalhado() - 43200000);
                                }
                            }

                            break;
                        }
                    }
                }
            } catch (Exception e) {
            }

            //Tratar interjornada no mesmo dia
            tratarInterjornadaMesmoDia(registroJornada, ultimaJornada, jnd);
            removerInterjornadasRepetidas(registroJornada);
            tratarHorasExtrasNoDomingo(jnd, adj);
            tratarQuebraInterjornadaViaParametro(registroJornada, ultimaJornada, jnd);
            registroJornada.add(adj);

            if (adj.getTipoJornada() == null) {
                previousJourney = jnd;
            } else if (Objects.equals(adj.getTipoJornada(), EVENT_TYPE.DSR.getDescription())) {
                Optional<Evento> dsrStart = jnd.getEventos().stream().filter(e -> e.getTipoEvento().getId() == EVENT_TYPE.DSR.getStartCode()).findAny();
                if (dsrStart.isPresent()) {
                    List<Date> dates = TimeHelper.getDatesBetween(dsrStart.get().getInstanteEvento(), jnd.getJornadaFim());
                    dates.remove(adj.getData());

                    dates.stream().forEach(d -> {
                        ApuracaoDeJornada clone = adj.clone();
                        clone.setData(d);
                        registroJornada.add(clone);
                    });
                }
            }

            ultimaJornada = jnd;


        }
    }

    public ArrayList<ApuracaoDeJornada> getLinhas() {
        return registroJornada;
    }

    public List<ResumoJornada> getListaDeResumoDeJornada() {
        return listaDeResumoDeJornada;
    }

    public void setListaDeResumoDeJornada(List<ResumoJornada> listaDeResumoDeJornada) {
        this.listaDeResumoDeJornada = listaDeResumoDeJornada;
    }


    public List<DetalheDaJornada> getListaDetalheDaJornada() {
        return listaDetalheDaJornada;
    }

    public void setListaDetalheDaJornada(List<DetalheDaJornada> listaDetalheDaJornada) {
        this.listaDetalheDaJornada = listaDetalheDaJornada;
    }

    private void tratarInterjornadaMesmoDia(ArrayList<ApuracaoDeJornada> registroJornada, Jornada ultimaJornada, Jornada jnd) {
        try {
            if (!registroJornada.isEmpty() && Objects.nonNull(ultimaJornada)) {
                //Pegar a ultima
                if (ultimaJornada.getType() == JOURNEY_TYPE.JORNADA && jnd.getType() == JOURNEY_TYPE.JORNADA) {
                    boolean isMesmoDia = TimeHelper.isSameDay(ultimaJornada.getJornadaFim(), jnd.getJornadaInicio());
                    if (isMesmoDia) {
                        int tempoEntreAsJornadas = Seconds.secondsBetween(new DateTime(ultimaJornada.getJornadaFim()), new DateTime(jnd.getJornadaInicio())).getSeconds();
                        if (tempoEntreAsJornadas < Constantes.TEMPO_SECONDOS_INTERJORNADA) {
                            int minutosFaltantes = Constantes.TEMPO_SECONDOS_INTERJORNADA - tempoEntreAsJornadas;
                            long t = minutosFaltantes * 1000;
                            ApuracaoDeJornada ultimaApuracaoDeJornada = registroJornada.get(registroJornada.size() - 1);
                            ultimaApuracaoDeJornada.setHorasExtrasInterJornadas(t);
                            ultimaApuracaoDeJornada.setHorasExtrasNoturnas(t);
                            ultimaApuracaoDeJornada.setJornadaMesmoDia(true);
                        }
                    }
                }
            }
        } catch (Exception e) {
            LogSistema.logError("tratarInterjornadaMesmoDia", e);
        }
    }


    private void tratarQuebraInterjornadaViaParametro(ArrayList<ApuracaoDeJornada> registroJornada, Jornada ultimaJornada, Jornada jnd) {
        try {
            if (!registroJornada.isEmpty() && Objects.nonNull(ultimaJornada)) {
                if(Objects.nonNull(jnd.getMotoristasId().getEmpresa().getParametro().getHabilitarQuebraInterjornada()) && jnd.getMotoristasId().getEmpresa().getParametro().getHabilitarQuebraInterjornada()) {
                    //Pegar a ultima
                    if (ultimaJornada.getType() == JOURNEY_TYPE.JORNADA && jnd.getType() == JOURNEY_TYPE.JORNADA) {
                        int tempoEntreAsJornadas = Seconds.secondsBetween(new DateTime(ultimaJornada.getJornadaFim()), new DateTime(jnd.getJornadaInicio())).getSeconds();
                        ApuracaoDeJornada apuracaoDeJornada = registroJornada.get(registroJornada.size() - 1);
                        if (tempoEntreAsJornadas < Constantes.TEMPO_SECONDOS_INTERJORNADA) {
                            int minutosFaltantes = Constantes.TEMPO_SECONDOS_INTERJORNADA - tempoEntreAsJornadas;
                            long t = minutosFaltantes * 1000;

                            apuracaoDeJornada.setHorasExtrasInterJornadas(t);
                            if (Objects.nonNull(apuracaoDeJornada.getHorasExtrasNoturnas()) && apuracaoDeJornada.getHorasExtrasNoturnas() > 0) {
                                apuracaoDeJornada.setHorasExtrasNoturnas(ultimaJornada.getHorasExtrasNoturnas() + t);
                            }
                        } else {
                            if (tempoEntreAsJornadas >= Constantes.TEMPO_SECONDOS_INTERJORNADA &&
                                    (Objects.nonNull(apuracaoDeJornada.getHorasExtrasInterJornadas()) && apuracaoDeJornada.getHorasExtrasInterJornadas() > 0)) {
                                apuracaoDeJornada.setHorasExtrasInterJornadas(0l);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            LogSistema.logError("tratarQuebraInterjornadaViaParametro", e);
        }
    }

    /**
     * Rsolver o problema de quando uma jornada foi do mesmo dia, esta dupicando o valor na linha de baixa.
     * Retirar a linha de baixo
     *
     * @param registroJornada
     */
    private void removerInterjornadasRepetidas(ArrayList<ApuracaoDeJornada> registroJornada) {
        try {
            ApuracaoDeJornada adjT = null;
            for (ApuracaoDeJornada adj : registroJornada) {
                if (Objects.isNull(adjT)) {
                    adjT = adj;
                } else {
                    if (adjT.getHorasExtrasInterJornadas().equals(adj.getHorasExtrasInterJornadas())) {
                        adj.setHorasExtrasInterJornadas(0l);
                    }

                    if (adjT.getHorasExtrasNoturnas().equals(adj.getHorasExtrasNoturnas())) {
                        adj.setHorasExtrasNoturnas(0l);
                    }
                    adjT = adj;
                }
            }
        } catch (Exception e) {
            LogSistema.logError("removerInterjornadasRepetidas", e);
        }
    }

    private void tratarHorasExtrasNoDomingo(Jornada jnd, ApuracaoDeJornada adj) {
        try {

            if (Objects.isNull(ultimaJornada)) {
                return;
            }

            if(Objects.nonNull(jnd.getHorasExtrasAbonada()) && jnd.getHorasExtrasAbonada()){
                return;
            }

            Turnos turnos = jnd.getMotoristasId().getTurnosId();
            //verificar se tem nao tem turno no domingo

            if (Objects.nonNull(turnos) && Objects.nonNull(turnos.getTempoDiario01()) && turnos.getTempoDiario01() == 0) {
                //Sabemos que o turno do motorista nao pode trabalhar no domingo.
                //vamos ver agora se a jornada esta no domingo
                if (Objects.nonNull(jnd.getMotoristasId()) && Objects.nonNull(jnd.getMotoristasId().getEmpresa()) && Objects.nonNull(jnd.getMotoristasId().getEmpresa().getParametro()) &&
                        Objects.nonNull(jnd.getMotoristasId().getEmpresa().getParametro().getCalcularHorasExtrasDomingo())
                        && jnd.getMotoristasId().getEmpresa().getParametro().getCalcularHorasExtrasDomingo()) {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(jnd.getJornadaInicio());
                    boolean isComecouNoDomingo = cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY;
                    if (isComecouNoDomingo) {
                        //AQUI AGORA VER COM O PABLO
                        jnd.setHedDSR(jnd.getHorasExtrasDiurnas());
                        adj.setHedDSR(jnd.getHorasExtrasDiurnas());
                        jnd.setHenDSR(jnd.getHorasExtrasNoturnas());
                        adj.setHenDSR(jnd.getHorasExtrasNoturnas());
                        jnd.setDsrHE(Boolean.TRUE);
                        jnd.setHorasExtrasDiurnas(0L);
                        jnd.setHorasExtrasNoturnas(0L);
                        adj.setHorasExtrasDiurnas(0L);
                        adj.setHorasExtrasNoturnas(0L);
                    }
                }
            }
        } catch (Exception e) {
            LogSistema.logError("tratarInterjornadaMesmoDia", e);
        }
    }


    /*
    * GAMBIRRA NERVOSA PARA EXCLUIR A FALTA
    * ISSO VAI SER RESOLVIDO COM PYTHON
    * SE DEUS QUISER
     */
    public void removerFalta(){
        try {
            ApuracaoDeJornada apuracaoDeJornadaDaVez = null;
            List<Integer> listaDeIndex = new ArrayList<>();
            int index = 0;
            for(ApuracaoDeJornada apuracaoDeJornada : registroJornada){
                if(Objects.isNull(apuracaoDeJornadaDaVez)){
                    apuracaoDeJornadaDaVez = apuracaoDeJornada;
                }else{
                    if("FALTA".equalsIgnoreCase(apuracaoDeJornada.getTipoJornada())){
                        //valido se a ultima esta dentro do mesmo dia da fata
                        if (TimeHelper.isSameDay(apuracaoDeJornada.getData(), apuracaoDeJornadaDaVez.getData())) {
                            listaDeIndex.add(index);
                            apuracaoDeJornadaDaVez = null;
                        }
                    }else{
                        apuracaoDeJornadaDaVez = apuracaoDeJornada;
                    }
                }
                index++;
            }
            if (!listaDeIndex.isEmpty()) {
                for (Integer _index: listaDeIndex) {
                    ApuracaoDeJornada aj =  registroJornada.get(_index);
                    registroJornada.remove(aj);
                }
            }
        }catch (Exception e){
            LogSistema.logError("removerFalta", e);
        }
    }

}