package br.com.cmctransportes.cjm.domain.entities.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @author William Leite
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class VeiculoVO implements Serializable {

    private static final long serialVersionUID = 1L;

    public VeiculoVO() {

    }

    public VeiculoVO(Integer id) {
        this.id = id;
    }

    @Getter
    @Setter
    private String placa;
    @Getter
    @Setter
    private String marca;
    @Getter
    @Setter
    private String cor;
    @Getter
    @Setter
    private String renavan;
    @Getter
    @Setter
    private TiposDeVeiculoVO tiposDeVeiculo;
    @Getter
    @Setter
    private Integer id;
    @Getter
    @Setter
    private String anoFabricacao;
    @Getter
    @Setter
    private String anoModelo;
    @Getter
    @Setter
    private String chassi;
    @Getter
    @Setter
    private Date dataCadastro;
    @Getter
    @Setter
    private String observacao;
    @Getter
    @Setter
    private Double velocidadeMaxima;
    @Getter
    @Setter
    private Integer velocidadeMaximaChuva;
    @Getter
    @Setter
    private Integer velocidadeMaximaDesacelaracao;
    @Getter
    @Setter
    private Integer velocidadeMaximaCurva;
    @Getter
    @Setter
    private String renavam;
    @Getter
    @Setter
    private Boolean status;
    @Getter
    @Setter
    private String tipo;
    @Getter
    @Setter
    private String cidade;
    @Getter
    @Setter
    private String telefoneProprietario;
    @Getter
    @Setter
    private Boolean usaEntradaDigitalUm;
    @Getter
    @Setter
    private Boolean excessoVelocidade;
    @Getter
    @Setter
    private Boolean antiJummer;
    @Getter
    @Setter
    private Boolean bateriaCarroBaixa;
    @Getter
    @Setter
    private Boolean faltaEnergiaPrincipal;
    @Getter
    @Setter
    private Boolean panico;
    @Getter
    @Setter
    private Boolean quantidadeDiasSemTrasmissao;
    @Getter
    @Setter
    private Boolean roubo;
    @Getter
    @Setter
    private Boolean semComunicacao;
    @Getter
    @Setter
    private Boolean vinteQuatroHorasComunicacao;
    @Getter
    @Setter
    private Boolean ativaRotaNoMapa;
    @Getter
    @Setter
    private Boolean ativaValidacaoDeCerca;
    @Getter
    @Setter
    private String trocaDeHorimetro = "00:00;";
    @Getter
    @Setter
    private Integer rpmModoEconomicoMinimo;
    @Getter
    @Setter
    private Integer rpmModoEconomicoMaximo;
    @Getter
    @Setter
    private Integer rpmMaximo;
    @Getter
    @Setter
    private Integer rpmInicioFaixaAzul;
    @Getter
    @Setter
    private Integer rpmFimFaixaAzul;
    @Getter
    @Setter
    private Integer rpmInicioFaixaEconomica;
    @Getter
    @Setter
    private Integer rpmFimFaixaEconomica;
    @Getter
    @Setter
    private Integer rpmInicioFaixaVerde;
    @Getter
    @Setter
    private Integer rpmFimFaixaVerde;
    @Getter
    @Setter
    private Integer rpmInicioFaixaAmarela;
    @Getter
    @Setter
    private Integer rpmFimFaixaAmarela;
    @Getter
    @Setter
    private Integer rpmInicioMarchaLenta;
    @Getter
    @Setter
    private Integer rpmFimMarchaLenta;
    @Getter
    @Setter
    private Boolean geraEnderecoAutomatico;
    @Getter
    @Setter
    private Boolean validarIbutton;
    @Getter
    @Setter
    private Double cargaMaxima;
    @Getter
    @Setter
    private String combustivel;
    @Getter
    @Setter
    private Boolean gerarControleViagemAutomatico;
    @Getter
    @Setter
    private Double distanciaMinimaViagemAutomatica;
    @Getter
    @Setter
    private String tipoEntradaDigital;
    @Getter
    @Setter
    private Boolean ativarModuloTemperaturaCamaraFria;
    @Getter
    @Setter
    private Boolean enviarNotificacaoTemperaturaAplicativo;
    @Getter
    @Setter
    private Double temperaturaMinima;
    @Getter
    @Setter
    private Double temperaturaMaxima;
    @Getter
    @Setter
    private ModeloVO modelo;
    @Getter
    @Setter
    private EquipamentoVO equipamento;
    @Getter
    @Setter
    private EquipamentoVO equipamentoDois;
    @Getter
    @Setter
    private EmpresaVO empresaVO;
    @Getter
    @Setter
    private Integer tipoDeVeiculo;
    @Getter
    @Setter
    private Integer unidadeId;
    @Getter
    @Setter
    private String faturamentoFormatada;
    @Getter
    @Setter
    private Integer porcentagemFaturamento;
    @Getter
    @Setter
    private Boolean cercaMovel;
    @Getter
    @Setter
    private Integer raioCercaMovel;
    @Getter
    @Setter
    private Boolean validaCercaMovel;
    @Getter
    @Setter
    private Boolean bloquearVeiculo;
    @Getter
    @Setter
    private String tipoEntradaDigitalDois;
    @Getter
    @Setter
    private Boolean usaEntradaDigitalDois;
    @Getter
    @Setter
    private String tipoEntradaDigitalTres;
    @Getter
    @Setter
    private Boolean usaEntradaDigitalTres;
    @Getter
    @Setter
    private String tipoEntradaDigitalQuatro;
    @Getter
    @Setter
    private Boolean usaEntradaDigitalQuatro;
    @Getter
    @Setter
    private Boolean validarRota;
    @Getter
    @Setter
    private Double ultimoHodometro;
    @Getter
    @Setter
    private Boolean mostrarViam;
    @Getter
    @Setter
    private List<HodometroVO> listaHodometro;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VeiculoVO veiculoVO = (VeiculoVO) o;
        return getPlaca().equals(veiculoVO.getPlaca()) &&
                getId().equals(veiculoVO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPlaca(), getId());
    }
}
