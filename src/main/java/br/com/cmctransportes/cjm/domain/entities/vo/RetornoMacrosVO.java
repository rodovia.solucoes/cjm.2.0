package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.Macros;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


public class RetornoMacrosVO extends RetornoVO {
    @Getter
    @Setter
    private Macros macros;
    @Getter
    @Setter
    private List<Macros> listaDeMacros;
}
