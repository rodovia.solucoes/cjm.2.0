package br.com.cmctransportes.cjm.domain.entities.jornada;

import br.com.cmctransportes.cjm.domain.entities.Evento;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author analista.ti
 */
public class Resumo {

    @Getter
    @Setter
    private String icon;
    @Getter
    @Setter
    private String color;
    @Getter
    @Setter
    private Long somatorioDiurno;
    @Getter
    @Setter
    private Long somatorioNoturno;
    @Getter
    @Setter
    private List<Evento> lista;

    public Resumo() {
        this.lista = new ArrayList<>();
        this.somatorioDiurno = 0L;
        this.somatorioNoturno = 0L;
    }

    public Long getSomatorio() {
        if (somatorioDiurno == null && somatorioNoturno == null) {
            return null;
        }

        Long result = new Long(0);
        if (somatorioDiurno != null) {
            result = somatorioDiurno;
        }

        if (somatorioNoturno != null) {
            result += somatorioNoturno;
        }

        return result;
    }

    public void addToList(Evento evt) {
        if (Objects.isNull(evt)) {
            return;
        }

        if (Objects.nonNull(evt.getDayNight())) {
            this.somatorioDiurno += evt.getDayNight().getTempoDiurno();
            this.somatorioNoturno += evt.getDayNight().getTempoNoturno();
        }

        this.lista.add(evt);
    }

    public List<Evento> getEventsList() {
        return this.lista;
    }
}
