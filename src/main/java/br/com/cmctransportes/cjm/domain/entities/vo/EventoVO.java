package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class EventoVO implements Serializable {
    private String descricao;
    private String dataEvento;
    private Double latitude;
    private Double longitude;
}
