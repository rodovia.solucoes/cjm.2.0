package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class TempoTransporteGraficoVO implements Serializable {
    private static final long serialVersionUID = 1L;
    private String category;
    private Double vazio;
    private Double cheio;
}
