/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author analista.ti
 */
@Getter
@Setter
public class Instante {

    private Date data;
    private Date original;
    private String justificativa;
    private String tipoOriginal;
}