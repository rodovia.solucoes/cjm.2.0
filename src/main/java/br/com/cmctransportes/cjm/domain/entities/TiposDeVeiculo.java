package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author analista.ti
 */
@Entity
@Table(name = "tipos_de_veiculo")
@Getter
@Setter
@NamedQueries({
        @NamedQuery(name = "TiposDeVeiculo.findAll", query = "SELECT t FROM TiposDeVeiculo t"),
        @NamedQuery(name = "TiposDeVeiculo.findById", query = "SELECT t FROM TiposDeVeiculo t WHERE t.id = :id"),
        @NamedQuery(name = "TiposDeVeiculo.findByDescricao", query = "SELECT t FROM TiposDeVeiculo t WHERE t.descricao = :descricao"),
        @NamedQuery(name = "TiposDeVeiculo.findByPermiteAcoplamento", query = "SELECT t FROM TiposDeVeiculo t WHERE t.permiteAcoplamento = :permiteAcoplamento"),
        @NamedQuery(name = "TiposDeVeiculo.findByTracaoPropria", query = "SELECT t FROM TiposDeVeiculo t WHERE t.tracaoPropria = :tracaoPropria")})
public class TiposDeVeiculo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TIPOS_DE_VEICULO_ID_SEQ", sequenceName = "TIPOS_DE_VEICULO_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TIPOS_DE_VEICULO_ID_SEQ")
    @Column(name = "id")
    private Integer id;

    @Column(name = "descricao", length = 15, nullable = false)
    private String descricao;

    @Column(name = "permite_acoplamento")
    private Boolean permiteAcoplamento;

    @Column(name = "tracao_propria")
    private Boolean tracaoPropria;

    @JoinColumn(name = "empresa_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Empresas empresaId;
}