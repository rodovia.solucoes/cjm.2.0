package br.com.cmctransportes.cjm.domain.entities.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * @author William Leite
 */
public class RelatorioEventoVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String nomeEvento;
    private Double latitude;
    private Double longitude;
    private String endereco;
    private String local;
    private String origem;
    private Date data;

    public RelatorioEventoVO(String nomeEvento, Double latitude, Double longitude, String local, Date data, String origem) {
        this.nomeEvento = nomeEvento;
        this.latitude = latitude;
        this.longitude = longitude;
        this.local = local;
        this.data = data;
        this.origem = origem;
    }

    public RelatorioEventoVO(String nomeEvento, Double latitude, Double longitude, String local, Date data, String origem, String endereco) {
        this.nomeEvento = nomeEvento;
        this.latitude = latitude;
        this.longitude = longitude;
        this.endereco = endereco;
        this.local = local;
        this.origem = origem;
        this.data = data;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getNomeEvento() {
        return nomeEvento;
    }

    public void setNomeEvento(String nomeEvento) {
        this.nomeEvento = nomeEvento;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.nomeEvento);
        hash = 53 * hash + Objects.hashCode(this.latitude);
        hash = 53 * hash + Objects.hashCode(this.longitude);
        hash = 53 * hash + Objects.hashCode(this.data);
        hash = 53 * hash + Objects.hashCode(this.local);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RelatorioEventoVO other = (RelatorioEventoVO) obj;
        if (!Objects.equals(this.nomeEvento, other.nomeEvento)) {
            return false;
        }
        if (!Objects.equals(this.latitude, other.latitude)) {
            return false;
        }
        if (!Objects.equals(this.longitude, other.longitude)) {
            return false;
        }
        if (!Objects.equals(this.data, other.data)) {
            return false;
        }
        if (!Objects.equals(this.local, other.local)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "RelatorioEventoVO{" + "nomeEvento=" + nomeEvento + ", latitude=" + latitude + ", longitude=" + longitude + ", local=" + local + ", data=" + data + ", origem=" + origem + '}';
    }

    /**
     * @return the origem
     */
    public String getOrigem() {
        return origem;
    }

    /**
     * @param origem the origem to set
     */
    public void setOrigem(String origem) {
        this.origem = origem;
    }
}
