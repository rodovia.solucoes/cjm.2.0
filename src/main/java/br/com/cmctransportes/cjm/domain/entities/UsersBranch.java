package br.com.cmctransportes.cjm.domain.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author William Leite
 */
@Entity
@Table(name = "users_branch")
@Getter
@Setter
public class UsersBranch implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "users_branch_id_gen", sequenceName = "users_branch_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "users_branch_id_gen")
    @Column(name = "id")
    private Integer id;

    @JoinColumn(name = "branch_id", referencedColumnName = "id")
    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    private Unidades branch;

    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    @JsonBackReference("usersBranch")
    private Users user;

    public UsersBranch() {
    }

    public UsersBranch(Integer id, Unidades unidade, Users usuario) {
        this.id = id;
        this.branch = unidade;
        this.user = usuario;
    }
}