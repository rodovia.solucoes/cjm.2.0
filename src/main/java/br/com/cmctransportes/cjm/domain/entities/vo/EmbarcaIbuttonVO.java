package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.IdentificacaoOperador;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EmbarcaIbuttonVO implements Serializable {
    private static final long serialVersionUID = 1L;
    @Getter
    @Setter
    private IdentificacaoOperador identificacaoOperadorNovo;
    @Getter
    @Setter
    private IdentificacaoOperador identificacaoOperadorAntigo;
}
