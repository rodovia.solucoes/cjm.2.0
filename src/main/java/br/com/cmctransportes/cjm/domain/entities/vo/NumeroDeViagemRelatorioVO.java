package br.com.cmctransportes.cjm.domain.entities.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class NumeroDeViagemRelatorioVO implements Serializable {
    private Integer totalDeViagens;
    private List<String> listaDePlacas;
    private List<DadosNumeroViagemVO> listaDeDadosNumeroDeViagem;
}
