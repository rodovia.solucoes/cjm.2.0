package br.com.cmctransportes.cjm.domain.entities.enums;

/**
 * @author Producao
 */
public enum JOURNEY_CALCULATION_STATUS {
    UNALTERED,
    ALTERED;
}
