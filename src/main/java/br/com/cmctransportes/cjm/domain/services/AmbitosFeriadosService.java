package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.AmbitosFeriados;
import br.com.cmctransportes.cjm.domain.repository.AmbitosFeriadosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author analista.ti
 */
@Service
public class AmbitosFeriadosService {

    @Autowired
    private AmbitosFeriadosRepository repository;

    public void save(AmbitosFeriados entity) {
        try {
            this.repository.saveAndFlush(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // public T getById(PK pk) {
    public List<AmbitosFeriados> findAll() {
        return this.repository.findAll();
    }

    public AmbitosFeriados getById(Integer id) {
        return this.repository.findById(id).get();
    }

    public void update(AmbitosFeriados entity) {
        this.save(entity);
    }

    public void delete(Integer id) {
        try {
            this.repository.deleteById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void delete(AmbitosFeriados entity) {
        this.delete(entity.getId());
    }
}