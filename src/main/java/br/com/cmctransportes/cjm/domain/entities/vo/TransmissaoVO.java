package br.com.cmctransportes.cjm.domain.entities.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class TransmissaoVO implements Serializable {
    private Date dataTransmissao;
    private Boolean ignicaoAtiva;
    private Double latitude;
    private Double longitude;
    private Double velocidade;
    private Boolean parado;
    private Boolean paradoComIgnicaoAtiva;
    private Integer minutosParados;
    private String endereco;

}
