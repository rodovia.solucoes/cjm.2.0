package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author William Leite
 */
@Entity
@Table(name = "motivo_abono")
@Getter
@Setter
public class MotivoAbono implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name = "MOTIVO_ABONO_ID_SEQ", sequenceName = "MOTIVO_ABONO_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MOTIVO_ABONO_ID_SEQ")
    @Column(name = "id")
    private Integer id;

    @Column(name = "descricao", length = 15, nullable = false)
    private String descricao;

    public MotivoAbono() {

    }

    public MotivoAbono(Integer id) {
        this.id = id;
    }
}