package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.*;
import br.com.cmctransportes.cjm.domain.entities.enums.EVENT_TYPE;
import br.com.cmctransportes.cjm.domain.entities.jornada.Resumo;
import br.com.cmctransportes.cjm.domain.entities.vo.*;
import br.com.cmctransportes.cjm.domain.repository.EventoRepository;
import br.com.cmctransportes.cjm.infra.dao.EventoDAO;
import br.com.cmctransportes.cjm.infra.dao.MotoristasDAO;
import br.com.cmctransportes.cjm.infra.dao.MotoristasVeiculosDAO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.sascarsoap.Motorista;
import br.com.cmctransportes.cjm.sascarsoap.Veiculo;
import br.com.cmctransportes.cjm.services.JourneyCalculusService;
import br.com.cmctransportes.cjm.singleton.DataSourcePostgres;
import br.com.cmctransportes.cjm.utils.LocationUtils;
import br.com.cmctransportes.cjm.utils.TimeHelper;
import lombok.Getter;
import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author analista.ti
 */
@Service
public class EventoService {
// TODO: criar GenericService

    @Autowired
    private JornadaService jornadaService;

    @Getter
    private EventoDAO dao;
    private MotoristasVeiculosDAO motoristaVeiculoDAO;

    @Autowired
    private DriverService driverService;

    @Autowired
    private DsrService dsrService;

    @Autowired
    private UnidadesService unidadesService;

    @Autowired
    private JourneyCalculusService journeyCalculusService;

    @Autowired
    private PontosCargaDescargaService pdcService;

    @Autowired
    private EventoRepository eventoRepository;

    @Autowired
    private LocalService localService;

    private MotoristasDAO motoristasDAO;

    private final EntityManager em;

    public EventoService(EntityManager em) {
        this.em = em;
        dao = new EventoDAO(em);
        this.motoristaVeiculoDAO = new MotoristasVeiculosDAO(em);
        this.motoristasDAO = new MotoristasDAO(em);
    }

    /**
     * Salva dois eventos encadeados i.e. um de início e um de fim.
     *
     * @param eventoInicio Evento Inicial
     * @param eventoFim    Evento Final
     * @param validate     Se valida o período
     */
    @Transactional
    public void saveChained(Evento eventoInicio, Evento eventoFim, boolean validate) {

        if (eventoInicio.getTipoEvento().getId() != 1 && eventoInicio.getTipoEvento().getId() != 21) {
            if (validate && !this.validateRange(eventoInicio, eventoFim)) {
                System.err.println("Periodo inválido!");
                throw new RuntimeException("Periodo inválido!");
            }
        }

        try {
            eventoInicio.setVeiculoMotor(this.retrieveVehicle(eventoInicio));
            this.eventoRepository.saveAndFlush(dao.address(eventoInicio));

            eventoFim.setVeiculoMotor(this.retrieveVehicle(eventoFim));
            this.eventoRepository.saveAndFlush(dao.address(eventoFim));
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        // TODO: verificar uma forma de encadear essas requisições para fazer
        // o rollback
        removeFaltaDaJornada(eventoInicio);
        removeFeriadoDaJornada(eventoInicio);

    }

    private boolean validateRange(Evento eventoInicio, Evento eventoFim) {
        Jornada jornada = eventoInicio.getJornadaId();
        if (Objects.isNull(jornada.getEventos())) {
            jornada = this.jornadaService.getById(eventoInicio.getJornadaId().getId());
        }

        this.journeyCalculusService.processEvents(jornada, null,false);

        boolean result = true;
        if (Objects.nonNull(jornada.getJornadaInicio())) {
            // nega a verificacao assim equivale ao <=
            result &= !(jornada.getJornadaInicio().after(eventoInicio.getInstanteEvento()));
        } else {
            result &= false;
        }
        if (Objects.nonNull(jornada.getJornadaFim())) {
            // nega a verificacao assim equivale ao >=
            result &= !(jornada.getJornadaFim().before(eventoFim.getInstanteEvento()));
        } else {
            result &= false;
        }

        return result;
    }

    private Veiculos retrieveVehicle(Evento entity) {
        Jornada jornada = entity.getJornadaId();
        if (Objects.isNull(jornada.getMotoristasId())) {
            jornada = this.jornadaService.getById(entity.getJornadaId().getId());
        }
        return this.motoristaVeiculoDAO.getByDriver(jornada.getMotoristasId().getId(), entity.getInstanteEvento());
    }

    @Transactional
    public void save(Evento entity) {
        // TODO: verificar uma forma de encadear essas requisições para fazer
        // o rollback
        removeFaltaDaJornada(entity);
        removeFeriadoDaJornada(entity);

        try {
            Evento tmpEntity = dao.getEventByInstant(entity);
            if (tmpEntity == null) {
                this.eventoRepository.saveAndFlush(this.dao.address(entity));
            } else {
                entity.setId(tmpEntity.getId());
            }

            entity.setVeiculoMotor(this.retrieveVehicle(entity));
        } catch (Exception e) {
            e.printStackTrace();
        }
        /*finally {
            boolean result = this.updateOrder(entity);
            if (result == false) {
                System.err.println("Não foi possível reordenar o evento!");
            }
        }*/
    }

    public List<RelatorioEventoVO> relatorioDeEventos(Integer motoristaId, Long dtInicio, Long dtFim) {
        Motoristas mot = this.driverService.getById(motoristaId);

        Date dataInicio = TimeHelper.getDate000000(new Date(dtInicio));
        Date dataFinal = TimeHelper.getDate235959(new Date(dtFim));

        List<RelatorioEventoVO> result = dao.getRelatorioDeEventos(mot, dataInicio, dataFinal);

        Set<LocalVO> locations = new HashSet<>(0);
        List<LocalVO> listaLocalSemRaio = new ArrayList<>();
        List<LocalVO> listaDeLocal = pdcService.findAllAsLocal(mot.getEmpresaId());

        for (LocalVO localVO : listaDeLocal) {
            List<CoordenadaVO> ll = localVO.getListaDeCoordenadas().stream().filter(c -> c.getRaio() != null && c.getIsRaio()).collect(Collectors.toList());
            if (ll != null && !ll.isEmpty()) {
                locations.add(localVO);
            } else {
                listaLocalSemRaio.add(localVO);
            }
        }
        final LocationUtils util = new LocationUtils();
        result.stream().forEach((RelatorioEventoVO vo) -> {

            if (Objects.isNull(vo.getLatitude()) || Objects.isNull(vo.getLongitude())) {
                vo.setLocal("Não informado");
            } else {
                if (!locations.isEmpty()) {
                    LocalVO local = new LocalVO(0, "", vo.getLatitude(), vo.getLongitude(), false);
                    Optional<LocalVO> closest = util.closest(local, locations);
                    if (closest.isPresent()) {
                        // verifica se a localidade encontada é próxima o suficiente
                        LocalVO voTemp = closest.get();
                        CoordenadaVO coordenadaVO = voTemp.getListaDeCoordenadas().get(0);
                        double lat = vo.getLatitude();
                        double lon = vo.getLongitude();
                        double raio = coordenadaVO.getRaio();
                        if (util.closeEnough(lat, coordenadaVO.getLatitude() , lon ,coordenadaVO.getLongitude() , raio )) {
                            vo.setLocal(String.format("%s", voTemp.getNome()));
                        }
                    }
                }

                if (!listaLocalSemRaio.isEmpty()) {
                    for (LocalVO localVO : listaLocalSemRaio) {
                        LatLng[] latLngs = localVO.convert();
                        if (vo.getLatitude() != null && vo.getLongitude() != null) {
                            if (util.verificarSeOPontoEstaNaRegiao(vo.getLatitude(), vo.getLongitude(), latLngs)) {
                                vo.setLocal(String.format("%s ", localVO.getNome()));
                                break;
                            }
                        }
                    }
                }
            }

            if((vo.getLocal() == null) ||(vo.getLocal() != null && vo.getLocal().isEmpty())){
                vo.setLocal("Não cadastrado");
            }
        });

        List<Date> inBetween = TimeHelper.getDatesBetween(dataInicio, dataFinal);
        inBetween.stream().forEach((d) -> {
            Date dayStart = TimeHelper.getDate000000(d);
            Date dayEnd = TimeHelper.getDate235959(d);
            if (result.stream().noneMatch(e -> e.getData().after(dayStart) && e.getData().before(dayEnd))) {
                result.add(new RelatorioEventoVO("", null, null, "", dayStart, ""));
            }
        });

        result.sort((e1, e2) -> e1.getData().compareTo(e2.getData()));
        return result;
    }

    private LocalVO searchWaitPlace(double latitude, double longitude, List<LocalVO> listaDeLocais) {
        Set<LocalVO> locations = new HashSet<>(0);
        List<LocalVO> listaLocalSemRaio = new ArrayList<>();

        for (LocalVO localVO : listaDeLocais) {
            List<CoordenadaVO> ll = localVO.getListaDeCoordenadas().stream().filter(c -> c.getRaio() != null && c.getIsRaio()).collect(Collectors.toList());
            if (ll != null && !ll.isEmpty()) {
                locations.add(localVO);
            } else {
                listaLocalSemRaio.add(localVO);
            }
        }

        final LocationUtils util = new LocationUtils();

        if (!locations.isEmpty()) {
            LocalVO local = new LocalVO(0, "", latitude, longitude, false);

            Optional<LocalVO> closest = util.closest(local, locations);
            if (closest.isPresent()) {
                // verifica se a localidade encontada é próxima o suficiente
                CoordenadaVO coordenadaVO = closest.get().getListaDeCoordenadas().get(0);
                if (util.closeEnough(latitude, coordenadaVO.getLatitude(), longitude, coordenadaVO.getLongitude(), coordenadaVO.getRaio())) {
                    return closest.get();
                }
            }
        }

        if (!listaLocalSemRaio.isEmpty()) {
            for (LocalVO localVO : listaLocalSemRaio) {
                LatLng[] latLngs = localVO.convert();
                if (latitude != 0 && longitude != 0) {
                    if (util.verificarSeOPontoEstaNaRegiao(latitude, longitude, latLngs)) {
                        return localVO;
                    }
                }
            }
        }

        return null;
    }



    // public T getById(PK pk) {
    public List<Evento> findAll() {
        // return dao.findAll();
        List<Evento> lista = this.eventoRepository.findAll();
        lista.forEach(ev -> {
            ajustarLocalVo(ev);
        });

        return lista;
    }

    public Evento getById(Integer id) {
        if (Objects.nonNull(id)) {
            Evento evento = this.eventoRepository.findById(id).get();
            ajustarLocalVo(evento);
            return evento;
        }
        return null;
    }

    private Evento getOriginal(Integer id, Evento evento) {
        Evento original = getById(id);

        if (original == null) {
            return original;
        }

        if (original.getInstanteOriginal() == null) {
            original.setInstanteOriginal(original.getInstanteEvento());
        }

        original.setInstanteEvento(evento.getInstanteEvento());
        original.setJustificativa(evento.getJustificativa());

        original.setOperadorAlteracao(evento.getOperadorAlteracao());
        ajustarLocalVo(original);
        return original;
    }

    /**
     * Atualiza dois eventos encadeados i.e. um de início e um de fim.
     *
     * @param idInicio     ID do Evento Inicial
     * @param eventoInicio Evento Inicial
     * @param idFim        ID do Evento Final
     * @param eventoFim    Evento Final
     */
    @Transactional
    public void updateChained(Integer idInicio, Evento eventoInicio, Integer idFim, Evento eventoFim) {
        Evento originalInicio = this.getOriginal(idInicio, eventoInicio);
        Evento originalFinal = this.getOriginal(idFim, eventoFim);

        try {
            originalInicio.setVeiculoMotor(this.retrieveVehicle(originalInicio));
            this.eventoRepository.saveAndFlush(this.dao.address(originalInicio));

            originalFinal.setVeiculoMotor(this.retrieveVehicle(originalFinal));
            this.eventoRepository.saveAndFlush(this.dao.address(originalFinal));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void update(Integer id, Evento entity) {
        Evento original = this.getOriginal(id, entity);
        try {
            original.setVeiculoMotor(this.retrieveVehicle(original));
            this.eventoRepository.saveAndFlush(this.dao.address(original));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Remove dois eventos encadeados i.e. um de início e um de fim.
     *
     * @param idInicio ID do Evento Inicial
     * @param idFinal  ID do Evento Final
     */
    @Transactional
    public void deleteChained(Integer idInicio, Integer idFinal) {
        Integer reordenarID = null;
        Evento eventoInicio = this.getById(idInicio);
        Evento eventoFinal = this.getById(idFinal);

        if (Objects.isNull(eventoInicio) || Objects.isNull(eventoFinal)) {
            return;
        }

        try {
            dao.delete(eventoFinal);
            dao.delete(eventoInicio);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void delete(Integer id, Integer user) {
        Evento entity = getById(id);
        entity.setOperadorLancamento(new Users(user));
        this.delete(entity);
    }

    @Transactional
    public void delete(Evento entity) {
        Integer reordenarID = null;

        if (entity == null) {
            return;
        }

        if (entity.getEventoAnterior() != null) {
            reordenarID = entity.getEventoAnterior().getId();
        } else if (entity.getEventoSeguinte() != null) {
            reordenarID = entity.getEventoSeguinte().getId();
        }

        try {
            dao.delete(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //    public List<Evento> getRelatorioDeJornada(Integer motorista, Integer mesReferencia) {
//        return dao.getRelatorioDeJornada(motorista, mesReferencia);
//    }
    public Evento getLastEvent(Integer motorista) {
        return dao.getLastevent(motorista);
    }

    public List<Evento> findCurrentEventAll(Integer idEmpresa, Integer idUnidade) {
        List<Evento> lista = dao.findCurrentEventAll(idEmpresa, idUnidade);
        lista.forEach(evento -> {
            Motoristas motoristas = motoristasDAO.buscarPeloUserId(evento.getOperadorLancamento());
            if (motoristas != null) {
                evento.getOperadorLancamento().setMotoristas(new MotoristaVO(motoristas.getId(), motoristas.getNome(), motoristas.getSobrenome()));
            }
            ajustarLocalVo(evento);
        });
        return lista;
    }

    private void removeFaltaDaJornada(Evento eventoInicio) {
        removeMarcacaoPorTipo(eventoInicio, 19);
    }

    @Transactional
    public void saveWithOutReOrder(Evento entity, boolean removeFalta) {
        // TODO: verificar uma forma de encadear essas requisições para fazer
        // o rollback
        if (removeFalta) {
            removeFaltaDaJornada(entity);
            removeFeriadoDaJornada(entity);
        }

        try {
            Evento tmpEntity = dao.getEventByInstant(entity);
            entity.setVeiculoMotor(this.retrieveVehicle(entity));

            if (tmpEntity == null) {
                this.eventoRepository.saveAndFlush(this.dao.address(entity));
            } else {
                entity.setId(tmpEntity.getId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void marcaRemovido(Integer id, Evento entity) {
        Evento original = this.getOriginal(id, entity);

        if (Objects.isNull(original)) {
            return;
        }

        Integer pairId = 0;

        if (entity.getTipoEvento().getId() > 0) {
            pairId = entity.getFimEventoId();
        } else { // fim de evento
            pairId = entity.getAberturaId();
        }

        Evento pairEvt = this.getOriginal(pairId, entity);

        try {
            original.setRemovido(entity.getRemovido());
            original.setJustificativa(entity.getJustificativa());

            if (Objects.nonNull(pairEvt)) {
                pairEvt.setRemovido(entity.getRemovido());
                pairEvt.setJustificativa(entity.getJustificativa());

                this.eventoRepository.saveAndFlush(this.dao.address(pairEvt));
            }

            this.eventoRepository.saveAndFlush(this.dao.address(original));

            // Se for DSR remove na tabela DSR
            if (original.getTipoEvento().getId() == EVENT_TYPE.DSR.getStartCode() || original.getTipoEvento().getId() == EVENT_TYPE.DSR.getEndCode()
                    || original.getTipoEvento().getId() == EVENT_TYPE.INTERJORNADA.getStartCode() || original.getTipoEvento().getId() == EVENT_TYPE.INTERJORNADA.getEndCode()) {
                this.dsrService.marcaRemovidoDsr(original);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Evento pegaPosterior(Evento entity, Stream<Evento> eventos) {
        Evento evento = null;

        Optional<Evento> eventoApos = eventos.filter(
                e -> e.getInstanteEvento().after(entity.getInstanteEvento())
        ).sorted(
                (e1, e2) -> e1.getInstanteEvento().compareTo(e2.getInstanteEvento())).findFirst();

        if (eventoApos.isPresent()) {
            evento = eventoApos.get();
            evento.setEventoAnterior(entity);
        }
        ajustarLocalVo(evento);
        return evento;
    }

    private Evento pegaAnterior(Evento entity, Stream<Evento> eventos) {
        Evento evento = null;

        Optional<Evento> eventoAntes = eventos.filter(e -> e.getInstanteEvento().before(entity.getInstanteEvento()))
                .sorted((e1, e2) -> e2.getInstanteEvento().compareTo(e1.getInstanteEvento())).findFirst();

        if (eventoAntes.isPresent()) {
            evento = eventoAntes.get();
            entity.setEventoAnterior(evento);
        }
        ajustarLocalVo(evento);
        return evento;
    }

    private void removeMarcacaoPorTipo(Evento eventoInicio, int tipo) {
        int tmpTipo = eventoInicio.getTipoEvento().getId();

        // para não apagar o que acabou de ser lançado
        if ((tmpTipo == tipo) || ((tmpTipo * -1) == tipo)) {
            return;
        }

        Evento inicioMarcacao = null;

        Jornada jornada = eventoInicio.getJornadaId();

        if (jornada != null) {
            if (Objects.isNull(jornada.getEventos())) {
                jornada = this.jornadaService.getById(eventoInicio.getJornadaId().getId());
            }
            if (Objects.nonNull(jornada.getEventos())) {
                this.journeyCalculusService.processEvents(jornada, null, false);
                inicioMarcacao = this.jornadaService.pegaEventoPorTipo(jornada, tipo);
            }
        }

        if (inicioMarcacao == null) {
            inicioMarcacao = dao.getEventoPorTipoNaData(eventoInicio, tipo);

            // realmente não tem falta...
            if (inicioMarcacao == null) {
                return;
            }
        }

        if (jornada == null) {
            jornada = inicioMarcacao.getJornadaId();
        }

        inicioMarcacao.getFimEvento();

        deleteChained(inicioMarcacao.getId(), inicioMarcacao.getFimEventoId());

//        if (aproveitaJornada) {

        Collection<Evento> tmpEvts = jornada.getEventos();
        if (Objects.nonNull(tmpEvts)) {
            tmpEvts.remove(inicioMarcacao);

            Evento fimFalta = inicioMarcacao.getEventoSeguinte();
            if (fimFalta != null) {
                tmpEvts.remove(fimFalta);
            }

            jornada.setEventos(tmpEvts);
        }

        eventoInicio.setJornadaId(jornada);
//        } else {
//            js.delete(jornada);
//        }
    }

    private void removeFeriadoDaJornada(Evento eventoInicio) {
        removeMarcacaoPorTipo(eventoInicio, 22);
    }

    public void removeDSRDaJornada(Evento eventoInicio) {
        removeMarcacaoPorTipo(eventoInicio, 20);
    }

    public boolean eventPlaceAllowsRest(Evento event, List<LocalVO> listaDeLocal) {
        boolean result = false;

        if (Objects.nonNull(event.getIdLocalEventoEspera())) {
            try {
                RetornoLocalVO retornoLocalVO = new LocalService().buscarLocalPeloCodigo(event.getIdLocalEventoEspera());
                if (Objects.nonNull(retornoLocalVO) && Objects.nonNull(retornoLocalVO.getLocal())) {
                    result = Objects.nonNull(retornoLocalVO.getLocal().isPermiteDescanso()) ? retornoLocalVO.getLocal().isPermiteDescanso() : false;
                }
            } catch (Exception r) {
                LogSistema.logError("eventPlaceAllowsRest", r);
            }
        } else if ((Objects.nonNull(event.getLatitude())) && (Objects.nonNull(event.getLongitude()))) {
            LocalVO tmpLocal = searchWaitPlace(event.getLatitude(), event.getLongitude(), listaDeLocal);

            if (Objects.nonNull(tmpLocal)) {
                result = tmpLocal.isPermiteDescanso();
            }
        }

        return result;
    }


    public List<JornadaMotorista> mountEventList(Date dataInicial, Date dataFinal, List<Motoristas> drivers, Integer user, String type) {
        Date calendarStart = dataInicial;
        Date calendarEnd = dataFinal;

        List<JornadaMotorista> jornadasMotorista = new ArrayList<>();

        // Para cada motorista
        drivers.stream().forEach(driver -> {
            JornadaMotorista jm = new JornadaMotorista();
            jm.setNomeMotorista(driver.getNome() + " " + driver.getSobrenome());
            jm.setNomeEmpresa(driver.getObjEmpresa().getNome());

            Unidades unidade = this.unidadesService.getById(driver.getUnidadesId());

            jm.setNomeUnidade(unidade.getApelido());

            List<Jornada> jornadas = new ArrayList<>();

            // Busca todas as jornadas para este motorista
            jornadas.addAll(this.jornadaService.jornadasDoMotoristaPorPeriodo(calendarStart, calendarEnd, driver.getId()).stream()
                    .map(j -> {
                        this.journeyCalculusService.processEvents(j, null, false);
                        return j;
                    })
                    .filter(j -> Objects.nonNull(j.getJornadaInicio()))
                    .sorted(Comparator.nullsLast((j1, j2) -> j1.getJornadaInicio().compareTo(j2.getJornadaInicio())))
                    .collect(Collectors.toList()));

            // Gera os resumos para este motorista
            Map<Date, List<EventoPeriodo>> resumoEvento = new HashMap<>();
            jornadas.stream().forEach(j -> {
                j.getResumoEstados().entrySet().stream().forEach(re -> {
                    Resumo r = re.getValue();
                    r.getLista().stream().forEach(e -> {
                        Date data = TimeHelper.getDate000000(e.getInstanteEvento());

                        if (Objects.isNull(resumoEvento.get(data))) {
                            resumoEvento.put(data, new ArrayList<>());
                        }

                        List<EventoPeriodo> newList = resumoEvento.get(data);
                        EventoPeriodo ep = new EventoPeriodo();

                        ep.setDataInicio(e.getInstanteEvento());
                        ep.setDataFim(e.getFimEvento());

                        if (Objects.equals(type, "N")) {
                            ep.setTipo(e.getTipoEvento().getDescricao());
                        } else {
                            ep.setTipo(re.getKey());
                        }

                        newList.add(ep);
                    });
                });
            });

            jm.setResumo(resumoEvento);
            jornadasMotorista.add(jm);
        });

        return jornadasMotorista;
    }


    public DetalheDashboardVO buscarUltimoEvento(MotoristaVO motorista) throws Exception {
        Evento evento = dao.buscarUltimoEvento(motorista);
        DetalheDashboardVO detalheDashboardVO = new DetalheDashboardVO();
        String dataEvento = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(evento.getInstanteEvento());
        detalheDashboardVO.setDataEventoViam(dataEvento);
        Veiculos veiculo = evento.getVeiculoMotor();
        if (veiculo != null) {
            detalheDashboardVO.setVeiculoAtrelado(veiculo.getPlaca());
        }

        switch (evento.getTipoEvento().getId()) {
            case -8:
            case 8:
                detalheDashboardVO.setEmDirecao(true);
                break;
            case 26:
            case -26:
                detalheDashboardVO.setComInterjornada(true);
                break;
            case -10:
            case 10:
                detalheDashboardVO.setEmCarga(true);
                break;
            case -11:
            case 11:
                detalheDashboardVO.setAguardandoCarregamento(true);
                break;
            case -7:
            case 7:
                detalheDashboardVO.setEmAbastecimento(true);
            case -12:
            case 12:
                detalheDashboardVO.setEmDescarga(true);
            case -9:
            case 9:
                detalheDashboardVO.setEmDescarga(true);
            default: {
                detalheDashboardVO.setOutroEvento(true);
                detalheDashboardVO.setNomeDoEvento(evento.getTipoEvento().getDescricao());
            }
        }
        List<EventoVO> list = new ArrayList<>();

        List<Evento> listaDeEventos = dao.buscarDezUltimosEvento(motorista);
        listaDeEventos.forEach(e -> {
            EventoVO eventoVO = new EventoVO();
            eventoVO.setDescricao(e.getTipoEvento().getDescricao());
            eventoVO.setDataEvento(new SimpleDateFormat("dd/MM HH:mm").format(e.getInstanteEvento()));
            list.add(eventoVO);
        });

        detalheDashboardVO.setListaEvento(list);
        return detalheDashboardVO;
    }


    public DetalheDashboardVO buscarUltimosEventosDoDia(MotoristaVO motorista) throws Exception {
        Evento evento = dao.buscarUltimoEvento(motorista);
        DetalheDashboardVO detalheDashboardVO = new DetalheDashboardVO();
        String dataEvento = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(evento.getInstanteEvento());
        detalheDashboardVO.setDataEventoViam(dataEvento);
        Veiculos veiculo = evento.getVeiculoMotor();
        if (veiculo != null) {
            detalheDashboardVO.setVeiculoAtrelado(veiculo.getPlaca());
        }

        switch (evento.getTipoEvento().getId()) {
            case -8:
            case 8:
                detalheDashboardVO.setEmDirecao(true);
                break;
            case 26:
            case -26:
                detalheDashboardVO.setComInterjornada(true);
                break;
            case -10:
            case 10:
                detalheDashboardVO.setEmCarga(true);
                break;
            case -11:
            case 11:
                detalheDashboardVO.setAguardandoCarregamento(true);
                break;
            case -7:
            case 7:
                detalheDashboardVO.setEmAbastecimento(true);
            case -12:
            case 12:
                detalheDashboardVO.setEmDescarga(true);
            case -9:
            case 9:
                detalheDashboardVO.setEmDescarga(true);
            default: {
                detalheDashboardVO.setOutroEvento(true);
                detalheDashboardVO.setNomeDoEvento(evento.getTipoEvento().getDescricao());
            }
        }
        List<EventoVO> list = new ArrayList<>();

        List<Evento> listaDeEventos = dao.buscarUltimosEventosDoDia(motorista);
        listaDeEventos.forEach(e -> {
            EventoVO eventoVO = new EventoVO();
            eventoVO.setDescricao(e.getTipoEvento().getDescricao());
            eventoVO.setDataEvento(new SimpleDateFormat("dd/MM HH:mm").format(e.getInstanteEvento()));
            eventoVO.setLatitude(e.getLatitude());
            eventoVO.setLongitude(e.getLongitude());
            list.add(eventoVO);
        });

        detalheDashboardVO.setListaEvento(list);
        return detalheDashboardVO;
    }

    private void ajustarLocalVo(Evento e) {
        try {
            if (Objects.nonNull(e.getIdLocalEventoEspera())) {
                RetornoLocalVO retornoLocalVO = localService.buscarLocalPeloCodigo(e.getIdLocalEventoEspera());
                if (Objects.nonNull(retornoLocalVO) && Objects.nonNull(retornoLocalVO.getLocal())) {
                    PontosDeEspera pontosDeEspera = new PontosDeEspera();
                    pontosDeEspera.setIdentificacao(retornoLocalVO.getLocal().getNome());
                    pontosDeEspera.setPermiteDescanso(retornoLocalVO.getLocal().isPermiteDescanso());
                    e.setLocalEventoEspera(pontosDeEspera);
                }
            }
        } catch (Exception ex) {
        }
    }


    public List<LocalGraficoVO> buscarEventoParaGrafico(Integer idEvento, Integer idMotorista, Date dataInicial, Date dataFinal){
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM HH:mm");
        List<LocalGraficoVO> lista = new ArrayList<>();
        try {
            String sql = " select e.tipo_evento, e.instante_evento, e.latitude, e.longitude, e.position_address from jornada j " +
                    " inner join evento e on e.jornada_id = j.id  " +
                    " where motoristas_id = ? " +
                    " and e.tipo_evento in(?, ?) " +
                    " and e.removido = false " +
                    " and e.instante_evento between ? and ? order by e.instante_evento ";
            connection = DataSourcePostgres.getInstance().getConnection();
            int idEventoInvertido = 0;
            if(idEvento != 1){
                idEventoInvertido = idEvento * -1;
            }
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idMotorista);
            preparedStatement.setInt(2, idEvento);
            preparedStatement.setInt(3, idEventoInvertido);
            preparedStatement.setTimestamp(4, new Timestamp(dataInicial.getTime()));
            preparedStatement.setTimestamp(5, new Timestamp(dataFinal.getTime()));
            resultSet = preparedStatement.executeQuery();
            List<Evento> listaTemp = new ArrayList<>();

            Evento ultimoEvento = null;

            int totalDeHoras = 0;

            while(resultSet.next()){
               Integer tipoEvento = resultSet.getInt("tipo_evento");
                Date instanteDoEvento = resultSet.getTimestamp("instante_evento");
                Double latitude = resultSet.getDouble("latitude");
                Double longitude = resultSet.getDouble("longitude");

                String endereco = resultSet.getString("position_address");
               if(isInicioDeEvento(tipoEvento)){
                   if(ultimoEvento == null){
                       ultimoEvento = new Evento();
                       ultimoEvento.setInstanteEvento(instanteDoEvento);
                       ultimoEvento.setLatitude(latitude);
                       ultimoEvento.setLongitude(longitude);
                       ultimoEvento.setPositionAddress(endereco);
                   }
               }

               if(!isInicioDeEvento(tipoEvento) && ultimoEvento != null ){
                   //Fecha o evento
                   LocalGraficoVO graficoVOinicio = new LocalGraficoVO();
                   graficoVOinicio.setDataEvento(simpleDateFormat.format(ultimoEvento.getInstanteEvento()));
                   if(ultimoEvento.getLatitude().equals(0.0) && ultimoEvento.getLongitude().equals(0.0)){
                       graficoVOinicio.setEndereco("Não informado");
                       graficoVOinicio.setUrlGoogleMaps("-");
                       graficoVOinicio.setStatus("Iniciou-Manual");
                   }else {
                       graficoVOinicio.setEndereco(ultimoEvento.getPositionAddress());
                       graficoVOinicio.setUrlGoogleMaps(new StringBuilder().append("http://maps.google.com/maps?q=")
                               .append(ultimoEvento.getLatitude()).append(",").append(ultimoEvento.getLongitude())
                               .toString());
                       graficoVOinicio.setStatus("Iniciou");
                   }
                   lista.add(graficoVOinicio);

                   LocalGraficoVO graficoVOFim = new LocalGraficoVO();
                   graficoVOFim.setDataEvento(simpleDateFormat.format(instanteDoEvento));
                   if(latitude.equals(0.0) && longitude.equals(0.0)){
                       graficoVOFim.setEndereco("Não informado");
                       graficoVOFim.setUrlGoogleMaps("-");
                       graficoVOFim.setStatus("Finalizou-Manual");
                   }else {
                       graficoVOFim.setEndereco(endereco);
                       graficoVOFim.setUrlGoogleMaps(new StringBuilder().append("http://maps.google.com/maps?q=")
                               .append(latitude).append(",").append(longitude)
                               .toString());
                       graficoVOFim.setStatus("Finalizou");
                   }
                   int minutos = Minutes.minutesBetween(new DateTime(ultimoEvento.getInstanteEvento()), new DateTime(instanteDoEvento)).getMinutes();
                   graficoVOFim.setValorHora(minutos);
                   totalDeHoras = totalDeHoras + minutos;
                   lista.add(graficoVOFim);
                   ultimoEvento = null;

               }

            }
        }catch (Exception e){
            LogSistema.logError("buscarEventoParaGrafico",e);
        }finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarEventoParaGrafico", ex);
            }
        }
        return lista;
    }

    /**
     * Metodo para buscar os eventos e ser processados como ociosos
     * @param idMotorista
     * @param dataInicial
     * @param dataFinal
     * @return
     */
    public List<Evento> buscarEventoParaGrafico(Integer idMotorista, Date dataInicial, Date dataFinal){
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM HH:mm");
        List<Evento> lista = new ArrayList<>();
        try {
            String sql = " select e.tipo_evento, e.instante_evento, e.latitude, e.longitude, e.position_address from jornada j " +
                    " inner join evento e on e.jornada_id = j.id  " +
                    " where motoristas_id = ? " +
                    " and e.removido = false " +
                    " and e.instante_evento between ? and ? order by e.instante_evento ";

            connection = DataSourcePostgres.getInstance().getConnection();

            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idMotorista);
            preparedStatement.setTimestamp(2, new Timestamp(dataInicial.getTime()));
            preparedStatement.setTimestamp(3, new Timestamp(dataFinal.getTime()));
            resultSet = preparedStatement.executeQuery();

            while(resultSet.next()){
                Integer tipoEvento = resultSet.getInt("tipo_evento");
                Date instanteDoEvento = resultSet.getTimestamp("instante_evento");
                Double latitude = resultSet.getDouble("latitude");
                Double longitude = resultSet.getDouble("longitude");
                String endereco = resultSet.getString("position_address");
                Evento evento = new Evento();
                evento.setInstanteEvento(instanteDoEvento);
                evento.setLatitude(latitude);
                evento.setLongitude(longitude);
                evento.setPositionAddress(endereco);
                evento.setTipoEvento(new EventosJornada(tipoEvento));
                lista.add(evento);
            }
        }catch (Exception e){
            LogSistema.logError("buscarEventoParaGrafico",e);
        }finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                LogSistema.logError("buscarEventoParaGrafico", ex);
            }
        }
        return lista;
    }

    private boolean isInicioDeEvento(int idEvento){
        int isPositivo = Integer.signum(idEvento);
        if(isPositivo >= 1){
            return true;
        }
        return false;
    }
}
