package br.com.cmctransportes.cjm.domain.services;


import br.com.cmctransportes.cjm.infra.dao.ParametroDAO;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;

@Service
public class ParametroService {
    private final EntityManager em;
    private final ParametroDAO parametroDAO;
    public ParametroService(EntityManager em) {
        this.em = em;
        this.parametroDAO = new ParametroDAO(em);
    }
}
