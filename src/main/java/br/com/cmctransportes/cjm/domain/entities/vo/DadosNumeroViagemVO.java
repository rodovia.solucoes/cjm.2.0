package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class DadosNumeroViagemVO implements Serializable {
    private int vec1 = -1;
    private int vec2 = -1;
    private int vec3 = -1;
    private int vec4 = -1;
    private int vec5 = -1;
    private int vec6 = -1;
    private int vec7 = -1;
    private int vec8 = -1;
    private int vec9 = -1;
    private int vec10 = -1;
    private Date data;

}
