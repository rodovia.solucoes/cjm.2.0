package br.com.cmctransportes.cjm.domain.services;


import br.com.cmctransportes.cjm.controllers.runnable.JornadaRunnble;
import br.com.cmctransportes.cjm.domain.entities.*;
import br.com.cmctransportes.cjm.domain.entities.Evento;
import br.com.cmctransportes.cjm.domain.repository.EventoRepository;
import br.com.cmctransportes.cjm.domain.repository.JourneyRepository;
import br.com.cmctransportes.cjm.domain.repository.SascarRepository;
import br.com.cmctransportes.cjm.infra.dao.EventoDAO;
import br.com.cmctransportes.cjm.infra.dao.JornadaDAO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.sascarsoap.*;
import br.com.cmctransportes.cjm.utils.ArquivoDeConfiguracao;
import br.com.cmctransportes.cjm.utils.ThreadFactory;
import br.com.cmctransportes.cjm.utils.TimeHelper;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.xml.datatype.XMLGregorianCalendar;
import java.lang.Exception;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Service
public class SascarJobWebService {

    @Autowired
    private SascarService sascarService;

    @Autowired
    private DriverService driverService;

    @Autowired
    private RastroService rastroService;

    @Autowired
    private EventoRepository eventoRepository;

    @Autowired
    private SascarVeiculoService  sascarVeiculoService;

    @Autowired
    private JourneyRepository journeyRepository;

    private final EventoDAO eventoDAO;

    @Getter
    private JornadaDAO dao;

    @Autowired
    private final EntityManager em;

    private static Sascar usuarioSascar;
    private Motoristas motorista = null;

    public SascarJobWebService(EntityManager em) {
        this.em = em;
        eventoDAO = new EventoDAO(em);
        dao = new JornadaDAO(em);
    }

    /**
     * Consulta ao SOAP sascar os dados para geração do evento ViaM
     *
     * @param dataInicio data de inicio
     * @param dataFim    data fim
     * @return Lista com todos eventos encontrados na data definida
     * @throws SasIntegraNotification exeption do SOAP
     */
    public List<PacotePosicao> ConsultaEventosSascar(String usuarioSascar, String senha, Integer idSascar, String dataInicio, String dataFim) throws SasIntegraNotification {
        SasIntegraWS sasIntegraWSService = new SasIntegraWSService().getSasIntegraWSPort();
        List<PacotePosicao> eventosSascar = sasIntegraWSService.obterPacotePosicaoMotoristaHistorico(usuarioSascar,
                senha, dataInicio, dataFim, idSascar);
        return eventosSascar;
    }



    /**
     * Recebe do controller os dados iniciais para tratamento e criação dos eventos
     *
     * @param dateEnd SOAP necessita de uma data fim da consulta
     * @return se foi bem sucessido a consulta ou não para o Controller
     * @throws SasIntegraNotification exeption da Sascar SOAP
     */
    public String ServerEvent(SascarVeiculo sascarVeiculo, long dateEnd, List<MacroSascar> listaMacroSascar, String userSascar, String senha) throws SasIntegraNotification {
        //consultar no banco a startDate
        String dataInicio = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(sascarVeiculo.getDataAtualizacao());
        String dataFim = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(dateEnd));
        List<PacotePosicao> eventosSascar = getPacotePosicaos(userSascar, senha, new Integer(sascarVeiculo.getIdSascar()), dataInicio, dataFim);
        eventosSascar = ajustarPacotes(eventosSascar);
        if (Objects.nonNull(eventosSascar) && !eventosSascar.isEmpty()) {
            this.readService(eventosSascar, sascarVeiculo, listaMacroSascar);
        }
        try {
            //update na data no banco com a dataEnd
            dateUpdate(dateEnd, sascarVeiculo);
            return "Concluido com sucesso";
        } catch (Exception e) {
            e.printStackTrace();
            return String.valueOf(e);
        }

    }

    /**
     * Lista de posições do SOAP Sascar
     *
     * @param dataInicio
     * @param dataFim
     * @return
     * @throws SasIntegraNotification
     */
    private List<PacotePosicao> getPacotePosicaos(String usuarioSascar, String senha, Integer idSascar, String dataInicio, String dataFim) throws SasIntegraNotification {
        SascarJobWebService sascarWebService = new SascarJobWebService(em);
        return this.ConsultaEventosSascar(usuarioSascar, senha,  idSascar, dataInicio,  dataFim);
    }

    /**
     * Atualiza a data no banco para a proxima consulta do Job Sascar
     *
     * @param dateEnd data da consulta atual
     */
    private void dateUpdate(long dateEnd, SascarVeiculo sascarVeiculo) {
        try {
            sascarVeiculo.setDataAtualizacao(new Date(dateEnd));
            this.sascarVeiculoService.update(sascarVeiculo);
        }catch (Exception e){
            LogSistema.logError("dateUpdate", e);
        }
    }

    /**
     * Cria o evento com os dados da lista de eventos extraidas do SOAP
     *
     * @param eventosSascar Lista de eventos
     * @return uma lista de Eventos no padrão ViaM
     */
    private void readService(List<PacotePosicao> eventosSascar, SascarVeiculo sascarVeiculo, List<MacroSascar> listaMacroSascar) {

        List<Evento> listaDeEvento = new ArrayList<>();
        try {
            if (eventosSascar != null && eventosSascar.isEmpty()) {
                return;
            }
            for (PacotePosicao split : eventosSascar) {
                String nomeMotorista = split.getNomeMotorista();
                validarMotorista(nomeMotorista, sascarVeiculo.getEmpresa());
                if(Objects.isNull(motorista)){
                    LogSistema.logInfo("SERVICO SASCAR: Motorista:"+nomeMotorista+ " NAO ENCONTRADO NA BASE VIA-M DADOS "+ split.toString());
                   continue;
                }

                Empresas empresa = motorista.getEmpresa();
                Integer tipoEventoArquivo = split.getCodigoMacro();

                LogSistema.logInfo(new SimpleDateFormat("dd/MM HH:mm:ss").format(new Date()) + " SERVICO SASCAR: Motorista:"+nomeMotorista+ " | " + split.toString());

                Integer idRodovia = this.buscarDePara(tipoEventoArquivo, listaMacroSascar, empresa.getId(), split.getConteudoMensagem());
                if (idRodovia != -100) {
                    Integer ultimoEvento = this.eventoDAO.buscarUltimoIdEvento(motorista.getUserId());
                    if (idRodovia == ultimoEvento) {
                        continue;
                    }
                    String conteudoMensagem = split.getConteudoMensagem();
                    Date instanteLancamento = toDate(split.getDataPosicao());
                    Date instanteEvento = toDate(split.getDataPacote());
                    Double latitude = split.getLatitude();
                    Double longitude = split.getLongitude();
                    String pontoParada = split.getPontoReferencia() + " Rua: " + split.getRua() + " Cidade: " + split.getCidade();

                    //Tempos usados para fechar eventos, nesse caso tiramos um segundo
                    Date tempoUm = TimeHelper.diminuirSegundosNaData(instanteEvento, 1);
                    Date tempoDois = TimeHelper.diminuirSegundosNaData(instanteLancamento, 1);

                    if ((idRodovia != 1 && idRodovia != -100) && ultimoEvento == 0) {
                        //siginifica que tem um fim de jornada com um novo evento e sem inicio de jornada
                        //insere uma nova jornada

                        this.createEvento(tempoUm, tempoDois, latitude, longitude, 1, pontoParada, motorista, empresa, listaDeEvento);
                        this.createEvento(instanteEvento, instanteLancamento, latitude, longitude, idRodovia, pontoParada, motorista, empresa, listaDeEvento);
                    } else if (idRodovia == 1 && (ultimoEvento == 0 || ultimoEvento == -100)) { //Abre uma nova jornada
                        this.createEvento(instanteEvento, instanteLancamento, latitude, longitude, idRodovia, pontoParada, motorista, empresa, listaDeEvento);
                    } else if (idRodovia == 0) {//fecha a jornada
                        if (ultimoEvento > 1) {
                            //temos que fechar os eventos
                            //tenho que fechar o evento atual
                            int valor = ultimoEvento * -1;
                            this.createEvento(tempoUm, tempoDois, latitude, longitude, valor, pontoParada, motorista, empresa, listaDeEvento);//fechei o evento
                        }
                        this.createEvento(instanteEvento, instanteLancamento, latitude, longitude, idRodovia, pontoParada, motorista, empresa, listaDeEvento);
                    } else if (idRodovia != 0 && idRodovia != 1 && idRodovia != -100) {
                        if (idRodovia > 1) {
                            if (ultimoEvento > 1 //nao esta fechado
                                    && idRodovia > 1 /* nao é inicio de jornda*/) {
                                //tenho que fechar o evento atual
                                int valor = ultimoEvento * -1;
                                this.createEvento(tempoUm, tempoDois, latitude, longitude, valor, pontoParada, motorista, empresa, listaDeEvento);//fechei o evento
                            }
                            //inserir novo evento
                            this.createEvento(instanteEvento, instanteLancamento, latitude, longitude, idRodovia, pontoParada, motorista, empresa, listaDeEvento);
                        } else if (idRodovia < 0) {
                            //vai fechar o evento
                            this.createEvento(instanteEvento, instanteLancamento, latitude, longitude, idRodovia, pontoParada, motorista, empresa, listaDeEvento);
                        }
                    }
                    createRastro(instanteEvento, instanteLancamento, latitude, longitude, motorista, empresa, split.getCidade(), split.getUf(), split.getRua());


                }
            }
            inserirMongo(listaDeEvento);
        } catch (Exception e) {
            LogSistema.logError("readService", e);
        }

    }




    /**
     * Cria o evento RASTRO com os dados da lista de eventos extraidas do SOAP
     *
     * @param motoristaId Id do motorista
     * @param empresaId   Id da empresa atrelada ao motorista
     * @return uma lista de Eventos no padrão ViaM
     */
    private void createRastro(Date instanteEvento, Date instanteLancamento, Double latitude, Double longitude,
                              Motoristas motoristaId, Empresas empresaId, String cidade, String uf, String endereco) {
        Rastro result = new Rastro();
        result.setEmpresaId(empresaId);
        result.setMotoristaId(motoristaId.getId());
        result.setInstante(instanteEvento);
        result.setInstanteEnvio(instanteLancamento);
        result.setLatitude(latitude);
        result.setLongitude(longitude);
        result.setCidade(cidade);
        result.setUf(uf);
        result.setEndereco(endereco);
        rastroService.save(result);
    }

    final AtomicReference<Jornada> atomicJornada = new AtomicReference<>();

    /**
     * Salva o evento no Banco
     */
    public void createEvento(Date instanteEvento, Date instanteLancamento, Double latitude, Double longitude, Integer tipoEvento,
                             String pontoParada, Motoristas motoristaId, Empresas empresaId, List<Evento> listaDeEventos) {

        try {

            Evento result = new Evento();

            final List<Evento> eventosJornada = new ArrayList<>();
            int[] eventosEterno = new int[]{2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 15, 16, 23, 25};
            final AtomicReference<Integer> isEterno = new AtomicReference<>(null);

            if (tipoEvento == 1) {
                Jornada jornada = new Jornada();
                jornada.setMotoristasId(motoristaId);
                jornada.setEmpresaId(empresaId);
                jornada.setDsrHE(Boolean.FALSE);
                jornada.setWaitingAsWorked(Boolean.FALSE);
                jornada.setLocked(Boolean.FALSE);
                jornada = this.journeyRepository.saveAndFlush(jornada);
                atomicJornada.set(jornada);
            } else {
                Jornada jornada = this.dao.getLastJourney(motoristaId.getId());
                if (Objects.isNull(jornada)) {
                    jornada = new Jornada();
                    jornada.setMotoristasId(motoristaId);
                    jornada.setEmpresaId(empresaId);
                    jornada.setDsrHE(Boolean.FALSE);
                    jornada.setWaitingAsWorked(Boolean.FALSE);
                    jornada.setLocked(Boolean.FALSE);
                    jornada = this.journeyRepository.saveAndFlush(jornada);
                }
                atomicJornada.set(jornada);
            }

            result.setJornadaId(atomicJornada.get());
            result.setInstanteEvento(instanteEvento);
            result.setInstanteLancamento(instanteLancamento);
            result.setLatitude(latitude);
            result.setLongitude(longitude);
            result.setTipoEvento(new EventosJornada(tipoEvento));
            result.setPositionAddress(pontoParada);
            result.setOperadorLancamento(new Users(motoristaId.getUserId()));
            result.setEmpresaId(empresaId);

            if (Arrays.binarySearch(eventosEterno, result.getTipoEvento().getId()) >= 0) {
                isEterno.set(result.getTipoEvento().getId());
            } else {
                this.eventoDAO.address(result);
                this.eventoRepository.save(result);
            }
            if (Objects.nonNull(isEterno.get())) {
                this.eventoDAO.address(result);
                this.eventoRepository.save(result);
            }

            if (tipoEvento == 0) {
                atomicJornada.set(null);
            }

            listaDeEventos.add(result);

        } catch (Exception e) {
            LogSistema.logError("createEvento", e);
        }
    }

    /**
     * Busca na lista o de para entre sascar e rodovia, se nao encontrar retorna -100
     *
     * @param tipoSascar
     * @param listaMacroSascar
     * @return
     */
    private int retorno = -100;

    private int buscarDePara(Integer tipoSascar, List<MacroSascar> listaMacroSascar, Integer idEmpresa, String conteudoMensagem) {

        try {
            retorno = -100;
            List<MacroSascar> result = listaMacroSascar.stream().filter(macro -> macro.getIdSascar() == tipoSascar &&
                    macro.getIdEmpresa() == idEmpresa).collect(Collectors.toList());
            ;
            if (result != null && !result.isEmpty()) {
                result.forEach(macroSascar -> {
                    if (macroSascar.getMensagem() != null && !macroSascar.getMensagem().isEmpty()) {
                        if (conteudoMensagem.contains(macroSascar.getMensagem())) {
                            retorno = macroSascar.getIdRodovia();
                        }
                    } else {
                        retorno = macroSascar.getIdRodovia();
                    }
                });
            }
        } catch (Exception e) {
            LogSistema.logError("buscarDePara", e);
        }

        return retorno;
    }

    /**
     * converte dateXML em milessegundos
     *
     * @param dataXml data no formato padrão do XML
     * @return data em milesegundos
     */
    private Date toDate(XMLGregorianCalendar dataXml) {
        return dataXml.toGregorianCalendar().getTime();
    }

    /**
     * "De Para" dos eventos Sascar para os Eventos ViaM
     *
     * @param tipoEventoArquivo Tipo de evento do SOAP Sascar
     * @return Tipo de evento no ViaM
     */
    public Integer convertEventType(Integer tipoEventoArquivo, String conteudoMensagem) {
        Integer result = null;
        if (tipoEventoArquivo.equals(0)) { //rastro
            result = 55;
        } else if (tipoEventoArquivo.equals(1)) { //se der null é pq veio 1
            result = null;
        } else if (tipoEventoArquivo.equals(2)) { //inicio de jornada = 1
            result = 1;
        } else if (tipoEventoArquivo.equals(3)) { //inicio de direção = 8
            result = 8;
        } else if (tipoEventoArquivo.equals(4)) { // fim de direção
            result = -8;
        } else if (tipoEventoArquivo.equals(5)) { //INICIO AGUARDANDO CARGA = 9
            result = 9;
        } else if (tipoEventoArquivo.equals(6)) { //Inicio Aguardando descarga = 11
            result = 11;
        } else if (tipoEventoArquivo.equals(7)) { //inicio refeição = 14
            result = 14;
        } else if (tipoEventoArquivo.equals(8)) { //fim de refeição = -14
            result = -14;
        } else if (tipoEventoArquivo.equals(9)) { //Inicio de Carga = 10
            result = 10;
        } else if (tipoEventoArquivo.equals(10)) { //INICIO DE FISCALIZACAO = 13
            result = 13;
        } else if (tipoEventoArquivo.equals(11)) { //fim de carga = -10
            result = -10;
        } else if (tipoEventoArquivo.equals(12)) { //inicio manifesto
            result = 2;
        } else if (tipoEventoArquivo.equals(13)) { //fim manifesto
            result = -2;
        } else if (tipoEventoArquivo.equals(14)) {
            result = null;
        } else if (tipoEventoArquivo.equals(15)) { // IDENTIFICAR ABASTECIMENTO 7
            result = null;
        } else if (tipoEventoArquivo.equals(16)) { //INICIO DE PARADA (abastecimento 7, lanche = 5, banheiro = 6, pista interditada 4, intervalo pessoal 25)
            if (conteudoMensagem.contains("_X___________")) {
                result = 7;
            } else if (conteudoMensagem.contains("___X_________")) {
                result = 6;
            } else if (conteudoMensagem.contains("_____X_______")) {
                result = 5;
            } else if (conteudoMensagem.contains("_______X_____")) {
                result = 25;
            } else if (conteudoMensagem.contains("_________X___")) {
                result = 4;
            } else if (conteudoMensagem.contains("___________X_")) {
                result = 23;
            }
        } else if (tipoEventoArquivo.equals(17)) { //FIM DE PARADA (abastecimento 7, lanche = 5, banheiro = 6, pista interditada 4, intervalo pessoal 25)
            if (conteudoMensagem.contains("_X___________")) {
                result = -7;
            } else if (conteudoMensagem.contains("___X_________")) {
                result = -6;
            } else if (conteudoMensagem.contains("_____X_______")) {
                result = -5;
            } else if (conteudoMensagem.contains("_______X_____")) {
                result = -25;
            } else if (conteudoMensagem.contains("_________X___")) {
                result = -4;
            } else if (conteudoMensagem.contains("___________X_")) {
                result = -23;
            }
        } else if (tipoEventoArquivo.equals(20)) { //repouso de direção = 15
            result = 15;
        } else if (tipoEventoArquivo.equals(21)) { // Fim repouso direção = -15
            result = -15;
        } else if (tipoEventoArquivo.equals(22)) { //Inicio Manutenção = 3
            result = 3;
        } else if (tipoEventoArquivo.equals(23)) { //FIM de manutenção = -3
            result = -3;
        } else if (tipoEventoArquivo.equals(30)) { //fim de jornada
            result = 0;
        } else if (tipoEventoArquivo.equals(31)) { //fim de aguardando carga
            result = -9;
        } else if (tipoEventoArquivo.equals(32)) { //FIM Aguardando descarga
            result = -11;
        } else if (tipoEventoArquivo.equals(33)) { //fim de fiscalizção = -13
            result = -13;
        } else if (tipoEventoArquivo.equals(34)) { //Inicio de descarga
            result = 12;
        } else if (tipoEventoArquivo.equals(35)) { //FIM de descarga
            result = -12;
        }
        return result;
    }


    private List<PacotePosicao> ajustarPacotes(List<PacotePosicao> lista) {
        List<PacotePosicao> novaLista = new ArrayList<>();
        try {
            //Retirar a turma que é macro igual a zero
            lista.stream()
                    .forEach(x -> {
                        if (x.getCodigoMacro() != 0) {
                            novaLista.add(x);
                        }
                    });
            //Ordena a lista por data
            novaLista.sort(Comparator.comparing((PacotePosicao s) -> toDate(s.getDataPacote())));
        } catch (Exception e) {
            LogSistema.logError("ajustarPacotes", e);
        }
        return novaLista;
    }

    private void inserirMongo(List<Evento> listaEvento) {
        try {
            for (Evento evento : listaEvento) {
                evento.setEventoSeguinte(null);
                evento.setEventoAnterior(null);
                ThreadFactory.getExecutor().execute(new JornadaRunnble(evento, "TECLADO"));
            }
        } catch (Exception e) {
            LogSistema.logError(this.getClass().getName() + ":inserirMongo", e);
        }
    }



    private void validarMotorista(String nomeMotorista, Integer idEmpresa){
        try {
            if(Objects.isNull(motorista)) {
                //vai buscar no banco
                motorista = driverService.buscarPeloNome(nomeMotorista, idEmpresa);
            }else{
                //verificar se é mudou de motorista
                if(Objects.nonNull(motorista)){
                    String nome = nomeMotorista.split("\\s+")[0];
                    String sobrenome = nomeMotorista.split("\\s+")[1];
                    boolean aa = motorista.getNome().toUpperCase().contains(nome.toUpperCase());
                    boolean bb = motorista.getSobrenome().toUpperCase().contains(sobrenome.toUpperCase());
                    if( !aa || !bb ){
                        motorista = driverService.buscarPeloNome(nomeMotorista, idEmpresa);
                    }
                }

            }
        }catch (Exception e){
            LogSistema.logError(this.getClass().getName() + ":validarMotorista", e);
        }
    }
}