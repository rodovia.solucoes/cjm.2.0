package br.com.cmctransportes.cjm.domain.entities.xml;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name="MensagemCB")
public class MensagemCB {


    private double mId;
    private int veiID;
    private String dt;
    private String lat;
    private String lon;
    private String rod;
    private String rua;
    private String mun;
    private String dMac;
    private int tfrID;
    private int motID;
    private String mot;


    @XmlElement
    public double getmId() {
        return mId;
    }

    public void setmId(double mId) {
        this.mId = mId;
    }

    @XmlElement
    public int getVeiID() {
        return veiID;
    }

    public void setVeiID(int veiID) {
        this.veiID = veiID;
    }

    @XmlElement
    public String getDt() {
        return dt;
    }

    public void setDt(String dt) {
        this.dt = dt;
    }

    @XmlElement
    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    @XmlElement
    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    @XmlElement
    public String getdMac() {
        return dMac;
    }

    public void setdMac(String dMac) {
        this.dMac = dMac;
    }

    @XmlElement
    public int getTfrID() {
        return tfrID;
    }

    public void setTfrID(int tfrID) {
        this.tfrID = tfrID;
    }

    @XmlElement
    public int getMotID() {
        return motID;
    }

    public void setMotID(int motID) {
        this.motID = motID;
    }

    @XmlElement
    public String getMot() {
        return mot;
    }

    public void setMot(String mot) {
        this.mot = mot;
    }

    @XmlElement
    public String getRod() {
        return rod;
    }

    public void setRod(String rod) {
        this.rod = rod;
    }

    @XmlElement
    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    @XmlElement
    public String getMun() {
        return mun;
    }

    public void setMun(String mun) {
        this.mun = mun;
    }


}
