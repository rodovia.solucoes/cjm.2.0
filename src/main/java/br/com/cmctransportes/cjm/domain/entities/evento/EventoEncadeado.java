package br.com.cmctransportes.cjm.domain.entities.evento;

import br.com.cmctransportes.cjm.domain.entities.vo.EventoWebVO;

/**
 * DTO usado nos eventos referentes a eventos encadeados.
 *
 * @author William Leite
 */
public class EventoEncadeado {

    private EventoWebVO eventoInicio;
    private EventoWebVO eventoFinal;

    public EventoWebVO getEventoInicio() {
        return eventoInicio;
    }

    public void setEventoInicio(EventoWebVO eventoInicio) {
        this.eventoInicio = eventoInicio;
    }

    public EventoWebVO getEventoFinal() {
        return eventoFinal;
    }

    public void setEventoFinal(EventoWebVO eventoFinal) {
        this.eventoFinal = eventoFinal;
    }
}
