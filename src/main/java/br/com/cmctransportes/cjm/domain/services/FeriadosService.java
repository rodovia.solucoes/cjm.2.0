package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.Feriados;
import br.com.cmctransportes.cjm.domain.entities.Motoristas;
import br.com.cmctransportes.cjm.domain.entities.Unidades;
import br.com.cmctransportes.cjm.domain.entities.vo.DiurnoNoturno;
import br.com.cmctransportes.cjm.domain.repository.FeriadosRepository;
import br.com.cmctransportes.cjm.infra.dao.FeriadosDAO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.utils.TimeHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author analista.ti
 */
@Service
public class FeriadosService {
// TODO: criar GenericService

    @Autowired
    private UnidadesService unidadesService;

    @Autowired
    private FeriadosRepository repository;

    private FeriadosDAO dao;

    private final EntityManager em;

    public FeriadosService(EntityManager em) {
        this.em = em;
        dao = new FeriadosDAO(em);
    }

    @Transactional
    public void save(Feriados entity) {
        try {
            this.repository.saveAndFlush(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Feriados> findAll() {

        return this.repository.findAll();
    }

    public List<Feriados> findAllByEmpresa(Integer idEmpresa, Integer idUnidade) {
        try {
            return this.dao.buscarTodos(idEmpresa, idUnidade);
        } catch (Exception e) {
            LogSistema.logError("findAllByEmpresa", e);
        }
        return null;

    }

    public Feriados getById(Integer id) {
        return this.repository.findById(id).get();
    }

    @Transactional
    public void update(Feriados entity) {
        try {
            this.repository.saveAndFlush(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void delete(Integer id) {
        try {
            this.repository.deleteById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void delete(Feriados entity) {
        try {
            this.repository.delete(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Date> getDataDosFeriados(Date dataInicial, Date dataFinal, Motoristas driver) {
        List<Date> result = dao.getDataDosFeriados(dataInicial, dataFinal, driver, this.unidadesService);
        return result;
    }

    public List<Feriados> getEventoEmFeriado(Date inicio, Date fim, Motoristas motorista) {
        return dao.getEventoEmFeriado(inicio, fim, motorista, this.unidadesService);
    }

    /**
     * Returns the holiday's amount of daily and night hours. All holidays are retrieve from the database based on the "inicio", "fim" and "motoristasId".
     * If there are no holidays, returns 0 as both the daily and night hours. For each holiday retrieved, checks if the start and end is between the "inicio"
     * and "fim" parameters, if not overwrites the "inicioFeriado" and "fimFeriado", and then separates the daily and night hours. The sum of all holidays
     * is then returned.
     *
     * @param motoristasId
     * @param inicio
     * @param fim
     * @return
     * @see br.com.cmctransportes.cjm.infra.dao.FeriadosDAO.getEventoEmFeriado
     * @see br.com.cmctransportes.cjm.domain.services.NoturnoService.calculaNoturno
     */
    public DiurnoNoturno pegaFeriadoTime(Motoristas motoristasId, Date inicio, Date fim) {
        DiurnoNoturno result = new DiurnoNoturno();
        List<Date> datasDoFeriado = new ArrayList<>();

        if (inicio == null || fim == null) {
            return result;
        }

        Date tmpIni = TimeHelper.getDate000000(inicio);
        Date tmpFim = TimeHelper.getDate235959(fim);

        List<Feriados> lista = dao.getEventoEmFeriado(inicio, fim, motoristasId, this.unidadesService);
        NoturnoService calcN = new NoturnoService();

        long diurno = 0;
        long noturno = 0;

        DiurnoNoturno tmpDN = new DiurnoNoturno();

        if (Objects.nonNull(lista) && !lista.isEmpty()) {
            // TODO: separar diurno e noturno
            Feriados tmpDsr = lista.get(0);

            Date feriadoIni = tmpDsr.getInicioFeriado();
            Date feriadoFim = tmpDsr.getFimFeriado();

            if (inicio.before(feriadoIni) && fim.after(feriadoIni) && (fim.compareTo(feriadoFim) <= 0)) {
                // Se inicio < inicioDsr  & fim > inicioDsr & fim <= fimDsr
                tmpIni = feriadoIni;
                tmpFim = fim;
            } else if (inicio.before(feriadoIni) && (fim.compareTo(feriadoFim) >= 0)) {
                // Se inicio < inicioDsr  & fim > fimDsr
                tmpIni = feriadoIni;
                tmpFim = feriadoFim;
            } else if ((inicio.compareTo(feriadoIni) >= 0) && (fim.compareTo(feriadoFim) <= 0)) {
                // Se inicio > inicioDsr  & fim < fimDsr
                tmpIni = inicio;
                tmpFim = fim;
            } else if ((inicio.compareTo(feriadoIni) >= 0) && fim.after(feriadoFim)) {
                // Se inicio > inicioDsr e fim > fimDsr
                int a = inicio.compareTo(feriadoIni);
                boolean as = fim.after(feriadoFim);
                tmpIni = inicio;
                tmpFim = feriadoFim;
            }

            tmpDN = calcN.calculaNoturno(tmpIni, tmpFim, true);

            diurno += tmpDN.getTempoDiurno();
            noturno += tmpDN.getTempoNoturno();
            datasDoFeriado.add(tmpDsr.getDataOcorrencia());

        }
        result.setTempoDiurno(diurno);
        result.setTempoNoturno(noturno);
        result.setListaDeDadosDeFeriados(datasDoFeriado);
        return result;
    }

    private int getMonth(Date date) {
        return getValuesInDate(date, false);
    }

    private int getDay(Date date) {
        return getValuesInDate(date, true);
    }

    private int getValuesInDate(Date date, boolean isDay) {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("America/Sao_Paulo"));
        cal.setTime(date);
        if (isDay) {
            return cal.get(Calendar.DAY_OF_MONTH);
        } else {
            return cal.get(Calendar.MONTH);
        }
    }
}