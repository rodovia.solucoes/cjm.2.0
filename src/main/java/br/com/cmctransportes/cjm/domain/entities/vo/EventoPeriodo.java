package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Israel Souza
 */
public class EventoPeriodo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private String tipo;

    @Getter
    @Setter
    private Date dataInicio;

    @Getter
    @Setter
    private Date dataFim;
}
