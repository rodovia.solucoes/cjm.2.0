package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.Ferias;
import br.com.cmctransportes.cjm.infra.dao.FeriasDAO;
import br.com.cmctransportes.cjm.utils.TimeHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author William Leite
 */
@Service
public class FeriasService {
    private FeriasDAO dao;

    private final EntityManager em;

    public FeriasService(EntityManager em) {
        this.em = em;
        dao = new FeriasDAO(em);
    }

    @Transactional
    public void save(Ferias entity) {
        try {
            dao.save(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Ferias> findAll() {
        return dao.findAll();
    }

    public List<Ferias> findByDriver(Integer motorista) {
        return dao.findByDriver(motorista);
    }

    public Ferias getById(Integer id) {
        return dao.getById(id);
    }

    @Transactional
    public void update(Ferias entity) {
        try {
            Ferias original = getById(entity.getId());
            int days = Period.between(LocalDate.now(), TimeHelper.toLocalDate(original.getDataInicio())).getDays();
            if (days < 30)
                return;

            dao.update(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Transactional
    public void delete(Integer id) {
        Ferias ferias = getById(id);
        this.delete(ferias);
    }

    @Transactional
    public void delete(Ferias entity) {
        try {
            dao.delete(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    List<Date> getDiasFeriasMotoristaPeriodo(Date dataInicial, Date dataFinal, Integer motorista) {
        List<Date> result = new ArrayList<Date>();

        // Se tiver férias no peiodo solicitado
        Ferias ferias = dao.pegaFeriasDoMotoristaPorPeriodo(dataInicial, dataFinal, motorista);

        if (ferias != null) {
            // Pega os dias das férias que ainda não foram lançados
            result = dao.pegaDiasDeFeriasNoPeriodo(ferias.getDataInicio(), ferias.getDataFinal(), motorista);
        }

        return result;
    }
}