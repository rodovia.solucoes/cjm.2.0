package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class Faturamento implements Serializable {

    private Integer quantidadeDeDias;
    private List<ResumoFaturamento> listaResumoFaturamento;
    private List<TimelineFaturamento> listaTimelineFaturamento;
    private List<GraficoResumo> listaGraficoResumo;
    private DadosFaturamento dadosFaturamento;
    private String nomeDoMotorista;
    private String placaDoVeiculo;

    private Boolean isDadosViaM;
}
