package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.Enderecos;
import br.com.cmctransportes.cjm.domain.entities.PontosDeEspera;
import br.com.cmctransportes.cjm.infra.dao.PontosDeEsperaDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * @author analista.ti
 */
@Service
public class PontosDeEsperaService {
// TODO: criar GenericService

    private PontosDeEsperaDAO dao;
    @Autowired
    private EnderecosService es;

    private final EntityManager em;

    public PontosDeEsperaService(EntityManager em) {
        this.em = em;
        dao = new PontosDeEsperaDAO(em);
    }

    @Transactional
    public void save(PontosDeEspera entity) throws Exception {
        try {
            es.save(entity.getEndereco());
            dao.save(entity);
        } catch (Exception e) {
            throw e;
        }
    }

    // public T getById(PK pk) {
    public List<PontosDeEspera> findAll() {
        // return dao.findAll();
        return dao.nqFindAll();
    }

    public PontosDeEspera getById(Integer id) {
        return dao.getById(id);
    }

    @Transactional
    public void update(PontosDeEspera entity) throws Exception {
        try {
            es.update(entity.getEndereco());
            dao.update(entity);
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional
    public void delete(Integer id) throws Exception {
        try {
            PontosDeEspera entity = getById(id);
            dao.delete(entity);
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional
    public void delete(PontosDeEspera entity) throws Exception {
        try {
            Enderecos tmp = entity.getEndereco();
            dao.delete(entity);
            es.delete(tmp);
        } catch (Exception e) {
            throw e;
        }
    }
}
