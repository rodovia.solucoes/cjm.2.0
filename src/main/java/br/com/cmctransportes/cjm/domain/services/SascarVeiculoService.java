package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.SascarVeiculo;
import br.com.cmctransportes.cjm.infra.dao.SascarVeiculoDAO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

@Service
public class SascarVeiculoService {

    private final EntityManager em;
    private SascarVeiculoDAO sascarVeiculoDAO;

    public SascarVeiculoService(EntityManager em) {
        this.em = em;
        this.sascarVeiculoDAO = new SascarVeiculoDAO(em);
    }

    @Transactional
    public void save(SascarVeiculo entity) throws Exception {
        entity.setDataAtualizacao(new Date());
        entity.setManual(Boolean.FALSE);
        this.sascarVeiculoDAO.save(entity);
    }

    public List<SascarVeiculo> findAll(Integer idEmpresa) throws Exception {
        return this.sascarVeiculoDAO.findAll(idEmpresa);
    }

    public List<SascarVeiculo> findAll() throws Exception {
        return this.sascarVeiculoDAO.findAll();
    }


    public List<SascarVeiculo> findAllManual(Integer idEmpresa) throws Exception{
        return this.sascarVeiculoDAO.findAllManual(idEmpresa);
    }
    public SascarVeiculo getById(Integer id) throws Exception{
        SascarVeiculo sascarVeiculo = this.sascarVeiculoDAO.getById(id);
        return sascarVeiculo;
    }

    @Transactional
    public void update(SascarVeiculo entity) throws Exception{
        try {
            this.sascarVeiculoDAO.save(entity);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

}
