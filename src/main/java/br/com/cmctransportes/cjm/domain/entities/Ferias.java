package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Cadastro de Ferias dos Motoristas
 *
 * @author William Leite
 */
@Entity
@Table(name = "ferias")
@Getter
@Setter
public class Ferias implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "FERIAS_ID_SEQ", sequenceName = "FERIAS_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FERIAS_ID_SEQ")
    @Column(name = "id")
    private Integer id;

    @Column(name = "motorista_id", nullable = false)
    private Integer motoristaId;

    @Column(name = "data_inicio", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date dataInicio;

    @Column(name = "data_final", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date dataFinal;
}