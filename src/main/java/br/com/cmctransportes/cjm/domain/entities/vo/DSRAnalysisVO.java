package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.Empresas;
import br.com.cmctransportes.cjm.domain.entities.Motoristas;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author William Leite
 */
public class DSRAnalysisVO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private int dsrCount;
    @Getter
    @Setter
    private int weeksPassed;
    @Getter
    @Setter
    private Date start;
    @Getter
    @Setter
    private Date end;
    @Getter
    @Setter
    private boolean hasSundayDSR;
    @Getter
    @Setter
    private Motoristas driver;
    @Getter
    @Setter
    private Empresas company;
    @Getter
    @Setter
    private Map<Date, Long> probableDSRMap;
    @Getter
    @Setter
    private List<Date> availableSundays;
    @Getter
    @Setter
    private Date leastWorkedSunday;
    @Getter
    @Setter
    private Map<Date, Long> dsrLackingCompensation;
    @Getter
    @Setter
    private Map<Integer, Boolean> dsrWeek;
}