package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public class RetornoVO implements Serializable {
    private static final long serialVersionUID = 1L;

    public RetornoVO() {

    }

    public RetornoVO(Integer codigoRetorno, String mensagem) {
        this.codigoRetorno = codigoRetorno;
        this.mensagem = mensagem;
    }

    @Getter
    @Setter
    private Integer codigoRetorno;
    @Getter
    @Setter
    private String mensagem;

}
