package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.Permissao;
import br.com.cmctransportes.cjm.infra.dao.PermissaoDAO;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public class PermissaoService {

    private final EntityManager em;
    private final PermissaoDAO dao;
    public PermissaoService(EntityManager em) {
        this.em = em;
        this.dao = new PermissaoDAO(em);
    }

    public List<Permissao> buscarTodos() throws Exception{
        return dao.buscarTodos();
    }
}
