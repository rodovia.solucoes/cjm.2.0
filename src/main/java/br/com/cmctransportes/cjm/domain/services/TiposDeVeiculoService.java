/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.TiposDeVeiculo;
import br.com.cmctransportes.cjm.infra.dao.TiposDeVeiculoDAO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * @author analista.ti
 */
@Service
public class TiposDeVeiculoService {
// TODO: criar GenericService

    private TiposDeVeiculoDAO dao;

    private final EntityManager em;

    public TiposDeVeiculoService(EntityManager em) {
        this.em = em;
        dao = new TiposDeVeiculoDAO(em);
    }

    @Transactional
    public void save(TiposDeVeiculo tipo) {
        try {
            dao.save(tipo);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    // public T getById(PK pk) {
    public List<TiposDeVeiculo> findAll() {
        // return dao.findAll();
        return dao.nqFindAll();
    }

    public TiposDeVeiculo getById(Integer id) {
        return dao.getById(id);
    }

    @Transactional
    public void update(TiposDeVeiculo tipo) {
        try {
            dao.update(tipo);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Transactional
    public void delete(Integer id) {
        try {
            TiposDeVeiculo tipo = getById(id);
            dao.delete(tipo);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Transactional
    public void delete(TiposDeVeiculo tipo) {
        try {
            dao.delete(tipo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<TiposDeVeiculo> findAll(Integer idEmpresa) {
        return dao.findAll(idEmpresa);
    }
}
