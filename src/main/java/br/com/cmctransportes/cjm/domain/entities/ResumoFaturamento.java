package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ResumoFaturamento implements Serializable {

    private String placa;
    private Integer indisponivel;
    private Integer ocioso;
    private Integer faturando;
    private Integer horasIndisponivel;
    private Integer horasOcioso;
    private Integer horasFaturando;

}
