package br.com.cmctransportes.cjm.domain.entities.evento;

import br.com.cmctransportes.cjm.domain.entities.Evento;
import br.com.cmctransportes.cjm.domain.entities.RelatorioDeEvento;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author William Leite
 */
public class RelatorioRegistroDeEventos extends RelatorioDeEvento {

    private ArrayList<RegistrosDeEvento> registroEvento;

    public RelatorioRegistroDeEventos(Integer mesReferencia) {
        super(mesReferencia);
    }

    public RelatorioRegistroDeEventos(Date inicio, Date fim) {
        super(inicio, fim);
    }

    public RelatorioRegistroDeEventos(Integer mesReferencia, List<Evento> listaEventos) {
        super(mesReferencia, listaEventos);
    }

    public void setEventos(List<Evento> relatorioDeEvento) {
        registroEvento = new ArrayList<RegistrosDeEvento>();

        for (Evento evt : relatorioDeEvento) {
            RegistrosDeEvento adj = new RegistrosDeEvento();
            adj.processa(evt);
            registroEvento.add(adj);
        }
    }

    public ArrayList<RegistrosDeEvento> getLinhas() {
        return registroEvento;
    }
}
