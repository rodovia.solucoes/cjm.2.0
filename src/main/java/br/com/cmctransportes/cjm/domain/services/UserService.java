package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.Users;
import br.com.cmctransportes.cjm.domain.entities.vo.UserVO;
import br.com.cmctransportes.cjm.domain.entities.vo.UsuarioMobileVO;
import br.com.cmctransportes.cjm.domain.repository.UnidadesRepository;
import br.com.cmctransportes.cjm.domain.repository.UsersRepository;
import br.com.cmctransportes.cjm.infra.dao.EmpresasDAO;
import br.com.cmctransportes.cjm.infra.dao.UsersDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author analista.ti
 */
@Service
public class UserService {
// TODO: criar GenericService

    private final UsersDAO dao;
    private final EmpresasDAO companyDao;

    private final EntityManager em;

    @Autowired
    private UnidadesRepository unidadesRepository;

    @Autowired
    private UsersRepository repository;

    public UserService(EntityManager em) {
        this.em = em;

        dao = new UsersDAO(em);
        companyDao = new EmpresasDAO(em);
    }

    @Transactional
    public void save(Users entity) throws Exception {
        try {
            entity.getUsersCompany().forEach((uc) -> {
                uc.setCompany(companyDao.getById(uc.getCompany().getId()));
                uc.setUser(entity);
            });

            entity.getUsersBranch().forEach((ub) -> {
                ub.setBranch(this.unidadesRepository.findById(ub.getBranch().getId()).get());
                ub.setUser(entity);
            });

            entity.getUsersClaims().forEach(ucl -> {
                ucl.setUserId(entity);
            });

            this.repository.saveAndFlush(entity);
        } catch (Exception e) {
            throw e;
        }
    }

    public List<UserVO> findAll() {
        List<Users> users = this.repository.findAll();
        return users.stream().map(UserVO::new).collect(Collectors.toList());
    }

    public List<UserVO> findByCompanyAndBranch(Integer company, Integer branch) throws Exception{

        try{
          return dao.findAll(company, branch).stream().map(UserVO::new)
                    .sorted(Comparator.comparing(UserVO::getUserName))
                    .sorted(Comparator.comparing(UserVO::getSituacao).reversed())
                    .collect(Collectors.toList());
        }catch (Exception e){
            throw new Exception(e);
        }
    }

    public Users getById(Integer id) {
        return this.repository.findById(id).get();
    }

    @Transactional
    public void update(Users entity) throws Exception {
        try {
            this.repository.saveAndFlush(entity);
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional
    public void removeBranch(Integer user, Integer branch) throws Exception {
        try {
            dao.removeBranch(user, branch);
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional
    public void removeCompany(Integer user, Integer company) throws Exception {
        try {
            dao.removeCompany(user, company);
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional
    public void removeClaims(Integer id) throws Exception {
        try {
            dao.removeClaims(id);
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional
    public void delete(Integer id) throws Exception {
        try {
            this.repository.deleteById(id);
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional
    public void delete(Users entity) {
        try {
            this.repository.delete(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Users getByUserName(String e) {
        return dao.getByUserName(e);
    }

    public void logarUsuarioMobile(UsuarioMobileVO usuarioMobileVO) throws Exception{
        try{
            usuarioMobileVO.setUsuarioLogado(Boolean.FALSE);
            dao.buscarUsuarioMobile(usuarioMobileVO);
        }catch (Exception e){
            throw e;
        }
    }
}
