package br.com.cmctransportes.cjm.domain.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "inconformidade")
@Getter
@Setter
public class Inconformidade implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "incomformidade_id_seq", sequenceName = "incomformidade_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "incomformidade_id_seq")
    @Column(name = "id")
    private Integer id;

    @JsonIgnoreProperties({"eventoSeguinte", "eventoAnterior", "motivoAbono", "dayNight", "operadorLancamento", "operadorAlteracao", "veiculoMotor", "veiculoCarga01", "veiculoCarga02"})
    @JoinColumn(name = "evento", referencedColumnName = "id")
    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    private Evento evento;

    @Column(name = "data_inconformidade")
    private Date dataInconformidade;

    @Column(name = "status")
    private Boolean status;

    @Column(name = "lido")
    private Boolean lido;

    @JsonIgnoreProperties({"usersClaims", "usersBranch", "usersCompany"})
    @JoinColumn(name = "usuario", referencedColumnName = "id")
    @ManyToOne
    private Users usuario;

    @Column(name = "data_leitura")
    private Date dataLeitura;

    @Column(name = "nome")
    private String nome;

    @Column(name = "observacao")
    private String observacao;

    @JsonIgnoreProperties({"enderecosId", "objEmpresa", "turnosId", "ferias", "veiculos", "user", "imagemId"})
    @JoinColumn(name = "motorista", referencedColumnName = "id")
    @ManyToOne
    private Motoristas motoristas;


    @JsonIgnoreProperties({"empresaPrincipal", "parametro"})
    @JoinColumn(name = "empresa", referencedColumnName = "id")
    @ManyToOne
    private Empresas empresa;

    @Column(name = "unidade")
    private Integer unidade;

    @Column(name = "id_veiculo")
    private Integer idVeiculo;

    @Column(name = "placa")
    private String placa;

    @Transient
    private String dataInconformidadeFormatada;
    @Transient
    private String dataLeituraFormatada;
    @Transient
    private String dataFormatada;

    public String getDataFormatada() {
        if(Objects.nonNull(this.dataInconformidade)){
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM HH:mm");
            this.dataFormatada = simpleDateFormat.format(this.getDataInconformidade());
        }
        return dataFormatada;
    }

    public void setDataFormatada(String dataFormatada) {
        this.dataFormatada = dataFormatada;
    }
}
