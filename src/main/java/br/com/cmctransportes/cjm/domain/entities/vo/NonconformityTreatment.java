package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.enums.JOURNEY_TYPE;
import br.com.cmctransportes.cjm.domain.entities.enums.TREATMENT_TYPE;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * @author William Leite
 */
public class NonconformityTreatment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private Date startDate;
    @Getter
    @Setter
    private Date endDate;
    @Getter
    @Setter
    private TREATMENT_TYPE type;
    @Getter
    @Setter
    private JOURNEY_TYPE previousType;

    @Getter
    @Setter
    private Integer userId;
    @Getter
    @Setter
    private Integer driverId;
    @Getter
    @Setter
    private Integer journeyId;
}
