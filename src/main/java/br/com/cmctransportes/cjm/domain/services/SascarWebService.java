package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.*;
import br.com.cmctransportes.cjm.domain.repository.EventoRepository;
import br.com.cmctransportes.cjm.domain.repository.JourneyRepository;
import br.com.cmctransportes.cjm.infra.dao.EventoDAO;
import br.com.cmctransportes.cjm.infra.dao.JornadaDAO;
import br.com.cmctransportes.cjm.sascarsoap.PacotePosicao;
import br.com.cmctransportes.cjm.sascarsoap.SasIntegraNotification;
import br.com.cmctransportes.cjm.sascarsoap.SasIntegraWS;
import br.com.cmctransportes.cjm.sascarsoap.SasIntegraWSService;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Desenvolvido por Cleber Leão
 */
@Service
public class SascarWebService {

    @Autowired
    private DriverService driverService;

    @Autowired
    private RastroService rastroService;

    @Autowired
    private EventoRepository eventoRepository;

    @Autowired
    private JourneyRepository journeyRepository;

    @Getter
    private JornadaDAO dao;

    @Autowired
    private final EntityManager em;

    private final EventoDAO eventoDAO;

    private static String usuarioSascar = "VIAMRODOVIA";
    private static String senhaSascar = "j3iaB7]x";

    public SascarWebService(EntityManager em){
        this.em = em;
        eventoDAO = new EventoDAO(em);
        dao = new JornadaDAO(em);
    }

    /**
     * Classe usuada para buscar no SOAP Sascar os eventos registrados das ultimas 48 horas
     * @param dataInicio SOAP necessita de uma data inicial de consulta
     * @param dataFim SOAP necessita de uma data fim da consulta
     * @param idVeiculo ID do veiculo com liberação na SASCAR para consulta no SOAP
     * @return todos eventos do motorista na data informada em uma Lista
     * @throws SasIntegraNotification
     */
    public List<PacotePosicao> ConsultaEventosSascar(String dataInicio, String dataFim, Integer idVeiculo) throws SasIntegraNotification {
        SasIntegraWS sasIntegraWSService = new SasIntegraWSService().getSasIntegraWSPort();
        List<PacotePosicao> eventosSascar = sasIntegraWSService.obterPacotePosicaoMotoristaHistorico(this.usuarioSascar, this.senhaSascar, dataInicio, dataFim, idVeiculo);
        return eventosSascar;
    }

    /**
     * Recebe do controller os dados iniciais para tratamento e criação dos eventos
     * @param motoristaId motorista que está sendo consultado
     * @param dataInicio  SOAP necessita de uma data inicial de consulta
     * @param dataFim SOAP necessita de uma data fim da consulta
     * @return se foi bem sucessido a consulta ou não para o Controller
     * @throws SasIntegraNotification exeption da Sascar SOAP
     */
    public String ServerEvent(Integer motoristaId, long dataInicio, long dataFim) throws SasIntegraNotification {
        String dateInicio = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(dataInicio));
        String dateFim = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(dataFim));

        Motoristas motoristas = this.driverService.getById(motoristaId);
        Empresas empresas = motoristas.getEmpresa();
        Integer idVeiculo = motoristas.getSascarId();

        SascarWebService sascarWebService = new SascarWebService(em);
        List<PacotePosicao> eventosSascar = this.ConsultaEventosSascar(dateInicio, dateFim ,idVeiculo);

        List<Evento> eventos = this.readService(eventosSascar,  motoristaId, empresas.getId());
        this.saveRecords(eventos, motoristas, empresas);
        try {
            return "Importação de 48 horas concluído com sucesso";
        }catch (Exception e) {
            e.printStackTrace();
           return String.valueOf(e);
        }

    }

    /**
     * Cria o evento com os dados da lista de eventos extraidas do SOAP
     * @param eventosSascar Lista de eventos
     * @param motoristaId Id do motorista
     * @param empresaId Id da empresa atrelada ao motorista
     * @return uma lista de Eventos no padrão ViaM
     */
    private List<Evento> readService(List<PacotePosicao> eventosSascar, Integer motoristaId, Integer empresaId) {
        List<Evento> eventos = new ArrayList<>();

        for(PacotePosicao split : eventosSascar){
            Integer tipoEventoArquivo = split.getCodigoMacro();
            String conteudoMensagem = split.getConteudoMensagem();
            Integer tipoEvento = convertEventType(tipoEventoArquivo, conteudoMensagem);
            Date instanteLancamento = toDate(split.getDataPosicao());
            Date instanteEvento = toDate(split.getDataPacote());
            Double latitude = split.getLatitude();
            Double longitude = split.getLongitude();
            String pontoParada = split.getPontoReferencia() + " Rua: " + split.getRua() + " Cidade: " + split.getCidade();

            if(tipoEvento != 55) {
                Evento eventoArquivo = this.createEvento(instanteEvento, instanteLancamento, latitude, longitude, tipoEvento, pontoParada,motoristaId, empresaId);
                eventos.add(eventoArquivo);
            } else{
                this.createRastro(instanteEvento, instanteLancamento, latitude, longitude, tipoEvento, pontoParada, motoristaId, empresaId);
            }
        }
        return eventos;
    }

    /**
     * Cria eventos na tabela restro quando o veículo está em transito ou até mesmo parado
     * @param instanteEvento instante do evento
     * @param instanteLancamento instante do envio ao servidor
     * @param latitude Latitude
     * @param longitude Longitude
     * @param tipoEvento Qual o evento do VIAM
     * @param pontoParada Local da localização no momento
     * @param motoristaId id motorista
     * @param empresaId id empresa
     */
    private void createRastro(Date instanteEvento, Date instanteLancamento, Double latitude, Double longitude, Integer tipoEvento, String pontoParada,Integer motoristaId, Integer empresaId) {
        Rastro result = new Rastro();
        result.setEmpresaId(new Empresas(empresaId));
        result.setMotoristaId(motoristaId);
        result.setInstante(instanteEvento);
        result.setInstanteEnvio(instanteLancamento);
        result.setLatitude(latitude);
        result.setLongitude(longitude);
        rastroService.save(result);
    }

    /**
     * Cria o Evento Sascar no padrão do Evento ViaM
     * @param instanteEvento instante Evento
     * @param instanteLancamento instante Lancamento
     * @param latitude latitude
     * @param longitude longitude
     * @param tipoEvento tipo de Evento viaM
     * @param pontoParada Local da localização no momento
     * @param motoristaId id motorista
     * @param empresaId  id empresa que o motorista está vinculado
     * @return
     */
    public Evento createEvento(Date instanteEvento, Date instanteLancamento, Double latitude, Double longitude, Integer tipoEvento, String pontoParada,Integer motoristaId, Integer empresaId) {
        if (Objects.isNull(tipoEvento)) {
            return null;
        } else if (Objects.isNull(instanteEvento)) {
            return null;
        } else if (Objects.isNull(instanteLancamento)) {
            return null;
        }
        Evento result = new Evento();
        result.setInstanteEvento(instanteEvento);
        result.setInstanteLancamento(instanteLancamento);
        result.setLatitude(latitude);
        result.setLongitude(longitude);
        result.setTipoEvento(new EventosJornada(tipoEvento));
        result.setPositionAddress(pontoParada);
        result.setOperadorLancamento(new Users(motoristaId));
        result.setEmpresaId(new Empresas(empresaId));
        return result;
    }

    /**
     * Salva o evento no Banco
     * @param eventos Lista de Eventos
     * @param motoristas qual motorista pertence os eventos
     * @param empresas qual empresa está vinculado o motorista
     */
    private void saveRecords(List<Evento> eventos, Motoristas motoristas, Empresas empresas) {
        final AtomicReference<Jornada> atomicJornada = new AtomicReference<>();
        final List<Evento> eventosJornada = new ArrayList<>();

        int[] eventosEterno = new int[]{2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 15, 16, 23, 25};
        final AtomicReference<Integer> isEterno = new AtomicReference<>(null);

        eventos.stream().sorted(Comparator.comparing(Evento::getInstanteEvento)).forEach(evento -> {

            if (evento.getTipoEvento().getId() == 1) {
                Jornada jornada = new Jornada();
                jornada.setMotoristasId(motoristas);
                jornada.setEmpresaId(empresas);
                jornada.setDsrHE(Boolean.FALSE);
                jornada.setWaitingAsWorked(Boolean.FALSE);
                jornada.setLocked(Boolean.FALSE);
                jornada = this.journeyRepository.saveAndFlush(jornada);
                atomicJornada.set(jornada);
            }else{
                atomicJornada.set(this.dao.getLastJourney(motoristas.getId()));
            }

            if (Objects.nonNull(isEterno.get())) {
                Date instanteEvento = new Date(evento.getInstanteEvento().getTime() - 1000);
                Date instanteLancamento = new Date(evento.getInstanteLancamento().getTime() - 1000);
                Integer tipoEvento = isEterno.get() * -1;
                String pontoParada ="Final Automático evento eterno";
                Evento eventoFim = this.createEvento(instanteEvento, instanteLancamento, evento.getLatitude(), evento.getLongitude(), tipoEvento, pontoParada, motoristas.getId(), empresas.getId());
                eventoFim.setJornadaId(atomicJornada.get());eventosJornada.add(this.eventoDAO.address(eventoFim));
                isEterno.set(null);
            }

            if (Arrays.binarySearch(eventosEterno, evento.getTipoEvento().getId()) >= 0) {
                isEterno.set(evento.getTipoEvento().getId());
            }
            evento.setJornadaId(atomicJornada.get());
            eventosJornada.add(this.eventoDAO.address(evento));
            // Fim jornada
            if (evento.getTipoEvento().getId() == 0) {
                this.eventoRepository.saveAll(eventosJornada);
                eventosJornada.clear();
                atomicJornada.set(null);
            }
        });
    }

    /**
     * converte dateXML em milessegundos
     * @param dataXml data no formato padrão do XML
     * @return data em milesegundos
     */
    private Date toDate(XMLGregorianCalendar dataXml) {
        return dataXml.toGregorianCalendar().getTime();
    }

    /**
     * "De Para" dos eventos Sascar para os Eventos ViaM
     * @param tipoEventoArquivo Tipo de evento do SOAP Sascar
     * @return Tipo de evento no ViaM
     */
    public Integer convertEventType(Integer tipoEventoArquivo, String conteudoMensagem) {
        Integer result = null;
        if (tipoEventoArquivo.equals(0)) { //rastro
            result = 55;
        } else if (tipoEventoArquivo.equals(1)) { //se der null é pq veio 1
            System.out.println("evento macro 1");
            result = null;
        } else if (tipoEventoArquivo.equals(2)) { //inicio de jornada = 1
            result = 1;
        }else if (tipoEventoArquivo.equals(3)) { //inicio de direção = 8
            result = 8;
        } else if (tipoEventoArquivo.equals(4)) { // fim de direção
            result = -8;
        }else if (tipoEventoArquivo.equals(5)) { //INICIO AGUARDANDO CARGA = 9
            result = 9;
        } else if (tipoEventoArquivo.equals(6)) { //Inicio Aguardando descarga = 11
            result = 11;
        }  else if (tipoEventoArquivo.equals(7)) { //inicio refeição = 14
            result = 14;
        } else if (tipoEventoArquivo.equals(8)) { //fim de refeição = -14
            result = -14;
        } else if (tipoEventoArquivo.equals(9)) { //Inicio de Carga = 10
            result = 10;
        }else if (tipoEventoArquivo.equals(10)) { //INICIO DE FISCALIZACAO = 13
            result =13 ;
        } else if (tipoEventoArquivo.equals(11)) { //fim de carga = -10
            result = -10;
        } else if (tipoEventoArquivo.equals(12)) { //inicio manifesto
            result = 2;
        }else if (tipoEventoArquivo.equals(13)) { //fim manifesto
            result = -2;
        }else if (tipoEventoArquivo.equals(14)) {
            result = null;
        }else if (tipoEventoArquivo.equals(15)) { // IDENTIFICAR ABASTECIMENTO 7
            result = null;
        }else if (tipoEventoArquivo.equals(16)) { //INICIO DE PARADA (abastecimento 7, lanche = 5, banheiro = 6, pista interditada 4, intervalo pessoal 25)
            if(conteudoMensagem.contains("_X___________")){
                result = 7;
            } else if(conteudoMensagem.contains("___X_________")) {
                result = 6;
            } else if(conteudoMensagem.contains("_____X_______")) {
                result = 5;
            } else if(conteudoMensagem.contains("_______X_____")) {
                result = 25;
            } else if(conteudoMensagem.contains("_________X___")) {
                result = 4;
            } else if(conteudoMensagem.contains("___________X_")) {
                result = 23;
            }
        } else if (tipoEventoArquivo.equals(17)) { //FIM DE PARADA (abastecimento 7, lanche = 5, banheiro = 6, pista interditada 4, intervalo pessoal 25)
            if(conteudoMensagem.contains("_X___________")){
                result = -7;
            } else if(conteudoMensagem.contains("___X_________")) {
                result = -6;
            } else if(conteudoMensagem.contains("_____X_______")) {
                result = -5;
            } else if(conteudoMensagem.contains("_______X_____")) {
                result = -25;
            } else if(conteudoMensagem.contains("_________X___")) {
                result = -4;
            } else if(conteudoMensagem.contains("___________X_")) {
                result = -23;
            }
        } else if (tipoEventoArquivo.equals(20)) { //repouso de direção = 15
            result = 15;
        } else if (tipoEventoArquivo.equals(21)) { // Fim repouso direção = -15
            result = -15;
        } else if (tipoEventoArquivo.equals(22)) { //Inicio Manutenção = 3
            result = 3;
        } else if (tipoEventoArquivo.equals(23)) { //FIM de manutenção = -3
            result = -3;
        } else if (tipoEventoArquivo.equals(30)) { //fim de jornada
            result = 0;
        } else if (tipoEventoArquivo.equals(31)) { //fim de aguardando carga
            result = -9;
        } else if (tipoEventoArquivo.equals(32)) { //FIM Aguardando descarga
            result = -11;
        } else if (tipoEventoArquivo.equals(33)) { //fim de fiscalizção = -13
            result = -13;
        } else if (tipoEventoArquivo.equals(34)) { //Inicio de descarga
            result = 12;
        } else if (tipoEventoArquivo.equals(35)) { //FIM de descarga
            result = -12;
        }
        return result;
    }
}