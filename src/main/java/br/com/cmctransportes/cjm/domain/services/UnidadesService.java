package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.Empresas;
import br.com.cmctransportes.cjm.domain.entities.Enderecos;
import br.com.cmctransportes.cjm.domain.entities.Unidades;
import br.com.cmctransportes.cjm.domain.repository.UnidadesRepository;
import br.com.cmctransportes.cjm.infra.dao.EmpresasDAO;
import br.com.cmctransportes.cjm.infra.dao.UnidadesDAO;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Objects;

/**
 * @author analista.ti
 */
@Service
public class UnidadesService {
    @Autowired
    private EnderecosService es;

    @Autowired
    private UnidadesRepository repository;

    private UnidadesDAO unidadesDAO;
    private EmpresasDAO empresasDAO;


    public UnidadesService(EntityManager em){
        this.unidadesDAO = new UnidadesDAO(em);
        this.empresasDAO = new EmpresasDAO(em);
    }

    @Transactional
    public void save(Unidades entity) throws Exception {
        Enderecos tmpEndereco = entity.getEndereco();
        if (Objects.isNull(tmpEndereco.getEmpresaId())) {
            tmpEndereco.setEmpresaId(entity.getEmpresaId().getId());
        }
        ajustarValorDiaria(entity);
        try {
            try {
                es.save(tmpEndereco);
            } catch (Exception e) {
                tmpEndereco.setId(null);
                throw e;
            }

            this.repository.saveAndFlush(entity);
        } catch (Exception e) {
            e.printStackTrace();
            es.delete(tmpEndereco);
            tmpEndereco.setId(null);
            entity.setId(null);
            throw e;
        }
    }

    public List<Unidades> findAll() {
        List<Unidades> lista = this.repository.findAll();
        lista.forEach(l->{
            converterDoubleEmString(l);
        });
        return lista;
    }

    public Unidades getById(Integer id) {
        Unidades unidades = this.repository.findById(id).get();
        converterDoubleEmString(unidades);
        return unidades;
    }

    @Transactional
    public void update(Unidades entity) {
        try {
            ajustarValorDiaria(entity);
            es.update(entity.getEndereco());
            this.repository.saveAndFlush(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void delete(Integer id) {
        try {
            Unidades entity = getById(id);
            this.repository.delete(entity);
            es.delete(entity.getEndereco());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void delete(Unidades entity) {
        try {
            this.repository.delete(entity);
            es.delete(entity.getEndereco());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Unidades> findAll(Integer idEmpresa) {
        return this.repository.findByEmpresaIdId(idEmpresa);
    }



    private void ajustarValorDiaria(Unidades unidades) {
        try {
            if (Objects.nonNull(unidades) &&  Objects.nonNull(unidades.getValorDiaria())) {
                Double valorDiaria = RodoviaUtils.convertStringToDouble(unidades.getValorDiaria());
                unidades.setValorDiariaNumerico(valorDiaria);
            }
        } catch (Exception e) {
        }

    }

    private void converterDoubleEmString(Unidades unidades){
        try {
            if (Objects.nonNull(unidades) &&  Objects.nonNull(unidades.getValorDiariaNumerico())) {
                String valorDiaria = RodoviaUtils.convertDoubleToMoneyReal(unidades.getValorDiariaNumerico());
                unidades.setValorDiaria(valorDiaria);
            }
        }catch (Exception e){

        }
    }

    public Double buscarValorDiaria(Integer idMotorista){
        Double valor = unidadesDAO.buscarValorDiaria(idMotorista);
        //Se valor igual a zero tenta buscar
        if(valor == 0d){
            valor =  empresasDAO.buscarValorDiaria(idMotorista);
        }
        return valor;
    }

}
