package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.Jornada;
import br.com.cmctransportes.cjm.domain.entities.Unidades;
import br.com.cmctransportes.cjm.domain.services.UnidadesService;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * @author analista.ti
 */
public class ResumoJornada implements Serializable {

    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private Integer id; // id da jornada
    @Getter
    @Setter
    private String nomeMotorista; // nome do motorista;
    @Getter
    @Setter
    private String sobrenomeMotorista; // nome do motorista;
    @Getter
    @Setter
    private String unidade; // identificação da unidade;
    @Getter
    @Setter
    private Date inicio; // instante inicial da jornada
    @Getter
    @Setter
    private Date ultimoLancamento; // instante do ultimo lancamento nesta jornada
    @Getter
    @Setter
    private Boolean finalizada; // se a jornada já está finalizada ou não
    @Getter
    @Setter
    private String tipoEvento; // tipo do evento
    @Getter
    @Setter
    private Double latitude;
    @Getter
    @Setter
    private Double longitude;
    @Getter
    @Setter
    private String telefone;
    @Getter
    @Setter
    private Boolean dsrHE;
    @Getter
    @Setter
    private Long worked;
    @Getter
    @Setter
    private Boolean waitingAsWorked;
    @Getter
    @Setter
    private Long waiting;
    @Getter
    @Setter
    private Long missing;
    @Getter
    @Setter
    private Boolean locked;
    @Getter
    @Setter
    private Boolean diariaMotorista;


    public ResumoJornada() {
    }

    public ResumoJornada(Integer id) {
        this.id = id;
    }

    public static ResumoJornada convertToResumoJornada(Jornada journey, UnidadesService us) {
        ResumoJornada result = new ResumoJornada();

        try {
            result.setId(journey.getId());
            result.setNomeMotorista(journey.getMotoristasId().getNome());
            result.setSobrenomeMotorista(journey.getMotoristasId().getSobrenome());

            Unidades unidade = us.getById(journey.getMotoristasId().getUnidadesId());
            result.setUnidade(unidade.getApelido());
            result.setInicio(journey.getJornadaInicio());
            result.setUltimoLancamento(journey.getJornadaFim());

            if (Objects.nonNull(journey.getType())) {
                result.setTipoEvento(journey.getType().toString());
            }

            result.setLatitude(journey.getLatitude());
            result.setLongitude(journey.getLongitude());

            result.setTelefone(journey.getMotoristasId().getTelefone());

            result.setDsrHE(journey.getDsrHE());
            result.setWorked(journey.getTrabalhado());

            result.setWaitingAsWorked(journey.getWaitingAsWorked());

            Long esperaDiurno = Objects.nonNull(journey.getEsperaDiurno()) ? journey.getEsperaDiurno() : 0L;
            Long esperaNoturno = Objects.nonNull(journey.getEsperaNoturno()) ? journey.getEsperaNoturno() : 0L;

            result.setWaiting(esperaDiurno + esperaNoturno);

            result.setMissing(journey.getHorasFaltosas());
            result.setLocked(Objects.equals(journey.getLocked(), Boolean.TRUE));
            result.setDiariaMotorista(Objects.equals(journey.getDiariaMotorista(), Boolean.TRUE));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }


}