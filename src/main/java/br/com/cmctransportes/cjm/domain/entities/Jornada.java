package br.com.cmctransportes.cjm.domain.entities;

import br.com.cmctransportes.cjm.domain.entities.enums.JOURNEY_TYPE;
import br.com.cmctransportes.cjm.domain.entities.jornada.Resumo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

/**
 * @author analista.ti
 */
@Entity
@Table(name = "jornada")
@Getter
@Setter
@NamedQueries({
        @NamedQuery(name = "Jornada.findAll", query = "SELECT j FROM Jornada j"),
        @NamedQuery(name = "Jornada.findById", query = "SELECT j FROM Jornada j WHERE j.id = :id")})
public class Jornada implements Serializable {

    public static final int TP_EVENTO = 0;
    public static final int TP_ESTADO = 1;
    public static final int TP_TIPOESTADO = 2;

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "JORNADA_ID_SEQ", sequenceName = "JORNADA_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "JORNADA_ID_SEQ")
    @Column(name = "id")
    private Integer id;

    @JsonIgnore
    @JoinColumn(name = "empresa_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Empresas empresaId;

    @JsonIgnoreProperties({"enderecosId"})
    @JoinColumn(name = "motoristas_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Motoristas motoristasId;

    @JsonManagedReference("jornadaId")
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "jornadaId")
    @Where(clause = "removido = false")
    @OrderBy("instanteEvento, id")
    private Collection<Evento> eventos;

    @Column(name = "dsr_he")
    private Boolean dsrHE;

    @Column(name = "waiting_as_worked")
    private Boolean waitingAsWorked;

    @Column(name = "locked")
    private Boolean locked;

    @Column(name = "diaria_motorista")
    private Boolean diariaMotorista;

    @Column(name = "horas_extras_abonada")
    private Boolean horasExtrasAbonada;

    @Column(name = "horas_extras_cinquenta_porcento ")
    private Boolean horasExtrasCinquentaPorcento;

    @Transient
    private Date jornadaInicio;

    @Transient
    private Date jornadaFim;

    @Transient
    private Long horasExtras;

    @Transient
    private Long trabalhado;

    @Transient
    private Long hedFeriado;

    @Transient
    private Long henFeriado;

    @Transient
    private Long hedDSR;

    @Transient
    private Long henDSR;

    @Transient
    private Long horasFaltosas;

    @Transient
    @JsonIgnore
    private Long horasAbonadas;

    @Transient
    @JsonIgnore
    private Long horasExtrasDiurnas;

    @Transient
    @JsonIgnore
    private Long horasExtrasNoturnas;

    @Transient
    @JsonIgnore
    private Long hedIntraJornada;

    @Transient
    @JsonIgnore
    private Long henIntraJornada;

    @Transient
    @JsonIgnore
    private Long adicionalNoturno;

    @Transient
    private Long esperaDiurno;

    @Transient
    private Long esperaNoturno;

    @Transient
    private Long descontoAlmoco;

    @Transient
    private Map<String, Resumo> resumoTiposEstados;

    @Transient
    @JsonIgnore
    private Map<String, Resumo> resumoEstados;

    @Transient
    private JOURNEY_TYPE type;

    @Transient
    private Double latitude;

    @Transient
    private Double longitude;

    @Transient
    private String textoLivre;

    public Jornada() {
    }

    public Jornada(Integer id) {
        this.id = id;
    }

    public Jornada(Empresas empresa, Motoristas driver) {
        this.empresaId = empresa;
        this.motoristasId = driver;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jornada)) {
            return false;
        }
        Jornada other = (Jornada) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.cmctransportes.cjm.domain.entities.Jornada[ id=" + id + " ]";
    }
}