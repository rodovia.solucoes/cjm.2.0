package br.com.cmctransportes.cjm.domain.repository;

import br.com.cmctransportes.cjm.domain.entities.Evento;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventoRepository extends JpaRepository<Evento, Integer> {
}
