package br.com.cmctransportes.cjm.domain.repository;

import br.com.cmctransportes.cjm.domain.entities.Cidades;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CidadesRepository extends JpaRepository<Cidades, String> {
}
