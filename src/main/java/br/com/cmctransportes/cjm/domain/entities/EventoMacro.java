package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "evento_macro")
@Getter
@Setter
public class EventoMacro implements Serializable {

    @Id
    @SequenceGenerator(name = "evento_macro_id_seq", sequenceName = "evento_macro_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "evento_macro_id_seq")
    @Column(name = "id")
    private Integer id;

    @Column(name = "evento", length = 100, nullable = false)
    private String evento;

    @Column(name = "valor")
    private Integer valor;

    @Column(name = "id_macro")
    private Integer idMacro;

}
