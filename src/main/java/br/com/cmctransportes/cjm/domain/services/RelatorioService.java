package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.constants.Constantes;
import br.com.cmctransportes.cjm.domain.entities.*;
import br.com.cmctransportes.cjm.domain.entities.vo.*;
import br.com.cmctransportes.cjm.infra.dao.EventoVeiculoDAO;
import br.com.cmctransportes.cjm.infra.dao.RankingMotoristaDAO;
import br.com.cmctransportes.cjm.infra.dao.TransmissaoDAO;
import br.com.cmctransportes.cjm.infra.dao.VeiculosDAO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.proxy.RelatorioTrajetto;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import br.com.cmctransportes.cjm.utils.TimeHelper;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanArrayDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.joda.time.Seconds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.io.*;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class RelatorioService {

    @Autowired
    private UnidadesService unidadesService;
    @Autowired
    private JornadaService jornadaService;
    @Autowired
    private EmpresasService empresasService;
    @Autowired
    private EventoMotoristaService eventoMotoristaService;


    private String diretorioJasper;
    private String diretorioGravacao;
    private String nomeRelatorio;
    private boolean isSubrelatorio = false;


    public RetornoRelatorioVO buscarRelatorioDeAcionamentoDeCompreensor(FiltroVO filtroVO) {
        ajustarDatas(filtroVO);
        return RelatorioTrajetto.getInstance().buscarRelatorioDeAcionamentoDeCompreensor(filtroVO);
    }

    public RetornoRelatorioVO buscarRelatorioAcionamentoTomadaDeForca(FiltroVO filtroVO) {
        ajustarDatas(filtroVO);
        return RelatorioTrajetto.getInstance().buscarRelatorioAcionamentoDePrancha(filtroVO);
    }

    public RetornoRelatorioVO buscarRelatorioDisponibilidade(FiltroVO filtroVO) {
        ajustarDatas(filtroVO);
        return RelatorioTrajetto.getInstance().buscarRelatorioDisponibilidade(filtroVO);
    }

    public RetornoRelatorioVO buscarRelatorioDisponibilidadeGrafico(FiltroVO filtroVO) {
        ajustarDatas(filtroVO);
        return RelatorioTrajetto.getInstance().buscarRelatorioDisponibilidadeGrafico(filtroVO);
    }

    public RetornoRelatorioVO buscarRelatorioEvento(FiltroVO filtroVO) {
        ajustarDatas(filtroVO);
        return RelatorioTrajetto.getInstance().buscarRelatorioEvento(filtroVO);
    }

    public RetornoRelatorioVO buscarRelatorioHorimetro(FiltroVO filtroVO) {
        ajustarDatas(filtroVO);
        return RelatorioTrajetto.getInstance().buscarRelatorioHorimetro(filtroVO);
    }

    public RetornoRelatorioVO buscarRelatorioPercurso(FiltroVO filtroVO) {
        ajustarDatas(filtroVO);
        return RelatorioTrajetto.getInstance().buscarRelatorioPercurso(filtroVO);
    }

    public RetornoRelatorioVO buscarRelatorioVelocidade(FiltroVO filtroVO) {
        ajustarDatas(filtroVO);
        return RelatorioTrajetto.getInstance().buscarRelatorioVelocidade(filtroVO);
    }


    public RetornoRelatorioVO buscarRelatorioPercursoPortatil(FiltroVO filtroVO) {
        ajustarDatas(filtroVO);
        return RelatorioTrajetto.getInstance().buscarRelatorioPercursoPortatil(filtroVO);
    }

    public RetornoRelatorioVO buscarRelatorioPresencaDeEquipamentos(FiltroVO filtroVO) {
        ajustarDatas(filtroVO);
        return RelatorioTrajetto.getInstance().buscarRelatorioPresencaDeEquipamentos(filtroVO);
    }

    public RetornoRelatorioVO buscarRelatorioTransporteDeEquipamentos(FiltroVO filtroVO) {
        ajustarDatas(filtroVO);
        return RelatorioTrajetto.getInstance().buscarRelatorioTransporteDeEquipamentos(filtroVO);
    }

    public RetornoRelatorioVO buscarGraficoProducaoEquipamento(FiltroVO filtroVO) {
        ajustarDatas(filtroVO);
        return RelatorioTrajetto.getInstance().buscarGraficoProducaoEquipamento(filtroVO);
    }


    public RetornoRelatorioVO buscarSensorBetoneira(FiltroVO filtroVO) {
        ajustarDatas(filtroVO);
        return RelatorioTrajetto.getInstance().buscarSensorBetoneira(filtroVO);
    }

    public RetornoRelatorioVO buscarNumeroDeViagem(FiltroVO filtroVO) {
        ajustarDatas(filtroVO);
        return RelatorioTrajetto.getInstance().buscarNumeroDeViagem(filtroVO);
    }

    public RetornoRelatorioVO buscarSensorPorta(FiltroVO filtroVO) {
        ajustarDatas(filtroVO);
        Empresas empresas = empresasService.getById(filtroVO.getIdCliente());
        filtroVO.setIdCliente(empresas.getIdClienteTrajetto());
        return RelatorioTrajetto.getInstance().buscarSensorPorta(filtroVO);
    }

    public RetornoRelatorioVO buscarRelatorioReferenciaLocal(FiltroVO filtroVO) {
        ajustarDatas(filtroVO);
        Empresas empresas = empresasService.getById(filtroVO.getIdCliente());
        filtroVO.setIdCliente(empresas.getIdClienteTrajetto());
        return RelatorioTrajetto.getInstance().buscarRelatorioReferenciaLocal(filtroVO);
    }

    public RetornoRelatorioVO buscarRelatorioReferenciaLocalDetalhes(FiltroVO filtroVO) {
        ajustarDatas(filtroVO);
        Empresas empresas = empresasService.getById(filtroVO.getIdCliente());
        filtroVO.setIdCliente(empresas.getIdClienteTrajetto());
        return RelatorioTrajetto.getInstance().buscarRelatorioReferenciaLocalDetalhes(filtroVO);
    }


    public RetornoRelatorioVO buscarRelatorioPercursoDetalhado(FiltroVO filtroVO) {
        ajustarDatas(filtroVO);
        return RelatorioTrajetto.getInstance().buscarPercursoDetalhado(filtroVO);
    }

    public RetornoRelatorioVO buscarRelatorioEventoSensoresDetalhado(FiltroVO filtroVO) {
        ajustarDatas(filtroVO);
        return RelatorioTrajetto.getInstance().buscarEventoSensoresDetalhado(filtroVO);
    }


    public RetornoRelatorioVO buscarPontosPercorridos(FiltroVO filtroVO) {
        ajustarDatas(filtroVO);
        return RelatorioTrajetto.getInstance().buscarPontosPercorridos(filtroVO);
    }


    public RetornoRelatorioVO buscarRelatorioEventosMotoristas(FiltroVO filtroVO) {

        VeiculosDAO veiculosDAO =  new VeiculosDAO(null);
        ajustarDatas(filtroVO);
        List<InformacaoRelatorioVO> listaInformacaoRelatorio = new ArrayList<>();

        List<EventoMotorista> lista = eventoMotoristaService.buscarPorPeriodos(filtroVO.getListaDeMotoristas(), filtroVO.getDataInicial(), filtroVO.getDataFinal(), filtroVO.getTipoEventoMotorista());
        SimpleDateFormat sf = new SimpleDateFormat("dd/MM HH:mm");
        Map<Integer, List<EventoMotorista>> map = new HashMap<>();
        for(EventoMotorista em : lista){
           if(map.containsKey(em.getMotoristas().getId())){
               List<EventoMotorista> l = map.get(em.getMotoristas().getId());
               em.setPlaca(l.get(0).getPlaca());
               l.add(em);
           }else{
               Veiculos veiculos = veiculosDAO.buscarVeiculoPeloMotorista(em.getMotoristas().getId());
               if(Objects.isNull(veiculos)){
                   em.setPlaca("-");
               }else{
                   em.setPlaca(veiculos.getPlaca());
               }
               List<EventoMotorista> l = new ArrayList<>();
               l.add(em);
               map.put(em.getMotoristas().getId(), l);
           }
        }


        for (Map.Entry<Integer, List<EventoMotorista>> entry : map.entrySet()) {
            List<EventoMotorista> l = entry.getValue();
            l.sort((s1, s2) -> {
                return s1.getDataEvento().compareTo(s2.getDataEvento());
            });

            InformacaoRelatorioVO vo = new InformacaoRelatorioVO();
            String nome = l.get(0).getMotoristas().getNome() + " " + l.get(0).getMotoristas().getSobrenome();
            vo.setCampo01(nome);

            List<InformacaoRelatorioVO> voList = new ArrayList<>();
            vo.setListaInformacaoRelatorio(voList);
            Integer tTotalCarregado = 0;
            Integer tTotalVazio = 0;
            Integer tHodTotalVazio = 0;
            Integer tHodTotalCarreqado = 0;
            for(EventoMotorista em : l){
                InformacaoRelatorioVO vo2 = new InformacaoRelatorioVO();
                vo2.setCampo01(em.getPlaca());
                vo2.setCampo02(em.getTipo().equals(1)?"Carregado":"Vazio");
                vo2.setCampo03(sf.format(em.getDataEvento()));
                vo2.setCampo04(sf.format(em.getDataFimEvento()));
                int tempoTotal = Minutes.minutesBetween(new DateTime(em.getDataEvento()), new DateTime(em.getDataFimEvento())).getMinutes();
                if(em.getTipo().equals(1)){
                    tTotalCarregado = tTotalCarregado + tempoTotal;
                }else{
                    tTotalVazio = tTotalVazio + tempoTotal;
                }

                vo2.setCampo05(TimeHelper.converteMinutosEmStringFormatadaTres(tempoTotal));
                vo2.setCampo06(em.getHodometroInicial().toString());
                vo2.setCampo07(em.getHodometroFinal().toString());
                Integer diferenca = em.getHodometroFinal() - em.getHodometroInicial();
                if(diferenca > 1000 || diferenca < 0){
                    vo2.setCampo08("Error");
                }else{
                    if(em.getTipo().equals(1)){
                        tHodTotalCarreqado = tHodTotalCarreqado + diferenca;
                    }else{
                        tHodTotalVazio = tHodTotalVazio + diferenca;
                    }
                    vo2.setCampo08(diferenca.toString());
                }

                voList.add(vo2);
            }

            vo.setCampo02(TimeHelper.converteMinutosEmStringFormatadaTres(tTotalCarregado));
            vo.setCampo03(TimeHelper.converteMinutosEmStringFormatadaTres(tTotalVazio));
            vo.setCampo04(tHodTotalCarreqado.toString());
            vo.setCampo05(tHodTotalVazio.toString());
            listaInformacaoRelatorio.add(vo);
        }

        RelatorioConverterVO relatorioConverter = new RelatorioConverterVO();
        relatorioConverter.setFiltro(filtroVO);
        relatorioConverter.setListaInformacaoRelatorio(listaInformacaoRelatorio);
        relatorioConverter.setNomeRelatorio("EVENTO_MOTORISTAS");

        return RelatorioTrajetto.getInstance().buscarGerarRelatorio(relatorioConverter);
    }


    public RetornoRelatorioVO buscarRelatorioProdutividadeVeiculo(FiltroVO filtroVO) {

        VeiculosDAO veiculosDAO =  new VeiculosDAO(null);
        TransmissaoDAO transmissaoDAO = new TransmissaoDAO();
        ajustarDatas(filtroVO);
        List<InformacaoRelatorioVO> listaInformacaoRelatorio = new ArrayList<>();

        List<EventoMotorista> lista = eventoMotoristaService.buscarPorVeiculos(filtroVO.getListaDeVeiculos(), filtroVO.getDataInicial(), filtroVO.getDataFinal(), filtroVO.getTipoEventoMotorista());
        SimpleDateFormat sf = new SimpleDateFormat("dd/MM HH:mm");
        Map<Integer, List<EventoMotorista>> map = new HashMap<>();
        for(EventoMotorista em : lista){
            if(map.containsKey(em.getIdVeiculo())){
                List<EventoMotorista> l = map.get(em.getIdVeiculo());
                em.setPlaca(l.get(0).getPlaca());
                l.add(em);
            }else{
                Veiculos veiculos = veiculosDAO.buscarVeiculoPeloId(em.getIdVeiculo());
                if(Objects.isNull(veiculos)){
                    em.setPlaca("-");
                }else{
                    em.setPlaca(veiculos.getPlaca());
                }
                List<EventoMotorista> l = new ArrayList<>();
                List<Double> lH = transmissaoDAO.buscarHodometroTelemetria(em.getIdVeiculo(), filtroVO.getDataInicial(), filtroVO.getDataFinal());
                if(lH.isEmpty()){
                    em.setHodometroInicialTelemetria(0);
                    em.setHodometroFinalTelemetria(0);
                }else {
                    if (Objects.nonNull(lH.get(0))) {
                        em.setHodometroInicialTelemetria(lH.get(0).intValue());
                    } else {
                        em.setHodometroInicialTelemetria(0);
                    }
                    if (Objects.nonNull(lH.get(1))) {
                        em.setHodometroFinalTelemetria(lH.get(1).intValue());
                    } else {
                        em.setHodometroFinalTelemetria(0);
                    }
                }
                l.add(em);
                map.put(em.getIdVeiculo(), l);
            }
        }


        for (Map.Entry<Integer, List<EventoMotorista>> entry : map.entrySet()) {
            List<EventoMotorista> l = entry.getValue();
            l.sort((s1, s2) -> {
                return s1.getDataEvento().compareTo(s2.getDataEvento());
            });

            InformacaoRelatorioVO vo = new InformacaoRelatorioVO();
            String nome = l.get(0).getPlaca();
            vo.setCampo01(nome);

            List<InformacaoRelatorioVO> voList = new ArrayList<>();
            vo.setListaInformacaoRelatorio(voList);
            Integer tTotalCarregado = 0;
            Integer tTotalVazio = 0;
            Integer tHodTotalVazio = 0;
            Integer tHodTotalCarreqado = 0;
            for(EventoMotorista em : l){
                InformacaoRelatorioVO vo2 = new InformacaoRelatorioVO();
                vo2.setCampo01( em.getMotoristas().getNome() + " " + em.getMotoristas().getSobrenome());
                vo2.setCampo02(em.getTipo().equals(1)?"Carregado":"Vazio");
                vo2.setCampo03(sf.format(em.getDataEvento()));
                vo2.setCampo04(sf.format(em.getDataFimEvento()));
                int tempoTotal = Minutes.minutesBetween(new DateTime(em.getDataEvento()), new DateTime(em.getDataFimEvento())).getMinutes();
                if(em.getTipo().equals(1)){
                    tTotalCarregado = tTotalCarregado + tempoTotal;
                }else{
                    tTotalVazio = tTotalVazio + tempoTotal;
                }

                vo2.setCampo05(TimeHelper.converteMinutosEmStringFormatadaTres(tempoTotal));
                vo2.setCampo06(converterHectometroParaKm(em.getHodometroInicial()));
                vo2.setCampo07(converterHectometroParaKm(em.getHodometroFinal()));
                Integer diferenca = em.getHodometroFinal() - em.getHodometroInicial();
                if(diferenca > 1000 || diferenca < 0){
                    vo2.setCampo08("Error");
                }else{
                    if(em.getTipo().equals(1)){
                        tHodTotalCarreqado = tHodTotalCarreqado + diferenca;
                    }else{
                        tHodTotalVazio = tHodTotalVazio + diferenca;
                    }
                    vo2.setCampo08(converterHectometroParaKm(diferenca));
                }

                voList.add(vo2);
            }

            Integer diferenca = l.get(0).getHodometroFinalTelemetria() - l.get(0).getHodometroInicialTelemetria();

            vo.setCampo02(TimeHelper.converteMinutosEmStringFormatadaTres(tTotalCarregado));
            vo.setCampo03(TimeHelper.converteMinutosEmStringFormatadaTres(tTotalVazio));
            vo.setCampo04(converterHectometroParaKm(tHodTotalCarreqado));
            vo.setCampo05(converterHectometroParaKm(tHodTotalVazio));
            vo.setCampo06(converterHectometroParaKm(l.get(0).getHodometroInicialTelemetria()));
            vo.setCampo07(converterHectometroParaKm(l.get(0).getHodometroFinalTelemetria()));
            vo.setCampo08(converterHectometroParaKm(diferenca));
            listaInformacaoRelatorio.add(vo);
        }

        RelatorioConverterVO relatorioConverter = new RelatorioConverterVO();
        relatorioConverter.setFiltro(filtroVO);
        relatorioConverter.setListaInformacaoRelatorio(listaInformacaoRelatorio);
        relatorioConverter.setNomeRelatorio("RELATORIO_PRODUTIVIDADE_VEICULO");

        return RelatorioTrajetto.getInstance().buscarGerarRelatorio(relatorioConverter);
    }

    private String converterHectometroParaKm(Integer valor){
        Double r = 0d;
        try {
            if(Objects.nonNull(valor) && valor > 0){
                r = valor /10d;
            }
        } catch (Exception e) {
        }
        return String.format("%.2f", r);
    }

    private int cont = 0;

    public RetornoRelatorioVO buscarRelatorioDiariaMotorista(FiltroVO filtroVO) {
        RetornoRelatorioVO retornoRelatorioVO = new RetornoRelatorioVO();
        try {
            cont = 0;
            ajustarDatas(filtroVO);
            diretorioJasper = Constantes.DIRETORIO_JASPER;
            diretorioGravacao = Constantes.DIRETORIO_TOMCAT_RELATORIO;
            diretorioJasper = diretorioJasper + "diariamotorista.jasper";
            nomeRelatorio = "relatorio_diaria_motorista";
            isSubrelatorio = true;
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            HashMap<String, String> parameters = new HashMap<String, String>();
            parameters.put("DATA_INICIAL", format.format(filtroVO.getDataInicial()));
            parameters.put("DATA_FINAL", format.format(filtroVO.getDataFinal()));
            List<InformacaoRelatorioVO> lista = new ArrayList<>();
            Double valorDiario = unidadesService.buscarValorDiaria(filtroVO.getMotorista().getId());
            InformacaoRelatorioVO informacaoRelatorioVO = jornadaService.buscarDadosRelatorioDiariaMotorista(filtroVO.getDataInicial(), filtroVO.getDataFinal(), filtroVO.getMotorista().getId());
            if (informacaoRelatorioVO != null) {
                informacaoRelatorioVO.getListaInformacaoRelatorio().forEach(i -> {
                    if ("Sim".equalsIgnoreCase(i.getCampo03())) {
                        String v = RodoviaUtils.convertDoubleToMoneyReal(valorDiario);
                        i.setCampo04(v);
                        cont++;
                    } else {
                        i.setCampo04("-");
                    }
                });
            }

            informacaoRelatorioVO.getListaInformacaoRelatorio().sort(Comparator.comparing(InformacaoRelatorioVO::getDataDoEvento));

            Double valorTotal = valorDiario * cont;
            informacaoRelatorioVO.setCampo02(RodoviaUtils.convertDoubleToMoneyReal(valorTotal));
            lista.add(informacaoRelatorioVO);
            String url = gerarPdf(parameters, lista.toArray());
            retornoRelatorioVO.setCodigoRetorno(110);
            retornoRelatorioVO.setMensagem("Relatorio gerado com sucesso!");
            retornoRelatorioVO.setUrl(url);
        } catch (Exception e) {
            retornoRelatorioVO.setCodigoRetorno(-1);
            retornoRelatorioVO.setMensagem(e.getMessage());
            LogSistema.logError("buscarRelatorioDiariaMotorista", e);
        }


        return retornoRelatorioVO;
    }


    private void ajustarDatas(FiltroVO filtroVO) {
        if(Objects.nonNull(filtroVO.getDataInicialString()) && Objects.nonNull(filtroVO.getDataFinalString())){
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("ddMMyyyyHHmm");
            try {
                filtroVO.setDataInicial(simpleDateFormat.parse(filtroVO.getDataInicialString()));
                filtroVO.setDataFinal(simpleDateFormat.parse(filtroVO.getDataFinalString()));
            }catch (Exception e){}
        }else {
            filtroVO.setDataInicial(TimeHelper.getDate000000(filtroVO.getDataInicial()));
            filtroVO.setDataFinal(TimeHelper.getDate235959(filtroVO.getDataFinal()));
        }
    }


    private String gerarPdf(HashMap parameters, Object[] array) throws Exception {
        InputStream arquivoJasper = null;
        FileOutputStream fileOutputStream = null;
        SimpleOutputStreamExporterOutput simpleOutputStreamExporterOutput = null;
        SimpleDateFormat formatador = new SimpleDateFormat("dd_MM_yyyy_HH_mm");
        String data = formatador.format(new Date());
        nomeRelatorio = nomeRelatorio + "_" + data + ".pdf";
        String diretorioGravacaoPdf = diretorioGravacao + nomeRelatorio;
        String diretorioLogoMarcaRelatorio = Constantes.DIRETORIO_IMAGENS_LOGOMARCA + "rodovia.png";
        parameters.put("DIRETORIO_LOGO", diretorioLogoMarcaRelatorio);
        if (isSubrelatorio) {
            String dirSubrelatorio = Constantes.DIRETORIO_JASPER;
            parameters.put("SUBREPORT_DIR", dirSubrelatorio);
        }
        try {
            arquivoJasper = new FileInputStream(new File(diretorioJasper));
        } catch (FileNotFoundException ex) {
            throw new Exception(ex);
        }
        try {
            JRDataSource jrRs = new JRBeanArrayDataSource(array);
            JasperPrint print = JasperFillManager.fillReport(arquivoJasper, parameters, jrRs);
            JRPdfExporter exporter = new JRPdfExporter();

            exporter.setExporterInput(new SimpleExporterInput(print));

            fileOutputStream = new FileOutputStream(diretorioGravacaoPdf);
            simpleOutputStreamExporterOutput = new SimpleOutputStreamExporterOutput(fileOutputStream);
            exporter.setExporterOutput(simpleOutputStreamExporterOutput);
            SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
            exporter.setConfiguration(configuration);
            exporter.exportReport();

        } catch (FileNotFoundException | JRException e) {
            throw new Exception(e);
        } finally {

            if (simpleOutputStreamExporterOutput != null) {
                simpleOutputStreamExporterOutput.close();
            }

            if (fileOutputStream != null) {
                fileOutputStream.close();
            }

            arquivoJasper.close();
        }

        return Constantes.URL_SERVIDOR_RELATORIO + nomeRelatorio;
    }


    public List<DetalheTelemetriaVO> buscarDetalhesTelemetriaMock(FiltroVO filtro) {


        List<DetalheTelemetriaVO> lista = new ArrayList<>();
        DetalheTelemetriaVO vo = new DetalheTelemetriaVO();
        vo.setStart("2021-07-22 06:00");
        vo.setEnd("2021-07-22 07:00");
        vo.setColor("#836FFF");
        vo.setDuracao("00:01:04");
        vo.setVelocidade("80 Km/h");
        vo.setHodometroInicio("5535.10 km");
        vo.setHodometroFim("5536.90 km");
        vo.setLatitude(-20.270518);
        vo.setLongitude(-45.556721);
        vo.setNomeEvento("ALTERR");
        vo.setResumo("NSA");
        vo.setEndereco("MG-170, Arcos, MG");
        lista.add(vo);

        vo = new DetalheTelemetriaVO();
        vo.setStart("2021-07-22 07:00");
        vo.setEnd("2021-07-22 08:00");
        vo.setColor("#836FFF");
        vo.setDuracao("00:01:04");
        vo.setVelocidade("80 Km/h");
        vo.setHodometroInicio("5535.10 km");
        vo.setHodometroFim("5536.90 km");
        vo.setLatitude(-20.270518);
        vo.setLongitude(-45.556721);
        vo.setNomeEvento("ALTERR");
        vo.setResumo("NSA");
        vo.setEndereco("MG-170, Arcos, MG");
        lista.add(vo);

        vo = new DetalheTelemetriaVO();
        vo.setStart("2021-07-22 08:00");
        vo.setEnd("2021-07-22 09:00");
        vo.setColor("#FF0000");
        vo.setDuracao("00:01:04");
        vo.setVelocidade("80 Km/h");
        vo.setHodometroInicio("5535.10 km");
        vo.setHodometroFim("5536.90 km");
        vo.setLatitude(-20.270518);
        vo.setLongitude(-45.556721);
        vo.setNomeEvento("Curva Brusca a Direita");
        vo.setResumo("C.B.Dir");
        vo.setEndereco("MG-170, Arcos, MG");
        lista.add(vo);


        vo = new DetalheTelemetriaVO();
        vo.setStart("2021-07-22 09:00");
        vo.setEnd("2021-07-22 10:00");
        vo.setColor("#836FFF");
        vo.setDuracao("00:01:04");
        vo.setVelocidade("80 Km/h");
        vo.setHodometroInicio("5535.10 km");
        vo.setHodometroFim("5536.90 km");
        vo.setLatitude(-20.270518);
        vo.setLongitude(-45.556721);
        vo.setNomeEvento("Curva Brusca a Direita");
        vo.setResumo("NSA");
        vo.setEndereco("MG-170, Arcos, MG");
        lista.add(vo);

        vo = new DetalheTelemetriaVO();
        vo.setStart("2021-07-22 10:00");
        vo.setEnd("2021-07-22 12:00");
        vo.setColor("#836FFF");
        vo.setDuracao("00:01:04");
        vo.setVelocidade("80 Km/h");
        vo.setHodometroInicio("5535.10 km");
        vo.setHodometroFim("5536.90 km");
        vo.setLatitude(-20.270518);
        vo.setLongitude(-45.556721);
        vo.setNomeEvento("Curva Brusca a Direita");
        vo.setResumo("NSA");
        vo.setEndereco("MG-170, Arcos, MG");
        lista.add(vo);

        vo = new DetalheTelemetriaVO();
        vo.setStart("2021-07-22 12:00");
        vo.setEnd("2021-07-22 13:00");
        vo.setColor("#FF0000");
        vo.setDuracao("00:0:04");
        vo.setVelocidade("120 Km/h");
        vo.setHodometroInicio("5535.10 km");
        vo.setHodometroFim("5536.90 km");
        vo.setLatitude(-20.270518);
        vo.setLongitude(-45.556721);
        vo.setNomeEvento("Acelaração Brusca");
        vo.setResumo("A.B");
        vo.setEndereco("MG-170, Arcos, MG");
        lista.add(vo);

        vo = new DetalheTelemetriaVO();
        vo.setStart("2021-07-22 13:00");
        vo.setEnd("2021-07-22 14:00");
        vo.setColor("#836FFF");
        vo.setDuracao("00:0:04");
        vo.setVelocidade("120 Km/h");
        vo.setHodometroInicio("5535.10 km");
        vo.setHodometroFim("5536.90 km");
        vo.setLatitude(-20.270518);
        vo.setLongitude(-45.556721);
        vo.setNomeEvento("Acelaração Brusca");
        vo.setResumo("NSA");
        vo.setEndereco("MG-170, Arcos, MG");
        lista.add(vo);

        vo = new DetalheTelemetriaVO();
        vo.setStart("2021-07-22 14:00");
        vo.setEnd("2021-07-22 15:00");
        vo.setColor("#836FFF");
        vo.setDuracao("00:0:04");
        vo.setVelocidade("120 Km/h");
        vo.setHodometroInicio("5535.10 km");
        vo.setHodometroFim("5536.90 km");
        vo.setLatitude(-20.270518);
        vo.setLongitude(-45.556721);
        vo.setNomeEvento("Acelaração Brusca");
        vo.setResumo("NSA");
        vo.setEndereco("MG-170, Arcos, MG");
        lista.add(vo);

        vo = new DetalheTelemetriaVO();
        vo.setStart("2021-07-22 15:00");
        vo.setEnd("2021-07-22 16:00");
        vo.setColor("#836FFF");
        vo.setDuracao("00:0:04");
        vo.setVelocidade("120 Km/h");
        vo.setHodometroInicio("5535.10 km");
        vo.setHodometroFim("5536.90 km");
        vo.setLatitude(-20.270518);
        vo.setLongitude(-45.556721);
        vo.setNomeEvento("Acelaração Brusca");
        vo.setResumo("NSA");
        vo.setEndereco("MG-170, Arcos, MG");
        lista.add(vo);

        vo = new DetalheTelemetriaVO();
        vo.setStart("2021-07-22 16:00");
        vo.setEnd("2021-07-22 17:00");
        vo.setColor("#FF0000");
        vo.setDuracao("00:0:04");
        vo.setVelocidade("120 Km/h");
        vo.setHodometroInicio("5535.10 km");
        vo.setHodometroFim("5536.90 km");
        vo.setLatitude(-20.270518);
        vo.setLongitude(-45.556721);
        vo.setNomeEvento("Excesso de Velocidade");
        vo.setResumo("E.V");
        vo.setEndereco("MG-170, Arcos, MG");
        lista.add(vo);

        vo = new DetalheTelemetriaVO();
        vo.setStart("2021-07-22 17:00");
        vo.setEnd("2021-07-22 19:00");
        vo.setColor("#363636");
        vo.setDuracao("00:0:04");
        vo.setVelocidade("120 Km/h");
        vo.setHodometroInicio("5535.10 km");
        vo.setHodometroFim("5536.90 km");
        vo.setLatitude(-20.270518);
        vo.setLongitude(-45.556721);
        vo.setNomeEvento("Excesso de Velocidade");
        vo.setResumo("NSA");
        vo.setEndereco("MG-170, Arcos, MG");
        lista.add(vo);

        vo = new DetalheTelemetriaVO();
        vo.setStart("2021-07-22 19:00");
        vo.setEnd("2021-07-22 20:00");
        vo.setColor("#363636");
        vo.setDuracao("00:0:04");
        vo.setVelocidade("120 Km/h");
        vo.setHodometroInicio("5535.10 km");
        vo.setHodometroFim("5536.90 km");
        vo.setLatitude(-20.270518);
        vo.setLongitude(-45.556721);
        vo.setNomeEvento("Excesso de Velocidade");
        vo.setResumo("NSA");
        vo.setEndereco("MG-170, Arcos, MG");
        lista.add(vo);

        vo = new DetalheTelemetriaVO();
        vo.setStart("2021-07-22 20:00");
        vo.setEnd("2021-07-22 21:00");
        vo.setColor("#363636");
        vo.setDuracao("00:0:04");
        vo.setVelocidade("120 Km/h");
        vo.setHodometroInicio("5535.10 km");
        vo.setHodometroFim("5536.90 km");
        vo.setLatitude(-20.270518);
        vo.setLongitude(-45.556721);
        vo.setNomeEvento("Excesso de Velocidade");
        vo.setResumo("NSA");
        vo.setEndereco("MG-170, Arcos, MG");
        lista.add(vo);

        vo = new DetalheTelemetriaVO();
        vo.setStart("2021-07-22 21:00:");
        vo.setEnd("2021-07-22 22:00");
        vo.setColor("#363636");
        vo.setDuracao("00:0:04");
        vo.setVelocidade("120 Km/h");
        vo.setHodometroInicio("5535.10 km");
        vo.setHodometroFim("5536.90 km");
        vo.setLatitude(-20.270518);
        vo.setLongitude(-45.556721);
        vo.setNomeEvento("Excesso de Velocidade");
        vo.setResumo("NSA");
        vo.setEndereco("MG-170, Arcos, MG");
        lista.add(vo);

        return lista;
    }


    public List<DetalheTelemetriaVO> buscarDetalhesTelemetria(FiltroVO filtro) {
        List<DetalheTelemetriaVO> l = new ArrayList<>();
        try {
            TransmissaoDAO transmissaoDAO = new TransmissaoDAO();
            List<Transmissao> listaPreProcessada = new ArrayList<>();
            List<Transmissao> listaPreProcessadaDois = new ArrayList<>();
            List<Transmissao> lista = transmissaoDAO.buscarTransmissaoMongo(filtro.getVeiculo().getId(), filtro.getDataInicial(), filtro.getDataFinal());
            Collections.sort(lista, new Comparator<Transmissao>() {
                public int compare(Transmissao o1, Transmissao o2) {
                    return o1.getDataTransmissao().compareTo(o2.getDataTransmissao());
                }
            });//2021-07-22 21:00
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Transmissao ultimaTransmissao = null;
            boolean fecharIgnicaoAtiva = false;
            for (Transmissao transmissao : lista) {
                if (Objects.isNull(ultimaTransmissao)) {
                    ultimaTransmissao = transmissao;
                } else {
                    if (ultimaTransmissao.getIgnicaoAtiva() && transmissao.getIgnicaoAtiva()) {
                        continue;
                    } else {
                        //Fechar ignicao ativa e começar de novo
                        if (ultimaTransmissao.getIgnicaoAtiva()) {
                            DetalheTelemetriaVO vo = new DetalheTelemetriaVO();
                            vo.setStart(simpleDateFormat.format(ultimaTransmissao.getDataTransmissao()));
                            vo.setEnd(simpleDateFormat.format(transmissao.getDataTransmissao()));
                            vo.setColor("#0000FF");
                            vo.setResumo("NSA");
                            l.add(vo);
                            ultimaTransmissao = transmissao;
                        } else {
                            if (!ultimaTransmissao.getIgnicaoAtiva() && !transmissao.getIgnicaoAtiva()) {
                                continue;
                            } else {
                                if (!ultimaTransmissao.getIgnicaoAtiva()) {
                                    DetalheTelemetriaVO vo = new DetalheTelemetriaVO();
                                    vo.setStart(simpleDateFormat.format(ultimaTransmissao.getDataTransmissao()));
                                    vo.setEnd(simpleDateFormat.format(transmissao.getDataTransmissao()));
                                    vo.setColor("#363636");
                                    vo.setResumo("NSA");
                                    l.add(vo);
                                    ultimaTransmissao = transmissao;
                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            LogSistema.logError("buscarDetalhesTelemetria", e);
        }
        return l;
    }


    public RelatorioGraficoVO buscarTempoTransporteGrafico(FiltroVO filtro) {
        RelatorioGraficoVO relatorioGraficoVO = new RelatorioGraficoVO();
        try {
            Map<VeiculoVO, List<RelatorioTempoTransporteGrafico>> map = new HashMap<>();
            for (VeiculoVO veiculoVO : filtro.getListaDeVeiculos()) {
                List<EventoVeiculoVO> lista = new EventoVeiculoDAO().buscarListaDeEnvento(veiculoVO.getId(), filtro.getDataInicial(), filtro.getDataFinal());

                lista.sort((EventoVeiculoVO s1, EventoVeiculoVO s2)->s1.getDataEvento().compareTo(s2.getDataEvento()));
                EventoVeiculoVO ultimoEventoVeiculo = null;
                for (EventoVeiculoVO vo : lista) {
                    if (Objects.nonNull(ultimoEventoVeiculo)) {
                        if (ultimoEventoVeiculo.getTipo().equals(2) && vo.getTipo().equals(1)) {
                            if (map.containsKey(veiculoVO)) {
                                RelatorioTempoTransporteGrafico rg = new RelatorioTempoTransporteGrafico();
                                rg.eventoVeiculoVOFim = vo;
                                rg.eventoVeiculoVOInicio = ultimoEventoVeiculo;
                                map.get(veiculoVO).add(rg);
                            } else {
                                List<RelatorioTempoTransporteGrafico> l = new ArrayList<>();
                                RelatorioTempoTransporteGrafico rg = new RelatorioTempoTransporteGrafico();
                                rg.eventoVeiculoVOFim = vo;
                                rg.eventoVeiculoVOInicio = ultimoEventoVeiculo;
                                map.put(veiculoVO, l);
                            }
                        }
                    }
                    ultimoEventoVeiculo = vo;
                }
            }

            List<TempoTransporteGraficoVO> listaTempoTransporteGrafico = new ArrayList<>();

            for (Map.Entry<VeiculoVO, List<RelatorioTempoTransporteGrafico>> entry : map.entrySet()) {

                int minutosVazio = 0;
                int minutosCheio = 0;

                VeiculoVO veiculoVO = entry.getKey();
                for (RelatorioTempoTransporteGrafico t : entry.getValue()) {
                    EventoVeiculoVO eventoVeiculoVOInicio = t.eventoVeiculoVOInicio;
                    EventoVeiculoVO eventoVeiculoVOFim = t.eventoVeiculoVOFim;

                    int segundos = Seconds.secondsBetween(new DateTime(eventoVeiculoVOInicio.getDataEvento()), new DateTime(eventoVeiculoVOFim.getDataEvento())).getSeconds();

                    if (eventoVeiculoVOInicio.getLocal().getFuncao().equals(1) && eventoVeiculoVOFim.getLocal().getFuncao().equals(2)) {
                        //Seguinifica que esta carregado
                        minutosCheio = minutosCheio + segundos;
                    } else {
                        minutosVazio = minutosVazio + segundos;
                    }

                }

                TempoTransporteGraficoVO tempoTransporteGraficoVO = new TempoTransporteGraficoVO();

                tempoTransporteGraficoVO.setCategory(veiculoVO.getPlaca());
                tempoTransporteGraficoVO.setCheio(new BigDecimal((minutosCheio / 60d) / 60d).setScale(2, RoundingMode.HALF_UP).doubleValue());
                tempoTransporteGraficoVO.setVazio(new BigDecimal((minutosVazio / 60d) / 60d).setScale(2, RoundingMode.HALF_UP).doubleValue());

                listaTempoTransporteGrafico.add(tempoTransporteGraficoVO);

            }
            relatorioGraficoVO.setListaTempoTransporteGrafico(listaTempoTransporteGrafico);

        } catch (Exception e) {
            LogSistema.logError("buscarTempoTransporteGrafico", e);
        }
        return relatorioGraficoVO;

    }


    public RelatorioGraficoVO buscarTempoCarregamento(FiltroVO filtro) {
        RelatorioGraficoVO relatorioGraficoVO = new RelatorioGraficoVO();
        try {
            Map<String, List<RelatorioTempoCarregamento>> map = new HashMap<>();
            for (VeiculoVO veiculoVO : filtro.getListaDeVeiculos()) {
                List<EventoVeiculoVO> lista = new EventoVeiculoDAO().buscarListaDeEventoCarregamento(veiculoVO.getId(), filtro.getDataInicial(), filtro.getDataFinal());

                lista.sort((EventoVeiculoVO s1, EventoVeiculoVO s2)->s1.getDataEvento().compareTo(s2.getDataEvento()));
                EventoVeiculoVO ultimoEventoVeiculo = null;
                for (EventoVeiculoVO vo : lista) {
                    if (Objects.nonNull(ultimoEventoVeiculo)) {
                        if (ultimoEventoVeiculo.getTipo().equals(2) && vo.getTipo().equals(1)) {
                            int segundos = Seconds.secondsBetween(new DateTime(ultimoEventoVeiculo.getDataEvento()), new DateTime(vo.getDataEvento())).getSeconds();
                            if(map.containsKey(vo.getLocal().getNome())){
                              List<RelatorioTempoCarregamento> lTemp = map.get(vo.getLocal().getNome());
                              boolean tem = lTemp.stream().anyMatch(c->c.veiculoVO.getId().equals(veiculoVO.getId()));
                              if(tem) {
                                  lTemp.forEach(c -> {
                                      if (c.veiculoVO.getId() == veiculoVO.getId()) {
                                          c.tempoTotal = c.tempoTotal + segundos;
                                      }
                                  });
                              }else{
                                  RelatorioTempoCarregamento ctc = new RelatorioTempoCarregamento();
                                  ctc.veiculoVO = veiculoVO;
                                  ctc.tempoTotal = segundos;
                                  lTemp.add(ctc);
                              }
                            }else{
                                List<RelatorioTempoCarregamento> lTemp = new ArrayList<>();
                                RelatorioTempoCarregamento ctc = new RelatorioTempoCarregamento();
                                ctc.veiculoVO = veiculoVO;
                                ctc.tempoTotal = segundos;
                                lTemp.add(ctc);
                                map.put(ultimoEventoVeiculo.getLocal().getNome(), lTemp);
                            }


                        }
                    }
                    ultimoEventoVeiculo = vo;
                }
            }

            List<TempoCarregamentoGraficoVO> listaTempoCarregamentoGrafico = new ArrayList<>();

            for (Map.Entry<String, List<RelatorioTempoCarregamento>> entry : map.entrySet()) {

                String local = entry.getKey();
                List<RelatorioTempoCarregamento> lTemp = entry.getValue();
                TempoCarregamentoGraficoVO tcg = new TempoCarregamentoGraficoVO();
                tcg.setLocal(local);
                List<TempoCarregamentoVO> voList = new ArrayList<>();
                for (RelatorioTempoCarregamento t : entry.getValue()) {
                    TempoCarregamentoVO voT = new TempoCarregamentoVO();
                    voT.setPlaca(t.veiculoVO.getPlaca());
                    voT.setHoras(new BigDecimal((t.tempoTotal / 60d) / 60d).setScale(2, RoundingMode.HALF_UP).doubleValue());
                    voList.add(voT);
                }
                tcg.setListaTempoCarregamento(voList);
                listaTempoCarregamentoGrafico.add(tcg);
            }
            relatorioGraficoVO.setListaTempoCarregamentoGrafico(listaTempoCarregamentoGrafico);
        } catch (Exception e) {
            LogSistema.logError("buscarTempoCarregamento", e);
        }
        return relatorioGraficoVO;

    }


    public RelatorioGraficoVO buscarVelocidadeParaRelatorio(FiltroVO filtro) {
        RelatorioGraficoVO relatorioGraficoVO = new RelatorioGraficoVO();
        List<RelatorioVelocidadeVO> listaRelatorioVelocidade = new ArrayList<>();
        try {
            for (VeiculoVO veiculoVO : filtro.getListaDeVeiculos()) {
                RelatorioVelocidadeVO velocidadeVO = new RelatorioVelocidadeVO();
                velocidadeVO.setPlaca(veiculoVO.getPlaca());
                List<Transmissao> lista = new TransmissaoDAO().buscarTrasmissaoParaRelatorio(veiculoVO.getId(), filtro.getDataInicial(), filtro.getDataFinal());
                List<VelocidadeGraficoVO> ll = new ArrayList<>();
                for (Transmissao vo : lista) {
                    VelocidadeGraficoVO vo1 = new VelocidadeGraficoVO();
                    vo1.setVelocidade(vo.getVelocidade());
                    vo1.setDataDaTransmissao(vo.getDataTransmissao());
                    ll.add(vo1);
                }
                velocidadeVO.setListaVelocidadeGrafico(ll);
                listaRelatorioVelocidade.add(velocidadeVO);
            }
            relatorioGraficoVO.setListaRelatorioVelocidade(listaRelatorioVelocidade);
        } catch (Exception e) {
            LogSistema.logError("buscarVelocidadeParaRelatorio", e);
        }
        return relatorioGraficoVO;
    }



    public RetornoRelatorioVO buscarRelatorioProdutividade(FiltroVO filtroVO) {
        ajustarDatas(filtroVO);
        return RelatorioTrajetto.getInstance().buscarRelatorioDeAcionamentoDeCompreensor(filtroVO);
    }


    class RelatorioTempoTransporteGrafico {
        EventoVeiculoVO eventoVeiculoVOInicio;
        EventoVeiculoVO eventoVeiculoVOFim;
        VeiculoVO veiculoVO;
    }

    class RelatorioTempoCarregamento {
        int tempoTotal;
        VeiculoVO veiculoVO;
    }


}
