package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author analista.ti
 */
@Entity
@Table(name = "log_situacao_motorista")
@Getter
@Setter
public class MotoristaStatus implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "LOG_SITUACAO_MOTORISTA_ID_SEQ", sequenceName = "LOG_SITUACAO_MOTORISTA_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LOG_SITUACAO_MOTORISTA_ID_SEQ")
    @Column(name = "id")
    private Integer id;

    @Column(name = "instante_lancamento", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date instanteLancamento;

    @Column(name = "operador_lancamento", nullable = false) // integer NOT NULL,
    private Integer operadorLancamento;

    @Column(name = "situacao_anterior") // integer,
    private Integer situacaoAnterior;

    @Column(name = "situacao_inserida", nullable = false) // integer,
    private Integer situacaoInserida;

    @Column(name = "motoristas_id", nullable = false) //id integer NOT NULL,
    private Integer motorista;

    @Column(name = "empresa_id", nullable = false) //integer NOT NULL,
    private Integer empresaId;

    @Column(name = "motivo", length = 60, nullable = false)
    private String motivo;
}