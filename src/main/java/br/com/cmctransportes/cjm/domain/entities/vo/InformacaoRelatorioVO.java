package br.com.cmctransportes.cjm.domain.entities.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InformacaoRelatorioVO {

    private String campo01;
    private String campo02;
    private String campo03;
    private String campo04;
    private String campo05;
    private String campo06;
    private String campo07;
    private String campo08;
    private String campo09;
    private String campo10;
    private String campo11;
    private Date dataDoEvento;

    private List<InformacaoRelatorioVO> listaInformacaoRelatorio;

    public InformacaoRelatorioVO() {
        listaInformacaoRelatorio = new ArrayList<>();
    }

    public void adicionarInformacaoRelatorioNaLista(InformacaoRelatorioVO informacaoRelatorio) {
        listaInformacaoRelatorio.add(informacaoRelatorio);
    }

    public String getCampo01() {
        return campo01;
    }

    public void setCampo01(String campo01) {
        this.campo01 = campo01;
    }

    public String getCampo02() {
        return campo02;
    }

    public void setCampo02(String campo02) {
        this.campo02 = campo02;
    }

    public String getCampo03() {
        return campo03;
    }

    public void setCampo03(String campo03) {
        this.campo03 = campo03;
    }

    public String getCampo04() {
        return campo04;
    }

    public void setCampo04(String campo04) {
        this.campo04 = campo04;
    }

    public String getCampo05() {
        return campo05;
    }

    public void setCampo05(String campo05) {
        this.campo05 = campo05;
    }

    public String getCampo06() {
        return campo06;
    }

    public void setCampo06(String campo06) {
        this.campo06 = campo06;
    }

    public String getCampo07() {
        return campo07;
    }

    public void setCampo07(String campo07) {
        this.campo07 = campo07;
    }

    public String getCampo08() {
        return campo08;
    }

    public void setCampo08(String campo08) {
        this.campo08 = campo08;
    }

    public List<InformacaoRelatorioVO> getListaInformacaoRelatorio() {
        return listaInformacaoRelatorio;
    }

    public void setListaInformacaoRelatorio(List<InformacaoRelatorioVO> listaInformacaoRelatorio) {
        this.listaInformacaoRelatorio = listaInformacaoRelatorio;
    }

    public Date getDataDoEvento() {
        return dataDoEvento;
    }

    public void setDataDoEvento(Date dataDoEvento) {
        this.dataDoEvento = dataDoEvento;
    }

    public String getCampo09() {
        return campo09;
    }

    public void setCampo09(String campo09) {
        this.campo09 = campo09;
    }

    public String getCampo10() {
        return campo10;
    }

    public void setCampo10(String campo10) {
        this.campo10 = campo10;
    }

    public String getCampo11() {
        return campo11;
    }

    public void setCampo11(String campo11) {
        this.campo11 = campo11;
    }
}
