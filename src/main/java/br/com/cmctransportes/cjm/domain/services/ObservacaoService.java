package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.Observacao;
import br.com.cmctransportes.cjm.domain.entities.vo.MotoristaVO;
import br.com.cmctransportes.cjm.domain.entities.vo.ObservacaoVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoObservacaoVO;
import br.com.cmctransportes.cjm.infra.dao.ObservacaoDAO;
import br.com.cmctransportes.cjm.proxy.ObservacaoTrajetto;
import br.com.cmctransportes.cjm.sascarsoap.Motorista;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ObservacaoService {

    private final EntityManager em;
    private ObservacaoDAO observacaoDAO;
     public ObservacaoService(EntityManager em) {
         this.em = em;
         this.observacaoDAO = new ObservacaoDAO(em);
    }

    @Transactional
    public RetornoObservacaoVO cadastrar(ObservacaoVO observacaoVO){
         if("VIASAT".equalsIgnoreCase(observacaoVO.getTipo())){
            return enviarObservacaoOrion(observacaoVO);
         }else{
             RetornoObservacaoVO retornoObservacaoVO = new RetornoObservacaoVO();
             try{
                 this.observacaoDAO.save(convertTO(observacaoVO));
                 retornoObservacaoVO.setCodigoRetorno(300);
                 retornoObservacaoVO.setMensagem("Observacao cadastrado com sucesso!");
             }catch (Exception e){
                 retornoObservacaoVO.setCodigoRetorno(-1);
                 retornoObservacaoVO.setMensagem(e.getMessage());
             }
             return retornoObservacaoVO;
         }
    }


    public RetornoObservacaoVO listarObservacaoTrajetto(Integer idVeiculo){
        RetornoObservacaoVO retornoObservacaoVO = ObservacaoTrajetto.getInstance().buscarListaDeObservacoes(idVeiculo);
        if(retornoObservacaoVO != null && retornoObservacaoVO.getListaDeObservacao() != null && !retornoObservacaoVO.getListaDeObservacao().isEmpty()){
            retornoObservacaoVO.getListaDeObservacao().forEach(obj->obj.setVeiculo(null));
        }
        return retornoObservacaoVO;
    }

    public RetornoObservacaoVO listarObservacaoViam(Integer idMotorista){
         RetornoObservacaoVO retornoObservacaoVO = new RetornoObservacaoVO();
         try{
             List<Observacao> lista = this.observacaoDAO.buscarLista(idMotorista);
             List<ObservacaoVO> listaVO = new ArrayList<>();
             lista.forEach(obs->listaVO.add(convertTO(obs)));
             retornoObservacaoVO.setListaDeObservacao(listaVO);
             retornoObservacaoVO.setMensagem("Observacao carregado com sucesso!");
             retornoObservacaoVO.setCodigoRetorno(301);
         }catch (Exception e){
             retornoObservacaoVO.setCodigoRetorno(-1);
             retornoObservacaoVO.setMensagem(e.getMessage());
         }

        return retornoObservacaoVO;
    }

    public List<Observacao> listarObservacaoViam(MotoristaVO motorista) throws Exception{
         return observacaoDAO.buscarLista(motorista.getId());
    }

    private RetornoObservacaoVO enviarObservacaoOrion(ObservacaoVO observacaoVO){
        return ObservacaoTrajetto.getInstance().cadastrarObservacao(observacaoVO);
    }

    private Observacao convertTO(ObservacaoVO observacaoVO){
        Observacao observacao = new Observacao();
        observacao.setDataDoCadastro(new Date());
        observacao.setIdMotorista(observacaoVO.getMotorista().getId());
        observacao.setTexto(observacaoVO.getTexto());
        return observacao;
    }

    private ObservacaoVO convertTO(Observacao observacao){
        ObservacaoVO observacaoVO = new ObservacaoVO();
        observacaoVO.setDataDoCadastro(observacao.getDataDoCadastro());
        observacaoVO.setTexto(observacao.getTexto());
        return observacaoVO;
    }
}
