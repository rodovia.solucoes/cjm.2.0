package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.Empresas;
import br.com.cmctransportes.cjm.domain.entities.Motoristas;
import br.com.cmctransportes.cjm.domain.entities.TiposDeVeiculo;
import br.com.cmctransportes.cjm.domain.entities.Veiculos;
import br.com.cmctransportes.cjm.domain.entities.vo.EmpresaVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoVeiculoVO;
import br.com.cmctransportes.cjm.domain.entities.vo.TiposDeVeiculoVO;
import br.com.cmctransportes.cjm.domain.entities.vo.VeiculoVO;
import br.com.cmctransportes.cjm.infra.dao.VeiculosDAO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.proxy.VeiculoTrajetto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

/**
 * @author analista.ti
 */
@Service
public class VeiculosService {
    // TODO: criar GenericService
    private VeiculosDAO dao;

    @Autowired
    private MotoristasVeiculosService motoristasVeiculosService;

    @Autowired
    private EmpresasService empresasService;
    @Autowired
    private TiposDeVeiculoService tiposDeVeiculoService;
    @Autowired
    private DriverService driverService;

    private final EntityManager em;

    public VeiculosService(EntityManager em) {
        this.em = em;
        dao = new VeiculosDAO(em);
    }

    @Transactional
    public void save(Veiculos entity) {
        try {
            dao.save(entity);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    // public T getById(PK pk) {
    public List<Veiculos> findAll() {
        // return dao.findAll();
        return dao.nqFindAll();
    }

    public Veiculos getById(String id) {
        return dao.getById(id);
    }

    @Transactional
    public void update(Veiculos entity) {
        try {
            dao.update(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void delete(String id) {
        try {
            Veiculos tipo = getById(id);
            dao.delete(tipo);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Transactional
    public void delete(Veiculos entity) {
        try {
            dao.delete(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Veiculos> findAll(Integer idEmpresa) {
        return dao.findAll(idEmpresa);
    }

    public List<Veiculos> findAll(Integer idEmpresa, Integer idUnidade) {
        return dao.findAll(idEmpresa, idUnidade);
    }

    public Veiculos getById(Integer idEmpresa, String id) {
        return dao.getById(id);
    }

    //DEPOIS DAS ALTERACOES DE TELAS AVALIAR OS METODOS ACIMA

    public RetornoVeiculoVO cadastrarVeiculo(VeiculoVO veiculoVO) throws Exception{
        try{
            if(veiculoVO.getEmpresaVO() == null){
                return new RetornoVeiculoVO(-1, "Empresa é obrigatorio.");
            }
            if(veiculoVO.getEmpresaVO().getIdClienteTrajetto() == null || veiculoVO.getEmpresaVO().getIdClienteTrajetto() == 0){
                Empresas empresas = empresasService.getById(veiculoVO.getEmpresaVO().getId());
                veiculoVO.getEmpresaVO().setIdClienteTrajetto(empresas.getIdClienteTrajetto());
            }
            return VeiculoTrajetto.getInstance().cadastrarVeiculo(veiculoVO);
        } catch (Exception e) {
            LogSistema.logError("cadastrarVeiculo", e);
            throw new Exception(e);
        }
    }

    public RetornoVeiculoVO editarVeiculo(VeiculoVO veiculoVO) throws Exception{
        try{
            if(veiculoVO.getEmpresaVO() == null){
                return new RetornoVeiculoVO(-1, "Empresa é obrigatorio.");
            }
            if(veiculoVO.getEmpresaVO().getIdClienteTrajetto() == null || veiculoVO.getEmpresaVO().getIdClienteTrajetto() == 0){
                Empresas empresas = empresasService.getById(veiculoVO.getEmpresaVO().getId());
                veiculoVO.getEmpresaVO().setIdClienteTrajetto(empresas.getIdClienteTrajetto());
            }
            return VeiculoTrajetto.getInstance().editarVeiculo(veiculoVO);
        } catch (Exception e) {
            LogSistema.logError("editarVeiculo", e);
            throw new Exception(e);
        }
    }



    public RetornoVeiculoVO listarVeiculo(Integer id, Integer unidade) throws Exception {
        try {
            //Buscar o Id Cliente Trajetto
            Empresas empresas = empresasService.getById(id);
            RetornoVeiculoVO retorno = VeiculoTrajetto.getInstance().listarVeiculo(empresas.getIdClienteTrajetto(), unidade);
            if (61 == retorno.getCodigoRetorno()) {
                retorno.getListaDeVeiculos().forEach(
                        v->ajustarTiposVeiculoVO(v, empresas));
            }
            return retorno;
        } catch (Exception e) {
            LogSistema.logError("listarVeiculo", e);
            throw new Exception(e);
        }
    }

    public RetornoVeiculoVO  buscarVeiculoPeloId(Integer idEmpresa, Integer idVeiculo) throws Exception {
        try {
            RetornoVeiculoVO retorno = VeiculoTrajetto.getInstance(). buscarVeiculoPeloId(idVeiculo);
            if (63 == retorno.getCodigoRetorno()) {
                //Buscar o Id Cliente Trajetto
                Empresas empresas = empresasService.getById(idEmpresa);
                ajustarTiposVeiculoVO(retorno.getVeiculo(), empresas);
            }
            return retorno;
        } catch (Exception e) {
            LogSistema.logError("listarVeiculo", e);
            throw new Exception(e);
        }
    }

    public RetornoVeiculoVO buscarListaDeVeiculoPeloCliente(Integer idEmpresa) throws Exception {
        try {
            //Buscar o Id Cliente Trajetto
            Empresas empresas = empresasService.getById(idEmpresa);
            RetornoVeiculoVO retorno = VeiculoTrajetto.getInstance().buscarListaDeVeiculoPeloCliente(empresas.getIdClienteTrajetto());
            if (61 == retorno.getCodigoRetorno()) {
                retorno.getListaDeVeiculos().forEach(
                        v->ajustarTiposVeiculoVO(v, empresas));
            }
            return retorno;
        } catch (Exception e) {
            LogSistema.logError("listarVeiculo", e);
            throw new Exception(e);
        }
    }


    public RetornoVeiculoVO buscarListaDeVeiculoMinimaPorEmpresa(Integer idEmpresa) throws Exception {
        try {
            //Buscar o Id Cliente Trajetto
            Empresas empresas = empresasService.getById(idEmpresa);
            RetornoVeiculoVO retorno = VeiculoTrajetto.getInstance().buscarListaDeVeiculoMinimaPorEmpresa(empresas.getIdClienteTrajetto());
            return retorno;
        } catch (Exception e) {
            LogSistema.logError("listarVeiculo", e);
            throw new Exception(e);
        }
    }

    public RetornoVeiculoVO buscarListaDeVeiculoMinimaPorEmpresaAtivos(Integer idEmpresa) throws Exception {
        try {
            //Buscar o Id Cliente Trajetto
            Empresas empresas = empresasService.getById(idEmpresa);
            RetornoVeiculoVO retorno = VeiculoTrajetto.getInstance().buscarListaDeVeiculoMinimaPorEmpresaAtivos(empresas.getIdClienteTrajetto());
            return retorno;
        } catch (Exception e) {
            LogSistema.logError("listarVeiculo", e);
            throw new Exception(e);
        }
    }



    private void ajustarTiposVeiculoVO(VeiculoVO veiculoVO, Empresas empresas) {
        try {
            TiposDeVeiculoVO tiposDeVeiculoVO = new TiposDeVeiculoVO();
            if(veiculoVO.getTipoDeVeiculo() != null){
                TiposDeVeiculo tiposDeVeiculo = tiposDeVeiculoService.getById(veiculoVO.getTipoDeVeiculo());
                tiposDeVeiculoVO.setDescricao(tiposDeVeiculo.getDescricao());
                tiposDeVeiculoVO.setId(veiculoVO.getTipoDeVeiculo());
                tiposDeVeiculoVO.setPermiteAcoplamento(tiposDeVeiculo.getPermiteAcoplamento());
                tiposDeVeiculoVO.setTracaoPropria(tiposDeVeiculo.getTracaoPropria());
            }
            veiculoVO.setTiposDeVeiculo(tiposDeVeiculoVO);
            veiculoVO.setEmpresaVO(new EmpresaVO(empresas.getId()));


        } catch (Exception e) {
            LogSistema.logError("buscarPorTipoDeVeiculoVO", e);
        }

    }

    public Veiculos buscarVeiculoPelaPlaca(String placa){
        try{
            Veiculos veiculos = dao.buscarVeiculoPelaPlaca(placa);
            return veiculos;
        } catch (Exception e) {
            LogSistema.logError("buscarVeiculoPelaPlaca", e);
        }
        return null;
    }

    /*
     * Buscar a lista de veiculos com hodometro para levar ao via-m
     */
    public List<VeiculoVO> buscarListaParaMobile(Integer idMotorista){
        List<VeiculoVO> list = new ArrayList<>();
        try{
            Motoristas motoristas = driverService.getById(idMotorista);
            List<VeiculoVO>  lista = dao.buscarVeiculosParaOViaM(motoristas.getEmpresa().getIdClienteTrajetto(), motoristas.getUnidadesId());
            list.addAll(lista);
        } catch (Exception e) {
            LogSistema.logError("buscarListaParaMobile", e);
        }

        return list;
    }

    public List<VeiculoVO> buscarVeiculos(Integer idCliente, Integer idUnidade){
        return dao.buscarVeiculos(idCliente, idUnidade);
    }

}