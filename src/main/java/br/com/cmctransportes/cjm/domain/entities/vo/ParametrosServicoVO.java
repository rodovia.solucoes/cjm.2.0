package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class ParametrosServicoVO implements Serializable {
    private String uuid;
    private List<String> listaDePlacas;
}
