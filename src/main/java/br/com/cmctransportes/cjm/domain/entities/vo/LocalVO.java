package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.LatLng;
import br.com.cmctransportes.cjm.logger.LogSistema;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * @author William Leite
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class LocalVO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private Integer id;
    @Getter
    @Setter
    private String nome;
    @Getter
    @Setter
    private Double latitude;
    @Getter
    @Setter
    private Double longitude;
    @Getter
    @Setter
    private boolean permiteDescanso;
    @Getter
    @Setter
    private Double boundary;
    @Getter
    @Setter
    private Integer localPai;
    @Getter
    @Setter
    private Boolean temFilho;
    @Getter
    @Setter
    private Boolean ativo;
    @Getter
    @Setter
    private Boolean mostrarNoMapaPrincipal;
    @Getter
    @Setter
    private Boolean mostrarNomeNoMapa;
    @Getter
    @Setter
    private Boolean notificaEvento;
    @Getter
    @Setter
    private Integer codigoUnico;
    @Getter
    @Setter
    private Integer funcao;
    @Getter
    @Setter
    private String endereco;
    @Getter
    @Setter
    private String bairro;
    @Getter
    @Setter
    private String complemento;
    @Getter
    @Setter
    private String cidade;
    @Getter
    @Setter
    private String uf;
    @Getter
    @Setter
    private String contato;
    @Getter
    @Setter
    private String telefone;
    @Getter
    @Setter
    private String tipo;
    @Getter
    @Setter
    private EmpresaVO cliente;
    @Getter
    @Setter
    private List<CoordenadaVO> listaDeCoordenadas;

    public LocalVO() {

    }

    public LocalVO(Integer id, String nome, Double latitude, Double longitude) {
        this.id = id;
        this.nome = nome;
        this.latitude = latitude;
        this.longitude = longitude;
        this.permiteDescanso = false;
    }

    public LocalVO(Integer id, String nome, Double latitude, Double longitude, Boolean permite) {
        this.id = id;
        this.nome = nome;
        this.latitude = latitude;
        this.longitude = longitude;
        this.permiteDescanso = false;

        if (Objects.nonNull(permite)) {
            this.permiteDescanso = permite;
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.latitude);
        hash = 97 * hash + Objects.hashCode(this.longitude);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LocalVO other = (LocalVO) obj;
        if (!Objects.equals(this.latitude, other.latitude)) {
            return false;
        }
        if (!Objects.equals(this.longitude, other.longitude)) {
            return false;
        }
        return true;
    }


    public LatLng[] convert() {
        LatLng[] array = null;
        try {
            array = new LatLng[this.listaDeCoordenadas.size()];
            int cont = 0;
            for(CoordenadaVO coordenada : this.listaDeCoordenadas){
                array[cont] = new LatLng(coordenada.getLatitude(), coordenada.getLongitude());
                cont++;
            }
        } catch (Exception e) {
           LogSistema.logError("convert", e);
        }

        return array;
    }

}