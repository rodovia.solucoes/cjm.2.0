package br.com.cmctransportes.cjm.domain.entities.enums;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;

import java.util.Arrays;
import java.util.Objects;

/**
 * @author William Leite
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum NONCONFORMITY_CALCULUS_TYPE {
    DSR("DSR", 1),
    INTERJOURNEY("Interjornada", 2);

    @Getter
    private final int id;
    @Getter
    private final String description;

    NONCONFORMITY_CALCULUS_TYPE(final String description, final int id) {
        this.id = id;
        this.description = description;
    }

    public static NONCONFORMITY_CALCULUS_TYPE fromCode(final int code) {
        return Arrays.stream(NONCONFORMITY_CALCULUS_TYPE.values()).filter(c -> Objects.equals(c.getId(), code)).findFirst().orElse(null);
    }
}
