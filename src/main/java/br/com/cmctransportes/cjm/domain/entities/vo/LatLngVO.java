package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class LatLngVO implements Serializable {
    private double latitude;
    private double longitude;
}
