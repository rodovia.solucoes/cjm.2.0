package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.EntityLog;
import br.com.cmctransportes.cjm.domain.entities.Users;
import br.com.cmctransportes.cjm.infra.dao.EntityLogDAO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.persistence.EntityManager;
import java.util.Date;

/**
 * @param <PK>
 * @param <T>
 * @author William Leite
 */
public class EntityLogService<PK extends Integer, T> {

    public static final String INSERT = "insert";
    public static final String DELETE = "delete";
    public static final String UPDATE = "update";

    private final EntityLogDAO dao;
    private final Class<T> clazz;

    public EntityLogService(Class<T> clazz, EntityManager entityManager) {
        dao = new EntityLogDAO(entityManager);
        this.clazz = clazz;
    }

    public void log(String operacao, T obj, PK objID, Integer userID) throws JsonProcessingException {
        EntityLog log = new EntityLog();
        log.setData_operacao(new Date());
        log.setEntityId(objID);
        log.setEntityType(this.clazz.getTypeName());
        log.setOperacao(operacao);

        Users user = new Users();
        user.setId(userID);

        log.setUserId(user);

        ObjectMapper mapper = new ObjectMapper();
        log.setJson(mapper.writeValueAsString(obj));

        this.dao.save(log);
    }
}