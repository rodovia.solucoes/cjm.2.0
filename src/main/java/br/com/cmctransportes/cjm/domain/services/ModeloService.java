package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.Empresas;
import br.com.cmctransportes.cjm.domain.entities.vo.EmpresaVO;
import br.com.cmctransportes.cjm.domain.entities.vo.ModeloVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoLocalVO;
import br.com.cmctransportes.cjm.domain.entities.vo.RetornoModeloVO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.proxy.ModeloTrajetto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ModeloService {

    @Autowired
    private EmpresasService empresasService;

    public RetornoModeloVO cadastrarModeloEquipamento(ModeloVO modeloVO) throws Exception{
        try {
            return ModeloTrajetto.getInstance().cadastrarModeloEquipamento(modeloVO);
        }catch (Exception e){
            LogSistema.logError("cadastrarModeloEquipamento", e);
            throw new Exception(e);
        }
    }

    public RetornoModeloVO cadastrarModeloVeiculo(ModeloVO modeloVO) throws Exception{
        try {
            return ModeloTrajetto.getInstance().cadastrarModeloVeiculo(modeloVO);
        }catch (Exception e){
            LogSistema.logError("cadastrarModeloVeiculo", e);
            throw new Exception(e);
        }
    }

    public RetornoModeloVO editarModelo(ModeloVO modeloVO) throws Exception{
        try {
            return ModeloTrajetto.getInstance().editarModelo(modeloVO);
        }catch (Exception e){
            LogSistema.logError("editarModelo", e);
            throw new Exception(e);
        }
    }

    public RetornoModeloVO buscarModelo(Integer id) throws Exception{
        try {
            return ModeloTrajetto.getInstance().buscarModelo(id);
        }catch (Exception e){
            LogSistema.logError("buscarModelo", e);
            throw new Exception(e);
        }
    }

    public RetornoModeloVO buscarListaModeloEquipamento(){
        return ModeloTrajetto.getInstance().buscarListaModeloEquipamento();
    }

    public RetornoModeloVO buscarListaModeloVeiculo(){
        return ModeloTrajetto.getInstance().buscarListaModeloVeiculo();
    }

    public RetornoModeloVO buscarModeloPortatil(){
        return ModeloTrajetto.getInstance().buscarModeloPortatil();
    }
}
