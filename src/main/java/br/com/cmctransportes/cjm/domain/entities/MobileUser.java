package br.com.cmctransportes.cjm.domain.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author analista.ti
 * <p>
 * Classe criada para facilitar a compatibilidade entre o usuário web e o mobile
 */
@Getter
@Setter
public class MobileUser implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private String userName;
    private String senha;

    // Adicionar depois
    @JsonIgnore
    private Collection<UsersClaims> usersClaims;

    public MobileUser() {
    }

    public MobileUser(Users usr) {
        this.id = usr.getId();
        this.userName = usr.getUserName();
        this.senha = usr.getSenha();

        this.usersClaims = usr.getUsersClaims();
    }
}
