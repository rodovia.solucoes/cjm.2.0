package br.com.cmctransportes.cjm.domain.entities.jornada;

import br.com.cmctransportes.cjm.domain.entities.EstadosJornada;

import java.util.Date;

/**
 * @author analista.ti
 */
public class ControleEstado {
    // TODO: trocar por tipo estado
    private EstadosJornada estado;
    private Date inicio;
    private Date fim;
    private Long intervalo;

    // TODO: trocar para java.time.LocalDateTime

    /**
     * @return the inicio
     */
    public Date getInicio() {
        return inicio;
    }

    /**
     * @param inicio the inicio to set
     */
    public void setInicio(Date inicio) {
        this.inicio = inicio;
    }

    /**
     * @return the fim
     */
    public Date getFim() {
        return fim;
    }

    /**
     * @param fim the fim to set
     */
    public void setFim(Date fim) {
        this.fim = fim;
    }

    /**
     * @return the intervalo
     */
    public Long getIntervalo() {
        if (intervalo == null) {
            if ((inicio != null) && (fim != null)) {
                intervalo = fim.getTime() - inicio.getTime();
            }
        }

        return intervalo;
    }

    /**
     * @param intervalo the intervalo to set
     */
    public void setIntervalo(Long intervalo) {
        this.intervalo = intervalo;
    }

    /**
     * @return the estado
     */
    public EstadosJornada getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(EstadosJornada estado) {
        this.estado = estado;
    }


}
