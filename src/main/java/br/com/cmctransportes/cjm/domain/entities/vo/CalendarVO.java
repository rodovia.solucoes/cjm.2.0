package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author William Leite
 */
public class CalendarVO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private List<String> weekHeader;
    @Getter
    @Setter
    private Map<Integer, CalendarWeekVO> weeks;
    @Getter
    @Setter
    private Long maxWorked;
    @Getter
    @Setter
    private Long maxRemaining;
    @Getter
    @Setter
    private List<NonconformityVO> orphanNonconformities;
}
