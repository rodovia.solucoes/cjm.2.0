package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "evento_motorista")
@Getter
@Setter
public class EventoMotorista implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "evento_motorista_id_seq", sequenceName = "evento_motorista_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "evento_motorista_id_seq")
    @Column(name = "id")
    private Integer id;

    /**
     * tipo 1 - Carregando, 2 - Vazio
     */
    @Column(name = "tipo")
    private Integer tipo;

    @Column(name = "data_evento", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataEvento;

    @Column(name = "data_fim_evento")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataFimEvento;

    @JoinColumn(name = "motorista", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Motoristas motoristas;

    @Column(name = "hodometro_inicial")
    private Integer hodometroInicial;

    @Column(name = "hodometro_final")
    private Integer hodometroFinal;

    @Transient
    private String placa;

    @Column(name = "id_veiculo_trajetto")
    private Integer idVeiculo;

    @Transient
    private Integer hodometroInicialTelemetria;
    @Transient
    private Integer hodometroFinalTelemetria;

}
