package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author analista.ti
 */
@Entity
@Table(name = "motivos")
@Getter
@Setter
public class Motivo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name = "MOTIVOS_ID_SEQ", sequenceName = "MOTIVOS_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MOTIVOS_ID_SEQ")
    @Column(name = "id")
    private Integer id;

    @Column(name = "descricao", length = 60, nullable = false)
    private String descricao;

    @ManyToOne
    @JoinColumn(name = "empresa_id")
    private Empresas empresaId;
}