package br.com.cmctransportes.cjm.domain.entities.jornada;

import br.com.cmctransportes.cjm.constants.Constantes;
import br.com.cmctransportes.cjm.domain.entities.*;
import br.com.cmctransportes.cjm.domain.entities.enums.EVENT_TYPE;
import br.com.cmctransportes.cjm.domain.entities.enums.JOURNEY_TYPE;
import br.com.cmctransportes.cjm.domain.entities.vo.NonconformityVO;
import br.com.cmctransportes.cjm.domain.services.EventosJornadaService;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.utils.TimeHelper;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import java.util.*;

/**
 * @author analista.ti
 */
public class ApuracaoDeJornada {

    @JsonIgnore
    @Getter
    @Setter
    private boolean isJornadaMesmoDia;

    @Getter
    @Setter
    private Date data;

    // Horas Extras Diurnas (hed)
    @Getter
    @Setter
    private Long horasExtrasDiurnas;
    @Getter
    @Setter
    private Long hedIntraJornada;
    @Getter
    @Setter
    private Long hedDSR;
    @Getter
    @Setter
    private Long hedFeriado; // TODO:

    // Horas Extras Noturnas (hen)
    @Getter
    @Setter
    private Long horasExtrasNoturnas;
    @Getter
    @Setter
    private Long henIntraJornada;
    @Getter
    @Setter
    private Long henDSR; // TODO:
    @Getter
    @Setter
    private Long henFeriado; // TODO:

    @Getter
    private Long horasExtrasInterJornadas; // TODO:

    @Getter
    private Long tempoDeEsperaDiurno;
    @Getter
    private Long tempoDeEsperaNoturno;

    @Getter
    @Setter
    private Long adicionalNoturno;

    @Getter
    @Setter
    private Long horasFaltosasAbonadas;
    @Getter
    @Setter
    private Long horasFaltosasDescontadas; // TODO:
    @Getter
    @Setter
    private Long horasApuradas;

    @Getter
    @Setter
    private String tipoJornada;

    @Getter
    @Setter
    private List<NonconformityVO> dsrNonconformities;
    @Getter
    @Setter
    private List<NonconformityVO> interjourneyNonconformities;

    //    private static final long ONZE_HORAS = (1000 * 60 * 60 * 11);
    public ApuracaoDeJornada() {
        this.dsrNonconformities = new ArrayList<>(0);
    }

    @Override
    @SuppressWarnings({"CloneDoesntCallSuperClone", "CloneDeclaresCloneNotSupported"})
    public ApuracaoDeJornada clone() {
        ApuracaoDeJornada result = new ApuracaoDeJornada();
        result.setAdicionalNoturno(this.getAdicionalNoturno());
        result.setData(this.getData());

        result.setDsrNonconformities(new ArrayList<>(0));
        if (Objects.nonNull(this.getDsrNonconformities())) {
            this.getDsrNonconformities().forEach(d -> result.getDsrNonconformities().add(d));
        }

        result.setHedDSR(this.getHedDSR());
        result.setHedFeriado(this.getHedFeriado());
        result.setHedIntraJornada(this.getHedIntraJornada());
        result.setHenDSR(this.getHenDSR());
        result.setHenFeriado(this.getHenFeriado());
        result.setHenIntraJornada(this.getHenIntraJornada());
        result.setHorasApuradas(this.getHorasApuradas());
        result.setHorasExtrasDiurnas(this.getHorasExtrasDiurnas());
        result.setHorasExtrasInterJornadas(this.getHorasExtrasInterJornadas());
        result.setHorasExtrasNoturnas(this.getHorasExtrasNoturnas());
        result.setHorasFaltosasAbonadas(this.getHorasFaltosasAbonadas());
        result.setHorasFaltosasDescontadas(this.getHorasFaltosasDescontadas());

        result.setInterjourneyNonconformities(new ArrayList<>(0));
        if (Objects.nonNull(this.getInterjourneyNonconformities())) {
            this.getInterjourneyNonconformities().forEach(i -> result.getInterjourneyNonconformities().add(i));
        }

        result.setTempoDeEsperaDiurno(this.getTempoDeEsperaDiurno());
        result.setTempoDeEsperaNoturno(this.getTempoDeEsperaNoturno());
        result.setTipoJornada(this.getTipoJornada());

        return result;
    }

    /**
     * @param horasExtrasInterJornadas the horasExtrasInterJornadas to set
     */
    public void setHorasExtrasInterJornadas(Long horasExtrasInterJornadas) {
        if ((horasExtrasInterJornadas == null) || (horasExtrasInterJornadas < 0)) {
            return;
        }

        this.horasExtrasInterJornadas = horasExtrasInterJornadas;
    }

    /**
     * @param tempoDeEsperaDiurno the tempoDeEsperaDiurno to set
     */
    public void setTempoDeEsperaDiurno(Long tempoDeEsperaDiurno) {
        if (tempoDeEsperaDiurno != null) {
            this.tempoDeEsperaDiurno = tempoDeEsperaDiurno;
        }
    }

    /**
     * @param tempoDeEsperaNoturno the tempoDeEsperaNoturno to set
     */
    public void setTempoDeEsperaNoturno(Long tempoDeEsperaNoturno) {
        if (tempoDeEsperaNoturno != null) {
            this.tempoDeEsperaNoturno = tempoDeEsperaNoturno;
        }
    }

    public void processa(Jornada jornada, EventosJornadaService eventosJornadaService) {
        horasExtrasDiurnas = new Long(0);
        hedIntraJornada = new Long(0);
        hedFeriado = new Long(0);
        hedDSR = new Long(0);
        horasExtrasNoturnas = new Long(0);
        henIntraJornada = new Long(0);
        henFeriado = new Long(0);
        henDSR = new Long(0);
        horasExtrasInterJornadas = new Long(0);
        adicionalNoturno = new Long(0);
        horasFaltosasAbonadas = new Long(0);
        horasFaltosasDescontadas = new Long(0);

        setTempoDeEsperaDiurno(new Long(0));
        setTempoDeEsperaNoturno(new Long(0));

        Optional<Evento> optEvento = jornada.getEventos().stream().findFirst();
        ///jornada.processaEventos();

        setData(jornada.getJornadaInicio());

        if (optEvento.isPresent()) {
            Evento evt = optEvento.get();

            // se o 1o não for do tipo 1, é tipo de jornada diferente
            if (evt.getTipoEvento().getId() != 1) {
                if (evt.getMotivoAbono() != null) {
                    setTipoJornada(evt.getMotivoAbono().getDescricao());
                } else if (evt.getTipoEvento().getId() == EVENT_TYPE.INTERJORNADA.getStartCode()) {
                    setTipoJornada(EVENT_TYPE.DSR.getDescription());
                    Optional<Evento> dsrEvent = jornada.getEventos().stream().filter(e -> e.getTipoEvento().getId() == EVENT_TYPE.DSR.getStartCode()).findAny();
                    if (dsrEvent.isPresent()) {
                        setData(dsrEvent.get().getInstanteEvento());
                    } else {
                        System.out.println("Jornada DSR não possui evento de DSR!");
                    }
                } else if (Objects.isNull(evt.getTipoEvento().getObjGeraEstado())) {
                    EventosJornada tipo = eventosJornadaService.getById(evt.getTipoEvento().getId());
                    setTipoJornada(tipo.getObjGeraEstado().getDescricao());
                } else {
                    setTipoJornada(evt.getTipoEvento().getObjGeraEstado().getDescricao());
                }
            }
        }

        setHedDSR(jornada.getHedDSR());
        setHenDSR(jornada.getHenDSR());

        if (Objects.nonNull(jornada.getHorasExtrasAbonada()) && jornada.getHorasExtrasAbonada()) {
            long totalTurno = 43200000l;
            if (jornada.getMotoristasId().getTurnosId().getTipo() != 0) {//caso nao seja do turno 12 x 36
                //aqui vamos ajustando.
                totalTurno = calcularTempoEfetivo(jornada.getMotoristasId()) + calcularAlmocoEfetivo(jornada.getMotoristasId());
            }
            if (jornada.getHedFeriado() > totalTurno) {
                setHedFeriado(jornada.getHedFeriado() - totalTurno);
            } else {
                setHedFeriado(0l);
            }

            if (jornada.getHenFeriado() > totalTurno) {
                setHenFeriado(jornada.getHenFeriado() - totalTurno);
            } else {
                setHenFeriado(0l);
            }

        } else {
            setHedFeriado(jornada.getHedFeriado());
            setHenFeriado(jornada.getHenFeriado());
        }

        setHorasApuradas(jornada.getTrabalhado());

        if (Objects.nonNull(jornada.getHorasExtrasAbonada()) && jornada.getHorasExtrasAbonada()) {
            setHorasFaltosasAbonadas(0l);
            setHorasFaltosasDescontadas(0l);
        } else {
            // Não entra em he100%{
            setHorasFaltosasAbonadas(jornada.getHorasAbonadas());
            setHorasFaltosasDescontadas(jornada.getHorasFaltosas() + jornada.getDescontoAlmoco());
        }

        setTempoDeEsperaDiurno(jornada.getEsperaDiurno());
        setTempoDeEsperaNoturno(jornada.getEsperaNoturno());
        UsersBranch usersBranch = jornada.getMotoristasId().getUser().getUsersBranch().iterator().next();
        if (Objects.nonNull(usersBranch) && Objects.nonNull(usersBranch.getBranch())
                && Objects.nonNull(usersBranch.getBranch().getUsarTurnoQuatroDois())
                && usersBranch.getBranch().getUsarTurnoQuatroDois()) {
            if (Objects.nonNull(jornada.getHorasExtrasAbonada()) && jornada.getHorasExtrasAbonada()) {
                long totalTurno = 43200000l;
                if (jornada.getMotoristasId().getTurnosId().getTipo() != 0) {//caso nao seja do turno 12 x 36
                    long totalTrabalhado = jornada.getTrabalhado();
                    long turnoDaJornada = calcularTempoEfetivo(jornada.getMotoristasId());
                    if (totalTrabalhado > turnoDaJornada) {
                        long horasExtrasAbonada = totalTrabalhado - turnoDaJornada;
                        long horasExtras = jornada.getHorasExtras();
                        if (Objects.nonNull(jornada.getAdicionalNoturno()) && jornada.getAdicionalNoturno() > 0l) {
                            if (horasExtrasAbonada > jornada.getAdicionalNoturno()) {
                                if(jornada.getAdicionalNoturno() > 0 && jornada.getHorasExtrasDiurnas() == 0){
                                    long porcentagemNoturna = (jornada.getAdicionalNoturno() * 100) / totalTrabalhado;
                                    long totalNoturno = (horasExtrasAbonada * porcentagemNoturna) /100l;
                                    long totalDiurno = horasExtrasAbonada - totalNoturno;
                                    setHorasExtrasDiurnas(totalDiurno);
                                    setHorasExtrasNoturnas(totalNoturno);
                                    setAdicionalNoturno(totalNoturno);
                                }else {
                                    setHorasExtrasDiurnas(horasExtrasAbonada - jornada.getAdicionalNoturno());
                                    setHorasExtrasNoturnas(jornada.getAdicionalNoturno());
                                    setAdicionalNoturno(jornada.getAdicionalNoturno());
                                }
                            } else {
                                if(Objects.nonNull(jornada.getHorasExtrasDiurnas()) && jornada.getHorasExtrasDiurnas() == 0 ){
                                    long porcentagemNoturna = (jornada.getAdicionalNoturno() * 100) / totalTrabalhado;
                                    long totalNoturno = (horasExtrasAbonada * porcentagemNoturna) /100l;
                                    long totalDiurno = horasExtrasAbonada - totalNoturno;
                                    setHorasExtrasDiurnas(totalDiurno);
                                    setHorasExtrasNoturnas(totalNoturno);
                                    setAdicionalNoturno(jornada.getAdicionalNoturno());
                                }else {
                                    long porcentagemHorasExtraDiurnas = 0l;
                                    long porcentagemHorasExtraNoturnas = 0l;
                                    if (Objects.nonNull(jornada.getHorasExtrasDiurnas()) && jornada.getHorasExtrasDiurnas() > 0) {
                                       if(horasExtras > 0) {
                                           porcentagemHorasExtraDiurnas = (jornada.getHorasExtrasDiurnas() * 100L) / horasExtras;
                                       }
                                        setAdicionalNoturno(jornada.getAdicionalNoturno());
                                    }
                                    if (Objects.nonNull(jornada.getHorasExtrasNoturnas()) && jornada.getHorasExtrasNoturnas() > 0) {
                                        if(horasExtras > 0) {
                                            porcentagemHorasExtraNoturnas = (jornada.getHorasExtrasNoturnas() * 100L) / horasExtras;
                                        }
                                        setAdicionalNoturno(jornada.getAdicionalNoturno());
                                    }
                                    if (porcentagemHorasExtraDiurnas > 0) {
                                        long porcentagem = (horasExtrasAbonada * porcentagemHorasExtraDiurnas) / 100L;
                                        setHorasExtrasDiurnas(porcentagem);
                                    } else {
                                        setHorasExtrasDiurnas(0l);
                                    }

                                    if (porcentagemHorasExtraNoturnas > 0) {
                                        long porcentagem = (horasExtrasAbonada * porcentagemHorasExtraNoturnas) / 100L;
                                        setHorasExtrasNoturnas(porcentagem);
                                        setAdicionalNoturno(jornada.getAdicionalNoturno());
                                    } else {
                                        setHorasExtrasNoturnas(0l);
                                    }
                                }
                            }
                        } else {
                            if (Objects.nonNull(jornada.getAdicionalNoturno()) && jornada.getAdicionalNoturno() > 0l) {
                                setHorasExtrasNoturnas(jornada.getHorasExtrasNoturnas());
                            } else {
                                setHorasExtrasNoturnas(0l);
                            }
                            setHorasExtrasDiurnas(horasExtrasAbonada);
                            setAdicionalNoturno(jornada.getAdicionalNoturno());
                        }

                    } else {
                        setHorasExtrasNoturnas(0l);
                        setHorasExtrasDiurnas(0l);
                    }
                } else {
                    if (jornada.getHorasExtrasDiurnas() > totalTurno) {
                        setHorasExtrasDiurnas(jornada.getHorasExtrasDiurnas() - totalTurno);
                    } else {
                        setHorasExtrasDiurnas(0l);
                    }

                    if (jornada.getHorasExtrasNoturnas() > totalTurno) {
                        setHorasExtrasNoturnas(jornada.getHorasExtrasDiurnas() - totalTurno);
                        setAdicionalNoturno(jornada.getAdicionalNoturno());
                    } else {
                        setHorasExtrasNoturnas(0l);
                    }
                }
            }
            else {
                if(jornada.getType().equals(JOURNEY_TYPE.JORNADA)
                        && jornada.getHorasExtrasNoturnas() > 0
                        && jornada.getHorasExtras() > 0
                        && jornada.getAdicionalNoturno() > 0
                        && jornada.getHorasExtrasDiurnas() == 0){
                        long adicionalNoturno = jornada.getAdicionalNoturno();
                        setHorasExtrasDiurnas(jornada.getHorasExtrasDiurnas());
                        setHorasExtrasNoturnas(jornada.getHorasExtrasNoturnas());
                }else {
                    setHorasExtrasDiurnas(jornada.getHorasExtrasDiurnas());
                    if(jornada.getHorasExtrasNoturnas() > 0){
                        setHorasExtrasNoturnas(jornada.getHorasExtrasNoturnas());
                    }else{
                        setHorasExtrasNoturnas(0l);
                    }

                }
                setAdicionalNoturno(jornada.getAdicionalNoturno());
                /*
                setHorasExtrasDiurnas(jornada.getHorasExtrasDiurnas());
                setHorasExtrasNoturnas(jornada.getHorasExtrasNoturnas());
                setAdicionalNoturno(jornada.getAdicionalNoturno());

                 */
            }
        } else {
            setHorasExtrasDiurnas(jornada.getHorasExtrasDiurnas());
            setHorasExtrasNoturnas(jornada.getHorasExtrasNoturnas());
            setAdicionalNoturno(jornada.getAdicionalNoturno());
        }

        setHedIntraJornada(jornada.getHedIntraJornada());
        setHenIntraJornada(jornada.getHenIntraJornada());

        final ObjectMapper objectMapper = new ObjectMapper();
        final TypeReference<HashMap<String, String>> typeReference = new TypeReference<HashMap<String, String>>() {
        };

        // Interjourney Nonconformities
        if (Objects.nonNull(this.getInterjourneyNonconformities()) && !this.getInterjourneyNonconformities().isEmpty()) {
            this.getInterjourneyNonconformities().stream().filter(n -> Objects.nonNull(n.getTreatmentArgs())).forEach((NonconformityVO n) -> {
                Map<String, String> args = null;
                try {
                    args = objectMapper.readValue(n.getTreatmentArgs(), typeReference);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

                if (Objects.nonNull(args)) {
                    switch (n.getType()) {
                        case INTERJOURNEY_NORMAL_PAY:
                            this.setHorasApuradas(this.getHorasApuradas() + Long.valueOf(args.get("amount")));
                            break;
                        case INTERJOURNEY_EXTRA_PAY:
                            this.setHorasExtrasInterJornadas(this.getHorasExtrasInterJornadas() + Long.valueOf(args.get("amount")));
                            break;
                        case INTERJOURNEY_NIGHT_PAY:
                            this.setHorasExtrasNoturnas(this.getHorasExtrasNoturnas() + Long.valueOf(args.get("extraNight")));
                            this.setAdicionalNoturno(this.getAdicionalNoturno() + Long.valueOf(args.get("additionalNight")));
                            this.setHorasExtrasInterJornadas(this.getHorasExtrasInterJornadas() + Long.valueOf(args.get("interjourney")));
                            break;
                    }
                }
            });
        }

        // Dsr Nonconformities
        if (Objects.nonNull(this.getDsrNonconformities()) && !this.getDsrNonconformities().isEmpty()) {
            this.getDsrNonconformities().stream().filter(n -> Objects.nonNull(n.getTreatmentArgs())).forEach(n -> {
                Map<String, String> args = null;
                try {
                    args = objectMapper.readValue(n.getTreatmentArgs(), typeReference);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

                if (Objects.nonNull(args)) {
                    switch (n.getType()) {
//                        case INVALID_DSR_COUNT:
//                            Long diff = Long.valueOf(args.get("weeksPassed")) - Long.valueOf(args.get("dsrCount"));
//                            Long overtime = TimeHelper.hours(11) + (diff * TimeHelper.hours(24));
//                            this.setHedDSR(this.getHedDSR() + overtime);
//                            break;
                        case DSR_LACKING_COMPENSATION:
                            this.setHedDSR(this.getHedDSR() + (TimeHelper.hours(5) - Long.valueOf(args.get("compensated"))));
                            break;
                    }
                }
            });
        }

        if (Objects.nonNull(jornada.getDsrHE()) && jornada.getDsrHE()) {
            setHorasExtrasDiurnas(0L);
            setHorasExtrasNoturnas(0L);
        }

    }


    private long calcularTempoEfetivo(Motoristas motoristas) {
        try {
            switch (TimeHelper.pegarDiaDaSemana()) {
                case 1: {
                    if (Objects.nonNull((motoristas)) && Objects.nonNull(motoristas.getTurnosId())
                            && Objects.nonNull(motoristas.getTurnosId().getTempoDiario01())) {
                        return motoristas.getTurnosId().getTempoDiario01();
                    }
                }
                break;
                case 2: {
                    if (Objects.nonNull((motoristas)) && Objects.nonNull(motoristas.getTurnosId())
                            && Objects.nonNull(motoristas.getTurnosId().getTempoDiario02())) {
                        return motoristas.getTurnosId().getTempoDiario02();
                    }
                }
                break;
                case 3: {
                    if (Objects.nonNull((motoristas)) && Objects.nonNull(motoristas.getTurnosId())
                            && Objects.nonNull(motoristas.getTurnosId().getTempoDiario03())) {
                        return motoristas.getTurnosId().getTempoDiario03();
                    }
                }
                break;
                case 4: {
                    if (Objects.nonNull((motoristas)) && Objects.nonNull(motoristas.getTurnosId())
                            && Objects.nonNull(motoristas.getTurnosId().getTempoDiario04())) {
                        return motoristas.getTurnosId().getTempoDiario04();
                    }
                }
                break;
                case 5: {
                    if (Objects.nonNull((motoristas)) && Objects.nonNull(motoristas.getTurnosId())
                            && Objects.nonNull(motoristas.getTurnosId().getTempoDiario05())) {
                        return motoristas.getTurnosId().getTempoDiario05();
                    }
                }
                break;
                case 6: {
                    if (Objects.nonNull((motoristas)) && Objects.nonNull(motoristas.getTurnosId())
                            && Objects.nonNull(motoristas.getTurnosId().getTempoDiario06())) {
                        return motoristas.getTurnosId().getTempoDiario06();
                    }
                }
                break;
                case 7: {
                    if (Objects.nonNull((motoristas)) && Objects.nonNull(motoristas.getTurnosId())
                            && Objects.nonNull(motoristas.getTurnosId().getTempoDiario07())) {
                        return motoristas.getTurnosId().getTempoDiario07();
                    }
                }
                break;

            }
        } catch (Exception e) {
            LogSistema.logError("calcularTempoDeTermino", e);
        }
        return 26400000;
    }

    private long calcularAlmocoEfetivo(Motoristas motoristas) {
        try {
            switch (TimeHelper.pegarDiaDaSemana()) {
                case 1: {
                    if (Objects.nonNull((motoristas)) && Objects.nonNull(motoristas.getTurnosId())
                            && Objects.nonNull(motoristas.getTurnosId().getTempoAlimentacao01())) {
                        return motoristas.getTurnosId().getTempoAlimentacao01();
                    }
                }
                break;
                case 2: {
                    if (Objects.nonNull((motoristas)) && Objects.nonNull(motoristas.getTurnosId())
                            && Objects.nonNull(motoristas.getTurnosId().getTempoAlimentacao02())) {
                        return motoristas.getTurnosId().getTempoAlimentacao02();
                    }
                }
                break;
                case 3: {
                    if (Objects.nonNull((motoristas)) && Objects.nonNull(motoristas.getTurnosId())
                            && Objects.nonNull(motoristas.getTurnosId().getTempoAlimentacao03())) {
                        return motoristas.getTurnosId().getTempoAlimentacao03();
                    }
                }
                break;
                case 4: {
                    if (Objects.nonNull((motoristas)) && Objects.nonNull(motoristas.getTurnosId())
                            && Objects.nonNull(motoristas.getTurnosId().getTempoAlimentacao04())) {
                        return motoristas.getTurnosId().getTempoAlimentacao04();
                    }
                }
                break;
                case 5: {
                    if (Objects.nonNull((motoristas)) && Objects.nonNull(motoristas.getTurnosId())
                            && Objects.nonNull(motoristas.getTurnosId().getTempoAlimentacao05())) {
                        return motoristas.getTurnosId().getTempoAlimentacao05();
                    }
                }
                break;
                case 6: {
                    if (Objects.nonNull((motoristas)) && Objects.nonNull(motoristas.getTurnosId())
                            && Objects.nonNull(motoristas.getTurnosId().getTempoAlimentacao06())) {
                        return motoristas.getTurnosId().getTempoAlimentacao06();
                    }
                }
                break;
                case 7: {
                    if (Objects.nonNull((motoristas)) && Objects.nonNull(motoristas.getTurnosId())
                            && Objects.nonNull(motoristas.getTurnosId().getTempoAlimentacao07())) {
                        return motoristas.getTurnosId().getTempoAlimentacao07();
                    }
                }
                break;

            }
        } catch (Exception e) {
            LogSistema.logError("calcularAlmoçoEfetivo", e);
        }
        return 4320000;
    }
}
