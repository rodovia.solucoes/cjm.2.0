package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.Viagem;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class EnvioViagemVO implements Serializable {
    private List<Viagem> listaDeViagem;
    private List<Integer> listaViagemExcluir;
    private String uuid;
}
