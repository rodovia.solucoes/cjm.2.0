package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.ControleCombustivel;
import br.com.cmctransportes.cjm.infra.dao.ControleCombustivelDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public class ControleCombustivelService {
    private final EntityManager em;
    private ControleCombustivelDAO controleCombustivelDAO;

    @Autowired
    private MotoristasVeiculosService motoristasVeiculosService;

    public ControleCombustivelService(EntityManager em) {
        this.em = em;
        this.controleCombustivelDAO = new ControleCombustivelDAO(em);
    }


    @Transactional
    public void save(ControleCombustivel entity) throws Exception {
        try{
            validarTipoCombustivel(entity);
            entity.setId(null);
            this.controleCombustivelDAO.save(entity);
        } catch (Exception e) {
            throw e;
        }
    }


    @Transactional
    public void saveFromRest(ControleCombustivel entity) throws Exception {
        try{
            Integer idVeiculo = motoristasVeiculosService.buscarIdDoVeiculoPeloMotorista(entity.getIdMotorista());
            entity.setIdVeiculo(idVeiculo);
            entity.setId(null);
            this.controleCombustivelDAO.save(entity);
        } catch (Exception e) {
            throw e;
        }
    }

    private void validarTipoCombustivel(ControleCombustivel controleCombustivel){
        if("Álcool".equalsIgnoreCase(controleCombustivel.getTipoCombustivel())){
            controleCombustivel.setTipoCombustivel("AL");
        }

        if("Diesel".equalsIgnoreCase(controleCombustivel.getTipoCombustivel())){
            controleCombustivel.setTipoCombustivel("DI");
        }

        if("Gasolina".equalsIgnoreCase(controleCombustivel.getTipoCombustivel())){
            controleCombustivel.setTipoCombustivel("GA");
        }
    }

    public List<ControleCombustivel> buscarPorVeiculo(Integer idVeiculo) throws Exception {
        try{
            return this.controleCombustivelDAO.findAll(idVeiculo);
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional
    public void excluir(Integer idControle){
        try{
            ControleCombustivel cc = this.controleCombustivelDAO.getById(idControle);
            this.controleCombustivelDAO.delete(cc);
        }catch (Exception e){
            throw e;
        }
    }
}
