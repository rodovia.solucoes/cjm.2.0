package br.com.cmctransportes.cjm.domain.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "viagem")
@Getter
@Setter
public class Viagem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "viagem_id_seq", sequenceName = "viagem_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "viagem_id_seq")
    @Column(name = "id")
    private Integer id;

    @Column(name = "placa")
    private String placa;

    @Column(name = "data_cadastro", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date dataCadastro;

    @Column(name = "meta")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal meta;

    @Column(name = "faturamento")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal faturamento;

    @Column(name = "projecao")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal projecao;

    @Column(name = "media_faturamento")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal mediaFaturamento;

    @Column(name = "dispersao")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal dispersao;

    @Column(name = "quantidade_manifesto")
    private Integer quantidadeManifesto;

    @Column(name = "inicio_da_viagem", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date inicioDaViagem;

    @Column(name = "fim_da_viagem", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date fimDaViagem;
}
