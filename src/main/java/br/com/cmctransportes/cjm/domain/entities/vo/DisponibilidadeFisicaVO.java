package br.com.cmctransportes.cjm.domain.entities.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class DisponibilidadeFisicaVO implements Serializable {
    private List<DadosDisponibilidadeFisicaVO> listaDadosDisponibilidadeFisica;
}
