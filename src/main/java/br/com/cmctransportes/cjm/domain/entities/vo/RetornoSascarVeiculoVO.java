package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.SascarVeiculo;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RetornoSascarVeiculoVO extends RetornoVO {
    @Getter
    @Setter
    private SascarVeiculo sascarVeiculo;
    @Getter
    @Setter
    private List<SascarVeiculo> listaSascarVeiculo;
}
