package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.Evento;
import br.com.cmctransportes.cjm.domain.entities.jornada.Resumo;
import br.com.cmctransportes.cjm.utils.TimeHelper;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Objects;

/**
 * @author William Leite
 */
@Getter
@Setter
public class PainelDiarioItemVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String color;
    private String label;
    private Long dado;

    private String inicio;
    private String termino;
    private String finalizado;
    private Long inicioTime;
    private Long workHours;

    public PainelDiarioItemVO(String color, Long dado, String label) {
        this.color = color;
        this.dado = dado;
        this.label = label;
    }

    /**
     * Construtor adicional para atender a construir da tabela do painel diário do motorista.
     *
     * @param resumo
     * @param label
     * @param workHours
     * @author Vinicius Prado
     */
    public PainelDiarioItemVO(Resumo resumo, String label, Long workHours) {
        this.color = resumo.getColor();
        this.dado = resumo.getSomatorio();
        this.label = label;
        prepadaDataToTable(resumo, workHours);
    }

    /**
     * Recebe o resumo extrai os dados para montar a tabela do painel diário do motorista.
     *
     * @param resumo
     * @param restante
     * @author Vinicius Prado
     */
    private void prepadaDataToTable(Resumo resumo, Long restante) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy HH:mm");

        resumo.getLista().forEach(a -> {
            if (Objects.nonNull(a.getInstanteEvento()) && Objects.nonNull(a.getFimEvento())) {
                this.setInicio(sdf.format(a.getInstanteEvento()).replace(" ", " | "));
                this.setTermino(sdf.format(a.getFimEvento()).replace(" ", " | "));
                calculatorDiffHours(a, restante);
            }
        });
    }

    /**
     * Calculo a diferença das horas de início de trabalho e o total da jornada efetiva.
     *
     * @param evento
     * @param restante
     * @author Vinicius Prado
     */
    private void calculatorDiffHours(Evento evento, Long restante) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");

        this.setFinalizado(sdf.format(evento.getFimEvento()).concat(" | ").concat(TimeHelper
                .beautifyTime(evento.getFimEvento().getTime() - evento.getInstanteEvento().getTime(), false)));
    }
}
