package br.com.cmctransportes.cjm.domain.entities;

import br.com.cmctransportes.cjm.domain.entities.vo.MotoristaVO;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

/**
 * @author analista.ti
 */
@Entity
@Table(name = "users")
@Getter
@Setter
@NamedQueries({@NamedQuery(name = "Users.findAll", query = "SELECT u FROM Users u"),
        @NamedQuery(name = "Users.findById", query = "SELECT u FROM Users u WHERE u.id = :id"),
        @NamedQuery(name = "Users.findByUserName", query = "SELECT u FROM Users u WHERE u.userName = :userName"),
        @NamedQuery(name = "Users.findBySenha", query = "SELECT u FROM Users u WHERE u.senha = :senha")})
public class Users implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "USERS_ID_SEQ", sequenceName = "USERS_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USERS_ID_SEQ")
    @Column(name = "id")
    private Integer id;

    @Column(name = "user_name", length = 25, nullable = false)
    private String userName;

    @Column(name = "senha")
    private String senha;

    @Column(name = "email", length = 100)
    private String email;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    @JsonManagedReference("usersClaims")
    private Collection<UsersClaims> usersClaims;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    @JsonManagedReference("usersBranch")
    private Collection<UsersBranch> usersBranch;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    @JsonManagedReference("usersCompany")
    private Collection<UsersCompany> usersCompany;

    @Column(name = "senha_cripto", length = 255, nullable = false)
    private String senhaCripto;

    @Column(name = "situacao", nullable = false)
    private Boolean situacao = true;

    @Column(name = "receber_alertas_eventos")
    private Boolean receberAlertasEventos;

    @Column(name = "mandar_email")
    private Boolean mandarEmail;

    @Column(name = "mandar_sms")
    private Boolean mandarSms;

    @Column(name = "mandar_sms_excesso_velocidade")
    private Boolean mandarSmsExcessoVelocidade;

    @Size(max = 20)
    @Column(name = "numero_celular_envio_sms")
    private String numeroCelularEnvioSms;

    @Size(max = 160)
    @Column(name = "token_mobile")
    private String tokenMobile;

    @Size(max = 6)
    @Column(name = "tipo_sistema_mobile")
    private String tipoSistemaMobile;

    @Column(name = "data_ultima_atualizacao_token")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataUltimaAtualizacaoToken;

    @Size(max = 5)
    @Column(name = "tempo_atualizacao_dashboard")
    private String tempoAtualizacaoDashboard;


    @Column(name = "consulta_jornada")
    private Boolean consultaJornada;

    @Column(name = "consulta_grade_jornada")
    private Boolean consulataGradeJornada;

    @Column(name = "relatorios_viam_marcacao")
    private Boolean relatorioViamMarcacao;

    @Column(name = "relatorios_viam_chat")
    private Boolean relatorioViamChat;

    @Column(name = "relatorios_viam_evento")
    private Boolean relatorioViamEvento;

    @Column(name = "relatorios_viasat_carama_fria")
    private Boolean relatorioViasarCamaraFria;

    @Column(name = "relatorios_viasat_disponibilidade")
    private Boolean relatorioViasatDisponibilidade;

    @Column(name = "relatorios_viasat_dispositivo_portatil")
    private Boolean relatorioViaSarDispositivoPortatil;

    @Column(name = "relatorios_viasat_eventos")
    private Boolean relatorioViaSatEvento;

    @Column(name = "relatorios_viasat_horimetro")
    private Boolean relatorioViaSatHorimetro;

    @Column(name = "relatorios_viasat_hodometro")
    private Boolean relatorioViaSatHodometro;

    @Column(name = "relatorios_viasat_percurso")
    private Boolean ralatorioViaSatPercurso;

    @Column(name = "relatorios_viasat_velocidade")
    private Boolean relatorioViaSatVelocidade;

    @Column(name = "relatorios_viasat_tomada_forca")
    private Boolean relatorioViaSatTomadaForca;

    @Column(name = "cadastro_unidade")
    private Boolean cadastroUnidade;

    @Column(name = "cadastro_motoristas")
    private Boolean cadastroMotorista;

    @Column(name = "cadastro_veiculos")
    private Boolean cadastroVeiculo;

    @Column(name = "cadastro_locais")
    private Boolean cadastroLocal;

    @Column(name = "cadastro_rota")
    private Boolean cadastroRota;

    @Column(name = "cadastro_feriados")
    private Boolean cadastroFeriados;

    @Column(name = "cadastro_usuario")
    private Boolean cadastroUsuario;

    @Column(name = "cadastro_cor")
    private Boolean cadastroCor;

    @Column(name = "cadastro_tolerancia")
    private Boolean cadastroTolerancia;

    @Column(name = "cadastro_turno")
    private Boolean cadastroTurno;

    @Column(name = "cadastro_motivo")
    private Boolean cadastroMotivo;

    @Column(name = "cadastro_equipamento_portatil")
    private Boolean cadastroEquipamentoPortatil;

    @Column(name = "cadastro_tipo_veiculo")
    private Boolean cadastroTipoVeiculo;

    @Column(name = "cadastro_modelo_veiculo")
    private Boolean cadastroModeloVeiculo;

    @Column(name = "cadastro_tipo_equipamento")
    private Boolean cadastroTipoEquipamento;

    @Column(name = "acessar_integracao")
    private Boolean acessarIntegracao;

    @Column(name = "acessar_viam")
    private Boolean acessarViam;

    @Column(name = "acessar_viasat")
    private Boolean acessarViaSat;

    @Column(name = "relatorios_diaria_motorista")
    private Boolean acessarRelatorioDiariaMotorista;

    @Column(name = "cadastro_diaria_motorista")
    private Boolean acessarCadastroDiariaMotorista;

    @Column(name = "cadastro_macro")
    private Boolean acessarMacro;

    @Column(name = "cadastro_operador")
    private Boolean acessarOperador;

    @Column(name = "cadastro_Hodometro")
    private Boolean acessarHodometro;

    @Column(name = "relatorio_presenca_equipamento")
    private Boolean acessarRelatorioPresencaEquipamento;

    @Column(name = "relatorio_trasnporte_equipamento")
    private Boolean acessarRelatorioTransporteEquipamento;

    @Column(name = "relatorios_sensor_betoneira")
    private Boolean acessarRelatorioSensorBetoneira;

    @Column(name = "cadastro_controle_combustivel")
    private Boolean acessarControleCombustivel;

    @Column(name = "cadastrar_tipo_ranking")
    private Boolean cadastrarTipoRanking;

    @Column(name = "acesso_ranking_motorista")
    private Boolean acessoRankingMotorista;

    @Column(name = "acessar_gestao")
    private Boolean acessarGestao;

    @Column(name = "acessar_sbh")
    private Boolean acessoSbh;

    @Column(name = "relatorio_viam_evento_motorista")
    private Boolean relatorioViamEventoMotorista;

    @Column(name = "habilitar_autenticacao")
    private Boolean habilitarAutenticacao;

    @Column(name = "relatorio_pontos_percorridos")
    private Boolean relatorioPontosPercorridos;


    @Column(name = "relatorios_viasat_produtividade_veiculo")
    private Boolean relatorioProdutividadeVeiculo;

    @Column(name = "acessar_desempenho_veiculo")
    private Boolean acessarDesempenhoVeiculo;
    @Column(name = "acessar_detalhes_monitoramento")
    private Boolean acessarDetalhesMonitoramento;
    @Column(name = "acessar_motorista_tela_monitoramento")
    private Boolean acessarMotoristaTelaMonitoramento;
    @Column(name = "agrupar_veiculos_mapa_monitoramento")
    private Boolean agruparVeiculosMapaMonitoramento;

    @Transient
    private MotoristaVO motoristas;


    public Users() {
    }

    public Users(Integer id) {
        this.id = id;
    }
}