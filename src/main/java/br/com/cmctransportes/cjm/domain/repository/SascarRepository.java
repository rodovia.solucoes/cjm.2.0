package br.com.cmctransportes.cjm.domain.repository;

import br.com.cmctransportes.cjm.domain.entities.Sascar;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SascarRepository extends JpaRepository<Sascar, Integer> {
}
