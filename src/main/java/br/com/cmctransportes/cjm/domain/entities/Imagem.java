/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author catatau
 */
@Entity
@Table(name = "imagem")
@Getter
@Setter
public class Imagem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "IMAGEM_ID_SEQ", sequenceName = "IMAGEM_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IMAGEM_ID_SEQ")
    @Column(name = "id")
    private Integer id;

    @Column(name = "dados", columnDefinition = "text", nullable = false)
    private String dados;
}