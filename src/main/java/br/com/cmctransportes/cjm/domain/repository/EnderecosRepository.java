package br.com.cmctransportes.cjm.domain.repository;

import br.com.cmctransportes.cjm.domain.entities.Enderecos;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EnderecosRepository extends JpaRepository<Enderecos, Integer> {
}
