package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TimelineFaturamento {
    private String category;
    private Long start;
    private String inicio;
    private Long end;
    private String fim;
    private String color;
    private String task;
    private String nomeEvento;
    private Integer orderm;
    private String tempoTotal;

    private Double latitudeInicial;
    private Double longitudeInicial;
    private String enderecoInicial;
    private Double latitudeFinal;
    private Double longitudeFinal;
    private String enderecoFinal;

    private String status = "Não informado";
    private String faturamento = "Não informado";

    private String hodometroInicial = "Não informado";
    private String hodometroFinal = "Não informado";


}
//https://www.google.com/search?client=firefox-b-d&q=Buscar+dias+uteis+usando+java