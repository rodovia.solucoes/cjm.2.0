package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.EventoMotorista;
import br.com.cmctransportes.cjm.domain.entities.Motoristas;
import br.com.cmctransportes.cjm.domain.entities.vo.HodometroVO;
import br.com.cmctransportes.cjm.domain.entities.vo.MotoristaVO;
import br.com.cmctransportes.cjm.domain.entities.vo.VeiculoVO;
import br.com.cmctransportes.cjm.infra.dao.EventoMotoristaDAO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.sascarsoap.Motorista;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class EventoMotoristaService {

    private final EntityManager em;
    private final EventoMotoristaDAO eventoMotoristaDAO;

    @Autowired
    private HodometroService hodometroService;
    @Autowired
    private DriverService driverService;

    public EventoMotoristaService(EntityManager em) {
        this.em = em;
        this.eventoMotoristaDAO = new EventoMotoristaDAO(em);
    }

    public List<EventoMotorista> buscarPorPeriodos(Integer motoristaId, Date dataInicial, Date dataFinal) {
        return eventoMotoristaDAO.buscarPorPeriodos(motoristaId, dataInicial, dataFinal);
    }

    public List<EventoMotorista> buscarPorPeriodos(Integer motoristaId, Integer tipo, Date dataInicial, Date dataFinal) {
        return eventoMotoristaDAO.buscarPorPeriodos(motoristaId, tipo, dataInicial, dataFinal);
    }


    public List<EventoMotorista> buscarPorPeriodos(List<MotoristaVO> motoristas, Date dataInicial, Date dataFinal, Integer tipoEvento){
        return  eventoMotoristaDAO.buscarPorPeriodos(motoristas, dataInicial, dataFinal, tipoEvento);
    }

    public List<EventoMotorista> buscarPorVeiculos(List<VeiculoVO> veiculos, Date dataInicial, Date dataFinal, Integer tipoEvento){
        return  eventoMotoristaDAO.buscarPorVeiculos(veiculos, dataInicial, dataFinal, tipoEvento);
    }

    @Transactional
    public void cadastrar(EventoMotorista eventoMotorista){
        try {
            eventoMotoristaDAO.save(eventoMotorista);
            if(Objects.nonNull(eventoMotorista.getPlaca()) && Objects.nonNull(eventoMotorista.getIdVeiculo())){
                if(!eventoMotorista.getPlaca().equalsIgnoreCase("NSA")) {
                    Motoristas motorista = driverService.getById(eventoMotorista.getMotoristas().getId());
                    String nome = motorista.getNome() + " " + motorista.getSobrenome();
                    HodometroVO hodometroVO = new HodometroVO();
                    hodometroVO.setDataDoCadastro(new Date());
                    hodometroVO.setDataUltimaAtualizacao(new Date());
                    hodometroVO.setHodometro(Double.valueOf(eventoMotorista.getHodometroFinal()));
                    hodometroVO.setHodometroRastreador(Double.valueOf(eventoMotorista.getHodometroFinal()));
                    hodometroVO.setVeiculo(new VeiculoVO(eventoMotorista.getIdVeiculo()));
                    hodometroVO.setTipo("V");
                    hodometroVO.setNomeDoMotorista(nome);
                    hodometroVO.setIdDoMotorista(eventoMotorista.getMotoristas().getId());
                    hodometroVO.setTipoCarregamento(eventoMotorista.getTipo());
                    hodometroService.registrarHodometroSemUltimaTransmissao(hodometroVO);
                }
            }
        }catch (Exception e){
            LogSistema.logError(EventoMotoristaService.class.getName(), e);
        }
    }
}
