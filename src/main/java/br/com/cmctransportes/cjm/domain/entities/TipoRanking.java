package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "tipo_ranking")
@Getter
@Setter
public class TipoRanking implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "tipo_ranking_id_seq", sequenceName = "tipo_ranking_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tipo_ranking_id_seq")
    @Column(name = "id")
    private Integer id;

    @Column(name = "data_cadastro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataCadastro;

    @Column(name = "titulo", length = 30, nullable = false)
    private String titulo;

    @Column(name = "descricao", length = 30, nullable = false)
    private String descricao;

    @Column(name = "cliente")
    private Integer cliente;

    @Column(name = "unidade")
    private Integer unidade;

    @Column(name = "tipo")
    private String tipo;

    @Column(name = "maximo_evento_minimo")
    private Integer maximoEventoMinimo;

    @Column(name = "maximo_evento_medio")
    private Integer maximoEventoMedio;

    @Column(name = "maximo_evento_maximo_acima_de")
    private Integer maximoEventomaximoAcimaDe;

    @Column(name = "status")
    private Boolean status;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TipoRanking that = (TipoRanking) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(dataCadastro, that.dataCadastro) &&
                Objects.equals(titulo, that.titulo) &&
                Objects.equals(descricao, that.descricao) &&
                Objects.equals(cliente, that.cliente) &&
                Objects.equals(unidade, that.unidade) &&
                Objects.equals(tipo, that.tipo) &&
                Objects.equals(maximoEventoMinimo, that.maximoEventoMinimo) &&
                Objects.equals(maximoEventoMedio, that.maximoEventoMedio) &&
                Objects.equals(maximoEventomaximoAcimaDe, that.maximoEventomaximoAcimaDe) &&
                Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, dataCadastro, titulo, descricao, cliente, unidade, tipo, maximoEventoMinimo, maximoEventoMedio, maximoEventomaximoAcimaDe, status);
    }

    @Override
    public String toString() {
        return "TipoRanking{" +
                "id=" + id +
                ", dataCadastro=" + dataCadastro +
                ", titulo='" + titulo + '\'' +
                ", descricao='" + descricao + '\'' +
                ", cliente=" + cliente +
                ", unidade=" + unidade +
                ", tipo='" + tipo + '\'' +
                ", maximoEventoMinimo=" + maximoEventoMinimo +
                ", maximoEventoMedio=" + maximoEventoMedio +
                ", maximoEventomaximoAcimaDe=" + maximoEventomaximoAcimaDe +
                ", status=" + status +
                '}';
    }
}
