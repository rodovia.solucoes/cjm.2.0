package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author analista.ti
 */
@Entity
@Table(name = "unidades")
@Getter
@Setter
@NamedQueries({
        @NamedQuery(name = "Unidades.findAll", query = "SELECT u FROM Unidades u"),
        @NamedQuery(name = "Unidades.findById", query = "SELECT u FROM Unidades u WHERE u.id = :id"),
        @NamedQuery(name = "Unidades.findByIdentificacao", query = "SELECT u FROM Unidades u WHERE u.identificacao = :identificacao"),
        @NamedQuery(name = "Unidades.findByApelido", query = "SELECT u FROM Unidades u WHERE u.apelido = :apelido")})
public class Unidades implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name = "UNIDADES_ID_SEQ", sequenceName = "UNIDADES_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UNIDADES_ID_SEQ")
    @Column(name = "id")
    private Integer id;

    @Column(name = "identificacao", length = 50, nullable = false)
    private String identificacao;

    @Column(name = "apelido", length = 15, nullable = false)
    private String apelido;

    @JoinColumn(name = "empresa_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Empresas empresaId;

    @JoinColumn(name = "endereco", referencedColumnName = "id")
    @OneToOne(optional = false)
    private Enderecos endereco;

    @Column(name = "dia_inicio_apuracao")
    private Integer diaInicioApuracao = 0;

    @Column(name = "dia_fim_apuracao")
    private Integer diaFimApuracao = 0;

    @Column(name = "chat_broadcast")
    private String chatBroadcast;

    @Column(name = "valor_diaria")
    private Double valorDiariaNumerico;

    @Column(name = "usar_turno_quatro_dois")
    private Boolean usarTurnoQuatroDois;

    @Column(name = "mostrar_compensacao_grade_jornada")
    private Boolean mostrarCompensacaoGradeJornada;

    @Column(name = "mostrar_abonar_horas_grade_jornada")
    private Boolean mostrarAbonarHorasGradeJornada;

    @Column(name = "mostrar_horas_extras_cinquenta_porcento")
    private Boolean mostrarHorasExtrasCinquentaPorcento;

    @Transient
    private String valorDiaria;

    public Unidades() {
    }

    public Unidades(Integer id) {
        this.id = id;
    }
}