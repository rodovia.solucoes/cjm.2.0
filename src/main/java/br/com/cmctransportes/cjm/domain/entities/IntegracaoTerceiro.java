package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IntegracaoTerceiro {
    private String empresa;
    private Boolean ativo;
    private Double ultimoId;
}
