package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class GraficoTelemetriaVO implements Serializable {
    private static final long serialVersionUID = 1L;

    private String titulo;
    private String descricaoUm;
    private Integer valorUm;
    private String descricaoDois;
    private Integer valorDois;
    private String descricaoTres;
    private Integer valorTres;
}
