/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cmctransportes.cjm.domain.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author analista.ti
 */
@Entity
@Table(name = "eventos_jornada")
@Getter
@Setter
@NamedQueries({
        @NamedQuery(name = "EventosJornada.findAll", query = "SELECT e FROM EventosJornada e"),
        @NamedQuery(name = "EventosJornada.findById", query = "SELECT e FROM EventosJornada e WHERE e.id = :id"),
        @NamedQuery(name = "EventosJornada.findByDescricao", query = "SELECT e FROM EventosJornada e WHERE e.descricao = :descricao")})
public class EventosJornada implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "EVENTOS_JORNADA_ID_SEQ", sequenceName = "EVENTOS_JORNADA_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EVENTOS_JORNADA_ID_SEQ")
    @Column(name = "id")
    private Integer id;

    @Column(name = "descricao", length = 15, nullable = false)
    private String descricao;

    @Column(name = "color", length = 7)
    private String color;

    @JoinColumn(name = "empresa_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @JsonIgnore
    private Empresas empresaId;

    @JoinColumn(name = "gera_estado", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @JsonBackReference("objGeraEstado")
    private EstadosJornada objGeraEstado;

    @Column(name = "icone")
    private String icone;

    public EventosJornada() {
    }

    public EventosJornada(Integer id) {
        this.id = id;
    }

    public Integer getGeraEstado() {
        if (this.objGeraEstado != null) {
            return this.objGeraEstado.getId();
        }

        return null;
    }

    public void setGeraEstado(Integer id) {
        this.objGeraEstado = new EstadosJornada(id);
    }
}