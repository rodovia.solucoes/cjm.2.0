package br.com.cmctransportes.cjm.domain.entities.jornada;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class DetalheDaJornada implements Serializable {
    private String chapa;
    private String evento;
    private String descricao;
    private String hora;
    private String falta;

}
