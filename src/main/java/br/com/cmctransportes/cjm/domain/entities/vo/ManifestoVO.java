package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class ManifestoVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String uuid;
    private Integer numeroDoManifesto;
    private Date data;
    private String placa;
    private String cpfMotorista;
    private Double valor;
}
