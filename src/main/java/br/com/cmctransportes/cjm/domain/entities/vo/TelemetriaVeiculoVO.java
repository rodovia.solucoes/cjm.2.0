package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TelemetriaVeiculoVO {

    private Integer idVeiculo;
    private String placa;
    private String motorista;
    private Double kmPercorrido;
    private String kmPercorridoFormatado;
    private Double consumoTotal;
    private String consumoTotalFormatado;
    private Double consumoMedio;
    private String consumoMedioFormatado;
    private Double velocidadeMedia;
    private String velocidadeMediaFormatado;
    private Double velocidadeMaxima;
    private String velocidadeMaximaFormatado;
    private Integer rpmMedia;
    private Integer rpmMaximo;

    private Double consumoDeCombustivelConducao;
    private String consumoDeCombustivelConducaoFormatado;
    private Double consumoDeCombustivelMarchaLenta;
    private String consumoDeCombustivelMarchaLentaFormatado;
    private Integer tempoDeFuncionamentoMotor;
    private Integer tempoConducao;
    private Integer marchaLenta;
    private Integer numeroFreadaBrusca = 0;
    private Integer numeroAcelaracaoBrusca = 0;
    private Integer numeroDeCurvaBrusca = 0;

    private Integer totalDesligado = 0;


    private String tempoDeFuncionamentoMotorFormatado;
    private String tempoConducaoFormatado;
    private String marchaLentaFormatado;
}
