package br.com.cmctransportes.cjm.domain.entities.jornada;

import br.com.cmctransportes.cjm.domain.entities.*;
import br.com.cmctransportes.cjm.domain.entities.enums.EVENT_TYPE;
import br.com.cmctransportes.cjm.domain.services.EventosJornadaService;
import br.com.cmctransportes.cjm.infra.dao.JustificativaDAO;
import lombok.Getter;
import lombok.Setter;

import java.util.*;

/**
 * @author analista.ti
 */
public class RegistrosDeJornada {
    @Getter
    @Setter
    private Date data;
    @Getter
    @Setter
    private Integer situacao;
    @Getter
    @Setter
    private Instante jornadaInicio;
    @Getter
    @Setter
    private Instante jornadaFim;
    @Getter
    @Setter
    private String tipoJornada;

    @Getter
    @Setter
    private Instante refeicaoInicio;
    @Getter
    @Setter
    private Instante refeicaoFim;

    @Getter
    @Setter
    private ArrayList<Instante> direcaoInicio;
    @Getter
    @Setter
    private ArrayList<Instante> direcaoFim;
    @Getter
    @Setter
    private ArrayList<Instante> descansoInicio;
    @Getter
    @Setter
    private ArrayList<Instante> descansoFim;
    @Getter
    @Setter
    private ArrayList<Instante> carregamentoInicio;
    @Getter
    @Setter
    private ArrayList<Instante> carregamentoFim;
    @Getter
    @Setter
    private ArrayList<Instante> descarregamentoInicio;
    @Getter
    @Setter
    private ArrayList<Instante> descarregamentoFim;
    @Getter
    @Setter
    private ArrayList<Instante> fiscalizacaoInicio;
    @Getter
    @Setter
    private ArrayList<Instante> fiscalizacaoFim;
    @Getter
    @Setter
    private ArrayList<Instante> compensacaoDeHoras;

    public RegistrosDeJornada() {
        this.fiscalizacaoFim = new ArrayList<>(0);
        this.fiscalizacaoInicio = new ArrayList<>(0);
        this.descarregamentoFim = new ArrayList<>(0);
        this.descarregamentoInicio = new ArrayList<>(0);
        this.carregamentoFim = new ArrayList<>(0);
        this.carregamentoInicio = new ArrayList<>(0);
        this.descansoFim = new ArrayList<>(0);
        this.descansoInicio = new ArrayList<>(0);
        this.direcaoFim = new ArrayList<>(0);
        this.direcaoInicio = new ArrayList<>(0);
        this.compensacaoDeHoras = new ArrayList<>(0);
    }

    @Override
    @SuppressWarnings({"CloneDeclaresCloneNotSupported", "CloneDoesntCallSuperClone"})
    public RegistrosDeJornada clone() {
        RegistrosDeJornada result = new RegistrosDeJornada();

        result.setCarregamentoFim(new ArrayList<>(0));
        if (Objects.nonNull(this.getCarregamentoFim())) {
            this.getCarregamentoFim().forEach(c -> result.getCarregamentoFim().add(c));
        }

        result.setCarregamentoInicio(new ArrayList<>(0));
        if (Objects.nonNull(this.getCarregamentoInicio())) {
            this.getCarregamentoInicio().forEach(c -> result.getCarregamentoInicio().add(c));
        }

        result.setData(this.getData());

        result.setDescansoFim(new ArrayList<>(0));
        if (Objects.nonNull(this.getDescansoFim())) {
            this.getDescansoFim().forEach(d -> result.getDescansoFim().add(d));
        }

        result.setDescansoInicio(new ArrayList<>(0));
        if (Objects.nonNull(this.getDescansoInicio())) {
            this.getDescansoInicio().forEach(d -> result.getDescansoInicio().add(d));
        }

        result.setDescarregamentoFim(new ArrayList<>(0));
        if (Objects.nonNull(this.getDescarregamentoFim())) {
            this.getDescarregamentoFim().forEach(d -> result.getDescarregamentoFim().add(d));
        }

        result.setDescarregamentoInicio(new ArrayList<>(0));
        if (Objects.nonNull(this.getDescarregamentoInicio())) {
            this.getDescarregamentoInicio().forEach(d -> result.getDescarregamentoInicio().add(d));
        }

        result.setDirecaoFim(new ArrayList<>(0));
        if (Objects.nonNull(this.getDirecaoFim())) {
            this.getDirecaoFim().forEach(d -> result.getDirecaoFim().add(d));
        }

        result.setDirecaoInicio(new ArrayList<>(0));
        if (Objects.nonNull(this.getDirecaoInicio())) {
            this.getDirecaoInicio().forEach(d -> result.getDirecaoInicio().add(d));
        }

        result.setFiscalizacaoFim(new ArrayList<>(0));
        if (Objects.nonNull(this.getFiscalizacaoFim())) {
            this.getFiscalizacaoFim().forEach(d -> result.getFiscalizacaoFim().add(d));
        }

        result.setFiscalizacaoInicio(new ArrayList<>(0));
        if (Objects.nonNull(this.getFiscalizacaoInicio())) {
            this.getFiscalizacaoInicio().forEach(d -> result.getFiscalizacaoInicio().add(d));
        }

        result.setCompensacaoDeHoras(new ArrayList<>(0));
        if (Objects.nonNull(this.getCompensacaoDeHoras())) {
            this.getCompensacaoDeHoras().forEach(d -> result.getCompensacaoDeHoras().add(d));
        }

        result.setJornadaFim(this.getJornadaFim());
        result.setJornadaInicio(this.getJornadaInicio());
        result.setRefeicaoFim(this.getRefeicaoFim());
        result.setRefeicaoInicio(this.getRefeicaoInicio());
        result.setSituacao(this.getSituacao());
        result.setTipoJornada(this.getTipoJornada());
        return result;
    }

    public void processa(Jornada jornada, EventosJornadaService eventosJornadaService) {
        if ((jornada != null) && (jornada.getEventos() != null)) {
            Evento evt = null;

            List<Justificativa> listaDeJustificativa = new JustificativaDAO(null).buscarPorJornada(jornada.getId());
            if(!listaDeJustificativa.isEmpty()){
                for(Justificativa justificativa: listaDeJustificativa){
                    Instante tmpInstante = new Instante();
                    tmpInstante.setData(justificativa.getDataCadastro());
                    tmpInstante.setOriginal(jornada.getJornadaInicio());
                    tmpInstante.setJustificativa(justificativa.getDados());
                    switch (justificativa.getTipo()){
                        case 1:tmpInstante.setTipoOriginal("Compensação de horas");
                        break;
                        default:tmpInstante.setTipoOriginal("Não Informado");
                        break;
                    }

                    compensacaoDeHoras.add(tmpInstante);
                }
            }

            Optional<Evento> optEvento = jornada.getEventos().stream().findFirst();
            if (optEvento != null) {
                evt = optEvento.get();

                // se o 1o não for do tipo 1, é tipo de jornada diferente
                if (evt.getTipoEvento().getId() != 1) {
                    if (evt.getMotivoAbono() != null) {
                        setTipoJornada(evt.getMotivoAbono().getDescricao());
                    } else {
                        if (evt.getTipoEvento().getId() == EVENT_TYPE.INTERJORNADA.getStartCode()) {
                            setTipoJornada(EVENT_TYPE.DSR.getDescription());
                        } else if (Objects.isNull(evt.getTipoEvento().getObjGeraEstado())) {
                            EventosJornada tipo = eventosJornadaService.getById(evt.getTipoEvento().getId());
                            setTipoJornada(tipo.getObjGeraEstado().getDescricao());
                        } else {
                            setTipoJornada(evt.getTipoEvento().getObjGeraEstado().getDescricao());
                        }
                    }
                }
            }

            while (evt != null) {
                Instante tmpInstante = new Instante();
                tmpInstante.setData(evt.getInstanteEvento());
                tmpInstante.setOriginal(evt.getInstanteOriginal());
                tmpInstante.setJustificativa(evt.getJustificativa());
                tmpInstante.setTipoOriginal(evt.getTipoEvento().getDescricao());

                switch (evt.getTipoEvento().getId()) {  // TODO: desacoplar o código do evento deste procedimento
                    case 0:
                        jornadaFim = tmpInstante;
                        break;
                    case 1:
                        jornadaInicio = tmpInstante;
                        data = evt.getInstanteEvento();
                        break;
                    case 8:
                        direcaoInicio.add(tmpInstante);
                        break;
                    case -8:
                        direcaoFim.add(tmpInstante);
                        break;
                    case 9:
                    case 10:
                        carregamentoInicio.add(tmpInstante);
                        break;
                    case -9:
                    case -10:
                        carregamentoFim.add(tmpInstante);
                        break;
                    case 13:
                        fiscalizacaoInicio.add(tmpInstante);
                        break;
                    case -13:
                        fiscalizacaoFim.add(tmpInstante);
                        break;
                    case 14:
                        refeicaoInicio = tmpInstante;
                        break;
                    case -14:
                        refeicaoFim = tmpInstante;
                        break;
                    case 15:
                        descansoInicio.add(tmpInstante);
                        break;
                    case -15:
                        descansoFim.add(tmpInstante);
                        break;
                    case 11:
                    case 12:
                        descarregamentoInicio.add(tmpInstante);
                        break;
                    case -11:
                    case -12:
                        descarregamentoFim.add(tmpInstante);
                        break;
                    case 17:
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 26:
                        if (data == null) {
                            data = evt.getInstanteEvento();
                        }
                        break;
                }

                evt = evt.getEventoSeguinte();
            }
        }
    }
}