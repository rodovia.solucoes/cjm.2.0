package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
@Getter
@Setter
public class DetalheVeiculoTelemetriaVO implements Serializable {
    private String consumoDeCombustivelLitros;
    private String consumoDeCombustivelConducaoLitros;
    private String consumoDeCombustivelMarchaLentaLitros;
    private String consumoDeCombustrivelKmLitro;
    private String distanciaPercorridaKm;
    private String tempoFuncionamentoMotorHorasMin;
    private String conducaoHoraMin;
    private String excessoDeVelocidadeHoraMin;
    private String marchaLentaHoraMin;
    private String freadasBruscaNumeros;
    private String acelaracaoBruscaNumero;
    private String marchaLentaPorcentagemDoTempoDeFuncionamentoMotor;
}
