package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.Motoristas;
import br.com.cmctransportes.cjm.domain.entities.MotoristasVeiculos;
import br.com.cmctransportes.cjm.infra.dao.MotoristasVeiculosDAO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @author William Leite
 */
@Service
public class MotoristasVeiculosService {
    private MotoristasVeiculosDAO dao;

    private final EntityManager em;

    public MotoristasVeiculosService(EntityManager em) {
        this.em = em;
        dao = new MotoristasVeiculosDAO(em);
    }

    @Transactional
    public void save(MotoristasVeiculos entity) {
        try {
            List<MotoristasVeiculos> motoristasVeiculosList = dao.getByVeiculo(entity.getVeiculoOrion());
            motoristasVeiculosList.forEach(mv->{
                mv.setDataFim(new Date());
                mv.setAtivo(Boolean.FALSE);
                this.update(mv);
            });
            //Verificar se tem algum carro ativo para o motorista
            motoristasVeiculosList = dao.getByDriver(entity.getMotoristasId());
            motoristasVeiculosList.forEach(mv->{
                mv.setDataFim(new Date());
                mv.setAtivo(Boolean.FALSE);
                this.update(mv);
            });
            entity.setAtivo(Boolean.TRUE);
            entity.setDataInicio(new Date());
            dao.save(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<MotoristasVeiculos> findAll() {
        return dao.nqFindAll();
    }

    public MotoristasVeiculos getById(Integer id) {
        return dao.getById(id);
    }

    @Transactional
   public void update(MotoristasVeiculos entity) {
        try {
            dao.update(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void delete(Integer id) {
        MotoristasVeiculos veiculo = getById(id);
        this.delete(veiculo);
    }


    public void editarMotorista(Integer idMotorista){
        try {
            List<MotoristasVeiculos> lista = dao.getByDriver(idMotorista);
            lista.forEach(l->{
                l.setDataFim(new Date());
                l.setAtivo(Boolean.FALSE);
                this.update(l);
            });
        }catch (Exception e){
            LogSistema.logError("editarMotorista:", e);
        }
    }

    @Transactional
    public void delete(MotoristasVeiculos entity) {
        try {
            dao.delete(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String buscarNomeMotoristaUltimoVeiculo(Integer idDoVeiculo){
        try{
           return dao.buscarNomeMotoristaUltimoVeiculo(idDoVeiculo);
        }catch (Exception e){
            LogSistema.logError("buscarNomeMotoristaUltimoVeiculo", e);
        }
        return "";
    }

    public List<Motoristas> buscarMotoristaVeiculo(Integer idDoVeiculo, Date dataInicial, Date dataFinal){
        try{
            List<Motoristas> lista = dao.buscarMotoristaVeiculoPorPeriodo(idDoVeiculo, dataInicial, dataFinal);
            if(Objects.nonNull(lista) && !lista.isEmpty()){
                return lista;
            }else{
                Motoristas motoristas = dao.buscarMotoristaVeiculo(idDoVeiculo);
                if(Objects.nonNull(motoristas)){
                    List<Motoristas> l = new ArrayList<>();
                    l.add(motoristas);
                    return l;
                }
            }
        }catch (Exception e){
            LogSistema.logError("buscarNomeMotoristaUltimoVeiculo", e);
        }
        return null;
    }

    public Integer buscarIdDoVeiculoPeloMotorista(Integer idDoMotorista){
        return dao.buscarIdDoVeiculoPeloMotorista(idDoMotorista);
    }


    public List<MotoristasVeiculos> getByDriver(Integer motoristasId){
        return dao.getByDriver(motoristasId);
    }
}