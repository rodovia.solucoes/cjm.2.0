package br.com.cmctransportes.cjm.domain.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "operador", uniqueConstraints={@UniqueConstraint(columnNames="numero_matricula")}
)
@Getter
@Setter
public class Operador implements Serializable {
    @Id
    @SequenceGenerator(name = "operador_id_seq", sequenceName = "operador_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "operador_id_seq")
    @Column(name = "id")
    private Integer id;

    @Column(name = "nome", length = 50, nullable = false)
    private String nome;

    @Column(name = "telefone", length = 15, nullable = false)
    private String telefone;

    @Column(name = "unidade_id")
    private Integer unidadeId;

    @JoinColumn(name = "empresa_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Empresas empresa;

    @Column(name = "data_criacao")
    @Temporal(TemporalType.DATE)
    private Date dataCriacao;

    @Column(name = "status")
    private Boolean status;

    @Column(name = "numero_matricula", length = 15, nullable = false)
    private String numeroMatricula;

    @Column(name = "macros_id")
    private Integer macroId;

    @Column(name = "ibutton")
    private String ibutton;

    @Column(name = "funcao")
    private String funcao;

    @Transient
    private List<OperadorVeiculo> listaOperadorVeiculo;
}
