package br.com.cmctransportes.cjm.domain.entities;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "foto_viam")
public class FotoViaM {

    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name = "foto_viam_id_seq", sequenceName = "foto_viam_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "foto_viam_id_seq")
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_registro")
    private Date dataRegistro;
    @Column(name = "foto")
    private String foto;
    @Column(name = "descricao")
    private String descricao;
    @Column(name = "latitude")
    private Double latitude;
    @Column(name = "longitude")
    private Double longitude;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_foto")
    @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
    private Date dataDaFoto;
    @JoinColumn(name = "motorista", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Motoristas motoristas;
    @Column(name = "visualizada_no_dashboard")
    private Boolean visualizadaNodashboard;
    @Transient
    private String dataFormatada;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Date getDataDaFoto() {
        return dataDaFoto;
    }

    public void setDataDaFoto(Date dataDaFoto) {
        this.dataDaFoto = dataDaFoto;
    }

    public Date getDataRegistro() {
        return dataRegistro;
    }

    public void setDataRegistro(Date dataRegistro) {
        this.dataRegistro = dataRegistro;
    }

    public Motoristas getMotoristas() {
        return motoristas;
    }

    public void setMotoristas(Motoristas motoristas) {
        this.motoristas = motoristas;
    }

    public Boolean getVisualizadaNodashboard() {
        return visualizadaNodashboard;
    }

    public void setVisualizadaNodashboard(Boolean visualizadaNodashboard) {
        this.visualizadaNodashboard = visualizadaNodashboard;
    }

    public String getDataFormatada() {
        return dataFormatada;
    }

    public void setDataFormatada(String dataFormatada) {
        this.dataFormatada = dataFormatada;
    }
}
