package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class EventoInternoVO implements Serializable {
    private Boolean ignicaoAtiva;
    private Date dataDoEvento;
    private Integer tipo;
    private String nomeLocal;
    private String motorista;
}
