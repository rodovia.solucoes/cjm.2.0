package br.com.cmctransportes.cjm.domain.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author William Leite
 */
@Entity
@Table(name = "users_company")
@Setter
@Getter
public class UsersCompany implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "USERS_BRANCH_ID_SEQ", sequenceName = "USERS_BRANCH_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USERS_BRANCH_ID_SEQ")
    @Column(name = "id")
    private Integer id;

    @JoinColumn(name = "company_id", referencedColumnName = "id")
    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    private Empresas company;

    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    @JsonBackReference("usersCompany")
    private Users user;

    public UsersCompany() {
    }

    public UsersCompany(Integer id, Empresas empresa, Users usuario) {
        this.id = id;
        this.company = empresa;
        this.user = usuario;
    }
}