package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

public class TimelineVO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private String data;
    @Getter
    @Setter
    private String descricao;
    @Getter
    @Setter
    private Date dataDoEvento;
}
