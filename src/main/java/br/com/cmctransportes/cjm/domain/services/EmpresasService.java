package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.Empresas;
import br.com.cmctransportes.cjm.domain.entities.Enderecos;
import br.com.cmctransportes.cjm.domain.entities.Permissao;
import br.com.cmctransportes.cjm.infra.dao.EmpresasDAO;
import br.com.cmctransportes.cjm.domain.entities.Parametro;
import br.com.cmctransportes.cjm.infra.dao.EnderecosDAO;
import br.com.cmctransportes.cjm.infra.dao.ParametroDAO;
import br.com.cmctransportes.cjm.infra.dao.PermissaoDAO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.proxy.ClienteTrajetto;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

/**
 * @author William Leite
 */
@Service
public class EmpresasService {

    private EmpresasDAO dao;
    private PermissaoDAO permissaoDAO;
    private ParametroDAO parametroDAO;
    private EnderecosDAO enderecosDAO;

    @Autowired
    private EnderecosService enderecosService;

    private final EntityManager em;

    public EmpresasService(EntityManager em) {
        this.em = em;
        dao = new EmpresasDAO(em);
        permissaoDAO = new PermissaoDAO(em);
        parametroDAO = new ParametroDAO(em);
        enderecosDAO = new EnderecosDAO(em);
    }


    public void save(Empresas entity) throws Exception {
        Enderecos tmpEndereco = entity.getEndereco();
        Parametro parametro = entity.getParametro();
        try {
            //

            ajustarValorDiaria(entity);
            dao.save(entity);
            //permissaoDAO.ajustarPermissoes(entity);
        } catch (Exception e) {
            e.printStackTrace();
            tmpEndereco.setId(null);
            entity.setId(null);
            throw e;
        }
    }

    public List<Empresas> findAll() throws Exception {
        List<Empresas> lista = dao.buscarTodos();
        for (Empresas empresas : lista) {
            List<Permissao> permissaoList = permissaoDAO.buscarListaPermissao(empresas);
            empresas.setListDePermissao(permissaoList);
            converterDoubleEmString(empresas);
        }
        return lista;
    }

    public Empresas getById(Integer id) {
        Empresas empresas = dao.getById(id);
        if (empresas != null) {
            try {
                List<Permissao> permissaoList = permissaoDAO.buscarListaPermissao(empresas);
                empresas.setListDePermissao(permissaoList);
                converterDoubleEmString(empresas);
            } catch (Exception e) {
                LogSistema.logError("buscarListaDePermissao", e);
            }

        }
        return empresas;
    }

    @Transactional
    public void update(Empresas entity) throws Exception {
        Enderecos tmpEndereco = entity.getEndereco();
        Parametro parametro = entity.getParametro();
        try {
            if (tmpEndereco != null) {
                if (tmpEndereco.getId() != null) {
                    enderecosService.update(tmpEndereco);
                } else {
                    if (tmpEndereco.getEmpresaId() == null) {
                        tmpEndereco.setEmpresaId(entity.getId());
                    }
                    enderecosService.save(tmpEndereco);
                }
            }

            if (parametro != null) {
                ajustarValorDiaria(entity);
                if (parametro.getId() != null) {
                    parametroDAO.update(parametro);
                } else {
                    parametroDAO.save(parametro);
                }
            }

            dao.update(entity);
            permissaoDAO.ajustarPermissoes(entity);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Transactional
    public void delete(Integer id) {
        try {
            Empresas entity = getById(id);
            permissaoDAO.excluir(entity);
            dao.delete(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void delete(Empresas entity) {
        try {
            permissaoDAO.excluir(entity);
            dao.delete(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void inserirEmpresaNativo(Empresas empresas) {
        try {
            ajustarValorDiaria(empresas);
            ClienteTrajetto.getInstance().cadastrarCliente(empresas);
            dao.inserirEmpresaNativo(empresas);
        } catch (Exception e) {
            LogSistema.logError("inserirEmpresaNativo", e);
        }
    }


    private void ajustarValorDiaria(Empresas empresas) {
        try {
            if (empresas != null && empresas.getParametro() != null && empresas.getParametro().getValorDiaria() != null) {
                Double valorDiaria = RodoviaUtils.convertStringToDouble(empresas.getParametro().getValorDiaria());
                empresas.getParametro().setValorDiariaNumerico(valorDiaria);
            }
        } catch (Exception e) {
        }

    }

    private void converterDoubleEmString(Empresas empresas){
        try {
            if (empresas != null && empresas.getParametro() != null && empresas.getParametro().getValorDiariaNumerico() != null) {
                String valorDiaria = RodoviaUtils.convertDoubleToMoneyReal(empresas.getParametro().getValorDiariaNumerico());
                empresas.getParametro().setValorDiaria(valorDiaria);
            }
        }catch (Exception e){

        }
    }

    public Double buscarValorDiaria(Integer idMotorista){
        try{
           return dao.buscarValorDiaria(idMotorista);
        }catch (Exception e){
            LogSistema.logError("buscarValorDiaria", e);
        }
        return 0d;

    }

    public boolean chaveValida(String uuid) throws Exception{
        return dao.chaveValida(uuid);
    }

    public List<Empresas> buscarEmpresasContrato(){
        List<Empresas> lista = new ArrayList<>();
        try {
            List<Empresas> l1 = this.dao.findAll();
            lista.addAll(l1);

            l1 = this.dao.buscarBaseOrion();
            lista.addAll(l1);
            lista.sort((Empresas e1, Empresas e2)->e1.getNome().compareTo(e2.getNome()));
        }catch (Exception e){
            LogSistema.logError("buscarEmpresasContrato", e);
        }

        return lista;
    }



}
