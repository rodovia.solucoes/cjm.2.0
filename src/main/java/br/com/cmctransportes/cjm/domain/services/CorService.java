/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.EstadosJornada;
import br.com.cmctransportes.cjm.domain.entities.EventosJornada;
import br.com.cmctransportes.cjm.domain.entities.TiposEstadoJornada;
import br.com.cmctransportes.cjm.domain.entities.vo.CorVO;
import br.com.cmctransportes.cjm.infra.dao.EstadosJornadaDAO;
import br.com.cmctransportes.cjm.infra.dao.EventosJornadaDAO;
import br.com.cmctransportes.cjm.infra.dao.TiposEstadosJornadaDAO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author William Leite
 */
@Service
public class CorService {

    public enum TIPO {
        ESTADOS_JORNADA("estados_jornada", "Estados da Jornada"),
        EVENTOS_JORNADA("eventos_jornada", "Eventos da Jornada"),
        TIPOS_ESTADOS("tipos_estados", "Tipos de Estados da Jornada");

        private final String name;
        private final String display;

        private TIPO(String name, String display) {
            this.name = name;
            this.display = display;
        }

        public String display() {
            return this.display;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    private final EntityManager em;

    public CorService(EntityManager em) {
        this.em = em;
    }

    public List<CorVO> find(String tipo, String nome) {
        List<CorVO> result = new ArrayList<>();
        if (Objects.equals(nome, "*")) {
            nome = "";
        }

        if (Objects.equals(tipo, "*") || Objects.equals(tipo, TIPO.ESTADOS_JORNADA.toString())) {
            EstadosJornadaDAO dao = new EstadosJornadaDAO(this.em);
            List<EstadosJornada> estados = dao.findByName(nome);
            final TIPO tipoEnum = TIPO.ESTADOS_JORNADA;
            estados.stream().forEach(e -> result.add(new CorVO(e.getId(), tipoEnum.display(), tipoEnum.toString(), e.getDescricao(), e.getColor())));
        }
        if (Objects.equals(tipo, "*") || Objects.equals(tipo, TIPO.EVENTOS_JORNADA.toString())) {
            EventosJornadaDAO dao = new EventosJornadaDAO(this.em);
            List<EventosJornada> eventos = dao.findByName(nome);
            final TIPO tipoEnum = TIPO.EVENTOS_JORNADA;
            eventos.stream().forEach(e -> result.add(new CorVO(e.getId(), tipoEnum.display(), tipoEnum.toString(), e.getDescricao(), e.getColor())));
        }
        if (Objects.equals(tipo, "*") || Objects.equals(tipo, TIPO.TIPOS_ESTADOS.toString())) {
            TiposEstadosJornadaDAO dao = new TiposEstadosJornadaDAO(this.em);
            List<TiposEstadoJornada> tipos = dao.findByName(nome);
            final TIPO tipoEnum = TIPO.TIPOS_ESTADOS;
            tipos.stream().forEach(e -> result.add(new CorVO(e.getId(), tipoEnum.display(), tipoEnum.toString(), e.getDescricao(), e.getColor())));
        }

        return result;
    }

    public String remove(String tipo, Integer id) {
        return this.update(new CorVO(id, "", tipo, "", null));
    }

    @Transactional
    public String update(CorVO cor) {
        String error = "";

        try {
            if (Objects.equals(cor.getTipoID(), TIPO.ESTADOS_JORNADA.toString())) {
                EstadosJornadaDAO dao = new EstadosJornadaDAO(this.em);
                EstadosJornada estado = dao.getById(cor.getId());
                if (estado == null) {
                    error = "Estado da Jornada não foi encontrado!";
                } else {
                    estado.setColor(cor.getCor());
                    dao.update(estado);
                }
            } else if (Objects.equals(cor.getTipoID(), TIPO.EVENTOS_JORNADA.toString())) {
                EventosJornadaDAO dao = new EventosJornadaDAO(this.em);
                EventosJornada evento = dao.getById(cor.getId());
                if (evento == null) {
                    error = "Evento da Jornada não foi encontrado!";
                } else {
                    evento.setColor(cor.getCor());
                    dao.update(evento);
                }
            } else if (Objects.equals(cor.getTipoID(), TIPO.TIPOS_ESTADOS.toString())) {
                TiposEstadosJornadaDAO dao = new TiposEstadosJornadaDAO(this.em);
                TiposEstadoJornada tipo = dao.getById(cor.getId());
                if (tipo == null) {
                    error = "Tipo de Estado da Jornada não foi encontrado!";
                } else {
                    tipo.setColor(cor.getCor());
                    dao.update(tipo);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            error = e.getMessage();
        }

        return error;
    }
}
