package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.Parametro;
import lombok.Getter;
import lombok.Setter;

public class RetornoParametroVO extends RetornoVO {

    @Getter
    @Setter
    private Parametro parametro;
    @Getter
    @Setter
    private TurnoVO turno;
}
