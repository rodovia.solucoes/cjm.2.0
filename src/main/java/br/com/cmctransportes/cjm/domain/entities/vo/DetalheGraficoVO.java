package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class DetalheGraficoVO implements Serializable {

    private static final long serialVersionUID = 1L;
    private String totalDeHoras;
    private String totalHodometro;
    private List<LocalGraficoVO> listaDeLocalGrafico;

}
