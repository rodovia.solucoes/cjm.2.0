package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author William Leite
 */
public class TiposDeVeiculoVO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private Integer id;
    @Getter
    @Setter
    private String descricao;
    @Getter
    @Setter
    private Boolean permiteAcoplamento;
    @Getter
    @Setter
    private Boolean tracaoPropria;

}
