package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author analista.ti
 */
@Entity
@Table(name = "empresas")
@Getter
@Setter
@NamedQueries({
        @NamedQuery(name = "Empresas.findAll", query = "SELECT e FROM Empresas e"),
        @NamedQuery(name = "Empresas.findById", query = "SELECT e FROM Empresas e WHERE e.id = :id"),
        @NamedQuery(name = "Empresas.findByNome", query = "SELECT e FROM Empresas e WHERE e.nome = :nome"),
        @NamedQuery(name = "Empresas.findByDisplayName", query = "SELECT e FROM Empresas e WHERE e.displayName = :displayName")})
public class Empresas implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "empresas_id_seq", sequenceName = "empresas_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "empresas_id_seq")
    @Column(name = "id")
    private Integer id;

    @Column(name = "nome", length = 50, nullable = false)
    private String nome;

    @Column(name = "display_name", length = 15, nullable = false)
    private String displayName;

    @Column(name = "cnpj", length = 14)
    private String cnpj;

    @Column(name = "inscricao_municipal", length = 20)
    private String inscricaoMunicipal;

    @Column(name = "inscricao_estadual", length = 20)
    private String inscricaoEstadual;

    @JoinColumn(name = "endereco", referencedColumnName = "id")
    @OneToOne(optional = true)
    private Enderecos endereco;

    @ManyToOne(optional = true)
    @JoinColumn(name = "empresa_id")
    private Empresas empresaPrincipal;

    @Column(name = "responsavel", length = 45)
    private String responsavel;

    @Column(name = "telefone", length = 15)
    private String telefone;

    @Column(name = "email", length = 100)
    private String email;

    @Column(name = "data_validade")
    private Date dataValidade;

    @Column(name = "qtd_usuarios ")
    private Integer quantidadeUsuarios;

    @Column(name = "licenca", length = 255)
    private String numeroLicenca;

    @Column(name = "bosh_service_url")
    private String boshServiceURL;

    @Column(name = "interjourney_compensation_limit")
    private Long interjourneyCompensationLimit;

    @Column(name = "interjourney_compensate_standby")
    private Boolean interjourneyCompensateStandby;

    @Column(name = "worked_days_dsr")
    private Long workedDaysDSR;

    @Column(name = "missing_days_dsr")
    private Long missingDaysDSR;

    @Column(name = "considers_interjourney")
    private Boolean considersInterjourney;

    @Column(name = "half_interjourney")
    private Boolean halfInterjourney;

    @Column(name = "sascar_integration")
    private Boolean sascarintegration;

    @Column(name = "frequency_reading")
    private Integer frequencyreading;

    @Column(name = "habilitar_viam")
    private Boolean habilitarViam;

    @Column(name = "habilitar_viasat")
    private Boolean habilitarViaSat;

    @Column(name = "id_cliente_trajetto")
    private Integer idClienteTrajetto;

    @Column(name = "uuid")
    private String uuid;

    @Transient
    private List<Permissao> listDePermissao;

    @JoinColumn(name = "parametro", referencedColumnName = "id")
    @OneToOne(optional = true)
    private Parametro parametro;

    @Transient
    private String tipoSistema;

    public Empresas() {
    }

    public Empresas(Integer id) {
        this.id = id;
    }
}