package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.controllers.web.v2.EventoFacadeREST;
import br.com.cmctransportes.cjm.controllers.web.v2.JourneyFacadeREST;
import br.com.cmctransportes.cjm.controllers.web.v2.JourneyGridFacadeREST;
import br.com.cmctransportes.cjm.domain.entities.*;
import br.com.cmctransportes.cjm.domain.entities.enums.*;
import br.com.cmctransportes.cjm.domain.entities.evento.EventoEncadeado;
import br.com.cmctransportes.cjm.domain.entities.vo.*;
import br.com.cmctransportes.cjm.infra.dao.EventoVeiculoDAO;
import br.com.cmctransportes.cjm.infra.dao.InconformidadeDAO;
import br.com.cmctransportes.cjm.infra.dao.StatusRotaDAO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.proxy.DashboardTrajetto;
import br.com.cmctransportes.cjm.services.JourneyCalculusService;
import br.com.cmctransportes.cjm.services.NonconformityCalculusService;
import br.com.cmctransportes.cjm.utils.TimeHelper;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * @author Producao
 */
@Service
public class InconformidadeService {


    private Integer INICIO_JORNADA = 1;
    private Integer INICIO_REFEICAO = 14;

    private final EntityManager em;

    @Autowired
    private DriverService driverService;

    @Autowired
    private UserService userService;

    @Autowired
    private JornadaService jornadaService;

    @Autowired
    private DsrService dsrService;

    @Autowired
    private JourneyCalculusService journeyCalculusService;

    @Autowired
    private NonconformityCalculusService nonconformityService;

    @Autowired
    private AutowireCapableBeanFactory beanFactory;

    @Autowired
    private EmpresasService empresasService;

    @Autowired
    private VeiculosService veiculosService;

    @Autowired
    private LocalService localService;

    @Autowired
    private UnidadesService unidadesService;

    private InconformidadeDAO inconformidadeDAO;

    public InconformidadeService(EntityManager em) {
        this.em = em;
        this.inconformidadeDAO = new InconformidadeDAO(em);
    }

    public OperationDiff requestOperation(FunctionParam target, OPERATIONS operation, Long start, Long end, Integer driverID, Integer userID) {
        OperationDiff diff = new OperationDiff();

        Motoristas driver = driverService.getById(driverID);
        Users user = userService.getById(userID);

        Date startDate = TimeHelper.getDate000000(new Date(start));
        Date endDate = TimeHelper.getDate235959(new Date(end));

        final Calendar c = Calendar.getInstance();
        c.setTime(startDate);

        Date calendarStart = startDate;
        // Reverse while we don't arrive at the week turn day
        while (c.get(Calendar.DAY_OF_WEEK) != (driver.getTurnosId().getWeekTurnDay() + 1)) {
            c.add(Calendar.DATE, -1);
            calendarStart = c.getTime();
        }

        // If the driver was fired, we overwrite the final date with the fired date
        Boolean fired = Objects.nonNull(driver.getDataDemissao());
        if (fired) {
            endDate = driver.getDataDemissao();
        }

        c.setTime(endDate);

        Date calendarEnd = endDate;
        Integer dayBefore = driver.getTurnosId().getWeekTurnDay();
        if (dayBefore == 0 || Objects.isNull(dayBefore)) {
            dayBefore = 7;
        }
        // Advance while we don't arrive at the day before the week turn day        
        while (c.get(Calendar.DAY_OF_WEEK) != dayBefore) {
            c.add(Calendar.DATE, 1);
            calendarEnd = c.getTime();
        }

        diff.setBefore(this.retrieveNonconformity(calendarStart, calendarEnd, fired, driver));

        this.doOperation(target, operation, user);

        diff.setAfter(this.retrieveNonconformity(calendarStart, calendarEnd, fired, driver));

        return diff;
    }


    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private void doOperation(FunctionParam target, OPERATIONS operation, Users user) {
        switch (operation) {
            case SAVE_EVENT: {
                CalendarEventVO vo = this.convertCalendar(target);

                JourneyGridFacadeREST facade = new JourneyGridFacadeREST();
                this.beanFactory.autowireBean(facade);

                RESTResponseVO<Boolean> result = facade.updateTime(vo, user.getId());
                if (!result.getResponse()) {
                    throw new RuntimeException(result.getError());
                }
            }
            break;
            case DELETE_EVENT: {
                CalendarEventVO vo = this.convertCalendar(target);

                JourneyGridFacadeREST facade = new JourneyGridFacadeREST();
                this.beanFactory.autowireBean(facade);

                RESTResponseVO<Boolean> result = facade.updateRemoved(vo);
                if (!result.getResponse()) {
                    throw new RuntimeException(result.getError());
                }
            }
            break;
            case ALLOWANCE_LUNCH_REMAINING: {
                Integer id = target.retrieve("id");

                JourneyFacadeREST facade = new JourneyFacadeREST();
                this.beanFactory.autowireBean(facade);

                RESTResponseVO<Boolean> result = facade.fillJourneyLunch(id, user.getId());
                if (!result.getResponse()) {
                    throw new RuntimeException(result.getError());
                }
            }
            break;
            case CREATE_ALLOWANCE:
            case CREATE_EVENT: {
                EventoEncadeado event = this.convertEventoEncadeado(target);

                EventoFacadeREST facade = new EventoFacadeREST();
                this.beanFactory.autowireBean(facade);

                RESTResponseVO<Boolean> result = facade.createChained(event, user.getId());
                if (!result.getResponse()) {
                    throw new RuntimeException(result.getError());
                }
            }
            break;
            case MANUAL_DSR: {
                NonconformityTreatment treatment = this.convertTreatment(target);
                Boolean result = this.manualDSRCreation(treatment);
                if (!result) {
                    throw new RuntimeException();
                }
            }
            break;
            case ALLOWANCE_REMAINING: {
                Integer id = target.retrieve("id");

                JourneyFacadeREST facade = new JourneyFacadeREST();
                this.beanFactory.autowireBean(facade);

                RESTResponseVO<Boolean> result = facade.fillJourney(id, user.getId());
                if (!result.getResponse()) {
                    throw new RuntimeException(result.getError());
                }
            }
            break;
            case MARK_DSR_HE: {
                Integer id = target.retrieve("id");

                JourneyFacadeREST facade = new JourneyFacadeREST();
                this.beanFactory.autowireBean(facade);

                RESTResponseVO<Boolean> result = facade.markDSRHE(id, user.getId());
                if (!result.getResponse()) {
                    throw new RuntimeException(result.getError());
                }
            }
            break;
            case MARK_AWAITING_AS_WORKED: {
                Integer id = target.retrieve("id");

                JourneyFacadeREST facade = new JourneyFacadeREST();
                this.beanFactory.autowireBean(facade);

                RESTResponseVO<Boolean> result = facade.markWaitingAsWorked(id, user.getId());
                if (!result.getResponse()) {
                    throw new RuntimeException(result.getError());
                }
            }
            break;
        }
    }

    private NonconformityTreatment convertTreatment(FunctionParam target) {
        NonconformityTreatment vo = new NonconformityTreatment();
        vo.setDriverId(target.retrieve("driverId"));
        String endDate = target.retrieve("endDate");
        if (!StringUtils.isAllBlank(endDate)) {
            try {
                vo.setEndDate(this.parseDate(endDate));
            } catch (ParseException ex) {
                ex.printStackTrace();
                throw new RuntimeException(ex);
            }
        }
        vo.setJourneyId(target.retrieve("journeyId"));

        if (Objects.nonNull(target.retrieve("previousType"))) {
            vo.setPreviousType(JOURNEY_TYPE.valueOf(target.retrieve("previousType")));
        } else {
            vo.setPreviousType(JOURNEY_TYPE.FALTA);
        }

        String startDate = target.retrieve("startDate");
        if (!StringUtils.isAllBlank(startDate)) {
            try {
                vo.setStartDate(this.parseDate(startDate));
            } catch (ParseException ex) {
                ex.printStackTrace();
                throw new RuntimeException(ex);
            }
        }
        vo.setType(TREATMENT_TYPE.valueOf(target.retrieve("type")));
        vo.setUserId(target.retrieve("userId"));
        return vo;
    }

    private EventoEncadeado convertEventoEncadeado(FunctionParam target) {
        EventoEncadeado vo = new EventoEncadeado();

        LinkedHashMap<String, Object> lhm = target.retrieve("eventoFinal");
        FunctionParam fp = new FunctionParam();
        lhm.entrySet().stream().forEach((Map.Entry<String, Object> e) -> fp.put(e.getKey(), e.getValue()));

        vo.setEventoFinal(this.convertEventoWeb(fp));

        LinkedHashMap<String, Object> lhm1 = target.retrieve("eventoInicio");
        FunctionParam fp1 = new FunctionParam();
        lhm1.entrySet().forEach((Map.Entry<String, Object> e) -> fp1.put(e.getKey(), e.getValue()));

        vo.setEventoInicio(this.convertEventoWeb(fp1));
        return vo;
    }

    private MotivoAbono convertMotivoAbono(FunctionParam target) {
        MotivoAbono vo = new MotivoAbono();
        vo.setDescricao(target.retrieve("descricao"));
        vo.setId(target.retrieve("id"));
        return vo;
    }

    private Date parseDate(String date) throws ParseException {
        Date result = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(date);

        // The date comes without timezone, 3 hours in the future
        Calendar c = Calendar.getInstance();
        c.setTime(result);
        c.add(Calendar.HOUR_OF_DAY, -3);

        return c.getTime();
    }

    private EventoWebVO convertEventoWeb(FunctionParam target) {
        EventoWebVO vo = new EventoWebVO();
        vo.setEmpresaId(target.retrieve("empresaId"));
        String instanteEvento = target.retrieve("instanteEvento");
        if (!StringUtils.isAllBlank(instanteEvento)) {
            try {
                vo.setInstanteEvento(this.parseDate(instanteEvento));
            } catch (ParseException ex) {
                ex.printStackTrace();
                throw new RuntimeException(ex);
            }
        }
        String instanteLancamento = target.retrieve("instanteLancamento");
        if (!StringUtils.isAllBlank(instanteLancamento)) {
            try {
                vo.setInstanteLancamento(this.parseDate(instanteLancamento));
            } catch (ParseException ex) {
                ex.printStackTrace();
                throw new RuntimeException(ex);
            }
        }
        vo.setJornadaId(target.retrieve("jornadaId"));
        vo.setJustificativa(target.retrieve("justificativa"));

        LinkedHashMap<String, Object> lhm = target.retrieve("motivoAbono");
        FunctionParam fp = new FunctionParam();
        if (Objects.nonNull(lhm)) {
            lhm.entrySet().stream().forEach(e -> fp.put(e.getKey(), e.getValue()));
            vo.setMotivoAbono(this.convertMotivoAbono(fp));
        }

        vo.setOperadorLancamento(target.retrieve("operadorLancamento"));
        vo.setTipoEvento(target.retrieve("tipoEvento"));
        return vo;
    }

    private CalendarEventVO convertCalendar(FunctionParam target) {
        CalendarEventVO vo = new CalendarEventVO();
        vo.setAberturaId(target.retrieve("aberturaId"));
        vo.setColor(target.retrieve("color"));
        vo.setFimEventoId(target.retrieve("fimEventoId"));
        vo.setIcone(target.retrieve("icone"));
        vo.setId(target.retrieve("id"));
        String instanteEvento = target.retrieve("instanteEvento");
        if (!StringUtils.isAllBlank(instanteEvento)) {
            try {
                vo.setInstanteEvento(this.parseDate(instanteEvento));
            } catch (ParseException ex) {
                ex.printStackTrace();
                throw new RuntimeException(ex);
            }
        }

        vo.setJustificativa(target.retrieve("justificativa"));
        vo.setLatitude(target.retrieve("latitude"));
        vo.setLocation(target.retrieve("location"));
        vo.setLocked(target.retrieve("locked"));
        vo.setLongitude(target.retrieve("longitude"));
        vo.setPlaca(target.retrieve("placa"));
        vo.setPosition(target.retrieve("position"));
        vo.setType(target.retrieve("type"));
        vo.setTypeDescription(target.retrieve("typeDescription"));
        return vo;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private List<NonconformityVO> retrieveNonconformity(Date calendarStart, Date calendarEnd, Boolean fired, Motoristas driver) {
        // If the driver was fired, we overwrite the calendar's end with the fired date
        List<Jornada> nonconformityJourneyList = this.jornadaService.jornadasDoMotoristaPorPeriodo(calendarStart, fired ? TimeHelper.getDate235959(driver.getDataDemissao()) : calendarEnd, driver.getId())
                .stream().map(j -> {
                    this.journeyCalculusService.processEvents(j, null,false);
                    return j;
                })
                .filter(j -> Objects.nonNull(j.getJornadaInicio())).sorted(Comparator.nullsLast(
                        (j1, j2) -> j1.getJornadaInicio().compareTo(j2.getJornadaInicio())
                )).collect(Collectors.toList());

        // If the driver was fired, we overwrite the calendar's end with the fired date
        List<NonconformityVO> nonconformities = nonconformityService.process(calendarStart, fired ? TimeHelper.getDate235959(driver.getDataDemissao()) : calendarEnd, nonconformityJourneyList, Arrays.asList(new Integer[]{NONCONFORMITY_CALCULUS_TYPE.DSR.getId(), NONCONFORMITY_CALCULUS_TYPE.INTERJOURNEY.getId()}), true);
        nonconformities.forEach((n) -> {
            n.setDriver(null);
        });
        return nonconformities;
    }

    @SuppressWarnings("AssignmentToMethodParameter")
    public CalendarVO assembleCalendar(Date dataInicial, Date dataFinal, Motoristas driver, Boolean preview, Boolean fired, Date firedDate, Integer user) {
        if (fired) {
            driver.setSituacao(0);
            driver.setDataDemissao(TimeHelper.getDate000000(firedDate));
            this.driverService.update(driver);
        }

        Unidades unidadesEmpresa = unidadesService.getById(driver.getUnidadesId());

        CalendarVO result = new CalendarVO();
        result.setWeeks(new HashMap<>(0));
        result.setMaxRemaining(0L);
        result.setMaxWorked(0L);

        final Calendar c = Calendar.getInstance();

        if (Objects.isNull(driver.getDataAdmissao())) {
            throw new RuntimeException("Motorista sem data de admissão preenchida!");
        }

        // If the initial date is before the driver's admission date, overwrite the initial with the admission
        if (dataInicial.before(driver.getDataAdmissao())) {
            dataInicial = driver.getDataAdmissao();
        }

        c.setTime(dataInicial);

        Date calendarStart = dataInicial;
        // Reverse while we don't arrive at the week turn day
        while (c.get(Calendar.DAY_OF_WEEK) != (driver.getTurnosId().getWeekTurnDay() + 1)) {
            c.add(Calendar.DATE, -1);
            calendarStart = c.getTime();
        }

        // If the driver was fired, we overwrite the final date with the fired date
        if (fired) {
            dataFinal = firedDate;
        }

        c.setTime(dataFinal);

        Date calendarEnd = dataFinal;
        Integer dayBefore = driver.getTurnosId().getWeekTurnDay();
        if (dayBefore == 0 || Objects.isNull(dayBefore)) {
            dayBefore = 7;
        }
        // Advance while we don't arrive at the day before the week turn day        
        while (c.get(Calendar.DAY_OF_WEEK) != dayBefore) {
            c.add(Calendar.DATE, 1);
            calendarEnd = c.getTime();
        }

        final List<Date> dates = TimeHelper.getDatesBetween(calendarStart, calendarEnd);

        // If the driver was fired, we overwrite the calendar's end with the fired date
        List<Jornada> nonconformityJourneyList = this.jornadaService.jornadasDoMotoristaPorPeriodo(calendarStart, fired ? TimeHelper.getDate235959(firedDate) : calendarEnd, driver.getId())
                .stream().map(j -> {
                    this.journeyCalculusService.processEvents(j, null, true);
                    this.jornadaService.fixEventLocation(j);
                    return j;
                })
                .filter(j -> Objects.nonNull(j.getJornadaInicio())).sorted(Comparator.nullsLast(
                        Comparator.comparing(Jornada::getJornadaInicio)
                )).collect(Collectors.toList());

        // If the driver was fired, we overwrite the calendar's end with the fired date
        List<NonconformityVO> nonconformities = nonconformityService.process(calendarStart, fired ? TimeHelper.getDate235959(firedDate) : calendarEnd, nonconformityJourneyList, Arrays.asList(new Integer[]{NONCONFORMITY_CALCULUS_TYPE.DSR.getId(), NONCONFORMITY_CALCULUS_TYPE.INTERJOURNEY.getId()}), !preview);
        nonconformities.forEach((n) -> {
            n.setDriver(null);
        });

        // Due to the time constraint and the inherit inability of the nonconformity calculus service to communicate the automatically created DSR
        // We query the database twice in order to reflect the changes made by the calculation.
        // If the driver was fired, we overwrite the calendar's end with the fired date
        // !!!!! NOT SINCE IT WAS DECIDED NO RECORDS WOULD BE CREATED DURING THE NONCONFORMITY CALCULATION !!!!!!!
        List<Jornada> journeyList = nonconformityJourneyList;

        List<String> headers = new ArrayList<>(7);
        final AtomicInteger w = new AtomicInteger(0);
        final AtomicReference<CalendarJourneyVO> previousJourney = new AtomicReference<>(null);

        dates.stream().forEach(d -> {
            c.setTime(d);

            String weekday = c.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.forLanguageTag("pt-br"));
            if (!headers.contains(weekday)) {
                headers.add(weekday);
            }

            CalendarDayVO day = new CalendarDayVO();
            day.setDay(c.get(Calendar.DAY_OF_MONTH));
            day.setWeekTurnDay((driver.getTurnosId().getWeekTurnDay() + 1) == c.get(Calendar.DAY_OF_WEEK));
            day.setWeekDay(c.get(Calendar.DAY_OF_WEEK));

            // On the week turn day we increment the week count.
            // Since the calendar always starts at the week turn day, we start w at 0.
            if (day.getWeekTurnDay()) {
                w.incrementAndGet();
            }

            Collections.sort(journeyList, new Comparator<Jornada>() {
                public int compare(Jornada o1, Jornada o2) {
                    return o1.getJornadaInicio().compareTo(o2.getJornadaInicio());
                }
            });

            Optional<Jornada> currentJourney = journeyList.stream().filter(j -> {
                Date end = Objects.nonNull(j.getJornadaFim()) ? j.getJornadaFim() : j.getJornadaInicio();
                List<Date> journeyDates = TimeHelper.getDatesBetween(j.getJornadaInicio(), end);
                return journeyDates.stream().anyMatch(dt -> TimeHelper.isSameDay(dt, d));
            }).sorted((j1, j2) -> {
                if (j1.getType() == JOURNEY_TYPE.JORNADA) {
                    return -1;
                } else if (j2.getType() == JOURNEY_TYPE.JORNADA) {
                    return 1;
                } else {
                    return j2.getJornadaInicio().compareTo(j1.getJornadaInicio());
                }
            }).findFirst();

            if (currentJourney.isPresent()) {
                CalendarJourneyVO calendarJourney = this.convertJourney(currentJourney.get(), d);

                if (Objects.nonNull(previousJourney.get())) {
                    calendarJourney.setPreviousType(previousJourney.get().getType());
                } else {
                    calendarJourney.setPreviousType(JOURNEY_TYPE.FALTA);
                }

                previousJourney.set(calendarJourney);

                day.setJourney(calendarJourney);

                if (calendarJourney.getType() == JOURNEY_TYPE.JORNADA) {
                    if (result.getMaxRemaining() < calendarJourney.getRestante()) {
                        result.setMaxRemaining(calendarJourney.getRestante());
                    }
                    if (result.getMaxWorked() < calendarJourney.getTrabalhado()) {
                        result.setMaxWorked(calendarJourney.getTrabalhado());
                    }
                }
            } else if (!fired) {
                boolean temJornadaMesmoDia = false;
                Jornada jornadaMesmoDia = null;

                for(Jornada j :journeyList){
                    if(TimeHelper.isSameDay(d, j.getJornadaInicio())){
                        temJornadaMesmoDia = true;
                        jornadaMesmoDia = j;
                    }
                }
                CalendarJourneyVO journey = new CalendarJourneyVO();
                if(temJornadaMesmoDia){
                    journey = convertJourney(jornadaMesmoDia, d);
                }else {
                    journey.setType(JOURNEY_TYPE.FALTA);
                    journey.setEvents(new ArrayList<>(0));
                    journey.setJornadaInicio(TimeHelper.getDate000000(d));
                    journey.setJornadaFim(TimeHelper.getDate235959(d));
                    journey.setRestante(TimeHelper.hours(23) + TimeHelper.minutes(59) + TimeHelper.seconds(59));
                    journey.setTrabalhado(0L);
                    journey.setLocked(false);

                    CalendarEventVO event = new CalendarEventVO();
                    event.setInstanteEvento(TimeHelper.getDate000000(d));
                    event.setType(EVENT_TYPE.FALTA.getStartCode());
                    event.setTypeDescription(EVENT_TYPE.FALTA.getDescription());
                    event.setLocked(false);
                    journey.getEvents().add(event);

                    event = new CalendarEventVO();
                    event.setInstanteEvento(TimeHelper.getDate235959(d));
                    event.setType(EVENT_TYPE.FALTA.getEndCode());
                    event.setTypeDescription(EVENT_TYPE.FALTA.getDescription());
                    event.setLocked(false);
                    journey.getEvents().add(event);
                }

                previousJourney.set(journey);
                day.setJourney(journey);
            }

            if (!result.getWeeks().containsKey(w.get())) {
                CalendarWeekVO week = new CalendarWeekVO();
                week.setSequence(w.get());
                week.setStatus(CALENDAR_WEEK_STATUS.OK);
                week.setDays(new ArrayList<>(0));
                week.setStart(TimeHelper.getDate000000(d));
                week.setMaxEvents(day.getJourney().getEvents().size());
                if (Objects.nonNull(day.getJourney().getTypeSummary())) {
                    week.setMaxTypes(day.getJourney().getTypeSummary().keySet().size());
                } else {
                    week.setMaxTypes(0);
                }
                result.getWeeks().put(w.get(), week);
            } else {
                Integer journeyEvents = Objects.nonNull(day.getJourney()) ? day.getJourney().getEvents().size() : 0;
                Integer summarySize = Objects.nonNull(day.getJourney()) ? Objects.nonNull(day.getJourney().getTypeSummary()) ? day.getJourney().getTypeSummary().keySet().size() : 0 : 0;

                if (result.getWeeks().get(w.get()).getMaxEvents() < journeyEvents) {
                    result.getWeeks().get(w.get()).setMaxEvents(journeyEvents);
                }

                if (result.getWeeks().get(w.get()).getMaxTypes() < summarySize) {
                    result.getWeeks().get(w.get()).setMaxTypes(summarySize);
                }

                result.getWeeks().get(w.get()).setEnd(TimeHelper.getDate235959(d));
            }

            result.getWeeks().get(w.get()).getDays().add(day);
        });

        result.setWeekHeader(headers);

        result.getWeeks().entrySet().forEach((entry) -> {
            if (Objects.nonNull(entry.getValue().getStart()) && Objects.nonNull(entry.getValue().getEnd())) {
                List<NonconformityVO> weekNonconformities = nonconformities.stream()
                        .filter((n) -> n.getStartDate().compareTo(entry.getValue().getStart()) >= 0 && n.getEndDate().compareTo(entry.getValue().getEnd()) <= 0)
                        .collect(Collectors.toList());

                if (!weekNonconformities.isEmpty()) {
                    entry.getValue().setStatus(CALENDAR_WEEK_STATUS.ERROR);
                    entry.getValue().setNonconformities(weekNonconformities);
                    weekNonconformities.stream().forEach(nonconformities::remove);
                }
            }

            // Fill weeks so that they have the same number of items
            entry.getValue().getDays().stream().forEach((day) -> {
                if (Objects.nonNull(day.getJourney())) {
                    while (day.getJourney().getEvents().size() < entry.getValue().getMaxEvents()) {
                        CalendarEventVO vo = new CalendarEventVO();
                        vo.setType(null);
                        vo.setLocked(false);
                        day.getJourney().getEvents().add(vo);
                    }

                    if(Objects.nonNull(unidadesEmpresa)){
                        if(Objects.nonNull(unidadesEmpresa.getMostrarAbonarHorasGradeJornada())){
                            day.getJourney().setMostrarAbonarHorasGradeJornada(unidadesEmpresa.getMostrarAbonarHorasGradeJornada());
                        }else{
                            day.getJourney().setMostrarAbonarHorasGradeJornada(Boolean.FALSE);
                        }
                        if(Objects.nonNull(unidadesEmpresa.getMostrarCompensacaoGradeJornada())){
                            day.getJourney().setMostrarCompensacaoGradeJornada(unidadesEmpresa.getMostrarCompensacaoGradeJornada());
                        }else{
                            day.getJourney().setMostrarCompensacaoGradeJornada(Boolean.FALSE);
                        }
                        if(Objects.nonNull(unidadesEmpresa.getMostrarHorasExtrasCinquentaPorcento())){
                            day.getJourney().setMostrarHorasExtrasCinquentaPorcento(unidadesEmpresa.getMostrarHorasExtrasCinquentaPorcento());
                        }else{
                            day.getJourney().setMostrarHorasExtrasCinquentaPorcento(Boolean.FALSE);
                        }
                    }


                    Integer i = 0;

                    if (Objects.isNull(day.getJourney().getTypeSummary())) {
                        day.getJourney().setTypeSummary(new HashMap<>(0));
                    }

                    while (day.getJourney().getTypeSummary().keySet().size() < entry.getValue().getMaxTypes()) {
                        day.getJourney().getTypeSummary().put("empty" + i++, null);
                    }
                }
            });
        });

        result.setOrphanNonconformities(nonconformities);
        return result;
    }

    public CalendarJourneyVO convertJourney(Jornada journey, Date day) {
        CalendarJourneyVO vo = new CalendarJourneyVO();
        vo.setId(journey.getId());
        vo.setDsrHE(journey.getDsrHE());
        vo.setAbonarHE(journey.getHorasExtrasAbonada());
        vo.setHorasExtrasCinquentaPorcento(journey.getHorasExtrasCinquentaPorcento());
        vo.setJornadaFim(journey.getJornadaFim());
        vo.setJornadaInicio(journey.getJornadaInicio());
        vo.setRestante(journey.getHorasFaltosas());
        vo.setRestanteAlmoco(journey.getDescontoAlmoco());
        vo.setTrabalhado(journey.getTrabalhado());
        vo.setType(journey.getType());
        vo.setWaitingAsWorked(journey.getWaitingAsWorked());
        vo.setLocked(Objects.equals(journey.getLocked(), Boolean.TRUE));

        vo.setEvents(new ArrayList<>(0));

        journey.getEventos().stream().forEach((event) -> {
            CalendarEventVO eventVO = new CalendarEventVO();
            eventVO.setId(event.getId());
            eventVO.setInstanteEvento(event.getInstanteEvento());

            eventVO.setType(event.getTipoEvento().getId());
            eventVO.setTypeDescription(event.getTipoEvento().getDescricao());
            eventVO.setColor(event.getTipoEvento().getColor());
            eventVO.setIcone(event.getTipoEvento().getIcone());

            if (Objects.nonNull(event.getVeiculoMotor())) {
                eventVO.setPlaca(event.getVeiculoMotor().getPlaca());
            }

            eventVO.setAberturaId(event.getAberturaId());
            eventVO.setFimEventoId(event.getFimEventoId());
            eventVO.setLatitude(event.getLatitude());
            eventVO.setLongitude(event.getLongitude());
            eventVO.setLocked(Objects.equals(event.getLocked(), Boolean.TRUE));

            if (Objects.nonNull(event.getIdLocalEventoEspera())) {
                try {
                    RetornoLocalVO retornoLocalVO = new LocalService().buscarLocalPeloCodigo(event.getIdLocalEventoEspera());
                    if (Objects.nonNull(retornoLocalVO) && Objects.nonNull(retornoLocalVO.getLocal())) {
                        eventVO.setLocation(retornoLocalVO.getLocal().getNome());
                    }
                } catch (Exception r) {
                    LogSistema.logError("eventPlaceAllowsRest", r);
                }

            }

            if (TimeHelper.isSameDay(event.getInstanteEvento(), day)) {
                eventVO.setPosition(0);
            } else if (event.getInstanteEvento().before(day)) {
                eventVO.setPosition(-1);
            } else {
                eventVO.setPosition(1);
            }

            vo.getEvents().add(eventVO);
        });

        vo.setTypeSummary(new HashMap<>(0));

        journey.getResumoTiposEstados().entrySet().stream().forEach((entry) -> {
            CalendarJourneySummaryVO summary = new CalendarJourneySummaryVO();
            summary.setIcon(entry.getValue().getIcon());
            summary.setDayTime(entry.getValue().getSomatorioDiurno());
            summary.setNightTime(entry.getValue().getSomatorioNoturno());
            summary.setTotal(summary.getDayTime() + summary.getNightTime());
            vo.getTypeSummary().put(entry.getKey(), summary);
        });

        return vo;
    }

    public List<NonconformityVO> calculate(Integer motorista, Date dataInicial, Date dataFinal, List<Integer> tipos, Boolean preview) {

        List<Jornada> jornadas = this.jornadaService.jornadasDoMotoristaPorPeriodo(dataInicial, dataFinal, motorista);

        jornadas.forEach(j -> this.journeyCalculusService.processEvents(j, null,false));

        jornadas = jornadas.stream()
                .sorted(Comparator.nullsLast(
                        (j1, j2) -> j1.getJornadaInicio().compareTo(j2.getJornadaInicio())
                )).collect(Collectors.toList());

        return this.nonconformityService.process(dataInicial, dataFinal, jornadas, tipos, preview);
    }

    public String uploadTreatments(List<NonconformityTreatment> treatments, Integer user) {
        StringBuilder result = new StringBuilder();

        treatments.stream().forEach(treatment -> {
            switch (treatment.getType()) {
                case MANUAL_DSR_CREATION:
                    result.append(this.manualDSRCreation(treatment)).append("\n");
                    break;
                case MARK_DSR_HE:
                    result.append(this.markDSRHE(treatment)).append("\n");
                    break;
                default:
                    break;
            }
        });

        return result.toString();
    }

    private String markDSRHE(NonconformityTreatment treatment) {
        String result = "";

        Jornada journey = this.jornadaService.getById(treatment.getJourneyId());
        if (Objects.nonNull(journey)) {
            journey.setDsrHE(true);
            this.jornadaService.save(journey);
        }

        return result;
    }

    private Boolean manualDSRCreation(NonconformityTreatment treatment) {
        Boolean result = Boolean.FALSE;

        try {
            Motoristas driver = driverService.getById(treatment.getDriverId());

            Dsr dsr = new Dsr();
            dsr.setEmpresaId(driver.getEmpresa());
            dsr.setFimDSR(treatment.getEndDate());
            dsr.setInicioDSR(treatment.getStartDate());

            dsr.setMotorista(driver);
            dsr.setOperadorLancamento(new Users(treatment.getUserId()));

            boolean interjourney = true;

            if (treatment.getPreviousType() == JOURNEY_TYPE.FOLGA || treatment.getPreviousType() == JOURNEY_TYPE.FERIAS || treatment.getPreviousType() == JOURNEY_TYPE.FERIADO) {
                interjourney = false;
            }

            if (interjourney) {
                long dsrOffset = treatment.getStartDate().getTime() - TimeHelper.seconds(1);
                dsr.setInicioInterjornada(new Date(dsrOffset - TimeHelper.hours(11)));
                dsr.setFimInterjornada(new Date(dsrOffset));
            }

            dsrService.save(dsr);
            result = Boolean.TRUE;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    private Integer idTipoEvento = 0;

    public List<Inconformidade> buscarInconformidadesDoDia(Integer idEmpresa, Integer unidade) {
        try {
            List<Inconformidade> ll = this.inconformidadeDAO.buscarListaIncoformidadeDia(idEmpresa, unidade);
            return ll;
        } catch (Exception e) {
            LogSistema.logError("buscarInconformidadesDoDia", e);
        }
        return null;
    }

    public List<Inconformidade> buscarAlertasDoViaSatDia(Integer idEmpresa, Integer unidade) {
        List<Inconformidade> inconformidades = new ArrayList<>();
        try {
            RetornoVeiculoVO ret = veiculosService.listarVeiculo(idEmpresa, unidade);
            List<VeiculoVO> lista = ret.getListaDeVeiculos();
            for(VeiculoVO vo : lista){
               List<EventoVeiculoVO> l =  new EventoVeiculoDAO().buscarUltimosEventosMongo(vo.getId());
               //Verificar se esta a mais de 20 minutos
               if(!l.isEmpty()){
                   EventoVeiculoVO eventoVeiculoVO = l.get(0);
                   if(eventoVeiculoVO.getTipo() == 4){
                      for(EventoVeiculoVO vo1 : l){
                        if(vo1.getTipo() == 4){
                            int minutos = Minutes.minutesBetween(new DateTime(vo1.getDataEvento()), new DateTime(new Date())).getMinutes();
                            if(minutos > 20){
                                SimpleDateFormat format = new SimpleDateFormat();
                                Inconformidade inconformidade = new Inconformidade();
                                inconformidade.setId(1);
                                inconformidade.setDataFormatada(format.format(vo1.getDataEvento()));
                                inconformidade.setDataInconformidade(vo1.getDataEvento());
                                inconformidade.setDataInconformidadeFormatada(format.format(vo1.getDataEvento()));
                                inconformidade.setIdVeiculo(vo.getId());
                                inconformidade.setPlaca(vo.getPlaca());
                                RetornoLocalVO vo2 = localService.buscarLocalPeloCodigo(vo1.getLocalId());
                                String s = "Permanencia acima de 20 minutos em "+ vo2.getLocal().getNome();
                                inconformidade.setNome(s);
                                inconformidades.add(inconformidade);
                                break;
                            }
                        }
                      }
                   }
               }
            }
        } catch (Exception e) {
            LogSistema.logError("buscarInconformidadesDoDia", e);
        }
        return inconformidades;
    }

    public void buscarAlertasForaDaCerca(Integer idEmpresa, Integer unidade, List<Inconformidade> listaDeIncormidade) {
        List<Inconformidade> inconformidades = new ArrayList<>();
        try {
            RetornoVeiculoVO ret = veiculosService.listarVeiculo(idEmpresa, unidade);
            List<VeiculoVO> lista = ret.getListaDeVeiculos();
            for(VeiculoVO vo : lista){
                StatusRotaDAO statusRotaDAO = new StatusRotaDAO();
                StatusRotaVO statusRotaVO = statusRotaDAO.buscarUltimoStatus(vo.getId());
                if(Objects.nonNull(statusRotaVO)) {
                    if (!statusRotaVO.getDentroRota()) {
                        SimpleDateFormat format = new SimpleDateFormat();
                        Inconformidade inconformidade = new Inconformidade();
                        inconformidade.setId(1);
                        inconformidade.setDataFormatada(format.format(statusRotaVO.getDataCadastro()));
                        inconformidade.setDataInconformidade(statusRotaVO.getDataCadastro());
                        inconformidade.setDataInconformidadeFormatada(format.format(statusRotaVO.getDataCadastro()));
                        inconformidade.setIdVeiculo(vo.getId());
                        inconformidade.setPlaca(vo.getPlaca());
                        String nome = statusRotaDAO.buscarNomeRota(statusRotaVO.getRota());
                        String s = "Veiculo saiu da rota "+ nome;
                        inconformidade.setNome(s);
                        inconformidades.add(inconformidade);
                        listaDeIncormidade.add(inconformidade);
                    }
                }
            }
        } catch (Exception e) {
            LogSistema.logError("buscarAlertasForaDaCerca", e);
        }
    }


    public List<Inconformidade> buscarTodasInconformidades(Integer idEmpresa, Integer unidade) {
        try {
            return this.inconformidadeDAO.buscarTodasInconformidade(idEmpresa, unidade);
        } catch (Exception e) {
            LogSistema.logError("buscarTodasInconformidades", e);
        }
        return null;
    }

    @Transactional
    public void marcarComoLido(Integer idInconformidade, Integer idUsuario) throws Exception {
        try {
            Inconformidade inconformidade = this.buscarPeloId(idInconformidade);
            inconformidade.setLido(Boolean.TRUE);
            inconformidade.setDataLeitura(new Date());
            inconformidade.setUsuario(new Users(idUsuario));
            this.inconformidadeDAO.update(inconformidade);
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional
    public void editar(Inconformidade inconformidade) throws Exception {
        try {
            inconformidade.setDataLeitura(new Date());
            this.inconformidadeDAO.update(inconformidade);
        } catch (Exception e) {
            throw e;
        }
    }

    public Inconformidade buscarPeloId(Integer idInconformidade) throws Exception {
        try {
            Inconformidade inconformidade = this.inconformidadeDAO.buscarPeloId(idInconformidade);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            inconformidade.setDataInconformidadeFormatada(simpleDateFormat.format(inconformidade.getDataInconformidade()));
            if (Objects.nonNull(inconformidade.getDataLeitura())) {
                inconformidade.setDataLeituraFormatada(simpleDateFormat.format(inconformidade.getDataLeitura()));
            } else {
                inconformidade.setDataLeituraFormatada(" - ");
            }

            return inconformidade;
        } catch (Exception e) {
            throw e;
        }
    }

    private boolean isNotNullOrNotEmpty(String valor) {
        if (valor == null || (valor != null && !valor.isEmpty())) {
            return true;
        }
        return false;
    }

    private int buscarMinutosDoTurno(Motoristas motoristas, Date data) {
        int horas = 0;
        Long temp = 0l;
        int diaDaSemana = TimeHelper.getDayOfWeek(data);
        Turnos turnos = motoristas.getTurnosId();
        switch (diaDaSemana) {
            case 1:
                temp = turnos.getTempoDiario01();
                break;
            case 2:
                temp = turnos.getTempoDiario02();
                break;
            case 3:
                temp = turnos.getTempoDiario03();
                break;
            case 4:
                temp = turnos.getTempoDiario04();
                break;
            case 5:
                temp = turnos.getTempoDiario05();
                break;
            case 6:
                temp = turnos.getTempoDiario06();
                break;
            case 7:
                temp = turnos.getTempoDiario07();
                break;
        }
        if (temp == null) {
            return 480; //equivale a 8 horas por dia de trabalho.
        }
        horas = (temp.intValue() / 1000) / 60;
        return horas;
    }

    private int buscarMinutosDaRefeicao(Motoristas motoristas, Date data) {
        int horas = 0;
        Long temp = 0l;
        int diaDaSemana = TimeHelper.getDayOfWeek(data);
        Turnos turnos = motoristas.getTurnosId();
        LogSistema.logInfo(motoristas.getId().toString());
        switch (diaDaSemana) {
            case 1:
                temp = turnos.getTempoAlimentacao01();
                break;
            case 2:
                temp = turnos.getTempoAlimentacao02();
                break;
            case 3:
                temp = turnos.getTempoAlimentacao03();
                break;
            case 4:
                temp = turnos.getTempoAlimentacao04();
                break;
            case 5:
                temp = turnos.getTempoAlimentacao05();
                break;
            case 6:
                temp = turnos.getTempoAlimentacao06();
                break;
            case 7:
                temp = turnos.getTempoAlimentacao07();
                break;
        }
        if (temp == null) {
            return 60; //equivale a 8 horas por dia de trabalho.
        }
        horas = (temp.intValue() / 1000) / 60;
        return horas;
    }

    //Buscar informidades dos veiculos no orion
    public List<AlertaInconformeVO> buscarInconformidadesVeiculos(EmpresaVO empresaVO) {
        Empresas empresas = empresasService.getById(empresaVO.getId());
        RetornoAlertaInconformeVO retorno = DashboardTrajetto.getInstance().buscarInconformidadesVeiculos(empresas.getIdClienteTrajetto());
        if (retorno.getCodigoRetorno() == 292) {
            return retorno.getListaDeAlertaInconforme();
        }
        return null;
    }

    private Evento buscarEventoEspecifico(List<Evento> listaDeEventos, Integer tipoEvento, boolean buscarFim) {
        try {
            List<Evento> lista = buscarListaEventosPeloTipo(listaDeEventos, tipoEvento);
            if (lista.size() % 2 != 0) {
                return lista.get(lista.size() - 1);
            } else {
                if (buscarFim) {
                    return lista.get(lista.size() - 1);
                }
            }
        } catch (Exception e) {
            LogSistema.logError("buscarEventoEspecifico", e);
        }
        return null;
    }

    private List<Evento> buscarListaEventosPeloTipo(List<Evento> listaDeEventos, Integer tipoEvento) {
        try {
            List<Evento> lista = listaDeEventos.stream().filter(ev -> ev.getTipoEvento().getId().equals(tipoEvento) || ev.getTipoEvento().getId().equals((tipoEvento * -1))).sorted(Comparator.comparing(Evento::getId)).collect(Collectors.toList());
            return lista;
        } catch (Exception e) {
            LogSistema.logError("buscarListaEventosPeloTipo", e);
        }
        return null;
    }

    private Evento buscarEventosPeloTipo(List<Evento> listaDeEventos, Integer tipoEvento) {
        try {

            Optional<Evento> find = listaDeEventos.stream().filter(ev -> ev.getTipoEvento().getId().equals(tipoEvento)).sorted(Comparator.comparing(Evento::getId)).findFirst();
            if (find.isPresent()) {
                Evento evento = find.get();
                return evento;
            }
        } catch (Exception e) {
            LogSistema.logError("buscarEventoEspecifico", e);
        }
        return null;
    }

    private int buscarTempoEsperaComEventosFechados(List<Evento> listaDeEventos) {
        int minutos = 0;
        try {
            List<Evento> lista = listaDeEventos.stream().filter(ev -> ev.getTipoEvento().getObjGeraEstado().getTiposEstadoJornadaId().getDescricao().equalsIgnoreCase("Espera") || ev.getTipoEvento().getId() == INICIO_REFEICAO).sorted(Comparator.comparing(Evento::getId)).collect(Collectors.toList());
            for (Evento evento : lista) {
                if (evento.getTipoEvento().getId() == INICIO_JORNADA) {
                    continue;
                } else {
                    Evento eventoFechamento = buscarEventosPeloTipo(listaDeEventos, (evento.getTipoEvento().getId() * -1));
                    if (eventoFechamento == null) {
                        return 0;//Caso nao encontre o fechamento retorna 0
                    } else {
                        Minutes minutes = Minutes.minutesBetween(new DateTime(evento.getInstanteEvento()), new DateTime(eventoFechamento.getInstanteEvento()));
                        int tempo = minutes.getMinutes();
                        minutos = minutos + tempo;
                    }
                }
            }
        } catch (Exception e) {
            LogSistema.logError("buscarTempoEsperaComEventosFechados", e);
        }
        return minutos;
    }


}
