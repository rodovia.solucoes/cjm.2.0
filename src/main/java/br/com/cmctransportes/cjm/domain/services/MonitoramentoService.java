package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.LatLng;
import br.com.cmctransportes.cjm.domain.entities.Telemetria;
import br.com.cmctransportes.cjm.domain.entities.Transmissao;
import br.com.cmctransportes.cjm.domain.entities.Veiculos;
import br.com.cmctransportes.cjm.domain.entities.vo.*;
import br.com.cmctransportes.cjm.infra.dao.EventoVeiculoDAO;
import br.com.cmctransportes.cjm.infra.dao.StatusRotaDAO;
import br.com.cmctransportes.cjm.infra.dao.TelemetriaDAO;
import br.com.cmctransportes.cjm.infra.dao.VeiculosDAO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.proxy.VeiculoMonitoramentoProxy;
import br.com.cmctransportes.cjm.utils.CalculadorDistancia;
import br.com.cmctransportes.cjm.utils.TimeHelper;
import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class MonitoramentoService {

    @Autowired
    private VeiculosService veiculosService;
    @Autowired
    private TransmissaoService transmissaoService;
    @Autowired
    private MotoristasVeiculosService motoristasVeiculosService;
    @Autowired
    private RotaService rotaService;

    public List<VeiculoMonitoramentoVO> buscarListaDeVeiculos(Integer idCliente, Integer idUnidade) {
        List<VeiculoMonitoramentoVO> lista = new ArrayList<>();
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM HH:mm");
            RetornoVeiculoVO retornoVeiculoVO = veiculosService.listarVeiculo(idCliente, idUnidade);
            TelemetriaDAO telemetriaDAO = new TelemetriaDAO();
            StatusRotaDAO statusRotaDAO = new StatusRotaDAO();
            for (VeiculoVO veiculoVO : retornoVeiculoVO.getListaDeVeiculos()) {
                if (Objects.isNull(veiculoVO.getEquipamento())) {
                    continue;
                }
                VeiculoMonitoramentoVO vo = VeiculoMonitoramentoProxy.getInstance().buscarVeiculo(veiculoVO.getPlaca());
                if(Objects.nonNull(vo)){
                    lista.add(vo);
                }
                Date dateFinal = new Date();
            }
        } catch (Exception e) {
            LogSistema.logError("buscarListaDeVeiculos", e);
        }
        return lista;
    }


    public List<Transmissao> buscarUltimasTrasnmissoes(Integer idVeiculo) {
        try {
            List<Transmissao> listaT = transmissaoService.buscarUltimasTransmissaosIgnicaoAtivaVelociadeMaiorQueZero(idVeiculo);
            return listaT;
        } catch (Exception e) {
            LogSistema.logError("buscarUltimasTrasnmissoes", e);
        }
        return null;
    }

    public DadoTelemetriaVO buscarTelemetriaDosVeiculos(List<VeiculoVO> ids, Date dataInicial, Date dataFinal) {
        List<TelemetriaVeiculoVO> lista = new ArrayList<>();
        List<Telemetria> listaFull = new ArrayList<>();
        DadoTelemetriaVO dadoTelemetria = new DadoTelemetriaVO();
        dataInicial = TimeHelper.getDate000000(dataInicial);
        dataFinal = TimeHelper.getDate235959(dataFinal);
        Double totalCombustivel = 0D;
        try {
            for (VeiculoVO idVeiculo : ids) {
                List<Telemetria> ll = new TelemetriaDAO().buscarTelemetriaPorVeiculo(idVeiculo.getId(), dataInicial, dataFinal);
                TelemetriaVeiculoVO telemetriaVeiculoVO = new TelemetriaVeiculoVO();
                Veiculos veiculo = new VeiculosDAO(null).buscarVeiculoModeloPeloId(idVeiculo.getId());
                telemetriaVeiculoVO.setPlaca(veiculo.getPlaca());
                telemetriaVeiculoVO.setIdVeiculo(veiculo.getId());
                String motorista = motoristasVeiculosService.buscarNomeMotoristaUltimoVeiculo(veiculo.getId());
                telemetriaVeiculoVO.setMotorista(motorista);
                Double maxHodometro = ll.stream().mapToDouble(Telemetria::getTotalHodometro).max().getAsDouble();
                Double minHodometro = 0d;
                Double kmPercorrido = 0d;
                if (maxHodometro == 0d) {
                    CalculadorDistancia calculadorDistancia = new CalculadorDistancia();
                    //Calcular os hodometro max e minimo pela lat e longitude
                    Telemetria tDaVez = null;
                    Double totalDistancia = 0d;
                    Collections.sort(ll, new Comparator<Telemetria>() {
                        public int compare(Telemetria o1, Telemetria o2) {
                            return o1.getDataCadastro().compareTo(o2.getDataCadastro());
                        }
                    });
                    for (Telemetria t : ll) {
                        if (Objects.isNull(tDaVez)) {
                            tDaVez = t;
                        } else {
                            Double distancia = calculadorDistancia.calcularDistancia(new LatLng(tDaVez.getLatitude(), tDaVez.getLongitude()), new LatLng(t.getLatitude(), t.getLongitude()));
                            if (distancia > 0.0) {
                                kmPercorrido = kmPercorrido + distancia;
                            }
                            tDaVez = t;
                        }
                    }
                } else {
                    minHodometro = ll.stream().filter(t -> t.getTotalHodometro() > 0d).mapToDouble(Telemetria::getTotalHodometro).min().getAsDouble();
                    kmPercorrido = maxHodometro - minHodometro;
                    if (veiculo.getIdModeloEquipamento().equals(180)) {//GV300
                        //GV300 manda como hectometre, divide por 10 para chegar nos km
                        kmPercorrido = kmPercorrido / 10;
                    }
                    if (veiculo.getIdModeloEquipamento().equals(166)) {//GV300
                        //Teltonica manda em metros
                        kmPercorrido = kmPercorrido / 1000;
                    }
                }
                telemetriaVeiculoVO.setKmPercorrido(kmPercorrido);
                Double maxCombustivel = ll.stream().mapToDouble(Telemetria::getCombustivelTotal).max().getAsDouble();
                if (maxCombustivel > 0) {
                    Double minCombustivel = ll.stream().filter(t -> t.getCombustivelTotal() > 0d).mapToDouble(Telemetria::getCombustivelTotal).min().getAsDouble();
                    Double consumoTotal = maxCombustivel - minCombustivel;
                    totalCombustivel = totalCombustivel + consumoTotal;
                    telemetriaVeiculoVO.setConsumoTotal(consumoTotal);
                    Double consumoMedio = kmPercorrido / consumoTotal;
                    telemetriaVeiculoVO.setConsumoMedio(consumoMedio);
                } else {
                    //ver se o campo mediaCaminhao tem algum valor
                    telemetriaVeiculoVO.setConsumoTotal(0d);
                    int cont = 0;
                    int media = 0;
                    for (Telemetria t : ll) {
                        if (t.getVelocidade() > 0 && t.getIgnicaoAtiva() && t.getMediaConsumo() > 0 && t.getMediaConsumo() < 10) {
                            cont++;
                            media = media + t.getMediaConsumo();
                        }
                    }
                    if (cont > 0 && media > 0) {
                        double c = media / cont;
                        telemetriaVeiculoVO.setConsumoMedio(c);
                    } else {
                        telemetriaVeiculoVO.setConsumoMedio(0d);
                    }

                }
                Double velocidadeMedia = ll.stream().filter(t -> t.getVelocidade() > 0).mapToDouble(Telemetria::getVelocidade).average().getAsDouble();
                telemetriaVeiculoVO.setVelocidadeMedia(velocidadeMedia);
                Double velocidadeMaxima = ll.stream().mapToDouble(Telemetria::getVelocidade).max().getAsDouble();
                telemetriaVeiculoVO.setVelocidadeMaxima(velocidadeMaxima);

                try {
                    Integer rpmMedia = (int) ll.stream().filter(t -> t.getRpm() > 0).mapToInt(Telemetria::getRpm).average().getAsDouble();
                    telemetriaVeiculoVO.setRpmMedia(rpmMedia);
                    Integer rpmMaxima = ll.stream().mapToInt(Telemetria::getRpm).max().getAsInt();
                    telemetriaVeiculoVO.setRpmMaximo(rpmMaxima);
                } catch (Exception e) {
                    telemetriaVeiculoVO.setRpmMedia(0);
                    telemetriaVeiculoVO.setRpmMaximo(0);
                }
                lista.add(telemetriaVeiculoVO);
                int totalDoTempo = Seconds.secondsBetween(new DateTime(dataInicial), new DateTime(dataFinal)).getSeconds();
                gerarTelemetriaDois(telemetriaVeiculoVO, ll, totalDoTempo);
            }
        } catch (Exception e) {
            LogSistema.logError("buscarTelemetriaDosVeiculos", e);
        }
        dadoTelemetria.setData(lista);
        gerarGrafico(dadoTelemetria);

        final DecimalFormat df = new DecimalFormat("##.00");
        dadoTelemetria.getData().forEach(d -> {
            if (d.getKmPercorrido() > 0d) {
                d.setKmPercorridoFormatado(df.format(d.getKmPercorrido()));
            } else {
                d.setKmPercorridoFormatado("0,00");
            }
            if (d.getConsumoTotal() > 0d) {
                d.setConsumoTotalFormatado(df.format(d.getConsumoTotal()));
            } else {
                d.setConsumoTotalFormatado("0,00");
            }
            if (d.getConsumoMedio() > 0) {
                d.setConsumoMedioFormatado(df.format(d.getConsumoMedio()));
            } else {
                d.setConsumoMedioFormatado("0,00");
            }
            if (d.getVelocidadeMedia() > 0d) {
                d.setVelocidadeMediaFormatado(df.format(d.getVelocidadeMedia()));
            } else {
                d.setVelocidadeMediaFormatado("0,00");
            }
            if (d.getVelocidadeMaxima() > 0) {
                d.setVelocidadeMaximaFormatado(df.format(d.getVelocidadeMaxima()));
            } else {
                d.setVelocidadeMaximaFormatado("0,00");
            }

            if (d.getConsumoDeCombustivelConducao() > 0d) {
                d.setConsumoDeCombustivelConducaoFormatado(df.format(d.getConsumoDeCombustivelConducao()));
            } else {
                d.setConsumoDeCombustivelConducaoFormatado("0,00");
            }

            if (d.getConsumoDeCombustivelMarchaLenta() > 0d) {
                d.setConsumoDeCombustivelMarchaLentaFormatado(df.format(d.getConsumoDeCombustivelMarchaLenta()));
            } else {
                d.setConsumoDeCombustivelMarchaLentaFormatado("0,00");
            }

            if (d.getTempoDeFuncionamentoMotor() > 0) {
                d.setTempoDeFuncionamentoMotorFormatado(TimeHelper.converteMinutosEmStringFormatadaTres(d.getTempoDeFuncionamentoMotor()));
            } else {
                d.setTempoDeFuncionamentoMotorFormatado("-");
            }

            if (d.getTempoConducao() > 0) {
                d.setTempoConducaoFormatado(TimeHelper.converteMinutosEmStringFormatadaTres(d.getTempoConducao()));
            } else {
                d.setTempoConducaoFormatado("-");
            }

            if (d.getMarchaLenta() > 0) {
                d.setMarchaLentaFormatado(TimeHelper.converteMinutosEmStringFormatadaTres(d.getMarchaLenta()));
            } else {
                d.setMarchaLentaFormatado("-");
            }

        });

        return dadoTelemetria;
    }

    private void gerarTelemetriaDois(TelemetriaVeiculoVO telemetriaVeiculoVO, List<Telemetria> lista, Integer totalDoTempo) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("dd/MM HH:mm:ss");
            List<GraficoTelemetriaVO> listaGraficoTelemetria = new ArrayList<>();
            Collections.sort(lista, new Comparator<Telemetria>() {
                public int compare(Telemetria o1, Telemetria o2) {
                    return o1.getDataCadastro().compareTo(o2.getDataCadastro());
                }
            });
            //Calcular tempo ligado
            int ligado = 0;
            int desligado = 0;
            int marchaLenta = 0;
            int conduzindo = 0;
            boolean isMarchaLenta = false;
            boolean isMarchaNormal = false;
            Double totalCombustivelConduzindo = 0d;
            Double totalCombustivelMarchaLenta = 0d;

            int contEventos = 0;
            int cont = 0;

            Telemetria ultimaTelemetria = null;
            Telemetria um = null;
            Telemetria dois = null;
            for (Telemetria t : lista) {

                if (t.getRpm() > 0 && t.getVelocidade().equals(0d) && t.getIgnicaoAtiva()) {
                    if (isMarchaNormal) {
                        isMarchaNormal = false;
                        if (contEventos <= 1) {
                            //buscar penultimo evento
                            um = lista.get(cont - 2);
                        }
                        int tempo = Seconds.secondsBetween(new DateTime(um.getDataCadastro()), new DateTime(dois.getDataCadastro())).getSeconds();
                        if (tempo > 0) {
                            ligado = ligado + tempo;
                            conduzindo = conduzindo + tempo;
                            if ((Objects.nonNull(um.getCombustivelTotal()) && um.getCombustivelTotal() > 0d)
                                    && (Objects.nonNull(dois.getCombustivelTotal()) && dois.getCombustivelTotal() > 0d)) {
                                double t2 = dois.getCombustivelTotal() - um.getCombustivelTotal();
                                totalCombustivelConduzindo = totalCombustivelConduzindo + t2;
                            }
                        }
                        um = null;
                        dois = null;
                        contEventos = 0;
                    }
                    if (Objects.isNull(um)) {
                        um = t;
                    }
                    dois = t;
                    isMarchaLenta = true;
                    contEventos++;
                }

                if (t.getRpm() > 0 && t.getVelocidade() > 0d && t.getIgnicaoAtiva()) {
                    if (isMarchaLenta) {
                        isMarchaLenta = false;
                        if (contEventos <= 1) {
                            //buscar penultimo evento
                            um = lista.get(cont - 2);
                        }
                        int tempo = Seconds.secondsBetween(new DateTime(um.getDataCadastro()), new DateTime(dois.getDataCadastro())).getSeconds();
                        if (tempo > 0) {
                            ligado = ligado + tempo;
                            marchaLenta = marchaLenta + tempo;
                            if ((Objects.nonNull(um.getCombustivelTotal()) && um.getCombustivelTotal() > 0d)
                                    && (Objects.nonNull(dois.getCombustivelTotal()) && dois.getCombustivelTotal() > 0d)) {
                                double t2 = dois.getCombustivelTotal() - um.getCombustivelTotal();
                                totalCombustivelMarchaLenta = totalCombustivelMarchaLenta + t2;
                            }
                        }
                        um = null;
                        dois = null;
                        contEventos = 0;
                    }
                    if (Objects.isNull(um)) {
                        um = t;
                    }
                    dois = t;
                    isMarchaNormal = true;
                    contEventos++;
                }

                if (t.getRpm().equals(0) && t.getVelocidade().equals(0) && !t.getIgnicaoAtiva()) {
                    if (isMarchaLenta) {
                        isMarchaLenta = false;
                        if (contEventos <= 1) {
                            //buscar penultimo evento
                            um = lista.get(cont - 1);
                        }
                        int tempo = Seconds.secondsBetween(new DateTime(um.getDataCadastro()), new DateTime(dois.getDataCadastro())).getSeconds();
                        if (tempo > 0) {
                            ligado = ligado + tempo;
                            marchaLenta = marchaLenta + tempo;
                            if ((Objects.nonNull(um.getCombustivelTotal()) && um.getCombustivelTotal() > 0d)
                                    && (Objects.nonNull(dois.getCombustivelTotal()) && dois.getCombustivelTotal() > 0d)) {
                                double t2 = dois.getCombustivelTotal() - um.getCombustivelTotal();
                                totalCombustivelMarchaLenta = totalCombustivelMarchaLenta + t2;
                            }
                        }
                        um = null;
                        dois = null;
                        contEventos = 0;
                    }
                    if (isMarchaNormal) {
                        isMarchaNormal = false;
                        if (contEventos <= 1) {
                            //buscar penultimo evento
                            um = lista.get(cont - 1);
                        }
                        int tempo = Seconds.secondsBetween(new DateTime(um.getDataCadastro()), new DateTime(dois.getDataCadastro())).getSeconds();
                        if (tempo > 0) {
                            ligado = ligado + tempo;
                            conduzindo = conduzindo + tempo;
                            if ((Objects.nonNull(um.getCombustivelTotal()) && um.getCombustivelTotal() > 0d)
                                    && (Objects.nonNull(dois.getCombustivelTotal()) && dois.getCombustivelTotal() > 0d)) {
                                double t2 = dois.getCombustivelTotal() - um.getCombustivelTotal();
                                totalCombustivelConduzindo = totalCombustivelConduzindo + t2;
                            }
                        }
                        um = null;
                        dois = null;
                        contEventos = 0;
                    }
                }
                cont++;
            }

            desligado = totalDoTempo - ligado;
            DecimalFormat df = new DecimalFormat("##.00");
            telemetriaVeiculoVO.setConsumoDeCombustivelConducao(totalCombustivelConduzindo);
            telemetriaVeiculoVO.setConsumoDeCombustivelConducaoFormatado(df.format(totalCombustivelConduzindo));
            telemetriaVeiculoVO.setConsumoDeCombustivelMarchaLenta(totalCombustivelMarchaLenta);
            telemetriaVeiculoVO.setConsumoDeCombustivelMarchaLentaFormatado(df.format(totalCombustivelMarchaLenta));
            telemetriaVeiculoVO.setTotalDesligado(desligado / 60);

            if (ligado > 60) {
                telemetriaVeiculoVO.setTempoDeFuncionamentoMotor(ligado / 60);
            } else {
                telemetriaVeiculoVO.setTempoDeFuncionamentoMotor(0);
            }
            if (conduzindo > 60) {
                telemetriaVeiculoVO.setTempoConducao(conduzindo / 60);
            } else {
                telemetriaVeiculoVO.setTempoConducao(0);
            }
            if (marchaLenta > 60) {
                telemetriaVeiculoVO.setMarchaLenta(marchaLenta / 60);
            } else {
                telemetriaVeiculoVO.setMarchaLenta(0);
            }


            long acelaracao = lista.stream().filter(b -> b.getAcelaracaoBrusca()).count();
            long freada = lista.stream().filter(b -> b.getFreadaBrusca()).count();
            long curva = lista.stream().filter(b -> b.getCurvaBrusca()).count();

            telemetriaVeiculoVO.setNumeroAcelaracaoBrusca((int) acelaracao);
            telemetriaVeiculoVO.setNumeroFreadaBrusca((int) freada);
            telemetriaVeiculoVO.setNumeroDeCurvaBrusca((int) curva);

        } catch (Exception e) {
            LogSistema.logError("gerarTelemetriaDois", e);
        }
    }


    private void gerarGrafico(DadoTelemetriaVO dadoTelemetria) {

        try {

            List<GraficoTelemetriaVO> listaGraficoTelemetria = new ArrayList<>();
            int totalLigado = 0;
            int totalDesligado = 0;
            int totalConduzindo = 0;
            int totalMarchaLenta = 0;
            double totalCombustivel = 0;
            Double totalCombustivelConduzindo = 0d;
            Double totalCombustivelMarchaLenta = 0d;
            for (TelemetriaVeiculoVO vo : dadoTelemetria.getData()) {
                totalLigado = totalLigado + vo.getTempoDeFuncionamentoMotor();
                totalDesligado = totalDesligado + vo.getTotalDesligado();
                totalConduzindo = totalConduzindo + vo.getTempoConducao();
                totalMarchaLenta = totalMarchaLenta + vo.getMarchaLenta();
                totalCombustivel = totalCombustivel + vo.getConsumoTotal();
                totalCombustivelConduzindo = totalCombustivelConduzindo + vo.getConsumoDeCombustivelConducao();
                totalCombustivelMarchaLenta = totalCombustivelMarchaLenta + vo.getConsumoDeCombustivelMarchaLenta();
            }


            GraficoTelemetriaVO g1 = new GraficoTelemetriaVO();
            g1.setTitulo("Motor Ligado/Desligado");
            g1.setDescricaoUm("Lig");
            if (totalLigado > 60) {
                g1.setValorUm((totalLigado / 60));//Horas
            } else {
                g1.setValorUm(totalLigado);//Horas
            }
            g1.setDescricaoDois("Desl");
            if (totalDesligado > 60) {
                g1.setValorDois((totalDesligado / 60));
            } else {
                g1.setValorDois(1);
            }
            listaGraficoTelemetria.add(g1);

            //Seghundo grafico
            GraficoTelemetriaVO g2 = new GraficoTelemetriaVO();
            g2.setTitulo("Tempo Funcionamento Motor");
            g2.setDescricaoUm("Condução");
            if (totalConduzindo > 60) {
                g2.setValorUm((totalConduzindo / 60));//Horas
            } else {
                g2.setValorUm(1);//Horas
            }
            g2.setDescricaoDois("Marcha lenta");
            if (totalMarchaLenta > 60) {
                g2.setValorDois((totalMarchaLenta / 60));
            } else {
                g2.setValorDois(1);
            }
            listaGraficoTelemetria.add(g2);


            //Seghundo grafico
            GraficoTelemetriaVO g3 = new GraficoTelemetriaVO();
            g3.setTitulo("Consumo de combustivel");
            g3.setDescricaoUm("Condução");
            g3.setValorUm(totalCombustivelConduzindo.intValue());//Horas
            g3.setDescricaoDois("Marcha lenta");
            g3.setValorDois(totalCombustivelMarchaLenta.intValue());
            listaGraficoTelemetria.add(g3);

            dadoTelemetria.setListaGraficoTelemetria(listaGraficoTelemetria);
        } catch (Exception e) {
            LogSistema.logError("gerarGraficos", e);
        }
    }

}
