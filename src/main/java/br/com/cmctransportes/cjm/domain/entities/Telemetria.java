package br.com.cmctransportes.cjm.domain.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class Telemetria implements Serializable {

    private static final long serialVersionUID = 1L;

    private String interruptorDeFreio;
    private String controleDeCruzeiroAtivo;
    private String chaveDeEmbreagem;
    private String estadoPTO;
    private String cargaAtualDoMotor;//porcentagem
    private String temperaturaLiquidoArrefecimentoMotor;
    private String velocidadeBaseadaNaRoda; //km/h
    private String posicaoDoPedalDeAcelaracao;//porcentagem
    private Integer rpm;
    private String temperaturaMotor;
    private String combustivelTotalUsadoPeloMotor;
    private String fuelLevel;
    private String totalHorasOperacaoMotor;
    private String taxaDeCombustivel;
    private String economiaCombustivelInstantanea;
    private String axixX;
    private String axixZ;
    private String axixY;
    private String tripOdometer;
    private String totalOdometer;
    private Boolean acelaracaoBrusca;
    private Boolean freadaBrusca;
    private Boolean curvaBrusca;
    private Integer greenDrivingValue;

    private Date dataCadastro;
    private Integer idVeiculo;
    private Integer idTransmissao;

    //essa parte nao vai para view
    @JsonIgnore
    private  Double totalHorimetro;
    @JsonIgnore
    private  Double totalHodometro;
    @JsonIgnore
    private Double combustivelTotal;
    @JsonIgnore
    private Integer fuelLevelInt;
    @JsonIgnore
    private Integer taxaCombustivelInt;
    @JsonIgnore
    private Double velocidade;
    @JsonIgnore
    private Boolean ignicaoAtiva;
    @JsonIgnore
    private Double latitude;
    @JsonIgnore
    private Double longitude;
    @JsonIgnore
    private Integer mediaConsumo;


}
