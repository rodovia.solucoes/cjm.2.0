package br.com.cmctransportes.cjm.domain.entities.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class EventoVeiculoVO implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer id;
    private Date dataEvento;
    private Integer tipo;
    private Integer localId;
    private Integer transmissaoId;
    private Integer veiculoId;
    private String tipoGeracaoViagem;

    @JsonIgnore
    private LocalVO local;

}
