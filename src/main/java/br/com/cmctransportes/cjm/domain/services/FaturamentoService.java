package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.constants.Constantes;
import br.com.cmctransportes.cjm.domain.entities.*;
import br.com.cmctransportes.cjm.domain.entities.vo.DetalheGraficoVO;
import br.com.cmctransportes.cjm.domain.entities.vo.FiltroVO;
import br.com.cmctransportes.cjm.domain.entities.vo.LocalGraficoVO;
import br.com.cmctransportes.cjm.domain.entities.vo.VeiculoVO;
import br.com.cmctransportes.cjm.infra.dao.JornadaDAO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.proxy.modelo.Veiculo;
import br.com.cmctransportes.cjm.sascarsoap.Motorista;
import br.com.cmctransportes.cjm.utils.TimeHelper;
import org.apache.commons.codec.binary.Hex;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;

import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class FaturamentoService {

    //private String FATURANDO = "Faturando";
    private String OCIOSO = "Ocioso";
    private String INDISPONIVEL = "Improdutivo";
    private String PRODUZINDO = "Em produção";
    private String EVENTO = "Evento";
    private Integer EVENTO_CARREGADO = 1;
    private Integer EVENTO_VAZIO = 2;

    @Autowired
    private MotoristasVeiculosService motoristasVeiculosService;
    @Autowired
    private VeiculosService veiculosService;
    @Autowired
    private TransmissaoService transmissaoService;
    @Autowired
    private EventoMotoristaService eventoMotoristaService;
    @Autowired
    private ViagemService viagemService;
    @Autowired
    private EventoService eventoService;

    private JornadaDAO jornadaDAO;


    public FaturamentoService(EntityManager em) {
        jornadaDAO = new JornadaDAO(em);
    }

    public List<ResumoFaturamento> calcularResumoFaturamento(FiltroVO filtroVO) {
        List<ResumoFaturamento> list = new ArrayList<>();
        try {

            Date dataInicio = TimeHelper.getDate000000(filtroVO.getDataInicial());
            Date dataFim = TimeHelper.getDateWithNowTime(filtroVO.getDataFinal());
            int quantidadeDeDias = Days.daysBetween(new DateTime(filtroVO.getDataFinal()), new DateTime(new Date())).getDays();
            if (quantidadeDeDias > 0) {
                dataFim = TimeHelper.getDate235959(filtroVO.getDataFinal());
            }

            for (VeiculoVO veiculoVO : filtroVO.getListaDeVeiculos()) {
                /* Primeiro tenta buscar pelo via-m */
                List<Motoristas> lista = motoristasVeiculosService.buscarMotoristaVeiculo(veiculoVO.getId(), dataInicio, dataFim);
                if (Objects.nonNull(lista) && !lista.isEmpty()) {
                    ResumoFaturamento resumoFaturamento = calcularPeloViaM(lista, dataInicio, dataFim);
                    resumoFaturamento.setPlaca(veiculoVO.getPlaca());
                    list.add(resumoFaturamento);
                }

                /*
                /*Segunda tenta buscar as transmissoes - VIA LOG
                List<Transmissao> listaTransmissao = transmissaoService.buscarDadosParaFaturamento(veiculoVO.getId(), dataInicio, dataFim);
                if (!listaTransmissao.isEmpty()) {
                    ResumoFaturamento resumoFaturamento = calcularPeloViaSat(listaTransmissao);
                    resumoFaturamento.setPlaca(veiculoVO.getPlaca());
                    list.add(resumoFaturamento);
                }*/
            }
        } catch (Exception e) {
            LogSistema.logError("buscarLocais", e);
        }
        return list;
    }


    private ResumoFaturamento calcularPeloViaM(List<Motoristas> listMotoristas, Date dataInicio, Date dataFim) {
        ResumoFaturamento resumoFaturamento = new ResumoFaturamento();
        try {
            int minutos = Minutes.minutesBetween(new DateTime(dataInicio), new DateTime(dataFim)).getMinutes();//equivale a 100% do tempo do caminhao
            int minutosOsioso = 0;
            int minutosFaturando = 0;
            int minutosIndisponivel = 0;
            for (Motoristas motoristas : listMotoristas) {
                List<Evento> listaEventos = jornadaDAO.buscarEventoTimeLineNativo(dataInicio, dataFim, motoristas.getId());
                Date inicioDaJornada = null;
                Date inicioDaDirecao = null;
                boolean calculouTempoOcioso = false;
                Evento ultimoEvento = null;

                for (Evento evento : listaEventos) {
                    if (evento.getTipoEvento().getId() == 26 || evento.getTipoEvento().getId() == -26
                            || evento.getTipoEvento().getId() == 20 || evento.getTipoEvento().getId() == -20
                            || evento.getTipoEvento().getId() == 17 || evento.getTipoEvento().getId() == -17
                            || evento.getTipoEvento().getId() == 24 || evento.getTipoEvento().getId() == -24) {
                        continue;
                    }

                    if (Objects.isNull(ultimoEvento)) {
                        ultimoEvento = evento;
                        continue;
                    }


                    if (!mesmoEvento(ultimoEvento, evento)) {
                        int v = Minutes.minutesBetween(new DateTime(ultimoEvento.getInstanteEvento()), new DateTime(evento.getInstanteEvento())).getMinutes();
                        if (v > 2) {
                            minutosOsioso = minutosOsioso + v;
                        }
                    } else {
                        if (mesmoEvento(ultimoEvento, evento)) {
                            int v = Minutes.minutesBetween(new DateTime(ultimoEvento.getInstanteEvento()), new DateTime(evento.getInstanteEvento())).getMinutes();
                            if (ultimoEvento.getTipoEvento().getId() == 8 || ultimoEvento.getTipoEvento().getId() == -8) {
                                minutosFaturando = minutosFaturando + v;
                            } else {
                                minutosIndisponivel = minutosIndisponivel + v;
                            }
                        }
                    }
                    ultimoEvento = evento;
                }
            }

            int porcentagemFaturando = (minutosFaturando * 100) / minutos;
            int porcentagemOcioso = (minutosOsioso * 100) / minutos;
            int porcetagemIndiponivel = (minutosIndisponivel * 100) / minutos;

            resumoFaturamento.setIndisponivel(porcetagemIndiponivel);
            resumoFaturamento.setOcioso(porcentagemOcioso);
            resumoFaturamento.setFaturando(porcentagemFaturando);

            resumoFaturamento.setHorasFaturando(minutosFaturando / 60);
            resumoFaturamento.setHorasIndisponivel(minutosIndisponivel / 60);
            resumoFaturamento.setHorasOcioso(minutosOsioso / 60);
        } catch (Exception e) {
            LogSistema.logError("calcularPeloViaM", e);
        }
        return resumoFaturamento;
    }


    private ResumoFaturamento calcularPeloViaSat(List<Transmissao> listaTransmissao) {
        ResumoFaturamento resumoFaturamento = new ResumoFaturamento();
        try {
            int minutosOsioso = 0;
            int minutosFaturando = 0;
            int minutosIndisponivel = 0;
            Transmissao ultimaTransmissao = null;
            Date dataInicial = null;
            for (Transmissao transmissao : listaTransmissao) {
                if (Objects.isNull(ultimaTransmissao)) {
                    ultimaTransmissao = transmissao;
                    dataInicial = transmissao.getDataTransmissao();
                    continue;
                }

                if (transmissao.getVelocidade() == 0 && transmissao.getIgnicaoAtiva()) {
                    int segundosOsioso = Seconds.secondsBetween(new DateTime(ultimaTransmissao.getDataTransmissao()), new DateTime(transmissao.getDataTransmissao())).getSeconds();
                    minutosOsioso = minutosOsioso + segundosOsioso;
                }

                if (transmissao.getVelocidade() > 0 && transmissao.getIgnicaoAtiva()) {
                    int segundosFaturando = Seconds.secondsBetween(new DateTime(ultimaTransmissao.getDataTransmissao()), new DateTime(transmissao.getDataTransmissao())).getSeconds();
                    minutosFaturando = minutosFaturando + segundosFaturando;
                }

                ultimaTransmissao = transmissao;
            }

            int segudosTotal = Seconds.secondsBetween(new DateTime(dataInicial), new DateTime(ultimaTransmissao.getDataTransmissao())).getSeconds();//equivale a 100% do tempo do caminhao

            int porcentagemFaturando = (minutosFaturando * 100) / segudosTotal;
            int porcentagemOcioso = (minutosOsioso * 100) / segudosTotal;
            int porcetagemIndiponivel = 100 - porcentagemFaturando - porcentagemOcioso;

            resumoFaturamento.setIndisponivel(porcetagemIndiponivel);
            resumoFaturamento.setOcioso(porcentagemOcioso);
            resumoFaturamento.setFaturando(porcentagemFaturando);


        } catch (Exception e) {
            LogSistema.logError("calcularPeloViaSat", e);
        }
        return resumoFaturamento;
    }


    public Faturamento gerarTimeLine(FiltroVO filtroVO) {
        String nomeMotista = "";
        final Faturamento faturamento = new Faturamento();
        List<TimelineFaturamento> listaTimeLine = new ArrayList<>();
        try {
            List<Motoristas> lista = motoristasVeiculosService.buscarMotoristaVeiculo(filtroVO.getVeiculo().getId(), filtroVO.getDataInicial(), filtroVO.getDataFinal());
            int cont = 0;
            lista.forEach(m -> {
                String nome = m.getNome() + " " + m.getSobrenome();
                faturamento.setNomeDoMotorista(nome);
            });
            if (Objects.nonNull(lista)) {
                List<TimelineFaturamento> listaTimeLineViaM = processarTimelineViaM(lista, filtroVO.getDataInicial(), filtroVO.getDataFinal());
                Collections.reverse(listaTimeLineViaM);
                listaTimeLine.addAll(listaTimeLineViaM);
            }

            /** AGUARDAR VIA LOG */
            /*Segunda tenta buscar as transmissoes */
            /*
            List<Transmissao> listaTransmissao = transmissaoService.buscarDadosParaFaturamento(filtroVO.getVeiculo().getId(), TimeHelper.getDate000000(filtroVO.getDataInicial()), TimeHelper.getDate235959(filtroVO.getDataInicial()));
            if (!listaTransmissao.isEmpty()) {
                List<TimelineFaturamento> listaTimeLineViaSat = processarTimelineViaSat(listaTransmissao);
                listaTimeLine.addAll(listaTimeLineViaSat);
            }
            */
            buscarViagensParaFaturamento(filtroVO.getVeiculo().getPlaca(), filtroVO.getDataInicial(), filtroVO.getDataFinal(), listaTimeLine, faturamento);

            listaTimeLine.forEach(t -> {
                if((Objects.nonNull(t.getLongitudeInicial()) && t.getLongitudeInicial() != 0) && (Objects.nonNull(t.getLatitudeInicial()) && t.getLatitudeInicial() != 0) && Objects.isNull(t.getEnderecoInicial())){
                    t.setEnderecoInicial("Não encontrado");
                }
                if((Objects.nonNull(t.getLongitudeFinal()) && t.getLongitudeFinal() != 0) && (Objects.nonNull(t.getLatitudeFinal()) && t.getLatitudeFinal() != 0) && Objects.isNull(t.getEnderecoFinal())){
                    t.setEnderecoFinal("Não encontrado");
                }
            });

            listaTimeLine.sort((o1, o2) -> o2.getOrderm().compareTo(o1.getOrderm()));
            faturamento.setListaTimelineFaturamento(listaTimeLine);
            gerarGraficoResumo(faturamento);
        } catch (Exception e) {
            LogSistema.logError("gerarTimeLine", e);
        }

        return faturamento;
    }

    double meta = 0d;

    private void buscarViagensParaFaturamento(String placa, Date dataInicial, Date dataFinal, List<TimelineFaturamento> listaTimeLine, Faturamento faturamento) {
        try {

            double fat = 0d;
            double dispersao = 0d;
            meta = 0d;
            List<Viagem> lista = viagemService.buscarPorVeiculo(placa, dataInicial, dataFinal);
            List<Viagem> metas = lista.stream().filter(distinctByMeta(Viagem::getMeta)).collect(Collectors.toList());
            metas.forEach(m -> {
                meta = meta + m.getMeta().doubleValue();
            });

            DecimalFormat decFormat = new DecimalFormat("'R$ ' 0.##");
            for (Viagem v : lista) {
                TimelineFaturamento timelineFaturamento = new TimelineFaturamento();
                timelineFaturamento.setOrderm(6);
                timelineFaturamento.setNomeEvento("Faturando");
                timelineFaturamento.setCategory("Faturando");
                timelineFaturamento.setStart(v.getInicioDaViagem().getTime());
                timelineFaturamento.setEnd(v.getFimDaViagem().getTime());
                timelineFaturamento.setTask("Faturamento: " + decFormat.format(v.getFaturamento()));
                timelineFaturamento.setColor("#00FFFF");
                listaTimeLine.add(timelineFaturamento);
                fat = fat + v.getFaturamento().doubleValue();
                dispersao = dispersao + v.getDispersao().doubleValue();
            }


            int dias = Days.daysBetween(new DateTime(dataInicial), new DateTime(dataFinal)).getDays();
            double metaPorDia = meta / dias;
            double faturamentoPorDia = fat / dias;

            double porcentual = (fat * 100) / meta;

            DadosFaturamento dadosFaturamento = new DadosFaturamento();
            dadosFaturamento.setDispersao(dispersao);
            dadosFaturamento.setMeta(meta);
            dadosFaturamento.setMetaPorDia(metaPorDia);
            dadosFaturamento.setFaturamento(fat);
            dadosFaturamento.setFaturamentoPorDia(faturamentoPorDia);
            dadosFaturamento.setDiasApurados(dias);
            dadosFaturamento.setPorcetagemFaturamento(porcentual);
            faturamento.setDadosFaturamento(dadosFaturamento);
        } catch (Exception e) {
            LogSistema.logError("buscarViagensParaFaturamento", e);
        }
    }

    private void gerarGraficoResumo(Faturamento faturamento) {
        try {
            List<TimelineFaturamento> lista = faturamento.getListaTimelineFaturamento();
            Map<String, List<TimelineFaturamento>> map = new HashMap<>();
            for (TimelineFaturamento timelineFaturamento : lista) {
                if (map.containsKey(timelineFaturamento.getNomeEvento())) {
                    map.get(timelineFaturamento.getNomeEvento()).add(timelineFaturamento);
                } else {
                    List<TimelineFaturamento> l = new ArrayList<>();
                    l.add(timelineFaturamento);
                    map.put(timelineFaturamento.getNomeEvento(), l);
                }
            }

            List<GraficoResumo> listGraficoResumos = new ArrayList<>();
            for (String ss : map.keySet()) {
                GraficoResumo graficoResumo = null;
                List<TimelineFaturamento> l = map.get(ss);
                for (TimelineFaturamento timelineFaturamento : l) {
                    if (Objects.isNull(graficoResumo)) {
                        graficoResumo = new GraficoResumo();
                        graficoResumo.setCor(timelineFaturamento.getColor());
                        graficoResumo.setEvento(ss);
                        int minutos = Minutes.minutesBetween(new DateTime(timelineFaturamento.getStart()), new DateTime(timelineFaturamento.getEnd())).getMinutes();
                        double horas = minutos / 60.0;
                        graficoResumo.setHoras(horas);
                    } else {
                        int minutos = Minutes.minutesBetween(new DateTime(timelineFaturamento.getStart()), new DateTime(timelineFaturamento.getEnd())).getMinutes();
                        double horas = minutos / 60.0;
                        graficoResumo.setHoras(graficoResumo.getHoras() + horas);
                    }
                }
                listGraficoResumos.add(graficoResumo);
            }

            listGraficoResumos.forEach(gr -> {
                gr.setHoras(roundTwoDecimals(gr.getHoras()));
            });

            faturamento.setListaGraficoResumo(listGraficoResumos);
        } catch (Exception e) {
            LogSistema.logError("gerarTimeLine", e);
        }
    }

    public double roundTwoDecimals(double d) {
        BigDecimal a = new BigDecimal(d);
        BigDecimal roundOff = a.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        return roundOff.doubleValue();
    }

    private List<TimelineFaturamento> processarTimelineViaM(List<Motoristas> listMotoristas, Date dataInicio, Date dataFim) {
        List<TimelineFaturamento> lista = new ArrayList<>();

        try {
            List<Evento> listaDeEventos = new ArrayList<>();
            for (Motoristas motoristas : listMotoristas) {
                List<Evento> ll = jornadaDAO.buscarEventoTimeLineNativo(dataInicio, dataFim, motoristas.getId());
                listaDeEventos.addAll(ll);
                ajustarEventoMotorista(motoristas.getId(), dataInicio, dataFim, lista);
            }
            Evento ultimoEvento = null;
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM HH:mm:ss");
            for (Evento evento : listaDeEventos) {
                if (evento.getTipoEvento().getId() == 26 || evento.getTipoEvento().getId() == -26
                        || evento.getTipoEvento().getId() == 20 || evento.getTipoEvento().getId() == -20
                        || evento.getTipoEvento().getId() == 17 || evento.getTipoEvento().getId() == -17
                        || evento.getTipoEvento().getId() == 24 || evento.getTipoEvento().getId() == -24) {
                    continue;
                }

                if (Objects.isNull(ultimoEvento)) {
                    ultimoEvento = evento;
                } else {
                    //verificar se é abertura e fechamento do mesmo evento
                    if (ultimoEvento.getTipoEvento().getId() == (evento.getTipoEvento().getId() * -1)) {
                        Integer idDoEvento = ultimoEvento.getTipoEvento().getId();
                        String nomeEvento = getNomeDoEvento(idDoEvento);
                        String categoria = ultimoEvento.getTipoEvento().getId() == 8 ? PRODUZINDO : INDISPONIVEL;
                        criarTimelineFaturamento(ultimoEvento, evento, categoria, nomeEvento, lista, 1);
                    } else {
                        if (ultimoEvento.getTipoEvento().getId() == 0 && evento.getTipoEvento().getId() == 1) {
                            //Evento de interjornada
                            criarTimelineFaturamento(ultimoEvento, evento, INDISPONIVEL, "Interjornada", lista, 3);
                        }
                    }
                    ultimoEvento = evento;
                }
            }

            //calcular ociosidade
            ultimoEvento = null;
            for (Evento evento : listaDeEventos) {
                if (evento.getTipoEvento().getId() == 26 || evento.getTipoEvento().getId() == -26
                        || evento.getTipoEvento().getId() == 20 || evento.getTipoEvento().getId() == -20
                        || evento.getTipoEvento().getId() == 17 || evento.getTipoEvento().getId() == -17
                        || evento.getTipoEvento().getId() == 24 || evento.getTipoEvento().getId() == -24) {
                    continue;
                }
                if (Objects.isNull(ultimoEvento)) {
                    ultimoEvento = evento;
                    continue;
                }

                if (ultimoEvento.getTipoEvento().getId() == 0 //fim de jornada
                        && evento.getTipoEvento().getId() == 1) {//inicio de jornada
                    ultimoEvento = evento;
                    continue;
                }

                if (!mesmoEvento(ultimoEvento, evento)) {
                    int v = Minutes.minutesBetween(new DateTime(ultimoEvento.getInstanteEvento()), new DateTime(evento.getInstanteEvento())).getMinutes();
                    if (v > 2) {
                        TimelineFaturamento timelineFaturamentoEvento = new TimelineFaturamento();
                        timelineFaturamentoEvento.setOrderm(2);
                        timelineFaturamentoEvento.setCategory(OCIOSO);
                        timelineFaturamentoEvento.setColor("#FF0000");
                        timelineFaturamentoEvento.setStart(ultimoEvento.getInstanteEvento().getTime());
                        timelineFaturamentoEvento.setNomeEvento("Ocioso");
                        timelineFaturamentoEvento.setEnd(evento.getInstanteEvento().getTime());
                        timelineFaturamentoEvento.setInicio(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(ultimoEvento.getInstanteEvento()));
                        timelineFaturamentoEvento.setFim(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(evento.getInstanteEvento()));
                        int tempo = Minutes.minutesBetween(new DateTime(timelineFaturamentoEvento.getStart()), new DateTime(timelineFaturamentoEvento.getEnd())).getMinutes();
                        String tt = TimeHelper.converteMinutosEmStringFormatadaDois(tempo);
                        String evOrigem = getNomeDoEvento(ultimoEvento.getTipoEvento().getId());
                        String evDest = getNomeDoEvento(evento.getTipoEvento().getId());
                        String task = "Ocioso entre " + evOrigem + ":" + evDest + " - " + tt;
                        timelineFaturamentoEvento.setTask(task);
                        timelineFaturamentoEvento.setTempoTotal(tt);
                        timelineFaturamentoEvento.setLatitudeInicial(ultimoEvento.getLatitude());
                        timelineFaturamentoEvento.setLongitudeInicial(ultimoEvento.getLongitude());
                        timelineFaturamentoEvento.setEnderecoInicial(ultimoEvento.getPositionAddress());
                        timelineFaturamentoEvento.setLatitudeFinal(evento.getLatitude());
                        timelineFaturamentoEvento.setLongitudeFinal(evento.getLongitude());
                        timelineFaturamentoEvento.setEnderecoFinal(evento.getPositionAddress());
                        lista.add(timelineFaturamentoEvento);
                    }
                }
                ultimoEvento = evento;
            }

        } catch (Exception e) {
            LogSistema.logError("processarTimeline", e);
        }
        return lista;
    }


    private void ajustarEventoMotorista(Integer idMotorista, Date dataInicial, Date dataFinal, List<TimelineFaturamento> lista) {
        try {
            List<EventoMotorista> listaEvento = eventoMotoristaService.buscarPorPeriodos(idMotorista, dataInicial, dataFinal);
            if(listaEvento.isEmpty()){
                TimelineFaturamento timelineFaturamento = new TimelineFaturamento();
                timelineFaturamento.setOrderm(4);
                timelineFaturamento.setCategory("Carregado");
                timelineFaturamento.setColor("#DEB887");
                timelineFaturamento.setTask("Sem Informação");
                timelineFaturamento.setNomeEvento("Carregado");
                timelineFaturamento.setStart(dataInicial.getTime());
                timelineFaturamento.setEnd(dataFinal.getTime());
                lista.add(timelineFaturamento);


                timelineFaturamento = new TimelineFaturamento();
                timelineFaturamento.setOrderm(5);
                timelineFaturamento.setCategory("Vazio");
                timelineFaturamento.setColor("#DEB887");
                timelineFaturamento.setTask("Sem Informação");
                timelineFaturamento.setNomeEvento("Vazio");
                timelineFaturamento.setStart(dataInicial.getTime());
                timelineFaturamento.setEnd(dataFinal.getTime());
                lista.add(timelineFaturamento);

            }else {
                for (EventoMotorista eventoMotorista : listaEvento) {
                    int hodometro = eventoMotorista.getHodometroFinal() - eventoMotorista.getHodometroInicial();
                    if (Objects.equals(eventoMotorista.getTipo(), EVENTO_VAZIO)) {
                        TimelineFaturamento timelineFaturamento = new TimelineFaturamento();
                        timelineFaturamento.setOrderm(5);
                        timelineFaturamento.setCategory("Vazio");
                        timelineFaturamento.setColor("#FF4500");
                        timelineFaturamento.setTask("Vazio - " + hodometro + " Km");
                        timelineFaturamento.setNomeEvento("Vazio");
                        timelineFaturamento.setStart(eventoMotorista.getDataEvento().getTime());
                        timelineFaturamento.setEnd(eventoMotorista.getDataFimEvento().getTime());
                        lista.add(timelineFaturamento);
                    }

                    if (Objects.equals(eventoMotorista.getTipo(), EVENTO_CARREGADO)) {
                        TimelineFaturamento timelineFaturamento = new TimelineFaturamento();
                        timelineFaturamento.setOrderm(4);
                        timelineFaturamento.setCategory("Carregado");
                        timelineFaturamento.setColor("#2E8B57");
                        timelineFaturamento.setTask("Carregado - " + hodometro + " Km");
                        timelineFaturamento.setNomeEvento("Carregado");
                        timelineFaturamento.setStart(eventoMotorista.getDataEvento().getTime());
                        timelineFaturamento.setEnd(eventoMotorista.getDataFimEvento().getTime());
                        lista.add(timelineFaturamento);
                    }
                }
            }
        } catch (Exception e) {
            LogSistema.logError("ajustarEventoMotorista", e);
        }
    }


    private void criarTimelineFaturamento(Evento ultimoEvento, Evento evento, String caterogia, String nomeEvento, List<TimelineFaturamento> lista, int orderm) {

        TimelineFaturamento timelineFaturamentoEvento = new TimelineFaturamento();
        timelineFaturamentoEvento.setOrderm(orderm);
        timelineFaturamentoEvento.setCategory(caterogia);
        if (Objects.equals("Interjornada", nomeEvento)) {
            timelineFaturamentoEvento.setColor("#CCCCCC");
        } else {
            if (Objects.nonNull(ultimoEvento.getTipoEvento().getColor())) {
                timelineFaturamentoEvento.setColor(ultimoEvento.getTipoEvento().getColor());
            } else {
                timelineFaturamentoEvento.setColor(ultimoEvento.getTipoEvento().getObjGeraEstado().getTiposEstadoJornadaId().getColor());
            }
        }

        timelineFaturamentoEvento.setStart(ultimoEvento.getInstanteEvento().getTime());
        timelineFaturamentoEvento.setInicio(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(ultimoEvento.getInstanteEvento()));
        timelineFaturamentoEvento.setNomeEvento(nomeEvento);
        timelineFaturamentoEvento.setEnd(evento.getInstanteEvento().getTime());
        timelineFaturamentoEvento.setFim(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(evento.getInstanteEvento()));
        int tempo = Minutes.minutesBetween(new DateTime(timelineFaturamentoEvento.getStart()), new DateTime(timelineFaturamentoEvento.getEnd())).getMinutes();
        String tt = TimeHelper.converteMinutosEmStringFormatadaDois(tempo);
        String task = nomeEvento + " - " + tt;
        timelineFaturamentoEvento.setTask(task);
        timelineFaturamentoEvento.setTempoTotal(tt);
        timelineFaturamentoEvento.setLatitudeInicial(ultimoEvento.getLatitude());
        timelineFaturamentoEvento.setLongitudeInicial(ultimoEvento.getLongitude());
        timelineFaturamentoEvento.setEnderecoInicial(ultimoEvento.getPositionAddress());

        timelineFaturamentoEvento.setLatitudeFinal(evento.getLatitude());
        timelineFaturamentoEvento.setLongitudeFinal(evento.getLongitude());
        timelineFaturamentoEvento.setEnderecoFinal(evento.getPositionAddress());


        lista.add(timelineFaturamentoEvento);
    }

    private static <T> Predicate<T> distinctByMeta(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }

    private String getNomeDoEvento(Integer id) {
        if (id < 0) {
            id = id * -1;
        }
        switch (id) {
            case 13:
                return "Fiscalização";
            case 12:
                return "Descarga";
            case 11:
                return "Aguardando Descarga";
            case 10:
                return "Carga";
            case 9:
                return "Aguardando Carga";
            case 7:
                return "Abastecimento";
            case 6:
                return "Banheiro";
            case 5:
                return "Lanche";
            case 3:
                return "Manutenção";
            case 2:
                return "Manifesto";
            case 14:
                return "Refeição";
            case 15:
                return "Descanso";
            case 21:
                return "Folga";
            case 22:
                return "Feriado";
            case 20:
                return "DSR";
            case 19:
                return "Falta";
            case 18:
                return "Ferias";
            case 17:
                return "Abono";
            case 23:
                return "Pista Interrompida";
            case 4:
                return "Sinistro";
            case 24:
                return "Abono Refeição";
            case 25:
                return "Intervalor Pessoal";
            case 26:
                return "Interjornada";
            case 0:
                return "Fim Jornada";
            case 1:
                return "Inicio Jornada";
            case 8:
                return "Direção";
        }
        return null;
    }

    private boolean mesmoEvento(Evento ultimoEvento, Evento eventoAtual) {
        if ((ultimoEvento.getTipoEvento().getId() * -1) == eventoAtual.getTipoEvento().getId() || (ultimoEvento.getTipoEvento().getId() == 0 && eventoAtual.getTipoEvento().getId() == 1)) {
            return true;
        }

        return false;
    }

    public DetalheGraficoVO buscarDetalheEvento(FiltroVO filtroVO) {
        DetalheGraficoVO detalheGraficoVO = null;
        try {
            if ("Ocioso".equalsIgnoreCase(filtroVO.getNomeColuna())) {
                detalheGraficoVO = calcularOsiosidade(filtroVO);
            } else {
                if (isEvento(filtroVO.getNomeColuna())) {
                    detalheGraficoVO = calcularSobreEventosDoMotorista(filtroVO);
                } else {
                    if ("Carregado".equalsIgnoreCase(filtroVO.getNomeColuna())) {
                        detalheGraficoVO = busrcarEventoMotorista(filtroVO, 1);
                    }
                    if ("Vazio".equalsIgnoreCase(filtroVO.getNomeColuna())) {
                        detalheGraficoVO = busrcarEventoMotorista(filtroVO, 2);
                    }
                }
            }
        } catch (Exception e) {
            LogSistema.logError("buscarDetalheEvento", e);
        }
        return detalheGraficoVO;
    }

    private boolean isEvento(String nomeDaColuna) {
        if ((nomeDaColuna.contains("Faturado")) || (nomeDaColuna.contains("Vazio")) || (nomeDaColuna.contains("Carregado"))) {
            return false;
        }
        return true;
    }


    private DetalheGraficoVO calcularSobreEventosDoMotorista(FiltroVO filtroVO) {
        DetalheGraficoVO detalheGraficoVO = new DetalheGraficoVO();
        try {
            int idEvento = getIdDoEvento(filtroVO.getNomeColuna().trim());
            List<LocalGraficoVO> listaLocalGraficoVO = new ArrayList<>();
            int totalDeMinutos = 0;
            for (VeiculoVO veiculo : filtroVO.getListaDeVeiculos()) {
                List<Motoristas> lista = motoristasVeiculosService.buscarMotoristaVeiculo(veiculo.getId(), filtroVO.getDataInicial(), filtroVO.getDataFinal());
                for (Motoristas m : lista) {
                    List<LocalGraficoVO> listaTemp = eventoService.buscarEventoParaGrafico(idEvento, m.getId(), filtroVO.getDataInicial(), filtroVO.getDataFinal());
                    Integer sum = listaTemp.stream()
                            .map(x -> x.getValorHora())
                            .reduce(0, Integer::sum);
                    listaLocalGraficoVO.addAll(listaTemp);
                    totalDeMinutos = totalDeMinutos + sum;
                }
            }

            detalheGraficoVO.setListaDeLocalGrafico(listaLocalGraficoVO);
            detalheGraficoVO.setTotalDeHoras(TimeHelper.converteMinutosEmStringFormatada(totalDeMinutos));
        } catch (Exception e) {
            LogSistema.logError("buscarDetalheEvento", e);
        }
        return detalheGraficoVO;
    }


    private DetalheGraficoVO busrcarEventoMotorista(FiltroVO filtroVO, int tipo) {
        DetalheGraficoVO detalheGraficoVO = new DetalheGraficoVO();
        List<LocalGraficoVO> listaLocalGraficoVO = new ArrayList<>();
        try {
            int totalMinutos = 0;
            int hodoTotal = 0;
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM HH:mm");
            for (VeiculoVO veiculo : filtroVO.getListaDeVeiculos()) {
                List<Motoristas> lista = motoristasVeiculosService.buscarMotoristaVeiculo(veiculo.getId(), filtroVO.getDataInicial(), filtroVO.getDataFinal());
                for (Motoristas m : lista) {
                    List<EventoMotorista> listaEVento = eventoMotoristaService.buscarPorPeriodos(m.getId(), tipo, filtroVO.getDataInicial(), filtroVO.getDataFinal());
                    for (EventoMotorista eventoMotorista : listaEVento) {
                        LocalGraficoVO graficoVO = new LocalGraficoVO();
                        graficoVO.setDataEvento(simpleDateFormat.format(eventoMotorista.getDataEvento()));
                        graficoVO.setDataEventoFinal(simpleDateFormat.format(eventoMotorista.getDataFimEvento()));
                        graficoVO.setEventoInicio(eventoMotorista.getHodometroInicial().toString());
                        graficoVO.setEventoFinal(eventoMotorista.getHodometroFinal().toString());
                        int minutos = Minutes.minutesBetween(new DateTime(eventoMotorista.getDataEvento()), new DateTime(eventoMotorista.getDataFimEvento())).getMinutes();
                        graficoVO.setTempoEntreEventos(TimeHelper.converteMinutosEmStringFormatada(minutos));
                        totalMinutos = totalMinutos + minutos;
                        int hodo = eventoMotorista.getHodometroFinal() - eventoMotorista.getHodometroInicial();
                        hodoTotal = hodoTotal + hodo;
                        graficoVO.setStatus(String.valueOf(hodo));
                        listaLocalGraficoVO.add(graficoVO);
                    }
                }
            }
            detalheGraficoVO.setListaDeLocalGrafico(listaLocalGraficoVO);
            detalheGraficoVO.setTotalDeHoras(TimeHelper.converteMinutosEmStringFormatada(totalMinutos));
            detalheGraficoVO.setTotalHodometro(String.valueOf(hodoTotal));

        } catch (Exception e) {
            LogSistema.logError("busrcarEventoMotorista", e);
        }

        return detalheGraficoVO;
    }

    private DetalheGraficoVO calcularOsiosidade(FiltroVO filtroVO) {
        DetalheGraficoVO detalheGraficoVO = new DetalheGraficoVO();
        try {
            List<LocalGraficoVO> listaGrafico = new ArrayList<>();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM HH:mm");
            int tempoTotal = 0;
            for (VeiculoVO veiculo : filtroVO.getListaDeVeiculos()) {
                List<Motoristas> lista = motoristasVeiculosService.buscarMotoristaVeiculo(veiculo.getId(), filtroVO.getDataInicial(), filtroVO.getDataFinal());
                for (Motoristas m : lista) {
                    List<Evento> listaDeEventos = eventoService.buscarEventoParaGrafico(m.getId(), filtroVO.getDataInicial(), filtroVO.getDataFinal());
                    Evento ultimoEvento = null;
                    for (Evento evento : listaDeEventos) {
                        if (Objects.isNull(ultimoEvento)) {
                            ultimoEvento = evento;
                            continue;
                        }
                        if (ultimoEvento.getTipoEvento().getId() == 0 //fim de jornada
                                && evento.getTipoEvento().getId() == 1) {//inicio de jornada
                            ultimoEvento = evento;
                            continue;
                        }
                        if (!mesmoEvento(ultimoEvento, evento)) {
                            int v = Minutes.minutesBetween(new DateTime(ultimoEvento.getInstanteEvento()), new DateTime(evento.getInstanteEvento())).getMinutes();
                            if (v > 2) {
                                tempoTotal = tempoTotal + v;
                                LocalGraficoVO graficoVO = new LocalGraficoVO();
                                graficoVO.setEventoInicio(getNomeDoEvento(ultimoEvento.getTipoEvento().getId()));
                                graficoVO.setDataEvento(simpleDateFormat.format(ultimoEvento.getInstanteEvento()));
                                graficoVO.setEventoFinal(getNomeDoEvento(evento.getTipoEvento().getId()));
                                graficoVO.setDataEventoFinal(simpleDateFormat.format(evento.getInstanteEvento()));
                                graficoVO.setTempoEntreEventos(TimeHelper.converteMinutosEmStringFormatada(v));
                                listaGrafico.add(graficoVO);
                            }
                        }
                        ultimoEvento = evento;
                    }
                }
            }

            detalheGraficoVO.setTotalDeHoras(TimeHelper.converteMinutosEmStringFormatada(tempoTotal));
            detalheGraficoVO.setListaDeLocalGrafico(listaGrafico);
        } catch (Exception e) {
            LogSistema.logError("calcularOsiosidade", e);
        }
        return detalheGraficoVO;
    }

    private Integer getIdDoEvento(String nome) {
        switch (nome) {
            case "Fiscalização":
                return 13;
            case "Descarga":
                return 12;
            case "Aguardando Descarga":
                return 11;
            case "Carga":
                return 10;
            case "Aguardando Carga":
                return 9;
            case "Abastecimento":
                return 7;
            case "Banheiro":
                return 6;
            case "Lanche":
                return 5;
            case "Manutenção":
                return 3;
            case "Manifesto":
                return 2;
            case "Refeição":
                return 14;
            case "Descanso":
                return 15;
            case "Folga":
                return 21;
            case "Feriado":
                return 22;
            case "DSR":
                return 20;
            case "Falta":
                return 19;
            case "Ferias":
                return 18;
            case "Abono":
                return 17;
            case "Pista Interrompida":
                return 23;
            case "Sinistro":
                return 4;
            case "Abono Refeição":
                return 24;
            case "Intervalor Pessoal":
                return 25;
            case "Interjornada":
                return 26;
            case "Fim Jornada":
                return 0;
            case "Inicio Jornada":
                return 1;
            case "Direção":
                return 8;
        }
        return null;
    }


    public String gerarExcel(Faturamento faturamento) {
        try {
            //preparar os dados
            //ordenar os eventos por data
            List<TimelineFaturamento> listaTimelineFaturamento = faturamento.getListaTimelineFaturamento();
            Collections.sort(listaTimelineFaturamento, new Comparator<TimelineFaturamento>() {
                public int compare(TimelineFaturamento o1, TimelineFaturamento o2) {
                    if (o1.getStart() == null || o2.getStart() == null)
                        return 0;
                    return o1.getStart().compareTo(o2.getStart());
                }
            });


            XSSFWorkbook workbook = new XSSFWorkbook();
            Sheet sheet = workbook.createSheet("Timeline");
            sheet.addMergedRegion(new CellRangeAddress(0, 0, 2, 5));
            sheet.autoSizeColumn(15);

            Font newFont = workbook.createFont();
            newFont.setFontHeightInPoints((short) (12));
            newFont.setItalic(false);


            CellStyle headerStyle = workbook.createCellStyle();
            headerStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
            headerStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
            headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
            headerStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
            headerStyle.setFont(newFont);

            Row rowPlaca = sheet.createRow(0);
            Cell cellPlaca = rowPlaca.createCell(0);
            cellPlaca.setCellValue(faturamento.getPlacaDoVeiculo());
            cellPlaca.setCellStyle(headerStyle);


            Row rowMotorista = sheet.createRow(2);
            Cell cellMotorista = rowPlaca.createCell(2);
            cellMotorista.setCellValue(faturamento.getNomeDoMotorista());
            cellMotorista.setCellStyle(headerStyle);

            int rowNumData = 2;
            int cellNumData = 0;

            int rowNumTitulo = 1;
            int cellNumTitulo = 1;

            int cellNumTempo = 1;

            int rowNumEventoMotorista = 3;
            int cellNumEventoMotorista = 1;


            Locale local = new Locale("pt", "BR");
            SimpleDateFormat simpleDateFormatDia = new SimpleDateFormat("dd/MMMM", local);
            SimpleDateFormat simpleDateFormatTempo = new SimpleDateFormat("HH:mm");
            SimpleDateFormat sd = new SimpleDateFormat("dd/MM HH:mm:ss");


            Row rowData = null;
            Cell cellData = null;
            Row rowTitulo = null;
            Row rowEventoMotorista = null;

            boolean isTrocarDeColuna = true;
            boolean isPrimeiro = true;


            for (TimelineFaturamento timelineFaturamento : listaTimelineFaturamento) {

                if ("Faturando".equalsIgnoreCase(timelineFaturamento.getCategory())) {
                    continue;
                }

                if (!isEventoMotorista(timelineFaturamento)) {
                    XSSFCellStyle tituloStyle = ajustarCor(workbook, timelineFaturamento.getColor());
                    //Aqui começa uma nova coluna para colocar

                    boolean isMesmoDia = TimeHelper.isSameDay(new Date(timelineFaturamento.getStart()), new Date(timelineFaturamento.getEnd()));
                    if (isMesmoDia) {
                        if (isPrimeiro) {
                            rowData = sheet.createRow(rowNumData);
                            cellData = rowData.createCell(cellNumData);
                            cellData.setCellValue(simpleDateFormatDia.format(new Date(timelineFaturamento.getStart())));
                            //Mesmo dia
                            rowTitulo = sheet.createRow(rowNumTitulo);
                            rowEventoMotorista = sheet.createRow(rowNumEventoMotorista);
                            isPrimeiro = false;
                        }

                        Cell cellTituloInicio = rowTitulo.createCell(cellNumTitulo++);
                        cellTituloInicio.setCellStyle(tituloStyle);
                        cellTituloInicio.setCellValue("Inicio " + timelineFaturamento.getNomeEvento());


                        Cell cellTituloFim = rowTitulo.createCell(cellNumTitulo++);
                        cellTituloFim.setCellStyle(tituloStyle);
                        cellTituloFim.setCellValue("Fim " + timelineFaturamento.getNomeEvento());

                        Cell cellTituloTotal = rowTitulo.createCell(cellNumTitulo++);
                        cellTituloTotal.setCellStyle(tituloStyle);
                        cellTituloTotal.setCellValue("Total");

                        Cell cellTempoInicio = rowData.createCell(cellNumTempo++);
                        cellTempoInicio.setCellValue(simpleDateFormatTempo.format(new Date(timelineFaturamento.getStart())));

                        Cell cellTempoFim = rowData.createCell(cellNumTempo++);
                        cellTempoFim.setCellValue(simpleDateFormatTempo.format(new Date(timelineFaturamento.getEnd())));

                        int minuto = Minutes.minutesBetween(new DateTime(timelineFaturamento.getStart()), new DateTime(timelineFaturamento.getEnd())).getMinutes();
                        Cell cellTempoTotal = rowData.createCell(cellNumTempo++);
                        cellTempoTotal.setCellValue(TimeHelper.converteMinutosEmStringFormatada(minuto));


                        Cell cellEventoMotorista = rowEventoMotorista.createCell(cellNumEventoMotorista++);
                        cellEventoMotorista.setCellValue("Carregado");

                    } else {

                        Cell cellTituloInicio = rowTitulo.createCell(cellNumTitulo++);
                        cellTituloInicio.setCellStyle(tituloStyle);
                        cellTituloInicio.setCellValue("Inicio " + timelineFaturamento.getNomeEvento());

                        Cell cellTempoInicio = rowData.createCell(cellNumTempo++);
                        cellTempoInicio.setCellValue(simpleDateFormatTempo.format(new Date(timelineFaturamento.getStart())));

                        rowNumData = rowNumData + 3;
                        rowNumTitulo = rowNumTitulo + 3;
                        rowNumEventoMotorista = rowNumEventoMotorista + 3;
                        cellNumTitulo = 1;
                        cellNumTempo = 1;
                        cellNumEventoMotorista = 1;
                        rowData = sheet.createRow(rowNumData);
                        cellData = rowData.createCell(cellNumData);
                        cellData.setCellValue(simpleDateFormatDia.format(new Date(timelineFaturamento.getEnd())));
                        rowTitulo = sheet.createRow(rowNumTitulo);
                        rowEventoMotorista = sheet.createRow(rowNumEventoMotorista);


                        Cell cellTituloFim = rowTitulo.createCell(cellNumTitulo++);
                        cellTituloFim.setCellStyle(tituloStyle);
                        cellTituloFim.setCellValue("Fim " + timelineFaturamento.getNomeEvento());

                        Cell cellTituloTotal = rowTitulo.createCell(cellNumTitulo++);
                        cellTituloTotal.setCellStyle(tituloStyle);
                        cellTituloTotal.setCellValue("Total");

                        Cell cellTempoFim = rowData.createCell(cellNumTempo++);
                        cellTempoFim.setCellValue(simpleDateFormatTempo.format(new Date(timelineFaturamento.getEnd())));

                        int minuto = Minutes.minutesBetween(new DateTime(timelineFaturamento.getStart()), new DateTime(timelineFaturamento.getEnd())).getMinutes();
                        Cell cellTempoTotal = rowData.createCell(cellNumTempo++);
                        cellTempoTotal.setCellValue(TimeHelper.converteMinutosEmStringFormatada(minuto));


                        Cell cellEventoMotorista = rowEventoMotorista.createCell(cellNumEventoMotorista++);
                        cellEventoMotorista.setCellValue("Carregado");

                    }
                }
            }

            SimpleDateFormat formatador = new SimpleDateFormat("dd_MM_yyyy_HH_mm");
            String data = formatador.format(new Date());
            String nomeRelatorio = "timeline" + "_" + data + ".xlsx";
            String diretorioGravacao = Constantes.DIRETORIO_TOMCAT_RELATORIO;
            String diretorioGravacaoExcel = diretorioGravacao + nomeRelatorio;
            FileOutputStream out =
                    new FileOutputStream(new File(diretorioGravacaoExcel));
            workbook.write(out);
            out.close();
            return Constantes.URL_SERVIDOR_RELATORIO + nomeRelatorio;

        } catch (Exception e) {
            LogSistema.logError("gerarExcel", e);
        }
        return "";
    }
    /*

        public static void main(String[] args) {
            try {
                XSSFWorkbook workbook = new XSSFWorkbook();
                Sheet sheet = workbook.createSheet("Timeline");
                sheet.addMergedRegion(new CellRangeAddress(0, 0, 2, 5));
                sheet.autoSizeColumn(15);


                Font newFont = workbook.createFont();

                newFont.setFontHeightInPoints((short) (12));
                newFont.setItalic(false);
    //
                System.out.println("#39B54A".substring(1));
                String rgbS = "39B54A";
                byte[] rgbB = Hex.decodeHex(rgbS);
                XSSFColor color = new XSSFColor(rgbB);
                XSSFCellStyle cellStyle = (XSSFCellStyle) workbook.createCellStyle();
                cellStyle.setFillForegroundColor(color);
                cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

                CellStyle headerStyle = workbook.createCellStyle();

                headerStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
                headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
                headerStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
                headerStyle.setFont(newFont);

                Row rowPlaca = sheet.createRow(0);
                Cell cellPlaca = rowPlaca.createCell(0);
                cellPlaca.setCellValue("QQM7636");
                cellPlaca.setCellStyle(cellStyle);


                Row rowMotorista = sheet.createRow(2);
                Cell cellMotorista = rowPlaca.createCell(2);
                cellMotorista.setCellValue("Vinicius Castro");
                cellMotorista.setCellStyle(headerStyle);

                int rowNumData = 2;
                int cellNumData = 0;

                int rowNumTitulo = 1;
                int cellNumTitulo = 1;

                int cellNumTempo = 1;

                int rowNumEventoMotorista = 3;
                int cellNumEventoMotorista = 1;

                for (int a = 0; a < 5; a++) {
                    Row rowData = sheet.createRow(rowNumData);
                    Cell cellData = rowData.createCell(cellNumData);
                    rowNumData = rowNumData + 3;
                    cellData.setCellValue("01/mar");
                    //Mesmo dia
                    Row rowTitulo = sheet.createRow(rowNumTitulo);
                    Row rowEventoMotorista = sheet.createRow(rowNumEventoMotorista);
                    for (int b = 0; b < 9; b++) {
                        Cell cellTitulo = rowTitulo.createCell(cellNumTitulo++);
                        cellTitulo.setCellValue("Titulo");

                        Cell cellTempo = rowData.createCell(cellNumTempo++);
                        cellTempo.setCellValue("12:12");

                        Cell cellEventoMotorista = rowEventoMotorista.createCell(cellNumEventoMotorista++);
                        cellEventoMotorista.setCellValue("Carregado");

                    }
                    rowNumTitulo = rowNumTitulo + 3;
                    rowNumEventoMotorista = rowNumEventoMotorista + 3;
                    cellNumTitulo = 1;
                    cellNumTempo = 1;
                    cellNumEventoMotorista = 1;

                }

                FileOutputStream out =
                        new FileOutputStream(new File("C:\\temp\\teste.xlsx"));
                workbook.write(out);
                out.close();
                System.out.println("Arquivo Excel criado com sucesso!");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


            public static HSSFColor setColor(HSSFWorkbook workbook, byte r, byte g, byte b) {
                HSSFPalette palette = workbook.getCustomPalette();
                HSSFColor hssfColor = null;
                try {
                    hssfColor = palette.findColor(r, g, b);
                    if (hssfColor == null) {
                        palette.setColorAtIndex(HSSFColor.LAVENDER.index, r, g, b);
                        hssfColor = palette.getColor(HSSFColor.LAVENDER.index);
                    }
                } catch (Exception e) {
                    LogSistema.logError("setColor", e);
                }

                return hssfColor;
            }

        */
    public XSSFCellStyle ajustarCor(XSSFWorkbook workbook, String cor) {

        XSSFCellStyle tituloStyle = (XSSFCellStyle) workbook.createCellStyle();
        try {
            Font newFontTitulo = workbook.createFont();
            //newFontTitulo.setBold(false);
            newFontTitulo.setFontHeightInPoints((short) (10));
            newFontTitulo.setItalic(false);
            String rgbS = cor.substring(1);
            byte[] rgbB = Hex.decodeHex(rgbS);
            XSSFColor color = new XSSFColor(rgbB);
            tituloStyle.setFillForegroundColor(color);
            tituloStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            tituloStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
            tituloStyle.setAlignment(CellStyle.ALIGN_CENTER);
            tituloStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
            tituloStyle.setFont(newFontTitulo);
            tituloStyle.setFillForegroundColor(color);
        } catch (Exception e) {
            LogSistema.logError("setColor", e);
        }
        return tituloStyle;
    }


    private boolean isEventoMotorista(TimelineFaturamento timelineFaturamento) {

        if (Objects.equals(timelineFaturamento.getCategory(), "Carregado")) {
            return true;
        }

        if (Objects.equals(timelineFaturamento.getCategory(), "Vazio")) {
            return true;
        }

        return false;
    }
}
