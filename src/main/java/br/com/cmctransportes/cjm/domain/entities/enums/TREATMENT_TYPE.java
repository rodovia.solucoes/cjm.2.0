package br.com.cmctransportes.cjm.domain.entities.enums;

/**
 * @author William Leite
 */
public enum TREATMENT_TYPE {
    MANUAL_DSR_CREATION,
    MARK_DSR_HE,
    NO_TREATMENT_AVAILABLE
}