package br.com.cmctransportes.cjm.domain.entities.vo;

import java.util.HashMap;

/**
 * @author William Leite
 */
public class FunctionParam extends HashMap<String, Object> {

    /**
     * @param <T>
     * @param key
     * @return
     */
    public <T> T retrieve(Object key) {
        return (T) super.get(key);
    }
}
