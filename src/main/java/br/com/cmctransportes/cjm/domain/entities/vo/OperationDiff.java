package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * @author William Leite
 */
public class OperationDiff implements Serializable {

    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private List<NonconformityVO> before;
    @Getter
    @Setter
    private List<NonconformityVO> after;
}
