package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author William Leite
 */
public class CalendarDayVO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private Integer day;
    @Getter
    @Setter
    private Boolean weekTurnDay;
    @Getter
    @Setter
    private Integer weekDay;
    @Getter
    @Setter
    private CalendarJourneyVO journey;
}