package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * @author William Leite
 */
public class CalendarEventVO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private Integer id;
    @Getter
    @Setter
    private Date instanteEvento;
    @Getter
    @Setter
    private Integer type;
    @Getter
    @Setter
    private String typeDescription;
    @Getter
    @Setter
    private String color;
    @Getter
    @Setter
    private String icone;
    @Getter
    @Setter
    private String placa;
    @Getter
    @Setter
    private String justificativa;
    @Getter
    @Setter
    private Integer position;
    @Getter
    @Setter
    private Integer fimEventoId;
    @Getter
    @Setter
    private Integer aberturaId;
    @Getter
    @Setter
    private Double latitude;
    @Getter
    @Setter
    private Double longitude;
    @Getter
    @Setter
    private String location;
    @Getter
    @Setter
    private Boolean locked;
}