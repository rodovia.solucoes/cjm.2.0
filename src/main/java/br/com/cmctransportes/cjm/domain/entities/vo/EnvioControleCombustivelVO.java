package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.ControleCombustivel;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class EnvioControleCombustivelVO implements Serializable {
    private List<ControleCombustivel> listaControleCombustivel;
    private List<Integer> listaIdExcluir;
}
