package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RetornoHodometroVO extends RetornoVO {

    private List<HodometroVO> listaDeHodometro;
    private HodometroVO hodometro;
    private String valor;
}
