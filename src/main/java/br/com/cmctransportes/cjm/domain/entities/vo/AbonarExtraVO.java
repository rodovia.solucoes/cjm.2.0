package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.Jornada;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class AbonarExtraVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Jornada jornada;
    private String justificativa;
    private Integer idUsuarioLogado;

}
