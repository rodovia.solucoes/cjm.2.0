package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.constants.Constantes;
import br.com.cmctransportes.cjm.domain.entities.*;
import br.com.cmctransportes.cjm.domain.entities.enums.EVENT_TYPE;
import br.com.cmctransportes.cjm.domain.entities.enums.JOURNEY_TYPE;
import br.com.cmctransportes.cjm.domain.entities.jornada.*;
import br.com.cmctransportes.cjm.domain.entities.vo.*;
import br.com.cmctransportes.cjm.domain.entities.vo.ResumoJornada;
import br.com.cmctransportes.cjm.domain.repository.EventoRepository;
import br.com.cmctransportes.cjm.domain.repository.JourneyRepository;
import br.com.cmctransportes.cjm.infra.dao.*;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.proxy.modelo.Local;
import br.com.cmctransportes.cjm.services.JourneyCalculationConverterService;
import br.com.cmctransportes.cjm.services.JourneyCalculusService;
import br.com.cmctransportes.cjm.services.nonconformity.InterjourneyNonconfomityService;
import br.com.cmctransportes.cjm.services.nonconformity.PaidWeeklyRestNonconformityService;
import br.com.cmctransportes.cjm.utils.LocationUtils;
import br.com.cmctransportes.cjm.utils.RodoviaUtils;
import br.com.cmctransportes.cjm.utils.TimeHelper;
import lombok.Getter;
import org.jfree.data.time.Millisecond;
import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.joda.time.Seconds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.sql.SQLOutput;
import java.sql.Time;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author analista.ti
 * <p>
 * Service Controle de Jornada
 */
@Service
public class JornadaService {

    @Getter
    private JornadaDAO dao;

    private final EntityManager em;

    @Autowired
    private UnidadesService unidadesService;

    @Autowired
    private UserService userService;

    @Autowired
    private EventoService eventoService;

    @Autowired
    private DriverService driverService;

    @Autowired
    private FeriadosService feriadosService;

    @Autowired
    private DsrService dsrService;

    @Autowired
    private FeriasService feriasService;

    @Autowired
    private JourneyCalculusService calculusService;

    @Autowired
    private PontosCargaDescargaService pdcService;

    @Autowired
    private JourneyRepository repository;

    @Autowired
    private PaidWeeklyRestNonconformityService paidWeeklyRestNonconformityService;

    @Autowired
    private InterjourneyNonconfomityService interjourneyNonconfomityService;

    @Autowired
    private EventoRepository eventoRepository;

    @Autowired
    private EventosJornadaService eventosJornadaService;

    @Autowired
    private JustificativaService justificativaService;

    public JornadaService(EntityManager em) {
        this.em = em;
        dao = new JornadaDAO(em);
    }

    /**
     * @param entity
     */
    @Transactional
    public Jornada save(Jornada entity) {
        try {
            if (Objects.isNull(entity.getEmpresaId())) {
                entity.setEmpresaId(new Empresas(1));
            }
            entity = this.repository.saveAndFlush(entity);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return entity;
    }

    /**
     * @return
     */
    public List<Jornada> findAll() {
        return this.repository.findAll();
    }

    /**
     * @param id
     * @return
     */
    public Jornada getById(Integer id) {
        return this.repository.findById(id).get();
    }

    /**
     * @param motorista
     * @return
     */
    public Jornada getLast(Integer motorista) {
        return dao.getLast(motorista);
    }

    /**
     * @param empresa
     * @param unidade
     * @return
     */
    public List<PainelDiarioVO> getAllLast(Integer empresa, Integer unidade) {
        return dao.getAllLast(empresa, unidade, this.calculusService);
    }

    /**
     * @param jornadas
     * @return
     */
    public List<MotoristaVO> findDriversByJourney(String jornadas) {
        List<MotoristaVO> result = new ArrayList<MotoristaVO>(0);
        List<Motoristas> drivers = dao.findDriversByJourney(jornadas);

        for (Motoristas m : drivers) {
            result.add(new MotoristaVO(m, this.userService, this.unidadesService));
        }

        return result;
    }


    public MotoristaVO findDriversByJourney(Integer idJornada) {
        Motoristas m = dao.findDriversByJourney(idJornada);
        if (m != null) {
            return new MotoristaVO(m, this.userService, this.unidadesService);
        }
        return null;
    }

    /**
     * @param entity
     */
    @Transactional
    public void update(Jornada entity) {
        try {
            this.repository.saveAndFlush(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param id
     */
    @Transactional
    public void delete(Integer id) {
        try {
            this.repository.deleteById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param entity
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void delete(Jornada entity) {
        try {
            this.repository.delete(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param driverID
     * @return
     */
    public Map<Integer, List<MonthVO>> period(Integer driverID) {
        Map<Integer, List<MonthVO>> result = new HashMap<>(0);

        JornadaDAO journeyDAO = new JornadaDAO(this.em);
        MotoristasDAO driverDAO = new MotoristasDAO(this.em);
        Motoristas driver = driverDAO.getById(driverID);
        UnidadesDAO unidadeDAO = new UnidadesDAO(this.em);
        Unidades unidade = unidadeDAO.getById(driver.getUnidadesId());

        Integer startDay = unidade.getDiaInicioApuracao();
        Integer endDay = unidade.getDiaFimApuracao();

        Calendar cal = Calendar.getInstance();

        List<Object[]> dbList = journeyDAO.findPeriods(driverID);
        dbList.stream().forEach(l -> {
            Integer month = ((Number) l[0]).intValue();
            Integer year = ((Number) l[1]).intValue();

            List<MonthVO> months = result.get(year);
            if (Objects.isNull(months)) {
                months = new ArrayList<>(0);
            }

            MonthVO vo = new MonthVO();
            /**Redefine o dia do mês, para evitar o salto do mês quando o último final for maior que o atual
             * Resets the day of month, to avoid month-jumping when the last end is greater than the current one
             *
             */

            cal.set(Calendar.DAY_OF_MONTH, 1);

            cal.set(Calendar.YEAR, year);
            cal.set(Calendar.MONTH, month - 1);

            vo.setName(cal.getDisplayName(Calendar.MONTH, Calendar.LONG_FORMAT, Locale.forLanguageTag("pt-BR")));

            if (startDay < endDay) {
                cal.set(Calendar.DAY_OF_MONTH, startDay);

                vo.setStart(cal.getTime());

                if (cal.getActualMaximum(Calendar.DAY_OF_MONTH) < endDay) {
                    cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
                } else {
                    cal.set(Calendar.DAY_OF_MONTH, endDay);
                }

                vo.setEnd(cal.getTime());
            } else {
                if (cal.getActualMaximum(Calendar.DAY_OF_MONTH) < endDay) {
                    cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
                } else {
                    cal.set(Calendar.DAY_OF_MONTH, endDay);
                }

                vo.setEnd(cal.getTime());

                cal.add(Calendar.MONTH, -1);
                cal.set(Calendar.DAY_OF_MONTH, startDay);

                vo.setStart(cal.getTime());
            }

            months.add(vo);
            months.sort(Comparator.comparing(MonthVO::getStart));
            result.put(year, months);
        });

        return result;
    }

    /**
     * @param journey
     * @param instanteEvento
     * @param tipoEvento
     * @param user
     * @return
     */
    private Evento createAllowanceEvent(Jornada journey, Date instanteEvento, Integer tipoEvento, Integer user) {
        Evento evento = new Evento();
        evento.setEmpresaId(journey.getEmpresaId());
        evento.setInstanteEvento(instanteEvento);
        evento.setInstanteLancamento(new Date());
        evento.setInstanteOriginal(instanteEvento);
        evento.setJustificativa("");
        evento.setTipoEvento(new EventosJornada(tipoEvento));
        evento.setOperadorLancamento(new Users(user));
        evento.setJornadaId(journey);
//        evento.setEventoAnterior(base);
        return evento;
    }

    /**
     * @param start
     * @param end
     * @param driver
     * @param userID
     * @return
     */
    @Transactional
    public String closeCalculation(Long start, Long end, Integer driver, Integer userID) {
        String result = "";
        JornadaDAO journeyDAO = new JornadaDAO(this.em);
        EventoDAO eventoDAO = new EventoDAO(this.em);
        UsersDAO userDAO = new UsersDAO(this.em);
        JourneyCalculationDAO calculationDAO = new JourneyCalculationDAO(this.em);
        JourneyCalculationConverterService converter = new JourneyCalculationConverterService();

        Date startDate = TimeHelper.getDate000000(new Date(start));
        Date endDate = TimeHelper.getDate235959(new Date(end));

        JourneyCalculation calculation = calculationDAO.findByStartDateAndEndDateAndDriverId(startDate, endDate, driver);
        Users user = userDAO.getById(userID);
        if (Objects.isNull(calculation) || user.getUsersClaims().stream().anyMatch(c -> Objects.equals(c.getClaim(), "apuracao"))) {
            /**Remover calculo anterior
             * Remove the previous calculation
             *
             */

            if (Objects.nonNull(calculation)) {
                calculationDAO.delete(calculation);
            }
            /**Criar calculo
             *
             */
            //create calculation
            RelatorioApuracaoDeJornada apuracao = this.getApuracaoDeJornada(driver, start, end, true);
            /**Converter
             *
             */
            // converts it
            calculation = converter.convert(apuracao, user);
            /**Salvar
             *
             */
            // saves it
            calculationDAO.save(calculation);
            /**Agora boqueia todos eventos
             *
             */
            // now we lock all events
            List<Jornada> journeys = dao.getRelatorioApuracaoDeJornada(driver, startDate, endDate);
            journeys.stream().forEach(j -> {
                j.getEventos().forEach(ev -> {
                    ev.setLocked(true);
                    this.eventoRepository.saveAndFlush(eventoDAO.address(ev));
                });

                j.setLocked(true);
                journeyDAO.update(j);
            });
        } else {
            result = "Já existe uma apuração nesse período!";
        }

        return result;
    }

    /**
     * @param journey
     * @param i
     * @return
     */
    public Evento pegaEventoPorTipo(Jornada journey, final int i) {
        if (Objects.isNull(journey)) {
            return null;
        }

        return journey.getEventos().stream()
                .filter(e -> Objects.nonNull(e.getTipoEvento()))
                .filter(e -> Objects.equals(e.getTipoEvento().getId(), i))
                .findFirst()
                .orElse(null);
    }

    /**
     * @param id
     * @param user
     * @return
     */
    public String fillJourney(Integer id, Integer user) {
        return fillJourney(id, user, true);
    }

    /**
     * @param id
     * @param user
     * @param ehHoraFaltosa
     * @return
     */
    public String fillJourney(Integer id, Integer user, boolean ehHoraFaltosa) {
        String result = "";

        Jornada jornada = this.getById(id);
        this.calculusService.processEvents(jornada, null, false);

        Long tmpFaltosas = 0L;
        Integer tipoEventoIni = 17;
        Integer tipoEventoFim = -17;

        if (ehHoraFaltosa) {
            tmpFaltosas = jornada.getHorasFaltosas();
        } else {
            tmpFaltosas = jornada.getDescontoAlmoco();
            tipoEventoIni = 24;
            tipoEventoFim = -24;
        }

        if (tmpFaltosas > 0) {
            Optional<Evento> opFimJornada = jornada.getEventos().stream().filter(e -> e.getInstanteEvento().equals(jornada.getJornadaFim())).findFirst();
            if (opFimJornada.isPresent()) {
                Evento fimJornada = opFimJornada.get();
                LocalDateTime startDate, endDate;

                // ID 0 == Fim Jornada
                if (fimJornada.getTipoEvento().getId() != 0) {
                    startDate = TimeHelper.toLocalDateTime(jornada.getJornadaInicio());
                    endDate = startDate.plus(tmpFaltosas, ChronoUnit.MILLIS).plusSeconds(1);
                    this.eventoService.deleteChained(fimJornada.getEventoAnteriorId(), fimJornada.getId());
                } else {
                    startDate = TimeHelper.toLocalDateTime(fimJornada.getInstanteEvento()).plusSeconds(1);
                    Long tmpAlowanceTime = tmpFaltosas;
                    endDate = startDate.plus(tmpAlowanceTime, ChronoUnit.MILLIS);
                }

                Evento inicio = this.createAllowanceEvent(jornada, TimeHelper.fromLocalDateTime(startDate), tipoEventoIni, user);
                this.eventoService.saveWithOutReOrder(inicio, false); //Chained(inicio, fim, false);
//                endDate = endDate.minusSeconds(1);
                Evento fim = this.createAllowanceEvent(jornada, TimeHelper.fromLocalDateTime(endDate), tipoEventoFim, user);
                this.eventoService.saveWithOutReOrder(fim, false);
            } else {
                result = "Jornada não possui fim.";
            }
        } else {
            result = "Jornada não possui horas faltosas";
        }

        return result;
    }

    /**
     * @param motorista
     * @param mesReferencia
     * @return
     */
    public RelatorioRegistroDeJornadas getRegistroDeJornada(Integer motorista, Integer mesReferencia) {

        RelatorioRegistroDeJornadas rdj = new RelatorioRegistroDeJornadas(mesReferencia);

        rdj.setJornadas(dao.getRelatorioDeJornada(motorista, rdj.getDataInicio(), rdj.getDataFim()),
                this.calculusService, this.paidWeeklyRestNonconformityService, this.interjourneyNonconfomityService,
                this.eventosJornadaService);

        return rdj;
    }

    /**
     * @param motorista
     * @param dtInicio
     * @param dtFim
     * @return
     */
    public RelatorioRegistroDeJornadas getRegistroDeJornada(Integer motorista, Long dtInicio, Long dtFim) {
        Motoristas driver = this.driverService.getById(motorista);

        Date dataInicio = TimeHelper.getDate000000(new Date(dtInicio));
        Date dataFim = RodoviaUtils.resignationLimit(new Date(dtFim), driver);

        RelatorioRegistroDeJornadas rdj = new RelatorioRegistroDeJornadas(dataInicio, dataFim);

        acertaJornada(dataInicio, dataFim, motorista);

        rdj.setJornadas(dao.getRelatorioDeJornada(motorista, rdj.getDataInicio(), rdj.getDataFim()),
                this.calculusService, this.paidWeeklyRestNonconformityService, this.interjourneyNonconfomityService,
                this.eventosJornadaService);

        return rdj;
    }

    /**
     * @param mesReferencia
     * @return
     */
    public List<RelatorioRegistroDeJornadas> getRegistroDeJornada(Integer mesReferencia) {
        List<RelatorioRegistroDeJornadas> lrdj = new ArrayList<RelatorioRegistroDeJornadas>();
        List<Jornada> listaJornadas = null;

        RelatorioRegistroDeJornadas rdj = new RelatorioRegistroDeJornadas(mesReferencia);

        List<Jornada> jornadas = dao.getRelatorioDeJornada(rdj.getDataInicio(), rdj.getDataFim());

        Motoristas motoristaAtual = null;
        /**para cada jornada
         *
         */
        for (Jornada jornada : jornadas) {
            /**se for outro motorista, adiciona o relatorio anterior e cria um novo
             *
             */
            if (!jornada.getMotoristasId().equals(motoristaAtual)) {
                motoristaAtual = jornada.getMotoristasId();
                if (listaJornadas != null) {
                    lrdj.add(new RelatorioRegistroDeJornadas(mesReferencia, listaJornadas, this.calculusService,
                            this.paidWeeklyRestNonconformityService, this.interjourneyNonconfomityService, this.eventosJornadaService));
                }
                listaJornadas = new ArrayList<Jornada>();
            }

            listaJornadas.add(jornada);
        }

        if (listaJornadas != null) {
            lrdj.add(new RelatorioRegistroDeJornadas(mesReferencia, listaJornadas, this.calculusService,
                    this.paidWeeklyRestNonconformityService, this.interjourneyNonconfomityService, this.eventosJornadaService));
        }

        return lrdj;
    }

    /**
     * @param motorista
     * @param dataInicio
     * @param dataFim
     * @return
     */
    public RelatorioApuracaoDeJornada getApuracaoDeJornadaInconformidades(Integer motorista, Long dataInicio, Long dataFim) {

        Date dataInicial = TimeHelper.getDate000000(new Date(dataInicio));
        Date dataFinal = TimeHelper.getDate235959(new Date(dataFim));

        RelatorioApuracaoDeJornada rdj = new RelatorioApuracaoDeJornada(dataInicial, dataFinal);

        rdj.setJornadas(dao.getRelatorioDeJornada(motorista, dataInicial, dataFinal), this.calculusService,
                this.paidWeeklyRestNonconformityService, this.interjourneyNonconfomityService, this.eventosJornadaService);

        return rdj;
    }

    /**
     * @param motorista
     * @param dataInicio
     * @param dataFim
     * @return
     */
    public RelatorioApuracaoDeJornada getApuracaoDeJornada(Integer motorista, Long dataInicio, Long dataFim) {
        Motoristas driver = this.driverService.getById(motorista);

        Date dataInicial = TimeHelper.getDate000000(new Date(dataInicio));
        Date dataFinal = RodoviaUtils.resignationLimit(new Date(dataFim), driver);

        RelatorioApuracaoDeJornada rdj = new RelatorioApuracaoDeJornada(dataInicial, dataFinal);
        /**cria eventos automáticos na jornada
         *
         */
        acertaJornada(dataInicial, dataFinal, motorista);
        /**seta as jornadas já com as faltas
         *
         */
        List<Jornada> result = dao.getRelatorioApuracaoDeJornada(motorista, dataInicial, dataFinal);

        rdj.setJornadas(result, this.calculusService, this.paidWeeklyRestNonconformityService,
                this.interjourneyNonconfomityService, this.eventosJornadaService);

        return rdj;
    }

    /**
     * @param driver
     * @param start
     * @param end
     * @param nonconformity
     * @return
     */
    public RelatorioApuracaoDeJornada assembleCalculationReport(Integer driver, Long start, Long end, boolean nonconformity) {
        JourneyCalculationDAO calculationDAO = new JourneyCalculationDAO(this.em);
        JourneyCalculationConverterService converter = new JourneyCalculationConverterService();

        Date startDate = TimeHelper.getDate000000(new Date(start));
        Date endDate = TimeHelper.getDate235959(new Date(end));

        JourneyCalculation calculation = calculationDAO.findByStartDateAndEndDateAndDriverId(startDate, endDate, driver);
        if (Objects.nonNull(calculation)) {
            return converter.convert(calculation);
        } else {
            return this.getApuracaoDeJornada(driver, start, end, nonconformity);
        }
    }

    /**
     * @param motorista
     * @param dataInicio
     * @param dataFim
     * @param nonconformity
     * @return
     */
    private Evento eventoInicioRefeicao = null;

    private RelatorioApuracaoDeJornada getApuracaoDeJornada(Integer motorista, Long dataInicio, Long dataFim, boolean nonconformity) {
        Motoristas driver = this.driverService.getById(motorista);

        Date dataInicial = TimeHelper.getDate000000(new Date(dataInicio));
        Date dataFinal = RodoviaUtils.resignationLimit(new Date(dataFim), driver);

        RelatorioApuracaoDeJornada rdj = new RelatorioApuracaoDeJornada(dataInicial, dataFinal, nonconformity);
        /**cria eventos automáticos na jornada
         *
         */
        acertaJornada(dataInicial, dataFinal, motorista);
        /**seta as jornadas já com as faltas
         *
         */
        List<Jornada> result = dao.getRelatorioApuracaoDeJornada(motorista, dataInicial, dataFinal);
        try {
            //Ordernar lista pela data de inicio
            Collections.sort(result, new Comparator<Jornada>() {
                @Override
                public int compare(Jornada u1, Jornada u2) {
                    return u1.getJornadaInicio().compareTo(u2.getJornadaInicio());
                }
            });
        } catch (Exception e) {
        }

        //Retirar faltas quando esta no mesmo dia
        Jornada ultimaJornadaProcessada = null;
        List<Integer> listaExcluir = new ArrayList<>();
        for (Jornada resumoJornada : result) {
            if(Objects.isNull(ultimaJornadaProcessada)){
                ultimaJornadaProcessada = resumoJornada;
            }else{
                if(JOURNEY_TYPE.FALTA.equals(resumoJornada.getType())||JOURNEY_TYPE.FALTA.equals(ultimaJornadaProcessada.getType())){
                    if (TimeHelper.isSameDay(resumoJornada.getJornadaInicio(), ultimaJornadaProcessada.getJornadaInicio())) {
                        if(JOURNEY_TYPE.FALTA.equals(resumoJornada.getType())){
                            listaExcluir.add(resumoJornada.getId());
                        }else{
                            listaExcluir.add(ultimaJornadaProcessada.getId());
                        }
                    }else{
                        ultimaJornadaProcessada = resumoJornada;
                    }
                }
            }
        }

        if (!listaExcluir.isEmpty()) {
            for (Integer id : listaExcluir) {
                result.removeIf(e -> e.getId().equals(id));
            }
        }


        rdj.setJornadas(result, this.calculusService, this.paidWeeklyRestNonconformityService,
                this.interjourneyNonconfomityService, this.eventosJornadaService);

        //REMOVER A BOSTA DA FALTA
        //QUE GAMBIARRA!
        rdj.removerFalta();

        if (Objects.nonNull(driver.getObjEmpresa().getParametro().getRelatorioResumoJornada()) && driver.getObjEmpresa().getParametro().getRelatorioResumoJornada()) {
            List<br.com.cmctransportes.cjm.domain.entities.jornada.ResumoJornada> lista = apuracaoDeResumoJornada(result);
            rdj.setListaDeResumoDeJornada(lista);
        }



        if (Objects.nonNull(driver.getObjEmpresa().getParametro().getHabilitarRelatorioDetalheApuracao()) && driver.getObjEmpresa().getParametro().getHabilitarRelatorioDetalheApuracao()) {
            List<DetalheDaJornada> l = processarDetalheJornada(result, rdj.getLinhas());
            for (DetalheDaJornada detalheDaJornada : l) {
                if (Objects.nonNull(driver.getNumeroMatricula()) && !driver.getNumeroMatricula().isEmpty()) {
                    detalheDaJornada.setChapa(driver.getNumeroMatricula());
                }
            }
            rdj.setListaDetalheDaJornada(l);
        }





        return rdj;
    }

    /**
     * @param motorista
     * @param mesReferencia
     * @return
     */
    public RelatorioApuracaoDeJornada getApuracaoDeJornada(Integer motorista, Integer mesReferencia) {
        RelatorioApuracaoDeJornada rdj = new RelatorioApuracaoDeJornada(mesReferencia);

        rdj.setJornadas(dao.getRelatorioApuracaoDeJornada(motorista, rdj.getDataInicio(), rdj.getDataFim()),
                this.calculusService, this.paidWeeklyRestNonconformityService, this.interjourneyNonconfomityService,
                this.eventosJornadaService);

        return rdj;
    }

    /**
     * Updates every event in the journey's collection with its "localEventoEspera" based on the event's "latitude" and "longitude".
     *
     * @param journey Journey to analyze
     */
    private int i = 0;

    public void fixEventLocation(Jornada journey) {
        Set<LocalVO> locations = new HashSet<>();
        List<LocalVO> listaLocalSemRaio = new ArrayList<>();
        List<LocalVO> listaDeLocal = pdcService.findAllAsLocal(journey.getMotoristasId().getEmpresaId());

        for (LocalVO localVO : listaDeLocal) {
            List<CoordenadaVO> ll = localVO.getListaDeCoordenadas().stream().filter(c -> c.getRaio() != null && c.getIsRaio()).collect(Collectors.toList());
            if (ll != null && !ll.isEmpty()) {
                locations.add(localVO);
            } else {
                listaLocalSemRaio.add(localVO);
            }
        }


        final LocationUtils util = new LocationUtils();
        journey.getEventos().stream().forEach(vo -> {
            if (!locations.isEmpty()) {
                LocalVO local = new LocalVO(0, "", vo.getLatitude(), vo.getLongitude(), false);
                Optional<LocalVO> closest = util.closest(local, locations);
                if (closest.isPresent()) {
                    /**verifica se a localidade encontada é próxima o suficiente
                     *
                     */

                    LocalVO localVO = closest.get();
                    CoordenadaVO coordenadaVO = localVO.getListaDeCoordenadas().get(0);
                    if (util.closeEnough(vo.getLatitude(), coordenadaVO.getLatitude(), vo.getLongitude(), coordenadaVO.getLongitude(), coordenadaVO.getRaio())) {
                        vo.setIdLocalEventoEspera(closest.get().getId());
                        PontosDeEspera pontosDeEspera = new PontosDeEspera();
                        pontosDeEspera.setIdentificacao(closest.get().getNome());
                        pontosDeEspera.setPermiteDescanso(closest.get().isPermiteDescanso());
                        vo.setLocalEventoEspera(pontosDeEspera);
                    }
                }
            }
            if (!listaLocalSemRaio.isEmpty()) {
                for (LocalVO localVO : listaLocalSemRaio) {
                    LatLng[] latLngs = localVO.convert();
                    if (vo.getLatitude() != null && vo.getLongitude() != null) {
                        if (util.verificarSeOPontoEstaNaRegiao(vo.getLatitude(), vo.getLongitude(), latLngs)) {
                            vo.setIdLocalEventoEspera(localVO.getId());
                            PontosDeEspera pontosDeEspera = new PontosDeEspera();
                            pontosDeEspera.setIdentificacao(localVO.getNome());
                            pontosDeEspera.setPermiteDescanso(localVO.isPermiteDescanso());
                            vo.setLocalEventoEspera(pontosDeEspera);
                            break;
                        }
                    }
                }
            }

        });
    }

    /**
     * @param mesReferencia
     * @return
     */
    public List<RelatorioApuracaoDeJornada> getApuracaoDeJornada(Integer mesReferencia) {
        List<RelatorioApuracaoDeJornada> lrdj = new ArrayList<>(0);
        List<Jornada> listaJornadas = null;

        RelatorioApuracaoDeJornada rdj = new RelatorioApuracaoDeJornada(mesReferencia);

        List<Jornada> jornadas = dao.getRelatorioDeJornada(rdj.getDataInicio(), rdj.getDataFim());

        if (jornadas == null) {
            return lrdj;
        }

        Motoristas motoristaAtual = null;
        /**para cada jornada, se for outro motorista, adiona o relatorio anterior e cria um novo
         *
         *
         */
        for (Jornada jornada : jornadas) {
            if (!jornada.getMotoristasId().equals(motoristaAtual)) {
                motoristaAtual = jornada.getMotoristasId();
                if (listaJornadas != null) {
                    lrdj.add(new RelatorioApuracaoDeJornada(mesReferencia, listaJornadas, this.calculusService,
                            this.paidWeeklyRestNonconformityService, this.interjourneyNonconfomityService, this.eventosJornadaService));
                }
                listaJornadas = new ArrayList<>(0);
            }

            listaJornadas.add(jornada);
        }

        if (listaJornadas != null) {
            lrdj.add(new RelatorioApuracaoDeJornada(mesReferencia, listaJornadas, this.calculusService,
                    this.paidWeeklyRestNonconformityService, this.interjourneyNonconfomityService, this.eventosJornadaService));
        }

        return lrdj;
    }

    /**
     * @param idEmpresa
     * @param idUnidade
     * @return
     */
    public List<ResumoJornada> getResumosJornadas(Integer idEmpresa, Integer idUnidade) {
        List<ResumoJornada> lrdj = dao.getResumosJornadas(idEmpresa, idUnidade);

        return lrdj;
    }

    /**
     * @param dataInicio
     * @param dataFim
     * @param motorista
     * @return
     */
    public String removeDuplicates(Long dataInicio, Long dataFim, Integer motorista) {

        Date dataInicial = TimeHelper.getDate000000(new Date(dataInicio));
        Date dataFinal = TimeHelper.getDate235959(new Date(dataFim));
        List<Jornada> journeys = dao.getResumosJornadas(dataInicial, dataFinal, motorista);

        Map<JOURNEY_TYPE, Map<String, Object>> journeyTypeByDate = new HashMap<>(0);
        List<Integer> ids = new ArrayList<>(0);
        List<Evento> eventIds = new ArrayList<>(0);

        for (Jornada j : journeys) {
            this.calculusService.processEvents(j, null, false);
        }

        journeys.stream().map((journey) -> {
            this.calculusService.processEvents(journey, null, false);
            return journey;
        }).filter(j -> Objects.nonNull(j.getJornadaInicio()))
                .sorted(Comparator.comparing(Jornada::getJornadaInicio).reversed()).forEach((journey) -> {
            Map<String, Object> obj;


            journey.getEventos().stream()
                    .filter(e -> e.getTipoEvento().getId() == 1)
                    .sorted(Comparator.comparing(Evento::getInstanteEvento))
                    .skip(1)
                    .forEach(eventIds::add);

            if (journeyTypeByDate.containsKey(journey.getType())) {
                obj = journeyTypeByDate.get(journey.getType());
                Long lastDate = ((Date) obj.get("jornadaInicio")).getTime();
                Integer id = (Integer) obj.get("id");

                if (lastDate - (30 * 1000) <= journey.getJornadaInicio().getTime()) {
                    ids.add(id);
                    eventIds.addAll(journey.getEventos());
                }
            } else {
                obj = new HashMap<>(0);
            }

            obj.put("jornadaInicio", journey.getJornadaInicio());
            obj.put("id", journey.getId());
            journeyTypeByDate.put(journey.getType(), obj);
        });

        eventIds.stream().forEach(eventoService::delete);
        ids.forEach(this::delete);
        Date dateFim = new Date();
        return "";
    }

    /**
     * @param id
     * @return
     */
    public ResumoJornada getResumoJornada(Integer id) {
        Jornada jornada = dao.getById(id);
        this.calculusService.processEvents(jornada, null, false);
        return ResumoJornada.convertToResumoJornada(jornada, this.unidadesService);
    }

    /**
     * @param dataInicio
     * @param dataFim
     * @param motorista
     * @return
     */
    public List<ResumoJornada> getResumosJornadas(Long dataInicio, Long dataFim, Integer motorista) {
        Date dataInicial = TimeHelper.getDate000000(new Date(dataInicio));
        Date dataFinal = TimeHelper.getDate235959(new Date(dataFim));

        Motoristas driver = this.driverService.getById(motorista);
        /**Todos os motoristas devem ter uma data de admissão
         *
         */
        // All drivers must have an admission date
        if (Objects.isNull(driver.getDataAdmissao())) {
            throw new RuntimeException("Motorista sem data de admissão!");
        }
        /**Se o motorista tiver uma data de admissão após a inicial, substitua a inicial
         *
         */
        // If the driver has an admission date after the initial, overwrite the initial
        if (driver.getDataAdmissao().after(dataInicial)) {
            dataInicial = TimeHelper.getDate000000(driver.getDataAdmissao());
        }
        /**Se o driver tiver uma data de demissão antes do final, substitua pela final
         *
         */
        // If the driver has a resignation date before the end, overwrite the end
        if (Objects.nonNull(driver.getDataDemissao()) && driver.getDataDemissao().before(dataFinal)) {
            dataFinal = driver.getDataDemissao();
        }

        acertaJornada(dataInicial, dataFinal, motorista);

        List<Jornada> jornadas = dao.getResumosJornadas(dataInicial, dataFinal, motorista);

        jornadas.forEach(j -> this.calculusService.processEvents(j, null, false));

        jornadas = jornadas.stream()
                .filter(j -> Objects.nonNull(j.getJornadaInicio()))
                .sorted(Comparator.nullsLast(Comparator.comparing(Jornada::getJornadaInicio)))
                .collect(Collectors.toList());

        List<ResumoJornada> resumos = jornadas.stream()
                .map(j -> ResumoJornada.convertToResumoJornada(j, this.unidadesService))
                .collect(Collectors.toList());

        //Retirar faltas quando esta no mesmo dia
        //GAMBIARRA NERVOSA

        ResumoJornada ultimaJornadaProcessada = null;
        List<Integer> listaExcluir = new ArrayList<>();
        for (ResumoJornada resumoJornada : resumos) {
            if(Objects.isNull(ultimaJornadaProcessada)){
                ultimaJornadaProcessada = resumoJornada;
            }else{
                if("FALTA".equalsIgnoreCase(resumoJornada.getTipoEvento()) ||"FALTA".equalsIgnoreCase(ultimaJornadaProcessada.getTipoEvento())){
                    if (TimeHelper.isSameDay(resumoJornada.getInicio(), ultimaJornadaProcessada.getInicio())) {
                        if("FALTA".equalsIgnoreCase(resumoJornada.getTipoEvento())){
                            listaExcluir.add(resumoJornada.getId());
                        }else{
                            listaExcluir.add(ultimaJornadaProcessada.getId());
                        }
                    }else{
                        ultimaJornadaProcessada = resumoJornada;
                    }
                }
            }
        }

        if (!listaExcluir.isEmpty()) {
            for (Integer id : listaExcluir) {
                resumos.removeIf(e -> e.getId().equals(id));
            }
        }
        return resumos;
    }

    /**
     * @param dataInicial
     * @param dataFinal
     * @param motoristaId
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private void geraFaltas(Date dataInicial, Date dataFinal, Integer motoristaId) {
        /**se uma destas partes for nula, não funciona
         *
         */
        if (dataInicial == null || dataFinal == null || motoristaId == null) {
            return;
        }
        /**pega o inicio do dia de hoje
         *
         */
        Date zeroHorasHoje = TimeHelper.getDate000000(new Date());
        /**se dataFinal for DEPOIS do início de hoje
         *
         */
        if (dataFinal.after(zeroHorasHoje)) {
            Calendar c = new GregorianCalendar();
            c.setTime(zeroHorasHoje);
            c.add(Calendar.SECOND, -1);
            /**Configura a dataFinal para o final de ontem, não posso lançar falta para o dia corrente!
             *
             *
             */
            dataFinal = c.getTime();
        }
        /**se a dataFinal for ANTES da dataInicial não faz nada
         *
         */
        if (dataFinal.before(dataInicial)) {
            return;
        }

        Motoristas driver = this.driverService.getById(motoristaId);

        if (Objects.isNull(driver.getDataAdmissao())) {
            throw new RuntimeException("Data de admissão não preenchida!");
        }

        List<Jornada> journeys = this.dao.getRelatorioDeJornada(motoristaId, dataInicial, dataFinal);
        //List<Jornada> journeys = this.dao.buscarJornadaNativoFaltas(dataInicial, dataFinal, motoristaId);
        Set<Date> dates = new HashSet<>();
        Set<Date> falta = new HashSet<>();

        journeys.stream().forEach(j -> {
            this.calculusService.processEvents(j, null, false);

            if (j.getType() == JOURNEY_TYPE.DSR) {
                Optional<Evento> dsrStart = j.getEventos().stream().filter(e -> e.getTipoEvento().getId() == EVENT_TYPE.DSR.getStartCode()).findAny();
                Optional<Evento> dsrEnd = j.getEventos().stream().filter(e -> e.getTipoEvento().getId() == EVENT_TYPE.DSR.getEndCode()).findAny();
                if (dsrStart.isPresent() && dsrEnd.isPresent()) {
                    Date end = j.getJornadaFim();
                    if (Objects.isNull(end) || end.before(dsrEnd.get().getInstanteEvento())) {
                        end = dsrEnd.get().getInstanteEvento();
                    }
                    List<Date> between = TimeHelper.getDatesBetween(dsrStart.get().getInstanteEvento(), end);
                    dates.addAll(between);
                }
            } else if (Objects.nonNull(j.getJornadaInicio()) && Objects.nonNull(j.getJornadaFim())) {
                List<Date> between = TimeHelper.getDatesBetween(j.getJornadaInicio(), j.getJornadaFim());
                dates.addAll(between);
            } else {
                dates.add(TimeHelper.getDate000000(j.getJornadaInicio()));
            }
        });

        List<Date> period = TimeHelper.getDatesBetween(dataInicial, dataFinal);
        period.removeAll(dates);

        final Calendar cal = Calendar.getInstance();
        for (Date tmpDate : period) {
            LocalDateTime currentDate = RodoviaUtils.convertToLocalDateTime(tmpDate.getTime()).toLocalDate().atStartOfDay();
            LocalDateTime dataAdmissao = RodoviaUtils.convertToLocalDateTime(driver.getDataAdmissao().getTime());
            /**Somente gera faltas e folgas para datas posteriores a data de admissao
             *
             */
            if (dataAdmissao.compareTo(currentDate) > 0) {
                continue;
            }

            LocalDateTime fim = currentDate.plusHours(23).plusMinutes(59).plusSeconds(59);

            cal.setTime(tmpDate);
            if (Objects.nonNull(driver.getTurnosId().getDiaDeFolga()) && cal.get(Calendar.DAY_OF_WEEK) == (driver.getTurnosId().getDiaDeFolga() + 1)) {
                this.createSingleEventJourney(driver, currentDate, fim, EVENT_TYPE.FOLGA.getStartCode());
            } else {
                this.createSingleEventJourney(driver, currentDate, fim, EVENT_TYPE.FALTA.getStartCode());
            }
        }
    }

    /**
     * @param dataInicial
     * @param dataFinal
     * @param motorista
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void acertaJornada(Date dataInicial, Date dataFinal, Integer motorista) {


        lancaDSR(dataInicial, dataFinal, motorista);
        Date now = new Date();
        if (dataFinal.after(now)) {
            dataFinal = now;
        }
        lancaFerias(dataInicial, dataFinal, motorista);
        lancaFeriados(dataInicial, dataFinal, motorista);
        geraFaltas(dataInicial, dataFinal, motorista);
    }

    /**
     * @param dataInicial
     * @param dataFinal
     * @param motorista
     * @return
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private boolean lancaFeriados(Date dataInicial, Date dataFinal, Integer motorista) {
        boolean lancou = false;
        Motoristas driver = this.driverService.getById(motorista);


        List<Date> dataVazia = this.feriadosService.getDataDosFeriados(dataInicial, dataFinal, driver);

        for (Date tmpDate : dataVazia) {
            LocalDateTime inicio = tmpDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate().atStartOfDay();
            LocalDateTime fim = inicio.plusHours(23).plusMinutes(59).plusSeconds(59);

            createSingleEventJourney(driver, inicio, fim, 22);

            lancou = true;
        }

        return lancou;
    }

    /**
     * @param dataInicial
     * @param dataFinal
     * @param motoristaId
     * @return
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private boolean lancaDSR(Date dataInicial, Date dataFinal, Integer motoristaId) {
        boolean lancou = false;

        Motoristas driver = this.driverService.getById(motoristaId);

        List<Dsr> agendados = this.dsrService.getAgendados(motoristaId, dataInicial, dataFinal);

        LocalDateTime inicio;
        LocalDateTime fim;

        for (Dsr dsr : agendados) {
            Date faltaInicio = Objects.isNull(dsr.getInicioInterjornada()) ? dsr.getInicioDSR() : dsr.getInicioInterjornada();

            dao.faltasDoMotoristaNoPeriodo(TimeHelper.getDate000000(faltaInicio),
                    TimeHelper.getDate235959(dsr.getFimDSR()), motoristaId).forEach(this.repository::delete);

            Jornada tmpJrn = this.createJourney(driver);
            // Create DSR (20)
            inicio = dsr.getInicioDSR().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            fim = dsr.getFimDSR().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();

            this.createChainedEvent(tmpJrn, inicio, fim, EVENT_TYPE.DSR.getStartCode());

            if (Objects.nonNull(dsr.getInicioInterjornada()) && Objects.nonNull(dsr.getFimInterjornada())) {
                // Create Interjourney
                inicio = dsr.getInicioInterjornada().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
                fim = dsr.getFimInterjornada().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();

                this.createChainedEvent(tmpJrn, inicio, fim, EVENT_TYPE.INTERJORNADA.getStartCode());
            }

            dsr.setProcessada('s');
            this.dsrService.save(dsr);

            lancou = true;
        }

        return lancou;
    }

    /**
     * @param driver
     * @return
     */
    private Jornada createJourney(Motoristas driver) {
        Jornada tmpJrn = new Jornada(driver.getEmpresa(), driver);
        tmpJrn.setLocked(false);

        tmpJrn = this.save(tmpJrn);
        return tmpJrn;
    }

    /**
     * @param journey
     * @param inicio
     * @param fim
     * @param idEvento
     */
    private void createChainedEvent(Jornada journey, LocalDateTime inicio, LocalDateTime fim, int idEvento) {
        if (Objects.isNull(journey.getEventos())) {
            journey.setEventos(new ArrayList<>());
        }

        Users sistema = new Users(-1);

        int idFechaEvento = idEvento * -1;

        EventosJornada evtFaltaBeg = new EventosJornada(idEvento);
        EventosJornada evtFaltaEnd = new EventosJornada(idFechaEvento);

        Evento evtBeg = new Evento(new Date(),
                journey,
                Date.from(inicio.atZone(ZoneId.systemDefault()).toInstant()),
                journey.getEmpresaId(),
                sistema,
                "Sistema",
                evtFaltaBeg);
        evtBeg.setLocked(false);
        journey.getEventos().add(evtBeg);

        Evento evtEnd = new Evento(new Date(),
                journey,
                Date.from(fim.atZone(ZoneId.systemDefault()).toInstant()),
                journey.getEmpresaId(),
                sistema,
                "Sistema",
                evtFaltaEnd);
        evtEnd.setLocked(false);
        journey.getEventos().add(evtEnd);

        this.eventoService.saveChained(evtBeg, evtEnd, false);
    }

    /**
     * @param driver
     * @param inicio
     * @param fim
     * @param idEvento
     */
    private void createSingleEventJourney(Motoristas driver, LocalDateTime inicio, LocalDateTime fim, int idEvento) {
        Jornada tmpJrn = this.createJourney(driver);
        this.createChainedEvent(tmpJrn, inicio, fim, idEvento);
    }

    /**
     * @param dataInicial
     * @param dataFinal
     * @param motorista
     * @return
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private boolean lancaFerias(Date dataInicial, Date dataFinal, Integer motorista) {
        boolean lancou = false;
        Motoristas driver = this.driverService.getById(motorista);

        List<Date> diasDeFerias = this.feriasService.getDiasFeriasMotoristaPeriodo(dataInicial, dataFinal, motorista);

        for (Date tmpDate : diasDeFerias) {
            LocalDateTime inicio = tmpDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate().atStartOfDay();
            LocalDateTime fim = inicio.plusHours(23).plusMinutes(59).plusSeconds(59);

            createSingleEventJourney(driver, inicio, fim, 18);

            lancou = true;
        }

        return lancou;
    }

    /**
     * @param dataInicial
     * @param dataFinal
     * @param motorista
     * @return
     */
    public List<Jornada> jornadasDoMotoristaPorPeriodo(Date dataInicial, Date dataFinal, Integer motorista) {
        acertaJornada(dataInicial, dataFinal, motorista);
        List<Jornada> lista =  dao.jornadasDoMotoristaNoPeriodo(dataInicial, dataFinal, motorista);
        return lista;
    }

    /**
     * @param eventoIni
     * @param eventoFim
     * @param motoristaId
     */
    public void saveRangedAllowance(Evento eventoIni, Evento eventoFim, Integer motoristaId) {

        Date dataInicial = eventoIni.getInstanteEvento();
        Date dataFinal = eventoFim.getInstanteEvento();
        /**se a dataFinal for ANTES da dataInicial não faz nada
         *
         */
        if (dataFinal.before(dataInicial)) {
            return;
        }

        LocalDateTime tmpFinal = dataFinal.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate()
                .atStartOfDay()
                .plusHours(23).plusMinutes(59).plusSeconds(59);

        dataFinal = Date.from(tmpFinal.atZone(ZoneId.systemDefault()).toInstant());

        Motoristas driver = this.driverService.getById(motoristaId);
        /**se fizer diferença se tem ou não jornada, buscar no banco (ver ex faltas)
         *
         */
        List<Date> dataVazia = getDatesBetween(dataInicial, dataFinal);

        for (Date tmpDate : dataVazia) {
            LocalDateTime inicio = tmpDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate().atStartOfDay();
            LocalDateTime fim = inicio.plusHours(23).plusMinutes(59).plusSeconds(59);

            criaAbono(driver, inicio, fim, eventoIni);
        }
    }

    /**
     * @param startDate
     * @param endDate
     * @return
     */
    private List<Date> getDatesBetween(Date startDate, Date endDate) {
        List<Date> datesInRange = new ArrayList<>();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(startDate);

        Calendar endCalendar = new GregorianCalendar();
        endCalendar.setTime(endDate);

        while (calendar.before(endCalendar)) {
            Date result = calendar.getTime();
            datesInRange.add(result);
            calendar.add(Calendar.DATE, 1);
        }
        return datesInRange;
    }

    /**
     * @param driver
     * @param inicio
     * @param fim
     * @param modelo
     */
    private void criaAbono(Motoristas driver, LocalDateTime inicio, LocalDateTime fim, Evento modelo) {
        Jornada tmpJrn = new Jornada(driver.getEmpresa(), driver);
        this.save(tmpJrn);

        Users usuer = modelo.getOperadorLancamento();
        int idEvento = modelo.getTipoEvento().getId();

        int idFechaEvento = idEvento * -1;

        EventosJornada evtJBeg = new EventosJornada(idEvento);
        EventosJornada evtJEnd = new EventosJornada(idFechaEvento);

        Evento evtBeg = new Evento(new Date(),
                tmpJrn,
                Date.from(inicio.atZone(ZoneId.systemDefault()).toInstant()),
                tmpJrn.getEmpresaId(),
                usuer,
                "ViaM WEB",
                evtJBeg);
        evtBeg.setInstanteLancamento(modelo.getInstanteLancamento());

        evtBeg.setJustificativa(modelo.getJustificativa());
        evtBeg.setMotivoAbono(modelo.getMotivoAbono());

        Evento evtEnd = new Evento(new Date(),
                tmpJrn,
                Date.from(fim.atZone(ZoneId.systemDefault()).toInstant()),
                tmpJrn.getEmpresaId(),
                usuer,
                "ViaM WEB",
                evtJEnd);
        evtEnd.setInstanteLancamento(modelo.getInstanteLancamento());

        evtEnd.setJustificativa(modelo.getJustificativa());
        evtEnd.setMotivoAbono(modelo.getMotivoAbono());

        this.eventoService.saveChained(evtBeg, evtEnd, false);
    }

    /**
     * @param journey
     * @return
     */
    public long restTimeInWaiting(Jornada journey) {
        long response = 0L;

        if (Objects.isNull(journey)) {
            return response;
        }
        if (Objects.isNull(journey.getResumoTiposEstados())) {
            return response;
        }

        Resumo tmpWaitResume = journey.getResumoTiposEstados().get("Espera");
        List<LocalVO> listaDeLocais = pdcService.findAllAsLocal(journey.getEmpresaId().getId());
        if (Objects.nonNull(tmpWaitResume)) {
            List<Evento> waitEvents = tmpWaitResume.getLista();
            for (Evento event : waitEvents) {
                DiurnoNoturno dnn = event.getDayNight();

                long totalTime = dnn.getTempoDiurno() + dnn.getTempoNoturno();

                if (totalTime > TimeHelper.DUAS_HORAS) {
                    if (this.eventoService.eventPlaceAllowsRest(event, listaDeLocais)) {
                        response += totalTime;
                    }
                }
            }
        }

        return response;
    }

    /**
     * @param journey
     * @return
     */
    public String markWaitingAsWorked(Jornada journey) {
        if (Objects.nonNull(journey.getWaitingAsWorked())) {
            journey.setWaitingAsWorked(!journey.getWaitingAsWorked());
        } else {
            journey.setWaitingAsWorked(true);
        }
        this.save(journey);
        return "";
    }

    /**
     * @param journeyID
     * @return
     */
    public String markDSRHE(Integer journeyID) {
        Jornada journey = this.dao.getById(journeyID);
        if (Objects.nonNull(journey.getDsrHE())) {
            journey.setDsrHE(!journey.getDsrHE());
        } else {
            journey.setDsrHE(true);
        }
        this.save(journey);
        return "";
    }

    public String marcarDiariaMotorista(Integer journeyID) {
        Jornada journey = this.dao.getById(journeyID);
        if (Objects.nonNull(journey.getDiariaMotorista())) {
            journey.setDiariaMotorista(!journey.getDiariaMotorista());
        } else {
            journey.setDiariaMotorista(true);
        }
        this.save(journey);
        return "";
    }

    public List<Jornada> buscarJornadasDoDia(List<Motoristas> motoristasList) {
        return dao.buscarJornadasDoDiaViaSQLNativo(motoristasList);
    }

    /**
     * Metodo para validar alguns calculos antes de serem retornados para o front-end
     * Calculos baseados no Mantis de numero 103
     *
     * @param jornada
     */
    public void verificarCalculosFinais(Jornada jornada) {
        try {
            if (Objects.nonNull(jornada.getWaitingAsWorked()) && jornada.getWaitingAsWorked()) {
                long horaFaltante = jornada.getTrabalhado() - jornada.getHorasFaltosas();
                if (horaFaltante > 0 && horaFaltante <= 60000) {
                    horaFaltante = 0;
                }
                if (horaFaltante < 0) {
                    horaFaltante = horaFaltante * -1;
                }
                jornada.setHorasFaltosas(horaFaltante);
            }
        } catch (Exception e) {
            LogSistema.logError("varificarCalculosFinais", e);
        }
    }

    public InformacaoRelatorioVO buscarDadosRelatorioDiariaMotorista(Date dtInicial, Date dtFinal, Integer motoristaId) {
        try {
            return dao.buscarDadosRelatorioDiariaMotorista(dtInicial, dtFinal, motoristaId);
        } catch (Exception e) {
            LogSistema.logError("buscarDadosRelatorioDiariaMotorista", e);
        }
        return null;
    }

    public void inserirJornadaManual(Jornada jornada) {
        try {
            jornada.setEmpresaId(new Empresas(jornada.getHedDSR().intValue()));
            jornada = this.repository.saveAndFlush(jornada);
            Evento evento = new Evento();
            evento.setJornadaId(jornada);
            evento.setTipoEvento(new EventosJornada(jornada.getHenDSR().intValue()));
            evento.setInstanteEvento(jornada.getJornadaInicio());
            evento.setInstanteLancamento(jornada.getJornadaInicio());
            evento.setOperadorLancamento(new Users(jornada.getHorasFaltosas().intValue()));
            evento.setEmpresaId(new Empresas(jornada.getHedDSR().intValue()));
            evento.setJustificativa(jornada.getTextoLivre());
            evento.setOrigem("Sistema");
            eventoService.save(evento);
            evento = new Evento();
            evento.setJornadaId(jornada);
            evento.setTipoEvento(new EventosJornada((jornada.getHenDSR().intValue() * -1)));
            evento.setInstanteEvento(jornada.getJornadaFim());
            evento.setInstanteLancamento(jornada.getJornadaFim());
            evento.setOperadorLancamento(new Users(jornada.getHorasFaltosas().intValue()));
            evento.setEmpresaId(new Empresas(jornada.getHedDSR().intValue()));
            evento.setJustificativa(jornada.getTextoLivre());
            evento.setOrigem("Sistema");
            eventoService.save(evento);
        } catch (Exception e) {
            LogSistema.logError("inserirJornadaManual", e);
        }
    }


    private List<DetalheDaJornada> processarDetalheJornada(List<Jornada> result, List<ApuracaoDeJornada> listaApuracao) {
        List<DetalheDaJornada> lista = new ArrayList<>();
        try {
            String[] eventos = {"0100", "0149", "0406", "3490", "4007"};
            String[] descricao = {"Hora Extra 50%", "Hora Extra 100%", "Adic. Noturno 20%", "Tempo de Espera", "Faltas"};
            double ficta =  1.1428;//valor ficta fixo
            double horaExtra50 = 0d;
            long horaExtra100 = 0l;
            double adicNoturno20 = 0d;
            long tempoEspera = 0l;
            double horaExtraNoturna = 0d;
            int faltas = 0;

            for(ApuracaoDeJornada apuracaoDeJornada: listaApuracao){
                if (Objects.nonNull(apuracaoDeJornada.getHorasExtrasInterJornadas()) && apuracaoDeJornada.getHorasExtrasInterJornadas() > 0) {
                    horaExtra100 = horaExtra100 + apuracaoDeJornada.getHorasExtrasInterJornadas();
                }

                if (Objects.nonNull(apuracaoDeJornada.getHorasExtrasNoturnas()) && apuracaoDeJornada.getHorasExtrasNoturnas() > 0) {
                    horaExtraNoturna = horaExtraNoturna + (apuracaoDeJornada.getHorasExtrasNoturnas() * ficta);
                }
                if (Objects.nonNull(apuracaoDeJornada.getHorasExtrasDiurnas()) && apuracaoDeJornada.getHorasExtrasDiurnas() > 0) {
                    horaExtra50 = horaExtra50 + apuracaoDeJornada.getHorasExtrasDiurnas();
                }

                if (Objects.nonNull(apuracaoDeJornada.getHedDSR()) && apuracaoDeJornada.getHedDSR() > 0) {
                    horaExtra100 = horaExtra100 + apuracaoDeJornada.getHedDSR();
                }

                if (Objects.nonNull(apuracaoDeJornada.getHenDSR()) && apuracaoDeJornada.getHenDSR() > 0) {
                    horaExtra100 = horaExtra100 + apuracaoDeJornada.getHenDSR();
                }

                if (Objects.nonNull(apuracaoDeJornada.getAdicionalNoturno()) && apuracaoDeJornada.getAdicionalNoturno() > 0) {
                    adicNoturno20 = adicNoturno20 + (apuracaoDeJornada.getAdicionalNoturno() * ficta);
                }
            }

            for (Jornada j : result) {
                boolean horasExtrasAbonada = false;
                if ((Objects.nonNull(j.getHorasExtrasAbonada()) && j.getHorasExtrasAbonada())) {
                    horasExtrasAbonada = j.getHorasExtrasAbonada();
                }

                if ((Objects.nonNull(j.getType()) && j.getType().equals(JOURNEY_TYPE.JORNADA))) {
                    if(!horasExtrasAbonada) {

                        if (Objects.nonNull(j.getHedFeriado()) && j.getHedFeriado() > 0) {
                            horaExtra100 = horaExtra100 + j.getHedFeriado();
                        }

                        if (Objects.nonNull(j.getHenFeriado()) && j.getHenFeriado() > 0) {
                            horaExtra100 = horaExtra100 + j.getHenFeriado();
                        }

                        if (Objects.nonNull(j.getEsperaDiurno()) && j.getEsperaDiurno() > 0) {
                            tempoEspera = tempoEspera + j.getEsperaDiurno();
                        }
                        if (Objects.nonNull(j.getEsperaNoturno()) && j.getEsperaNoturno() > 0) {
                            tempoEspera = tempoEspera + j.getEsperaNoturno();
                        }

                    }
                }
                if (Objects.nonNull(j.getType()) && j.getType().equals(JOURNEY_TYPE.FALTA)) {
                    faltas = faltas + 1;
                }


            }

            horaExtra50 = horaExtra50 + horaExtraNoturna;

            Collections.sort(result, new Comparator<Jornada>() {
                @Override
                public int compare(Jornada u1, Jornada u2) {
                    return u1.getJornadaInicio().compareTo(u2.getJornadaInicio());
                }
            });


            for (int i = 0; i < eventos.length; i++) {
                DetalheDaJornada dj = new DetalheDaJornada();
                dj.setChapa("00000");
                dj.setEvento(eventos[i]);
                dj.setDescricao(descricao[i]);
                dj.setHora("00:00");
                switch (i) {
                    case 0: {
                        if (horaExtra50 > 0) {
                            dj.setHora(TimeHelper.converteMinutosEmStringFormatadaTres((horaExtra50)));
                        }
                    }
                    break;
                    case 1: {
                        if (horaExtra100 > 0) {
                            dj.setHora(TimeHelper.converteMinutosEmStringFormatadaTres(horaExtra100));
                        }
                    }
                    break;
                    case 2: {
                        if (adicNoturno20 > 0) {
                            dj.setHora(TimeHelper.converteMinutosEmStringFormatadaTres(adicNoturno20));
                        }
                    }
                    break;
                    case 3: {
                        if (tempoEspera > 0) {
                            dj.setHora(TimeHelper.converteMinutosEmStringFormatadaTres(tempoEspera));
                        }
                    }
                    break;
                    case 4: {
                        if (faltas > 0) {
                            dj.setFalta(String.valueOf(faltas));
                        }
                    }
                    break;
                }
                lista.add(dj);
            }

        } catch (Exception e) {
            LogSistema.logError("processarDetalheJornada", e);
        }
        return lista;
    }

    private int totalDeExtraDirigida = 0;
    private int totalDeTempoDeEspera = 0;
    private int horasExcedentes = 0;
    private int totalHorasExcedentes = 0;
    private int totalDeHorasTrabalhadas = 0;

    private List<br.com.cmctransportes.cjm.domain.entities.jornada.ResumoJornada> apuracaoDeResumoJornada(List<Jornada> result) {
        List<br.com.cmctransportes.cjm.domain.entities.jornada.ResumoJornada> lista = new ArrayList<>();
        br.com.cmctransportes.cjm.domain.entities.jornada.ResumoJornada utimaResumoJornada = null;
        int totalDeHoraExtra = 0;
        totalDeExtraDirigida = 0;
        totalDeTempoDeEspera = 0;
        totalHorasExcedentes = 0;
        totalDeHorasTrabalhadas = 0;
        horasExcedentes = 0;

        try {

            Collections.sort(result, new Comparator<Jornada>() {
                @Override
                public int compare(Jornada u1, Jornada u2) {
                    return u1.getJornadaInicio().compareTo(u2.getJornadaInicio());
                }
            });

            int pegaProximo = 1;
            for (Jornada jornada : result) {
                horasExcedentes = 0;
                br.com.cmctransportes.cjm.domain.entities.jornada.ResumoJornada resumoJornada = new br.com.cmctransportes.cjm.domain.entities.jornada.ResumoJornada();
                resumoJornada.setMes(TimeHelper.mesString(jornada.getJornadaInicio()));
                long tempoPrevisaoTermino = jornada.getJornadaInicio().getTime() + calcularTempoDeTermino(jornada.getMotoristasId());
                resumoJornada.setPrevisaoTermino(TimeHelper.conveterDataEmString(new Date(tempoPrevisaoTermino), "dd/MM/yy HH:mm"));
                resumoJornada.setEntrada(TimeHelper.conveterDataEmString(jornada.getJornadaInicio(), "dd/MM/yy HH:mm"));
                resumoJornada.setSaida(TimeHelper.conveterDataEmString(jornada.getJornadaFim(), "dd/MM/yy HH:mm"));
                resumoJornada.setHorasTrabalhada(TimeHelper.converteMinutosEmStringFormatadaTres(jornada.getTrabalhado()));
                if (Objects.nonNull(jornada.getHorasExtras()) && jornada.getHorasExtras() > 0) {
                    resumoJornada.setHoraExtra(TimeHelper.converteMinutosEmStringFormatadaTres(jornada.getHorasExtras()));
                    totalDeHoraExtra = totalDeHoraExtra + (int) (jornada.getHorasExtras() / 60000);
                } else {
                    resumoJornada.setHoraExtra("00:00");
                }

                int horasFaltosas = (int) (jornada.getHorasFaltosas() / 60000);
                calcularResumoJornada(jornada.getEventos(), resumoJornada, jornada.getMotoristasId(), horasFaltosas);
                lista.add(resumoJornada);

                if (pegaProximo < result.size()) {
                    Date dataProximoJornada = result.get(pegaProximo).getJornadaInicio();
                    int minutos = Minutes.minutesBetween(new DateTime(jornada.getJornadaFim()), new DateTime(dataProximoJornada)).getMinutes();
                    if (minutos > 0) {
                        resumoJornada.setInterjornada(TimeHelper.converteMinutosEmStringFormatadaTres(minutos));
                    } else {
                        resumoJornada.setInterjornada("00:00");
                    }
                } else {
                    resumoJornada.setInterjornada("00:00");
                }
                int minutos = (int) ((jornada.getTrabalhado() / 1000) / 60);
                if (Objects.nonNull(resumoJornada.getTempoEsperaI())) {
                    int totalJornada = resumoJornada.getTempoEsperaI() + minutos;
                    resumoJornada.setTotalJornada(TimeHelper.converteMinutosEmStringFormatadaTres(totalJornada));
                    totalDeHorasTrabalhadas = totalDeHorasTrabalhadas + totalJornada;
                } else {
                    resumoJornada.setTotalJornada("00:00");
                }

                /*Calcula horas extras trabalhadas

                 */
                if (Objects.nonNull(jornada.getHorasExtras()) && jornada.getHorasExtras() > 0) {
                    minutos = (int) ((jornada.getHorasExtras() / 1000) / 60);
                    horasExcedentes = horasExcedentes + minutos;
                    totalDeExtraDirigida = totalDeExtraDirigida + minutos;
                    resumoJornada.setExtraDirigida(TimeHelper.converteMinutosEmStringFormatadaTres(minutos));
                } else {
                    resumoJornada.setExtraDirigida("00:00");
                }

                resumoJornada.setHoraExcedentes(TimeHelper.converteMinutosEmStringFormatadaTres(horasExcedentes));


                totalHorasExcedentes = totalHorasExcedentes + horasExcedentes;
                horasExcedentes = 0;
                pegaProximo++;
            }
        } catch (Exception e) {
            LogSistema.logError("apuracaoDeResumoJornada", e);
        }
        //Add ultima linha com os totais
        br.com.cmctransportes.cjm.domain.entities.jornada.ResumoJornada resumoJornadaTotais = new br.com.cmctransportes.cjm.domain.entities.jornada.ResumoJornada();
        resumoJornadaTotais.setEntrada("");
        resumoJornadaTotais.setSaida("");
        resumoJornadaTotais.setParadas("");
        resumoJornadaTotais.setRefeicao("");
        resumoJornadaTotais.setExtraDirigida("");
        resumoJornadaTotais.setAguardando("");
        resumoJornadaTotais.setPrevisaoTermino("");
        resumoJornadaTotais.setDescanso("");
        resumoJornadaTotais.setCargaDescarga("");
        resumoJornadaTotais.setInterjornada("");
        resumoJornadaTotais.setHoraExtra(TimeHelper.converteMinutosEmStringFormatadaTres(totalDeHoraExtra));
        resumoJornadaTotais.setOcioso("TOTAL");
        resumoJornadaTotais.setHorasTrabalhada("");
        resumoJornadaTotais.setTempoDeEspera("");
        resumoJornadaTotais.setTotalJornada("");
        resumoJornadaTotais.setPrevisaoTermino("");
        resumoJornadaTotais.setExtraDirigida(TimeHelper.converteMinutosEmStringFormatadaTres(totalDeExtraDirigida));
        resumoJornadaTotais.setExtraEspera(TimeHelper.converteMinutosEmStringFormatadaTres(totalDeTempoDeEspera));
        resumoJornadaTotais.setHoraExcedentes(TimeHelper.converteMinutosEmStringFormatadaTres(totalHorasExcedentes));
        resumoJornadaTotais.setTotalJornada(TimeHelper.converteMinutosEmStringFormatadaTres(totalDeHorasTrabalhadas));
        lista.add(resumoJornadaTotais);
        return lista;
    }

    private void calcularResumoJornada(Collection<Evento> lTemp,
                                       br.com.cmctransportes.cjm.domain.entities.jornada.ResumoJornada resumoJornada,
                                       Motoristas motoristas, int horasFaltosas) {
        try {
            //Pegar o inicio da Jornada
            boolean achouInicioJornada = false;
            Evento eventoInicioJornada = null;
            int totalParadas = 0;
            int totalAlmoco = 0;
            int totalDirecao = 0;
            int totalAguardando = 0;
            int totalDescanso = 0;
            int totalCargaDescarga = 0;
            int totalOcioso = 0;
            Evento eventoInicial = null;
            Evento ultimoEvento = null;

            for (Evento evento : lTemp) {
                if (evento.getTipoEvento().getId() == 1) {
                    achouInicioJornada = true;
                    ultimoEvento = evento;
                }

                if (achouInicioJornada) {
                    if (evento.getTipoEvento().getId() == 7
                            || evento.getTipoEvento().getId() == 5
                            || evento.getTipoEvento().getId() == 6
                            || evento.getTipoEvento().getId() == 3
                            || evento.getTipoEvento().getId() == 4
                            || evento.getTipoEvento().getId() == 23) {
                        eventoInicial = evento;
                        int a = Minutes.minutesBetween(new DateTime(ultimoEvento.getInstanteEvento()), new DateTime(evento.getInstanteEvento())).getMinutes();
                        totalOcioso = totalOcioso + a;
                    }

                    if (evento.getTipoEvento().getId() == -7
                            || evento.getTipoEvento().getId() == -5
                            || evento.getTipoEvento().getId() == -6
                            || evento.getTipoEvento().getId() == -3
                            || evento.getTipoEvento().getId() == -4
                            || evento.getTipoEvento().getId() == -23) {
                        int a = Minutes.minutesBetween(new DateTime(eventoInicial.getInstanteEvento()), new DateTime(evento.getInstanteEvento())).getMinutes();
                        totalParadas = totalParadas + a;
                        eventoInicial = null;
                        ultimoEvento = evento;
                    }

                    if (evento.getTipoEvento().getId() == 14) {
                        eventoInicial = evento;
                        int a = Minutes.minutesBetween(new DateTime(ultimoEvento.getInstanteEvento()), new DateTime(evento.getInstanteEvento())).getMinutes();
                        totalOcioso = totalOcioso + a;
                    }
                    if (evento.getTipoEvento().getId() == -14) {
                        int a = Minutes.minutesBetween(new DateTime(eventoInicial.getInstanteEvento()), new DateTime(evento.getInstanteEvento())).getMinutes();
                        totalAlmoco = totalAlmoco + a;
                        eventoInicial = null;
                        ultimoEvento = evento;
                    }

                    if (evento.getTipoEvento().getId() == 8) {
                        eventoInicial = evento;
                        int a = Minutes.minutesBetween(new DateTime(ultimoEvento.getInstanteEvento()), new DateTime(evento.getInstanteEvento())).getMinutes();
                        totalOcioso = totalOcioso + a;
                    }
                    if (evento.getTipoEvento().getId() == -8) {
                        int a = Minutes.minutesBetween(new DateTime(eventoInicial.getInstanteEvento()), new DateTime(evento.getInstanteEvento())).getMinutes();
                        totalDirecao = totalDirecao + a;
                        eventoInicial = null;
                        ultimoEvento = evento;
                    }

                    if (evento.getTipoEvento().getId() == 11 || evento.getTipoEvento().getId() == 9) {
                        eventoInicial = evento;
                        int a = Minutes.minutesBetween(new DateTime(ultimoEvento.getInstanteEvento()), new DateTime(evento.getInstanteEvento())).getMinutes();
                        totalOcioso = totalOcioso + a;
                    }
                    if (evento.getTipoEvento().getId() == -11 || evento.getTipoEvento().getId() == -9) {
                        if (Objects.nonNull(eventoInicial)) {
                            int a = Minutes.minutesBetween(new DateTime(eventoInicial.getInstanteEvento()), new DateTime(evento.getInstanteEvento())).getMinutes();
                            totalAguardando = totalAguardando + a;
                            eventoInicial = null;
                        }
                        ultimoEvento = evento;
                    }

                    if (evento.getTipoEvento().getId() == 15) {
                        if (Objects.nonNull(ultimoEvento.getInstanteEvento())) {
                            int a = Minutes.minutesBetween(new DateTime(ultimoEvento.getInstanteEvento()), new DateTime(evento.getInstanteEvento())).getMinutes();
                            totalOcioso = totalOcioso + a;
                        }
                        eventoInicial = evento;
                    }
                    if (evento.getTipoEvento().getId() == -15) {
                        int a = Minutes.minutesBetween(new DateTime(eventoInicial.getInstanteEvento()), new DateTime(evento.getInstanteEvento())).getMinutes();
                        totalDescanso = totalDescanso + a;
                        eventoInicial = null;
                        ultimoEvento = evento;
                    }

                    if (evento.getTipoEvento().getId() == 10 || evento.getTipoEvento().getId() == 12) {
                        eventoInicial = evento;
                        int a = Minutes.minutesBetween(new DateTime(ultimoEvento.getInstanteEvento()), new DateTime(evento.getInstanteEvento())).getMinutes();
                        totalOcioso = totalOcioso + a;
                    }
                    if (evento.getTipoEvento().getId() == -10 || evento.getTipoEvento().getId() == -12) {
                        int a = Minutes.minutesBetween(new DateTime(eventoInicial.getInstanteEvento()), new DateTime(evento.getInstanteEvento())).getMinutes();
                        totalCargaDescarga = totalCargaDescarga + a;
                        eventoInicial = null;
                        ultimoEvento = evento;
                    }
                }
            }
            resumoJornada.setParadas(TimeHelper.converteMinutosEmStringFormatadaTres(totalParadas));
            resumoJornada.setRefeicao(TimeHelper.converteMinutosEmStringFormatadaTres(totalAlmoco));

            resumoJornada.setHorasDirigidas(TimeHelper.converteMinutosEmStringFormatadaTres(totalDirecao));
            resumoJornada.setAguardando(TimeHelper.converteMinutosEmStringFormatadaTres(totalAguardando));
            resumoJornada.setDescanso(TimeHelper.converteMinutosEmStringFormatadaTres(totalDescanso));
            resumoJornada.setCargaDescarga(TimeHelper.converteMinutosEmStringFormatadaTres(totalCargaDescarga));
            resumoJornada.setOcioso(TimeHelper.converteMinutosEmStringFormatadaTres(totalOcioso));
            int tempoDeEspera = totalAguardando + totalCargaDescarga;
            if (tempoDeEspera > horasFaltosas) {
                int dif = tempoDeEspera - horasFaltosas;
                totalDeTempoDeEspera = totalDeTempoDeEspera + dif;
                horasExcedentes = horasExcedentes + dif;
                resumoJornada.setExtraEspera(TimeHelper.converteMinutosEmStringFormatadaTres(dif));
            } else {
                resumoJornada.setExtraEspera("00:00");
            }

            resumoJornada.setTempoDeEspera(TimeHelper.converteMinutosEmStringFormatadaTres(tempoDeEspera));
            resumoJornada.setTempoEsperaI(tempoDeEspera);

        } catch (Exception e) {
            LogSistema.logError("apuracaoDeResumoJornada", e);
        }
    }

    private long calcularTempoDeTermino(Motoristas motoristas) {
        try {
            switch (TimeHelper.pegarDiaDaSemana()) {
                case 1: {
                    if (Objects.nonNull((motoristas)) && Objects.nonNull(motoristas.getTurnosId())
                            && Objects.nonNull(motoristas.getTurnosId().getTempoDiario01()) && Objects.nonNull(motoristas.getTurnosId().getTempoAlimentacao01())) {
                        return motoristas.getTurnosId().getTempoDiario01() + motoristas.getTurnosId().getTempoAlimentacao01();
                    }
                }
                break;
                case 2: {
                    if (Objects.nonNull((motoristas)) && Objects.nonNull(motoristas.getTurnosId())
                            && Objects.nonNull(motoristas.getTurnosId().getTempoDiario02()) && Objects.nonNull(motoristas.getTurnosId().getTempoAlimentacao02())) {
                        return motoristas.getTurnosId().getTempoDiario02() + motoristas.getTurnosId().getTempoAlimentacao02();
                    }
                }
                break;
                case 3: {
                    if (Objects.nonNull((motoristas)) && Objects.nonNull(motoristas.getTurnosId())
                            && Objects.nonNull(motoristas.getTurnosId().getTempoDiario03()) && Objects.nonNull(motoristas.getTurnosId().getTempoAlimentacao03())) {
                        return motoristas.getTurnosId().getTempoDiario03() + motoristas.getTurnosId().getTempoAlimentacao03();
                    }
                }
                break;
                case 4: {
                    if (Objects.nonNull((motoristas)) && Objects.nonNull(motoristas.getTurnosId())
                            && Objects.nonNull(motoristas.getTurnosId().getTempoDiario04()) && Objects.nonNull(motoristas.getTurnosId().getTempoAlimentacao04())) {
                        return motoristas.getTurnosId().getTempoDiario04() + motoristas.getTurnosId().getTempoAlimentacao04();
                    }
                }
                break;
                case 5: {
                    if (Objects.nonNull((motoristas)) && Objects.nonNull(motoristas.getTurnosId())
                            && Objects.nonNull(motoristas.getTurnosId().getTempoDiario05()) && Objects.nonNull(motoristas.getTurnosId().getTempoAlimentacao05())) {
                        return motoristas.getTurnosId().getTempoDiario05() + motoristas.getTurnosId().getTempoAlimentacao05();
                    }
                }
                break;
                case 6: {
                    if (Objects.nonNull((motoristas)) && Objects.nonNull(motoristas.getTurnosId())
                            && Objects.nonNull(motoristas.getTurnosId().getTempoDiario06()) && Objects.nonNull(motoristas.getTurnosId().getTempoAlimentacao06())) {
                        return motoristas.getTurnosId().getTempoDiario06() + motoristas.getTurnosId().getTempoAlimentacao06();
                    }
                }
                break;
                case 7: {
                    if (Objects.nonNull((motoristas)) && Objects.nonNull(motoristas.getTurnosId())
                            && Objects.nonNull(motoristas.getTurnosId().getTempoDiario07()) && Objects.nonNull(motoristas.getTurnosId().getTempoAlimentacao07())) {
                        return motoristas.getTurnosId().getTempoDiario07() + motoristas.getTurnosId().getTempoAlimentacao07();
                    }
                }
                break;

            }
        } catch (Exception e) {
            LogSistema.logError("calcularTempoDeTermino", e);
        }
        return 26400000 + 3600000;
    }

    private long calcularTempoEfetivo(Motoristas motoristas) {
        try {
            switch (TimeHelper.pegarDiaDaSemana()) {
                case 1: {
                    if (Objects.nonNull((motoristas)) && Objects.nonNull(motoristas.getTurnosId())
                            && Objects.nonNull(motoristas.getTurnosId().getTempoDiario01())) {
                        return motoristas.getTurnosId().getTempoDiario01();
                    }
                }
                break;
                case 2: {
                    if (Objects.nonNull((motoristas)) && Objects.nonNull(motoristas.getTurnosId())
                            && Objects.nonNull(motoristas.getTurnosId().getTempoDiario02())) {
                        return motoristas.getTurnosId().getTempoDiario02();
                    }
                }
                break;
                case 3: {
                    if (Objects.nonNull((motoristas)) && Objects.nonNull(motoristas.getTurnosId())
                            && Objects.nonNull(motoristas.getTurnosId().getTempoDiario03())) {
                        return motoristas.getTurnosId().getTempoDiario03();
                    }
                }
                break;
                case 4: {
                    if (Objects.nonNull((motoristas)) && Objects.nonNull(motoristas.getTurnosId())
                            && Objects.nonNull(motoristas.getTurnosId().getTempoDiario04())) {
                        return motoristas.getTurnosId().getTempoDiario04();
                    }
                }
                break;
                case 5: {
                    if (Objects.nonNull((motoristas)) && Objects.nonNull(motoristas.getTurnosId())
                            && Objects.nonNull(motoristas.getTurnosId().getTempoDiario05())) {
                        return motoristas.getTurnosId().getTempoDiario05();
                    }
                }
                break;
                case 6: {
                    if (Objects.nonNull((motoristas)) && Objects.nonNull(motoristas.getTurnosId())
                            && Objects.nonNull(motoristas.getTurnosId().getTempoDiario06())) {
                        return motoristas.getTurnosId().getTempoDiario06();
                    }
                }
                break;
                case 7: {
                    if (Objects.nonNull((motoristas)) && Objects.nonNull(motoristas.getTurnosId())
                            && Objects.nonNull(motoristas.getTurnosId().getTempoDiario07())) {
                        return motoristas.getTurnosId().getTempoDiario07();
                    }
                }
                break;

            }
        } catch (Exception e) {
            LogSistema.logError("calcularTempoDeTermino", e);
        }
        return 26400000;
    }



    public void abonarHorasExtra(AbonarExtraVO abonarExtraVO) {
        try {
            //Primeiro gravar a justificativa
            Justificativa justificativa = new Justificativa();
            justificativa.setDados(abonarExtraVO.getJustificativa());
            justificativa.setDataCadastro(new Date());
            justificativa.setIdJornada(abonarExtraVO.getJornada().getId());
            justificativa.setIdUsuario(abonarExtraVO.getIdUsuarioLogado());
            justificativa.setTipo(1);//tipo abonar hora extra na jornada.
            this.justificativaService.gravar(justificativa);
            Jornada jornada = this.dao.getById(abonarExtraVO.getJornada().getId());
            Boolean abonaHorasExtra = jornada.getHorasExtrasAbonada();
            if (Objects.isNull(abonaHorasExtra)) {
                jornada.setHorasExtrasAbonada(Boolean.TRUE);
            } else {
                if (abonaHorasExtra) {
                    jornada.setHorasExtrasAbonada(Boolean.FALSE);
                } else {
                    jornada.setHorasExtrasAbonada(Boolean.TRUE);
                }
            }
            this.update(jornada);
        } catch (Exception e) {
            LogSistema.logError("abonarHorasExtra", e);
        }
    }

    public void horasExtrasCinquentaPorcento(AbonarExtraVO abonarExtraVO) {
        try {
            //Primeiro gravar a justificativa
            Justificativa justificativa = new Justificativa();
            justificativa.setDados(abonarExtraVO.getJustificativa());
            justificativa.setDataCadastro(new Date());
            justificativa.setIdJornada(abonarExtraVO.getJornada().getId());
            justificativa.setIdUsuario(abonarExtraVO.getIdUsuarioLogado());
            justificativa.setTipo(2);//tipo hora extra 50%
            this.justificativaService.gravar(justificativa);
            Jornada jornada = this.dao.getById(abonarExtraVO.getJornada().getId());
            Boolean horasExtrasCinquentaPorcento = jornada.getHorasExtrasCinquentaPorcento();
            if (Objects.isNull(horasExtrasCinquentaPorcento)) {
                jornada.setHorasExtrasCinquentaPorcento(Boolean.TRUE);
            } else {
                if (horasExtrasCinquentaPorcento) {
                    jornada.setHorasExtrasCinquentaPorcento(Boolean.FALSE);
                } else {
                    jornada.setHorasExtrasCinquentaPorcento(Boolean.TRUE);
                }
            }
            this.update(jornada);
        } catch (Exception e) {
            LogSistema.logError("horasExtrasCinquentaPorcento", e);
        }
    }

    public void deletarJornadaComFerias(Date dataInicio, Date dataFim, Integer motorista) {
        this.dao.deletarJornadaComFerias(dataInicio, dataFim, motorista);
    }

}
