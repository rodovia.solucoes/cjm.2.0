/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cmctransportes.cjm.domain.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author analista.ti
 */
@Entity
@Table(name = "unidades_federativas")
@Getter
@Setter
@NamedQueries({
        @NamedQuery(name = "UnidadesFederativas.findAll", query = "SELECT u FROM UnidadesFederativas u"),
        @NamedQuery(name = "UnidadesFederativas.findByCodIbge", query = "SELECT u FROM UnidadesFederativas u WHERE u.codIbge = :codIbge"),
        @NamedQuery(name = "UnidadesFederativas.findBySigla", query = "SELECT u FROM UnidadesFederativas u WHERE u.sigla = :sigla"),
        @NamedQuery(name = "UnidadesFederativas.findByNome", query = "SELECT u FROM UnidadesFederativas u WHERE u.nome = :nome"),
        @NamedQuery(name = "UnidadesFederativas.findByRegiao", query = "SELECT u FROM UnidadesFederativas u WHERE u.regiao = :regiao")})
public class UnidadesFederativas implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "cod_ibge")
    private Integer codIbge;

    @Column(name = "sigla", length = 2, nullable = false)
    private String sigla;

    @Column(name = "nome", length = 45, nullable = false)
    private String nome;

    @Column(name = "regiao", length = 15, nullable = false)
    private String regiao;

    @JoinColumn(name = "capital", referencedColumnName = "cod_ibge")
    @OneToOne
    @JsonIgnore
    private Cidades capital;
}