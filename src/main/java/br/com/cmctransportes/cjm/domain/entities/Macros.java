package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "macros")
@Getter
@Setter
public class Macros implements Serializable {

    @Id
    @SequenceGenerator(name = "macros_id_seq", sequenceName = "macros_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "macros_id_seq")
    @Column(name = "id")
    private Integer id;

    @Column(name = "nome", length = 100, nullable = false)
    private String nome;

    @Column(name = "data_cadastro")
    @Temporal(TemporalType.DATE)
    private Date dataCriacao;

    @Column(name = "status")
    private Boolean status;

    @Column(name = "empresa")
    private Integer empresa;

    /**
     * ENVENTOS - VIA-SAT = 1
     * JORNADA - VIA-M = 2
     */
    @Column(name = "tipo")
    private Integer tipo;

    @Transient
    private List<EventoMacro> listaDeEventos;
}
