package br.com.cmctransportes.cjm.domain.entities;

import br.com.cmctransportes.cjm.domain.entities.enums.NONCONFORMITY_CALCULUS_TYPE;
import br.com.cmctransportes.cjm.domain.entities.enums.NONCONFORMITY_TYPE;
import br.com.cmctransportes.cjm.domain.entities.enums.TREATMENT_TYPE;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author William Leite
 */
@Entity
@Table(name = "nonconformity")
@Getter
@Setter
public class Nonconformity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "NONCONFORMITY_ID_SEQ", sequenceName = "NONCONFORMITY_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NONCONFORMITY_ID_SEQ")
    @Column(name = "id")
    private Long id;

    @Enumerated
    @Column(name = "calculus_type")
    private NONCONFORMITY_CALCULUS_TYPE calculusType;

    @Enumerated
    @Column(name = "type")
    private NONCONFORMITY_TYPE type;

    @Column(name = "description")
    private String description;

    @Enumerated
    @Column(name = "treatment_type")
    private TREATMENT_TYPE treatmentType;

    @Column(name = "treatment_description")
    private String treatmentDescription;

    @Column(name = "treatment_args")
    private String treatmentArgs;

    @Column(name = "driver")
    private Integer driver;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "start_date")
    private Date startDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "end_date")
    private Date endDate;

    @Column(name = "month")
    private Integer month;

    @Column(name = "year")
    private Integer year;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private Users user;

    @ManyToOne
    @JoinColumn(name = "journey_calculation_line_id", referencedColumnName = "id")
    private JourneyCalculationLine line;

    public Nonconformity() {
        this.treatmentType = TREATMENT_TYPE.NO_TREATMENT_AVAILABLE;
        this.treatmentDescription = "Nenhum tratamento está disponível no momento.";
    }
}