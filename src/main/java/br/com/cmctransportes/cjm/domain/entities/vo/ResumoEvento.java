package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Israel Souza
 */
public class ResumoEvento implements Serializable {

    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private Date data;

    @Getter
    @Setter
    private List<EventoPeriodo> listaEventos;
}
