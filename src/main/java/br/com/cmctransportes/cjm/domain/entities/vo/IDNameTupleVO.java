/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.UsersCompany;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author William Leite
 */
public class IDNameTupleVO implements Serializable {

    private Integer id;
    private Integer ternaryID;
    private String name;
    private boolean habilitarControleDeDiarias;
    private boolean habilitarRmLobore;

    public IDNameTupleVO() {
    }

    public IDNameTupleVO(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public IDNameTupleVO(Integer id, Integer ternaryID, String name) {
        this.id = id;
        this.ternaryID = ternaryID;
        this.name = name;
    }

    public IDNameTupleVO(UsersCompany usersCompany) {
        this.id =  usersCompany.getCompany().getId();
        this.ternaryID = usersCompany.getId();
        this.name = usersCompany.getCompany().getNome();
        if(Objects.nonNull(usersCompany.getCompany().getParametro()) && Objects.nonNull(usersCompany.getCompany().getParametro().getHabilitarControleDeDiarias())){
            this.habilitarControleDeDiarias = usersCompany.getCompany().getParametro().getHabilitarControleDeDiarias();
        }else{
            this.habilitarControleDeDiarias = false;
        }

        if(Objects.nonNull(usersCompany.getCompany().getParametro()) && Objects.nonNull(usersCompany.getCompany().getParametro().getHabilitarRmLobore())){
            this.habilitarRmLobore = usersCompany.getCompany().getParametro().getHabilitarRmLobore();
        }else{
            this.habilitarRmLobore = false;
        }

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTernaryID() {
        return ternaryID;
    }


    public boolean isHabilitarRmLobore() {
        return habilitarRmLobore;
    }

    public void setHabilitarRmLobore(boolean habilitarRmLobore) {
        this.habilitarRmLobore = habilitarRmLobore;
    }

    public boolean isHabilitarControleDeDiarias() {
        return habilitarControleDeDiarias;
    }

    public void setHabilitarControleDeDiarias(boolean habilitarControleDeDiarias) {
        this.habilitarControleDeDiarias = habilitarControleDeDiarias;
    }

    public void setTernaryID(Integer ternaryID) {
        this.ternaryID = ternaryID;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.id);
        hash = 97 * hash + Objects.hashCode(this.ternaryID);
        hash = 97 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final IDNameTupleVO other = (IDNameTupleVO) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.ternaryID, other.ternaryID)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "IDNameTupleVO{" + "id=" + id + ", ternaryID=" + ternaryID + ", name=" + name + '}';
    }
}
