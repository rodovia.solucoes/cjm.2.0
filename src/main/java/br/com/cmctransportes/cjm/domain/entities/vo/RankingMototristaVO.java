package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RankingMototristaVO {
    private List<ResumoRankingMototristaVO> listaResumoRankingMototrista;
    private MotoristaRankingDadosVO motoristaRankingDados;
}
