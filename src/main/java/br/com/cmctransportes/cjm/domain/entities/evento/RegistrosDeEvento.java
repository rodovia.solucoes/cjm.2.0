/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cmctransportes.cjm.domain.entities.evento;

import br.com.cmctransportes.cjm.domain.entities.Evento;
import br.com.cmctransportes.cjm.domain.entities.Instante;

import java.util.ArrayList;
import java.util.Date;
// TODO: trocar por LocalDateTime

/**
 * @author analista.ti
 */
public class RegistrosDeEvento {
    // TODO: refatorar essa classe
    private Date data;
    private Integer situacao;
    private Instante jornadaInicio; // TODO: virar uma classe "estado"
    private Instante jornadaFim; // TODO: virar uma classe "estado"

    // TODO: colocar em um Array
    private Instante refeicaoInicio; // TODO: virar uma classe "estado"
    private Instante refeicaoFim; // TODO: virar uma classe "estado"

    private ArrayList<Instante> direcaoInicio = new ArrayList<Instante>(); // TODO: virar uma classe "estado"
    private ArrayList<Instante> direcaoFim = new ArrayList<Instante>(); // TODO: virar uma classe "estado"

    private ArrayList<Instante> descansoInicio = new ArrayList<Instante>();
    ; // TODO: virar uma classe "estado"
    private ArrayList<Instante> descansoFim = new ArrayList<Instante>();
    ; // TODO: virar uma classe "estado"

    private ArrayList<Instante> carregamentoInicio = new ArrayList<Instante>(); // TODO: virar uma classe "estado"
    private ArrayList<Instante> carregamentoFim = new ArrayList<Instante>(); // TODO: virar uma classe "estado"

    private ArrayList<Instante> descarregamentoInicio = new ArrayList<Instante>(); // TODO: virar uma classe "estado"
    private ArrayList<Instante> descarregamentoFim = new ArrayList<Instante>(); // TODO: virar uma classe "estado"

    public RegistrosDeEvento() {
    }

    /**
     * @return the data
     */
    public Date getData() {
        return data;
    }

    /**
     * @return the situacao
     */
    public Integer getSituacao() {
        return situacao;
    }

    /**
     * @return the jornadaInicio
     */
    public Instante getJornadaInicio() {
        return jornadaInicio;
    }

    /**
     * @return the jornadaFim
     */
    public Instante getJornadaFim() {
        return jornadaFim;
    }

    /**
     * @return the refeicaoInicio
     */
    public Instante getRefeicaoInicio() {
        return refeicaoInicio;
    }

    /**
     * @return the refeicaoFim
     */
    public Instante getRefeicaoFim() {
        return refeicaoFim;
    }

    /**
     * @return the descansoInicio
     */
    public ArrayList<Instante> getDescansoInicio() {
        return descansoInicio;
    }

    /**
     * @return the descansoFim
     */
    public ArrayList<Instante> getDescansoFim() {
        return descansoFim;
    }

    /**
     * @return the carregamentoInicio
     */
    public ArrayList<Instante> getCarregamentoInicio() {
        return carregamentoInicio;
    }

    /**
     * @return the carregamentoFim
     */
    public ArrayList<Instante> getCarregamentoFim() {
        return carregamentoFim;
    }

    /**
     * @return the descarregamentoInicio
     */
    public ArrayList<Instante> getDescarregamentoInicio() {
        return descarregamentoInicio;
    }

    /**
     * @return the descarregamentoFim
     */
    public ArrayList<Instante> getDescarregamentoFim() {
        return descarregamentoFim;
    }

    /**
     * @return the direcaoInicio
     */
    public ArrayList<Instante> getDirecaoInicio() {
        return direcaoInicio;
    }

    /**
     * @return the direcaoFim
     */
    public ArrayList<Instante> getDirecaoFim() {
        return direcaoFim;
    }


    public void processa(Evento evento) {
    }
}