package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.AlertaViam;
import br.com.cmctransportes.cjm.domain.entities.Empresas;
import br.com.cmctransportes.cjm.domain.entities.EventosJornada;
import br.com.cmctransportes.cjm.domain.entities.Motoristas;
import br.com.cmctransportes.cjm.domain.entities.vo.AlertaViamVO;
import br.com.cmctransportes.cjm.infra.dao.AlertaViamDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.Date;

@Service
public class AlertaViamService {

    private final EntityManager em;
    private AlertaViamDAO alertaViamDAO;


    public AlertaViamService(EntityManager em) {
        this.em = em;
        alertaViamDAO = new AlertaViamDAO(em);
    }

    @Transactional
    public void save(AlertaViamVO alertaViamVO) throws Exception {
        try {
            try {
                AlertaViam alertaViam = new AlertaViam();
                alertaViam.setDataRegistro(new Date());
                alertaViam.setDataRegistroNoMobile(new Date(alertaViamVO.getDataAlerta()));
                alertaViam.setEventosJornada(new EventosJornada(alertaViamVO.getTipoDoEvento()));
                alertaViam.setEmpresas(new Empresas(alertaViamVO.getEmpresa()));
                alertaViam.setMotoristas(new Motoristas(alertaViamVO.getOperador()));
                alertaViam.setJornada(alertaViamVO.getJornada());
                alertaViam.setStatus(alertaViamVO.getStatus());
                alertaViam.setDataDaNotificacao(new Date(alertaViamVO.getHoraDanotificacao()));
                alertaViam.setQuantidadeCliqueCancelar(alertaViamVO.getQuantidadeCliqueCancelar());
                alertaViam.setQuantidadeCliqueOk(alertaViamVO.getQuantidadeCliqueOk());
                alertaViamDAO.save(alertaViam);
            } catch (Exception e) {
                throw e;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
}
