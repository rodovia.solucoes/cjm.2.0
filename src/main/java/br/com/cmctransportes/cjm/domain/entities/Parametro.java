package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "parametro")
@Getter
@Setter
public class Parametro  implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name = "PARAMETRO_ID_SEQUENCIA", sequenceName = "PARAMETRO_ID_SEQUENCIA", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PARAMETRO_ID_SEQUENCIA")
    @Column(name = "id")
    private Integer id;

    @Column(name = "tempo_direcao", length = 5)
    private String tempoDirecao;
    @Column(name = "inicio_tempo_direcao", length = 5)
    private String inicioTempoDirecao;
    @Column(name = "repetir_alerta_tempo_direcao")
    private Boolean repetirAlertaTempoDirecao;
    @Column(name = "tempo_jornada_finalizada", length = 5)
    private String tempoJornadaFinalizada;
    @Column(name = "repetir_alerta_tempo_jornada_finalizada")
    private Boolean repetirAlertaTempoJornadaFinalizada;
    @Column(name = "tempo_refeicao", length = 5)
    private String tempoRefeicao;
    @Column(name = "repetir_alerta_tempo_refeicao")
    private Boolean repetirAlertaTempoRefeicao;
    @Column(name = "jornada_trabalho_extrapolada")
    private Boolean jornadaTrabalhoExtrapolada;
    @Column(name = "horas_extras_acima_duas_horas")
    private Boolean horasExtrasAcimaDuasHoras;
    @Column(name = "horas_extras_acima_quatro_horas")
    private Boolean horasExtrasAcimaQuatroHoras;
    @Column(name = "refeicao_inferior_uma_hora")
    private Boolean refeicaoInferiorUmaHora;
    @Column(name = "descanso_acima_onze_horas")
    private Boolean descansoAcimaOnzeHoras;
    @Column(name = "descanso_inferior_onze_horas")
    private Boolean descansoInferiorOnzeHoras;
    @Column(name = "horas_disposicao_acima_de_trinta_minutos")
    private String  horasDisposicaoAcimadeTrintaMinutos;
    @Column(name = "tempo_de_espera_acima_de_uma_hora")
    private Boolean tempoDeEsperaAcimaDeUmaHora;
    @Column(name = "tempo_de_manutencao_acima_de_uma_hora")
    private Boolean tempoDeManutencaoAcimaDeUmaHora;
    @Column(name = "tempo_de_lanche_acima_de_quinze_minuto")
    private Boolean tempoDeLancheAcimaDeQuinzeMinuto;
    @Column(name = "em_fiscalizacao")
    private Boolean emFiscalizacao;
    @Column(name = "sinistro")
    private Boolean sinistro;
    @Column(name = "pista_interditada")
    private Boolean pistaInterditada;
    @Column(name = "habilitar_limite_refeicao")
    private Boolean habilitarLimiteRefeicao;
    @Column(name = "habilitar_controle_diarias")
    private Boolean habilitarControleDeDiarias;
    @Column(name = "valor_diaria")
    private Double valorDiariaNumerico;
    @Column(name ="forcar_km_viam")
    private Boolean forcarKmViam;
    @Column(name = "forcar_carga_descarga_viam")
    private Boolean forcarCargaDescargaViam;
    @Column(name = "relatorio_resumo_jornada")
    private Boolean relatorioResumoJornada;
    @Column(name = "calcular_feriado")
    private Boolean calcularFeriado;
    @Column(name = "horas_extras_domingo")
    private Boolean calcularHorasExtrasDomingo;
    @Column(name = "habilitar_relatorio_detalhe_apuracao")
    private Boolean habilitarRelatorioDetalheApuracao;
    @Column(name = "quebra_interjornada")
    private Boolean habilitarQuebraInterjornada;
    @Column(name = " habilitar_rm_labore")
    private Boolean habilitarRmLobore;
    @Column(name = "empresa_id")
    private Integer empresaId;
    @Transient
    private String valorDiaria;

}
