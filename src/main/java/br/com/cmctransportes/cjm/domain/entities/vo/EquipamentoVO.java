package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

public class EquipamentoVO implements Serializable {
    private static final long serialVersionUID = 1L;
    @Getter
    @Setter
    private Integer id;
    @Getter
    @Setter
    private Date dataCadastro;
    @Getter
    @Setter
    private String imei;
    @Getter
    @Setter
    private String numeroCelular;
    @Getter
    @Setter
    private String numeroSerial;
    @Getter
    @Setter
    private String observacao;
    @Getter
    @Setter
    private String operadora;
    @Getter
    @Setter
    private Boolean status;
    @Getter
    @Setter
    private String tipoEquipamento;//PR = Proprietario PA = Particular
    @Getter
    @Setter
    private String tipoChip;//PR = Proprietario PA = Particular
    @Getter
    @Setter
    private ModeloVO modelo;
    @Getter
    @Setter
    private Boolean equipamentoAlocado;
    @Getter
    @Setter
    private EmpresaVO empresa;

}
