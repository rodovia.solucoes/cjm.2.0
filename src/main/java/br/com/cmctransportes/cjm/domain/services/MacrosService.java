package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.Macros;
import br.com.cmctransportes.cjm.infra.dao.MacrosDAO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

@Service
public class MacrosService {
    private final EntityManager em;
    private MacrosDAO macroDAO;

    public MacrosService(EntityManager em) {
        this.em = em;
        this.macroDAO = new MacrosDAO(em);
    }

    @Transactional
    public void save(Macros entity) throws Exception {
        entity.setId(null);
        entity.setDataCriacao(new Date());
        this.macroDAO.salvar(entity);
    }

    @Transactional
    public void editar(Macros entity) throws Exception {
        if(entity.getDataCriacao() == null){
            entity.setDataCriacao(new Date());
        }
        this.macroDAO.editar(entity);
    }

    public List<Macros> buscarLista(Integer idEmpresa) throws Exception{
        return this.macroDAO.buscarLista(idEmpresa);
    }

    public Macros buscarPeloId(Integer id) throws Exception{
        return this.macroDAO.buscarPeloId(id);
    }

}
