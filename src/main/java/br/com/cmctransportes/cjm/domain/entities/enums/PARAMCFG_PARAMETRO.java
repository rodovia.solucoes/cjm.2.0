package br.com.cmctransportes.cjm.domain.entities.enums;

/**
 * Representa os Parametros de um Grupo
 *
 * @author Cristhiano Roberto
 */
public enum PARAMCFG_PARAMETRO {

    XMPP_LOGIN,
    XMPP_SENHA,
    XMPP_ENDPOINT,
    XMPP_RESOURCE_USERS,
    DSR_MAXTIME,
    DSR_INTERVAL,
    BOSH_SERVICE_URL,
    COMPENSAR_EM_ESPERA,
    PAGAMENTO_PROPORCIONAL,
    LIMITE_MAX_GOZO;
}
