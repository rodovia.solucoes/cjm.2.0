package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.Transmissao;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class MonitorViaSatVO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer idVeiculo;
    private String placa;
    private Transmissao transmissao;



}
