package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.Justificativa;
import br.com.cmctransportes.cjm.infra.dao.JustificativaDAO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

@Service
public class JustificativaService {

    private JustificativaDAO justificativaDAO;
    private final EntityManager em;

    public JustificativaService(EntityManager em) {
        this.em = em;
        justificativaDAO = new JustificativaDAO(em);
    }


    @Transactional
    public void gravar(Justificativa justificativa){
        this.justificativaDAO.saveSemTransactional(justificativa);
    }
}
