package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;
import java.util.List;

public class RetornoLocalVO extends RetornoVO {

    public RetornoLocalVO() {
    }

    public RetornoLocalVO(Integer codigoRetorno, String mensagem) {
        super(codigoRetorno, mensagem);
    }

    @Getter
    @Setter
    private LocalVO local;
    @Getter
    @Setter
    private List<LocalVO> listaDeLocal;
}
