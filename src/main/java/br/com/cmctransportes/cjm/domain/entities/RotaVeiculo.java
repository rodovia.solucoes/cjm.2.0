package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class RotaVeiculo implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer id;
    private Integer rota;
    private Integer veiculo;
    private Boolean atual;
}
