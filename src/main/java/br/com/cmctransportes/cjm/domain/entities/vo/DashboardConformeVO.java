package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class DashboardConformeVO implements Serializable {
    private List<ConformidadeVO> listaDeConformes;
    private List<ConformidadeVO> listaDeInconformes;
}
