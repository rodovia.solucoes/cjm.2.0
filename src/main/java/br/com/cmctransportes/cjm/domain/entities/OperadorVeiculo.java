package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "operador_veiculo")
@Getter
@Setter
public class OperadorVeiculo implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "operador_veiculo_id_seq", sequenceName = "operador_veiculo_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "operador_veiculo_id_seq")
    @Column(name = "id")
    private Integer id;

    @Column(name = "operador_id", nullable = false)
    private Integer operadorId;

    @Column(name = "data_inicio", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataInicio;

    @Column(name = "veiculo_orion")
    private Integer veiculoOrion;

}
