package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class VelocidadeGraficoVO implements Serializable {
    private static final long serialVersionUID = 1L;
    private int velocidade;
    private Date dataDaTransmissao;
}
