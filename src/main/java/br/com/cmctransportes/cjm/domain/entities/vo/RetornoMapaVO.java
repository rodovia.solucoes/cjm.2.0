package br.com.cmctransportes.cjm.domain.entities.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RetornoMapaVO extends RetornoVO {

    @Getter
    @Setter
    private LatLngVO latLng;
    @Getter
    @Setter
    private List<VeiculoTransmissaoVO> listaVeiculoTransmissao;
    @Getter
    @Setter
    private List<EquipPortatilTransmissaoVO> listaEquipPortatilTransmissao;
    @Getter
    @Setter
    private boolean viasatAtivo = false;

    public RetornoMapaVO() {

    }

    public RetornoMapaVO(Integer codigoRetorno, String mensagem) {
        super(codigoRetorno, mensagem);
    }
}
