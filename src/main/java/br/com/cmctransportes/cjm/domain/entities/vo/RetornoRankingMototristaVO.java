package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RetornoRankingMototristaVO extends RetornoVO {
    private RankingMototristaVO rankingMototrista;
    private List<RelatorioPontuacaoAvancadaVO> listaRelatorioPontuacaoAvancada;
    private List<TemposDeFaixaVO> listaTemposDeFaixa;
}
