package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * Classe para ser enviada para o serviço buscarListaAgregados
 */
@Getter
@Setter
public class TransmissaoServicoVO implements Serializable {
    private Integer idRodovia;
    private String placa;
    private Date dataEvento;
    private Double velocidade;
    private Double latitude;
    private Double longitude;
    private String cidade;
    private String estado;
    private String pontoReferenciaProximo;
}
