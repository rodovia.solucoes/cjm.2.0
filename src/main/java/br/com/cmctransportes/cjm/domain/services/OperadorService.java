package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.Operador;
import br.com.cmctransportes.cjm.domain.entities.OperadorVeiculo;
import br.com.cmctransportes.cjm.infra.dao.EmpresasDAO;
import br.com.cmctransportes.cjm.infra.dao.OperadorDAO;
import br.com.cmctransportes.cjm.infra.dao.OperadorVeiculoDAO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;


@Service
public class OperadorService  {

    private final EntityManager em;
    private final OperadorDAO operadorDAO;
    private final EmpresasDAO empresasDAO;
    private final OperadorVeiculoDAO operadorVeiculoDAO;

    public OperadorService(EntityManager em) {
        this.em = em;
        this.operadorDAO = new OperadorDAO(em);
        this.empresasDAO = new EmpresasDAO(em);
        this.operadorVeiculoDAO = new OperadorVeiculoDAO(em);
    }

    @Transactional
    public void save(Operador entity) throws Exception {
        entity.setId(null);
        entity.setDataCriacao(new Date());
        this.operadorDAO.save(entity);
        try {
            for(OperadorVeiculo operadorVeiculo : entity.getListaOperadorVeiculo()){
                operadorVeiculo.setDataInicio(new Date());
                operadorVeiculo.setOperadorId(entity.getId());
                this.operadorVeiculoDAO.save(operadorVeiculo);
            }
        }catch (Exception e){

        }
    }

    public List<Operador> findAll(Integer idEmpresa, Integer idUnidade) throws Exception{
        return this.operadorDAO.findAll(idEmpresa, idUnidade);
    }

    public Operador getById(Integer id) {
        Operador operador = this.operadorDAO.getById(id);
        List<OperadorVeiculo> lista = this.operadorVeiculoDAO.buscarPeloOperador(operador.getId());
        operador.setListaOperadorVeiculo(lista);
        return operador;
    }

    @Transactional
    public void update(Operador entity) {
        try {
            if(entity.getDataCriacao() == null){
                entity.setDataCriacao(new Date());
            }
            this.operadorDAO.update(entity);

            List<OperadorVeiculo> listaOperadorVeiculo = this.operadorVeiculoDAO.buscarPeloOperador(entity.getId());
            for (OperadorVeiculo operadorVeiculo : listaOperadorVeiculo) {
                this.operadorVeiculoDAO.delete(operadorVeiculo);
            }
            for (OperadorVeiculo ov : entity.getListaOperadorVeiculo()) {
                ov.setId(null);
                ov.setOperadorId(entity.getId());
                ov.setDataInicio(new Date());
                this.operadorVeiculoDAO.save(ov);
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

}
