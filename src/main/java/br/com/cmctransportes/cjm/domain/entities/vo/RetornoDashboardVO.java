package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.FotoViaM;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RetornoDashboardVO extends RetornoVO{
    private DetalheDashboardVO detalheDashboard;
    private List<FotoViaM> listaFotoViaM;
    private List<TimelineVO> listaTimelineVO;
    private List<RotaVO> listaDeRota;
    private TelemetriaVO telemetria;
    private List<TemperaturaVO> listaDeTemperatura;
    private List<PortasDigitaisVO> listaPortasDigitais;
}
