package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.Transmissao;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class RetornoMonitoramentoVO extends RetornoVO {

    @Getter
    @Setter
    private List<VeiculoMonitoramentoVO> listaVeiculoMonitoramento;
    @Getter
    @Setter
    private DadoTelemetriaVO dadoTelemetria;
    @Getter
    @Setter
    private List<Transmissao> listaDePontosPercorridos;
}
