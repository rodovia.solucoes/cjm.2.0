package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.Motoristas;
import br.com.cmctransportes.cjm.utils.TimeHelper;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Map;

/**
 * @author William Leite
 */
public class InterjourneyAnalysisVO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private Map<TimeHelper.Period, Long> normalPay;
    @Getter
    @Setter
    private Map<TimeHelper.Period, Long> extraPay;
    @Getter
    @Setter
    private Map<TimeHelper.Period, Long> nightPay;
    @Getter
    @Setter
    private Motoristas driver;
}