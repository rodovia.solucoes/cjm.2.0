package br.com.cmctransportes.cjm.domain.entities.vo;

import java.io.Serializable;

/**
 * @author William
 */
public class RESTResponseVO<T> implements Serializable {

    private String error;
    private T response;

    public RESTResponseVO(String error, T response) {
        this.error = error;
        this.response = response;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public T getResponse() {
        return response;
    }

    public void setResponse(T response) {
        this.response = response;
    }
}
