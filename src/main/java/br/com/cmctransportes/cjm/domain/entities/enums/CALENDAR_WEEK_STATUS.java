package br.com.cmctransportes.cjm.domain.entities.enums;

/**
 * @author William Leite
 */
public enum CALENDAR_WEEK_STATUS {
    OK,
    ERROR
}