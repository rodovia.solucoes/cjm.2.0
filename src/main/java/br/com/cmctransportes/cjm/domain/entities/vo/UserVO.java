package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.constants.Constantes;
import br.com.cmctransportes.cjm.domain.entities.*;
import br.com.cmctransportes.cjm.domain.services.EmpresasService;
import br.com.cmctransportes.cjm.domain.services.UnidadesService;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type;
import lombok.Getter;
import lombok.Setter;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author William Leite
 */

public class UserVO {

    private Integer id;
    private String userName;
    private String password;
    private String criptoPassword;
    private String email;
    private List<IDNameTupleVO> companyList;
    private List<IDNameTupleVO> branchList;
    @Getter
    @Setter
    private Map<String, List<IDNameTupleVO>> branchMap;
    private List<IDNameTupleVO> claimList;
    private Boolean situacao;


    private Boolean receberAlertasEventos;

    private Boolean mandarEmail;

    private Boolean mandarSms;

    private Boolean mandarSmsExcessoVelocidade;

    private String numeroCelularEnvioSms;

    private String tempoAtualizacaoDashboard;


    private Boolean consultaJornada = Boolean.FALSE;
    private Boolean consulataGradeJornada = Boolean.FALSE;
    private Boolean relatorioViamMarcacao = Boolean.FALSE;
    private Boolean relatorioViamChat = Boolean.FALSE;
    private Boolean relatorioViamEvento = Boolean.FALSE;
    private Boolean relatorioViasarCamaraFria = Boolean.FALSE;
    private Boolean relatorioViasatDisponibilidade = Boolean.FALSE;
    private Boolean relatorioViaSarDispositivoPortatil = Boolean.FALSE;
    private Boolean relatorioViaSatEvento = Boolean.FALSE;
    private Boolean relatorioViaSatHorimetro = Boolean.FALSE;
    private Boolean relatorioViaSatHodometro = Boolean.FALSE;
    private Boolean ralatorioViaSatPercurso = Boolean.FALSE;
    private Boolean relatorioViaSatVelocidade = Boolean.FALSE;
    private Boolean relatorioViaSatTomadaForca = Boolean.FALSE;
    private Boolean cadastroUnidade = Boolean.FALSE;
    private Boolean cadastroMotorista = Boolean.FALSE;
    private Boolean cadastroVeiculo = Boolean.FALSE;
    private Boolean cadastroLocal = Boolean.FALSE;
    private Boolean cadastroRota = Boolean.FALSE;
    private Boolean cadastroFeriados = Boolean.FALSE;
    private Boolean cadastroUsuario = Boolean.FALSE;
    private Boolean cadastroCor = Boolean.FALSE;
    private Boolean cadastroTolerancia = Boolean.FALSE;
    private Boolean cadastroTurno = Boolean.FALSE;
    private Boolean cadastroMotivo = Boolean.FALSE;
    private Boolean cadastroEquipamentoPortatil = Boolean.FALSE;
    private Boolean cadastroTipoVeiculo = Boolean.FALSE;
    private Boolean cadastroModeloVeiculo = Boolean.FALSE;
    private Boolean cadastroTipoEquipamento = Boolean.FALSE;
    private Boolean acessarIntegracao = Boolean.FALSE;
    private Boolean acessarViam = Boolean.FALSE;
    private Boolean acessarViaSat = Boolean.FALSE;
    private Boolean acessarRelatorioDiariaMotorista = Boolean.FALSE;
    private Boolean acessarCadastroDiariaMotorista = Boolean.FALSE;
    private Boolean acessarMacro = Boolean.FALSE;
    private Boolean acessarOperador = Boolean.FALSE;
    private Boolean acessarHodometro = Boolean.FALSE;
    private Boolean acessarRelatorioPresencaEquipamento = Boolean.FALSE;
    private Boolean acessarRelatorioTransporteEquipamento = Boolean.FALSE;
    private Boolean acessarRelatorioSensorBetoneira = Boolean.FALSE;
    private Boolean acessarControleCombustivel = Boolean.FALSE;
    private Boolean acessarGestao = Boolean.FALSE;
    private Boolean cadastrarTipoRanking = Boolean.FALSE;
    private Boolean acessoRankingMotorista = Boolean.FALSE;
    private Boolean acessoSbh = Boolean.FALSE;
    private Boolean relatorioViamEventoMotorista = Boolean.FALSE;
    private Boolean habilitarAutenticacao = Boolean.FALSE;
    private Boolean relatorioPontosPercorridos = Boolean.FALSE;
    private Boolean relatorioProdutividadeVeiculo = Boolean.FALSE;
    private Boolean acessarDesempenhoVeiculo = Boolean.FALSE;
    private Boolean acessarDetalhesMonitoramento = Boolean.FALSE;
    private Boolean acessarMotoristaTelaMonitoramento = Boolean.FALSE;
    private Boolean agruparVeiculosMapaMonitoramento = Boolean.FALSE;

    public UserVO() {
    }

    public UserVO(Users user) {

        this(user.getId(), user.getUserName(), user.getSenha(), user.getSenhaCripto(), user.getEmail(), user.getUsersBranch(), user.getUsersCompany(), user.getUsersClaims(), user.getSituacao(), (user.getReceberAlertasEventos() == null ? false : user.getReceberAlertasEventos()), (user.getMandarEmail() == null ? false : user.getMandarEmail()), (user.getMandarSms() == null ? false : user.getMandarSms()), (user.getMandarSmsExcessoVelocidade() == null ? false : user.getMandarSmsExcessoVelocidade()), user.getNumeroCelularEnvioSms(), user.getTempoAtualizacaoDashboard(),
                user.getConsultaJornada() == null ? Boolean.FALSE : user.getConsultaJornada(),
                user.getConsulataGradeJornada() == null ? Boolean.FALSE : user.getConsulataGradeJornada(),
                user.getRelatorioViamMarcacao() == null ? Boolean.FALSE : user.getRelatorioViamMarcacao(),
                user.getRelatorioViamChat() == null ? Boolean.FALSE : user.getRelatorioViamChat(),
                user.getRelatorioViamEvento() == null ? Boolean.FALSE : user.getRelatorioViamEvento(),
                user.getRelatorioViasarCamaraFria() == null ? Boolean.FALSE : user.getRelatorioViasarCamaraFria(),
                user.getRelatorioViasatDisponibilidade() == null ? Boolean.FALSE : user.getRelatorioViasatDisponibilidade(),
                user.getRelatorioViaSarDispositivoPortatil() == null ? Boolean.FALSE : user.getRelatorioViaSarDispositivoPortatil(),
                user.getRelatorioViaSatEvento() == null ? Boolean.FALSE : user.getRelatorioViaSatEvento(),
                user.getRelatorioViaSatHorimetro() == null ? Boolean.FALSE : user.getRelatorioViaSatHorimetro(),
                user.getRelatorioViaSatHodometro() == null ? Boolean.FALSE : user.getRelatorioViaSatHodometro(),
                user.getRalatorioViaSatPercurso() == null ? Boolean.FALSE : user.getRalatorioViaSatPercurso(),
                user.getRelatorioViaSatVelocidade() == null ? Boolean.FALSE : user.getRelatorioViaSatVelocidade(),
                user.getRelatorioViaSatTomadaForca() == null ? Boolean.FALSE : user.getRelatorioViaSatTomadaForca(),
                user.getCadastroUnidade() == null ? Boolean.FALSE : user.getCadastroUnidade(),
                user.getCadastroMotorista() == null ? Boolean.FALSE : user.getCadastroMotorista(),
                user.getCadastroVeiculo() == null ? Boolean.FALSE : user.getCadastroVeiculo(),
                user.getCadastroLocal() == null ? Boolean.FALSE : user.getCadastroLocal(),
                user.getCadastroFeriados() == null ? Boolean.FALSE : user.getCadastroFeriados(),
                user.getCadastroUsuario() == null ? Boolean.FALSE : user.getCadastroUsuario(),
                user.getCadastroCor() == null ? Boolean.FALSE : user.getCadastroCor(),
                user.getCadastroTolerancia() == null ? Boolean.FALSE : user.getCadastroTolerancia(),
                user.getCadastroTurno() == null ? Boolean.FALSE : user.getCadastroTurno(),
                user.getCadastroMotivo() == null ? Boolean.FALSE : user.getCadastroMotivo(),
                user.getCadastroEquipamentoPortatil() == null ? Boolean.FALSE : user.getCadastroEquipamentoPortatil(),
                user.getCadastroTipoVeiculo() == null ? Boolean.FALSE : user.getCadastroTipoVeiculo(),
                user.getCadastroModeloVeiculo() == null ? Boolean.FALSE : user.getCadastroModeloVeiculo(),
                user.getCadastroTipoEquipamento() == null ? Boolean.FALSE : user.getCadastroTipoEquipamento(),
                user.getAcessarIntegracao() == null ? Boolean.FALSE : user.getAcessarIntegracao(),
                user.getAcessarViam() == null ? Boolean.FALSE : user.getAcessarViam(),
                user.getAcessarViaSat() == null ? Boolean.FALSE : user.getAcessarViaSat(),
                user.getAcessarRelatorioDiariaMotorista() == null ? Boolean.FALSE : user.getAcessarRelatorioDiariaMotorista(),
                user.getAcessarCadastroDiariaMotorista() == null ? Boolean.FALSE : user.getAcessarCadastroDiariaMotorista(),
                user.getAcessarMacro() == null ? Boolean.FALSE: user.getAcessarMacro(),
                user.getAcessarOperador() == null? Boolean.FALSE: user.getAcessarOperador(),
                user.getAcessarHodometro() == null? Boolean.FALSE: user.getAcessarHodometro(),
                user.getAcessarRelatorioPresencaEquipamento() == null? Boolean.FALSE: user.getAcessarRelatorioPresencaEquipamento(),
                user.getAcessarRelatorioTransporteEquipamento() == null?Boolean.FALSE:user.getAcessarRelatorioTransporteEquipamento(),
                user.getAcessarRelatorioSensorBetoneira() == null?Boolean.FALSE:user.getAcessarRelatorioSensorBetoneira(),
                user.getAcessarControleCombustivel() == null?Boolean.FALSE:user.getAcessarControleCombustivel(),
                user.getAcessarGestao() == null?Boolean.FALSE:user.getAcessarGestao(),
                user.getCadastroRota() == null?Boolean.FALSE:user.getCadastroRota(),
                user.getCadastrarTipoRanking() == null?Boolean.FALSE:user.getCadastrarTipoRanking(),
                user.getAcessoRankingMotorista() == null?Boolean.FALSE:user.getAcessoRankingMotorista(),
                user.getAcessoSbh() == null?Boolean.FALSE:user.getAcessoSbh(),
                user.getRelatorioViamEventoMotorista() == null?Boolean.FALSE:user.getRelatorioViamEventoMotorista(),
                user.getHabilitarAutenticacao() == null?Boolean.FALSE:user.getHabilitarAutenticacao(),
                user.getRelatorioPontosPercorridos() == null?Boolean.FALSE:user.getRelatorioPontosPercorridos(),
                user.getRelatorioProdutividadeVeiculo() == null?Boolean.FALSE:user.getRelatorioProdutividadeVeiculo(),
                user.getAcessarDesempenhoVeiculo() == null?Boolean.FALSE:user.getAcessarDesempenhoVeiculo(),
                user.getAcessarDetalhesMonitoramento() == null?Boolean.FALSE:user.getAcessarDetalhesMonitoramento(),
                user.getAcessarMotoristaTelaMonitoramento() == null?Boolean.FALSE:user.getAcessarMotoristaTelaMonitoramento(),
                user.getAgruparVeiculosMapaMonitoramento() == null?Boolean.FALSE:user.getAgruparVeiculosMapaMonitoramento()
                );
    }

    public UserVO(Integer id, String userName, String userPassword, String userCriptoPassword, String email, Collection<UsersBranch> branches, Collection<UsersCompany> companies, Collection<UsersClaims> claims, boolean situacao, boolean receberAlertasEventos, boolean mandarEmail, boolean mandarSms, boolean mandarSmsExcessoVelocidade, String numeroCelularEnvioSms, String tempoAtualizacaoDashboard, Boolean consultaJornada,
                  Boolean consulataGradeJornada,
                  Boolean relatorioViamMarcacao,
                  Boolean relatorioViamChat,
                  Boolean relatorioViamEvento,
                  Boolean relatorioViasarCamaraFria,
                  Boolean relatorioViasatDisponibilidade,
                  Boolean relatorioViaSarDispositivoPortatil,
                  Boolean relatorioViaSatEvento,
                  Boolean relatorioViaSatHorimetro,
                  Boolean relatorioViaSatHodometro,
                  Boolean ralatorioViaSatPercurso,
                  Boolean relatorioViaSatVelocidade,
                  Boolean relatorioViaSatTomadaForca,
                  Boolean cadastroUnidade,
                  Boolean cadastroMotorista,
                  Boolean cadastroVeiculo,
                  Boolean cadastroLocal,
                  Boolean cadastroFeriados,
                  Boolean cadastroUsuario,
                  Boolean cadastroCor,
                  Boolean cadastroTolerancia,
                  Boolean cadastroTurno,
                  Boolean cadastroMotivo,
                  Boolean cadastroEquipamentoPortatil,
                  Boolean cadastroTipoVeiculo,
                  Boolean cadastroModeloVeiculo,
                  Boolean cadastroTipoEquipamento,
                  Boolean acessarIntegracao,
                  Boolean acessarViam,
                  Boolean acessarViaSat,
                  Boolean acessarRelatorioDiariaMotorista,
                  Boolean acessarCadastroDiariaMotorista,
                  Boolean acessarMacro,
                  Boolean acessarOperador,
                  Boolean acessarHodometro,
                  Boolean acessarRelatorioPresencaEquipamento,
                  Boolean acessarRelatorioTransporteEquipamento,
                  Boolean acessarRelatorioSensorBetoneira,
                  Boolean acessarControleCombustivel,
                  Boolean acessarGestao,
                  Boolean cadastroRota,
                  Boolean cadastrarTipoRanking,
                  Boolean acessoRankingMotorista,
                  Boolean acessoSbh,
                  Boolean relatorioViamEventoMotorista,
                  Boolean habilitarAutenticacao,
                  Boolean relatorioPontosPercorridos,
                  Boolean relatorioProdutividadeVeiculo,
                  Boolean acessarDesempenhoVeiculo,
                  Boolean acessarDetalhesMonitoramento,
                  Boolean acessarMotoristaTelaMonitoramento,
                  Boolean agruparVeiculosMapaMonitoramento
                  ) {
        this.id = id;
        this.userName = userName;
        this.password = userPassword;
        this.criptoPassword = userCriptoPassword;
        this.email = email;
        this.situacao = situacao;

        this.receberAlertasEventos = receberAlertasEventos;
        this.mandarEmail = mandarEmail;
        this.mandarSms = mandarSms;
        this.mandarSmsExcessoVelocidade = mandarSmsExcessoVelocidade;
        this.numeroCelularEnvioSms = numeroCelularEnvioSms;
        this.tempoAtualizacaoDashboard = tempoAtualizacaoDashboard;

        boolean isAdmin = false;

        if (claims != null) {
            this.claimList = new ArrayList<>(claims.stream()
                    .map(c -> new IDNameTupleVO(c.getId(), c.getClaim()))
                    .collect(Collectors.toList()));

            isAdmin = this.claimList.stream().anyMatch(s -> Constantes.ADMIN.equalsIgnoreCase(s.getName()) || Constantes.APURACAO.equalsIgnoreCase(s.getName()));
        }

        if (companies != null) {
            this.companyList = new ArrayList<>(companies.stream()
                    .map(c -> new IDNameTupleVO(c))
                    .collect(Collectors.toList()));

            if (!isAdmin) {
                this.companyList.removeIf(c -> c.getId() == 0);
            }
        }

        this.branchMap = new HashMap<>(0);
        if (branches != null) {
            this.branchList = new ArrayList<>(branches.stream()
                    .map(b -> new IDNameTupleVO(b.getBranch().getId(), b.getId(), b.getBranch().getIdentificacao()))
                    .collect(Collectors.toList()));

            branches.stream().forEach((branch) -> {
                String empresa = branch.getBranch().getEmpresaId().getNome();
                if (!this.branchMap.containsKey(empresa)) {
                    this.branchMap.put(empresa, new ArrayList<>(0));
                }

                this.branchMap.get(empresa).add(new IDNameTupleVO(branch.getBranch().getId(), branch.getId(), branch.getBranch().getIdentificacao()));
            });
            if (isAdmin) {
                this.branchMap.entrySet().stream().forEach((entry) -> {
                    entry.getValue().add(0, new IDNameTupleVO(0, "Todas"));
                });
            } else {
                this.branchList.removeIf(c -> c.getId() == 0);
            }

        } else {
            this.branchList = new ArrayList<>(0);
        }

        this.consultaJornada = consultaJornada;
        this.consulataGradeJornada = consulataGradeJornada;
        this.relatorioViamMarcacao = relatorioViamMarcacao;
        this.relatorioViamChat = relatorioViamChat;
        this.relatorioViamEvento = relatorioViamEvento;
        this.relatorioViasarCamaraFria = relatorioViasarCamaraFria;
        this.relatorioViasatDisponibilidade = relatorioViasatDisponibilidade;
        this.relatorioViaSarDispositivoPortatil = relatorioViaSarDispositivoPortatil;
        this.relatorioViaSatEvento = relatorioViaSatEvento;
        this.relatorioViaSatHorimetro = relatorioViaSatHorimetro;
        this.relatorioViaSatHodometro = relatorioViaSatHodometro;
        this.ralatorioViaSatPercurso = ralatorioViaSatPercurso;
        this.relatorioViaSatVelocidade = relatorioViaSatVelocidade;
        this.relatorioViaSatTomadaForca = relatorioViaSatTomadaForca;
        this.cadastroUnidade = cadastroUnidade;
        this.cadastroMotorista = cadastroMotorista;
        this.cadastroVeiculo = cadastroVeiculo;
        this.cadastroLocal = cadastroLocal;
        this.cadastroFeriados = cadastroFeriados;
        this.cadastroUsuario = cadastroUsuario;
        this.cadastroCor = cadastroCor;
        this.cadastroTolerancia = cadastroTolerancia;
        this.cadastroTurno = cadastroTurno;
        this.cadastroMotivo = cadastroMotivo;
        this.cadastroEquipamentoPortatil = cadastroEquipamentoPortatil;
        this.cadastroTipoVeiculo = cadastroTipoVeiculo;
        this.cadastroModeloVeiculo = cadastroModeloVeiculo;
        this.cadastroTipoEquipamento = cadastroTipoEquipamento;
        this.acessarIntegracao = acessarIntegracao;
        this.acessarViam = acessarViam;
        this.acessarViaSat = acessarViaSat;
        this.acessarRelatorioDiariaMotorista = acessarRelatorioDiariaMotorista;
        this.acessarCadastroDiariaMotorista = acessarCadastroDiariaMotorista;
        this.acessarMacro = acessarMacro;
        this.acessarOperador = acessarOperador;
        this.acessarHodometro = acessarHodometro;
        this.acessarRelatorioPresencaEquipamento = acessarRelatorioPresencaEquipamento;
        this.acessarRelatorioTransporteEquipamento = acessarRelatorioTransporteEquipamento;
        this.acessarRelatorioSensorBetoneira = acessarRelatorioSensorBetoneira;
        this.acessarControleCombustivel = acessarControleCombustivel;
        this.acessarGestao = acessarGestao;
        this.cadastroRota = cadastroRota;
        this.cadastrarTipoRanking = cadastrarTipoRanking;
        this.acessoRankingMotorista = acessoRankingMotorista;
        this.acessoSbh = acessoSbh;
        this.relatorioViamEventoMotorista = relatorioViamEventoMotorista;
        this.habilitarAutenticacao = habilitarAutenticacao;
        this.relatorioPontosPercorridos = relatorioPontosPercorridos;
        this.relatorioProdutividadeVeiculo = relatorioProdutividadeVeiculo;
        this.acessarDesempenhoVeiculo = acessarDesempenhoVeiculo;
        this.acessarDetalhesMonitoramento = acessarDetalhesMonitoramento;
        this.acessarMotoristaTelaMonitoramento = acessarMotoristaTelaMonitoramento;
        this.agruparVeiculosMapaMonitoramento = agruparVeiculosMapaMonitoramento;
    }

    public Users revert(Integer company, Integer branch, UnidadesService unitsService, EmpresasService companyService) {
        Users user = new Users();
        user.setEmail(this.email);
        user.setId(this.id);
        user.setSenha(this.password);
        user.setUserName(this.userName);
        user.setSenhaCripto(this.criptoPassword);
        user.setSituacao(this.situacao == null ? true : this.situacao);
        user.setReceberAlertasEventos(this.receberAlertasEventos == null ? false : this.receberAlertasEventos);
        user.setMandarEmail(this.mandarEmail == null ? false : this.mandarEmail);
        user.setMandarSms(this.mandarSms == null ? false : this.mandarSms);
        user.setMandarSmsExcessoVelocidade(this.mandarSmsExcessoVelocidade == null ? false : this.mandarSmsExcessoVelocidade);
        user.setNumeroCelularEnvioSms(this.numeroCelularEnvioSms);
        user.setTempoAtualizacaoDashboard(this.tempoAtualizacaoDashboard);
        user.setConsultaJornada(this.consultaJornada);
        user.setConsulataGradeJornada(this.consulataGradeJornada);
        user.setRelatorioViamMarcacao(this.relatorioViamMarcacao);
        user.setRelatorioViamChat(this.relatorioViamChat);
        user.setRelatorioViamEvento(this.relatorioViamEvento);
        user.setRelatorioViasarCamaraFria(this.relatorioViasarCamaraFria);
        user.setRelatorioViasatDisponibilidade(this.relatorioViasatDisponibilidade);
        user.setRelatorioViaSarDispositivoPortatil(this.relatorioViaSarDispositivoPortatil);
        user.setRelatorioViaSatEvento(this.relatorioViaSatEvento);
        user.setRelatorioViaSatHorimetro(this.relatorioViaSatHorimetro);
        user.setRelatorioViaSatHodometro(this.relatorioViaSatHodometro);
        user.setRalatorioViaSatPercurso(this.ralatorioViaSatPercurso);
        user.setRelatorioViaSatVelocidade(this.relatorioViaSatVelocidade);
        user.setRelatorioViaSatTomadaForca(this.relatorioViaSatTomadaForca);
        user.setCadastroUnidade(this.cadastroUnidade);
        user.setCadastroMotorista(this.cadastroMotorista);
        user.setCadastroVeiculo(this.cadastroVeiculo);
        user.setCadastroLocal(this.cadastroLocal);
        user.setCadastroFeriados(this.cadastroFeriados);
        user.setCadastroUsuario(this.cadastroUsuario);
        user.setCadastroCor(this.cadastroCor);
        user.setCadastroTolerancia(this.cadastroTolerancia);
        user.setCadastroTurno(this.cadastroTurno);
        user.setCadastroMotivo(this.cadastroMotivo);
        user.setCadastroEquipamentoPortatil(this.cadastroEquipamentoPortatil);
        user.setCadastroTipoVeiculo(this.cadastroTipoVeiculo);
        user.setCadastroModeloVeiculo(this.cadastroModeloVeiculo);
        user.setCadastroTipoEquipamento(this.cadastroTipoEquipamento);
        user.setAcessarIntegracao(this.acessarIntegracao);
        user.setAcessarViam(this.acessarViam);
        user.setAcessarViaSat(this.acessarViaSat);
        user.setAcessarCadastroDiariaMotorista(this.acessarCadastroDiariaMotorista);
        user.setAcessarRelatorioDiariaMotorista(this.acessarRelatorioDiariaMotorista);
        user.setAcessarMacro(this.acessarMacro);
        user.setAcessarOperador(this.acessarOperador);
        user.setAcessarHodometro(this.acessarHodometro);
        user.setAcessarRelatorioPresencaEquipamento(this.acessarRelatorioPresencaEquipamento);
        user.setAcessarRelatorioTransporteEquipamento(this.acessarRelatorioTransporteEquipamento);
        user.setAcessarRelatorioSensorBetoneira(this.acessarRelatorioSensorBetoneira);
        user.setAcessarControleCombustivel(this.acessarControleCombustivel);
        user.setAcessarGestao(this.acessarGestao);
        user.setCadastroRota(this.cadastroRota);
        user.setCadastrarTipoRanking(this.cadastrarTipoRanking);
        user.setAcessoRankingMotorista(this.acessoRankingMotorista);
        user.setAcessoSbh(this.acessoSbh);
        user.setRelatorioViamEventoMotorista(this.relatorioViamEventoMotorista);
        user.setHabilitarAutenticacao(this.habilitarAutenticacao);
        user.setRelatorioPontosPercorridos(this.relatorioPontosPercorridos);
        user.setRelatorioProdutividadeVeiculo(this.relatorioProdutividadeVeiculo);
        user.setAcessarDesempenhoVeiculo(this.acessarDesempenhoVeiculo);
        user.setAcessarDetalhesMonitoramento(this.acessarDetalhesMonitoramento);
        user.setAcessarMotoristaTelaMonitoramento(this.acessarMotoristaTelaMonitoramento);
        user.setAgruparVeiculosMapaMonitoramento(this.agruparVeiculosMapaMonitoramento);

        if (Objects.nonNull(this.branchList)) {
            user.setUsersBranch(new ArrayList<>(
                    this.branchList.stream()
                            .map(b -> new UsersBranch(b.getTernaryID(), unitsService.getById(b.getId()), user))
                            .collect(Collectors.toList())));
        } else {
            user.setUsersBranch(new ArrayList<>());
            user.getUsersBranch().add(new UsersBranch(null, new Unidades(branch), user));
        }
        if (Objects.nonNull(this.claimList)) {
            user.setUsersClaims(new ArrayList<>(
                    this.claimList.stream()
                            .map(c -> new UsersClaims(c.getId() == 0 ? null : c.getId(), c.getName(), user))
                            .collect(Collectors.toList())));
        }
        if (Objects.nonNull(this.companyList)) {
            user.setUsersCompany(new ArrayList<>(this.companyList.stream()
                    .map(c -> new UsersCompany(c.getTernaryID(), companyService.getById(c.getId()), user))
                    .collect(Collectors.toList())));
        } else {
            user.setUsersCompany(new ArrayList<>());
            user.getUsersCompany().add(new UsersCompany(null, new Empresas(company), user));
        }
        return user;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<IDNameTupleVO> getCompanyList() {
        return companyList;
    }

    public void setCompanyList(List<IDNameTupleVO> companyList) {
        this.companyList = companyList;
    }

    public List<IDNameTupleVO> getBranchList() {
        return branchList;
    }

    public void setBranchList(List<IDNameTupleVO> branchList) {
        this.branchList = branchList;
    }

    public List<IDNameTupleVO> getClaimList() {
        return claimList;
    }

    public void setClaimList(List<IDNameTupleVO> claimList) {
        this.claimList = claimList;
    }

    public Boolean getReceberAlertasEventos() {
        return receberAlertasEventos;
    }

    public void setReceberAlertasEventos(Boolean receberAlertasEventos) {
        this.receberAlertasEventos = receberAlertasEventos;
    }

    public Boolean getMandarEmail() {
        return mandarEmail;
    }

    public void setMandarEmail(Boolean mandarEmail) {
        this.mandarEmail = mandarEmail;
    }

    public Boolean getMandarSms() {
        return mandarSms;
    }

    public void setMandarSms(Boolean mandarSms) {
        this.mandarSms = mandarSms;
    }

    public Boolean getMandarSmsExcessoVelocidade() {
        return mandarSmsExcessoVelocidade;
    }

    public void setMandarSmsExcessoVelocidade(Boolean mandarSmsExcessoVelocidade) {
        this.mandarSmsExcessoVelocidade = mandarSmsExcessoVelocidade;
    }

    public String getNumeroCelularEnvioSms() {
        return numeroCelularEnvioSms;
    }

    public void setNumeroCelularEnvioSms(String numeroCelularEnvioSms) {
        this.numeroCelularEnvioSms = numeroCelularEnvioSms;
    }

    @Override
    public String toString() {
        return "UserVO{" + "id=" + id + ", userName=" + userName + ", companyList=" + companyList + ", branchList=" + branchList + ", claimList=" + claimList + '}';
    }

    /**
     * @return the criptoPassword
     */
    public String getCriptoPassword() {
        return criptoPassword;
    }

    /**
     * @param criptoPassword the criptoPassword to set
     */
    public void setCriptoPassword(String criptoPassword) {
        this.criptoPassword = criptoPassword;
    }

    public Boolean getSituacao() {
        return situacao;
    }

    public void setSituacao(Boolean situacao) {
        this.situacao = situacao;
    }

    public String getTempoAtualizacaoDashboard() {
        return tempoAtualizacaoDashboard;
    }

    public void setTempoAtualizacaoDashboard(String tempoAtualizacaoDashboard) {
        this.tempoAtualizacaoDashboard = tempoAtualizacaoDashboard;
    }

    public Boolean getConsultaJornada() {
        return consultaJornada;
    }

    public void setConsultaJornada(Boolean consultaJornada) {
        this.consultaJornada = consultaJornada;
    }

    public Boolean getConsulataGradeJornada() {
        return consulataGradeJornada;
    }

    public void setConsulataGradeJornada(Boolean consulataGradeJornada) {
        this.consulataGradeJornada = consulataGradeJornada;
    }

    public Boolean getRelatorioViamMarcacao() {
        return relatorioViamMarcacao;
    }

    public void setRelatorioViamMarcacao(Boolean relatorioViamMarcacao) {
        this.relatorioViamMarcacao = relatorioViamMarcacao;
    }

    public Boolean getRelatorioViamChat() {
        return relatorioViamChat;
    }

    public void setRelatorioViamChat(Boolean relatorioViamChat) {
        this.relatorioViamChat = relatorioViamChat;
    }

    public Boolean getRelatorioViamEvento() {
        return relatorioViamEvento;
    }

    public void setRelatorioViamEvento(Boolean relatorioViamEvento) {
        this.relatorioViamEvento = relatorioViamEvento;
    }

    public Boolean getRelatorioViasarCamaraFria() {
        return relatorioViasarCamaraFria;
    }

    public void setRelatorioViasarCamaraFria(Boolean relatorioViasarCamaraFria) {
        this.relatorioViasarCamaraFria = relatorioViasarCamaraFria;
    }

    public Boolean getRelatorioViasatDisponibilidade() {
        return relatorioViasatDisponibilidade;
    }

    public void setRelatorioViasatDisponibilidade(Boolean relatorioViasatDisponibilidade) {
        this.relatorioViasatDisponibilidade = relatorioViasatDisponibilidade;
    }

    public Boolean getRelatorioViaSarDispositivoPortatil() {
        return relatorioViaSarDispositivoPortatil;
    }

    public void setRelatorioViaSarDispositivoPortatil(Boolean relatorioViaSarDispositivoPortatil) {
        this.relatorioViaSarDispositivoPortatil = relatorioViaSarDispositivoPortatil;
    }

    public Boolean getRelatorioViaSatEvento() {
        return relatorioViaSatEvento;
    }

    public void setRelatorioViaSatEvento(Boolean relatorioViaSatEvento) {
        this.relatorioViaSatEvento = relatorioViaSatEvento;
    }

    public Boolean getRelatorioViaSatHorimetro() {
        return relatorioViaSatHorimetro;
    }

    public void setRelatorioViaSatHorimetro(Boolean relatorioViaSatHorimetro) {
        this.relatorioViaSatHorimetro = relatorioViaSatHorimetro;
    }

    public Boolean getRelatorioViaSatHodometro() {
        return relatorioViaSatHodometro;
    }

    public void setRelatorioViaSatHodometro(Boolean relatorioViaSatHodometro) {
        this.relatorioViaSatHodometro = relatorioViaSatHodometro;
    }

    public Boolean getRalatorioViaSatPercurso() {
        return ralatorioViaSatPercurso;
    }

    public void setRalatorioViaSatPercurso(Boolean ralatorioViaSatPercurso) {
        this.ralatorioViaSatPercurso = ralatorioViaSatPercurso;
    }

    public Boolean getRelatorioViaSatVelocidade() {
        return relatorioViaSatVelocidade;
    }

    public void setRelatorioViaSatVelocidade(Boolean relatorioViaSatVelocidade) {
        this.relatorioViaSatVelocidade = relatorioViaSatVelocidade;
    }

    public Boolean getRelatorioViaSatTomadaForca() {
        return relatorioViaSatTomadaForca;
    }

    public void setRelatorioViaSatTomadaForca(Boolean relatorioViaSatTomadaForca) {
        this.relatorioViaSatTomadaForca = relatorioViaSatTomadaForca;
    }

    public Boolean getCadastroUnidade() {
        return cadastroUnidade;
    }

    public void setCadastroUnidade(Boolean cadastroUnidade) {
        this.cadastroUnidade = cadastroUnidade;
    }

    public Boolean getCadastroMotorista() {
        return cadastroMotorista;
    }

    public void setCadastroMotorista(Boolean cadastroMotorista) {
        this.cadastroMotorista = cadastroMotorista;
    }

    public Boolean getCadastroVeiculo() {
        return cadastroVeiculo;
    }

    public void setCadastroVeiculo(Boolean cadastroVeiculo) {
        this.cadastroVeiculo = cadastroVeiculo;
    }

    public Boolean getCadastroLocal() {
        return cadastroLocal;
    }

    public void setCadastroLocal(Boolean cadastroLocal) {
        this.cadastroLocal = cadastroLocal;
    }

    public Boolean getCadastroFeriados() {
        return cadastroFeriados;
    }

    public void setCadastroFeriados(Boolean cadastroFeriados) {
        this.cadastroFeriados = cadastroFeriados;
    }

    public Boolean getCadastroUsuario() {
        return cadastroUsuario;
    }

    public void setCadastroUsuario(Boolean cadastroUsuario) {
        this.cadastroUsuario = cadastroUsuario;
    }

    public Boolean getCadastroCor() {
        return cadastroCor;
    }

    public void setCadastroCor(Boolean cadastroCor) {
        this.cadastroCor = cadastroCor;
    }

    public Boolean getCadastroTolerancia() {
        return cadastroTolerancia;
    }

    public void setCadastroTolerancia(Boolean cadastroTolerancia) {
        this.cadastroTolerancia = cadastroTolerancia;
    }

    public Boolean getCadastroTurno() {
        return cadastroTurno;
    }

    public void setCadastroTurno(Boolean cadastroTurno) {
        this.cadastroTurno = cadastroTurno;
    }

    public Boolean getCadastroMotivo() {
        return cadastroMotivo;
    }

    public void setCadastroMotivo(Boolean cadastroMotivo) {
        this.cadastroMotivo = cadastroMotivo;
    }

    public Boolean getCadastroEquipamentoPortatil() {
        return cadastroEquipamentoPortatil;
    }

    public void setCadastroEquipamentoPortatil(Boolean cadastroEquipamentoPortatil) {
        this.cadastroEquipamentoPortatil = cadastroEquipamentoPortatil;
    }

    public Boolean getCadastroTipoVeiculo() {
        return cadastroTipoVeiculo;
    }

    public void setCadastroTipoVeiculo(Boolean cadastroTipoVeiculo) {
        this.cadastroTipoVeiculo = cadastroTipoVeiculo;
    }

    public Boolean getCadastroModeloVeiculo() {
        return cadastroModeloVeiculo;
    }

    public void setCadastroModeloVeiculo(Boolean cadastroModeloVeiculo) {
        this.cadastroModeloVeiculo = cadastroModeloVeiculo;
    }

    public Boolean getCadastroTipoEquipamento() {
        return cadastroTipoEquipamento;
    }

    public void setCadastroTipoEquipamento(Boolean cadastroTipoEquipamento) {
        this.cadastroTipoEquipamento = cadastroTipoEquipamento;
    }

    public Boolean getAcessarIntegracao() {
        return acessarIntegracao;
    }

    public void setAcessarIntegracao(Boolean acessarIntegracao) {
        this.acessarIntegracao = acessarIntegracao;
    }

    public Boolean getAcessarViam() {
        return acessarViam;
    }

    public void setAcessarViam(Boolean acessarViam) {
        this.acessarViam = acessarViam;
    }

    public Boolean getAcessarViaSat() {
        return acessarViaSat;
    }

    public void setAcessarViaSat(Boolean acessarViaSat) {
        this.acessarViaSat = acessarViaSat;
    }


    public Boolean getAcessarRelatorioDiariaMotorista() {
        return acessarRelatorioDiariaMotorista;
    }

    public void setAcessarRelatorioDiariaMotorista(Boolean acessarRelatorioDiariaMotorista) {
        this.acessarRelatorioDiariaMotorista = acessarRelatorioDiariaMotorista;
    }

    public Boolean getAcessarCadastroDiariaMotorista() {
        return acessarCadastroDiariaMotorista;
    }

    public void setAcessarCadastroDiariaMotorista(Boolean acessarCadastroDiariaMotorista) {
        this.acessarCadastroDiariaMotorista = acessarCadastroDiariaMotorista;
    }

    public Boolean getAcessarMacro() {
        return acessarMacro;
    }

    public void setAcessarMacro(Boolean acessarMacro) {
        this.acessarMacro = acessarMacro;
    }

    public Boolean getAcessarOperador() {
        return acessarOperador;
    }

    public void setAcessarOperador(Boolean acessarOperador) {
        this.acessarOperador = acessarOperador;
    }

    public Boolean getAcessarHodometro() {
        return acessarHodometro;
    }

    public void setAcessarHodometro(Boolean acessarHodometro) {
        this.acessarHodometro = acessarHodometro;
    }

    public Boolean getAcessarRelatorioPresencaEquipamento() {
        return acessarRelatorioPresencaEquipamento;
    }

    public void setAcessarRelatorioPresencaEquipamento(Boolean acessarRelatorioPresencaEquipamento) {
        this.acessarRelatorioPresencaEquipamento = acessarRelatorioPresencaEquipamento;
    }

    public Boolean getAcessarRelatorioTransporteEquipamento() {
        return acessarRelatorioTransporteEquipamento;
    }

    public void setAcessarRelatorioTransporteEquipamento(Boolean acessarRelatorioTransporteEquipamento) {
        this.acessarRelatorioTransporteEquipamento = acessarRelatorioTransporteEquipamento;
    }

    public Boolean getAcessarRelatorioSensorBetoneira() {
        return acessarRelatorioSensorBetoneira;
    }

    public void setAcessarRelatorioSensorBetoneira(Boolean acessarRelatorioSensorBetoneira) {
        this.acessarRelatorioSensorBetoneira = acessarRelatorioSensorBetoneira;
    }

    public Boolean getAcessarControleCombustivel() {
        return acessarControleCombustivel;
    }

    public void setAcessarControleCombustivel(Boolean acessarControleCombustivel) {
        this.acessarControleCombustivel = acessarControleCombustivel;
    }

    public Boolean getAcessarGestao() {
        return acessarGestao;
    }

    public void setAcessarGestao(Boolean acessarGestao) {
        this.acessarGestao = acessarGestao;
    }

    public Boolean getCadastroRota() {
        return cadastroRota;
    }

    public void setCadastroRota(Boolean cadastroRota) {
        this.cadastroRota = cadastroRota;
    }

    public Boolean getCadastrarTipoRanking() {
        return cadastrarTipoRanking;
    }

    public void setCadastrarTipoRanking(Boolean cadastrarTipoRanking) {
        this.cadastrarTipoRanking = cadastrarTipoRanking;
    }

    public Boolean getAcessoRankingMotorista() {
        return acessoRankingMotorista;
    }

    public void setAcessoRankingMotorista(Boolean acessoRankingMotorista) {
        this.acessoRankingMotorista = acessoRankingMotorista;
    }

    public Boolean getAcessoSbh() {
        return acessoSbh;
    }

    public void setAcessoSbh(Boolean acessoSbh) {
        this.acessoSbh = acessoSbh;
    }


    public Boolean getRelatorioViamEventoMotorista() {
        return relatorioViamEventoMotorista;
    }

    public void setRelatorioViamEventoMotorista(Boolean relatorioViamEventoMotorista) {
        this.relatorioViamEventoMotorista = relatorioViamEventoMotorista;
    }

    public Boolean getHabilitarAutenticacao() {
        return habilitarAutenticacao;
    }

    public void setHabilitarAutenticacao(Boolean habilitarAutenticacao) {
        this.habilitarAutenticacao = habilitarAutenticacao;
    }

    public Boolean getRelatorioPontosPercorridos() {
        return relatorioPontosPercorridos;
    }

    public void setRelatorioPontosPercorridos(Boolean relatorioPontosPercorridos) {
        this.relatorioPontosPercorridos = relatorioPontosPercorridos;
    }

    public Boolean getRelatorioProdutividadeVeiculo() {
        return relatorioProdutividadeVeiculo;
    }

    public void setRelatorioProdutividadeVeiculo(Boolean relatorioProdutividadeVeiculo) {
        this.relatorioProdutividadeVeiculo = relatorioProdutividadeVeiculo;
    }

    public Boolean getAcessarDesempenhoVeiculo() {
        return acessarDesempenhoVeiculo;
    }

    public void setAcessarDesempenhoVeiculo(Boolean acessarDesempenhoVeiculo) {
        this.acessarDesempenhoVeiculo = acessarDesempenhoVeiculo;
    }

    public Boolean getAcessarDetalhesMonitoramento() {
        return acessarDetalhesMonitoramento;
    }

    public void setAcessarDetalhesMonitoramento(Boolean acessarDetalhesMonitoramento) {
        this.acessarDetalhesMonitoramento = acessarDetalhesMonitoramento;
    }

    public Boolean getAcessarMotoristaTelaMonitoramento() {
        return acessarMotoristaTelaMonitoramento;
    }

    public void setAcessarMotoristaTelaMonitoramento(Boolean acessarMotoristaTelaMonitoramento) {
        this.acessarMotoristaTelaMonitoramento = acessarMotoristaTelaMonitoramento;
    }

    public Boolean getAgruparVeiculosMapaMonitoramento() {
        return agruparVeiculosMapaMonitoramento;
    }

    public void setAgruparVeiculosMapaMonitoramento(Boolean agruparVeiculosMapaMonitoramento) {
        this.agruparVeiculosMapaMonitoramento = agruparVeiculosMapaMonitoramento;
    }
}
