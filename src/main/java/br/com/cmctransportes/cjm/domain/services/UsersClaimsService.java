/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.UsersClaims;
import br.com.cmctransportes.cjm.infra.dao.UsersClaimsDAO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * @author Catatau
 */
@Service
public class UsersClaimsService {
    private UsersClaimsDAO dao;

    private final EntityManager em;

    public UsersClaimsService(EntityManager em) {
        this.em = em;
        dao = new UsersClaimsDAO(em);
    }

    @Transactional
    public void save(UsersClaims entity) {
        try {
            dao.save(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<UsersClaims> findAll() {
        return dao.nqFindAll();
    }

    public List<UsersClaims> findByUser(Integer user_id) {
        return dao.findByUser(user_id);
    }

    public UsersClaims getById(Integer id) {
        return dao.getById(id);
    }

    @Transactional
    public void update(UsersClaims entity) {
        try {
            dao.update(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void delete(Integer id) {
        try {
            UsersClaims entity = getById(id);
            dao.delete(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void delete(UsersClaims entity) {
        try {
            dao.delete(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
