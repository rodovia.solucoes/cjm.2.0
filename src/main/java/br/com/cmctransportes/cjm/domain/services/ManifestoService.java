package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.Manifesto;
import br.com.cmctransportes.cjm.domain.entities.Operador;
import br.com.cmctransportes.cjm.domain.entities.OperadorVeiculo;
import br.com.cmctransportes.cjm.infra.dao.ManifestoDAO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.Date;

@Service
public class ManifestoService {

    private ManifestoDAO manifestoDAO;

    public ManifestoService(EntityManager em) {
        this.manifestoDAO = new ManifestoDAO(em);
    }

    @Transactional
    public void save(Manifesto entity) throws Exception {
        entity.setId(null);
        this.manifestoDAO.save(entity);
    }

}
