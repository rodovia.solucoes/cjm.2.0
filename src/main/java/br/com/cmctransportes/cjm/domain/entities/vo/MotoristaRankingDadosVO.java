package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class MotoristaRankingDadosVO {
    private String nomeDoMotorista;
    private Integer mediaNotas;
    private List<NotasMotoristaGraficoVO> listaDeNotas;
    private List<String> listaDeTitulos;
    private List<String[]> listaDeDados;
}
