package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class InconformidadeVO implements Serializable {
    private String evento;
    private String dataDoOcorrido;
}
