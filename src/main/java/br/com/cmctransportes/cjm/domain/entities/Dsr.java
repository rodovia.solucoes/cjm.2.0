package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author William Leite
 */
@Entity
@Table(name = "dsr")
@NamedQueries({
        @NamedQuery(name = "Dsr.findAll", query = "SELECT c FROM Dsr c"),
        @NamedQuery(name = "Dsr.findById", query = "SELECT j FROM Dsr j WHERE j.id = :id")
})
public class Dsr implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "DSR_ID_SEQ", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "DSR_ID_SEQ", sequenceName = "DSR_ID_SEQ", allocationSize = 1)
    @Column(name = "id", nullable = false)
    @Getter
    @Setter
    private Integer id;

    @JoinColumn(name = "empresa_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @Getter
    @Setter
    private Empresas empresaId;

    @JoinColumn(name = "motorista_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @Getter
    @Setter
    private Motoristas motorista;

    @Column(name = "inicio_dsr", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @Getter
    @Setter
    private Date inicioDSR;

    @Column(name = "fim_dsr", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @Getter
    @Setter
    private Date fimDSR;

    @Column(name = "processada")
    @Getter
    @Setter
    private char processada = 'n';

    @JoinColumn(name = "operador_lancamento", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @Getter
    @Setter
    private Users operadorLancamento;

    @Column(name = "inicio_interjornada")
    @Temporal(TemporalType.TIMESTAMP)
    @Getter
    @Setter
    private Date inicioInterjornada;

    @Column(name = "fim_interjornada")
    @Temporal(TemporalType.TIMESTAMP)
    @Getter
    @Setter
    private Date fimInterjornada;
}