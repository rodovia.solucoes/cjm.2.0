package br.com.cmctransportes.cjm.domain.entities;

import br.com.cmctransportes.cjm.domain.entities.vo.DiurnoNoturno;
import br.com.cmctransportes.cjm.domain.entities.vo.ResumoJornada;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.Objects;

/**
 * @author analista.ti
 */
@Entity
@Table(name = "evento")
@NamedQueries({
        @NamedQuery(name = "Evento.findAll", query = "SELECT e FROM Evento e"),
        @NamedQuery(name = "Evento.findById", query = "SELECT e FROM Evento e WHERE e.id = :id"),
        @NamedQuery(name = "Evento.findByInstanteEvento", query = "SELECT e FROM Evento e WHERE e.instanteEvento = :instanteEvento"),
        @NamedQuery(name = "Evento.findByInstanteLancamento", query = "SELECT e FROM Evento e WHERE e.instanteLancamento = :instanteLancamento"),
        @NamedQuery(name = "Evento.findAllNonRemoved", query = "SELECT e FROM Evento e WHERE e.removido = false AND e.latitude IS NOT NULL AND e.longitude IS NOT NULL AND e.positionAddress IS NULL")})
@JsonIgnoreProperties(value = {"intervalo", "intervaloNoturno", "hedIntrajornada", "henIntrajornada", "paralelo"}, allowGetters = true, ignoreUnknown = true)
public class Evento implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer id;
    private Date instanteEvento; // Este é sempre o instante que será levado em consideração
    private Date instanteLancamento; // 
    private Date instanteOriginal; // Caso o lançamento seja alterado, aqui fica o valor ORIGINAL, se a alteração for alterada, não modifica esse valor que estará preenchido
    @JsonIgnore
    private Empresas empresaId;
    private EventosJornada tipoEvento;

    @JsonBackReference("jornadaId")
    private Jornada jornadaId;
    @JsonIgnore
    private Integer idLocalEventoEspera;
    private Users operadorLancamento; // usuário que fez o lançamento original
    private Users operadorAlteracao;  // usuário que fez a ULTIMA alteração
    private Veiculos veiculoMotor;
    private Veiculos veiculoCarga01;
    private Veiculos veiculoCarga02;

    private Double longitude;
    private Double latitude;

    private String origem;
    private String justificativa;

    @JsonManagedReference("eventoAnterior")
    private Evento eventoSeguinte;
    @JsonBackReference("eventoAnterior")
    private Evento eventoAnterior;
    private MotivoAbono motivoAbono;
    private Boolean removido = false;
    private String versaoOrigem;
    private Boolean locked = false;

    /* Calculados */
    private Date fimEvento;
    private Integer fimEventoId;
    private Integer aberturaId;

    private DiurnoNoturno dayNight;

    @Transient
    public DiurnoNoturno getDayNight() {
        return dayNight;
    }

    public void setDayNight(DiurnoNoturno dayNight) {
        this.dayNight = dayNight;
    }

    private boolean paralelo = false;

    private String positionAddress;
    @Transient
    private ResumoJornada resumoJornada;
    @Transient
    private Rastro rastro;
    @Transient
    private String instanteEventoFormatado;
    @Transient
    private PontosDeEspera localEventoEspera;
    @Transient
    private String pontoProximo;
    @Transient
    private Boolean eventoGravado;


    public Evento() {
    }

    public Evento(Integer id) {
        this.id = id;
    }

    public Evento(Integer id, Date instanteEvento, Date instanteLancamento) {
        this.id = id;
        setInstanteEvento(instanteEvento);
        this.instanteLancamento = instanteLancamento;
    }

    public Evento(Date lancamento, Jornada jornada, Date instante, Empresas empresa, Users opLancamento, String origemLancamento, EventosJornada tpEvento) {
        setInstanteLancamento(lancamento);
        setJornadaId(jornada);
        setInstanteEvento(instante);
        setEmpresaId(empresa);
        setOperadorLancamento(opLancamento);
        setOrigem(origemLancamento);
        setTipoEvento(tpEvento);
    }

    @Id
    @SequenceGenerator(name = "EVENTO_ID_SEQ", sequenceName = "EVENTO_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EVENTO_ID_SEQ")
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "instante_evento", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getInstanteEvento() {
        return truncDate(instanteEvento);
    }

    public void setInstanteEvento(Date instanteEvento) {
        this.instanteEvento = instanteEvento;

        this.dayNight = new DiurnoNoturno();
    }

    @Column(name = "instante_lancamento", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getInstanteLancamento() {
        return instanteLancamento;
    }

    public void setInstanteLancamento(Date instanteLancamento) {
        this.instanteLancamento = instanteLancamento;
    }

    @JoinColumn(name = "empresa_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    public Empresas getEmpresaId() {
        return empresaId;
    }

    public void setEmpresaId(Empresas empresaId) {
        this.empresaId = empresaId;
    }

    @JoinColumn(name = "tipo_evento", referencedColumnName = "id")
    @ManyToOne(optional = false)
    public EventosJornada getTipoEvento() {
        return tipoEvento;
    }

    public void setTipoEvento(EventosJornada tipoEvento) {
        this.tipoEvento = tipoEvento;
    }

    @JoinColumn(name = "jornada_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    public Jornada getJornadaId() {
        return jornadaId;
    }

    public void setJornadaId(Jornada jornadaId) {
        this.jornadaId = jornadaId;
    }

    @Column(name = "local_evento_espera")
    public Integer getIdLocalEventoEspera() {
        return idLocalEventoEspera;
    }

    public void setIdLocalEventoEspera(Integer localEventoEspera) {
        this.idLocalEventoEspera = localEventoEspera;
    }

    @JoinColumn(name = "operador_lancamento", referencedColumnName = "id")
    @ManyToOne(optional = false)
    public Users getOperadorLancamento() {
        return operadorLancamento;
    }

    public void setOperadorLancamento(Users operadorLancamento) {
        this.operadorLancamento = operadorLancamento;
    }

    @JoinColumn(name = "motivo_abono_id", referencedColumnName = "id")
    @ManyToOne(optional = true)
    public MotivoAbono getMotivoAbono() {
        return motivoAbono;
    }

    public void setMotivoAbono(MotivoAbono motivoAbono) {
        this.motivoAbono = motivoAbono;
    }

    @JoinColumn(name = "veiculo_motor", referencedColumnName = "placa")
    @ManyToOne(optional = true)
    public Veiculos getVeiculoMotor() {
        return veiculoMotor;
    }

    public void setVeiculoMotor(Veiculos veiculoMotor) {
        this.veiculoMotor = veiculoMotor;
    }

    @JoinColumn(name = "veiculo_carga01", referencedColumnName = "placa")
    @ManyToOne(optional = true)
    public Veiculos getVeiculoCarga01() {
        return veiculoCarga01;
    }

    public void setVeiculoCarga01(Veiculos veiculoCarga01) {
        this.veiculoCarga01 = veiculoCarga01;
    }

    @JoinColumn(name = "veiculo_carga02", referencedColumnName = "placa")
    @ManyToOne(optional = true)
    public Veiculos getVeiculoCarga02() {
        return veiculoCarga02;
    }

    public void setVeiculoCarga02(Veiculos veiculoCarga02) {
        this.veiculoCarga02 = veiculoCarga02;
    }

    /**
     * @return the longitude
     */
    @Column(name = "longitude", nullable = true)
    public Double getLongitude() {
        return longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    /**
     * @return the latitude
     */
    @Column(name = "latitude", nullable = true)
    public Double getLatitude() {
        return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the instanteOriginal
     */
    @Column(name = "instante_original", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getInstanteOriginal() {
        return instanteOriginal;
    }

    /**
     * @param instanteOriginal the instanteOriginal to set
     */
    public void setInstanteOriginal(Date instanteOriginal) {
        this.instanteOriginal = instanteOriginal;
    }

    /**
     * @return the operadorAlteracao
     */
    @JoinColumn(name = "operador_alteracao", referencedColumnName = "id")
    @ManyToOne(optional = true)
    public Users getOperadorAlteracao() {
        return operadorAlteracao;
    }

    /**
     * @param operadorAlteracao the operadorAlteracao to set
     */
    public void setOperadorAlteracao(Users operadorAlteracao) {
        this.operadorAlteracao = operadorAlteracao;
    }

    /**
     * @return the origem
     */
    @Column(name = "origem_id", nullable = true)
    public String getOrigem() {
        return origem;
    }

    /**
     * @param origem the origem to set
     */
    public void setOrigem(String origem) {
        this.origem = origem;
    }

    /**
     * @return the justificativa
     */
    @Column(name = "justificativa", nullable = true)
    public String getJustificativa() {
        return justificativa;
    }

    /**
     * @param justificativa the justificativa to set
     */
    public void setJustificativa(String justificativa) {
        this.justificativa = justificativa;
    }

    @Transient
    public Evento getEventoSeguinte() {
        if (Objects.isNull(this.eventoSeguinte)
                && Objects.nonNull(this.jornadaId)
                && Objects.nonNull(this.jornadaId.getEventos())) {
            this.eventoSeguinte = this.jornadaId.getEventos().stream()
                    .filter(e -> Objects.nonNull(e.getInstanteEvento()))
                    .filter(e -> e.getInstanteEvento().after(this.getInstanteEvento()))
                    .sorted(Comparator.comparing(Evento::getInstanteEvento))
                    .findFirst()
                    .orElse(null);
        }

        return this.eventoSeguinte;
    }

    public void setEventoSeguinte(Evento eventoSeguinte) {
        this.eventoSeguinte = eventoSeguinte;
    }

    public void setEventoAnterior(Evento eventoAnterior) {
        this.eventoAnterior = eventoAnterior;
    }

    @Transient
    public Evento getEventoAnterior() {
        if (Objects.isNull(this.eventoAnterior)
                && Objects.nonNull(this.jornadaId)
                && Objects.nonNull(this.jornadaId.getEventos())) {
            this.eventoAnterior = this.jornadaId.getEventos().stream()
                    .filter(e -> e.getInstanteEvento().before(this.getInstanteEvento()))
                    .sorted(Comparator.comparing(Evento::getInstanteEvento))
                    .reduce((first, second) -> second)
                    .orElse(null);
        }

        return this.eventoAnterior;
    }

    @Column(name = "removido", nullable = true)
    public Boolean getRemovido() {
        if (removido == null) {
            removido = false;
        }

        return removido;
    }

    @Column(name = "locked", nullable = true)
    public Boolean getLocked() {
        if (Objects.isNull(this.locked)) {
            this.locked = false;
        }

        return this.locked;
    }

    public void setLocked(Boolean locked) {
        if (Objects.isNull(locked)) {
            this.locked = false;
        } else {
            this.locked = locked;
        }
    }

    public void setRemovido(Boolean removido) {
        if (removido == null) {
            removido = false;
        }

        this.removido = removido;
    }


    @Column(name = "position_address", nullable = true)
    public String getPositionAddress() {
        return positionAddress;
    }

    public void setPositionAddress(String positionAddress) {
        this.positionAddress = positionAddress;
    }


    @Column(name = "versao_origem", nullable = true)
    /**
     * @return the versaoOrigem
     */
    public String getVersaoOrigem() {
        return versaoOrigem;
    }

    /**
     * @param versaoOrigem the versaoOrigem to set
     */
    public void setVersaoOrigem(String versaoOrigem) {
        this.versaoOrigem = versaoOrigem;
    }

    @Transient
    public Integer getFimEventoId() {
        return fimEventoId;
    }

    public void setFimEventoId(Integer fimEventoId) {
        this.fimEventoId = fimEventoId;
    }

    @Transient
    public Date getFimEvento() {
        if (this.fimEvento == null && this.tipoEvento != null) {
            Evento proximoEvento = this.getEventoSeguinte();
            while (proximoEvento != null) {
                Integer tipoEventoId = this.tipoEvento.getId();
                // Eventos espelho
                if ((tipoEventoId <= 1) // se for inicio jornada, ou fim de evento, o próximo é  o seu fim
                        || (tipoEventoId + proximoEvento.tipoEvento.getId() == 0)) { // se não, procura o evento espelho

                    if (tipoEventoId <= 1 && this.paralelo) {
                        this.setFimEvento(this.getInstanteEvento());
                        this.setFimEventoId(this.getId());
                    } else {
                        this.setFimEvento(proximoEvento.getInstanteEvento());
                        this.setFimEventoId(proximoEvento.getId());
                    }
                    proximoEvento = null;
                } else {
                    // Pega próximo evento
                    // se pulou é evento paralelo!
                    proximoEvento.paralelo = true;
                    proximoEvento = proximoEvento.getEventoSeguinte();
                }
            }
        }

        return this.fimEvento;
    }

    /**
     * @param fimEvento the fimEvento to set
     */
    public void setFimEvento(Date fimEvento) {
        this.fimEvento = fimEvento;
    }

    @Transient
    public Integer getJornada() {
        if (jornadaId == null) {
            return null;
        }

        return jornadaId.getId();
    }

    @Transient
    public Integer getEventoAnteriorId() {
        if (eventoAnterior == null) {
            return null;
        }

        return eventoAnterior.getId();
    }

    @Override
    public int hashCode() {
        // TODO: trocar isso aqui para usar o instante lançamento + id do motorista
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Evento)) {
            return false;
        }
        Evento other = (Evento) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.cmctransportes.cjm.domain.entities.Evento[ id=" + id + " ]";
    }

    private Date truncDate(Date d) {
        if (Objects.isNull(d)) {
            return null;
        }

        Calendar c = Calendar.getInstance();
        c.setTime(d);
//        c.set(Calendar.HOUR_OF_DAY, 23);
//        c.set(Calendar.MINUTE, 59);
//        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }

    public void setAberturaId(Integer id) {
        this.aberturaId = id;
    }

    @Transient
    public Integer getAberturaId() {
        if (this.aberturaId == null && this.tipoEvento != null) {
            Evento eventoAnterior = this.getEventoAnterior();
            while (eventoAnterior != null) {
                // Eventos espelho
                if (eventoAnterior.getFimEventoId() == this.id) {
                    this.aberturaId = eventoAnterior.id;

                    eventoAnterior = null;
                } else {
                    eventoAnterior = eventoAnterior.getEventoAnterior();
                }
            }
        }

        return this.aberturaId;
    }

    @Transient
    public ResumoJornada getResumoJornada() {
        return resumoJornada;
    }

    @Transient
    public void setResumoJornada(ResumoJornada resumoJornada) {
        this.resumoJornada = resumoJornada;
    }

    @Transient
    public Rastro getRastro() {
        return rastro;
    }

    @Transient
    public void setRastro(Rastro rastro) {
        this.rastro = rastro;
    }

    @Transient
    public String getInstanteEventoFormatado() {
        return instanteEventoFormatado;
    }

    @Transient
    public void setInstanteEventoFormatado(String instanteEventoFormatado) {
        this.instanteEventoFormatado = instanteEventoFormatado;
    }

    @Transient
    public void limparDados() {
        this.operadorAlteracao = null;
        this.operadorLancamento = null;
        this.veiculoMotor = null;
        this.eventoSeguinte = null;

    }

    @Transient
    public PontosDeEspera getLocalEventoEspera() {
        return localEventoEspera;
    }

    @Transient
    public void setLocalEventoEspera(PontosDeEspera localEventoEspera) {
        this.localEventoEspera = localEventoEspera;
    }
    @Transient
    public String getPontoProximo() {
        return pontoProximo;
    }
    @Transient
    public void setPontoProximo(String pontoProximo) {
        this.pontoProximo = pontoProximo;
    }
    @Transient
    public Boolean getEventoGravado() {
        return eventoGravado;
    }
    @Transient
    public void setEventoGravado(Boolean eventoGravado) {
        this.eventoGravado = eventoGravado;
    }
}
