/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cmctransportes.cjm.domain.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author analista.ti
 */
@Entity
@Table(name = "users_claims")
@Getter
@Setter
@NamedQueries({
        @NamedQuery(name = "UsersClaims.findAll", query = "SELECT u FROM UsersClaims u"),
        @NamedQuery(name = "UsersClaims.findById", query = "SELECT u FROM UsersClaims u WHERE u.id = :id"),
        @NamedQuery(name = "UsersClaims.findByClaim", query = "SELECT u FROM UsersClaims u WHERE u.claim = :claim")})
public class UsersClaims implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "users_branch_id_gen", sequenceName = "users_branch_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "users_branch_id_gen")
    @Column(name = "id")
    private Integer id;

    @Column(name = "claim", length = 255, nullable = false)
    private String claim;

    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    @JsonBackReference("usersClaims")
    private Users userId;

    public UsersClaims() {
    }

    public UsersClaims(Integer id, String name, Users users) {
        this.id = id;
        this.claim = name;
        this.userId = users;
    }
}