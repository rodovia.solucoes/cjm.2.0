package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VeiculoTransmissaoVO {
    private VeiculoMinimoVO veiculo;
    private TransmissaoMinimoVO transmissao;
    private LocalMinimoVO local;
    private MotoristaMinimoVO motorista;
}
