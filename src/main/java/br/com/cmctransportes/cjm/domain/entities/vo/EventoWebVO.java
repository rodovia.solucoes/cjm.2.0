package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.*;
import br.com.cmctransportes.cjm.domain.services.EmpresasService;
import br.com.cmctransportes.cjm.domain.services.EventosJornadaService;
import br.com.cmctransportes.cjm.domain.services.JornadaService;
import br.com.cmctransportes.cjm.domain.services.UserService;

import java.util.Date;
import java.util.Objects;

/**
 * @author Producao
 */
public class EventoWebVO {
    private Integer tipoEvento;
    private Date instanteEvento;
    private Date instanteLancamento;
    private String justificativa;
    private Integer empresaId;
    private Integer jornadaId;
    private Integer operadorLancamento;
    private MotivoAbono motivoAbono;

    public Evento revert(EmpresasService empresasService, JornadaService jornadaService,
                         EventosJornadaService eventosJornadaService, UserService userService) {
        Evento result = new Evento();

        Empresas empresa = pegaEmpresa(empresasService);
        Jornada jornada = pegaJornada(jornadaService);
        EventosJornada eventosJornada = pegaTipoEvento(eventosJornadaService);
        Users users = pegaOperadorLancamento(userService);

        result.setTipoEvento(eventosJornada);
        result.setInstanteEvento(getInstanteEvento());
        result.setInstanteLancamento(getInstanteLancamento());
        result.setJustificativa(justificativa);
        result.setEmpresaId(empresa);
        result.setJornadaId(jornada);
        result.setOperadorLancamento(users);
        result.setMotivoAbono(motivoAbono);
        result.setOrigem("ViaM Web");
        result.setVersaoOrigem("");

        return result;
    }

    private Users pegaOperadorLancamento(UserService userService) {
        Users users = userService.getById(operadorLancamento);
        return users;
    }

    private EventosJornada pegaTipoEvento(EventosJornadaService eventosJornadaService) {
        EventosJornada eventosJornada = eventosJornadaService.getById(tipoEvento);
        return eventosJornada;
    }

    private Jornada pegaJornada(JornadaService jornadaService) {
        if (Objects.nonNull(this.jornadaId)) {
            Jornada jornada = jornadaService.getById(getJornadaId());
            return jornada;
        }
        return null;
    }

    private Empresas pegaEmpresa(EmpresasService empresasService) {
        Empresas empresa = empresasService.getById(getEmpresaId());
        return empresa;
    }

    /**
     * @return the tipoEvento
     */
    public Integer getTipoEvento() {
        return tipoEvento;
    }

    /**
     * @param tipoEvento the tipoEvento to set
     */
    public void setTipoEvento(Integer tipoEvento) {
        this.tipoEvento = tipoEvento;
    }

    /**
     * @return the instanteEvento
     */
    public Date getInstanteEvento() {
        return instanteEvento;
    }

    /**
     * @param instanteEvento the instanteEvento to set
     */
    public void setInstanteEvento(Date instanteEvento) {
        this.instanteEvento = instanteEvento;
    }

    /**
     * @return the instanteLancamento
     */
    public Date getInstanteLancamento() {
        return instanteLancamento;
    }

    /**
     * @param instanteLancamento the instanteLancamento to set
     */
    public void setInstanteLancamento(Date instanteLancamento) {
        this.instanteLancamento = instanteLancamento;
    }

    /**
     * @return the justificativa
     */
    public String getJustificativa() {
        return justificativa;
    }

    /**
     * @param justificativa the justificativa to set
     */
    public void setJustificativa(String justificativa) {
        this.justificativa = justificativa;
    }

    /**
     * @return the empresaId
     */
    public Integer getEmpresaId() {
        return empresaId;
    }

    /**
     * @param empresaId the empresaId to set
     */
    public void setEmpresaId(Integer empresaId) {
        this.empresaId = empresaId;
    }

    /**
     * @return the jornadaId
     */
    public Integer getJornadaId() {
        return jornadaId;
    }

    /**
     * @param jornadaId the jornadaId to set
     */
    public void setJornadaId(Integer jornadaId) {
        this.jornadaId = jornadaId;
    }

    /**
     * @return the operadorLancamento
     */
    public Integer getOperadorLancamento() {
        return operadorLancamento;
    }

    /**
     * @param operadorLancamento the operadorLancamento to set
     */
    public void setOperadorLancamento(Integer operadorLancamento) {
        this.operadorLancamento = operadorLancamento;
    }

    /**
     * @return the motivoAbono
     */
    public MotivoAbono getMotivoAbono() {
        return motivoAbono;
    }

    /**
     * @param motivoAbono the motivoAbono to set
     */
    public void setMotivoAbono(MotivoAbono motivoAbono) {
        this.motivoAbono = motivoAbono;
    }

}
