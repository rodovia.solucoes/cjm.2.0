package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResumoRankingMototristaVO {
    private Integer idMotorista;
    private String nomeMotorista;
    private Integer nota;
    private String cor;
}
