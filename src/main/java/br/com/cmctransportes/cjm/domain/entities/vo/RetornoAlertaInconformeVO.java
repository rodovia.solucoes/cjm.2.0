package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RetornoAlertaInconformeVO extends RetornoVO {
    private List<AlertaInconformeVO> listaDeAlertaInconforme;
}
