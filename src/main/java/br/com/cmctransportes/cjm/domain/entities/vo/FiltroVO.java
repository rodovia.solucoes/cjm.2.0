package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class FiltroVO implements Serializable {
    private static final long serialVersionUID = 1L;
    private Date dataInicial;
    private Date dataFinal;
    private Double velocidade;
    private Boolean ignicaoAtiva;
    private Boolean buscarPelaVelocidadeMaximaDoVeiculo = false;
    private VeiculoVO veiculo;
    private EquipamentoPortatilVO equipamentoPortatil;
    private MotoristaVO motorista;
    private List<VeiculoVO> listaDeVeiculos;
    private List<MotoristaVO> listaDeMotoristas;
    private List<LocalVO> listaDeLocais;
    /**
     * VALORES QUE PODEM VIR
     * PDF
     * XLS
     * CSV
     */
    private String tipoArquivo;
    private String nomeColuna;
    /*
    * 1 = Carga
    * 2 - descarga
     */
    private Integer tipoDeCerca;
    private Integer horasDiariaManutencao;
    private Integer horasDiariaProgramada;
    private Integer idCliente;
    private Integer idUnidade;
    private String dataInicialString;
    private String dataFinalString;
    private Integer tipoSensor;
    private Integer paradaMaxima;
    private Double velocidadeMinima;
    /**
     *  1 = entrada de cerca
     *  2 = saida de cerca
     *  3 = sensor porta motorista
     *  4 = sensor porta bau
     */
    private Integer tipoEventoSensor;
    /**
     * 1 Carregado
     * 2 Descarregado
     */
    private Integer tipoEventoMotorista;
    private Integer tempoParado;
}
