/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.Evento;
import br.com.cmctransportes.cjm.domain.entities.EventosJornada;
import br.com.cmctransportes.cjm.domain.entities.MotivoAbono;
import br.com.cmctransportes.cjm.infra.dao.EventoDAO;
import br.com.cmctransportes.cjm.infra.dao.EventosJornadaDAO;
import br.com.cmctransportes.cjm.infra.dao.MotivoAbonoDAO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @author analista.ti
 */
@Service
public class EventosJornadaService {
    // TODO: criar GenericService
    private EventosJornadaDAO dao;
    private MotivoAbonoDAO motivoDAO;
    private EventoDAO eventoDAO;

    private final EntityManager em;

    public EventosJornadaService(EntityManager em) {
        this.em = em;
        dao = new EventosJornadaDAO(em);
        motivoDAO = new MotivoAbonoDAO(em);
        eventoDAO = new EventoDAO(em);
    }

    @Transactional
    public void save(EventosJornada entity) {
        try {
            dao.save(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // public T getById(PK pk) {
    public List<EventosJornada> findAll() {
        // return dao.findAll();
        return dao.nqFindAll();
    }

    public List<MotivoAbono> findAllowances() {
        return motivoDAO.findAll();
    }

    public List<EventosJornada> findByDateJourney(Integer idJornada, Long date) {
        List<EventosJornada> lista = new ArrayList<>();
        Date fromDate = new Date(date);
        Evento evento = this.eventoDAO.getLastEvent(idJornada, fromDate);
        if (Objects.nonNull(evento)) {
            lista.addAll(evento.getTipoEvento().getObjGeraEstado().getEventosPermitidos());
        }
        return lista;
    }

    public EventosJornada getById(Integer id) {
        return dao.getById(id);
    }

    @Transactional
    public void update(EventosJornada entity) {
        try {
            dao.update(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void delete(Integer id) {
        try {
            EventosJornada tipo = getById(id);
            dao.delete(tipo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void delete(EventosJornada entity) {
        try {
            dao.delete(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}