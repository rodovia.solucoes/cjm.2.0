package br.com.cmctransportes.cjm.domain.entities.vo;

import br.com.cmctransportes.cjm.domain.entities.Inconformidade;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class AlertaInconformidadeVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<Inconformidade> listaInconformidadeViaSat;
    private List<Inconformidade> listaInconformidadeViaM;
    private List<Inconformidade> listaAlertaViaSat;
}
