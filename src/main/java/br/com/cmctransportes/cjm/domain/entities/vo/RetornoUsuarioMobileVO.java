package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RetornoUsuarioMobileVO extends RetornoVO {
    private UsuarioMobileVO usuario;
}
