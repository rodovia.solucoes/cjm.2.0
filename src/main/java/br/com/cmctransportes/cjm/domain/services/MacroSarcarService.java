package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.MacroSascar;
import br.com.cmctransportes.cjm.infra.dao.MacroSascarDAO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MacroSarcarService {

    public List<MacroSascar> buscarTodas(){
        List<MacroSascar> lista = null;
        try {
            lista = new MacroSascarDAO().buscar();
        }catch (Exception e){
            LogSistema.logError("buscarTodas", e);
        }

        return lista;
    }

}
