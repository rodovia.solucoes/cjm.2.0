/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author analista.ti
 */
@Entity
@Table(name = "tolerancias")
@Getter
@Setter
@NamedQueries({
        @NamedQuery(name = "Tolerancias.findAll", query = "SELECT t FROM Tolerancias t"),
        @NamedQuery(name = "Tolerancias.findById", query = "SELECT t FROM Tolerancias t WHERE t.id = :id"),
        @NamedQuery(name = "Tolerancias.findByDescricao", query = "SELECT t FROM Tolerancias t WHERE t.descricao = :descricao"),
        @NamedQuery(name = "Tolerancias.findByAtrasoEntrada", query = "SELECT t FROM Tolerancias t WHERE t.atrasoEntrada = :atrasoEntrada"),
        @NamedQuery(name = "Tolerancias.findByAtrasoSaida", query = "SELECT t FROM Tolerancias t WHERE t.atrasoSaida = :atrasoSaida"),
        @NamedQuery(name = "Tolerancias.findByHoraExtraEntrada", query = "SELECT t FROM Tolerancias t WHERE t.horaExtraEntrada = :horaExtraEntrada"),
        @NamedQuery(name = "Tolerancias.findByHoraExtraSaida", query = "SELECT t FROM Tolerancias t WHERE t.horaExtraSaida = :horaExtraSaida"),
        @NamedQuery(name = "Tolerancias.findByAtrasoAlmoco", query = "SELECT t FROM Tolerancias t WHERE t.atrasoAlmoco = :atrasoAlmoco"),
        @NamedQuery(name = "Tolerancias.findByHoraExtraAlmoco", query = "SELECT t FROM Tolerancias t WHERE t.horaExtraAlmoco = :horaExtraAlmoco"),
        @NamedQuery(name = "Tolerancias.findByTotalAtrasoDiario", query = "SELECT t FROM Tolerancias t WHERE t.totalAtrasoDiario = :totalAtrasoDiario"),
        @NamedQuery(name = "Tolerancias.findByTotalHoraExtraDiario", query = "SELECT t FROM Tolerancias t WHERE t.totalHoraExtraDiario = :totalHoraExtraDiario")})
public class Tolerancias implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name = "TOLERANCIAS_ID_SEQ", sequenceName = "TOLERANCIAS_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TOLERANCIAS_ID_SEQ")
    @Column(name = "id")
    private Integer id;

    @Column(name = "descricao", length = 45)
    private String descricao;

    @Column(name = "atraso_entrada")
    private Integer atrasoEntrada;

    @Column(name = "atraso_saida")
    private Integer atrasoSaida;

    @Column(name = "hora_extra_entrada")
    private Integer horaExtraEntrada;

    @Column(name = "hora_extra_saida")
    private Integer horaExtraSaida;

    @Column(name = "atraso_almoco")
    private Integer atrasoAlmoco;

    @Column(name = "hora_extra_almoco")
    private Integer horaExtraAlmoco;

    @Column(name = "total_atraso_diario")
    private Integer totalAtrasoDiario;

    @Column(name = "total_hora_extra_diario")
    private Integer totalHoraExtraDiario;

    @JoinColumn(name = "empresa_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Empresas empresaId;
}