package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MotoristaMinimoVO {
    private Integer id;
    private String nome;
}
