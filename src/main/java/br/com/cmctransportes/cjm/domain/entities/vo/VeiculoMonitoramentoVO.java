package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class VeiculoMonitoramentoVO implements Serializable{
    private Integer id;
    private String placa;
    private String dataTransmissao;
    private Boolean ignicao;
    private String endereco;
    private Double lat;
    private Double lng;
    private String nomeMotorista;
    private String velocidade;
    private Double odometro;
    private String combustivel;
    private Integer rpm;
    private String modeloVeiculo;
    private Double voltagem;
    private String cidade;
    private String uf;
    private String evento;
    private String cerca;
    private List<RotaVO> listaDeRotas;
    private StatusRotaVO statusRota;
    private String dataDoEvento;
}
