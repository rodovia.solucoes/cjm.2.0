package br.com.cmctransportes.cjm.domain.entities.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class DetalheTelemetriaVO implements Serializable {
    private String start;
    private String end;
    private String color;
    private String duracao;
    private String velocidade;
    private String ignicao;
    private String hodometroInicio;
    private String hodometroFim;
    private Double latitude;
    private Double longitude;
    private String nomeEvento;
    private String resumo;
    private String endereco;
}
