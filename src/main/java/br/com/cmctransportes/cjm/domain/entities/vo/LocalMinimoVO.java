package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class LocalMinimoVO implements Serializable {
    private boolean temLocal;
    private String nome;
    private String status;
}
