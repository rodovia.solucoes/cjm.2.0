package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "ibutton_veiculo_motorista")
@Getter
@Setter
public class IbuttonVeiculoMotorista implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "ibutton_veiculo_motorista_id_seq", sequenceName = "ibutton_veiculo_motorista_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ibutton_veiculo_motorista_id_seq")
    @Column(name = "id")
    private Integer id;
    @Column(name = "veiculo")
    private Integer idVeiculo;
    @Column(name = "identificacao_operador")
    private Integer identificacaoOperador;
    @Column(name = "motorista")
    private Integer motorista;
    @Column(name = "operador")
    private Integer operador;
}
