package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.*;
import br.com.cmctransportes.cjm.domain.entities.vo.*;
import br.com.cmctransportes.cjm.infra.dao.MotoristasDAO;
import br.com.cmctransportes.cjm.infra.dao.RankingMotoristaDAO;
import br.com.cmctransportes.cjm.infra.dao.TelemetriaDAO;
import br.com.cmctransportes.cjm.infra.dao.VeiculosDAO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import org.apache.commons.lang3.time.DateUtils;

import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.joda.time.Seconds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class RankingMotoristaService {

    private MotoristasDAO motoristasDAO;
    private RankingMotoristaDAO rankingMotoristaDAO;

    @Autowired
    private VeiculosService veiculosService;
    @Autowired
    private MotoristasVeiculosService motoristasVeiculosService;

    private Map<TipoRanking, List<RankingMotorista>> mapaDeRankingMotorista = new HashMap<>();

    public RankingMotoristaService(EntityManager em) {
        motoristasDAO = new MotoristasDAO(em);
        rankingMotoristaDAO = new RankingMotoristaDAO(em);
    }


    public RankingMototristaVO buscarResumo(Integer idEmpresa, Integer idUnidade) {
        RankingMototristaVO vo = new RankingMototristaVO();
        try {
            List<ResumoRankingMototristaVO> lista = new ArrayList<>();
            List<Motoristas> listaDeMotoristas = motoristasDAO.findAllRankingMotorista(idEmpresa, idUnidade);
            for (Motoristas motoristas : listaDeMotoristas) {
                //buscar veiculo por motoristas
                mapaDeRankingMotorista.clear();
                List<RankingMotorista> listaRankingMotorista = rankingMotoristaDAO.buscarListaDeRankingMotorista(motoristas.getId());
                if (Objects.nonNull(listaRankingMotorista) && !listaDeMotoristas.isEmpty()) {
                    ajustarDadosRankingMotorista(listaRankingMotorista);
                    processarRanking(motoristas, lista);
                }
            }
            vo.setListaResumoRankingMototrista(lista);
        } catch (Exception e) {
            LogSistema.logError("buscarResumo", e);
        }
        return vo;
    }

    private void processarRanking(Motoristas motoristas, List<ResumoRankingMototristaVO> lista) {
        try {
            double[] notas = new double[mapaDeRankingMotorista.size()];
            int cont = 0;
            for (Map.Entry<TipoRanking, List<RankingMotorista>> entry : mapaDeRankingMotorista.entrySet()) {
                TipoRanking tipoRanking = entry.getKey();
                int totalDeIntens = entry.getValue().size();
                int maximoEventoMinimo = tipoRanking.getMaximoEventoMinimo();
                int maximoEventoMedio = tipoRanking.getMaximoEventoMedio();
                int maximoEventomaximoAcimaDe = tipoRanking.getMaximoEventomaximoAcimaDe();

                if (totalDeIntens <= maximoEventoMinimo) {
                    double porcentagem = (totalDeIntens * 100) / maximoEventoMinimo;
                    notas[cont] = porcentagem;
                }

                if (totalDeIntens > maximoEventoMinimo && totalDeIntens <= maximoEventoMedio) {
                    double porcentagem = (totalDeIntens * 100) / maximoEventoMedio;
                    notas[cont] = porcentagem;
                }

                if (totalDeIntens >= maximoEventomaximoAcimaDe) {
                    double porcentagem = (totalDeIntens * 100) / maximoEventomaximoAcimaDe;
                    notas[cont] = porcentagem;
                }
                cont++;
            }

            double media = 0d;
            for (int i = 0; i < notas.length; i++) {
                media = media + notas[i];
            }

            media = media / notas.length;
            int nota = 1;
            String cor = "#00FF7F";

            if (media > 0d && media <= 19d) {
                nota = 1;
            }
            if (media >= 20d && media <= 29d) {
                nota = 2;
            }
            if (media >= 30d && media <= 39d) {
                nota = 3;
            }
            if (media >= 40d && media <= 49d) {
                nota = 4;
            }
            if (media >= 50d && media <= 59d) {
                nota = 5;
                cor = "#4169E1";
            }
            if (media >= 60d && media <= 69d) {
                nota = 6;
                cor = "#4169E1";
            }
            if (media >= 70d && media <= 79d) {
                nota = 7;
                cor = "#4169E1";
            }
            if (media >= 80d && media <= 89d) {
                nota = 8;
                cor = "#FF6347";
            }
            if (media >= 90d && media < 99d) {
                nota = 9;
                cor = "#FF6347";
            }
            if (media >= 99d) {
                nota = 10;
                cor = "#FF6347";
            }

            ResumoRankingMototristaVO r = new ResumoRankingMototristaVO();
            r.setCor(cor);
            r.setIdMotorista(motoristas.getId());
            String m = motoristas.getNome() + " " + motoristas.getSobrenome();
            r.setNomeMotorista(m);
            r.setNota(nota);
            lista.add(r);
        } catch (Exception e) {
            LogSistema.logError("processarRanking", e);
        }
    }

    private void ajustarDadosRankingMotorista(List<RankingMotorista> listaRankingMotorista) {
        try {
            Date ultimaHora = null;
            for (RankingMotorista rm : listaRankingMotorista) {
                if (mapaDeRankingMotorista.containsKey(rm.getTipoRanking())) {
                    int minutos = Minutes.minutesBetween(new DateTime(ultimaHora), new DateTime(rm.getDataCadastro())).getMinutes();
                    if (minutos > 1) {
                        mapaDeRankingMotorista.get(rm.getTipoRanking()).add(rm);
                    }
                    ultimaHora = rm.getDataCadastro();
                } else {
                    ultimaHora = null;
                    List<RankingMotorista> lista = new ArrayList<>();
                    lista.add(rm);
                    mapaDeRankingMotorista.put(rm.getTipoRanking(), lista);
                    ultimaHora = rm.getDataCadastro();
                }
            }
        } catch (Exception e) {
            LogSistema.logError("ajustarDadosRankingMotorista", e);
        }
    }


    public RankingMototristaVO buscarRankingMotorista(Integer idMotorista) {
        RankingMototristaVO vo = new RankingMototristaVO();
        try {
            List<Integer> listaDeNotasMedias = new ArrayList<>();
            List<NotasMotoristaGraficoVO> listaDeNotas = new ArrayList<>();
            List<RankingMotorista> listaRankingMotorista = rankingMotoristaDAO.buscarListaDeRankingMotorista(idMotorista);

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Map<String, List<RankingMotorista>> mapaTemp = new HashMap<>();
            for (RankingMotorista rm : listaRankingMotorista) {
                String dataF = simpleDateFormat.format(rm.getDataCadastro());
                if (mapaTemp.containsKey(dataF)) {
                    mapaTemp.get(dataF).add(rm);
                } else {
                    List<RankingMotorista> l = new ArrayList<>();
                    l.add(rm);
                    mapaTemp.put(dataF, l);
                }
            }


            mapaTemp = mapaTemp.entrySet().stream()
                    .sorted(Map.Entry.comparingByKey())
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                            (oldValue, newValue) -> oldValue, LinkedHashMap::new));

            for (Map.Entry<String, List<RankingMotorista>> entry : mapaTemp.entrySet()) {
                List<RankingMotorista> lista = entry.getValue();
                mapaDeRankingMotorista.clear();
                ajustarDadosRankingMotorista(lista);
                processarDetalhesRanking(listaDeNotas, listaDeNotasMedias, lista.get(0).getDataCadastro());
            }

            MotoristaRankingDadosVO motoristaRankingDados = new MotoristaRankingDadosVO();
            motoristaRankingDados.setListaDeNotas(listaDeNotas);
            Motoristas motoristas = motoristasDAO.getById(idMotorista);
            motoristaRankingDados.setNomeDoMotorista(motoristas.getNome() + " " + motoristas.getSobrenome());
            Integer sum = listaDeNotasMedias.stream().reduce(0, Integer::sum);
            if (sum > 0) {
                int media = sum / listaDeNotasMedias.size();
                motoristaRankingDados.setMediaNotas(media);
            } else {
                motoristaRankingDados.setMediaNotas(1);
            }
            ajustarTabelaResumoMotorista(motoristaRankingDados, listaRankingMotorista);
            vo.setMotoristaRankingDados(motoristaRankingDados);
        } catch (Exception e) {
            LogSistema.logError("buscarResumo", e);
        }

        return vo;
    }


    private void processarDetalhesRanking(List<NotasMotoristaGraficoVO> listaDeNotas, List<Integer> listaDeNotasMedia, Date ultimaData) {
        try {
            double[] notas = new double[mapaDeRankingMotorista.size()];
            int cont = 0;
            for (Map.Entry<TipoRanking, List<RankingMotorista>> entry : mapaDeRankingMotorista.entrySet()) {
                TipoRanking tipoRanking = entry.getKey();
                int totalDeIntens = entry.getValue().size();
                int maximoEventoMinimo = tipoRanking.getMaximoEventoMinimo();
                int maximoEventoMedio = tipoRanking.getMaximoEventoMedio();
                int maximoEventomaximoAcimaDe = tipoRanking.getMaximoEventomaximoAcimaDe();

                if (totalDeIntens <= maximoEventoMinimo) {
                    double porcentagem = (totalDeIntens * 100) / maximoEventoMinimo;
                    notas[cont] = porcentagem;
                }

                if (totalDeIntens >= maximoEventoMinimo && totalDeIntens < maximoEventoMedio) {
                    double porcentagem = (totalDeIntens * 100) / maximoEventoMedio;
                    notas[cont] = porcentagem;
                }

                if (totalDeIntens >= maximoEventomaximoAcimaDe) {
                    double porcentagem = (totalDeIntens * 100) / maximoEventomaximoAcimaDe;
                    notas[cont] = porcentagem;
                }
                cont++;
            }

            double media = 0d;
            for (int i = 0; i < notas.length; i++) {
                media = media + notas[i];
            }

            media = media / notas.length;
            int nota = 1;

            if (media > 0d && media <= 19d) {
                nota = 1;
            }
            if (media >= 20d && media <= 29d) {
                nota = 2;
            }
            if (media >= 30d && media <= 39d) {
                nota = 3;
            }
            if (media >= 40d && media <= 49d) {
                nota = 4;
            }
            if (media >= 50d && media <= 59d) {
                nota = 5;
            }
            if (media >= 60d && media <= 69d) {
                nota = 6;
            }
            if (media >= 70d && media <= 79d) {
                nota = 7;
            }
            if (media >= 80d && media <= 89d) {
                nota = 8;
            }
            if (media >= 90d && media < 99d) {
                nota = 8;
            }
            if (media >= 99d) {
                nota = 10;
            }

            NotasMotoristaGraficoVO notasMotoristaGraficoVO = new NotasMotoristaGraficoVO();
            listaDeNotasMedia.add(nota);
            notasMotoristaGraficoVO.setDataDoEvento(ultimaData);
            notasMotoristaGraficoVO.setNota(nota);
            notasMotoristaGraficoVO.setS(new SimpleDateFormat("dd/MM/yyyy").format(ultimaData));
            listaDeNotas.add(notasMotoristaGraficoVO);
        } catch (Exception e) {
            LogSistema.logError("processarDetalhesRanking", e);
        }
    }


    private void ajustarTabelaResumoMotorista(MotoristaRankingDadosVO motoristaRankingDados, List<RankingMotorista> listaRankingMotorista) {
        try {
            List<String> listaDeTitulos = new ArrayList<>();
            listaDeTitulos.add("Data");
            listaDeTitulos.add("Evento");


            Collections.sort(listaRankingMotorista, new Comparator<RankingMotorista>() {
                public int compare(RankingMotorista o1, RankingMotorista o2) {
                    return o1.getDataCadastro().compareTo(o2.getDataCadastro());
                }
            });

            List<String[]> listaDeDados = new ArrayList<>();

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            Date ultimaData = null;
            for (RankingMotorista rankingMotorista : listaRankingMotorista) {
                if (Objects.isNull(ultimaData)) {
                    ultimaData = rankingMotorista.getDataCadastro();
                    String[] d1 = new String[3];
                    d1[0] = simpleDateFormat.format(rankingMotorista.getDataCadastro());
                    d1[1] = rankingMotorista.getTipoRanking().getDescricao();
                    listaDeDados.add(d1);
                } else {
                    int minutos = Minutes.minutesBetween(new DateTime(ultimaData), new DateTime(rankingMotorista.getDataCadastro())).getMinutes();
                    if (minutos > 1) {
                        String[] d1 = new String[3];
                        d1[0] = simpleDateFormat.format(rankingMotorista.getDataCadastro());
                        d1[1] = rankingMotorista.getTipoRanking().getDescricao();
                        listaDeDados.add(d1);
                    }
                    ultimaData = rankingMotorista.getDataCadastro();
                }
            }
            motoristaRankingDados.setListaDeDados(listaDeDados);
            motoristaRankingDados.setListaDeTitulos(listaDeTitulos);
        } catch (Exception e) {
            LogSistema.logError("ajustarTabelaResumoMotorista", e);
        }
    }


    public List<RelatorioPontuacaoAvancadaVO> processarRelatorioPontuacaoAvancada(FiltroVO filtroVO) {
        List<RelatorioPontuacaoAvancadaVO> lista = new ArrayList<>();
        try {
            List<Motoristas> listaDeMotoristas = motoristasDAO.findAllRankingMotorista(filtroVO.getIdCliente(), filtroVO.getIdUnidade());
            for (Motoristas motoristas : listaDeMotoristas) {
                List<RankingMotorista> listaRankingMotorista = rankingMotoristaDAO.buscarListaDeRankingMotorista(motoristas.getId(), filtroVO.getDataInicial(), filtroVO.getDataFinal());
                mapaDeRankingMotorista.clear();
                ajustarDadosRankingMotorista(listaRankingMotorista);
                processarRelatorioPontuacaoAvancada(motoristas, lista);
            }
        } catch (Exception e) {
            LogSistema.logError("processarRelatorioPontuacaoAvancada", e);
        }
        return lista;
    }


    private void processarRelatorioPontuacaoAvancada(Motoristas motoristas, List<RelatorioPontuacaoAvancadaVO> lista) {
        try {
            int totalAcelaracaoBrusca = 0;
            int totalCurcaBruscaDireita = 0;
            int totalCurcaBruscaEsquerda = 0;
            int totalFreadaBrusca = 0;
            int totalExcessoVelocidadeUm = 0;
            int totalExcessoVelocidadeDois = 0;
            int totalMarchaLenta = 0;
            int totalDirecaoAcimaFaixaVerde = 0;

            double[] notas = new double[mapaDeRankingMotorista.size()];
            int cont = 0;
            for (Map.Entry<TipoRanking, List<RankingMotorista>> entry : mapaDeRankingMotorista.entrySet()) {
                TipoRanking tipoRanking = entry.getKey();
                int totalDeIntens = entry.getValue().size();
                switch (tipoRanking.getTipo()) {
                    case "1":
                        totalAcelaracaoBrusca = totalDeIntens;
                        break;
                    case "2":
                        totalCurcaBruscaDireita = totalDeIntens;
                        break;
                    case "3":
                        totalCurcaBruscaEsquerda = totalDeIntens;
                        break;
                    case "4":
                        totalFreadaBrusca = totalDeIntens;
                        break;
                    case "5":
                        totalExcessoVelocidadeUm = totalDeIntens;
                        break;
                    case "6":
                        totalExcessoVelocidadeDois = totalDeIntens;
                        break;
                    case "7":
                        totalMarchaLenta = totalDeIntens;
                        break;
                    case "8":
                        totalDirecaoAcimaFaixaVerde = totalDeIntens;
                        break;
                }

                int maximoEventoMinimo = tipoRanking.getMaximoEventoMinimo();
                int maximoEventoMedio = tipoRanking.getMaximoEventoMedio();
                int maximoEventomaximoAcimaDe = tipoRanking.getMaximoEventomaximoAcimaDe();

                if (totalDeIntens < maximoEventoMinimo) {
                    double porcentagem = (totalDeIntens * 100) / maximoEventoMinimo;
                    notas[cont] = porcentagem;
                }

                if (totalDeIntens >= maximoEventoMinimo && totalDeIntens < maximoEventoMedio) {
                    double porcentagem = (totalDeIntens * 100) / maximoEventoMedio;
                    notas[cont] = porcentagem;
                }

                if (totalDeIntens >= maximoEventomaximoAcimaDe) {
                    double porcentagem = (totalDeIntens * 100) / maximoEventomaximoAcimaDe;
                    notas[cont] = porcentagem;
                }
                cont++;
            }

            double media = 0d;
            for (int i = 0; i < notas.length; i++) {
                media = media + notas[i];
            }

            media = media / notas.length;
            int nota = 1;

            if (media > 0d && media <= 19d) {
                nota = 1;
            }
            if (media >= 20d && media <= 29d) {
                nota = 2;
            }
            if (media >= 30d && media <= 39d) {
                nota = 3;
            }
            if (media >= 40d && media <= 49d) {
                nota = 4;
            }
            if (media >= 50d && media <= 59d) {
                nota = 5;
            }
            if (media >= 60d && media <= 69d) {
                nota = 6;
            }
            if (media >= 70d && media <= 79d) {
                nota = 7;
            }
            if (media >= 80d && media <= 89d) {
                nota = 8;
            }
            if (media >= 90d && media < 99d) {
                nota = 8;
            }
            if (media >= 99d) {
                nota = 10;
            }

            RelatorioPontuacaoAvancadaVO vo = new RelatorioPontuacaoAvancadaVO();
            vo.setNomeMotorista(motoristas.getNome() + " " + motoristas.getSobrenome());
            vo.setNota(nota);
            vo.setQtdeAcelaracaoBrusca(totalAcelaracaoBrusca);
            vo.setQtdeCurvaBruscaDireita(totalCurcaBruscaDireita);
            vo.setQtdeCurvaBruscaEsquerda(totalCurcaBruscaEsquerda);
            vo.setQtdeFreadaBrusca(totalFreadaBrusca);
            vo.setQtdeExcessoVel80kmh(totalExcessoVelocidadeUm);
            vo.setQtdeExcessoVel60kmhChuva(totalExcessoVelocidadeDois);
            vo.setQtdeMarchaLenta(totalMarchaLenta);
            vo.setQtdeDirecaoAcimaFaixaVerde(totalDirecaoAcimaFaixaVerde);
            vo.setDistanciaPercorrida("");
            lista.add(vo);
        } catch (Exception e) {
            LogSistema.logError("processarRanking", e);
        }
    }


    public List<TemposDeFaixaVO> processarRelatorioTempoDeFaixa(FiltroVO filtroVO) {
        List<TemposDeFaixaVO> lista = new ArrayList<>();
        try {
            List<Motoristas> listaDeMotoristas = motoristasDAO.findAllRankingMotorista(filtroVO.getIdCliente(), filtroVO.getIdUnidade());
            for (Motoristas motoristas : listaDeMotoristas) {
                List<MotoristasVeiculos> listaMotoristasVeiculos = motoristasVeiculosService.getByDriver(motoristas.getId());
                int tempoEmFaixaAzul = 0;
                int tempoEmFaixaEconomica = 0;
                int tempoEmFaixaVerde = 0;
                int tempoEmFaixaAmarela = 0;
                int tempoEmFaixaVermelha = 0;
                int tempoEmMarchaLenta = 0;
                for (MotoristasVeiculos motoristasVeiculos : listaMotoristasVeiculos) {
                    Veiculos veiculos = new VeiculosDAO(null).buscarVeiculoPeloId(motoristasVeiculos.getVeiculoOrion());

                    List<Telemetria> listaTelemetria = new TelemetriaDAO().buscarUltimaTelemetria(motoristasVeiculos.getVeiculoOrion(), filtroVO.getDataInicial(), filtroVO.getDataFinal());
                    Telemetria ultimaTelemetria = null;
                    for (Telemetria telemetria : listaTelemetria) {
                        if (Objects.nonNull(telemetria.getRpm())) {
                            if (Objects.isNull(ultimaTelemetria)) {
                                ultimaTelemetria = telemetria;
                            } else {
                                Integer rpm = telemetria.getRpm();
                                if(Objects.isNull(rpm)){
                                    continue;
                                }

                                if (rpm >= veiculos.getRpmInicioFaixaAzul()  && rpm <= veiculos.getRpmFimFaixaAzul()) {
                                    //calcula faixa verde
                                    int segundos = Seconds.secondsBetween(new DateTime(ultimaTelemetria.getDataCadastro()), new DateTime(telemetria.getDataCadastro())).getSeconds();
                                    tempoEmFaixaAzul = tempoEmFaixaAzul + segundos;
                                    ultimaTelemetria = telemetria;
                                }

                                if (rpm >= veiculos.getRpmInicioFaixaEconomica()  && rpm <= veiculos.getRpmFimFaixaEconomica()) {
                                    int segundos = Seconds.secondsBetween(new DateTime(ultimaTelemetria.getDataCadastro()), new DateTime(telemetria.getDataCadastro())).getSeconds();
                                    tempoEmFaixaEconomica = tempoEmFaixaEconomica + segundos;
                                    ultimaTelemetria = telemetria;
                                }

                                if (rpm >= veiculos.getRpmInicioFaixaVerde()  && rpm <= veiculos.getRpmFimFaixaVerde()) {
                                    int segundos = Seconds.secondsBetween(new DateTime(ultimaTelemetria.getDataCadastro()), new DateTime(telemetria.getDataCadastro())).getSeconds();
                                    tempoEmFaixaVerde = tempoEmFaixaVerde + segundos;
                                    ultimaTelemetria = telemetria;
                                }

                                if (rpm >= veiculos.getRpmInicioFaixaAmarela()  && rpm <= veiculos.getRpmFimFaixaAmarela()) {
                                    int segundos = Seconds.secondsBetween(new DateTime(ultimaTelemetria.getDataCadastro()), new DateTime(telemetria.getDataCadastro())).getSeconds();
                                    tempoEmFaixaAmarela = tempoEmFaixaAmarela + segundos;
                                    ultimaTelemetria = telemetria;
                                }

                                if (rpm >= veiculos.getRpmMaximo()) {
                                    int segundos = Seconds.secondsBetween(new DateTime(ultimaTelemetria.getDataCadastro()), new DateTime(telemetria.getDataCadastro())).getSeconds();
                                    tempoEmFaixaVermelha = tempoEmFaixaVermelha + segundos;
                                    ultimaTelemetria = telemetria;
                                }


                            }
                        }
                    }

                }
                TemposDeFaixaVO temposDeFaixaVO = new TemposDeFaixaVO();
                temposDeFaixaVO.setMotorista(motoristas.getNome() + " " + motoristas.getSobrenome());
                temposDeFaixaVO.setTempoFaixaAzul(converte(tempoEmFaixaAzul));
                temposDeFaixaVO.setTempoFaixaExtraEconomica(converte(tempoEmFaixaEconomica));
                temposDeFaixaVO.setTempoFaixaVerde(converte(tempoEmFaixaVerde));
                temposDeFaixaVO.setTempoFaixaAmarela(converte(tempoEmFaixaAmarela));
                temposDeFaixaVO.setTempoFaixaVermelha(converte(tempoEmFaixaVermelha));
                lista.add(temposDeFaixaVO);
            }
        } catch (Exception e) {
            LogSistema.logError("processarRelatorioTempoDeFaixa", e);
        }
        return lista;
    }

    public static String converte(int segundos) {
        if (segundos == 0) {
            return "00:00:00";
        }
        int hours = segundos / 3600;
        int secondsLeft = segundos - hours * 3600;
        int minutes = secondsLeft / 60;
        int seconds = secondsLeft - minutes * 60;
        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }
}
