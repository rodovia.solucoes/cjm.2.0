package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.activemq.ActiveMQMessageProducer;
import br.com.cmctransportes.cjm.constants.Constantes;
import br.com.cmctransportes.cjm.domain.entities.Empresas;
import br.com.cmctransportes.cjm.domain.entities.IbuttonVeiculoMotorista;
import br.com.cmctransportes.cjm.domain.entities.IdentificacaoOperador;
import br.com.cmctransportes.cjm.domain.entities.vo.EmbarcaIbuttonVO;
import br.com.cmctransportes.cjm.domain.entities.vo.MotoristaOperadorVO;
import br.com.cmctransportes.cjm.domain.entities.vo.VeiculoAutorizacaoVO;
import br.com.cmctransportes.cjm.infra.dao.IbuttonVeiculoMotoristaDAO;
import br.com.cmctransportes.cjm.infra.dao.IdentificacaoOperadorDAO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Service
public class IdentificacaoOperadorService implements Serializable{

    @Autowired
    private EmpresasService empresasService;

    private final EntityManager em;

    private IdentificacaoOperadorDAO identificacaoOperadorDAO;
    private IbuttonVeiculoMotoristaDAO ibuttonVeiculoMotoristaDAO;

    public IdentificacaoOperadorService(EntityManager em) {
        this.em = em;
        identificacaoOperadorDAO = new IdentificacaoOperadorDAO(em);
        ibuttonVeiculoMotoristaDAO = new IbuttonVeiculoMotoristaDAO(em);
    }

    public List<IdentificacaoOperador> buscarIdentificacaoOperador(Integer idEmpresa, Integer idUnidade) {
        return identificacaoOperadorDAO.buscarIdentificacaoOperador(idEmpresa, idUnidade);
    }

    public List<MotoristaOperadorVO> buscarMotoristaOperador(Integer idEmpresa, Integer idUnidade) {
        return identificacaoOperadorDAO.buscarMotoristaOperador(idEmpresa, idUnidade);
    }

    @Transactional
    public void cadastrar(IdentificacaoOperador identificacaoOperador) {
        identificacaoOperador.setAtualizar(Boolean.TRUE);
        contarIbuttons(identificacaoOperador);
        enviarFilaJMS(identificacaoOperador, null);
        identificacaoOperadorDAO.save(identificacaoOperador);
        inserirIbuttonVeiculoMotorista(identificacaoOperador);
    }

    @Transactional
    public void editar(IdentificacaoOperador identificacaoOperador) {
        identificacaoOperador.setAtualizar(Boolean.FALSE);
        contarIbuttons(identificacaoOperador);
        IdentificacaoOperador old = identificacaoOperadorDAO.getById(identificacaoOperador.getId());
        enviarFilaJMS(identificacaoOperador, old);
        identificacaoOperadorDAO.update(identificacaoOperador);
        inserirIbuttonVeiculoMotorista(identificacaoOperador);
    }


    private void inserirIbuttonVeiculoMotorista(IdentificacaoOperador identificacaoOperador){
        try {
            List<String> listaDeVeiculos = quebrarDados(identificacaoOperador.getListaVeiculos());
            List<String> listaDeMotoristas = quebrarDados(identificacaoOperador.getListaMotoristas());
            List<String> listaDeOperador = quebrarDados(identificacaoOperador.getListaOperadores());

            //Inserir veiculos com motoristas
            for(String s : listaDeVeiculos){
                for(String ss : listaDeMotoristas){
                    IbuttonVeiculoMotorista ivm= new IbuttonVeiculoMotorista();
                    ivm.setIdentificacaoOperador(identificacaoOperador.getId());
                    ivm.setIdVeiculo(Integer.parseInt(s));
                    ivm.setMotorista(Integer.parseInt(ss));
                    ibuttonVeiculoMotoristaDAO.save(ivm);
                }
            }

            for(String s : listaDeVeiculos){
                for(String ss : listaDeOperador){
                    IbuttonVeiculoMotorista ivm= new IbuttonVeiculoMotorista();
                    ivm.setIdentificacaoOperador(identificacaoOperador.getId());
                    ivm.setIdVeiculo(Integer.parseInt(s));
                    ivm.setOperador(Integer.parseInt(ss));
                    ibuttonVeiculoMotoristaDAO.save(ivm);
                }
            }

        }catch (Exception e) {
            LogSistema.logError("inserirIbuttonVeiculoMotorista", e);
        }
    }

    private List<String> quebrarDados(String dados) {
        List<String> l = new ArrayList<>();
        if(Objects.nonNull(dados) && !dados.isEmpty()) {
            if (dados.contains(";")) {
                String[] s = dados.split(";");
                l.addAll(Arrays.asList(s));
            } else {
                l.add(dados);
            }
        }
        return l;
    }

    public List<VeiculoAutorizacaoVO> buscarVeiculoAutorizacao(Integer idEmpresa, Integer idUnidade) {
        Empresas empresas = empresasService.getById(idEmpresa);
        return identificacaoOperadorDAO.buscarVeiculoAutorizacao(empresas.getIdClienteTrajetto(), idUnidade);
    }

    public IdentificacaoOperador buscarIdentificacaoOperadorPeloId(Integer id) {
        return identificacaoOperadorDAO.getById(id);
    }


    private void enviarFilaJMS(IdentificacaoOperador identificacaoOperadorNovo, IdentificacaoOperador identificacaoOperadorAntigo) {
        try {
            EmbarcaIbuttonVO t = new EmbarcaIbuttonVO();
            t.setIdentificacaoOperadorAntigo(identificacaoOperadorAntigo);
            t.setIdentificacaoOperadorNovo(identificacaoOperadorNovo);
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String json = ow.writeValueAsString(t);
            ActiveMQMessageProducer msgQueueSender = new ActiveMQMessageProducer();
            msgQueueSender.setup(true, false, Constantes.EMBARCAR_IBUTTON);
            msgQueueSender.sendMessage(json);
            msgQueueSender.commit(true);
            msgQueueSender.close();
        } catch (Exception e) {
            LogSistema.logError("enviarFilaJMS", e);
        }
    }



    private void contarIbuttons(IdentificacaoOperador identificacaoOperador){
        int cont = 0;
        try {
            if(Objects.nonNull(identificacaoOperador.getListaMotoristas()) && !identificacaoOperador.getListaMotoristas().isEmpty()){
                cont = cont + identificacaoOperador.getListaMotoristas().split(";").length;
            }

            if(Objects.nonNull(identificacaoOperador.getListaOperadores()) && !identificacaoOperador.getListaOperadores().isEmpty()){
                cont = cont + identificacaoOperador.getListaOperadores().split(";").length;
            }

            identificacaoOperador.setQuantidadeIbutton(cont);
        }catch (Exception e){
            LogSistema.logError("contarIbuttons", e);
        }
    }
}
