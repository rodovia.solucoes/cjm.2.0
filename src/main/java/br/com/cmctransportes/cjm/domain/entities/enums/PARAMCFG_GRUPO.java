package br.com.cmctransportes.cjm.domain.entities.enums;

/**
 * Representa os grupos permitidos do projeto
 *
 * @author Cristhiano Roberto
 */
public enum PARAMCFG_GRUPO {

    SISTEMA,
    DSR,
    INTERJORNADA;

}
