package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author William Leite
 */
@Entity
@Table(name = "entity_log")
@Getter
@Setter
public class EntityLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "ENTITY_LOG_ID_SEQ", sequenceName = "ENTITY_LOG_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENTITY_LOG_ID_SEQ")
    @Column(name = "id")
    private Integer id;

    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Users userId;

    @Column(name = "data_operacao", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date data_operacao;

    @Column(name = "operacao", nullable = false)
    private String operacao;

    @Column(name = "entity_id", nullable = true)
    private Integer entityId;

    @Column(name = "entity_type", nullable = false)
    private String entityType;

    @Column(name = "json", columnDefinition = "text")
    private String json;
}