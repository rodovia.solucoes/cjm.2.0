package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author William Leite
 */
@Entity
@Table(name = "journey_manifest")
@Getter
@Setter
public class JourneyManifest implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "JOURNEY_MANIFEST_ID_SEQ", sequenceName = "JOURNEY_MANIFEST_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "JOURNEY_MANIFEST_ID_SEQ")
    @Column(name = "id")
    private Long id;

    @Column(name = "start_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    @Column(name = "end_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    @JoinColumn(name = "journey_id", referencedColumnName = "id")
    @ManyToOne
    private Jornada journey;

    @Column(name = "manifest_number")
    private String manifestNumber;
}