package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.Empresas;
import br.com.cmctransportes.cjm.domain.entities.Transmissao;
import br.com.cmctransportes.cjm.domain.entities.vo.MonitorViaSatVO;
import br.com.cmctransportes.cjm.domain.entities.vo.VeiculoVO;
import br.com.cmctransportes.cjm.infra.dao.TransmissaoDAO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.proxy.VeiculoTrajetto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class MonitorViaSatService {
    @Autowired
    private EmpresasService empresasService;

    public List<MonitorViaSatVO>  buscarDados(Integer idCliente, Integer idUnidade){
        List<MonitorViaSatVO> lista = new ArrayList<>();
        try {
            TransmissaoDAO transmissaoDAO = new TransmissaoDAO();
            Empresas empresas = empresasService.getById(idCliente);
            List<VeiculoVO> listaDeVeiculos = VeiculoTrajetto.getInstance().listarVeiculos(empresas.getIdClienteTrajetto(), idUnidade);
            if(Objects.nonNull(listaDeVeiculos)){
                for(VeiculoVO veiculoVO: listaDeVeiculos) {
                    Transmissao transmissao = transmissaoDAO.buscarUltimaTransmissaoMongo(veiculoVO.getId());
                    if(Objects.nonNull(transmissao)) {
                        MonitorViaSatVO monitorViaSatVO = new MonitorViaSatVO();
                        monitorViaSatVO.setPlaca(veiculoVO.getPlaca());
                        monitorViaSatVO.setIdVeiculo(veiculoVO.getId());
                        monitorViaSatVO.setTransmissao(transmissao);
                        lista.add(monitorViaSatVO);
                    }
                }
            }
        }catch (Exception e){
            LogSistema.logError("buscarDados", e);
        }
        return lista;
    }


}
