package br.com.cmctransportes.cjm.domain.entities;

import br.com.cmctransportes.cjm.domain.services.EventosJornadaService;
import br.com.cmctransportes.cjm.services.JourneyCalculusService;
import br.com.cmctransportes.cjm.services.nonconformity.InterjourneyNonconfomityService;
import br.com.cmctransportes.cjm.services.nonconformity.PaidWeeklyRestNonconformityService;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * @author analista.ti
 */
@Getter
@Setter
public class RelatorioDeJornada {

    private Integer mesReferencia;
    private Motoristas motorista;
    private Date dataInicio;
    private Date dataFim;

    // Empresa
    private Date calculaDataFim(Integer mesReferencia) {
        GregorianCalendar agora = (GregorianCalendar) GregorianCalendar.getInstance();

        int ano = agora.get(GregorianCalendar.YEAR);
        int mes = mesReferencia;
        int dia = 20;   // TODO: usar configuração para isso aqui

        // se o mês de referência for maior que o atual, pega o ano anterior
        if (mesReferencia > agora.get(GregorianCalendar.MONTH)) {
            ano -= 1;
        }

        // se o  mês for igual
        if (mesReferencia == agora.get(GregorianCalendar.MONTH)) {

            // se não passou do dia 20 ainda não fechou o mês, pega o anterior
            if (agora.get(GregorianCalendar.DAY_OF_MONTH) <= dia) {
                mes -= 1;
            }
        }

        return (new GregorianCalendar(ano, mes, dia)).getTime();
//        return (new GregorianCalendar(ano, 11, 30)).getTime(); // TODO: valor para teste
    }

    private Date calculaDataInicio(Integer mesReferencia) {
        GregorianCalendar tmpCalendar = new GregorianCalendar();
        tmpCalendar.setTime(calculaDataFim(mesReferencia));
        tmpCalendar.add(GregorianCalendar.MONTH, -1);
        return tmpCalendar.getTime();
//        return (new GregorianCalendar(2016, 0, 1)).getTime(); // TODO: valor para teste
    }

    public RelatorioDeJornada() {
    }

    public RelatorioDeJornada(Integer mesReferencia) {
        this.mesReferencia = mesReferencia;
        setDataInicio(calculaDataInicio(mesReferencia));
        setDataFim(calculaDataFim(mesReferencia));
    }

    public RelatorioDeJornada(Date inicio, Date fim) {
        GregorianCalendar tmpCalendar = new GregorianCalendar();
        tmpCalendar.setTime(fim);

        this.mesReferencia = tmpCalendar.get(GregorianCalendar.MONTH);
        setDataInicio(inicio);
        setDataFim(fim);
    }

    public RelatorioDeJornada(Integer mesReferencia, List<Jornada> relatorioDeJornada, JourneyCalculusService calculusService,
                              PaidWeeklyRestNonconformityService paidWeeklyRestNonconformityService,
                              InterjourneyNonconfomityService interjourneyNonconfomityService,
                              EventosJornadaService eventosJornadaService) {
        this.mesReferencia = mesReferencia;
        setDataInicio(calculaDataInicio(mesReferencia));
        setDataFim(calculaDataFim(mesReferencia));

        setJornadas(relatorioDeJornada, calculusService, paidWeeklyRestNonconformityService, interjourneyNonconfomityService, eventosJornadaService);
    }

    // TODO: rever a lógica disso aqui
    public void setJornadas(List<Jornada> relatorioDeJornada, JourneyCalculusService calculusService,
                            PaidWeeklyRestNonconformityService paidWeeklyRestNonconformityService,
                            InterjourneyNonconfomityService interjourneyNonconfomityService,
                            EventosJornadaService eventosJornadaService) {
        if ((relatorioDeJornada != null) && (!relatorioDeJornada.isEmpty())) {
            Jornada first = relatorioDeJornada.get(0);

            setMotorista(first.getMotoristasId());
        }
    }

    /**
     * @return the identificacaoTurno
     */
    public String getIdentificacaoTurno() {
        if ((this.motorista != null) && (this.motorista.getTurnosId() != null)) {
            return this.motorista.getTurnosId().getIdentificacao();
        }

        return "";
    }
}
