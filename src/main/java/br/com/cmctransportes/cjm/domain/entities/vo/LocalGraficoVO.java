package br.com.cmctransportes.cjm.domain.entities.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class LocalGraficoVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String dataEvento;
    /*
     * Indica se é Começo ou fim
     */
    private String status;
    private String endereco;
    private String urlGoogleMaps;
    private Boolean carregado;
    private Boolean vazio;
    @JsonIgnore
    private int valorHora = 0;

    private String eventoInicio;
    private String eventoFinal;
    private String dataEventoFinal;
    private String tempoEntreEventos;

}
