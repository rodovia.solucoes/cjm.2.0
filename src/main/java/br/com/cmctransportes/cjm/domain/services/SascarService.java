package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.MacroSascar;
import br.com.cmctransportes.cjm.domain.entities.Motoristas;
import br.com.cmctransportes.cjm.domain.entities.Sascar;
import br.com.cmctransportes.cjm.domain.repository.MotoristasRepository;
import br.com.cmctransportes.cjm.domain.repository.SascarRepository;
import br.com.cmctransportes.cjm.infra.dao.MacroSascarDAO;
import br.com.cmctransportes.cjm.infra.dao.SascarDAO;
import br.com.cmctransportes.cjm.logger.LogSistema;
import br.com.cmctransportes.cjm.utils.ArquivoDeConfiguracao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * Desenvolvido por Cleber Leão
 */
@Service
public class SascarService {

    @Autowired
    private DriverService driverService;

    @Autowired
    private SascarRepository sascarRepository;

    @Autowired
    private MotoristasRepository motoristasRepository;


    @Autowired
    private final EntityManager em;


    private final SascarDAO dao;

    /**
     * Classe de instrução em serviços de consulta no banco de dados referente a integração Sascar
     * @param em
     */
    public SascarService(EntityManager em){
        this.em = em;
        dao = new SascarDAO(em);
    }

    /**
     *
     * @param driver mototrista para salvar no banco
     * @param sascar objeto Sascar para salvar no banco
     * @return o resultado se foi persistido no banco ou não
     * @throws SQLException caso ocorrão alguma excessão de SQL será apresentado no console
     */
    public Sascar save(Integer driver, Sascar sascar) throws SQLException {

        Sascar result = null;
        if (driver != null){
            Integer byIdSascar = this.verificaId(driver);
            if (byIdSascar == driver){
                result = null;
            }else if (byIdSascar == 0){
                Sascar sa = new Sascar();
                Date startDate = new Date();
                sa.setIdVeiculoSascar(1316898);
                sa.setOperadorLancamento(driver);
                sa.setSenhaSascar(sascar.getSenhaSascar());
                sa.setUsuarioSascar(sascar.getUsuarioSascar());
                sa.setStartDate(startDate);
                result = this.sascarRepository.save(sa);
            }
        }
        return result;
    }

    /**
     * Consulta para verificar usuário sascar salvo no banco de cada requisição no SOAP Sascar
     * @param id verifica se existe o id informado na consulta
     * @return usuario Sascar que pode fazer a consulta ou não
     */
    public Sascar getByIdSascar(Integer id) {
        Sascar usuarioSascar = dao.getByIdSascar(id);
        return usuarioSascar;
    }

    /**
     * Busca todos usuários sascar no banco
     * @return os usurios que existem na Sascar
     */
    public List<Sascar> findAllSascar() {
        return dao.findAllSascar();
    }

    public List<Sascar> findAllSascar(Boolean isManual) {
        return dao.findAllSascar(isManual);
    }

    /**
     * Verificar se operador de lançamento existe no banco de dados
     * @param driver motorista para ser verificado
     * @return o motorista encontrado
     * @throws SQLException
     */
    public Integer verificaId(Integer driver) throws SQLException {
        Integer result = dao.verificaId(driver);
        return result;
    }

    /**
     * Consultar a data inicial que é a ultima data consultada no SOAP que muda a cada nova requisição
     * @param motoristaId pelo ID motorista consulta qual é a data que foi a ultima consulta SOAP dele
     * @return a data da ultima consulta SOAP que foi realizada com sucesso
     */
    public long findDateSascar(Integer motoristaId) {
        long result = dao.findDateSascar(motoristaId);
        return result;
    }

    @Transactional
    public void gerarDadosSascar(Motoristas motoristas){
        try {
            //verificar se na base ja tem algum com o motorista
            Sascar sascar = dao.getByIdVeiculo(motoristas.getSascarId());
            if(sascar == null){
                //Cadastra um novo
                sascar = dao.getByIdSascar(motoristas.getId());
                if(sascar != null){
                    sascar.setIdVeiculoSascar(motoristas.getSascarId());
                    dao.update(sascar);
                }else {
                    sascar = new Sascar();
                    sascar.setOperadorLancamento(motoristas.getUserId());
                    sascar.setIdVeiculoSascar(motoristas.getSascarId());
                    sascar.setUsuarioSascar(new ArquivoDeConfiguracao().buscarValor("spring.sascar.usuario"));
                    sascar.setSenhaSascar(new ArquivoDeConfiguracao().buscarValor("spring.sascar.senha"));
                    sascar.setStartDate(new Date());
                    dao.save(sascar);
                }
            }else{
                sascar.setOperadorLancamento(motoristas.getId());
                dao.update(sascar);
            }
        }catch (Exception e){
            LogSistema.logError("gerarDadosSascar", e);
        }
    }

    public List<MacroSascar> buscarMacrosSascar(){
        return new MacroSascarDAO().buscar();
    }
}
