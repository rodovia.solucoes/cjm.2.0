package br.com.cmctransportes.cjm.domain.entities.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class NotasMotoristaGraficoVO {
    private Integer nota;
    private Date dataDoEvento;
    private String s;
}
