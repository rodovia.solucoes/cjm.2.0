/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cmctransportes.cjm.domain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author analista.ti
 */
@Entity
@Table(name = "tipos_estado_jornada")
@Getter
@Setter
@NamedQueries({
        @NamedQuery(name = "TiposEstadoJornada.findAll", query = "SELECT t FROM TiposEstadoJornada t"),
        @NamedQuery(name = "TiposEstadoJornada.findById", query = "SELECT t FROM TiposEstadoJornada t WHERE t.id = :id"),
        @NamedQuery(name = "TiposEstadoJornada.findByDescricao", query = "SELECT t FROM TiposEstadoJornada t WHERE t.descricao = :descricao")})
public class TiposEstadoJornada implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TIPOS_ESTADO_JORNADA_ID_SEQ", sequenceName = "TIPOS_ESTADO_JORNADA_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TIPOS_ESTADO_JORNADA_ID_SEQ")
    @Column(name = "id")
    private Integer id;

    @Column(name = "descricao", length = 15, nullable = false)
    private String descricao;

    @Column(name = "color", length = 7)
    private String color;

    @Column(name = "icone", length = 200)
    private String icone;
}