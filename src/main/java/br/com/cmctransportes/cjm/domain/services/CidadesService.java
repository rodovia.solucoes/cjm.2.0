/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cmctransportes.cjm.domain.services;

import br.com.cmctransportes.cjm.domain.entities.Cidades;
import br.com.cmctransportes.cjm.domain.repository.CidadesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author analista.ti
 */
@Service
public class CidadesService {

    @Autowired
    private CidadesRepository repository;

    public Cidades getbyId(String id) {
        return this.repository.findById(id).get();
    }

    public List<Cidades> findAll() {
        return this.repository.findAll();
    }

}
