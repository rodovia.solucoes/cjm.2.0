package br.com.cmctransportes.cjm.utils;

import br.com.cmctransportes.cjm.domain.entities.LatLng;

public class CalculadorDistancia {

    public synchronized Double calcularDistancia(LatLng latLng1, LatLng latLng2) {
        String unit = "K"; //Kilometros
        double theta = latLng1.longitude - latLng2.longitude;
        double dist = Math.sin(deg2rad(latLng1.latitude)) * Math.sin(deg2rad(latLng2.latitude)) + Math.cos(deg2rad(latLng1.latitude)) * Math.cos(deg2rad(latLng2.latitude)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        if ("K".equals(unit)) {
            dist = dist * 1.609344;
        } else if ("N".equals(unit)) {
            dist = dist * 0.8684;
        }

        return (dist);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::	This function converts decimal degrees to radians
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::        This function converts radians to decimal degrees
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }
}
