package br.com.cmctransportes.cjm.utils;

import br.com.cmctransportes.cjm.logger.LogSistema;
import lombok.Getter;
import lombok.Setter;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @author Producao
 */
public class TimeHelper {

    public static final long UM_SEGUNDO = 1000;
    public static final long UM_MINUTO = 60 * UM_SEGUNDO;
    public static final long UMA_HORA = 60 * UM_MINUTO;
    public static final long UM_DIA = 24 * UMA_HORA;
    public static final long DEZ_MINUTOS = 10 * UM_MINUTO;
    public static final long QUINZE_MINUTOS = 15 * UM_MINUTO;
    public static final long TRINTA_MINUTOS = 30 * UM_MINUTO;
    public static final long DUAS_HORAS = 2 * UMA_HORA;
    public static final long QUATRO_HORAS = 4 * UMA_HORA;
    public static final long CINCO_HORAS = 5 * UMA_HORA;
    public static final long SEIS_HORAS = 6 * UMA_HORA;
    public static final long VINTEDUAS_HORAS = 22 * UMA_HORA;
    public static final long VINTEQUATRO_HORAS = 24 * UMA_HORA;



    public static Date fixDaylightSavings(final Date date) {
        boolean isDaylight = TimeZone.getDefault().inDaylightTime(date);
        if (isDaylight) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.HOUR_OF_DAY, -1);
            return cal.getTime();
        }
        return date;
    }

    public static long nullsafeSubtract(final Long l1, final Long l2, final Long l3) {
        long result = 0L;
        if (Objects.nonNull(l1)) {
            result = l1;
        }
        if (Objects.nonNull(l2)) {
            result -= l2;
        }
        if (Objects.nonNull(l3)) {
            result -= l3;
        }
        return result;
    }

    public static long nullsafeAdd(final Long... numbers) {
        return Arrays.stream(numbers).filter(Objects::nonNull).reduce(0L, Long::sum);
    }

    public static long minutes(int minutes) {
        return minutes * TimeHelper.UM_MINUTO;
    }

    public static long hours(int hours) {
        return hours * TimeHelper.UMA_HORA;
    }

    public static long days(int days) {
        return days * TimeHelper.UM_DIA;
    }

    public static long seconds(int seconds) {
        return seconds * TimeHelper.UM_SEGUNDO;
    }

    public static String beautifyTime(long msec, boolean fullFormat) {
        if (msec < 1) {
            return "00:00:00";
        }

        DecimalFormat df = new DecimalFormat("00");
        df.setRoundingMode(RoundingMode.DOWN);

        long time = msec;
        long hh = time / TimeHelper.UMA_HORA;
        time -= hh * TimeHelper.UMA_HORA;

        long mm = time / TimeHelper.UM_MINUTO;
        time -= mm * TimeHelper.UM_MINUTO;

        long ss = time / 1000;
        time -= ss * 1000;

        if (fullFormat) {
            return df.format(hh) + ":" + df.format(mm) + ":" + df.format(ss);
        } else {
            return df.format(hh) + ":" + df.format(mm);
        }
    }

    public static boolean isSunday(Date inicio) {
        if (Objects.isNull(inicio)) {
            return false;
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(inicio);

        return cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY;
    }

    public static LocalDateTime toLocalDateTime(Date date) {
        if (date.getClass().equals(java.sql.Date.class)) {
            Date d = buscarDataAtual();
            d.setTime(date.getTime());
            date = d;
        }
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    public static LocalDate toLocalDate(Date date) {
        if (date.getClass().equals(java.sql.Date.class)) {
            Date d = buscarDataAtual();
            d.setTime(date.getTime());
            date = d;
        }
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static Date fromLocalDate(LocalDate date) {
        return Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public static Date fromLocalDateTime(LocalDateTime date) {
        return Date.from(date.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static boolean isSameDay(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(date1);
        cal2.setTime(date2);
        return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) && cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
    }

    public static List<Date> getDatesBetween(Date start, Date end) {
        List<Date> result = new ArrayList<>();
        LocalDate startDate = TimeHelper.toLocalDate(start);
        LocalDate endDate = TimeHelper.toLocalDate(end);

        while (!startDate.isAfter(endDate)) {
            result.add(TimeHelper.fromLocalDate(startDate));
            startDate = startDate.plusDays(1);
        }
        return result;
    }

    public static long getDifferenceDays(Date beginDate, Date endDate) {
        long diff = endDate.getTime() - beginDate.getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }

    public static Date addDays(Date data, int dias) {
        long newDate = data.getTime() + TimeHelper.days(dias);
        Date d = buscarDataAtual();
        d.setTime(newDate);
        return d;
    }

    public static Date getDate235959(Date d) {
        if (d == null) {
            return null;
        }
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        c.set(Calendar.MILLISECOND, 999);
        return c.getTime();
    }

    public static Date getDateWithNowTime(Date d) {
        if (d == null) {
            return null;
        }

        Date dd = new Date();
        Calendar c = Calendar.getInstance();
        Calendar cc = Calendar.getInstance();
        c.setTime(d);
        c.set(Calendar.HOUR_OF_DAY, cc.get(Calendar.HOUR_OF_DAY));
        c.set(Calendar.MINUTE, cc.get(Calendar.MINUTE));
        c.set(Calendar.SECOND, 59);
        c.set(Calendar.MILLISECOND, 999);
        return c.getTime();
    }

    public static Date getDate000000(Date d) {
        if (d == null) {
            return null;
        }
        Calendar c = new GregorianCalendar();
        c.setTime(d);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTime();
    }


    public static Date diminuirHoras(Date d, Integer horasDiminuir) {
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        c.add(Calendar.HOUR_OF_DAY, horasDiminuir);
        return c.getTime();
    }

    public static Date getDate000000() {
        return getDate000000(buscarDataAtual());
    }

    public static Date convertHorasStringEmDate(String hora){
        try{
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
            return dateFormat.parse(hora);
        }catch (Exception e){
            return buscarDataAtual();
        }
    }


    public static int convertHorasStringEmMinutes(String hora){
        try{
            String[] s = hora.split(":");
            int minutosDasHoras = Integer.valueOf(s[0]) * 60;
            int minutos = Integer.valueOf(s[1]);
            int valor = minutosDasHoras + minutos;
            return valor;
        }catch (Exception e){}
        return 30; //caso ocorra erro retorno 30 minutos por default

    }

    public static String converteMinutosEmStringFormatada(int minutos){
        int hours = minutos / 60;
        int minutes = minutos % 60;
        return String.format("%dh:%02dm", hours, minutes);
    }


    public static String converteMinutosEmStringFormatadaDois(int minutos){
        int hours = minutos / 60;
        int minutes = minutos % 60;
        return String.format("%dh:%02dm", hours, minutes);
    }

    public static String converteMinutosEmStringFormatadaTres(long milliseconds){
        int minutos =  (int) ((milliseconds / 1000) / 60);
        int hours = minutos / 60;
        int minutes = minutos % 60;
        return String.format("%02d:%02d", hours, minutes);
    }

    public static String converteMinutosEmStringFormatadaTres(double milliseconds){
        int minutos =  (int) ((milliseconds / 1000) / 60);
        int hours = minutos / 60;
        int minutes = minutos % 60;
        return String.format("%02d:%02d", hours, minutes);
    }


    public static String converteMinutosEmStringFormatadaTres(int minutos){
        int hours = minutos / 60;
        int minutes = minutos % 60;
        return String.format("%02d:%02d", hours, minutes);
    }



    public static int getDayOfWeek(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.DAY_OF_WEEK);
    }

    public static int getMinutesOfDate(Date date){
        Long r = buscarDataAtual().getTime() - date.getTime();
        r = (r / 1000) /60;
        int aqw = r.intValue();
        return aqw;
    }

    public static class Period {

        @Getter
        @Setter
        private Date start;
        @Getter
        @Setter
        private Date end;

        public Period(Date start, Date end) {
            this.start = start;
            this.end = end;
        }
    }

    public static Date diminuirSegundosNaData(Date date, int segundos){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.SECOND, (segundos * -1));
        return calendar.getTime();
    }

    public static String pegarDiaDaSemana(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int v = calendar.get(Calendar.DAY_OF_WEEK);
        switch (v){
            case 1: return "Dom";
            case 2: return "Seg";
            case 3: return "Ter";
            case 4: return "Qua";
            case 5: return "Qui";
            case 6: return "Sex";
            case 7: return "Sab";
        }
        return "-";
    }

    public static int pegarDiaDaSemana(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        int v = calendar.get(Calendar.DAY_OF_WEEK);
        switch (v){
            case 1: return 1;
            case 2: return 2;
            case 3: return 3;
            case 4: return 4;
            case 5: return 5;
            case 6: return 6;
            case 7: return 7;
        }
        return 0;
    }

    public static Date buscarDataAtual() {
        try {
            Calendar calendario
                    = new Calendar.Builder()
                    .setInstant(new Date())
                    .setTimeZone(TimeZone.getTimeZone("America/Sao_Paulo"))
                    .setLocale(new Locale("pt", "br"))

                    .build();
            calendario.add(Calendar.HOUR_OF_DAY, -1);
            return calendario.getTime();
        } catch (Exception e) {
            return new Date();
        }
    }

    public static Date buscarDataAtual(Long time) {
        try {
            Calendar calendario
                    = new Calendar.Builder()
                    .setInstant(new Date(time))
                    .setTimeZone(TimeZone.getTimeZone("America/Sao_Paulo"))
                    .setLocale(new Locale("pt", "br"))

                    .build();
            calendario.add(Calendar.HOUR_OF_DAY, -1);
            Date d = calendario.getTime();
            return d;
        } catch (Exception e) {
            return new Date();
        }
    }

    public static String mesString(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int month = cal.get(Calendar.MONTH);
        switch (month+1){
            case 1:return "Janeiro";
            case 2:return "Fevereiro";
            case 3:return "Março";
            case 4:return "Abril";
            case 5:return "Maio";
            case 6:return "Junho";
            case 7:return "Julho";
            case 8:return "Agosto";
            case 9:return "Setembro";
            case 10:return "Outubro";
            case 11:return "Novembro";
            case 12:return "Dezembro";
        }
        return null;
    }

    public static String conveterDataEmString(Date date, String padrao){
        return new SimpleDateFormat(padrao).format(date);
    }

}
