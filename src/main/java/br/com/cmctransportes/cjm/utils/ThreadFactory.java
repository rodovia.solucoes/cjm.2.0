package br.com.cmctransportes.cjm.utils;

import br.com.cmctransportes.cjm.constants.Constantes;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 *
 * @author Vinicius
 */
public class ThreadFactory {

    private static Executor executor;

    public static Executor getExecutor(){
        if(executor == null){
            executor = Executors.newFixedThreadPool(Constantes.NUMERO_DE_THREADS);
        }
        return executor;
    }
}
