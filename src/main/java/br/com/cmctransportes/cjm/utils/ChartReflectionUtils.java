package br.com.cmctransportes.cjm.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ChartReflectionUtils {

    private String assembleMethod(String prefix, String method) {
        return String.format("%s%s", prefix, method);
    }

    private String assembleGet(String property) {
        char upper = property.toUpperCase().charAt(0);
        String rest = property.substring(1);
        return this.assembleMethod("get", String.format("%s%s", upper, rest));
    }

    private String assembleSet(String property) {
        char upper = property.toUpperCase().charAt(0);
        String rest = property.substring(1);
        return this.assembleMethod("set", String.format("%s%s", upper, rest));
    }

    @SuppressWarnings("unchecked")
    public <R, T> R invokeGetter(T obj, String property) {
        try {
            String methodName = this.assembleGet(property);
            Method method = obj.getClass().getMethod(methodName, null);
            return (R) method.invoke(obj);
        } catch (IllegalArgumentException | SecurityException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            return null;
        }
    }

    public <T, P> T invokeSetter(T obj, String property, P value) {
        try {
            String methodName = this.assembleSet(property);
            Method method = obj.getClass().getMethod(methodName, value.getClass());
            method.invoke(obj, value);
            return obj;
        } catch (NullPointerException e) {
            return obj;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
