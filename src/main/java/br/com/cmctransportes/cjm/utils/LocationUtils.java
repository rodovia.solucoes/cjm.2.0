package br.com.cmctransportes.cjm.utils;

import br.com.cmctransportes.cjm.domain.entities.LatLng;
import br.com.cmctransportes.cjm.domain.entities.vo.LocalVO;

import java.awt.geom.Point2D;
import java.util.Comparator;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

public class LocationUtils {

    public Optional<LocalVO> closest(LocalVO ref, Set<LocalVO> collection) {
        Optional<LocalVO> result = Optional.empty();
        if (Objects.nonNull(ref.getLatitude()) && Objects.nonNull(ref.getLongitude())) {
            result = collection.stream().min(new NearestComparator(ref));
        }
        return result;
    }

    /**
     * Calculate distance between two points in latitude and longitude taking into account height difference. If you are not interested in height difference pass 0.0. Uses Haversine method as its
     * base.
     * <p>
     * lat1, lon1 Start point lat2, lon2 End point el1 Start altitude in meters el2 End altitude in meters
     *
     * @returns Distance in Meters
     */
    private double calculateDistance(
            double lat1, double lat2, double lon1,
            double lon2, double el1, double el2) {

        final int R = 6371; // Radius of the earth in KM

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);

        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        double distance = R * c * 1000; // convert to meters

        double height = el1 - el2;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance);
    }

    public boolean closeEnough(Double latitude, Double latitude0, Double longitude, Double longitude0, double boundary) {
        // se falhar um dos parâmetros não pode calcular a distância
        if ((latitude == null)
                || (latitude0 == null)
                || (longitude == null)
                || (longitude0 == null)) {
            return false;
        }

        double distance = calculateDistance(
                latitude,
                latitude0,
                longitude,
                longitude0,
                0, 0);  // ignorando a altura

        return (distance <= boundary);
    }

    private class NearestComparator implements Comparator<LocalVO> {

        private final PointDouble pTarget;

        public NearestComparator(LocalVO target) {
            pTarget = new PointDouble(target.getLatitude(), target.getLongitude());
        }

        @Override
        public int compare(LocalVO o1, LocalVO o2) {
            if (Objects.isNull(o1.getLatitude()) || Objects.isNull(o1.getLongitude())) {
                return -1;
            }
            if (Objects.isNull(o2.getLatitude()) || Objects.isNull(o2.getLongitude())) {
                return 1;
            }

            PointDouble p1 = new PointDouble(o1.getLatitude(), o1.getLongitude());
            double d1 = p1.distance(pTarget);
            PointDouble p2 = new PointDouble(o2.getLatitude(), o2.getLongitude());
            double d2 = p2.distance(pTarget);

            return (d1 < d2) ? -1 : ((d1 > d2) ? 1 : 0);
        }
    }

    @SuppressWarnings("CloneableImplementsClone")
    private class PointDouble extends Point2D {

        private double x;
        private double y;

        private PointDouble(double latitude, double longitude) {
            this.x = latitude;
            this.y = longitude;
        }

        @Override
        public double getX() {
            return x;
        }

        @Override
        public double getY() {
            return y;
        }

        @Override
        public void setLocation(double x, double y) {
            this.x = x;
            this.y = y;
        }

    }


    public boolean verificarSeOPontoEstaNaRegiao(double latitude, double longitude, LatLng[] thePath) {
        int crossings = 0;

        LatLng point = new LatLng(latitude, longitude);
        int count = thePath.length;
        // for each edge
        for (int i = 0; i < count; i++) {
            LatLng a = thePath[i];
            int j = i + 1;
            if (j >= count) {
                j = 0;
            }
            LatLng b = thePath[j];
            if (calcularDados(point, a, b)) {
                crossings++;
            }
        }
        // odd number of crossings?
        return (crossings % 2 == 1);
    }

    private boolean calcularDados(LatLng point, LatLng a, LatLng b) {
        try {
            double px = point.longitude;
            double py = point.latitude;
            double ax = a.longitude;
            double ay = a.latitude;
            double bx = b.longitude;
            double by = b.latitude;
            if (ay > by) {
                ax = b.longitude;
                ay = b.latitude;
                bx = a.longitude;
                by = a.latitude;
            }
            // alter longitude to cater for 180 degree crossings
            if (px < 0) {
                px += 360;
            }

            if (ax < 0) {
                ax += 360;
            }

            if (bx < 0) {
                bx += 360;
            }

            if (py == ay || py == by) {
                py += 0.00000001;
            }
            if ((py > by || py < ay) || (px > Math.max(ax, bx))) {
                return false;
            }
            if (px < Math.min(ax, bx)) {
                return true;
            }

            double red = (ax != bx) ? ((by - ay) / (bx - ax)) : Double.MAX_VALUE;
            double blue = (ax != px) ? ((py - ay) / (px - ax)) : Double.MAX_VALUE;
            return (blue >= red);
        }catch (Exception e){
            return false;
        }

    }

}
