package br.com.cmctransportes.cjm.utils;

import br.com.cmctransportes.cjm.domain.entities.Motoristas;
import org.hibernate.exception.ConstraintViolationException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.NumberFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author William Leite
 */
public class RodoviaUtils {

    public static Date resignationLimit(Date end, Motoristas driver) {
        if (Objects.nonNull(driver) && Objects.nonNull(driver.getDataDemissao())) {
            LocalDate endDate = TimeHelper.toLocalDate(end);
            Date resignation = driver.getDataDemissao();
            LocalDate resignationDate = TimeHelper.toLocalDate(resignation);

            if (endDate.isAfter(resignationDate)) {
                end = TimeHelper.fromLocalDate(resignationDate);
            }
        }

        return TimeHelper.getDate235959(end);
    }

    public static String handleConstraintViolation(ConstraintViolationException cause) {
        StringBuilder sb = new StringBuilder();
        sb.append("Erro de violação de restrição!<br />");
        sb.append(String.format("Nome restrição: [%s]<br />", cause.getConstraintName()));

        String probableCause = "";
        switch (cause.getConstraintName()) {
            case "evento_users_fk":
                probableCause = "Usuário possui eventos relacionados";
                break;
        }

        if (!probableCause.isEmpty()) {
            sb.append(String.format("Causa provável: [%s]", probableCause));
        }
        return sb.toString();
    }

    public static String cryptWithMD5(String pass) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] passBytes = pass.getBytes();
            md.reset();
            byte[] digested = md.digest(passBytes);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < digested.length; i++) {
                sb.append(String.format("%02x", (0xff & digested[i])));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(RodoviaUtils.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @SuppressWarnings("CallToPrintStackTrace")
    public static String exceptionMessage(Exception e) {
        e.printStackTrace();
        return String.format("Ocorreu um erro: %s", Objects.nonNull(e.getLocalizedMessage()) ? e.getLocalizedMessage() : e.toString());
    }

    //Need to be created because some date are created in java.sql.date incorrectly
    public static LocalDateTime convertToLocalDateTime(Long timestamp) {
        java.util.Date dataAdmissaoTmp = new java.util.Date(timestamp);
        Instant instant = dataAdmissaoTmp.toInstant();
        LocalDateTime result = (instant).atZone(ZoneId.systemDefault()).toLocalDateTime();
        return result;
    }


    public static Double convertStringToDouble(String valor){
        try {
            if(valor != null && !valor.isEmpty()){
                valor = valor.replaceAll(",", ".");
                valor = valor.replaceAll("R", "");
                valor = valor.replaceAll("\\$", "");
                valor = valor.trim();
                return Double.valueOf(valor);
            }
        }catch (Exception ed){}
        return null;
    }

    public static String convertDoubleToMoneyReal(Double valor){
        try{
            Locale ptBr = new Locale("pt", "BR");
            String valorString = NumberFormat.getCurrencyInstance(ptBr).format(valor);
            return valorString;
        }catch (Exception e){}
        return "";
    }

    public static String limpaCPF(String cpf){
        cpf = cpf.replaceAll("\\.","");
        cpf = cpf.replaceAll("-", "");
        return cpf;
    }
}
