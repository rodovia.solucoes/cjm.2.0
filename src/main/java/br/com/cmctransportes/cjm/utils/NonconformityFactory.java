package br.com.cmctransportes.cjm.utils;

import br.com.cmctransportes.cjm.domain.entities.Dsr;
import br.com.cmctransportes.cjm.domain.entities.Motoristas;
import br.com.cmctransportes.cjm.domain.entities.enums.NONCONFORMITY_CALCULUS_TYPE;
import br.com.cmctransportes.cjm.domain.entities.enums.NONCONFORMITY_TYPE;
import br.com.cmctransportes.cjm.domain.entities.enums.TREATMENT_TYPE;
import br.com.cmctransportes.cjm.domain.entities.vo.NonconformityVO;

import java.util.Date;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;

/**
 * @author William Leite
 */
public class NonconformityFactory {

    public static NonconformityFactory getInstance(Motoristas driver) {
        return new NonconformityFactory(driver);
    }

    private final Motoristas driver;

    private NonconformityFactory(Motoristas driver) {
        this.driver = driver;
    }

    public NonconformityVO interjourneyNormalPay(Date start, Date end, long gap) {
        NonconformityVO result = new NonconformityVO();

        result.setCalculusType(NONCONFORMITY_CALCULUS_TYPE.INTERJOURNEY);
        result.setType(NONCONFORMITY_TYPE.INTERJOURNEY_NORMAL_PAY);
        result.setDescription("Quebra de Interjornada");
        result.setDriver(this.driver.getId());

        result.setStartDate(start);
        result.setEndDate(end);

        result.setTreatmentType(TREATMENT_TYPE.NO_TREATMENT_AVAILABLE);
        result.setTreatmentDescription(String.format("Pagamento de hora normal no montante restante [%s]", TimeHelper.beautifyTime(gap, true)));
        result.setTreatmentArgs(String.format("{\"amount\": \"%s\"}", gap));

        return result;
    }

    public NonconformityVO interjourneyExtraPay(Date start, Date end, long gap) {
        NonconformityVO result = new NonconformityVO();

        result.setCalculusType(NONCONFORMITY_CALCULUS_TYPE.INTERJOURNEY);
        result.setType(NONCONFORMITY_TYPE.INTERJOURNEY_EXTRA_PAY);
        result.setDescription("Quebra de Interjornada");
        result.setDriver(this.driver.getId());

        result.setStartDate(start);
        result.setEndDate(end);

        result.setTreatmentType(TREATMENT_TYPE.NO_TREATMENT_AVAILABLE);
        result.setTreatmentDescription(String.format("Pagamento de hora extra no montante restante [%s]", TimeHelper.beautifyTime(gap, true)));
        result.setTreatmentArgs(String.format("{\"amount\": \"%s\"}", gap));

        return result;
    }

    public NonconformityVO interjourneyNightPay(Date start, Date end, long gap) {
        NonconformityVO result = new NonconformityVO();

        result.setCalculusType(NONCONFORMITY_CALCULUS_TYPE.INTERJOURNEY);
        result.setType(NONCONFORMITY_TYPE.INTERJOURNEY_NIGHT_PAY);
        result.setDescription("Quebra de Intejornada");
        result.setDriver(this.driver.getId());

        result.setStartDate(start);
        result.setEndDate(end);

        long extraNight = gap;
        long additionalNight = (long) (gap * 0.5);
        long interjourney = gap;

        double overtime = interjourney + ((extraNight + additionalNight) * 1.1428);

        result.setTreatmentType(TREATMENT_TYPE.NO_TREATMENT_AVAILABLE);
        result.setTreatmentDescription(String.format("Pagamento de hora extra com adicional noturno no montante restante [%s]", TimeHelper.beautifyTime((long) overtime, true)));
        result.setTreatmentArgs(String.format("{\"amount\": \"%s\", \"extraNight\": \"%s\", \"additionalNight\": \"%s\", \"interjourney\": \"%s\"}", gap + interjourney, extraNight, additionalNight, interjourney));

        return result;
    }

    @Deprecated
    public NonconformityVO dsrCount(Date start, Date end, Integer dsrCount, Integer weeksPassed) {
        NonconformityVO result = new NonconformityVO();

        result.setCalculusType(NONCONFORMITY_CALCULUS_TYPE.DSR);
        result.setType(NONCONFORMITY_TYPE.INVALID_DSR_COUNT);
        result.setDescription("Número de DSR no periodo é diferente do número de semanas");
        result.setDriver(this.driver.getId());

        result.setStartDate(start);
        result.setEndDate(end);

        result.setTreatmentType(TREATMENT_TYPE.NO_TREATMENT_AVAILABLE);
        result.setTreatmentDescription(String.format("DSR encontradas: [%s]; Semanas encontradas: [%s]", dsrCount, weeksPassed));
        result.setTreatmentArgs(String.format("{\"dsrCount\": \"%s\", \"weeksPassed\": \"%s\"}", dsrCount, weeksPassed));

        return result;
    }

    @Deprecated
    public NonconformityVO noAvailableSunday(Date start, Date end) {
        NonconformityVO result = new NonconformityVO();

        result.setCalculusType(NONCONFORMITY_CALCULUS_TYPE.DSR);
        result.setType(NONCONFORMITY_TYPE.NO_AVAILABLE_SUNDAY);
        result.setDescription("Não existem domingos disponíveis no período");
        result.setDriver(this.driver.getId());

        result.setStartDate(start);
        result.setEndDate(end);

        return result;
    }

    public NonconformityVO dsrWithCompensation(Date date, long gap) {
        NonconformityVO result = new NonconformityVO();

        result.setCalculusType(NONCONFORMITY_CALCULUS_TYPE.DSR);
        result.setType(NONCONFORMITY_TYPE.DSR_WITH_COMPENSATION);
        result.setDescription("DSR de 30H");
        result.setDriver(this.driver.getId());

        result.setStartDate(new Date(date.getTime() + TimeHelper.hours(11)));
        result.setEndDate(new Date(date.getTime() + gap));

        result.setTreatmentType(TREATMENT_TYPE.MANUAL_DSR_CREATION);
        result.setTreatmentDescription("Lançamento manual de DSR");

        return result;
    }

    public NonconformityVO perfectDSRScenario(Date date) {
        NonconformityVO result = new NonconformityVO();

        result.setCalculusType(NONCONFORMITY_CALCULUS_TYPE.DSR);
        result.setType(NONCONFORMITY_TYPE.PERFECT_DSR_SCENARIO);
        result.setDescription("Sugestão de DSR como hora extra (24h)");
        result.setDriver(this.driver.getId());

        result.setStartDate(date);
        result.setEndDate(new Date(date.getTime() + TimeHelper.VINTEQUATRO_HORAS - 1));

        result.setTreatmentType(TREATMENT_TYPE.MANUAL_DSR_CREATION);
        result.setTreatmentDescription("Criação manual de DSR como hora extra, com inclusão automatica de interjornada");

        return result;
    }

    @Deprecated
    public NonconformityVO noLiableSunday(Date start, Date end, List<Date> sundays) {
        NonconformityVO result = new NonconformityVO();

        result.setCalculusType(NONCONFORMITY_CALCULUS_TYPE.DSR);
        result.setType(NONCONFORMITY_TYPE.NO_LIABLE_SUNDAY);
        result.setDescription("Não existem domingos capazes de abrigar uma DSR (normal) no período");
        result.setDriver(this.driver.getId());

        result.setStartDate(start);
        result.setEndDate(end);

        return result;
    }

    public NonconformityVO dsrLackingCompensation(Entry<Date, Long> entry) {
        NonconformityVO result = new NonconformityVO();

        result.setCalculusType(NONCONFORMITY_CALCULUS_TYPE.DSR);
        result.setType(NONCONFORMITY_TYPE.DSR_LACKING_COMPENSATION);
        result.setDescription("DSR com menos de 35H, com falta de compensação");
        result.setDriver(this.driver.getId());

        result.setStartDate(TimeHelper.getDate000000(entry.getKey()));
        result.setEndDate(TimeHelper.getDate235959(entry.getKey()));

        result.setTreatmentType(TREATMENT_TYPE.NO_TREATMENT_AVAILABLE);
        result.setTreatmentDescription(String.format("Pagamento de [%s] horas extras.", entry.getValue()));
        result.setTreatmentArgs(String.format("{\"compensated\": %s}", entry.getValue()));
        return result;
    }

    @Deprecated
    public NonconformityVO invalidFirstDSR(Date firstDSRDate, long accumulatedDSR) {
        NonconformityVO result = new NonconformityVO();

        result.setCalculusType(NONCONFORMITY_CALCULUS_TYPE.DSR);
        result.setType(NONCONFORMITY_TYPE.INVALID_FIRST_DSR);
        result.setDescription("Primeira DSR (35H) inválida");
        result.setDriver(this.driver.getId());

        result.setStartDate(firstDSRDate);
        result.setEndDate(TimeHelper.getDate235959(firstDSRDate));

        result.setTreatmentType(TREATMENT_TYPE.NO_TREATMENT_AVAILABLE);
        if (accumulatedDSR < (TimeHelper.hours(35) / 2)) {
            result.setTreatmentDescription(String.format("DSR com [%s] horas; pagamento de 35 horas extra", accumulatedDSR / TimeHelper.UMA_HORA));
        } else {
            result.setTreatmentDescription(String.format("DSR com [%s] horas; pagamento de %s horas", accumulatedDSR / TimeHelper.UMA_HORA, (TimeHelper.hours(35) - accumulatedDSR) / TimeHelper.UMA_HORA));
        }

        return result;
    }

    public NonconformityVO presumableDSR(Dsr dsr) {
        NonconformityVO result = new NonconformityVO();

        result.setCalculusType(NONCONFORMITY_CALCULUS_TYPE.DSR);
        result.setType(NONCONFORMITY_TYPE.PRESUMABLE_DSR);
        result.setDescription("DSR criada automaticamente pelo Sistema");
        result.setDriver(this.driver.getId());

        if (Objects.nonNull(dsr.getInicioInterjornada())) {
            result.setStartDate(dsr.getInicioInterjornada());
        } else {
            result.setStartDate(dsr.getInicioDSR());
        }

        result.setEndDate(dsr.getFimDSR());

        return result;
    }

    public NonconformityVO noSundayDSR(Date start, Date end, Date leastWorkedSunday) {
        NonconformityVO result = new NonconformityVO();

        result.setCalculusType(NONCONFORMITY_CALCULUS_TYPE.DSR);
        result.setType(NONCONFORMITY_TYPE.NO_SUNDAY_DSR);
        result.setDescription("DSR não realizado em um domingo no mês");
        result.setDriver(this.driver.getId());

        result.setStartDate(start);
        result.setEndDate(end);

        result.setTreatmentType(TREATMENT_TYPE.MARK_DSR_HE);
        result.setTreatmentDescription("Marcação de jornada para recebimento de horas extras referentes a DSR faltante");

        return result;
    }

    public NonconformityVO dsrHE(Integer week, Date start) {
        NonconformityVO result = new NonconformityVO();

        week += 1;

        result.setCalculusType(NONCONFORMITY_CALCULUS_TYPE.DSR);
        result.setType(NONCONFORMITY_TYPE.DSRHE);
        result.setDescription(String.format("DSR não realizado na %sª semana", week));
        result.setDriver(this.driver.getId());

        result.setStartDate(new Date(start.getTime() + TimeHelper.days((week - 1) * 7)));
        result.setEndDate(new Date(start.getTime() + TimeHelper.days((week * 7) - 1)));

        result.setTreatmentType(TREATMENT_TYPE.MARK_DSR_HE);
        result.setTreatmentDescription("Marcação de jornada para recebimento de horas extras referentes a DSR faltante");

        return result;
    }
}
