package br.com.cmctransportes.cjm.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ArquivoDeConfiguracao {
    public String buscarValor(String chave){
        Properties prop = new Properties();
        try {
            InputStream inputStream = getClass()
                    .getClassLoader().getResourceAsStream("application.properties");
            //load a properties file from class path, inside static method
            prop.load(inputStream);

            //get the property value and print it out
            return prop.getProperty(chave);
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
        return "";
    }
}
