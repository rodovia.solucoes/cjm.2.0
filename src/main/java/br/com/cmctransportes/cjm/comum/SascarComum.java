package br.com.cmctransportes.cjm.comum;

import br.com.cmctransportes.cjm.domain.entities.Sascar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SascarComum {
    public static int cont = 0;
    private static final List<Sascar> listaSascar = new ArrayList<>();

    public static List<Sascar> getListaSascar() {
        List<Sascar> ll = Collections.synchronizedList(listaSascar);
        return ll;
    }

    public static void setListaSascar(List<Sascar> lista) {
        listaSascar.clear();
        listaSascar.addAll(lista);
    }

}
