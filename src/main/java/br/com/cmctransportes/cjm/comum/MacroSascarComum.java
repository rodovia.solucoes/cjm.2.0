package br.com.cmctransportes.cjm.comum;

import br.com.cmctransportes.cjm.domain.entities.MacroSascar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MacroSascarComum {

    public static int cont = 0;
    private static final List<MacroSascar> listaMacroSascar = new ArrayList<>();

    public static List<MacroSascar> getListaMacroSascar() {
        List<MacroSascar> ll = Collections.synchronizedList(listaMacroSascar);
        return ll;
    }

    public static void setListaMacroSascar(List<MacroSascar> lista) {
        listaMacroSascar.clear();
        listaMacroSascar.addAll(lista);
    }

}
