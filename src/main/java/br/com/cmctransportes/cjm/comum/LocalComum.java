package br.com.cmctransportes.cjm.comum;


import br.com.cmctransportes.cjm.domain.entities.vo.LocalVO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LocalComum {
    public static int cont = 0;
    private static final List<LocalVO> listaLocal = new ArrayList<>();

    public static List<LocalVO> getListaLocal() {
        List<LocalVO> ll = Collections.synchronizedList(listaLocal);
        return ll;
    }

    public static void setListaLocal(List<LocalVO> lista) {
        listaLocal.clear();
        listaLocal.addAll(lista);
    }
}
