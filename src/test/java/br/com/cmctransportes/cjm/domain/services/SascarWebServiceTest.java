//package br.com.cmctransportes.cjm.domain.services;
//
//import br.com.cmctransportes.cjm.Bootstrap;
//import br.com.cmctransportes.cjm.domain.entities.Motoristas;
//import br.com.cmctransportes.cjm.sascarsoap.SasIntegraNotification;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.web.server.LocalServerPort;
//
//import java.sql.Timestamp;
//import java.util.Date;
//
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = Bootstrap.class)
//public class SascarWebServiceTest {
//
//    @LocalServerPort
//    private int port;
//
//    @Autowired
//    private SascarEventWebService service;
//
//    @Autowired
//    private JornadaService jornadaService;
//
//    private DriverService driverService;
//
//    public SascarWebServiceTest(){
//
//    }
//    @Test
//    void serverEvent() throws SasIntegraNotification {
//        Motoristas motoristaId = new Motoristas();
//        motoristaId.setId(170);
//        java.util.Date date = new Date();
//        Long dateEnd = date.getTime();
//        Long startDate = (date.getTime() - 30000000);
//        //Long startDate = (date.getTime() - 3000000);
////        long lon1= 1580180400000L;
////        long lon2 = 1580439599000L;
//
//        String teste = this.service.ServerEvent(motoristaId, startDate, dateEnd);
//        Assertions.assertEquals("Concluido com sucesso", teste);
//        System.out.println(teste);
//    }
//
////    @Test
////    void getLast() {
////        Integer motoristaId = 171;
////        System.out.println(this.jornadaService.getLast(motoristaId));
////    }
//}
