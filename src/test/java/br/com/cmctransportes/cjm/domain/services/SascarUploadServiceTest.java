//package br.com.cmctransportes.cjm.domain.services;
//
//import br.com.cmctransportes.cjm.Bootstrap;
//import br.com.cmctransportes.cjm.domain.entities.Evento;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.web.server.LocalServerPort;
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.net.URISyntaxException;
//import java.nio.file.Files;
//import java.nio.file.Path;
//import java.nio.file.Paths;
//import java.util.Date;
//
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = Bootstrap.class)
//class SascarUploadServiceTest {
//
//    @LocalServerPort
//    private int port;
//
//    @Autowired
//    private SascarUploadService service;
//
//    @Test
//    void uploadFile() throws URISyntaxException, IOException {
//        Path path = Paths.get(SascarUploadServiceTest.class.getClassLoader().getResource("sascar.csv").toURI());
//        InputStream stream = Files.newInputStream(path);
//        String result = this.service.uploadFile(stream, 331, 1);
//    }
//
//    @Test
//    void convertLatitude() {
//        String latitude = "19° 54' 59\"\" S";
//        Double result = this.service.convertLatitude(latitude);
//        Assertions.assertEquals(-19.916389, result);
//    }
//
//    @Test
//    void convertLongitude() {
//        String longitude = "44° 2' 48\"\" W";
//        Double result = this.service.convertLongitude(longitude);
//        Assertions.assertEquals(-44.046668, result);
//    }
//
//    @Test
//    void convertEventType() {
//        String tipoEventoArquivo = "\"FIM DE JORNADA|LOGIN: 1606                              SENHA: 1606                              KM FINAL:  39353 \"";
//        String tipoEventoArquivo1 = "\"PARADAS|(X)ABASTECIMENTO                         ( )LANCHE                                ( )BANHEIRO                              ( )REPOUSO DIRECAO                       ( )INTERVALO FEMININO                    ( )SINISTRO                              ( )PISTA INTERDITADA                     ( )INTERVALO PESSOAL                     \"";
//        String tipoEventoArquivo2 = "FIM DE DIRECAO|";
//        String tipoEventoArquivo3 = "INICIO DE DIRECAO|";
//        String tipoEventoArquivo4 = "CARGA/DESCARGA|( )AGUARDANDO CARGA                      ( )CARGA                                 ( )AGUARDANDO DESCARGA                   (X)DESCARGA                              ( )MANIFESTO";
//        String tipoEventoArquivo5 = "FIM DE REFEICAO|";
//        String tipoEventoArquivo6 = "INICIO DE REFEICAO|";
//        String tipoEventoArquivo7 = "CARGA/DESCARGA|( )AGUARDANDO CARGA                      ( )CARGA                                 (X)AGUARDANDO DESCARGA                   ( )DESCARGA                              ( )MANIFESTO";
//        String tipoEventoArquivo8 = "FIM DE REFEICAO|";
//        String tipoEventoArquivo9 = "INICIO DE REFEICAO|";
//        String tipoEventoArquivo10 = "\"INICIO DE JORNADA|LOGIN: 1606                              SENHA: 1606                              KM INICIAL:  39320 \"";
//        String tipoEventoArquivo11 = "\"FIM DE JORNADA|LOGIN: 1606                              SENHA: 1606                              KM FINAL:  39320 \"";
//        String tipoEventoArquivo12 = "FIM DE DIRECAO|";
//        String tipoEventoArquivo13 = "INICIO DE DIRECAO|";
//        String tipoEventoArquivo14 = "\"INICIO DE JORNADA|LOGIN: 1606                              SENHA: 1606                              KM INICIAL:  39238 \"";
//
//        Integer result = this.service.convertEventType(tipoEventoArquivo);
//        Assertions.assertEquals(0, result);
//        Integer result1 = this.service.convertEventType(tipoEventoArquivo1);
//        Assertions.assertEquals(7, result1);
//        Integer result2 = this.service.convertEventType(tipoEventoArquivo2);
//        Assertions.assertEquals(-8, result2);
//        Integer result3 = this.service.convertEventType(tipoEventoArquivo3);
//        Assertions.assertEquals(8, result3);
//        Integer result4 = this.service.convertEventType(tipoEventoArquivo4);
//        Assertions.assertEquals(12, result4);
//        Integer result5 = this.service.convertEventType(tipoEventoArquivo5);
//        Assertions.assertEquals(-14, result5);
//        Integer result6 = this.service.convertEventType(tipoEventoArquivo6);
//        Assertions.assertEquals(14, result6);
//        Integer result7 = this.service.convertEventType(tipoEventoArquivo7);
//        Assertions.assertEquals(11, result7);
//        Integer result8 = this.service.convertEventType(tipoEventoArquivo8);
//        Assertions.assertEquals(-14, result8);
//        Integer result9 = this.service.convertEventType(tipoEventoArquivo9);
//        Assertions.assertEquals(14, result9);
//        Integer result10 = this.service.convertEventType(tipoEventoArquivo10);
//        Assertions.assertEquals(1, result10);
//        Integer result11 = this.service.convertEventType(tipoEventoArquivo11);
//        Assertions.assertEquals(0, result11);
//        Integer result12 = this.service.convertEventType(tipoEventoArquivo12);
//        Assertions.assertEquals(-8, result12);
//        Integer result13 = this.service.convertEventType(tipoEventoArquivo13);
//        Assertions.assertEquals(8, result13);
//        Integer result14 = this.service.convertEventType(tipoEventoArquivo14);
//        Assertions.assertEquals(1, result14);
//    }
//
//    @Test
//    void createEvento() {
//        Date instanteEvento = new Date("25/11/2019 15:25:25");
//        Date instanteLancamento = new Date("25/11/2019 15:25:25");
//        Double latitude = -19.916389;
//        Double longitude = -44.046668;
//        Integer tipoEvento = 1;
//        String pontoParada = "Soma Logistica";
//        Integer motorista = 274;
//        Integer empresa = 1;
//
//        Evento result = this.service.createEvento(instanteEvento, instanteLancamento, latitude, longitude, tipoEvento, pontoParada, motorista, empresa);
//
//        Assertions.assertEquals(instanteEvento, result.getInstanteEvento());
//        Assertions.assertEquals(instanteLancamento, result.getInstanteLancamento());
//        Assertions.assertEquals(latitude, result.getLatitude());
//        Assertions.assertEquals(longitude, result.getLongitude());
//    }
//
//
//}